#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
//#include <linux/eventfd.h>
#include "unistd.h"
#include <fcntl.h>
#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <math.h>
#include <errno.h>
#include <semaphore.h>
//#include <linux/time.h>

#include "amsg_c.h"
#include "clientsock.h"
#include "crc32.h"
#include "timer.h"

#include "port_settings.h"

static char* Version = "1.1"; //no more odd length msg, only even length for new AERA_TRANSPORT_LAYER

static struct connCliInfo ConnCliInfoArray[2];


struct performanceTimer PTimer;
struct performanceTimer LatencyTimer;
static struct performanceTimer Program_timer;
static struct performanceTimer BW_timer;

static unsigned long long T2_len_acc = 0;
static unsigned long long T3_len_acc = 0;

static void usage(char *s)
{
	static char *usage_text  = "\
    -N no T2 send\n\
	-n no T3 send\n\
	";
	//old arguments:
	//-S sleep 1ms after each write() call, default disabled
	//-s sleep 1ms after each write() error call, default enabled
	//-w x send packet id from 0-(x-1) x<=8 comms packets

	printf("*********************************\n");
	printf("*KIT_comms application, ver %s *\n", Version);
	printf("*********************************\n");
	printf("usage: %s [options]\n", s);
    printf("%s", usage_text);
}

int main(int argc, char **argv)
{
	extern char *optarg;
	extern int optind;

	int c;

	int rd_timeout = 125; //msec
	bool isT2gen = true;
	bool isT3gen = true;

    char *pname;
    pname = *argv;

    // parse command line
    while ((c = getopt(argc, argv, "Nn")) != EOF)
    {
		switch (c)
		{
			case 'N':
				isT2gen = false;
				break;
			case 'n':
				isT3gen = false;
				break;
			case 'h':
			default: usage(pname); exit(0);
		}
    }

    reset_performanceTimer(&BW_timer, false, stdout);
    reset_performanceTimer(&Program_timer, false, stdout);
    reset_performanceTimer(&LatencyTimer, false, stdout);
    reset_performanceTimer(&PTimer, true, stdout);

    fflush(stdout);

    /* the second file for writing */
    FILE* log_fd = fopen("./cli_log", "w");
    if (log_fd == NULL) {
    	fprintf(stderr, "%s - cannot append ./cli_log to write\n", __FUNCTION__);
    	exit(EXIT_FAILURE);
    }
    snapshot_performanceTimer(&PTimer, true, log_fd);
    fprintf(log_fd, "**** new-run ****\n");
    fclose(log_fd);

    chksum_crc32gentab();
    time_t seed = time(NULL);
    srand (1345138369);
    fprintf(stdout, "random seed: %u\n", (unsigned int)seed);

    initConnCliInfo(&ConnCliInfoArray[0], "RT_CONN", "localhost", PORT_NUMBER_RT);
    initConnCliInfo(&ConnCliInfoArray[1], "HQ_CONN", "localhost", PORT_NUMBER_HQ);

	//int ind;
	int loopCount = 0;
	int secondCnt = 0;
	void* amsg_c_r;
	uint8_t* amsg_c_t;
	uint8_t connIndex;

	uint32_t latency;
	uint16_t length;

	while (1)
	{
		// look for lost stations and Eb,T3,Gui to be accepted
		if (loopCount % 100 == 0) {
			recoverConnectionIfLost(&ConnCliInfoArray[0]);
			recoverConnectionIfLost(&ConnCliInfoArray[1]);
		}
		amsg_c_r = NULL;
		readFromClientConnections(ConnCliInfoArray, 2, &amsg_c_r, &connIndex, rd_timeout*1000);
		if (amsg_c_r != NULL) {
			autocheck_testdata(amsg_c_r, (connIndex==0)?true:false, &latency, true); //connIndex 0 is RT, otherwise HQ
			free(amsg_c_r);		// only necessary for ReadSockMsg, ReadSockMsgSM_TO uses static memories
		}
		if (snapshot_performanceTimer(&BW_timer, false, stdout) >= 1000) { //each second do something e.g T2 = 800 B/s
			if (isT2gen == true) {
				length = ((uint16_t)rand()*2)%800+10; //at least 10 bytes =2 byte runningno + 4 bytes time + 4 bytes CRC
				amsg_c_t = create_test_amsg_c(length, true);
				if (amsg_c_t != NULL) {
					if (writeToClientConnection(&ConnCliInfoArray[0], amsg_c_t) == 0) { //for RT
						T2_len_acc += length;
					}
					free(amsg_c_t);
				} else {
					fprintf(stderr, "MEM_ERR!%s - socket connection send data malloc fail!\n", __FUNCTION__);
				}
			}

			if (isT3gen == true) {
				if (secondCnt%60 == 20) { // 24576 B avg / 60 sec = 400 B/s
					length = ((uint16_t)rand()*2)%16384+16384; //from 16k to 32k, surely bigger than 10 bytes
					amsg_c_t = create_test_amsg_c(length, false);
					if (amsg_c_t != NULL) {
						if (writeToClientConnection(&ConnCliInfoArray[1], amsg_c_t) == 0) { //for HQ
							T3_len_acc += length;

							/* debug log */
							log_fd = fopen("./cli_log", "a");
							if (log_fd == NULL) {
								fprintf(stderr, "%s - cannot append ./cli_log to write\n", __FUNCTION__);
								exit(EXIT_FAILURE);
							}
							unsigned long t_diff = snapshot_performanceTimer(&PTimer, true, log_fd); fprintf(log_fd, ": A T3 is sent, t_diff: %lu\n", t_diff);
							fclose(log_fd);
							if (t_diff > 24*3600*1000) { //1 day
							 reset_performanceTimer(&PTimer, false, stdout); //reset
							 T3_len_acc = 0;
							 T2_len_acc = 0;
							}
							/* debug log end*/
						}
						free(amsg_c_t);
					} else {
						fprintf(stderr, "MEM_ERR!%s - socket connection send data malloc fail!\n", __FUNCTION__);
					}
				}
			}
			secondCnt++;
			reset_performanceTimer(&BW_timer, false, stdout);
		}

		if (snapshot_performanceTimer(&Program_timer, false, stdout) >= 1*30*1000) { //30 seconds, plot something
			log_fd = fopen("./cli_log", "a");
		    if (log_fd == NULL) {
		    	fprintf(stderr, "%s - cannot append ./cli_log to write\n", __FUNCTION__);
		    	exit(EXIT_FAILURE);
		    }
		    snapshot_performanceTimer(&PTimer, true, log_fd); fprintf(log_fd, "alive, T2_acc: %llu, T3_acc: %llu\n", T2_len_acc, T3_len_acc);
		    fclose(log_fd);

		    reset_performanceTimer(&Program_timer, false, stdout);
		}
		loopCount++;
	}
}
