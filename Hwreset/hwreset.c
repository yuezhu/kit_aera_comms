/*
 * hwreset.c
 *
 *  Created on: Sep 20, 2012
 *      Author: dev
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>


#define TS7200_WATCHDOG_CTRL    0x23800000      // read/write
// must feed watchdog less than 30uSecs before writing this reg
//      val 0x00 = watchdog disabled
//      val 0x01 = 250 mSec watchdog reset counter
//      val 0x02 = 500 mSec watchdog reset counter
//      val 0x03 = 1000 mSec watchdog reset counter
//      val 0x04 = reserved
//      val 0x05 = 2000 mSec watchdog reset counter
//      val 0x06 = 4000 mSec watchdog reset counter
//      val 0x07 = 8000 mSec watchdog reset counter

#define TS7200_WATCHDOG_FEED    0x23C00000      // write only
//      val 0x05 = feed the watchdog to restart the reset counter
//      bit 2 = pulses at 32kHz (I think)

//use SBC-CPLD-WDT (not CPU-WDT) to
int main()
{
	int fd;
	char* ctrl;
	char* feed;
	if ((fd = open("/dev/mem", O_RDWR|O_SYNC)) == -1)
	{
	    perror("Cannot open mem\n");
	    exit(1);
	}
	if ((ctrl = (char*)mmap(0, 4096, PROT_READ|PROT_WRITE, MAP_SHARED, fd, TS7200_WATCHDOG_CTRL)) == (void *)-1)
	{
	    perror("mmap error\n");
	    exit(1);
	}
	if ((feed= (char*)mmap(0, 4096, PROT_READ|PROT_WRITE, MAP_SHARED, fd, TS7200_WATCHDOG_FEED)) == (void *)-1)
	{
	    perror("mmap error\n");
	    exit(1);
	}

	//feed WDT first then start WDT
	*feed =  0x05;

	//start WDT with 8 second within 30us after first feed
	*ctrl = 0x07;

	//just endless loop with no feed, giving something
	while(1) {
		printf(".");
		fflush(stdout);
		usleep(1000000);
	}
}


