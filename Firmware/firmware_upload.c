/********************************************************************/
/*																	*/
/*  FIRMWARE_UPLOAD													*/
/*  ===============													*/
/*																	*/
/*  Program to send a new firmware to base station and local 		*/
/*  stations of the AERA-communication project.					 	*/
/*																	*/
/*  Author:		Fridtjof Feldbusch									*/
/*  Date: 		23.5.2012											*/
/*	Version:	2.0													*/
/*																	*/
/********************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h> // getopt()
#include <string.h>
#include <assert.h>
#include <math.h>    // Linux: set linker flag -lm
#include <stdbool.h> // data type bool
#include <pthread.h>

#include "alt_types.h" // fixed size data types
#include "crc32.h"
#include "datap.h"
#include "netmon.h"
#include "ncp.h"
#include "epcs.h" // Flash HW addresses
#include "tdma_protocol.h" // lsdata addresses
#include "conversion.h"

#define MAX_THREADS 256
#define _ceilf(a) (((a) - (int)(a)) > 0.0 ? (int)(a) + 1 : a)
#define BLOCKSIZE 512
#define FW_PKT_VALIDATION_CODE 0x50ABA83F
#define MAX_LOCAL_STATIONS 250 //maximal 250 SU IDs in FW start packet
#define MAX_SU_ID 4096

pthread_mutex_t gpio_mutex=PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t data_mutex=PTHREAD_MUTEX_INITIALIZER;

typedef enum {
    FW_START    = 1,
    FW_FRACTION = 2,
    FW_STOP = 3,
    FW_FB_START = 4,
    FW_FB_STOP = 5,
    FW_FB_ERR = 6
} firmware_logic_pkt_type;

typedef enum {
	FW_DONOTHING=-1,
	FW_IDLE=0,
	FW_STARTED=1,
	FW_STOPPED=2,
	FW_FINISH=3,
	FW_HALTED=4
} transfer_host_states;

typedef enum {
	FW_START_INVALID    = 1,
	FW_ERASE_ONGOING   = 2,
	FW_ERASE_FAILED    = 3,
	FW_ERASE_SUCCESS   = 4
} start_ack_type;

//0: ignore list, FW for all, 1: FW for SUs in list, 2: FW for SUs not in list, 3: ignore list, FW for none
typedef enum {
    IGNORE_LIST_VALID = 0,
    IN_LIST_VALID = 1,
    NOT_IN_LIST_VALID = 2,
    IGNORE_LIST_INVALID = 3
} mode_id_list_types;

typedef enum {
    LOCAL    = 1, // --> BS
    REMOTE   = 2  // --> LS
} source_types;

typedef enum {
	REMOTE_HOST = 0,
	LOCAL_HOST = 1,
	REMOTE_GATEWAY = 2,
	LOCAL_GATEWAY = 3
} component_code;

struct lsdata_t {
	unsigned char data[512];  // Byte 0 - 511
	unsigned char validation[4]; // Byte 512 - 516;
	unsigned char type; // Byte 517
	unsigned char misc[23]; // 518 - 540
};

typedef struct lsdata_t lsdata_struct;

struct log_datap_t {
	unsigned int sub_packet_no;
	unsigned int su_id;
	network_monitor_data nw_monitor_data;
	unsigned int packet_id;
	int ls_data_length;
	unsigned char ls_data[540];
	unsigned char ls_runningNo;
	unsigned long crc1;
	unsigned long current_crc1;
	unsigned long crc2;
	unsigned long current_crc2;
};

typedef struct log_datap_t log_datap;

struct feedback_t {
	bool crc;
	bool reconfig;
	alt_u16 num_fracs_retrans;
};

typedef struct feedback_t feedback_struct;

// Argument structure for threads
struct arg_t {
	datap *data_packet;
	firmware_logic_pkt_type pkt_type;
};

typedef struct arg_t arg_struct;

/****************************************************************/
/* Functions for sending data packets to base/local station		*/
/****************************************************************/

void init_data_packet(datap *ptr_datap) {
	int i;
	unsigned char nmd[20];
	for (i = 0; i < 20; i++) nmd[i] = 0;
	unsigned char lsd[544];
	for (i = 0; i < 544; i++) lsd[i] = 0;
	set_dp_sub_packet_no(ptr_datap, 0);
	set_SU_ID(ptr_datap, 0);
	set_net_monitor_data(ptr_datap, &nmd[0]);
	set_packet_id(ptr_datap, 0);
	set_packet_id_runnung_no(ptr_datap, 0);
	set_ls_data_length(ptr_datap, 0);
	set_ls_data(ptr_datap, &lsd[0]);
};

void init_ls_data(lsdata_struct *lsdata) {
	int i;
	for  (i = 0; i < 512; i++) lsdata->data[i] = 0;
	for  (i = 0; i < 4; i++) lsdata->validation[i] = 0;
	lsdata->type = 0;
	for  (i = 0; i < 23; i++) lsdata->misc[i] = 0;
};

void init_feedback(feedback_struct *feedback) {
	int i;
	for  (i = 0; i < MAX_SU_ID; i++) feedback[i].crc = false;
	for  (i = 0; i < MAX_SU_ID; i++) feedback[i].reconfig = false;
	for  (i = 0; i < MAX_SU_ID; i++) feedback[i].num_fracs_retrans = 0;
}


int read_destinations(char *filename, unsigned short *destinations) {
	FILE *file;
	char line[80];
	int i;
	for (i = 0; i < MAX_LOCAL_STATIONS; i++) destinations[i] = 0;
	file = fopen(filename, "r");
	if (file == NULL) {
		printf("File %s could not be opened!\n", filename);
		return -EXIT_FAILURE;
	}
	i = 0;
	while(fgets(&line[0], 80, file)) {
		destinations[i] = (unsigned short)(atoi(line));
		i++;
	}
	fclose(file);
	return i;
}

/*
int get_id_list_from_file(const char* filename, alt_u8* id_list_len, alt_u16 id_list[])
{
	FILE *f;
	char line[256];
	int ofs;
	*id_list_len = 0;
	if((f = fopen(filename, "rt")) != NULL)
	{
		while (read_line(f, line, sizeof(line))) {
			if (*id_list_len >= MAX_LOCAL_STATIONS) {
				fprintf(stderr, "id list in file %s too long, more than %d elements\n", filename, MAX_LOCAL_STATIONS);
				return -1;
			}
			sscanf(line, "%u", &id_list[*id_list_len]);
			if (id_list[*id_list_len]>=MAX_SU_ID) {
				fprintf(stderr, "id at %d line in file %s is invalid\n", (*id_list_len+1), filename);
				return -2;
			}
			(*id_list_len)++;
		}
		fclose(f);
		printf("id list in file %s is successfully loaded with %d elements\n", filename, *id_list_len);
		return 0;
	}
	fprintf(stderr, "%s not found\n", filename);
	return -3;
}
*/

unsigned char encode_packet_id(unsigned char service, component_code destination) {
	unsigned char packet_id;
	packet_id = (unsigned char) (destination<<6)|(service&0x07);
	return packet_id;
};

void set_dest_SU_ID(lsdata_struct *lsdata, unsigned int id, int index) {
	lsdata->data[5 + index*2] &= ~(0xf);
	lsdata->data[5 + index*2] |= (id >> 8) & 0xf; 		// set bits 3,2,1 and 0 with high part of id
	lsdata->data[5 + index*2 + 1] = id & 0xff;		 	// set lower part if id
}

datap create_start_packet(alt_u8 sector_base, unsigned char service, component_code destination, unsigned long length,
		mode_id_list_types id_list_type, alt_u8 id_list_len, alt_u16 id_list[]) {
	datap start_packet;
	lsdata_struct lsdata;
	init_ls_data(&lsdata);
	int j;
	firmware_logic_pkt_type packet_type = FW_START;
	lsdata.type = (unsigned char) packet_type & 0xff;

	lsdata.data[FW_SECTOR_BASE_OFS] = sector_base;
	conv_to_u8_arr(&(lsdata.data[FW_UPDATE_SIZE_OFS]), length, FW_UPDATE_SIZE_LEN);

	//extended for addressing start, Yue Zhu 08.11.2012
	lsdata.data[FW_ID_LIST_MODE_OFS] = (unsigned char)id_list_type;
	id_list_len = (id_list_len > MAX_LOCAL_STATIONS)?MAX_LOCAL_STATIONS:id_list_len; //saturation
	lsdata.data[FW_ID_LIST_SIZE_OFS] = id_list_len;
	for (j=0; j<id_list_len; j++) {
		conv_to_u8_arr(&(lsdata.data[FW_ID_LIST_OFS+FW_ID_LEN*j]), id_list[j], FW_ID_LEN);
	}
	//extended for addressing end, Yue Zhu 08.11.2012

	conv_to_u8_arr(&lsdata.validation[0], FW_PKT_VALIDATION_CODE, FW_VALIDATION_LEN);
	start_packet.onair_id[0] = 0;
	start_packet.onair_id[1] = 0;
	for (j = 0; j < 20; j++) start_packet.nw_monitor[j] = 0;
	start_packet.packet_id = encode_packet_id(service, destination);
	set_ls_data_length(&start_packet, FW_START_PKT_BYTES);
	memcpy(&(start_packet.ls_data), &lsdata, sizeof(lsdata));
	set_crc1(&start_packet);
	set_crc2(&start_packet);
	return start_packet;
}

datap create_data_packet(unsigned char service, component_code destination, unsigned short runningnumber, char *ptr_buffer, int size) {
	datap data_packet;
	lsdata_struct lsdata;
	init_ls_data(&lsdata);
	int j;
	firmware_logic_pkt_type packet_type = FW_FRACTION;
	lsdata.type = (unsigned char) packet_type & 0xff;
	conv_to_u8_arr(&lsdata.validation[0], FW_PKT_VALIDATION_CODE, FW_VALIDATION_LEN);
	memcpy(&(lsdata.data[FW_FRACTION_OFS]), ptr_buffer, size);
	conv_to_u8_arr(&(lsdata.data[FW_CUR_FRACTION_OFS]), runningnumber, FW_CUR_FRACTION_LEN);
	// create data packet
	data_packet.onair_id[0] = 0;
	data_packet.onair_id[1] = 0;
	for (j = 0; j < 20; j++) data_packet.nw_monitor[j] = 0;
	data_packet.packet_id = encode_packet_id(service, destination);
	set_ls_data_length(&data_packet, FW_FRACTION_PKT_BYTES);
	memcpy(&(data_packet.ls_data), &lsdata, sizeof(lsdata));
	set_crc1(&data_packet);
	// for debug purposes:
/*	if (runningnumber == 10) {
		unsigned char dummy0 = data_packet.ls_data[0];
		unsigned char dummy1 = data_packet.ls_data[1];
		unsigned char dummy2 = data_packet.ls_data[2];
		data_packet.ls_data[0] = 0;
		data_packet.ls_data[1] ^= 255;
		data_packet.ls_data[2] = 255;
		set_crc1(&data_packet);
		data_packet.ls_data[0] = dummy0;
		data_packet.ls_data[1] = dummy1;
		data_packet.ls_data[2] = dummy2;
	}*/
	set_crc2(&data_packet);
	return data_packet;
}

datap create_stop_packet(unsigned char service, component_code destination, int crc, bool isReconfigReq) {
	datap stop_packet;
	lsdata_struct lsdata;
	init_ls_data(&lsdata);
	int j;
	firmware_logic_pkt_type packet_type = FW_STOP;
	lsdata.type = (unsigned char) packet_type & 0xff;
	conv_to_u8_arr(&(lsdata.data[FW_CRC32_OFS]), crc, FW_CRC32_LEN);
	lsdata.data[FW_RECONFIG_REQ_OFS] = isReconfigReq;
	conv_to_u8_arr(&lsdata.validation[0], FW_PKT_VALIDATION_CODE, FW_VALIDATION_LEN);
	stop_packet.onair_id[0] = 0;
	stop_packet.onair_id[1] = 0;
	for (j = 0; j < 20; j++) stop_packet.nw_monitor[j] = 0;
	stop_packet.packet_id = encode_packet_id(service, destination);
	set_ls_data_length(&stop_packet, FW_STOP_PKT_BYTES);
	memcpy(&(stop_packet.ls_data), &lsdata, sizeof(lsdata));
	set_crc1(&stop_packet);
	set_crc2(&stop_packet);
	return stop_packet;
}

static void *send_packet_th(void *arg) {
	arg_struct *data = (arg_struct *) arg;
	datap data_packet;
	memcpy(&data_packet, data->data_packet, sizeof(data_packet));
	firmware_logic_pkt_type pkt_type = data->pkt_type;
	pthread_mutex_unlock(&data_mutex);
	int success = 1; // 1: sending data_packet was not successful, 0: sending data_packet was successful
	int i,j;
	// send data_packet to base station
	i = 0;
	while ((i < 5) && (success == 1)) {  // slow retransmission
		j = 0;
		pthread_mutex_lock(&gpio_mutex);
		while ((j < 100000) && (success == 1)) { // fast retransmission
			success = try_write_datap(&data_packet, data_packet.packet_id);
			j++;
		}
		pthread_mutex_unlock(&gpio_mutex);
		//sleep(1);
		i++;
	}
	if (success == 1) {
		printf("\nCould *not* send data packet ID 0x%x!\n", data_packet.packet_id);
	}
	pthread_exit((void*)success);
}

static int send_packet(datap *data_packet, firmware_logic_pkt_type pkt_type) {
	int success = 1; // 1: sending data_packet was not successful, 0: sending data_packet was successful
	int i,j;
	// send data_packet to base station
	i = 0;
	while ((i < 5) && (success == 1)) {  // slow retransmission
		j = 0;
		while ((j < 100000) && (success == 1)) { // fast retransmission
			success = try_write_datap(data_packet, data_packet->packet_id);
			j++;
		}
		//sleep(1);
		i++;
	}
	if (success == 1) {
		printf("\nCould *not* send data packet ID 0x%x!\n", data_packet->packet_id);
	}
	return success;
}

bool is_firmware_packet(datap* data) {
    alt_u32 validation_code = conv_u8_arr(&data->ls_data[FW_VALIDATION_OFS], FW_VALIDATION_LEN);
    if (validation_code == FW_PKT_VALIDATION_CODE) {
        return true;
    }
    return false;
}

firmware_logic_pkt_type get_type(datap* data) {
    return (data->ls_data[FW_TYPE_OFS]);
}

lsdata_struct unpack_fb_packet(datap* packet) {
	lsdata_struct lsdata;
	memcpy(&lsdata, &(packet->ls_data[0]), sizeof(lsdata));
	return lsdata;
}

void print_packet(datap* data) {
	unsigned char packet_id = get_packet_id(data);
	unsigned char code = (packet_id & 0x07);
	unsigned char id = (packet_id >> 6);
	int length = get_ls_data_length(data);
	int i;
	printf("id: %d\ndest: %d\ndata:\n", id, code);
	for (i=0; i<length; i++) {
		printf("%d: %d, ", i, data->ls_data[i]);
		if ((i%10) == 9) printf("\n");
	}
	//printf("\ncrc1: %lx\n", get_crc1(data));
}

void print_buffer(unsigned char* data) {
	int i;
	printf("Data to send:\n");
	for (i=0; i<BLOCKSIZE; i++) {
		printf("%d: %d, ", i, data[i]);
		if ((i%10) == 9) printf("\n");
	}
	printf("\n");
}

void print_usage() {
	printf("\nUsage:\n");
	printf("Firmware [options] -f <file>\n");
	printf("    -f filename, binary file of firmware for update \n");
	printf("options:\n");
	printf("    -S the firmware will be loaded to all the subscriber units (default: base station)\n");
	printf("    -s <file> the firmware will be loaded to the subscriber units defined in file\n");
	printf("    -n <file> the firmware will be loaded to the subscriber units not defined in file\n");
	printf("    -H the firmware is hardware image\n");
	printf("    -F the firmware file is a factory image, DANGEROUS! if factory image is corrupt, a field reflash is needed\n");
    printf("    -R reconfigure after firmware is transfered, default off\n");
}

/****************************************************************/
/* Main program													*/
/****************************************************************/

int main(int argc, char *argv[]) {

	pthread_t th[MAX_THREADS];
	static int ret[MAX_THREADS]; // Array with return values of the threads
	int th_counter;
	extern char *optarg; // global variable
	extern int optind;   // global variable
	char *su_filename = NULL;
	unsigned short destinations[MAX_LOCAL_STATIONS];
	// FW file:
	char *filename = NULL;
	FILE *file;
	unsigned long file_length = 0;
	unsigned long rest_length = 0;
	char buffer[BLOCKSIZE];
	//int EOF_flag = 0;
	alt_u32 file_crc = 0xffffffff; // data type must be 32 bit !!
	// data packets
	datap data_packet;
	datap receive_data_packet;
	unsigned int su_id;
	unsigned short packet_no = 0;
	int packets_to_send;
	int packets_sent;
	int packets_received;
	arg_struct data;
	// packet content
	alt_u8 sector_base;
	unsigned char service; // changing between 0 and 1
	component_code destination;
	lsdata_struct lsdata;
	// flow control
	int Sent_flag = 0;
	bool isFwFileHW = false;
	bool isFwFileFactory = false;
	bool isReconfig = false;
	bool destSU = false;
	static transfer_host_states State = FW_DONOTHING;
	int data_to_read;
	int packet_read_flag = 1; // 0: success, 1: failure
	bool crc_ok;
	bool reconfig_ack;
	alt_u16 num_fracs_retrans;
	feedback_struct feedback[MAX_SU_ID];
	int num_of_fb_stop_packets = 0;
	bool wait_fb_stop = true;
	int num_of_stations = MAX_LOCAL_STATIONS;
	int max_time_counter = 0;
	mode_id_list_types modeIdListTypes = IGNORE_LIST_VALID;
	// misc
	int i, j, c;

	printf("\nAERA Firmware Upload Program 2.0\n");
	printf("================================\n\n");
	while ((c = getopt(argc, argv, "hf:Ss:n:HFR")) !=EOF) {
		switch(c) {
		case 'h':
			print_usage();
			break;
		case 'f':
			filename = optarg;
			break;
		case 'S':
			destSU = true;
			//modeIdListType = IGNORE_LIST_VALID; no change to init value
			break;
		case 's':
			destSU = true;
			su_filename = optarg;
			modeIdListTypes = IN_LIST_VALID;
			break;
		case 'n':
			destSU = true;
			su_filename = optarg;
			modeIdListTypes = NOT_IN_LIST_VALID;
			break;
		case 'H':
			isFwFileHW = true;
			break;
		case 'F':
			isFwFileFactory = true;
			break;
		case 'R':
			isReconfig = true;
			break;
		default:
			print_usage();
			exit(0);
		}
	}
	if (destSU == true && isFwFileFactory == true) {
		printf("Remote factory firmware update to subscribers is not allowed!\n");
		return EXIT_FAILURE;
	}
    chksum_crc32gentab();
    srand ( time(NULL) );
    file_crc = 0xffffffff;
    sector_base = isFwFileFactory==false?(isFwFileHW==true?HW2_OFS:SW2_OFS):(isFwFileHW==true?HW1_OFS:SW1_OFS);
    if (destSU) {
    	destination = REMOTE_GATEWAY;
    } else {
    	destination = LOCAL_GATEWAY;
    }
    if (su_filename != NULL) {
    	num_of_stations = read_destinations(su_filename, &destinations[0]);
    	if (num_of_stations < 0) {
    		fprintf(stderr, "no id in file %s not found\n", su_filename);
    		return EXIT_FAILURE;
    	} else {
    		//print destinations
    		printf("%d destinations: ", num_of_stations);
    		for (i=0; i<num_of_stations; i++) {
    			if (destinations[i] >= MAX_SU_ID) {
    	    		fprintf(stderr, "%d id invalid!\n", i+1);
    	    		return EXIT_FAILURE;
    			}
    			printf("%d, ", destinations[i]);
    		}
    		printf("\n");
    	}
    }

	printf("Check BS availability ... ");
	while (bs_available() == 0) {}; // Wait: BS available? Exit when true
	printf("BS available!\n");
	// Send NCP?
	// Loop for sending and receiving packets
	while ((State != FW_FINISH) || (wait_fb_stop)) {
		// Sending packages
		if (State == FW_DONOTHING) {
			State = FW_IDLE;
		}
		if (State == FW_IDLE) {
			th_counter = 0;
			init_feedback(&feedback[0]);
			// open firmware file
			if (filename != NULL) {
				setbuf(stdout, NULL); // disable buffering of stdout
				file = fopen(filename, "r");
				if (file == NULL) {
					printf("File %s could not be opened!\n", filename);
					return EXIT_FAILURE;
				}
				// get file length
				fseek(file, 0, SEEK_END);
				file_length = ftell(file);
				rest_length = file_length; // = Update_size_left_tx in comms
				fseek(file, 0, SEEK_SET);
				packets_to_send = ceil((double) file_length / (double) BLOCKSIZE);
				printf("Start sending firmware %s within %d packets to %s station...\n", filename, packets_to_send, destSU==true?"local":"base");
				// send start packet:
				//TODO: pack destinations-list into start packet
				data_packet = create_start_packet(sector_base, 1, destination, file_length, modeIdListTypes, num_of_stations, &destinations[0]);
				//print_packet(&data_packet);
				Sent_flag = send_packet(&data_packet, FW_START);
				if (Sent_flag == 1) {
					printf("Mission impossible! Could not send start packet.\n");
					return EXIT_FAILURE;
				}
				printf("Start packet sent.\n");
				// send firmware file:
				printf("Data packets:");
				packet_no = 0;
				packets_sent = 1;
				State = FW_STARTED;
				/****************/
				//Workaround: timing problem -> start packet is lost!
				//add wait time between start and fraction 0 helps.
				sleep(1);
				/****************/
			} else {
				if (isReconfig) { // reconfiguration only
					printf("Send reconfiguration command to %s station...\n", destSU==true?"local":"base");
					data_packet = create_start_packet(sector_base, 1, destination, 0, modeIdListTypes, num_of_stations, &destinations[0]);
					Sent_flag = send_packet(&data_packet, FW_START);
					if (Sent_flag == 1) {
						printf("Mission impossible! Could not send start packet.\n");
						return EXIT_FAILURE;
					}
				}
				State = FW_STOPPED;
			}
		}
		if (State == FW_STARTED) {
			if (rest_length > 0) {
				fread(&buffer[0], BLOCKSIZE, 1, file);
				service = packet_no%2; // alternating 0 and 1
				if (rest_length > BLOCKSIZE) {
					file_crc = chksum_crc32_cont(file_crc, &buffer[0], BLOCKSIZE, false);
					rest_length = rest_length - BLOCKSIZE;
				} else {
					file_crc = chksum_crc32_cont(file_crc, &buffer[0], rest_length, false); // for the last packet
					rest_length = 0;
				}
				// create data packet
				pthread_mutex_lock(&data_mutex); // will be released, when the new thread saved the data in his own data structure
				data_packet = create_data_packet(service, destination, packet_no, &buffer[0], BLOCKSIZE);
				data.data_packet = &data_packet;
				data.pkt_type = FW_FRACTION;
				// try to send data packet
				//printf("CRC of firmware file for current fraction %d: %d, Hex: 0x%x\n", packet_no, (unsigned int) file_crc, (unsigned int) file_crc);
				if (packet_no % 5 == 0) printf(" ");
				if (packet_no % 50 == 0) printf("\n%4d: ", packet_no+1);
				if (pthread_create(&th[th_counter], NULL, &send_packet_th, &data) != 0) {
					printf("Could not create thread to send packet.\n");
					return EXIT_FAILURE;
				}
				printf(".");
				th_counter++;
				packet_no++;
				packets_sent++;
				if (th_counter >= MAX_THREADS) {
					for (j = 0; j < th_counter; j++) {
						pthread_join(th[j], (void**)&ret[j]);
						if (ret[j] == 1) {
							printf("Firmware upload failed! Could not send packet in thread %d\n", j);
							return EXIT_FAILURE;
						}
					}
					th_counter = 0;
				}
				/*Sent_flag = send_packet(&data_packet, FW_FRACTION);
				if (Sent_flag == 1) {
					printf("Firmware upload failed! Could not send packet no %d\n", packet_no);
					return EXIT_FAILURE;
				}*/
			} else {
				for (j = 0; j < th_counter; j++) {
					pthread_join(th[j], (void**)&ret[j]);
					if (ret[j] == 1) {
						printf("Firmware upload failed! Could not send packet in thread %d\n", j);
						return EXIT_FAILURE;
					}
				}
				State = FW_STOPPED;
			}
		}
		if (State == FW_STOPPED) {
			printf("\n");
			if (filename != NULL) {
				fclose(file);
			}
			// send stop packet:
			file_crc = chksum_crc32_cont(file_crc, &buffer[0], rest_length, true);
			service = packet_no%2;
			data_packet = create_stop_packet(service, destination, file_crc, isReconfig);
			//print_packet(&data_packet);
			Sent_flag = send_packet(&data_packet, FW_STOP);
			if (Sent_flag == 1) {
				printf("Firmware upload failed! Could not send stop packet.\n");
				return EXIT_FAILURE;
			};
			printf("Stop packet sent.\n");
			packets_sent++;
			State = FW_FINISH;
		}
		if (State == FW_FINISH) {
			// Base station send just one FB_STOP packet
			if ((!destSU) && (num_of_fb_stop_packets >= 1)) {
				wait_fb_stop = false;
			}
			// Wait for FB_STOP packets by counting no of received FB_STOP packets
			if (num_of_fb_stop_packets >= num_of_stations) {
				wait_fb_stop = false;
			}
			// Wait for FB_STOP packets by counting time
			if (max_time_counter >= 1000000) { // timeout
				wait_fb_stop = false;
			}
			max_time_counter++;
		}
		// Receiving packages
		pthread_mutex_lock(&gpio_mutex);
		data_to_read = poll_data_to_read();
		if (data_to_read > 0) {
			packet_read_flag = read_datap(&receive_data_packet);
			if ((packet_read_flag == 0) && (is_firmware_packet(&receive_data_packet))) {
				switch (get_type(&receive_data_packet)) {
				case FW_FB_ERR:
					lsdata = unpack_fb_packet(&receive_data_packet);
					printf("\nERROR! Debug code: %u\n", lsdata.data[FW_DEBUGCODE_OFS]);
					break;
				case FW_FB_STOP:
					num_of_fb_stop_packets++;
					lsdata = unpack_fb_packet(&receive_data_packet);
					su_id = get_SU_ID(&receive_data_packet);
					feedback[su_id].crc = (bool) lsdata.data[FW_FWCRC_OK_OFS];
					feedback[su_id].reconfig = (bool) lsdata.data[FW_RECONFIG_ACK_OFS];
					feedback[su_id].num_fracs_retrans = (alt_u16) conv_u8_arr(&lsdata.data[FW_NUM_FRACS_RETRANS_OFS], FW_NUM_FRACS_RETRANS_LEN);
					crc_ok = (bool) lsdata.data[FW_FWCRC_OK_OFS];
					reconfig_ack = (bool) lsdata.data[FW_RECONFIG_ACK_OFS];
					num_fracs_retrans = (alt_u16) conv_u8_arr(&lsdata.data[FW_NUM_FRACS_RETRANS_OFS], FW_NUM_FRACS_RETRANS_LEN);
					if (destSU) {
						printf("Station %d: ", su_id);
					} else {
						printf("Base station: ");
					}
					printf("crc %s, reconfiguration %s, packets to retransmit %d\n", crc_ok==true?"OK":"corrupt", reconfig_ack==true?"yes":"no", num_fracs_retrans);
					if (num_fracs_retrans > 0) {
						printf("Numbers of packets to retransmit: \n");
						printf("(");
						for (i = 0; i < num_fracs_retrans; i++) {
							printf("%d: %d; ", i, (alt_u16)conv_u8_arr(&lsdata.data[FW_FRACS_RETRANS_OFS + i * FW_CUR_FRACTION_LEN], FW_CUR_FRACTION_LEN));
						}
						printf(")\n");
					}
					break;
				default:
					break;
				};
			}
			packets_received++;
		}
		packet_read_flag = 1;
		pthread_mutex_unlock(&gpio_mutex);
	} // while not FW_FINISH
	// Print statistic from FB_STOP packets
	printf("\n");
	return EXIT_SUCCESS;
}



