// Decode network monitor information contained in the data packet

#ifndef NETMON_H_
#define NETMON_H_

#define packed_data __attribute__((__packed__))

#include "auxiliary.h"

struct double_byte_t {
	unsigned char byte[2];
};

typedef struct double_byte_t double_byte;

// a substructure of datap:
struct network_monitor_data_t {
	unsigned char dnlink_status_nth_slot_succ[8];
	unsigned char dnlink_status_is_crc_err[8];
	unsigned char dnlink_status_num_symerr[8];
	unsigned char ncp_status_nth_slot_succ;
	unsigned char ncp_status_is_crc_err;
	unsigned char ncp_status_num_symerr;
	int su_rssi;
	int su_bg_rssi;
	int recv_delay;
	command_type command_response;
	unsigned char su_status;
	unsigned char su_tx_power;
	misc_info_type misc_info;
	double_byte misc_data;
};

typedef struct network_monitor_data_t network_monitor_data;

// Functions to decode a byte array into network_monitor_data
unsigned char get_nwm_nth_slot_succ(unsigned char *ptr_nwm_data, int index);
unsigned char get_nwm_is_crc_err(unsigned char *ptr_nwm_data, int index);
unsigned char get_nwm_num_symerr(unsigned char *ptr_nwm_data, int index);
unsigned char get_ncp_nwm_nth_slot_succ(unsigned char *ptr_nwm_data);
unsigned char get_ncp_nwm_is_crc_err(unsigned char *ptr_nwm_data);
unsigned char get_ncp_nwm_num_symerr(unsigned char *ptr_nwm_data);
int get_nwm_su_rssi(unsigned char *ptr_nwm_data);
int get_nwm_su_bg_rssi(unsigned char *ptr_nwm_data);
int get_nwm_recv_delay(unsigned char *ptr_nwm_data);
command_type get_nwm_command_response(unsigned char *ptr_nwm_data);
unsigned char get_nwm_su_status(unsigned char *ptr_nwm_data);
misc_info_type get_nwm_misc_info_type(unsigned char *ptr_nwm_data);
double_byte get_nwm_misc_data(unsigned char *ptr_nwm_data);

#endif /* NETMON_H_ */
