/************************************************************/
/* auciliary.c												*/
/* 															*/
/* Conversion routines										*/
/************************************************************/


int convert_uchar2int(unsigned char byte) {
	int nativeInt;
	const int negative = (byte & (1 << 7)) != 0;
	if (negative) nativeInt = byte | ~((1 << 8) - 1);
	else nativeInt = byte;
	return nativeInt;
}

