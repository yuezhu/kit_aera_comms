
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

#include "ncp.h"

#define SCBBUF_NCP_SLOT_NUM 180
#define SCBBUF_NCP_SLOT_NUM_OFS 1564
#define SCBBUF_NCP_HANDSHAKE_OFS 1568
#define BS_TEST_OFS 4095

/************************************************/
/* conversion functions for ncp-packet			*/
/************************************************/

void set_tx_power_low(ncp *ptr_ncp, unsigned char tpl) {
	ptr_ncp->aif[AIF_TXPOWER_LOW_OFS] = tpl;
}

unsigned char get_tx_power_low(ncp *ptr_ncp) {
	return ptr_ncp->aif[AIF_TXPOWER_LOW_OFS];
}

void set_tx_power_medium(ncp *ptr_ncp, unsigned char tpm) {
	ptr_ncp->aif[AIF_TXPOWER_MEDIUM_OFS] = tpm;
}

unsigned char get_tx_power_medium(ncp *ptr_ncp) {
	return ptr_ncp->aif[AIF_TXPOWER_MEDIUM_OFS];
}

void set_tx_power_high(ncp *ptr_ncp, unsigned char tph) {
	ptr_ncp->aif[AIF_TXPOWER_HIGH_OFS] = tph;
}

unsigned char get_tx_power_high(ncp *ptr_ncp) {
	return ptr_ncp->aif[AIF_TXPOWER_HIGH_OFS];
}

void set_tx_power_max(ncp *ptr_ncp, unsigned char tpmx) {
	ptr_ncp->aif[AIF_TXPOWER_MAX_OFS] = tpmx;
}

unsigned char get_tx_power_max(ncp *ptr_ncp) {
	return ptr_ncp->aif[AIF_TXPOWER_MAX_OFS];
}

void set_max_block_length(ncp *ptr_ncp, unsigned int mbl) {
	ptr_ncp->aif[AIF_MAX_BLOCKL_OFS]   = (mbl >> 8) & 0xff; // high byte
	ptr_ncp->aif[AIF_MAX_BLOCKL_OFS+1] = (mbl & 0xff);	   // low byte
}

unsigned int get_max_block_length(ncp *ptr_ncp) {
	unsigned int retval;
	retval = ptr_ncp->aif[AIF_MAX_BLOCKL_OFS] << 8;
	retval = retval + ptr_ncp->aif[AIF_MAX_BLOCKL_OFS+1];
	return retval;
}

void set_future_frequency(ncp *ptr_ncp, unsigned char ff) {
	ptr_ncp->aif[AIF_FUT_FREQU_OFS] = ff;
}

unsigned char get_future_frequency(ncp *ptr_ncp) {
	return ptr_ncp->aif[AIF_FUT_FREQU_OFS];
}

void set_curr_frequency(ncp *ptr_ncp, unsigned char cf) {
	ptr_ncp->aif[AIF_CURR_FREQU_OFS] = cf;
}

unsigned char get_curr_frequency(ncp *ptr_ncp) {
	return ptr_ncp->aif[AIF_CURR_FREQU_OFS];
}

void set_number_of_frames(ncp *ptr_ncp, unsigned char nf) {
	ptr_ncp->aif[AIF_NO_FRAMES_OFS] = nf;
}

unsigned char get_number_of_frames(ncp *ptr_ncp) {
	return ptr_ncp->aif[AIF_NO_FRAMES_OFS];
}

void set_number_NCP_slots(ncp *ptr_ncp, unsigned char nNs) {
	ptr_ncp->aif[AIF_NO_NCP_SLOTS_OFS] = nNs;
}

unsigned char get_number_NCP_slots(ncp *ptr_ncp) {
	return ptr_ncp->aif[AIF_NO_NCP_SLOTS_OFS];
}

void set_no_up_slots(ncp *ptr_ncp, unsigned char nus) {
	ptr_ncp->aif[AIF_NO_UP_SLOTS_OFS] = nus;
}

unsigned char get_no_up_slots(ncp *ptr_ncp) {
	return ptr_ncp->aif[AIF_NO_UP_SLOTS_OFS];
}

void set_no_down_slots(ncp *ptr_ncp, unsigned char nds) {
	ptr_ncp->aif[AIF_NO_DOWN_SLOTS_OFS] = nds;
}

unsigned char get_no_down_slots(ncp *ptr_ncp) {
	return ptr_ncp->aif[AIF_NO_DOWN_SLOTS_OFS];
}

void set_no_arq_slots(ncp *ptr_ncp, unsigned char as) {
	ptr_ncp->aif[AIF_NO_ARQ_SLOTS_OFS] &= ~(0xf << 4); 	// clear old value
	ptr_ncp->aif[AIF_NO_ARQ_SLOTS_OFS] |= as << 4;  	// set bits 7,6,5 and 4
}

unsigned char get_no_arq_slots(ncp *ptr_ncp) {
	return (ptr_ncp->aif[AIF_NO_ARQ_SLOTS_OFS] & 0xf0) >> 4;
}

void set_no_std_download_packets(ncp *ptr_ncp, unsigned char sdp) {
	ptr_ncp->aif[AIF_NO_STD_DPACKETS_OFS] &= ~(0x0f); 		// clear old value
	ptr_ncp->aif[AIF_NO_STD_DPACKETS_OFS] |= sdp & 0x0f;  	// set bits 3,2,1 and 0
}

unsigned char get_no_std_download_packets(ncp *ptr_ncp) {
	return (ptr_ncp->aif[AIF_NO_STD_DPACKETS_OFS] & 0x0f);
}

void set_BS_firmware_version(ncp *ptr_ncp, unsigned int fw) {
	ptr_ncp->aif[AIF_FW_VERSION_OFS]   = (fw >> 8) & 0xff; // high byte
	ptr_ncp->aif[AIF_FW_VERSION_OFS+1] = (fw & 0xff);	   // low byte
}

unsigned int get_BS_firmware_version(ncp *ptr_ncp) {
	unsigned int retval;
	retval = ptr_ncp->aif[AIF_FW_VERSION_OFS] << 8;
	retval = retval + ptr_ncp->aif[AIF_FW_VERSION_OFS+1];
	return retval;
}

int write_ncp(ncp *ptr_ncp) {
	int fh; // file handle
	unsigned char sbcbuf_ncp_handshake = 1;
	unsigned char sbcbuf_slot_num[2];
	int slot_num = SCBBUF_NCP_SLOT_NUM;
	sbcbuf_slot_num[0] = (unsigned char) ((slot_num >> 8) & 0xff);	// high byte
	sbcbuf_slot_num[1] = (unsigned char) (slot_num & 0xff); 		// low byte
	int count = 0;
	
	if ((fh = open("/dev/GPIO-driver", O_RDWR)) == -1) {
		perror("Can't open /dev/GPIO-driver!");
		return EXIT_FAILURE;
	}
	// BS ready to accept new data?
	while ((sbcbuf_ncp_handshake & 0x01) == 1) {
		lseek(fh, SCBBUF_NCP_HANDSHAKE_OFS, SEEK_SET);
		read(fh, &sbcbuf_ncp_handshake, 1);
		count++;
		sleep(1);
		if (count > 400) {
			printf("BS busy!\n");
			return EXIT_FAILURE;
		}
	}
	// write sbcbuf_slot_num (const. 180) to BS
	lseek(fh, SCBBUF_NCP_SLOT_NUM_OFS, SEEK_SET);
	write(fh, &sbcbuf_slot_num, 2);
	// send packet to BS
	lseek(fh, 0, SEEK_SET);
	if ((write(fh, ptr_ncp, sizeof(*ptr_ncp))) == -1) {
		perror("Error writing NCP to BS!");
		return EXIT_FAILURE;
	}

	sbcbuf_ncp_handshake = 1;
	lseek(fh, SCBBUF_NCP_HANDSHAKE_OFS, SEEK_SET);
	write(fh, &sbcbuf_ncp_handshake, 1);

	// close GPIO-driver
	close(fh);
	return EXIT_SUCCESS;
}

int read_ncp(ncp *ptr_ncp) {
	int fh;
	if ((fh = open("/dev/GPIO-driver", O_RDWR)) == -1) {
		perror("Can't open /dev/GPIO-driver!");
		return EXIT_FAILURE;
	}
	lseek(fh, 0, SEEK_SET);
	read(fh, ptr_ncp, sizeof(*ptr_ncp));
	close(fh);
	return EXIT_SUCCESS;
}

int bs_available() { // returns 1 if BS is available
	int fh;
	int result = 0;
	unsigned char identifier;
	if ((fh = open("/dev/GPIO-driver", O_RDWR)) == -1) {
		perror("Can't open /dev/GPIO-driver!");
		return EXIT_FAILURE;
	}
	lseek(fh, BS_TEST_OFS, SEEK_SET);
	read(fh, &identifier, 1);
	close(fh);
	//printf("bs_available():identifier %d", identifier);
	if (identifier == 0x55) result = 1;
	return result;
}

int bs_needs_NCP() { // returns 1 if BS need a new NCP
	int fh;
	int result = 0;
	unsigned char sbcbuf_ncp_handshake;
	if ((fh = open("/dev/GPIO-driver", O_RDWR)) == -1) {
		perror("Can't open /dev/GPIO-driver!");
		return EXIT_FAILURE;
	}
	lseek(fh, SCBBUF_NCP_HANDSHAKE_OFS, SEEK_SET);
	read(fh, &sbcbuf_ncp_handshake, 1);
	close(fh);
	if ((sbcbuf_ncp_handshake & 0x02)>>1 == 1) result = 1; // test bit1
	return result;
}
