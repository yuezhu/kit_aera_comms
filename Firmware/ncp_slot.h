#ifndef NCP_SLOT_H
#define NCP_SLOT_H

#define packed_data __attribute__((__packed__))

#include "auxiliary.h"

/************************************************/
/* Type and class declarations for ncp-slot		*/
/************************************************/

struct ncp_slot_t {
	unsigned char slot_attribute[2];
	unsigned char station_attribute;
}packed_data;

typedef struct ncp_slot_t ncp_slot; // Class name

// Methods of class ncp_slot
void init_slot(ncp_slot *ptr_ncp_slot);
void set_link_direction(ncp_slot *ptr_ncp_slot, link_direction_t ls);
link_direction_t get_link_direction(ncp_slot *ptr_ncp_slot);
void set_sub_packet_no(ncp_slot *ptr_ncp_slot, unsigned char sp_no);
unsigned char get_sub_packet_no(ncp_slot *ptr_ncp_slot);
void set_SUorBS_ID(ncp_slot *ptr_ncp_slot, unsigned int id);
unsigned int get_SUorBS_ID(ncp_slot *ptr_ncp_slot);
void set_link_status(ncp_slot *ptr_ncp_slot, link_status_t ls);
link_status_t get_link_status(ncp_slot *ptr_ncp_slot);
void set_transmission_delay(ncp_slot *ptr_ncp_slot, unsigned char td);
unsigned char get_transmission_delay(ncp_slot *ptr_ncp_slot);
void set_transmission_power(ncp_slot *ptr_ncp_slot, unsigned char tp);
unsigned char get_transmission_power(ncp_slot *ptr_ncp_slot);

#endif
