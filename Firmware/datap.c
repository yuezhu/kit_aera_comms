#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <assert.h>

#include "datap.h"
#include "crc32.h"
#include "netmon.h"

/************************************************/
/* conversion functions for data packet			*/
/************************************************/

void set_dp_sub_packet_no(datap *ptr_datap, unsigned char sp_no) {
	ptr_datap->onair_id[SUB_PACKET_NO_OFS] &= ~(0x7 << 4); 	// clear old value
	ptr_datap->onair_id[SUB_PACKET_NO_OFS] |= sp_no << 4;  	// set bits 6,5 and 4
}

unsigned char get_dp_sub_packet_no(datap *ptr_datap) {
	return (ptr_datap->onair_id[SUB_PACKET_NO_OFS] >> 4) & 0x7; // get bits 6,5 and 4
}

void set_SU_ID(datap *ptr_datap, unsigned int id) {
	ptr_datap->onair_id[SU_ID_OFS] &= ~(0xf);
	ptr_datap->onair_id[SU_ID_OFS] |= (id >> 8) & 0xf; 		// set bits 3,2,1 and 0 with high part of id
	ptr_datap->onair_id[SU_ID_OFS + 1] = id & 0xff;		 	// set lower part if id
}

unsigned int get_SU_ID(datap *ptr_datap) {
	unsigned int retval;
	retval = (ptr_datap->onair_id[SU_ID_OFS] & 0x7) << 8;   // get upper part
	retval = retval + ptr_datap->onair_id[SU_ID_OFS + 1];	// get lower part
	return retval;
}

void set_net_monitor_data(datap *ptr_datap, unsigned char *nmd) {
	int i;
	for (i = 0; i < 20; i++) {
		ptr_datap->nw_monitor[i] = *(nmd + i);
	}
}

/* alt:
void get_net_monitor_data(datap *ptr_datap, unsigned char *buffer) {
	int i;
	for (i = 0; i < 20; i++) {
		*(buffer + i) = ptr_datap->nw_monitor[i];
	}
}
*/

network_monitor_data get_net_monitor_data(datap *ptr_datap) {
	network_monitor_data retval;
	int i;
	unsigned char buffer[20];
	for (i = 0; i < 20; i++) {
		buffer[i] = ptr_datap->nw_monitor[i];
	}
	for (i = 0; i < 8; i++) {
		retval.dnlink_status_nth_slot_succ[i] = get_nwm_nth_slot_succ(&buffer[0], i);
		retval.dnlink_status_is_crc_err[i] = get_nwm_is_crc_err(&buffer[0], i);
		retval.dnlink_status_num_symerr[i] = get_nwm_num_symerr(&buffer[0], i);
	}
	retval.ncp_status_nth_slot_succ = get_ncp_nwm_nth_slot_succ(&buffer[0]);
	retval.ncp_status_is_crc_err = get_ncp_nwm_is_crc_err(&buffer[0]);
	retval.ncp_status_num_symerr = get_ncp_nwm_num_symerr(&buffer[0]);
	retval.su_rssi =  get_nwm_su_rssi(&buffer[0]);
	retval.su_bg_rssi = get_nwm_su_bg_rssi(&buffer[0]);
	retval.recv_delay = get_nwm_recv_delay(&buffer[0]);
	retval.command_response = get_nwm_command_response(&buffer[0]);
	retval.su_status = get_nwm_su_status(&buffer[0]);
	retval.misc_info = get_nwm_misc_info_type(&buffer[0]);
	retval.misc_data = get_nwm_misc_data(&buffer[0]);
	//printf("misc_info: %i byte0: 0x%x byte1: 0x%x \n",retval.misc_info, retval.misc_data.byte[0], retval.misc_data.byte[1]);
	return retval;
}

void set_packet_id(datap *ptr_datap, unsigned char pid) {
	ptr_datap->packet_id &= ~(0xf); 		// clear old value
	ptr_datap->packet_id |= (pid & 0xf);  	// set bits 3,2,1 and 0
}

unsigned char get_packet_id(datap *ptr_datap) {
	 return (ptr_datap->packet_id & 0x0f);
}

void set_packet_id_runnung_no(datap *ptr_datap, unsigned char pidr) {
	ptr_datap->packet_id &= ~(0xf << 4); 		// clear old value
	ptr_datap->packet_id |= (pidr & 0xf) << 4;  // set bits 7,6,5 and 4
}

unsigned char get_packet_id_running_no(datap *ptr_datap) {
	 return (ptr_datap->packet_id >> 4) & 0x0f;
}

void set_ls_data_length(datap *ptr_datap, unsigned int plength) {
	ptr_datap->ls_data_length[0] |= (plength >> 8) & 0xff; 	// high byte
	ptr_datap->ls_data_length[1] = plength & 0xff;			// low byte
	//printf("Set: %u -> %x %x \n", plength, ptr_datap->ls_data_length[0], ptr_datap->ls_data_length[1]);
}

int get_ls_data_length(datap *ptr_datap) {
	int plength;
	plength = ((unsigned int) ptr_datap->ls_data_length[0] << 8); 			// high byte
	plength = plength + (unsigned int) ptr_datap->ls_data_length[1];		// low byte
	//printf("Get: %x %x -> %u \n", ptr_datap->ls_data_length[0], ptr_datap->ls_data_length[1], plength);
	if (plength > 540) plength = -plength;
	return plength;
}

void set_ls_data(datap *ptr_datap, unsigned char *lsd) {
	int i;
	unsigned int ls_data_length;
	ls_data_length = get_ls_data_length(ptr_datap);
	assert(ls_data_length <= 540);
	/*ls_data_length = sizeof(lsd);
	if (ls_data_length > 540) ls_data_length = 540;
	set_ls_data_length(ptr_datap, ls_data_length); */
	for (i = 0; i < ls_data_length; i++) {
		ptr_datap->ls_data[i] = *(lsd + i);
	}
}

void get_ls_data(datap *ptr_datap,  unsigned char *buffer) {
	int i;
	unsigned int ls_data_length;
	ls_data_length = get_ls_data_length(ptr_datap);
	//TEST START
	ls_data_length = 540;
	//TEST END
	assert(ls_data_length <= 540);
	for (i = 0; i < ls_data_length; i++) { 
		*(buffer + i) = ptr_datap->ls_data[i];
	}
}

/*two additional funktions to load a timestamp into the first 4 bytes of ls_data for performance measures*/
void set_timestamp(datap *ptr_datap, clock_t ts) {
	int i;
	for (i = 0; i < 4; i++) { // 4 bytes for datatype unsigned long int
			ptr_datap->ls_data[i] = ((unsigned long int)ts >> 8*i) & 0xff;
	}
}

clock_t get_timestamp(datap *ptr_datap) {
	unsigned long int int_ts = 0;
	int i;
	for (i = 0; i < 4; i++) { // 4 bytes for datatype unsigned integer
		int_ts = int_ts + ((unsigned long int)(ptr_datap->ls_data[i] & 0xff) << 8*i);
	}
	return (clock_t)int_ts;
}

void set_crc1(datap *ptr_datap) {
	int i;
	unsigned int ls_data_length;
	unsigned long crc;
	unsigned char *ptr_packet;
	ls_data_length = get_ls_data_length(ptr_datap);
	//assert(ls_data_length <= 540);
	if (ls_data_length > 540) ls_data_length = 540;
	ptr_packet = (unsigned char*) ptr_datap;
	ptr_packet = ptr_packet + PACKET_ID_OFS;
	chksum_crc32gentab();
	crc = chksum_crc32(ptr_packet, ls_data_length + 3); // packet_id, ls_data_length[2], ls_data[0..540]
	for (i = 0; i < 4; i++) { 
		// highest byte in crc2[0] ... lowesrt byte in crc2[3]
		ptr_datap->ls_data[ls_data_length + i] = (unsigned char) ((crc >> (3-i)*8) & 0xff);
	}
}

unsigned long get_crc1(datap *ptr_datap) {
	int i;
	unsigned int ls_data_length;
	ls_data_length = get_ls_data_length(ptr_datap);
	if (ls_data_length > 540) ls_data_length = 540;
	unsigned long crc = 0;
	for (i = 0; i < 4; i++) { 
		// highest byte from ptr_datap->crc2[0] ... lowesrt byte from ptr_datap->crc2[3]
		crc = crc + (unsigned long) (ptr_datap->ls_data[ls_data_length + i] << (3-i)*8);
	}
	return crc;
}

unsigned long check_crc1(datap *ptr_datap) {
	unsigned int ls_data_length;
	unsigned long crc;
	unsigned char *ptr_packet;
	ls_data_length = get_ls_data_length(ptr_datap);
	assert(ls_data_length <= 540);
	ptr_packet = (unsigned char*) ptr_datap;
	ptr_packet = ptr_packet + PACKET_ID_OFS;
	chksum_crc32gentab();
	crc = chksum_crc32(ptr_packet, ls_data_length + 3); // packet_id, ls_data_length[2], ls_data[0..540]
	return crc;
}

void set_crc2(datap *ptr_datap) {
	int i;
	unsigned long crc;
	unsigned char *ptr_packet;
	ptr_packet = (unsigned char*) ptr_datap;
	chksum_crc32gentab();
	crc = chksum_crc32(ptr_packet, CRC2_OFS-1);
	for (i = 0; i < 4; i++) { 
		// highest byte in crc2[0] ... lowesrt byte in crc2[3]
		ptr_datap->crc2[i] = (unsigned char) ((crc >> (3-i)*8) & 0xff);
	}
}

unsigned long get_crc2(datap *ptr_datap) {
	int i;
	unsigned long crc = 0;
	for (i = 0; i < 4; i++) { 
		// highest byte from ptr_datap->crc2[0] ... lowesrt byte from ptr_datap->crc2[3]
		crc = crc + (unsigned long) (ptr_datap->crc2[i] << (3-i)*8);
	}
	return crc;
}

unsigned long check_crc2(datap *ptr_datap) {
	unsigned long crc;
	unsigned char *ptr_packet;
	ptr_packet = (unsigned char*) ptr_datap;
	chksum_crc32gentab();
	crc = chksum_crc32(ptr_packet, CRC2_OFS-1);
	return crc;
}

// two additional funktions helping to identify packets
void set_packet_no(datap *ptr_datap, unsigned char pnr) {
	ptr_datap->ls_data[8] &= ~(0xff); 		// clear old value
	ptr_datap->ls_data[8] |= (pnr & 0xff);  // set bits
}

unsigned char get_packet_no(datap *ptr_datap) {
	 return ptr_datap->ls_data[8];
}

int get_datap_slot_count(timing *ptr_timing) {
	int slot_count = 0;
	slot_count = ((unsigned int) (ptr_timing->slot_count[0]) << 8); 			// high byte, byte1 MSB
	slot_count = slot_count + (unsigned int) (ptr_timing->slot_count[1]);		// low byte, byte0 LSB
	return slot_count;
}

int get_datap_frame_count(timing *ptr_timing) {
	int frame_count;
	frame_count = (int) ptr_timing->frame_count;
	return frame_count;
}

int try_write_datap(datap *ptr_datap, int pack_id) { // UP: SBC -> BS
	int fh; // file handle
	unsigned char bit[8] = {0x01, 0x02, 0x04, 0x08, 0x0f, 0x10, 0x20, 0x40};
	unsigned char daru_data_handshake;  // DARU_status
	unsigned char rtpacket_handshake; 	// Tx_buffer_status
	unsigned char rtpacket_open; 		// Tx_slot_status
	daru_buffer_control buffer_control;
	unsigned char packet_id = (unsigned char) (pack_id & 0x07);
	// open GPIO-driver
	if ((fh = open("/dev/GPIO-driver", O_RDWR)) == -1) {
		perror("Can't open /dev/GPIO-driver!");
		return EXIT_FAILURE;
	}
	lseek(fh, DARU_DATA_HANDSHAKE_OFS, SEEK_SET);
	read(fh, &buffer_control, 3);
	daru_data_handshake = buffer_control.darustatus;
	rtpacket_handshake = buffer_control.bufferstatus;
	rtpacket_open = buffer_control.slotstatus;
	// BS ready to accept new data?
//printf("Handshake: DARU-DATA-HANDSHAKE, bit 0 ...\n");
	if ((daru_data_handshake & bit[0]) == 1) {
		close(fh);
		return EXIT_FAILURE;
	}
	if (packet_id < 4) {  // buffer 0 - 3 for realtime packet available ?
//printf("Realtime buffer %d full?\n", packet_id);
		if (((rtpacket_handshake & bit[packet_id]) == 1) || ((rtpacket_open & bit[packet_id]) == 0)) {
			close(fh);
			return EXIT_FAILURE;
		}
	}
	if (packet_id > 3) { // ringbuffer for realtime packet available ?
		rtpacket_handshake = (rtpacket_handshake >> 4) & 0x0f; // mask bits 7 - 4
		rtpacket_open = (rtpacket_open >> 5) & 0x07; 	// mask bits 7 - 5
//printf("Ringbuffer full? \n");
		if ((rtpacket_handshake == 0) || (rtpacket_open == 0)) {
			close(fh);
			return EXIT_FAILURE;
		}
	}
	// write daru_data_handshake to BS to indicate the begin of packet transfer
	daru_data_handshake |= 0x02; 	// set bit 1 
	lseek(fh, DARU_DATA_HANDSHAKE_OFS, SEEK_SET);
	write(fh, &daru_data_handshake, 1);
	// send data packet to BS
	lseek(fh, DATA_ASSEMBLY_RAM_UP_OFS, SEEK_SET);
	write(fh, ptr_datap, sizeof(*ptr_datap));
	// write daru_data_handshake to BS to indicate the end of packet transfer
	daru_data_handshake |= 0x01; 	// set bit 0 
	lseek(fh, DARU_DATA_HANDSHAKE_OFS, SEEK_SET);
	write(fh, &daru_data_handshake, 1);
	/*
printf("Datenpaket wurde �bertragen. Teste Erfolg ...\n");
	// test success of transmission
	count = 0;
	while ((daru_data_handshake & 0x01) == 0x01) { // bit 1 indicates that BS has not finished accepting the packet
		lseek(fh, DARU_DATA_HANDSHAKE_OFS, SEEK_SET);
		read(fh, &daru_data_handshake, 1);
		count++;
		usleep(1);
		if (count > 400) {
			printf("BS did not process the data packet!\n");
			close(fh);
			return EXIT_FAILURE;
		}
	};
	if ((daru_data_handshake & 0x04) == 0x04) { // bit 2 indicates a lost packet due to a timing error
		perror("Packet lost due to timing error!\n");
		close(fh);
		return EXIT_FAILURE;
	};
	if ((daru_data_handshake & 0x08) == 0x08) { // bit 3 indicates frame timing problem
		perror("Frame timing problem occurred!\n");
		close(fh);
		return EXIT_FAILURE;
	};
	*/
	// close GPIO-driver
	close(fh);
	return EXIT_SUCCESS;
}

int write_datap(datap *ptr_datap, int pack_id) { // UP: SBC -> BS
	int fh; // file handle
	unsigned char bit[8] = {0x01, 0x02, 0x04, 0x08, 0x0f, 0x10, 0x20, 0x40};
	unsigned char daru_data_handshake;  // DARU_status
	unsigned char rtpacket_handshake; 	// Tx_buffer_status
	unsigned char rtpacket_open; 		// Tx_slot_status
	daru_buffer_control buffer_control;
	unsigned char packet_id = (unsigned char) (pack_id & 0x07);
	//unsigned char daru_data_handshake = 0x01; // bit 0
	//unsigned char rtpacket0_handshake = 0x02; // bit 1
	//unsigned char rtpacket1_handshake = 0x08; // bit 3
	int count = 0;
	// open GPIO-driver
	if ((fh = open("/dev/GPIO-driver", O_RDONLY)) == -1) {
		perror("Can't open /dev/GPIO-driver!");
		return EXIT_FAILURE;
	}
	lseek(fh, DARU_DATA_HANDSHAKE_OFS, SEEK_SET);
	read(fh, &buffer_control, 3);
	daru_data_handshake = buffer_control.darustatus;
	rtpacket_handshake = buffer_control.bufferstatus;
	rtpacket_open = buffer_control.slotstatus;
	// BS ready to accept new data?
//printf("Handshake: DARU-DATA-HANDSHAKE, bit 0 ...\n");
	while ((daru_data_handshake & bit[0]) == 1) { // BS not ready to accept datap
		lseek(fh, DARU_DATA_HANDSHAKE_OFS, SEEK_SET);
		read(fh, &daru_data_handshake, 1);
		count++;
		usleep(1);
		if (count > 400) {
			printf("BS busy!\n");
			close(fh);
			return EXIT_FAILURE;
		}
	}
	if (packet_id < 4) {  // bufferX for realtime packet X available ?
printf("Handshake: TX-BUFFER-HANDSHAKE, bit 1 and 0 ...\n");
		count = 0;
		while (((rtpacket_handshake & bit[packet_id]) == 1) || ((rtpacket_open & bit[packet_id]) == 0)) { // test bit X
			lseek(fh, DARU_DATA_HANDSHAKE_OFS, SEEK_SET);
			read(fh, &buffer_control, 3);
			daru_data_handshake = buffer_control.darustatus;
			rtpacket_handshake = buffer_control.bufferstatus;
			rtpacket_open = buffer_control.slotstatus;
			count++;
			usleep(1);
			if (count > 400) {
				printf("Real time buffer %d of BS busy!\n", packet_id);
				close(fh);
				return EXIT_FAILURE;
			}
		}
	}
	if (packet_id > 3) { // ringbuffer for packet available ?
		rtpacket_handshake = (rtpacket_handshake >> 4) & 0x0f; // mask bits 7 - 4
		rtpacket_open = (rtpacket_open >> 5) & 0x07; 	// mask bits 7 - 5
printf("Handshake: TX-BUFFER-HANDSHAKE, bit 2 and 3 ...\n");
		count = 0;
		while ((rtpacket_handshake == 0) || (rtpacket_open == 0)) {
			lseek(fh, DARU_DATA_HANDSHAKE_OFS, SEEK_SET);
			read(fh, &buffer_control, 3);
			daru_data_handshake = buffer_control.darustatus;
			rtpacket_handshake = (buffer_control.bufferstatus >> 4) & 0x0f;
			rtpacket_open = (buffer_control.slotstatus >> 5) & 0x07;
			count++;
			usleep(1);
			if (count > 400) {
				printf("Ringbuffer of BS busy!\n");
				close(fh);
				return EXIT_FAILURE;
			}
		}
	}
	// write daru_data_handshake to BS to indicate the begin of packet transfer
	daru_data_handshake |= 0x02; 	// set bit 1 
	lseek(fh, DARU_DATA_HANDSHAKE_OFS, SEEK_SET);
	write(fh, &daru_data_handshake, 1);
	// send data packet to BS
	lseek(fh, DATA_ASSEMBLY_RAM_UP_OFS, SEEK_SET);
	write(fh, ptr_datap, sizeof(*ptr_datap));
	// write daru_data_handshake to BS to indicate the end of packet transfer
	daru_data_handshake |= 0x01; 	// set bit 0 
	lseek(fh, DARU_DATA_HANDSHAKE_OFS, SEEK_SET);
	write(fh, &daru_data_handshake, 1);
printf("Datenpaket wurde �bertragen. Teste Erfolg ...\n");
	// test success of transmission
	count = 0;
	while ((daru_data_handshake & 0x01) == 0x01) { // bit 1 indicates that BS has not finished accepting the packet
		lseek(fh, DARU_DATA_HANDSHAKE_OFS, SEEK_SET);
		read(fh, &daru_data_handshake, 1);
		count++;
		usleep(1);
		if (count > 400) {
			printf("BS did not process the data packet!\n");
			close(fh);
			return EXIT_FAILURE;
		}
	};
	if ((daru_data_handshake & 0x04) == 0x04) { // bit 2 indicates a lost packet due to a timing error
		perror("Packet lost due to timing error!\n");
		close(fh);
		return EXIT_FAILURE;
	};
	if ((daru_data_handshake & 0x08) == 0x08) { // bit 3 indicates frame timing problem
		perror("Frame timing problem occurred!\n");
		close(fh);
		return EXIT_FAILURE;
	};
	// close GPIO-driver
	close(fh);
	return EXIT_SUCCESS;
}

unsigned char poll_data_to_read() {  // returns 1 when a data packet is available
	unsigned char result = 0;
	unsigned char dard_data_handshake;
	int fh;
	if ((fh = open("/dev/GPIO-driver", O_RDWR)) == -1) {
		perror("Can't open /dev/GPIO-driver!");
		return EXIT_FAILURE;
	};
	lseek(fh, DARD_DATA_HANDSHAKE_OFS, SEEK_SET);
	read(fh, &dard_data_handshake, 1);
	if ((dard_data_handshake & 0x01) == 0x01) { // bit 0 indicates data available when set
		result = 1;
	};
	close(fh);
	return result;
}

int read_datap_timing(timing *ptr_timing) {
	int fh;
	// open driver
	if ((fh = open("/dev/GPIO-driver", O_RDWR)) == -1) {
		perror("Can't open /dev/GPIO-driver!");
		return EXIT_FAILURE;
	};
	// read packet from BS
	lseek(fh, DARD_DATA_TIMING_OFS, SEEK_SET);
	read(fh, ptr_timing, sizeof(*ptr_timing));
	// close driver
	close(fh);
	return EXIT_SUCCESS;
}

int read_datap(datap *ptr_datap) { // DOWN: BS -> SBC
	int fh;
	unsigned char dard_data_handshake;
	// open driver
	if ((fh = open("/dev/GPIO-driver", O_RDWR)) == -1) {
		perror("Can't open /dev/GPIO-driver!");
		return EXIT_FAILURE;
	};
	// read packet from BS
	lseek(fh, DATA_ASSEMBLY_RAM_DOWN_OFS, SEEK_SET);
	read(fh, ptr_datap, sizeof(*ptr_datap));
	// indicate read finished to BS
	dard_data_handshake = 0;
	lseek(fh, DARD_DATA_HANDSHAKE_OFS, SEEK_SET);
	write(fh, &dard_data_handshake, 1);
	// close driver
	close(fh);
	return EXIT_SUCCESS;
}
