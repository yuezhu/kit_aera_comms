#ifndef DATAP_H
#define DATAP_H

#include <time.h>
#include "netmon.h"

#define packed_data __attribute__((__packed__))

#define SUB_PACKET_NO_OFS 0
#define SU_ID_OFS 0
#define NET_MONITOR_DATA_OFS 2
#define PACKET_ID_OFS 22
#define LS_DATA_LENGTH_OFS 23
#define LS_DATA_OFS 25
#define CRC2_OFS 569
#define DATA_ASSEMBLY_RAM_UP_OFS 8192
#define DARU_DATA_HANDSHAKE_OFS 9212 // 8192 + 1020
#define DATA_ASSEMBLY_RAM_DOWN_OFS 12288
#define DARD_DATA_HANDSHAKE_OFS 13308 // 12288 + 1020
#define DARD_DATA_TIMING_OFS 13309 // 1288 + 1020 + 1


/************************************************/
/* Type and class declarations for data-packet	*/
/************************************************/

struct daru_buffer_control_t {
	unsigned char darustatus;
	unsigned char bufferstatus;
	unsigned char slotstatus;
}packed_data;

typedef struct daru_buffer_control_t daru_buffer_control;

struct timing_t {
	unsigned char frame_count;
	unsigned char slot_count[2];
}packed_data;

typedef struct timing_t timing;

struct datap_t {
	unsigned char onair_id[2];
	unsigned char nw_monitor[20];
	unsigned char packet_id;
	unsigned char ls_data_length[2];
	unsigned char ls_data[544]; // ls_data[0..540], crc1[4]
	unsigned char crc2[4];
}packed_data;	

typedef struct datap_t datap; // Class name

// Methods of class datap
void set_dp_sub_packet_no(datap *ptr_datap, unsigned char sp_no);
unsigned char get_dp_sub_packet_no(datap *ptr_datap);
void set_SU_ID(datap *ptr_datap, unsigned int id);
unsigned int get_SU_ID(datap *ptr_datap);
void set_net_monitor_data(datap *ptr_datap, unsigned char *nmd);
//void get_net_monitor_data(datap *ptr_datap, unsigned char *buffer);
network_monitor_data get_net_monitor_data(datap *ptr_datap);
void set_packet_id(datap *ptr_datap, unsigned char pid);
unsigned char get_packet_id(datap *ptr_datap);
void set_packet_id_runnung_no(datap *ptr_datap, unsigned char pidr);
unsigned char get_packet_id_running_no(datap *ptr_datap);
//void set_ls_packet(datap *ptr_datap, unsigned char *lsp);
//void get_ls_packet(datap *ptr_datap, unsigned char *buffer);
void set_ls_data_length(datap *ptr_datap, unsigned int plength);
int get_ls_data_length(datap *ptr_datap);
void set_ls_data(datap *ptr_datap, unsigned char *lsd);
void get_ls_data(datap *ptr_datap, unsigned char *buffer);
void set_timestamp(datap *ptr_datap, clock_t ts);
clock_t get_timestamp(datap *ptr_datap);
void set_crc1(datap *ptr_datap);
unsigned long get_crc1(datap *ptr_datap);
unsigned long check_crc1(datap *ptr_datap);
void set_crc2(datap *ptr_datap);
unsigned long get_crc2(datap *ptr_datap);
unsigned long check_crc2(datap *ptr_datap);
void set_packet_no(datap *ptr_datap, unsigned char pnr);
unsigned char get_packet_no(datap *ptr_datap);
int get_datap_slot_count(timing *ptr_timing);
int get_datap_frame_count(timing *ptr_timing);
int try_write_datap(datap *ptr_datap, int packet_id);
int write_datap(datap *ptr_datap, int packet_id);
unsigned char poll_data_to_read();
int read_datap_timing(timing *ptr_timing);
int read_datap(datap *ptr_datap);

#endif
