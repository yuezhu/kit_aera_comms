/*
 * conversion.h
 *
 *  Created on: Mar 23, 2012
 *      Author: dev
 */

#ifndef CONVERSION_H_
#define CONVERSION_H_

#include "alt_types.h"

__inline__ static alt_u32 conv_u8_arr(alt_u8* dat, alt_u8 len) //BIG ENDIAN
{
    alt_u32 ret = 0;
    int i;
    for (i=0;i<len;i++)
    {
        ret = ret|(alt_u32)(dat[i]<<(8*(len-i-1)));
    }
    return ret;
}

__inline__ static void conv_to_u8_arr(alt_u8* dat, alt_u32 val, alt_u8 len) //BIG ENDIAN
{
    int i;
    for (i=0;i<len;i++)
    {
        dat[i] = (alt_u8)(val>>(8*(len-i-1)));
    }
}

#endif /* CONVERSION_H_ */
