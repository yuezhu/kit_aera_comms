/*
 * epcs.h
 *
 *  Created on: Mar 27, 2012
 *      Author: dev
 */

#ifndef EPCS_H_
#define EPCS_H_

#define SUID_BLK  0
#define CALIB_BLK 1
//note !! here all the non-volatile data should only be programmed once (appended)!
//since the flash can only change 1->0!
//NOTE: Naming convention: "Block" = "Sector"

#define BLK_LEN           65536
#define BLK_LEN_LOG2      16 // (2^^16 = 65536)
#define BLK_NUM           128

#define HW1_OFS           0  //0x000000, region: 2MB
#define HW2_OFS           32 //0x200000, region: 2MB
#define SW1_OFS           64 //0x400000, region: 1MB
#define SW2_OFS           80 //0x500000, region: 1MB
#define FLASHROM_OFS      96 //0x600000, region: 1.875MB
#define USERDATA_OFS      124 //block 124~127 total 256Kbytes are reserved as user data!!!!! should not be overlap with firmware

#endif /* EPCS_H_ */
