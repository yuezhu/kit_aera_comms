#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "auxiliary.h"
#include "netmon.h"

#define DNLINK_STATUS_OFS 0
#define NCP_STATUS_OFS 8
#define SU_RSSI_OFS 9
#define SU_BG_RSSI_OFS 10
#define RECV_DELAY_OFS 11
#define COMMAND_RESPONSE_OFS 13
#define SU_STATUS_OFS 14
#define SU_TX_POWER_OFS 15
#define MISC_INFO_OFS 16
#define MISC_DATA_OFS 17

/********************************************************/
/* conversion functions for network monitor information	*/
/********************************************************/

unsigned char get_nwm_nth_slot_succ(unsigned char *ptr_nwm_data, int index) {
	unsigned char retval;
	retval = ptr_nwm_data[DNLINK_STATUS_OFS + index] & 0x07; // bit0, 1, 2
	return retval;
}

unsigned char get_nwm_is_crc_err(unsigned char  *ptr_nwm_data, int index) {
	unsigned char retval;
	retval = (ptr_nwm_data[DNLINK_STATUS_OFS + index] >> 3) & 0x07; // bit3, 4, 5
	return retval;
}

unsigned char get_nwm_num_symerr(unsigned char  *ptr_nwm_data, int index) {
	unsigned char retval;
	retval = (ptr_nwm_data[DNLINK_STATUS_OFS + index] >> 6) & 0x03; // bit6, 7
	return retval;
}

unsigned char get_ncp_nwm_nth_slot_succ(unsigned char  *ptr_nwm_data) {
	unsigned char retval;
	retval = ptr_nwm_data[NCP_STATUS_OFS] & 0x07; // bit0, 1, 2
	return retval;
}

unsigned char get_ncp_nwm_is_crc_err(unsigned char  *ptr_nwm_data) {
	unsigned char retval;
	retval = (ptr_nwm_data[NCP_STATUS_OFS] >> 3) & 0x07; // bit3, 4, 5
	return retval;
}

unsigned char get_ncp_nwm_num_symerr(unsigned char  *ptr_nwm_data) {
	unsigned char retval;
	retval = (ptr_nwm_data[NCP_STATUS_OFS] >> 6) & 0x03; // bit6, 7
	return retval;
}

int get_nwm_su_rssi(unsigned char  *ptr_nwm_data) {
	return convert_uchar2int(ptr_nwm_data[SU_RSSI_OFS]);
}

int get_nwm_su_bg_rssi(unsigned char  *ptr_nwm_data) {
	return convert_uchar2int(ptr_nwm_data[SU_BG_RSSI_OFS]);
}

int get_nwm_recv_delay(unsigned char  *ptr_nwm_data) {
	int retval;
	retval = ((unsigned int) ptr_nwm_data[RECV_DELAY_OFS] << 8); 			// high byte
	retval = retval + (unsigned int) ptr_nwm_data[RECV_DELAY_OFS + 1];		// low byte
	return retval;
}

command_type get_nwm_command_response(unsigned char  *ptr_nwm_data) {
	command_type retval;
	switch(ptr_nwm_data[COMMAND_RESPONSE_OFS]) {
		case 0:
			retval = RESET;
		break;
		case 1:
			retval = LOOPBACK;
		break;
	}
	return retval;
}

unsigned char get_nwm_su_status(unsigned char  *ptr_nwm_data) {
	return ptr_nwm_data[SU_STATUS_OFS];
}

int get_su_tx_power(unsigned char *ptr_nwm_data) {
	return convert_uchar2int(ptr_nwm_data[SU_TX_POWER_OFS]);
}

misc_info_type get_nwm_misc_info_type(unsigned char  *ptr_nwm_data) {
	misc_info_type retval;
	switch (ptr_nwm_data[MISC_INFO_OFS]) {
		case 0:
			retval = NONE;
		break;
		case 1:
			retval = FIRMWARE_VER;
		break;
		case 2:
			retval = SOFT_VER;
		break;
		case 3:
			retval = RF_TEMPERATURE;
		break;
		case 4:
			retval = RX_LNAVGA_SETTINGS;
		break;
	}
	return retval;
}

double_byte get_nwm_misc_data(unsigned char *ptr_nwm_data) {
	double_byte retval;
	retval.byte[0] = ptr_nwm_data[MISC_DATA_OFS];
	retval.byte[1] = ptr_nwm_data[MISC_DATA_OFS + 1];
	return retval;
}

