/*
 * conversion.h
 *
 *  Created on: Nov 10, 2011
 *      Author: dev
 */

#ifndef AUXILIARY_H_
#define AUXILIARY_H_

typedef enum {up, down} link_direction_t;
typedef enum {awaiting_connection, established, broken, unused} link_status_t;

typedef enum {NO_CMD = 0, CMD_FOR_SU = 1, CMD_FOR_BS = 2} command_dest;
typedef enum {NOP = 0, RESET = 1, LOOPBACK = 2} command_type;

typedef enum {NONE = 0, FIRMWARE_VER = 1, SOFT_VER = 2, RF_TEMPERATURE = 3, RX_LNAVGA_SETTINGS = 4} misc_info_type;

typedef enum {UNKNOWN = 0, CORRECT_WITH_NO_FEC = 1,  CRC_ERR = 2, PREAMBLE_ERR = 3, CORRECT_WITH_FEC = 4} recv_stat;
typedef enum {NO_LOOPBACK = 0, LOOPBACK_ONAIR = 1, LOOPBACK_LOCAL = 2, SOURCE_LOCAL = 3, SOURCE_ONAIR = 4} loopback_mode;

int convert_uchar2int(unsigned char byte);

#endif /* AUXILIARY_H_ */
