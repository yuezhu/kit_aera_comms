#include "ncp_slot.h"
#include "auxiliary.h"
#include <stdio.h>

/************************************************/
/* conversion functions for ncp-slot			*/
/************************************************/

void init_slot(ncp_slot *ptr_ncp_slot) {
	ptr_ncp_slot->slot_attribute[0] = 0;
	ptr_ncp_slot->slot_attribute[1] = 0;
	ptr_ncp_slot->station_attribute = 0;
}

void set_link_direction(ncp_slot *ptr_ncp_slot, link_direction_t ld) {
	if (ld == up) {
		ptr_ncp_slot->slot_attribute[0] &= ~(1 << 7); // clear bit in highest position
	} else { // (ls == down)
		ptr_ncp_slot->slot_attribute[0] |= (1 << 7);  // set bit in highest position
	}
}

link_direction_t get_link_direction(ncp_slot *ptr_ncp_slot) {
	link_direction_t retval;
	// test bit in highest position:
	if ((ptr_ncp_slot->slot_attribute[0] & (1 << 7)) == 0) retval = up;
	else retval = down;
	return retval;
}

void set_sub_packet_no(ncp_slot *ptr_ncp_slot, unsigned char sp_no) {
	ptr_ncp_slot->slot_attribute[0] &= ~(0x7 << 4); // clear old value
	ptr_ncp_slot->slot_attribute[0] |= sp_no << 4;  // set bits 6,5 and 4
}

unsigned char get_sub_packet_no(ncp_slot *ptr_ncp_slot) {
	return (ptr_ncp_slot->slot_attribute[0] >> 4) & 0x7; // get bits 6,5 and 4
}

void set_SUorBS_ID(ncp_slot *ptr_ncp_slot, unsigned int id) {
	ptr_ncp_slot->slot_attribute[0] &= ~(0xf);
	ptr_ncp_slot->slot_attribute[0] |= (id >> 8) & 0xf; // set bits 3,2,1 and 0 with high part of id
	ptr_ncp_slot->slot_attribute[1] = id & 0xff;		 // set lower part if id
}

unsigned int get_SUorBS_ID(ncp_slot *ptr_ncp_slot) {
	unsigned int retval;
	retval = (ptr_ncp_slot->slot_attribute[0] & 0x7) << 8;  // get upper part
	retval = retval + ptr_ncp_slot->slot_attribute[1];		 // get lower part
	return retval;
}

void set_link_status(ncp_slot *ptr_ncp_slot, link_status_t ls) {
	ptr_ncp_slot->station_attribute &= ~(0x3 << 6); // clear bits 7 and 6
	switch (ls) {
		case awaiting_connection:
			ptr_ncp_slot->station_attribute |= (0 & 0x3) << 6; 
		break;
		case established:
			ptr_ncp_slot->station_attribute |= (1 & 0x3) << 6; 
		break;
		case broken:
			ptr_ncp_slot->station_attribute |= (2 & 0x3) << 6; 
		break;
		case unused:
			ptr_ncp_slot->station_attribute |= (3 & 0x3) << 6; 
	}
}

link_status_t get_link_status(ncp_slot *ptr_ncp_slot) {
	link_status_t retval;
	// test bits 7 and 6:
	switch ((ptr_ncp_slot->station_attribute >> 6) & 0x3) {
		case 0:
			retval = awaiting_connection;
		break;
		case 1:
			retval = established;
		break;
		case 2:
			retval = broken;
		break;
		case 3:
			retval = unused;
	}
	return retval;
}

void set_transmission_delay(ncp_slot *ptr_ncp_slot, unsigned char td) {
	ptr_ncp_slot->station_attribute &= ~(0xf << 2); // clear old value
	ptr_ncp_slot->station_attribute |= (td & 0xf) << 2;  // set bits 5,4,3 and 2
}

unsigned char get_transmission_delay(ncp_slot *ptr_ncp_slot) {
	return (ptr_ncp_slot->station_attribute >> 2) & 0xf; // get bits 5,4,3 and 2
}

void set_transmission_power(ncp_slot *ptr_ncp_slot, unsigned char tp) {
	ptr_ncp_slot->station_attribute &= ~(0x3); // clear old value
	ptr_ncp_slot->station_attribute |= tp & 0x3;  // set bits 1 and 0
}

unsigned char get_transmission_power(ncp_slot *ptr_ncp_slot) {
	return (ptr_ncp_slot->station_attribute & 0x3); // get bits 1 and 0
}

