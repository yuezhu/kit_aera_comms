#ifndef NCP_H
#define NCP_H

#include <fcntl.h>
#include <unistd.h>
#include <time.h>

#include "ncp_slot.h"

#define packed_data __attribute__((__packed__))

#define NO_OF_SLOTS 200

#define NCP_SLOTASM_OFS 27
#define AIF_TXPOWER_LOW_OFS 0
#define AIF_TXPOWER_MEDIUM_OFS 1
#define AIF_TXPOWER_HIGH_OFS 2
#define AIF_TXPOWER_MAX_OFS 3
#define AIF_MAX_BLOCKL_OFS 4
#define AIF_FUT_FREQU_OFS 6
#define AIF_CURR_FREQU_OFS 7
#define AIF_NO_FRAMES_OFS 12
#define AIF_NO_NCP_SLOTS_OFS 13
#define AIF_NO_UP_SLOTS_OFS 14
#define AIF_NO_DOWN_SLOTS_OFS 16
#define AIF_NO_ARQ_SLOTS_OFS 17
#define AIF_NO_STD_DPACKETS_OFS 18
#define AIF_FW_VERSION_OFS 20


/************************************************/
/* Type and class declarations for ncp-packet	*/
/************************************************/

struct ncp_t {
	unsigned char time[4];
	unsigned char aif[23];
	ncp_slot slot[NO_OF_SLOTS]; // NO_OF_SLOT ncp_slot objekts
}packed_data;	

typedef struct ncp_t ncp; // Class name

// Methods of class ncp
void set_tx_power_low(ncp *ptr_ncp, unsigned char tpl);
unsigned char get_tx_power_low(ncp *ptr_ncp);
void set_tx_power_medium(ncp *ptr_ncp, unsigned char tpm);
unsigned char get_tx_power_medium(ncp *ptr_ncp);
void set_tx_power_high(ncp *ptr_ncp, unsigned char tph);
unsigned char get_tx_power_high(ncp *ptr_ncp);
void set_tx_power_max(ncp *ptr_ncp, unsigned char tpmx);
unsigned char get_tx_power_max(ncp *ptr_ncp);
void set_max_block_length(ncp *ptr_ncp, unsigned int mbl);
unsigned int get_max_block_length(ncp *ptr_ncp);
void set_future_frequency(ncp *ptr_ncp, unsigned char ff);
unsigned char get_future_frequency(ncp *ptr_ncp);
void set_curr_frequency(ncp *ptr_ncp, unsigned char cf);
unsigned char get_curr_frequency(ncp *ptr_ncp);
void set_number_of_frames(ncp *ptr_ncp, unsigned char nf);
unsigned char get_number_of_frames(ncp *ptr_ncp);
void set_number_NCP_slots(ncp *ptr_ncp, unsigned char nNs);
unsigned char get_number_NCP_slots(ncp *ptr_ncp);
void set_no_up_slots(ncp *ptr_ncp, unsigned char nus);
unsigned char get_no_up_slots(ncp *ptr_ncp);
void set_no_down_slots(ncp *ptr_ncp, unsigned char nds);
unsigned char get_no_down_slots(ncp *ptr_ncp);
void set_no_arq_slots(ncp *ptr_ncp, unsigned char as);
unsigned char get_no_arq_slots(ncp *ptr_ncp);
void set_no_std_download_packets(ncp *ptr_ncp, unsigned char sdp);
unsigned char get_no_std_download_packets(ncp *ptr_ncp);
void set_BS_firmware_version(ncp *ptr_ncp, unsigned int fw);
unsigned int get_BS_firmware_version(ncp *ptr_ncp);
int write_ncp(ncp *ptr_ncp);
int read_ncp(ncp *ptr_ncp);
int bs_available();
int bs_needs_NCP();

#endif
