#ifdef SOCKLIB

#include <ScType.h>
#include <amsg.h>
#include "./socklib/socklib.h"

#define EVMSGSIZE		3102

int clientSocket_fd = 0;
int clientSocket_fd2 = 0;
int clientSocketMax;
int clientSocket_fd_now;

fd_set clientFDS;
struct timeval sockTimeout = {0,100000}; /* AW: timeout for select */
int readTimeout = 100000;		 /* AW: timeout for read */
int port = 0;
char *server = NULL;
UINT16 sendBuff[65000];
int LsCheckS3DataDone = 1;
int notProcessedT3s = 0;

#else

#include <RcType.h>
#include <RcLog.h>
#include <RcBuffer16.h>
#include <RcSocket.h>
#include <amsg.h>

#define INPUT_FIFO_SIZE            16
#define OUTPUT_FIFO_SIZE           16

static RcSocket  ClientSocket = NULL;

#endif

#define INPUT_BUFFER_SIZE          65000
#define OUTPUT_BUFFER_SIZE         65000
#define TIMEOUT                    1000000
#define SELECT_TIMEOUT             1000000

struct sigaction           StopSignalAction;
struct sigaction           PreviousSignalAction;
static int                 StopFlag = 0;      // 1: stop eveything
static FILE*               T2TimeFile = NULL;
static FILE*               T3TimeFile = NULL;
static FILE*               EventTimeFile = NULL;
static unsigned int        EventDisplay = 100;
static unsigned int        T2Skip = 1;
static unsigned int        NReceivedPacket = 0;

// Counters
static unsigned long int   IT3 = 0;
static unsigned long int   IT2 = 0;
static unsigned long int   IEvent = 0;
static unsigned long int   INoEvent = 0;
static unsigned long int   EventNumberBreak = 0;
static unsigned short int  PreviousEventNumber = 0;

// For rate calculation
static long double	           PreviousT2 = 0;
static long double	           PreviousIEvent = 0;
static long double	           PreviousT2Time = 0;
static long double	           PreviousEventTime = 0;
static long double	           LastT2Time = 0;
static long double	           LastT3Time = 0;
static long double	           LastEventTime = 0;

#ifdef SOCKLIB

void sendStartToLS(int sock_fd) {
	int ret = 0;
	UINT16 lsid = 1;
	UINT16 msg[4];
	msg[0] = 3 * sizeof(UINT16); // length in bytes of Msg without Msg->length
	msg[1] = 3;		     // length in UINT16 words  of Amsg including Amsg->length
	msg[2] = LS_START;	     // start tag
	msg[3] = lsid;		     // destination lsid - doesn't matter here
	printf("writing start command to station %hd at sock %d ...\n", lsid, sock_fd);
	ret = WriteSockMsg(sock_fd, (Msg*)&msg);
	printf("writing returned %d\n", ret);	
}

int openConnections()
{
	int MaxTries = 20, tries = 0;
	while (tries < MaxTries) {
		tries++;
		printf("Trying to connect to %s:%d ...\n", server, port);
		if (!(clientSocket_fd = OpenInetClient(port,server))) {
			printf("OpenInetClient() for %s:%d failed!!\n", server, port);
			clientSocket_fd = 0;
		} else {
			printf("OpenInetClient() for %s:%d  successfully\n", server, port);
			FD_ZERO(&clientFDS);
			FD_SET(clientSocket_fd,&clientFDS);
		}

		if (!(clientSocket_fd2 = OpenInetClient(port+1,server))) {
			printf("OpenInetClient() for %s:%d failed!!\n", server, port+1);
			clientSocket_fd2 = 0;
		} else {
			printf("OpenInetClient() for %s:%d  successfully\n", server, port+1);
			FD_SET(clientSocket_fd2,&clientFDS);
		}
		if (clientSocket_fd != 0 && clientSocket_fd2 != 0) {
			return 1; //for success
		}
		sleep (1);
	}
	printf("OpenInetClient() for %s:%d or %s:%d failed %d times!!\n", server, port, server, port+1, tries);
	return 0;
}

void closeConnections() {
	if (clientSocket_fd != 0) close(clientSocket_fd);
	clientSocket_fd = 0;
	if (clientSocket_fd2 != 0) close(clientSocket_fd2);
	clientSocket_fd2 = 0;
	printf("Connections closed\n");
}

void resetConnections() {
	int ret = 0;
	static int resetCount = 0;
	if (clientSocket_fd != 0 || clientSocket_fd2 != 0) closeConnections();

	while (clientSocket_fd == 0 || clientSocket_fd2 == 0) {
		if ((ret = openConnections()) != 0) {
			resetCount++;
			printf("resetConnections(): re-connected successfully, this was the %d reset in total\n", resetCount);
			break;
		} else {
			printf("resetConnections(): re-connection failed!!!\n");
			closeConnections();
			sleep(5);
		}
	}
}
#endif		// #ifdef SOCKLIB

/************************************************************/
int LsCheckOpen ( char*  name,
		    char*  ipAddress,
		    int    portNumber,
                    int    displayInput,
                    int    displayOutput	)
/************************************************************/
{
    //=======================================================
    // Open binary files.
    /*
    T2TimeFile = fopen ( "./LsCheckT2Time.dat", "w" );
    if ( T2TimeFile == NULL ) return (-1);

    T3TimeFile = fopen ( "./LsCheckT3Time.dat", "w" );
    if ( T3TimeFile == NULL ) return (-1);

    EventTimeFile = fopen ( "./LsCheckEventTime.dat", "w" );
    if ( EventTimeFile == NULL ) return (-1);
    */
  EventTimeFile = fopen ( "./LsCheckEventTime_k.txt", "w" );
    if ( EventTimeFile == NULL ) return (-1);
    //=======================================================
    // Create, open the ClientSocket object and install the handlers.
#ifdef SOCKLIB
    if ( !openConnections() ) return (-1);
#else
#ifdef RcRcLight
    ClientSocket = RcSocketNew ( name,
	                         0x2,
				 ipAddress, portNumber,
			         INPUT_BUFFER_SIZE,
	                         INPUT_FIFO_SIZE,
				 (unsigned short int) displayInput,
			         OUTPUT_BUFFER_SIZE,
	                         OUTPUT_FIFO_SIZE,
				 (unsigned short int) displayOutput );
#endif
#ifdef Rc4
    ClientSocket = RcSocketNew ( name,
	                         0x2,
				 ipAddress, portNumber,
			         INPUT_BUFFER_SIZE,
	                         INPUT_FIFO_SIZE,
				 (unsigned short int) displayInput,
			         OUTPUT_BUFFER_SIZE,
	                         OUTPUT_FIFO_SIZE,
				 (unsigned short int) displayOutput );
#endif
#ifdef Rc56
    ClientSocket = RcSocketNew ( name,
	                         0x1,
				 ipAddress, portNumber,
			         INPUT_BUFFER_SIZE,
	                         INPUT_FIFO_SIZE,
			         TIMEOUT,
			         OUTPUT_BUFFER_SIZE,
	                         OUTPUT_FIFO_SIZE,
			         SELECT_TIMEOUT );
#endif
    if ( ClientSocket == NULL )
    {
	printf ( "ERROR: in RcSocketNew\n" );
	return (-1);
    }
    else printf ( "INFO: RcSocketNew Ok.\n" );
#endif

    return (1);
}
    
/************************************************************/
int LsCheckClose ()
/************************************************************/
{
    printf ( "###############################################################################\n");
    
    //=======================================================
    // StopFlag and delete Socket.
    printf ( "INFO: Delete Sockets before ending.\n");
#ifdef SOCKLIB
    closeConnections();
#else
    if ( 0 > RcSocketDelete ( ClientSocket ) )
    {
	printf ( "ERROR: in RcSocketDelete\n" );
    }
#endif
	
    //=======================================================
    // Close binary files.
    /*
    printf ( "INFO: Close files.\n" );
    fclose ( EventTimeFile );
    fclose ( T3TimeFile );
    fclose ( T2TimeFile );
    */
    fclose (EventTimeFile);	    

    return (1);
}   
    
/************************************************************/
int LsCheckData ()
/************************************************************/
{

    // Buffers
#ifdef RcRcLight
    RcBuffer16               rcInBuf = NULL;
    RcBuffer16               rcOutBuf = NULL;
#endif
    unsigned short int*      inBuf = NULL;
    unsigned short int       inBufSize = 0;
    unsigned short int*      outBuf = NULL;
    unsigned short int*      outBuf1 = NULL;
    unsigned short int       outBufSize = 0;

    // Messages
    unsigned short int       inBufMsg = 0;
    unsigned short int       inMsgSize = 0;
    unsigned short int       inMsgT2 = 0;
    unsigned short int       t2LsId = 0;
    unsigned short int       eventNumber = 0;
    unsigned short int       eventLsId = 0;
    unsigned int*            eventSecond = 0;
    unsigned int*            eventNanoSecond = 0;

    ls_geteventbody*  t3 = NULL;

    // Time calculation
    unsigned int*            t2Second = NULL;
    T2SSEC*                  t2SubSecond = NULL;
    unsigned int             second[1024];
    unsigned int             nanoSecond[1024];
    long double              t2Time[1024];
    long double              eventTime[1024];
    long double              t3Time[1024];

    // Local counters
    unsigned short int       nT2 = 0;
    unsigned short int       iT2 = 0;
    unsigned short int       iT3 = 0;
    unsigned short int       iEvent = 0;

    // Rates
    long double              responseTime = 0;
    long double              t2Rate = 0;
    long double              eventRate = 0;

    //=======================================================
    // Process Input Buffer
#ifdef RcRcLight
    rcInBuf = RcSocketGetInputBufferToBeEmptied ( ClientSocket );
    if ( rcInBuf == NULL ) return (0);
    inBufSize = RcBuffer16GetUsedSize ( rcInBuf );
    if ( inBufSize == 0 || inBufSize > INPUT_BUFFER_SIZE ) return (0);
    inBuf = RcBuffer16GetWords ( rcInBuf );
#endif
#ifdef SOCKLIB
	if (clientSocket_fd == 0 || clientSocket_fd2 == 0) openConnections();

	// check the socket and process incoming data
	Msg *getMsg;
	int msgLength = 0;
	int evInMsgCount = 0;
	int sel = 0;
	FD_ZERO(&clientFDS);
	FD_SET(clientSocket_fd,&clientFDS);
	FD_SET(clientSocket_fd2,&clientFDS);

		
	clientSocketMax = (clientSocket_fd>clientSocket_fd2)?clientSocket_fd:clientSocket_fd2;

	int evMsgCount = 0;
	int i;
	if ((sel = select(clientSocketMax+1,&clientFDS,NULL,NULL,&sockTimeout)) > 0)
	{
	    if(FD_ISSET(clientSocket_fd, &clientFDS)){
		clientSocket_fd_now = clientSocket_fd;
	    } else if (FD_ISSET(clientSocket_fd2, &clientFDS)) {
		clientSocket_fd_now = clientSocket_fd2;
	    } else {
		printf("Error: select ok but no FD active????");
		return 0;
	    }

	    if (!(getMsg = ReadSockMsgSM_TO(clientSocket_fd_now, readTimeout)))
	    {
		printf("Error: ReadSockMsgSM_TO() returned NULL - resetting connection ...\n");
		resetConnections();
		return 0;
	    }
	    else
	    {	// have a message
		msgLength = getMsg->length;
		inBuf = (UINT16*) getMsg->data;
		UINT16 amsgLength = inBuf[0];
		UINT16 amsgTag = inBuf[1];

		if (amsgTag = LS_EVENT)
		{
		    inBufSize = msgLength/sizeof(UINT16); // alle prozessieren
		    evInMsgCount = msgLength/sizeof(UINT16)/EVMSGSIZE;
		    notProcessedT3s += evInMsgCount - 1; // the first one is processed
		    // inBufSize = amsgLength;		 // nur die erste nehmen, bei den weiteren gibt es noch Probleme
		}
		else inBufSize = msgLength/sizeof(UINT16);	// alle prozessieren
			
		// printf("select() returned %d, processing msg of length %d bytes = %d count of UINT16, first tag %d ...\n", sel, msgLength, inBufSize, amsgTag);
	    	UINT16 *alp = &inBuf[0];	// always pointing to the amsg->length, jump from one amsg to next
	    	char *cp = (char*) alp;
	    	UINT16 al = *alp;
	    	int uintsRead = 0, amsgCounter = 1;
	    	if (amsgTag == LS_EVENT) while (uintsRead < inBufSize)
		{
		    // printf("AMSG no. %d has length %hu and tag %d, read %d of %hu \n", amsgCounter, al, alp[1], uintsRead, inBufSize);
		    for (i = -6; i< 14; i++)
		    { // printing the last 3 and first 7 UINT16 of the AMSG
			// if ((i%2) == 0) printf ( "INFO: UINT16[%d] = %hu = 0x%X\n", i/2, alp[i/2] , alp[i/2]);
			// printf ( "INFO: char[%d] = %d = 0x%X\n", i, cp[i] , cp[i]);
		    }
		    for (i = -6; i< 14; i++)
		    { // printing the last 6 and first 14 chars of the AMSG
			// printf ( "INFO: char[%d] = %c = 0x%X\n", i, cp[i] , cp[i]);
		    }
		    alp += al;
		    cp = (char*) alp;
		    uintsRead += al;
		    al = *alp;
		    amsgCounter++;
	    	}
	    }
	}
/*	else if (sel == 0)
	{
	    // printf("select() returned %d, nothing to do ...\n", sel);
	    LsCheckS3DataDone = 1;
	    return 0;
	}
	else 
	{
	    printf("select() returned %d, error? Resetting connection ...\n", sel);
	    resetConnection();
	    return 0;
	}*/
#endif
#ifdef Rc4
    inBuf = RcSocketGetInputBufferToBeEmptied ( ClientSocket );
    if ( inBuf == NULL ) return (0);
    inBufSize = inBuf[0];
    if ( inBufSize == 0 || inBufSize > INPUT_BUFFER_SIZE ) return (0);
    inBuf++;
#endif
#ifdef Rc56
    inBuf = RcSocketGetInputBufferToBeEmptied ( ClientSocket );
    if ( inBuf == NULL ) return (0);
    inBufSize = inBuf[0];
    if ( inBufSize == 0 || inBufSize > INPUT_BUFFER_SIZE ) return (0);
    inBuf++;
#endif
    //printf ( "INFO: Received Buffer ( size %d): %d %d %d - last %d\n", inBufSize, inBuf[0], inBuf[1], inBuf[3], inBuf[inBufSize - 1] );

    while ( inBufMsg < inBufSize )
    {
	inBuf = inBuf + inMsgSize;
	inMsgSize = inBuf[0];  // Message Size
	if ( inMsgSize > inBufSize )
	{

	    printf ( "ERROR: Msg size (%d) > Buffer size (%d)\n",
		     inBuf[0], inBuf[1], inBufSize );
#ifndef SOCKLIB
	    RcSocketSetInputBufferEmptied ( ClientSocket );
#endif
		return (0);
	}
	// printf ( "INFO: message: size %d - tag %d - last %d\n", inBuf[0], inBuf[1], inBuf[inBuf[0] - 1] );
	switch ( inBuf[1] )    // Message Tag
	{
	    case LS_T2:
		// Save T2 second, nanosecond and time
		t2LsId = inBuf[2];                   // T2 LS Identifier 
		t2Second = (unsigned int*) ( inBuf + 3 );  // T2 GPS second
		for  ( inMsgT2 = 5; inMsgT2 < inMsgSize; inMsgT2 += 2 )
		{
		    t2SubSecond = (T2SSEC*) ( inBuf + inMsgT2 );
		    nanoSecond[nT2] = T2NSEC (t2SubSecond );
		    second[nT2] = *t2Second;
		    t2Time[nT2] = (double) second[nT2] + 1E-9 * (double) nanoSecond[nT2];
		    nT2++;
		    IT2++;
		}
		LastT2Time = t2Time[nT2 - 1];
		break;
	    case LS_EVENT:
		eventNumber = inBuf[3];
		//printf ( "Received evno %d\n", eventNumber );
		if ( eventNumber != PreviousEventNumber + 1 )
		{
		    printf ( "evno break : waited %d - received %d\n", PreviousEventNumber + 1, eventNumber );
		    EventNumberBreak++;
		}
		PreviousEventNumber = eventNumber;
		eventLsId = 0xff & inBuf[4];
		eventSecond = (unsigned int*) ( inBuf + 6 );
		eventNanoSecond = (unsigned int*) ( inBuf + 8 );
		eventTime[iEvent] = (double) *eventSecond + 1E-9 * (double) *eventNanoSecond;
		LastEventTime = eventTime[iEvent];
		iEvent++;
		IEvent++;
		break;
	    case LS_NO_EVENT:
		eventNumber = inBuf[2];
		eventLsId = 0xff & inBuf[3];
		INoEvent++;
		break;
	    default:
		printf ( "ERROR: LS unknown message: size %d - tag %d in buffer of size %d\n",
			 inBuf[0], inBuf[1], inBufSize );
#ifndef SOCKLIB
		RcSocketSetInputBufferEmptied ( ClientSocket );
#endif
		return (0);
	}
	inBufMsg += inMsgSize;
    }

#ifndef SOCKLIB
    RcSocketSetInputBufferEmptied ( ClientSocket );
#endif

    //=======================================================
    // Prepare and send Output Buffer with T3 LS Event Request Message
#ifdef RcRcLight
    rcOutBuf = RcSocketGetOutputBufferToBeFilled ( ClientSocket );
    if ( rcOutBuf == NULL ) return (0);
    outBuf = RcBuffer16GetWords ( rcOutBuf );
#endif
#ifdef SOCKLIB
    outBuf = &sendBuff[1];
#endif
#ifdef Rc4
    outBuf = RcSocketGetOutputBufferToBeFilled ( ClientSocket );
    if ( outBuf == NULL ) return (0);
#endif
#ifdef Rc56
    outBuf1 = RcSocketGetOutputBufferToBeFilled ( ClientSocket );
    if ( outBuf1 == NULL ) return (0);
    outBuf = outBuf1 + 1;
#endif

    // Generate one T3 from 't2Skip' T2 
    for ( iT2 = 0; iT2 < nT2; iT2++ )
    {
	if ( !( (IT2-nT2+iT2) % T2Skip ) )
	{
	    outBuf[0] = 6;
	    outBuf[1] = LS_GETEVENT;
	    t3 = (ls_geteventbody*) ( outBuf + 2 );
	    T3STATFILL ( t3, t2LsId, second[iT2], ( nanoSecond[iT2] & 0x3fffffc0 ) >> 6 );
	    outBuf[3] = IT3 + 1; //evno start at 1
	    //printf ( "Requested evno %d\n", outBuf[3] );

	    outBufSize += 6;
	    outBuf += 6;

	    t3Time[iT3] = t2Time[iT2];
	    // printf("t3Time[%d] = %f, t2Time[%d] = %f \n", iT3, t3Time[iT3], iT2, t2Time[iT2]);
	    iT3++;
	    IT3++;
	    
	    if ( outBufSize > OUTPUT_BUFFER_SIZE )
	    {
		printf("Exceed output buffer size ! Stopped cumulating T2s, now sending");
		outBufSize -= 6;
		break;
	    }
	}
    }
    if (iT3 > 0) LastT3Time = t3Time[iT3 - 1];

    // printf ( "T3 %ld - Ev %ld - evno %d\n", IT3, IEvent, eventNumber );
#ifdef RcRcLight
    RcBuffer16SetUsedSize ( rcOutBuf, outBufSize );
    RcSocketSetOutputBufferFilled ( ClientSocket );
#endif
#ifdef SOCKLIB
    if (outBufSize > 0)
    {
	int  written;
	sendBuff[0] = (UINT16) outBufSize * sizeof(UINT16);	
	// printf("outBufSize = %d, set sendBuff[0] = %d, writing message to socket...\n", outBufSize, sendBuff[0]);
	if ((written = WriteSockMsg(clientSocket_fd, (Msg*) sendBuff)) != 0)
	{
	    // if (written < 0) perror("WriteSockMsg() in LsCheckS3: ");
	    printf("%d - WriteSockMsg() returned %d, resetting connection\n", (int)time(NULL), written);
	    fflush(stdout);
	    resetConnections();
	} // else printf("WriteSockMsg() returned %d, written %d bytes\n", written, sendBuff[0]);
	outBufSize = 0;
    }
#endif
#ifdef Rc4
    RcSocketSetOutputBufferFilled ( ClientSocket, outBufSize );
#endif
#ifdef Rc56
    outBuf1[0] = outBufSize;
    RcSocketSetOutputBufferFilled ( ClientSocket );
#endif
   
    //=======================================================
    // Display statistics immediatly after the sending of T3 buffer.
    if ( EventDisplay != 0 && iEvent != 0 )
    {
	if ( ( IEvent - PreviousIEvent ) > EventDisplay  )
	{
	    responseTime = LastT3Time - LastEventTime;
	    t2Rate = ( IT2 - PreviousT2 ) / ( LastT2Time - PreviousT2Time );
	    eventRate = ( IEvent - PreviousIEvent  ) / ( LastEventTime - PreviousEventTime );

	    printf ( "T2 %.9Lf  iT2/iT3 %.1f  T2-Ev %.3Lfs/%d  evno-iEv %d  break %lu  iT3 %lu/iNo %lu  T2 %.0Lf/s  Ev %.0Lf/s\n",
		      LastT3Time,
		      (float)IT2 / (float)IT3,
		      responseTime,
		      (unsigned short int) ( IT3 % 65536 ) - eventNumber,
		      eventNumber - (unsigned short int) ( IEvent % 65536 ),
		      EventNumberBreak,
		      IT3, INoEvent,
		      t2Rate, eventRate );

	    PreviousT2 = IT2 - 1;
	    PreviousIEvent = IEvent - 1;
	    PreviousT2Time = LastT2Time;
	    PreviousEventTime = LastEventTime;
	}
    }

    // StopFlag after n received packets !!!
    if ( NReceivedPacket > 0 && IEvent >= NReceivedPacket ) StopFlag = 1;

    //=======================================================
    // Save times into file
    /*
    if ( nT2 != fwrite ( t2Time, sizeof (double), nT2, T2TimeFile ) )
    {
	printf ( "ERROR: in T2TimeFile fwrite of %d double : %s\n", nT2, strerror ( errno ) );
    }
    fflush ( T2TimeFile );

    if ( iT3 != fwrite ( t3Time, sizeof (double), iT3, T3TimeFile ) )
    {
	printf ( "ERROR: in T3TimeFile fwrite of %d double : %s\n", iT3, strerror ( errno ) );
    }
    fflush ( T3TimeFile );

    if ( iEvent != fwrite ( eventTime, sizeof (double), iEvent, EventTimeFile ) )
    {
	printf ( "ERROR: in fwrite of %d double : %s\n", iEvent, strerror ( errno ) );
    }
    fflush ( EventTimeFile );
    */
    int k=0;
    for (k=0; k<iEvent; k++) {
       fprintf(EventTimeFile, "%.9Lf\n", eventTime[k]);
    }
    fflush ( EventTimeFile );

    return (1);
}

/************************************************************/
static void LsCheckStop ( int signo )
/************************************************************/
{
    printf ( "###############################################################################\n");
    printf ( "INFO Ctrl+C (SIGINT) requests process ending.\n" );
    StopFlag = 1;
}   
    
/************************************************************/
int LsCheckBanner ()
/************************************************************/
{
    printf ( "###############################################################################\n");
    printf ( "First Argument (Mandatory):\n" );
    printf ( "   Server Ip address (example: 134.158.27.19)\n" );
    printf ( "Second Argument (Optional):\n" );
    printf ( "   Port number >= 1024 (default)\n" );
    printf ( "Third Argument (Optional):\n" );
    printf ( "   T2 skip:\n" );
    printf ( "   - (0): off (default)\n" );
    printf ( "   - (1): off (default)\n" );
    printf ( "   - (n): 1/(n T2)\n" );
    printf ( "Fourth Argument (Optional):\n" );
    printf ( "   Requested input packet = 0 = infinite (default)\n" );
    printf ( "Fifth Argument (Optional):\n" );
    printf ( "   Event display:\n" );
    printf ( "   - (0): Off\n" );
    printf ( "   - (n): 1/(n Events)\n" );
    printf ( "   - (100): 1/(100 Events) (default)\n" );
    printf ( "Sixth Argument (Optional):\n" );
    printf ( "   Loop Delay (microseconds) = 100 (default)\n" );
    printf ( "Seventh Argument (Optional):\n" );
    printf ( "   Input Buffer Quicklook:\n" );
    printf ( "   - (0): Off (default)\n" );
    printf ( "   - (n): 1/n buffer\n" );
    printf ( "Heigth Argument (Optional):\n" );
    printf ( "   Output Buffer Quicklook:\n" );
    printf ( "   - (0): Off (default)\n" );
    printf ( "   - (n): 1/n buffer\n" );
    printf ( "###############################################################################\n\n");

    return (1);
}   
    
/************************************************************/
int main ( int argc, char** argv )
/************************************************************/
{
    char              message[128];
    struct sigaction  stopSignalAction;
    struct sigaction  previousSignalAction;

    // Input parameters
    char         ipAddress[16];
    int          portNumber = 1024;
    int          loopSleep = 1000;
    int          displayInput = 0;
    int          displayOutput = 0;
    char*        name = "LsCheck";

    //=======================================================
    // Manage program argument.
    if ( argc < 2 || argc > 9 )
    {
	LsCheckBanner ();
	return (-1);
    }

    strcpy ( ipAddress, argv[1] );
    if ( strlen ( argv[1] ) < 8 )
    {
	LsCheckBanner ();
	return (-1);
    }
    
    if ( argc > 2 )
    {
	portNumber = atoi ( argv[2] );
	if ( portNumber < 1024 )
	{
	    LsCheckBanner ();
	    return (-1);
	}
    }

    if ( argc > 3 )
    {
	T2Skip = atoi ( argv[3] );
	if ( T2Skip == 0 ) T2Skip = 1;
    }
    
    if ( argc > 4 )
    {
	NReceivedPacket = (int) atof ( argv[4] );
	if ( NReceivedPacket < 0 )
	{
	    RcClientBanner ();
	    return (-1);
	}
    }
    
    if ( argc > 5 )
    {
	EventDisplay = atoi ( argv[5] );
    }
    
    if ( argc > 6 )
    {
	loopSleep = atoi ( argv[6] );
    }
    
    if ( argc > 7 )
    {
	displayInput = atoi ( argv[7] );
    }
    
    if ( argc > 8 )
    {
	displayOutput = atoi ( argv[8] );
    }

#ifdef SOCKLIB
    port = portNumber;
    server = ipAddress;
#endif

    printf ( "###############################################################################\n");
    printf ( "Parameters: ipAddress %s - portNumber %d - T2Skip %d - NReceivedPacket %d\n", ipAddress, portNumber, T2Skip, NReceivedPacket );
    printf ( "Parameters: T3display %d - loopSleep %d - displayInput %d - displayOutput %d\n", EventDisplay, loopSleep, displayInput, displayOutput );
    printf ( "###############################################################################\n\n");

    //=======================================================
    // Program stop by SIGINT (Ctrl+C).
    stopSignalAction.sa_handler = LsCheckStop;
    sigemptyset ( &stopSignalAction.sa_mask );
    stopSignalAction.sa_flags = 0;
    sigaction ( SIGINT, &stopSignalAction, &previousSignalAction );
    sigaction ( SIGTERM, &stopSignalAction, &previousSignalAction );
    sigaction ( SIGKILL, &stopSignalAction, &previousSignalAction );

    //=======================================================
    // Start all the objects.
    if ( 0 > LsCheckOpen ( name, ipAddress, portNumber, displayInput, displayOutput ) )
    {
	printf ( "ERROR: in LsCheckOpen\n" );
	return (-1);
    }
	
    //=======================================================
    // Send and receive buffers through tcp sockets.
    while ( !StopFlag )
    {
	if ( 0 > LsCheckData () ) break;
	usleep ( loopSleep );
    }

    //=======================================================
    // Delete objects.
    LsCheckClose ();

    return (1);
}
