#!/bin/bash

T2file_ref_dir="./fake-home/daq-ref/log/"
T2file_ref_1=""
T2file_ref_2=""
T2file_ref_3=""

T2file_ref_path_1=""
T2file_ref_path_2=""
T2file_ref_path_3=""

T2file_ref_1=$(cd $T2file_ref_dir; basename "$(find -name pm_ls00$1*__t2file.log)")
T2file_ref_2=$(cd $T2file_ref_dir; basename "$(find -name pm_ls00$2*__t2file.log)")
T2file_ref_3=$(cd $T2file_ref_dir; basename "$(find -name pm_ls00$3*__t2file.log)")

echo $T2file_ref_1
echo $T2file_ref_2
echo $T2file_ref_3

if [[ -n $T2file_ref_1 ]]; then
T2file_ref_path_1=$T2file_ref_dir$T2file_ref_1;
fi

if [[ -n $T2file_ref_2 ]]; then
T2file_ref_path_2=$T2file_ref_dir$T2file_ref_2;
fi

if [[ -n $T2file_ref_3 ]]; then
T2file_ref_path_3=$T2file_ref_dir$T2file_ref_3;
fi

echo $T2file_ref_path_1
echo $T2file_ref_path_2
echo $T2file_ref_path_3 

echo "..concatenate.."
cat $T2file_ref_path_1 $T2file_ref_path_2 $T2file_ref_path_3 > $T2file_ref_dir"cat_tmp.txt"

echo "..sort.."
sort -nk 2 $T2file_ref_dir"cat_tmp.txt" > $T2file_ref_dir"cat_tmp_sorted.txt" 


