#!/bin/bash

T2file_kitcomms_dir="./fake-home/daq/log/"
T2file_ref_dir="./fake-home/daq-ref/log/"
T2file_kitcomms=""
T2file_ref=""
T2file_kitcomms_path=""
T2file_ref_path=""

T2file_kitcomms=$(cd $T2file_kitcomms_dir; basename "$(find -name pm_ls00$1*__t2file.log)")

if [[ -z $2 ]]; then
echo "load cat_tmp_sorted.txt...";
T2file_ref="cat_tmp_sorted.txt";
else
T2file_ref=$(cd $T2file_ref_dir; basename "$(find -name pm_ls00$2*__t2file.log)")
fi

T2file_kitcomms_path=$T2file_kitcomms_dir$T2file_kitcomms
echo $T2file_kitcomms_path

T2file_ref_path=$T2file_ref_dir$T2file_ref
echo $T2file_ref_path

diff $T2file_kitcomms_path $T2file_ref_path


