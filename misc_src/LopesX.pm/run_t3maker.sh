#!/bin/sh

ctrl_c()
{
echo -en "\n************ctrl-C out**********\n"
export HOME=/home/daq
echo $HOME
exit $?
}

trap ctrl_c SIGINT

export HOME=/home/daq/temp/fake-home/daq
echo $HOME
/home/daq/temp/t3maker_sl_20130507
export HOME=/home/daq
echo $HOME
