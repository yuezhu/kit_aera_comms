#!/bin/sh

ctrl_c()
{
echo -en "\n************ctrl-C out**********\n"
export HOME=/home/daq
echo $HOME
exit $?
}

trap ctrl_c SIGINT

export HOME=/home/daq/temp/fake-home/daq
echo $HOME
/home/daq/AERA/trunk/lopesX/trunk/T3maker/Debug/T3maker &
/home/daq/AERA/trunk/aevb/DebugX/aevb &
export HOME=/home/daq
echo $HOME
