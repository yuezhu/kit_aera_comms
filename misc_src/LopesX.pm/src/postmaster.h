/*
 * postmaster.h
 *
 *  Created on: 15.08.2011
 *      Author: weindl
 */

#ifndef POSTMASTER_H_
#define POSTMASTER_H_

#ifndef RcType_
#include <RcType.h>
#endif
#include "amsg.h"

#include <stdint.h>
#include <sys/socket.h>				// getsockopt

#ifdef USE_MSG
extern "C" {
//	#include "socklib.h"
//	#include "connlib.h"
}
#endif

// ports to open as server
#define T3MAKER_PORT 5012
#define EVBUILDER_PORT 5013
#define GUI_PORT 5014
#define MONITOR_PORT 5015
#define PROMPT_PORT 5019

//#define MAX_LS		24		// AW, 20.12.2012 - this is AERA first stage
#define MAX_LS		154			// AW, 20.12.2012 - this is AERA second stage

#define MSG_SNDBUFF_LENGTH		65540		// buffer length for buffer with messages to send - 65k is about the size of an Rc buffer

#define MIN_GPS_TIME			900000000	// just for consistency checks of T2 gps seconds, corresponds to a timestamp at Jul 13, 2008, where AERA did not take data

// mit dem MACRO SWAP32 werden die bytes des GPS Zeitstempels so umsortiert, wie es die holländische Elektronik ausgibt: ABCD => DCBA
// MACRO from Christoph Ruehle to SWAP the bytes of german T2SSEC to the same order as in dutch electronics
#define SWAP32(x) ((((x) & 0xff) << 24) | (((x) & 0xff00) << 8) | (((x) & 0xff0000) >> 8) | (((x) >> 24) & 0xff))

// MACRO to swap 2 UINT16 or the upper and lower half of an 32 bit int
#define SWAP_UL(i)	(((i & 0xFFFF) << 16) | ((i >> 16) & 0xFFFF))

// das Macro GET_T0_FROM_T2MSG soll die Umkehrung zu SWAP32 sein
#define GET_T0_FROM_T2MSG(a)	SWAP32(((a&0xFFFF0000)>>16) | ((a&0xFFFF)<<16))

#define FUNC_BEGIN			printf("%s ...\n", __FUNCTION__);fflush(stdout);
#define FUNC_END			printf("%s done\n", __FUNCTION__);fflush(stdout);

// client ports to connect to are read from config file


#define NO_MSG_CONN_TIMEOUT		30									// timeout in seconds since last messages, then connection is closed/resetted
#define CONN_CHECK_INTERVAL		10									// interval in seconds since last messages, then connection is closed/resetted

#define LS_SEL_TIMEOUT			10									// timeout in microseconds for read select on station sockets
#define LS_READ_TIMEOUT			50000								// timeout in microseconds for read on station sockets, passed to socklib
#define T3_READ_TIMEOUT			10									// timeout in microseconds for read select on T3 socket
#define EB_READ_TIMEOUT			10									// timeout in microseconds for read select on Eb socket
#define GUI_READ_TIMEOUT		10									// timeout in microseconds for read select on Gui socket
#define PR_READ_TIMEOUT			10									// timeout in microseconds for read select on Pr socket
#define KITCOMMS_READ_TIMEOUT		10						// timeout in microseconds for read select on KitComms socket
#define KITCOMMS_SEL_TIMEOUT		10						// timeout in microseconds for read select on KitComms socket

#define LATE_T2_LIMIT				3										// seconds, if T2s are older respectively to the newest one, a message is displayed

#define PM_CONF_FILE_NAME			"/conf/pm.init";				// configuration file of postmaster in $HOME
#define ARRAY_FILE_NAME				"/conf/array.conf";					// configuration file of array in $HOME
#define T2_FILE_NAME_HEAD			"/log/pm_ls";					// t2 file names,one for each station, supplemented with station ID
#define T2_FILE_NAME_TAIL			"_t2file.log";
#define T3_FILE_NAME_HEAD			"/log/pm_ls";					// t3 file names,one for each station, supplemented with station ID
#define T3_FILE_NAME_TAIL			"_t3file.dat";

#define ACT_STATISTIC_INTERVAL		10								// the interval used for doing and printing statistics

#define MAX_CAR_NAME	200
typedef struct {
	char name[MAX_CAR_NAME];				// a suitable name to identify this counter/rate struct should not need more than 200 chars!
//	for total time
	time_t startTime;						// when this thing first happens/appears
	uint64_t totalCount;					// AW, 07.08.2012: 4e9 may be a bit less to count T2s of 100 stations => changed to 64 bit
	double totalRate;						// the total rate, since it first happened
//	for actual time interval
	time_t actTime;
	uint64_t actCount;
	double actRate;						// the actual rate, since the last actTime
	void calcNow() {
		time_t now = time(NULL);
		if (startTime == 0) startTime = now;
		else {
			totalRate = (double) totalCount/difftime(now, startTime);
//			printf("totalRate of %s at %d / %s : totalCount = %8llu, totalRate = %10.6f \n", name, (int)time(NULL), getTimeStringNow(), totalCount, totalRate);
		}
		if (actTime == 0) actTime = now;
		else {
			actRate = (double) actCount/difftime(now, actTime);
//			actTime = now;
//			actCount = 0;
		}
	}
	void init(char *n) {
		if (strlen(n) > MAX_CAR_NAME) strncpy(name, n, MAX_CAR_NAME-1);
		else sprintf(name, "%s", n);
		startTime = 0;
		totalCount = 0;
		totalRate = 0.0;
		actTime = 0;
		actCount = 0;
		actRate = 0.0;
	}
	void print() {
		printf(     "Count and Rate of %s at %d / %s : actCount = %6llu, actRate = %10.6f / totalCount = %8llu, totalRate = %10.6f \n", name, (int)time(NULL), getTimeStringNow(), actCount, actRate, totalCount, totalRate);
	}
	void print2File(FILE *fp) {
		if (fp == NULL) return;
		fprintf(fp, "Count and Rate of %s at %d / %s : actCount = %6llu, actRate = %10.6f / totalCount = %8llu, totalRate = %10.6f \n", name, (int)time(NULL), getTimeStringNow(), actCount, actRate, totalCount, totalRate);
	}
	void resetActNow() {
		time_t now = time(NULL);
		actTime = now;
		actCount = 0;
	}
} CountAndRate;

typedef struct {
	int lsid;
	int gpssecond;
	int nsecond;
} lst2;


#endif /* POSTMASTER_H_ */

