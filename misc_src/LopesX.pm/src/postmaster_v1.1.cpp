//============================================================================
// Name        : postmaster.cpp
// Author      : A. Weindl
// Version     :
// Copyright   :
// Description : a postmaster for the AERA daq, acting as a relais between local stations and all other daq processes

// Notes:
//	- There is no way to detect a broken connection but on writing to it!! Or just rest it, if there is a time interval without messages - or send "alive" messages?!
//
//
// History:
//  - 25.03.2013: KIT_COMMS code built-in, Y.Zhu
//  - 11.12.2012: not only sending ALIVE to t3maker, but also to LS, for test purpose
//  - 19.11.2012: working on aera tcp/ip transport definitions - move to a separate library?
//  - 11.10.2012: fixed bug reading eventnumber and lsid from LSNOEVENT
//  - 07.08.2012: changed total and actual count in struct CountAndRate from 32 to 64 bit
//	- 19.04.2012: added logging via lloglib using the "printf"-format with llout(). So each entry may be doubled for syslog
//	- 02.04.2012: started to make postmaster compatible to Rc.
//	              This is done using #define USE_RC, where Rc should be used, or else socklib
//
//
//
// TODO:
//	- add syslog to pmLog
//  - make a switch to turn off logging to file?
//  - socklib: make different readSocket() for stations and other sockets
//  - test stations more often for read than T3? Eb, Prompt, Gui may be read once a second
//	- add function to dump init params from pm.init
//  - (re-)connecting to sockets with OpenInetClient() may take some time and block the postmaster if connect() blocks (if no server is listening on port or networkproblems)
//  - prevent alssim from stopping if pm stops

// DONE:
//  - 19.04.2012: logging via lloglib
//	- extend statistics: count T2 per station, t3 requests and t3 answers/data per station
//  - total and actual rates
//  - process multiple AMSG per Msg
//============================================================================

//#define USE_MYSQL		// AW; 26.03.2013: put here for test purpose, has to be moved to makefile

#include <iostream>
using namespace std;

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iomanip>

#include <sys/stat.h>	// for stat()
#include <sys/statfs.h>	// for statfs()		// statfs() is not a Posix standard function
#include <sys/statvfs.h>	// for statvfs()

#include "amsg.h"
#include "AeraEvent.h"
#include "AeraDefs.h"
#include "AeraTags.h"
#include "AeraRcDefs.h"
using namespace aera;

#include "postmaster.h"

#include "llog.h"
#include <sys/stat.h>

#ifdef USE_MYSQL
#include "pmMysql.h"
#endif		// #ifdef USE_MYSQL

#ifdef USE_RC
#include "AeraRcDefs.h"
#include "rcdefs.h"
#include <Rc.h>


void disconnectSocket(rcConnInfo *ci);
#ifndef USE_RC5
int processRcBuffer(RcBuffer16 buffer);
#else
int processRcBuffer(UINT16 *buffer);
#endif
void readSocket(rcConnInfo *cip);
void sendStartToLS(rcConnInfo *lsconn);
void sendStopToLS(rcConnInfo *lsconn);
void sendAliveAck(rcConnInfo *conn);
#else
extern "C" {
	#include "socklib.h"
	#include "connlib.h"
}
void disconnectSocket(connInfo *ci);
void forwardEvToAevb(Msg *msgp);
void forwardT2toT3(Msg *msgp);
int processMsg(Msg *msgp);
void readSocket(connInfo *cip);
void sendStartToLS(connInfo *lsconn);
void sendStopToLS(connInfo *lsconn);
void socketHasError(connInfo *ci);
void sendAliveAck(connInfo *conn);
#endif

void checkSocketsForRead();

void checkGui();
void checkPrompt();
void checkT3();
void checkStationConnections();
void pmLog(char *fmt, ...);
void processEvMsg(AMSG *amsgp);
void processT2Msg(AMSG *amsgp);
void processT3Msg(AMSG *amsgp);
void sendLSinitialize();
void sendStartToAllLS();
void sendStopToAllLS();
void sendT3reqToLS(AMSG *amsgp);
	
#ifndef USE_RC
	#define KIT_COMMS //place to deactivate all changes in this cpp related to kit comms, note: only for #ifndef USE_RC!
#endif
#ifdef KIT_COMMS
void sendT3reqToLsKitComms(AMSG *amsgt3p);
void checkKitCommsForRead(); //for "#ifdef CHECK_SOCKETS_SEPARATELY", #else: it is built in  
#endif
void showConnectionStatus();
void startDaq();
void stopDaq();
void terminatePM(int sig);

static uint32_t t2errorCount = 0;
//static uint32_t t2totalCount = 0;
static uint32_t t2MsgtotalCount = 0;

static uint32_t t3errorCount = 0;
//static uint32_t t3totalCount = 0;
static uint32_t t3MsgtotalCount = 0;

static char * pmConfFileName = PM_CONF_FILE_NAME;
static char * arrayFileName = ARRAY_FILE_NAME;
static char * t2FileNameHead = T2_FILE_NAME_HEAD;
static char * t2FileNameTail = T2_FILE_NAME_TAIL;
static char * t3FileNameHead = T3_FILE_NAME_HEAD;
static char * t3FileNameTail = T3_FILE_NAME_TAIL;

string daqhome;

FILE* confFile;
FILE* arrayFile;
FILE* t2Files[MAX_LS];
FILE* t3Files[MAX_LS];
static fd_set acceptClientFDS;				// fd_set to check if Eb, T3 and Gui want to be accepted
static fd_set readClientFDS;				// fd_set for all available readable client sockets (all sockets, except station sockets) to check if there is something to be read on the socket
int maxReadNumClients;
struct timeval readClientsTimeval = {0,10};

static fd_set readFDS;						// one read mask for all sockets

static fd_set readPromptFDS;				// fd_set to check if there is something from prompt to be read
static fd_set readEbFDS;					// fd_set to check if there is something from T3 to be read
static fd_set readT3FDS;					// fd_set to check if there is something from T3 to be read
static fd_set readGuiFDS;					// fd_set to check if there is something from Gui to be read

static fd_set readStationsFDS;				// fd_set to check if a stations has send something to be read
int maxReadNumStations;

// stuff set via config file pm_init
bool autoStartDaq = false;
bool writeT2File = false;
bool writeT2FileInAscii = false;
bool writeT3File = false;
bool writeT3FileInAscii = false;
bool useGui = true;
bool useEb = true;
bool useT3 = true;
bool usePr = true;
//bool useKitComms = false;			// KitComms off by default
bool useDB = true;
uint32_t selLsTimeout  = LS_SEL_TIMEOUT;
//uint32_t readLsTimeout  = 100000; // LS_READ_TIMEOUT as recommended value for lopes-lab by Christoph Ruehle
uint32_t readLsTimeout  = LS_READ_TIMEOUT;
uint32_t readT3Timeout  = T3_READ_TIMEOUT;
uint32_t readEbTimeout  = EB_READ_TIMEOUT;
uint32_t readGuiTimeout = GUI_READ_TIMEOUT;
uint32_t readPrTimeout  = PR_READ_TIMEOUT;
uint32_t noMsgConnTimeout = NO_MSG_CONN_TIMEOUT;			// timeout in seconds since last messages, then connection is closed/resetted
uint32_t connCheckInterval = CONN_CHECK_INTERVAL;			// interval in which to check connections
time_t lastConnCheckTime = 0;
uint32_t readStationsTimeoutUs = 100;						// timeout in microseconds to select readable sockets

#ifdef USE_RC
rcConnInfo EbConn, T3Conn, GuiConn, PromptConn;
rcConnInfo LsConn[MAX_LS];
#else
connInfo EbConn, T3Conn, GuiConn, PromptConn;			// connection infos of Eb, T3 and Gui
//struct sockaddr *EbConnAddress, T3ConnAddress, GuiConnAddress;		 => servAddress included in connInfo
connInfo LsConn[MAX_LS];					// connection infos of all stations found in array.txt
	#ifdef KIT_COMMS
	char* KitCommsVersion = "1.1";
	char* KitCommsDescription = " 1. support parameters to control rate for T3reqlist via kitcomms with M-Bias detection; \
	2. support array.conf with KITCOMMS - port = 0, thus part of the statistics; \
	3. reduced T3req-list for kitcomms;\
	4. selKitCommsTimeout and readKitCommsTimeout (though no use)\
	Support T2/T3/T3-req via RT/HQ, limitation: STANDALONE only for kit_comms, not for entire system, \ 
	as duplicated T2/T3s from same LS over eth and kit_comms will be mixedly forwarded and processed, to be improved";
	connInfo KitCommsRTConn;				// connection infos for kit Comms SBC, real time channel
	connInfo KitCommsHQConn;				// connection infos for kit Comms SBC, high quality channel
	bool useKitComms = false;
	uint32_t readKitCommsTimeout = KITCOMMS_READ_TIMEOUT;
	uint32_t selKitCommsTimeout = KITCOMMS_SEL_TIMEOUT;
	uint32_t reConnectTryLimit = 0; //0 = "unlimited try" as "pre-config" default value
	bool sendT3reqList = false;
		int IntSkipKitComms = 1; //1 here means: no skip
		int ExtMBiasSkipKitComms = 1; //1 here means: no skip, Mbias
			int subsecRangeMBias = 6; // +- (6-1)
		int ExtOthersSkipKitComms = 1; //1 here means: no skip, Other ext T3reqlist except Mbias
	static fd_set readKitCommsFDS;				// fd_set to check if a stations has send something to be read
	#define LSID_RANGE 256
	bool IsLsKitCommsArr[LSID_RANGE]; //array indexed by LS-ID
	#endif //KIT_COMMS
#endif
uint32_t lsCount = 0;						// station count of stations in array.conf
uint32_t lsConnectedCount = 0;				// count of connected stations

#ifndef USE_RC
char msgRcvBuff[MSG_RCVBUFF_LENGTH];
char msgSndBuff[MSG_SNDBUFF_LENGTH];
#endif

// for logging to file
FILE *pmlogf;
int loglevel = 0;
#define PMLOGBUFSIZE	(256*1024)			// if maybe a complete event has to be dumped...
static char logbuff[PMLOGBUFSIZE];
char pmlogfname[256];
int pmlogfindex;		// index of logfile, if it is broken on file size or log time
int pmLogFileMaxSize = AERA_FILE_MAX_SIZE;
bool useSyslog = false;

bool daqStarted = false;

time_t mainStartTime = 0;
time_t daqStartedTime = 0;
int actStatisticsInterval = ACT_STATISTIC_INTERVAL;
time_t lastActStatisticsTime = 0;

// for checking duplicate T2s
#define MAX_T2ARRAY_SIZE		10000
lst2 t2array[MAX_T2ARRAY_SIZE];
int t2arrayIndex = 0;
int duplicateCount = 0;

// JLB 30/01/13 Start
#define TEST_MAX_RUNTIME                3600 * 48 // maximum time in seconds
// JLB 30/01/13 End

// receive and send ALIVE/ALIVE_ACK according to AERA tcp/ip transport proposal
#define USE_AERA_TRANSPORT_DEFINITIONS
time_t aliveSentTimeT3 = 0;			// remembering, at what time an ALIVE was sent to T3
time_t aliveSentTimeEb = 0;			// remembering, at what time an ALIVE was sent to Eb
time_t aliveSentTimeGui = 0;		// remembering, at what time an ALIVE was sent to Gui
time_t aliveSentTimePrompt = 0;		// remembering, at what time an ALIVE was sent to Prompt
time_t aliveSentTimeLS[MAX_LS];		// remembering, at what time an ALIVE was sent to a local station

CountAndRate t2car;
CountAndRate t3car;
CountAndRate t2sNotSentCar;

CountAndRate lsT2Car[MAX_LS];			// count and rate of T2 per station
CountAndRate lsT3Car[MAX_LS];			// count and rate of T3 per station
CountAndRate lsDataCar[MAX_LS];			// count and rate of data per station
CountAndRate lsConnCar[MAX_LS];			// count and rate of connections established per station
CountAndRate lsDisConnCar[MAX_LS];			// count and rate of disconnections seen per station
CountAndRate lsDisConnTimesCar[MAX_LS];		// counting time intervals of disconnections
CountAndRate lsResetCar[MAX_LS];			// count and rate of resets made per station due to timeouts


uint32_t lastT2Second[MAX_LS];				// on receiving T2s from a stations, the received gpstime is saved here, to compare it with times of downgoing T3 requests
uint32_t actT3Second[MAX_LS];

/*
int t2sNotSentNoT3ConnCounter = 0;			// a counter of T2s not send due a missing T3maker, is resetted on a successful sent
time_t lastT2SentTime = 0;					// timestamp of the last T2 successfully sent
time_t lastT2NotSentMsgTime = 0;			// timestamp of last time the "T2 not sent" message was reported
int lastT2NotSentMsgTimeInterval = 10;		// timeinterval to sent the next "T2 not sent" message report
int lastT2NotSentMsgT2Interval = 10000;		// count of T2s, to sent the next "T2 not sent" message report
// => or just have a "Count and rate" for t3maker of T2s sent to T3maker?
*/

// helper functions

/**
 * @brief initialize the event builder log file - basically all llout0 and llout1 are also written to this log file
 */
void initPmLog() {
	llout0<<"initPmLog() ..."<<endl;
	sprintf(pmlogfname, "%s/%s/pm_%02d_%s.log", daqhome.c_str(),AERA_LOG_PATH, pmlogfindex, getTimeStringNowUS());
	pmlogf = fopen(pmlogfname, "w");
	if (pmlogf == NULL) {
		llout0<<"initPmLog() - failed to open log file "<<pmlogfname<<endl;
		llout0<<"trying to open logfile locally..."<<endl;
		sprintf(pmlogfname, "./pm_%02d_%s.log", pmlogfindex, getTimeStringNowUS());
		pmlogf = fopen(pmlogfname, "w");
		if (pmlogf == NULL) {
			llout0<<"initPmLog() - failed to open log file "<<pmlogfname<<"locally, logging via file skipped!"<<endl;
		}
	} else {
		fprintf(pmlogf, "%s - initPmLog() - opened log file %s\n", getTimeStringNow(), pmlogfname);
		llout0<<"initPmLog() - opened log file "<<pmlogfname<<endl;
		pmlogfindex++;
	}
	initLLogLevel (loglevel);
	initLLogFile (pmlogf);
	initLLogPrintTime(true);			// print time information in log output
	initLLogLogBufSize(PMLOGBUFSIZE);	// set size of logbuffer
}

void checkLogFileSize() {
	static bool statLogFails = false;
	static time_t lastLogOpened = 0, lastStatTime = 0;

	bool openNewLogfile = false;
	int retryStat = 600;				// try every 10 min again to stat the file
	time_t now;
	int filesize;
	struct stat stFileInfo;
	int ret=0;

	FUNC_BEGIN

//	pmLog("%s no postmaster log file %s openend yet!?", __FUNCTION__, pmlogfname);
	if (pmlogf == NULL) {
		llout0<<__FUNCTION__<<" no postmaster log file "<<pmlogfname<<" openend yet!?"<<endl;
		return;
	}
	now = time(NULL);
	if (lastStatTime == 0) lastStatTime = now;
	if (lastLogOpened == 0) lastLogOpened = now;
	if (!statLogFails || (statLogFails && ((difftime(now, lastStatTime) > retryStat)))) {			// && ((difftime(lastStatTime, now) > retryStat))) {
//		fprintf(pmlogf, "%s - checking size of pmlogfile %s\n", __FUNCTION__, pmlogfname);
//		pmLog("%s - checking size of pmlogfile %s\n", __FUNCTION__, pmlogfname);
//		llout0<<__FUNCTION__<<" - checking size of pmlogfile  "<<pmlogfname<<endl;
		lastStatTime = now;
		llout3<<__FUNCTION__<<"_1_"<<endl;

		if ((ret = stat(pmlogfname,&stFileInfo)) != 0) {
			statLogFails = true;
//			pmLog("%s : stat() on pm log file %s failed!?\n", __FUNCTION__, pmlogfname);
			fprintf(pmlogf, "%s - checkLogFileSize(): stat() on pm log file %s failed!?\n", getTimeStringNow(),  pmlogfname);
//			llout0<<"checkLogFileSize(): stat() on aera log file "<<pmlogfname<<" failed!?"<<endl;
			return;
		} else statLogFails = false;
		filesize = stFileInfo.st_size;

		llout3<<__FUNCTION__<<"_2_"<<endl;
		llout1<<"checkLogFileSize(): size of "<<pmlogfname<<" : "<<filesize<<", max file size: "<<pmLogFileMaxSize<<endl;
//			if ((filesize + sizeof(aera_event)) > AERA_FILE_MAX_SIZE) {		// not each added event has full size
		if ((filesize) > pmLogFileMaxSize) {
//			pmLog("checkLogFileSize(): size of %s : %d reached max file size: %d, open new log file... \n", pmlogfname, filesize, MAX_LOG_SIZE);
			fprintf(pmlogf, "%s - checkLogFileSize(): size of %s : %d reached max file size: %d, open new log file... \n", getTimeStringNow(),  pmlogfname, filesize, pmLogFileMaxSize);
//			llout0<<"checkLogFileSize(): size of "<<pmlogfname<<" : "<<filesize<<" reached max file size: "<<pmLogFileMaxSize<<", open new log file..."<<endl;
			openNewLogfile = true;
		}
	} else if (statLogFails && (difftime(now,lastLogOpened) > MAX_LOG_TIME)){	// if stat fails, use time to open a new file
//		pmLog("%s - checkLogFileSize(): opening time of %s : %d reached max file time: %d, open new log file... \n", pmlogfname, (int)lastLogOpened, MAX_LOG_TIME);
		fprintf(pmlogf, "%s - checkLogF(char*)&LsConn[counter].nameileSize(): opening time of %s : %d reached max file time: %d, open new log file... \n", getTimeStringNow(),  pmlogfname, (int)lastLogOpened, MAX_LOG_TIME);
//		llout0<<"checkLogFileSize(): opening time of "<<pmlogfname<<" : "<<lastLogOpened<<" reached max file size: "<<MAX_LOG_TIME<<", open new log file..."<<endl;
		openNewLogfile = true;
	}
	if (openNewLogfile) {
		lastLogOpened = now;
		fflush(pmlogf);
		fclose(pmlogf);
		initPmLog();
	}

	FUNC_END
}
/*		at the moment, this is only there for socklib. For Rc, it would be possible to dump a buffer to file
void writeMsgToFile(Msg* msg, char *filename) {
	if (msg == NULL) return;
	if (msg->length < 100) return;

	int written;
	char tfn[200];
	FILE* datfile;
	sprintf(tfn, "%s", filename);
//	sprintf(tfn, "%s/log/msg_%06d.dat", getenv("HOME"), writeCount);
	datfile = fopen(tfn, "w");
	if (!datfile) {
		printf("writeMsgToFile() - unable to open ls dat file %s\n", tfn);
		datfile = NULL;
		return;
	} else {
		printf("writeMsgToFile() - opened ls dat file %s\n", tfn);
		fprintf(pmlogf, "%s - %s - opened ls dat file %s\n", getTimeStringNow(), __FUNCTION__, tfn);
	}
	// dump the Msg to file
	if (datfile) {
		written =fwrite(msg, msg->length, 1, datfile);
		printf("writeMsgToFile() - written %d bytes to file %s\n", written, tfn);
		fprintf(pmlogf, "%s - %s - written %d bytes to file %s\n", getTimeStringNow(), __FUNCTION__, written, tfn);
	}
	if (datfile) fclose(datfile);
}
*/

// return the actual microsecond
uint32_t getMicrosecondNow() {
	struct timeval tv;
	struct timezone tz;
	gettimeofday(&tv, &tz);
//	struct tm *tm;
//	tm=localtime(&tv.tv_sec);
//	printf(" %d:%02d:%02d %d \n", tm->tm_hour, tm->tm_min, tm->tm_sec, tv.tv_usec);
	return tv.tv_usec;
}

// return the actual second - with the possibility to set timezone
uint32_t getSecondNow() {
	struct timeval tv;
	struct timezone tz;
	struct tm *tm;
	gettimeofday(&tv, &tz);
//	tm=localtime(&tv.tv_sec);
//	printf(" %d:%02d:%02d %d \n", tm->tm_hour, tm->tm_min, tm->tm_sec, tv.tv_usec);
	return tv.tv_sec;
}

// this is compare for simple arrays used in qsort - check the T2s of the actual second for duplicates
int compareT2(const void *int1, const void *int2) {
//	int t2_1 = *(int*)int1;
//	int t2_2 = *(int*)int2;
//	if ((*(int*)int1) <  (*(int*)int2)) return -1;
//	if ((*(int*)int1) == (*(int*)int2)) return 0;
//	if ((*(int*)int1) >  (*(int*)int2)) return 1;

	lst2 lst2_1 = *(lst2*)int1;
	lst2 lst2_2 = *(lst2*)int2;
//	printf("%s - have t2 1 : ls%2d : s = %8d, ns = %9d\n", __FUNCTION__, lst2_1.lsid, lst2_1.gpssecond, lst2_1.nsecond);
//	printf("%s - have t2 2 : ls%2d : s = %8d, ns = %9d\n", __FUNCTION__, lst2_2.lsid, lst2_2.gpssecond, lst2_2.nsecond);
	if (lst2_1.gpssecond < lst2_2.gpssecond)  return -1;
	else if (lst2_1.gpssecond == lst2_2.gpssecond) {
		if      (lst2_1.nsecond <  lst2_2.nsecond) return -1;
		else if (lst2_1.nsecond == lst2_2.nsecond) return 0;
		else if (lst2_1.nsecond >  lst2_2.nsecond) return 1;
	} //else if (lst2_1.gpssecond > lst2_2.gpssecond) {
		return 1;
//	}
}

void checkForDuplicateT2s() {
//	printf("%s - before qsort\n", __FUNCTION__);
//	for (int i=0; i<t2arrayIndex; i++) printf("%s - have t2 %3d : ls%2d : s = %8d, ns = %9d\n", __FUNCTION__, i, t2array[i].lsid, t2array[i].gpssecond, t2array[i].nsecond);

	qsort(&t2array, t2arrayIndex, sizeof(lst2), compareT2);
	// check for correct ordering
//	printf("%s - after qsort\n", __FUNCTION__);
	int multiplicityCount = 0;
	int i, k;
	llout0<<__FUNCTION__<<" - searching in "<<t2arrayIndex<<" T2s for duplicates ..."<<endl;
	for (i=0; i<t2arrayIndex; i++) {
//		printf("%s - have t2 %3d : ls%2d : s = %8d, ns = %9d\n", __FUNCTION__, i, t2array[i].lsid, t2array[i].gpssecond, t2array[i].nsecond);
		if (i>0) {
			if (t2array[i].gpssecond == t2array[i-1].gpssecond) {
				if (t2array[i].nsecond == t2array[i-1].nsecond) {
//					printf("%s - Found duplicate T2!!! have t2 %3d : ls %2d and ls %2d have same T2 : s = %8d, ns = %9d\n", __FUNCTION__, i, t2array[i].lsid, t2array[i-1].lsid, t2array[i].gpssecond, t2array[i].nsecond);
					multiplicityCount = 2;
					// Now we have 2 identical timestamps, check for more with same timestamp
					int nanos = t2array[i-1].nsecond;
					int sec =  t2array[i-1].gpssecond;
//					llout0<<__FUNCTION__<<" - have multiplicity, starting at index "<<(i+1)<<" and looping over "<<(t2arrayIndex-i)<<" remaining T2s  to find more equal ones"<<endl;
					for (k = (i+1); k<t2arrayIndex; k++) {		// loop over the remaining T2s till the end of array
//						printf("%s - i=%d, k = %d\n", __FUNCTION__, i, k);
						if (t2array[k].nsecond == nanos) {
							multiplicityCount++;
//							printf("%s - duplicate T2: t2 %3d of ls %2d has same T2 : s = %8d, ns = %9d, multiplicity = %d\n", __FUNCTION__, k, t2array[k].lsid, t2array[k].gpssecond, t2array[k].nsecond, multiplicityCount);
						} else {	// no equal T2s, but new ones
//							llout0<<__FUNCTION__<<" - new T2"<<endl;
//							printf("%s : t2 %3d of ls %2d has T2 : s = %8d, ns = %9d, the first different after the multiplicity \n", __FUNCTION__, k, t2array[k].lsid, t2array[k].gpssecond, t2array[k].nsecond);
//TODO: WHY IS SOMETIMES SEARCHING NOT STOPPED BUT LOOKS LIKE CONTINUEING FROM THE BEGINNING?
							if (multiplicityCount > 2) {
								duplicateCount++;
								printf("%s - T3 number %d? Duplicate T2 had multiplicity %d!!! second = %8d, ns = %9d\n", __FUNCTION__, duplicateCount, multiplicityCount, sec, nanos);
								if (multiplicityCount > MAX_LS) {
									llout0<<__FUNCTION__<<" - ERROR - BigBug: multiplicity "<<multiplicityCount<<"higher than max count of stations: "<<MAX_LS<<endl;
								}
							}
							i = k;	// the one already different is compared with (i-1) again in the outer check loop
//							printf("%s - i = (k) = %d - continue check at T2[%d]\n", __FUNCTION__, k, i);
							break;
						}
					}
				} else {
					multiplicityCount = 0;
//					llout0<<__FUNCTION__<<" - following nano seconds do not match"<<endl;
				}
			}
		}
	}
}

// empty the vector
void emptyT2Vector() {
	t2arrayIndex = 0;
}

// ##################### 						POSTMASTER CODE 				###################################
#ifndef USE_RC
// whatever you think you need to check ...
int isSocketError(int sockfd) {
	if (sockfd == 0) return 0;

	int sockerr = errno;

//	perror("isSocketError");
//	printf("%s - have errno %d \n", __FUNCTION__, sockerr);

	switch (sockerr) {
		case EBADF:
		case ENETDOWN:
		case EPIPE:
		case ECONNREFUSED:
		case EAGAIN:
		case ENOTCONN:
			pmLog("%s - have socket errno %d\n", __FUNCTION__, sockerr);
//			printf("%s - have socket errno %d \n", __FUNCTION__, sockerr);
//			fprintf(pmlogf, "%s - %s - have socket errno %d\n", getTimeStringNow(), __FUNCTION__, sockerr);
			return 1;
			break;
		default:
//			printf("%s - error %d not a reason to disconnect the socket ...\n", __FUNCTION__, sockerr);
			break;
	}
//   if (sockerr > 0) return 1;		// do not consider all errno here, but only network errors
    return sockerr;
}
#endif

FILE* openConfigFile(const char *mode) {
	llout0<<__FUNCTION__<<" - in mode "<<mode<<endl;

// looking for HOME
	char confFilePath[200];			// 200 should be enough for a path like: /home/daq/conf/pm_init
	sprintf(confFilePath, "%s", getenv("HOME"));
	int n = strlen(confFilePath);
	llout0<<__FUNCTION__<<" - have HOME: "<<confFilePath<<" with length "<<n<<endl;
	sprintf((char*)&confFilePath[n], "%s", pmConfFileName);

	llout0<<__FUNCTION__<<" - opening config file "<<confFilePath<<endl;
	FILE *pCfgFile = fopen(confFilePath, mode);
	if (!pCfgFile) {
		llout0<<__FUNCTION__<<" - unable to open config file "<<confFilePath<<endl;
		return NULL;
	} else llout0<<__FUNCTION__<<" - config file "<<confFilePath<<" openend"<<endl;
	return pCfgFile;
}

// at the beginning, log isn't available in this function!
void readConfigFile() {		// parse the complete file and set all found parameters
	int lineLength = 200;
	int val = 0;
	char aline[lineLength];
	llout0<<"readConfigFile() - reading config file ..."<<endl;
	while (fgets(aline, lineLength, confFile) != NULL) {
		string sline(aline);
//		llout0<<"readConfigFile() - read line: >>"<<sline<<"<<"<<endl;
		if (sline[0] ==  '#' ) continue;		// ignore comments
		else {
//			printf("read line : %s", aline);
			string subLine, restLine;
			if ((subLine = sline.substr(0, strlen("autoStartDaq"))) ==  "autoStartDaq" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter autoStartDaq with value "<<val<<",ignored, autoStartDaq set = false"<<endl;
					autoStartDaq = false;
					daqStarted  = false;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter autoStartDaq with value "<<val<<", autoStartDaq set = false"<<endl;
					autoStartDaq = false;
					daqStarted  = false;
				} else if (val >= 1) {
					autoStartDaq = true;
					daqStarted  = true;
					llout0<<"readConfigFile() - found parameter autoStartDaq with value "<<autoStartDaq<<" , autoStartDaq set = true"<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("pmLogFileMaxSize"))) ==  "pmLogFileMaxSize" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				pmLogFileMaxSize = atoi(restLine.c_str());
				if (pmLogFileMaxSize <= 0) {
					llout0<<"aeraEventBuilder::readConfigFile() - found parameter fileMaxSize with invalid value "<<pmLogFileMaxSize<<", setting to "<<MAX_LOG_SIZE<<endl;
					pmLogFileMaxSize = MAX_LOG_SIZE;
//					llout0<<"aeraEventBuilder::readConfigFile() - found parameter fileMaxSize with invalid value "<<pmLogFileMaxSize<<", exiting"<<endl;
//					terminate();
				} else {
					llout0<<"aeraEventBuilder::readConfigFile() - found parameter fileMaxSize with value "<<pmLogFileMaxSize<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("writeT2File"))) ==  "writeT2File" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter writeT2File with value "<<val<<",ignored, writeT2File set = false"<<endl;
					writeT2File = false;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter writeT2File with value "<<val<<", writeT2File set = false"<<endl;
					writeT2File = false;
				} else if (val == 1) {
					writeT2File = true;
					writeT2FileInAscii = false;
					llout0<<"readConfigFile() - found parameter writeT2File with value "<<writeT2File<<" , writeT2File set = true, writeT2FileInAscii = false"<<endl;
				} else if (val > 1) {
					writeT2File = true;
					writeT2FileInAscii = true;
					llout0<<"readConfigFile() - found parameter writeT2File with value "<<writeT2File<<" , writeT2File set = true, writeT2FileInAscii = true"<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("writeT3File"))) ==  "writeT3File" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter writeT3File with value "<<val<<",ignored, writeT3File set = false"<<endl;
					writeT3File = false;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter writeT3File with value "<<val<<", writeT3File set = false"<<endl;
					writeT3File = false;
				} else if (val == 1) {
					writeT3File = true;
					writeT3FileInAscii = false;
					llout0<<"readConfigFile() - follout0parameter writeT3File with value "<<writeT3File<<" , writeT3File set = true, writeT3FileInAscii = false"<<endl;
				} else if (val > 1) {
					writeT3File = true;
					writeT3FileInAscii = true;
					llout0<<"readConfigFile() - follout0parameter writeT3File with value "<<writeT3File<<" , writeT3File set = true, writeT3FileInAscii = true"<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("useT3"))) ==  "useT3" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter useT3 with value "<<val<<",ignored, useT3 set = false"<<endl;
					useT3 = false;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter useT3 with value "<<val<<", useT3 set = false"<<endl;
					useT3 = false;
				} else if (val >= 1) {
					useT3 = true;
					llout0<<"readConfigFile() - found parameter useT3 with value "<<useT3<<" , useT3 set = true"<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("useEb"))) ==  "useEb" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter useEb with value "<<val<<",ignored, useEb set = false"<<endl;
					useEb = false;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter useEb with value "<<val<<", useEb set = false"<<endl;
					useEb = false;
				} else if (val >= 1) {
					useEb = true;
					llout0<<"readConfigFile() - found parameter useEb with value "<<useEb<<" , useEb set = true"<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("useGui"))) ==  "useGui" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter useGui with value "<<val<<",ignored, useGui set = false"<<endl;
					useGui = false;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter useGui with value "<<val<<", useGui set = false"<<endl;
					useGui = false;
				} else if (val >= 1) {
					useGui = true;
					llout0<<"readConfigFile() - found parameter useGui with value "<<useGui<<" , useGui set = true"<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("usePr"))) ==  "usePr" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter usePr with value "<<val<<",ignored, usePr set = false"<<endl;
					usePr = false;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter usePr with value "<<val<<", usePr set = false"<<endl;
					usePr = false;
				} else if (val >= 1) {
					usePr = true;
					llout0<<"readConfigFile() - found parameter usePr with value "<<usePr<<" , usePr set = true"<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("useKitComms"))) ==  "useKitComms" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter useKitComms with value "<<val<<",ignored, useKitComms set = false"<<endl;
					useKitComms = false;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter useKitComms with value "<<val<<", useKitComms set = false"<<endl;
					useKitComms = false;
				} else if (val >= 1) {
					useKitComms = true;
					llout0<<"readConfigFile() - found parameter useKitComms with value "<<useKitComms<<" , useKitComms set = true"<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("noMsgConnTimeout"))) ==  "noMsgConnTimeout" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter noMsgConnTimeout with value "<<val<<",ignored, noMsgConnTimeout set = "<<NO_MSG_CONN_TIMEOUT<<endl;
					noMsgConnTimeout = NO_MSG_CONN_TIMEOUT;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter noMsgConnTimeout with value "<<val<<", noMsgConnTimeout set = "<<NO_MSG_CONN_TIMEOUT<<endl;
					noMsgConnTimeout = NO_MSG_CONN_TIMEOUT;
				} else if (val >= 1) {
					noMsgConnTimeout = val;
					llout0<<"readConfigFile() - found parameter noMsgConnTimeout with value "<<noMsgConnTimeout<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("selLsTimeout"))) ==  "selLsTimeout" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter selLsTimeout with value "<<val<<",ignored, selLsTimeout set = "<<LS_SEL_TIMEOUT<<endl;
					selLsTimeout = LS_SEL_TIMEOUT; //corrected YZ
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter selLsTimeout with value "<<val<<", selLsTimeout set = "<<LS_SEL_TIMEOUT<<endl;
					selLsTimeout = LS_SEL_TIMEOUT;
				} else if (val >= 1) {
					selLsTimeout = val;
					llout0<<"readConfigFile() - found parameter selLsTimeout with value "<<selLsTimeout<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("readLsTimeout"))) ==  "readLsTimeout" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter readLsTimeout with value "<<val<<",ignored, readLsTimeout set = "<<LS_READ_TIMEOUT<<endl;
					readLsTimeout = LS_READ_TIMEOUT;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter readLsTimeout with value "<<val<<", readLsTimeout set = "<<LS_READ_TIMEOUT<<endl;
					readLsTimeout = LS_READ_TIMEOUT;
				} else if (val >= 1) {
					readLsTimeout = val;
					llout0<<"readConfigFile() - found parameter readLsTimeout with value "<<readLsTimeout<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("readT3Timeout"))) ==  "readT3Timeout" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter readT3Timeout with value "<<val<<",ignored, readT3Timeout set = "<<T3_READ_TIMEOUT<<endl;
					readT3Timeout = T3_READ_TIMEOUT;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter readT3Timeout with value "<<val<<", readT3Timeout set = "<<T3_READ_TIMEOUT<<endl;
					readT3Timeout = T3_READ_TIMEOUT;
				} else if (val >= 1) {
					readT3Timeout = val;
					llout0<<"readConfigFile() - found parameter readT3Timeout with value "<<readT3Timeout<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("readEbTimeout"))) ==  "readEbTimeout" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter readEbTimeout with value "<<val<<",ignored, readEbTimeout set = "<<EB_READ_TIMEOUT<<endl;
					readEbTimeout = EB_READ_TIMEOUT;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter readEbTimeout with value "<<val<<", readEbTimeout set = "<<EB_READ_TIMEOUT<<endl;
					readEbTimeout = EB_READ_TIMEOUT;
				} else if (val >= 1) {
					readEbTimeout = val;
					llout0<<"readConfigFile() - found parameter readEbTimeout with value "<<readEbTimeout<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("readGuiTimeout"))) ==  "readGuiTimeout" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter readGuiTimeout with value "<<val<<",ignored, readGuiTimeout set = "<<GUI_READ_TIMEOUT<<endl;
					readGuiTimeout = GUI_READ_TIMEOUT;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter readGuiTimeout with value "<<val<<", readGuiTimeout set = "<<GUI_READ_TIMEOUT<<endl;
					readGuiTimeout = GUI_READ_TIMEOUT;
				} else if (val >= 1) {
					readGuiTimeout = val;
					llout0<<"readConfigFile() - found parameter readGuiTimeout with value "<<readGuiTimeout<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("readPrTimeout"))) ==  "readPrTimeout" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter readPrTimeout with value "<<val<<",ignored, readPrTimeout set = "<<PR_READ_TIMEOUT<<endl;
					readPrTimeout = PR_READ_TIMEOUT;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter readPrTimeout with value "<<val<<", readPrTimeout set = "<<PR_READ_TIMEOUT<<endl;
					readPrTimeout = PR_READ_TIMEOUT;
				} else if (val >= 1) {
					readPrTimeout = val;
					llout0<<"readConfigFile() - found parameter readPrTimeout with value "<<readPrTimeout<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("Eb"))) ==  "Eb" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				sprintf(EbConn.name, "%s", "Eb");
				llout0<<"restLine: "<<restLine<<endl;
				// read IP
				sscanf(restLine.c_str(), " %s %d", (char*)&EbConn.ip, &EbConn.port);
				llout0<<"readConfigFile() - read Eb IP "<<EbConn.ip<<" and port "<<EbConn.port<<endl;
				continue;
			}
			if ((subLine = sline.substr(0, strlen("T3"))) ==  "T3" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				sprintf(T3Conn.name, "%s", "T3");
				llout0<<"restLine: "<<restLine<<endl;
				// read IP
				sscanf(restLine.c_str(), " %s %d", (char*)&T3Conn.ip, &T3Conn.port);
				llout0<<"readConfigFile() - read T3 IP "<<T3Conn.ip<<" and port "<<T3Conn.port<<endl;
				continue;
			}
			if ((subLine = sline.substr(0, strlen("Pr"))) ==  "Pr" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				sprintf(PromptConn.name, "%s", "Pr");
				llout0<<"restLine: "<<restLine<<endl;
				// read IP
				sscanf(restLine.c_str(), " %s %d", (char*)&PromptConn.ip, &PromptConn.port);
				llout0<<"readConfigFile() - read Prompt IP "<<PromptConn.ip<<" and port "<<PromptConn.port<<endl;
				continue;
			}
			if ((subLine = sline.substr(0, strlen("Gui"))) ==  "Gui" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				sprintf(GuiConn.name, "%s", "Gui");
				llout0<<"restLine: "<<restLine<<endl;
				// read IP
				sscanf(restLine.c_str(), " %s %d", (char*)&GuiConn.ip, &GuiConn.port);
				llout0<<"readConfigFile() - read Gui IP "<<EbConn.ip<<" and port "<<GuiConn.port<<endl;
				continue;
			}
			if ((subLine = sline.substr(0, strlen("loglevel"))) ==  "loglevel" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<__FUNCTION__<<" - found parameter loglevel with invalid value "<<val<<", using default value "<<loglevel<<endl;
//					syslog(LOG_INFO, "readConfigFile() - found parameter loglevel with invalid value %d, using default value %d", val, loglevel);
				} else {
					initLLogLevel(val);
					llout0<<__FUNCTION__<<" - found parameter loglevel with value "<<loglevel<<endl;
//					syslog(LOG_INFO, "readConfigFile() - found parameter loglevel with value %d", loglevel);
				}
				continue;
			}
#ifdef KIT_COMMS //load KIT_COMMS connection info and other parameters
			if ((subLine = sline.substr(0, strlen("KitCommsRTConn"))) ==  "KitCommsRTConn" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				sprintf(KitCommsRTConn.name, "%s", "KitCommsRTConn");
				llout0<<"restLine: "<<restLine<<endl;
				// read IP
				sscanf(restLine.c_str(), " %s %d", (char*)&KitCommsRTConn.ip, &KitCommsRTConn.port);
				llout0<<"readConfigFile() - read KitCommsRTConn IP "<<KitCommsRTConn.ip<<" and port "<<KitCommsRTConn.port<<endl;
				continue;
			}
			if ((subLine = sline.substr(0, strlen("KitCommsHQConn"))) ==  "KitCommsHQConn" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				sprintf(KitCommsHQConn.name, "%s", "KitCommsHQConn");
				llout0<<"restLine: "<<restLine<<endl;
				// read IP
				sscanf(restLine.c_str(), " %s %d", (char*)&KitCommsHQConn.ip, &KitCommsHQConn.port);
				llout0<<"readConfigFile() - read KitCommsHQConn IP "<<KitCommsHQConn.ip<<" and port "<<KitCommsHQConn.port<<endl;
				continue;
			}
			if ((subLine = sline.substr(0, strlen("useKitComms"))) ==  "useKitComms" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter useKitComms with value "<<val<<",ignored, useKitComms set = false"<<endl;
					useKitComms = false;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter useKitComms with value "<<val<<", useKitComms set = false"<<endl;
					useKitComms = false;
				} else if (val >= 1) {
					useKitComms = true;
					llout0<<"readConfigFile() - found parameter useKitComms with value "<<useKitComms<<" , useKitComms set = true"<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("selKitCommsTimeout"))) ==  "selKitCommsTimeout" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter selKitCommsTimeout with value "<<val<<",ignored, selKitCommsTimeout set = "<<KITCOMMS_READ_TIMEOUT<<endl;
					selKitCommsTimeout = KITCOMMS_SEL_TIMEOUT;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter selKitCommsTimeout with value "<<val<<", selKitCommsTimeout set = "<<KITCOMMS_READ_TIMEOUT<<endl;
					selKitCommsTimeout = KITCOMMS_SEL_TIMEOUT;
				} else if (val >= 1) {
					selKitCommsTimeout = val;
					llout0<<"readConfigFile() - found parameter selKitCommsTimeout with value "<<selKitCommsTimeout<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("readKitCommsTimeout"))) ==  "readKitCommsTimeout" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter readKitCommsTimeout with value "<<val<<",ignored, readKitCommsTimeout set = "<<KITCOMMS_READ_TIMEOUT<<endl;
					readKitCommsTimeout = KITCOMMS_READ_TIMEOUT;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter readKitCommsTimeout with value "<<val<<", readKitCommsTimeout set = "<<KITCOMMS_READ_TIMEOUT<<endl;
					readKitCommsTimeout = KITCOMMS_READ_TIMEOUT;
				} else if (val >= 1) {
					readKitCommsTimeout = val;
					llout0<<"readConfigFile() - found parameter readKitCommsTimeout with value "<<readKitCommsTimeout<<endl;
				}
				continue;
			}
			//some "user" parameters, would be useful for test and operational use. If you don't like it or find them tedious, just remove also in pm.init
			if ((subLine = sline.substr(0, strlen("reConnectTryLimit"))) ==  "reConnectTryLimit" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter reConnectTryLimit with value "<<val<<",ignored, reConnectTryLimit set = false"<<endl;
					reConnectTryLimit = 0; //0 for always try
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter reConnectTryLimit with value "<<val<<", reConnectTryLimit set = false"<<endl;
					reConnectTryLimit = 0;
				} else if (val >= 1) {
					reConnectTryLimit = val;
					llout0<<"readConfigFile() - found parameter reConnectTryLimit with value "<<reConnectTryLimit<<" , reConnectTryLimit set = true"<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("sendT3reqList"))) ==  "sendT3reqList" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter sendT3reqList with value "<<val<<",ignored, sendT3reqList set = false"<<endl;
					sendT3reqList = false;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter sendT3reqList with value "<<val<<", sendT3reqList set = false"<<endl;
					sendT3reqList = false;
				} else if (val >= 1) {
					sendT3reqList = true;
					llout0<<"readConfigFile() - found parameter sendT3reqList with value "<<sendT3reqList<<" , sendT3reqList set = true"<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("IntSkipKitComms"))) ==  "IntSkipKitComms" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter IntSkipKitComms with value "<<val<<",ignored, IntSkipKitComms set = false"<<endl;
					IntSkipKitComms = 1; //1 for no skip
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter IntSkipKitComms with value "<<val<<", IntSkipKitComms set = false"<<endl;
					IntSkipKitComms = 0; //0 for deactivate
				} else if (val >= 1) {
					IntSkipKitComms = val;
					llout0<<"readConfigFile() - found parameter IntSkipKitComms with value "<<IntSkipKitComms<<" , IntSkipKitComms set = true"<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("ExtMBiasSkipKitComms"))) ==  "ExtMBiasSkipKitComms" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter ExtMBiasSkipKitComms with value "<<val<<",ignored, ExtMBiasSkipKitComms set = false"<<endl;
					ExtMBiasSkipKitComms = 1; //1 for no skip
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter ExtMBiasSkipKitComms with value "<<val<<", ExtMBiasSkipKitComms set = false"<<endl;
					ExtMBiasSkipKitComms = 0; //0 for deactivate
				} else if (val >= 1) {
					ExtMBiasSkipKitComms = val;
					llout0<<"readConfigFile() - found parameter ExtMBiasSkipKitComms with value "<<ExtMBiasSkipKitComms<<" , ExtMBiasSkipKitComms set = true"<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("ExtOthersSkipKitComms"))) ==  "ExtOthersSkipKitComms" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter ExtOthersSkipKitComms with value "<<val<<",ignored, ExtOthersSkipKitComms set = false"<<endl;
					ExtOthersSkipKitComms = 1; //1 for no skip
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter ExtOthersSkipKitComms with value "<<val<<", ExtOthersSkipKitComms set = false"<<endl;
					ExtOthersSkipKitComms = 0; //0 for deactivate
				} else if (val >= 1) {
					ExtOthersSkipKitComms = val;
					llout0<<"readConfigFile() - found parameter ExtOthersSkipKitComms with value "<<ExtOthersSkipKitComms<<" , ExtOthersSkipKitComms set = true"<<endl;
				}
				continue;
			}
			if ((subLine = sline.substr(0, strlen("subsecRangeMBias"))) ==  "subsecRangeMBias" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<"readConfigFile() - found invalid parameter subsecRangeMBias with value "<<val<<",ignored, subsecRangeMBias set = false"<<endl;
					subsecRangeMBias = 6;
				} else if (val == 0) {
					llout0<<"readConfigFile() - found parameter subsecRangeMBias with value "<<val<<", subsecRangeMBias set = false"<<endl;
					subsecRangeMBias = 0;
				} else if (val >= 1) {
					subsecRangeMBias = val;
					llout0<<"readConfigFile() - found parameter subsecRangeMBias with value "<<subsecRangeMBias<<" , subsecRangeMBias set = true"<<endl;
				}
				continue;
			}
#endif
			if ((subLine = sline.substr(0, strlen("useDB"))) ==  "useDB" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				val = atoi(restLine.c_str());
				if (val < 0) {
					llout0<<__FUNCTION__<<" - readConfigFile() - found parameter useDB with invalid value "<<val<<" <0, exiting"<<endl;
//					syslog(LOG_EMERG, "%s - readConfigFile() - found parameter useDB with invalid value %d <0, exiting", __FUNCTION__, val);
					terminatePM(SIGTERM);
				} else {
					llout1<<__FUNCTION__<<" - readConfigFile() - found parameter useDB with value "<<val<<endl;
//					syslog(LOG_INFO, "%s - readConfigFile() - found parameter useDB with value %d", __FUNCTION__, val);
					if (val == 1) useDB = true;
					else useDB = false;
				}
				continue;
			}
		}
	}
}

FILE* openArrayFile(const char *mode) {
	llout0<<__FUNCTION__<<" - in mode "<<mode<<endl;

// looking for HOME
	char arrayFilePath[200];			// 200 should be enough for a path like: /home/daq/conf/array.conf
	sprintf(arrayFilePath, "%s", getenv("HOME"));
	int n = strlen(arrayFilePath);
	llout0<<__FUNCTION__<<" - have HOME: "<<arrayFilePath<<" with length "<<n<<endl;
	sprintf((char*)&arrayFilePath[n], "%s", arrayFileName);

	llout0<<__FUNCTION__<<" - opening array file "<<arrayFilePath<<endl;
	FILE *pCfgFile = fopen(arrayFilePath, mode);
	if (!pCfgFile) {
		llout0<<__FUNCTION__<<" - unable to open array file "<<arrayFilePath<<endl;
		return NULL;
	} else llout0<<__FUNCTION__<<" - array file "<<arrayFilePath<<" openend\n";
	return pCfgFile;
}

void readArrayFile() {	// parse the complete file and fill station connection infos
		int lineLength = 200;
		int counter = 0;
		char aline[lineLength];
		string subLine, restLine;
		char carn[MAX_CAR_NAME];

		llout0<<"readArrayFile() - reading array file ..."<<endl;
		while (fgets(aline, lineLength, arrayFile) != NULL) {
			llout0<<"Line "<<counter<<" :"<<aline<<endl;
			string sline(aline);
			if (sline[0] ==  '#' ) continue;		// ignore comments
			if (sline.length() <=1) continue;
			if ((subLine = sline.substr(0, strlen("version"))) ==  "version" ) {
				restLine = sline.substr(subLine.length() + 1, sline.length() - subLine.length() -1 );
				llout0<<"array file has version "<<restLine<<endl;
				continue;
			}
			sscanf(aline, "%hu %s %s %d", &LsConn[counter].lsid, (char*)&LsConn[counter].name, (char*)&LsConn[counter].ip, &LsConn[counter].port);
			counter++;
			if (counter > MAX_LS) {
				llout0<<"readArrayFile() : reached maximum number of stations defined in postmaster.h! Skipping rest of array file..."<<endl;
				break;
			}
			lsCount = counter;
		}
		llout0<<"readArrayFile() : found "<<lsCount<<" stations in array file..."<<endl;

		llout0<<"Found stations are:"<<endl;
		for (uint32_t i=0; i<lsCount;i++) {
			llout0<<"Station "<<i<<" has ID/Name/IP/Port : "<<LsConn[i].lsid<<"/"<<LsConn[i].name<<"/"<<LsConn[i].ip<<"/"<<LsConn[i].port<<""<<endl;

			sprintf(carn, "%s - T2 statistics                 ", LsConn[i].name);
			lsT2Car[i].init(carn);
			sprintf(carn, "%s - T3 statistics                 ", LsConn[i].name);
			lsT3Car[i].init(carn);
			sprintf(carn, "%s - data statistics               ", LsConn[i].name);
			lsDataCar[i].init(carn);
			sprintf(carn, "%s - connection statistics         ", LsConn[i].name);
			lsConnCar[i].init(carn);
			sprintf(carn, "%s - disconnection statistics      ", LsConn[i].name);
			lsDisConnCar[i].init(carn);
			sprintf(carn, "%s - disconnected times statistics ", LsConn[i].name);
			lsDisConnTimesCar[i].init(carn);
			sprintf(carn, "%s - reset (msg timeout) statistics", LsConn[i].name);
			lsResetCar[i].init(carn);
		}
}

// open one per station
// TODO: move t2file FILE* into connInfo as a logfile?
void openT2Files() {
	char tfn[200];
	for (int i=0; i<MAX_LS;i++) {
#ifdef USE_RC
		if (LsConn[i].lsid != 0 && LsConn[i].sock != NULL) {	// open only for those initialized AND connected
#else
		if (LsConn[i].lsid != 0 && (LsConn[i].sock != 0 || LsConn[i].port == 0)) { // open only for those initialized AND connected //Changed for KITCOMMS
#endif
			sprintf(tfn, "%s/%s%03d_%s_%s", getenv("HOME"), t2FileNameHead, LsConn[i].lsid, getTimeStringNowUS(), t2FileNameTail);
			llout0<<"Index "<<i<<" is LS "<<LsConn[i].lsid<<" => build t2 file name : "<<tfn<<", trying to open t2 file..."<<endl;
			// existing files are overwritten!
			t2Files[i] = fopen(tfn, "w");
			if (!t2Files[i]) {
				llout0<<__FUNCTION__<<" - unable to open t2 file "<<tfn<<endl;
				t2Files[i] = NULL;
			} else 	llout0<<__FUNCTION__<<" - openend t2 file "<<tfn<<" succesfully\n";

		}
	}
}

void closeT2Files() {
	for (uint32_t i=0; i<MAX_LS; i++) {
		if (t2Files[i] != NULL) {
			llout0<<__FUNCTION__<<" - closing t2file at index "<<i<<"...\n";
			fflush(t2Files[i]);
			fclose(t2Files[i]);
			t2Files[i] = NULL;
		}
	}
}

void flushT2Files() {
//	llout0<<__FUNCTION__<<" - flushing t2files ...\n";
	for (uint32_t i=0; i<MAX_LS; i++) {
		if (t2Files[i] != NULL) {
//			llout0<<__FUNCTION__<<" - flushing t2file at index "<<i<<"...\n";
			fflush(t2Files[i]);
		}
	}
}

// open one per station
void openT3Files() {
	char tfn[200];
	for (int i=0; i<MAX_LS;i++) {
		if (LsConn[i].lsid != 0 && (LsConn[i].sock != 0 || LsConn[i].port == 0)) {	// open only for those initialized AND connected Changed for KITCOMMS
			sprintf(tfn, "%s/%s%03d_%s_%s", getenv("HOME"), t3FileNameHead, LsConn[i].lsid, getTimeStringNowUS(), t3FileNameTail);
			llout0<<"Index "<<i<<" is LS "<<LsConn[i].lsid<<" => build t3 file name : "<<tfn<<", trying to open t3 file..."<<endl;
			// existing files are overwritten!
			t3Files[i] = fopen(tfn, "w");
			if (!t3Files[i]) {
				llout0<<__FUNCTION__<<" - unable to open t3 file "<<tfn<<"\n";
				t3Files[i] = NULL;
			} else 	llout0<<__FUNCTION__<<" - openend t3 file "<<tfn<<" succesfully\n";

		}
	}
}

void closeT3Files() {
	for (uint32_t i=0; i<MAX_LS; i++) {
		if (t3Files[i] != NULL) {
			llout0<<__FUNCTION__<<" - closing t3file at index "<<i<<"...\n";
			fflush(t3Files[i]);
			fclose(t3Files[i]);
			t3Files[i] = NULL;
		}
	}
}


void initPostmaster() {
#ifndef USE_RC
	for (int i=0;i<MAX_LS;i++) {
		resetConnInfo(&LsConn[i]);
	}	
	#ifdef KIT_COMMS
		resetConnInfo(&KitCommsRTConn);
		resetConnInfo(&KitCommsHQConn);
		for (int j=0; j<LSID_RANGE; j++) {
			IsLsKitCommsArr[j] = false;
		}
	#endif //KIT_COMMS
	resetConnInfo((connInfo*)&EbConn);
	dumpConnInfo((connInfo*)&EbConn);
	resetConnInfo((connInfo*)&T3Conn);
	resetConnInfo((connInfo*)&GuiConn);
	resetConnInfo((connInfo*)&PromptConn);
#endif

	char *ctmp = getenv("HOME");
	if (ctmp != NULL) {
		daqhome = ctmp;
		llout0<<"initPostmaster() - got HOME : "<<daqhome<<endl;
	} else {
		daqhome = DAQHOME;
		llout0<<"initPostmaster() - getenv(HOME) failed, using default : "<<daqhome<<endl;
	}
	initPmLog();
	if (pmlogf == NULL) {
		cerr<<__FUNCTION__<<" - could not open logfile => exiting"<<endl;
		terminatePM(SIGABRT);
	}

	t2car.init("T2 ");
	t2sNotSentCar.init("T2sNotSent ");
	t3car.init("T3 ");
	char n[MAX_CAR_NAME];
	for (int i=0; i < MAX_LS; i++) {
		lsT2Car[i].init("");
		lsT3Car[i].init("");
		lsDataCar[i].init("");
		lsConnCar[i].init("");
		lsDisConnCar[i].init("");
		lsDisConnTimesCar[i].init("");
		lsResetCar[i].init("");
		lastT2Second[i] = 0;
		actT3Second[i] = 0;

		aliveSentTimeLS[MAX_LS] = 0;
		if (writeT2File == true) t2Files[i] = NULL;
		if (writeT3File == true) t3Files[i] = NULL;
	}

#ifdef USE_MYSQL
	// connect to the mysql DB, to write the status, ...
	if (useDB) {
		if (connectToDB()) {
			pmLog("%s - %s - connection to DB established\n", getTimeStringNow(), __FUNCTION__);
			llout0<<__FUNCTION__<<" - connection to DB established"<<endl;
//			syslog(LOG_INFO, "%s - connection to DB established", __FUNCTION__);
		} else {
			pmLog("%s - %s - failed to connect to DB\n", getTimeStringNow(), __FUNCTION__);
			llout0<<__FUNCTION__<<" - - failed to connect to DB"<<endl;
//			syslog(LOG_INFO, "%s - failed to connect to DB", __FUNCTION__);
		}
	}
#endif		// #ifdef USE_MYSQL
}

#ifdef USE_RC
bool connectToStation(rcConnInfo* ci) {
//****** JLB 26/09/2012 + 19/11/12 start
#ifndef USE_RC6
	if ((ci->sock = RcSocketNew( ci->name,		// open client connection to stations
                RcSocketRecoveryExtended,
		ci->ip,
		ci->port,
        PMSV_OUTPUT_BUFFER_SIZE,
        INPUT_FIFO_SIZE,
		INPUT_QUICKLOOK,
		PMSV_INPUT_BUFFER_SIZE,
		OUTPUT_FIFO_SIZE,
		OUTPUT_QUICKLOOK)) == NULL) {
#else	// this is for USE_RC6
	if ((ci->sock = RcSocketNew( ci->name,		// open client connection to stations
                0x0101, // JLBY 24/09/12 + 19/11/12 Group #1.
		ci->ip, ci->port,
                LSPM_BUFFER_SIZE, INPUT_FIFO_SIZE,
		TIMEOUT,
		PMLS_BUFFER_SIZE, OUTPUT_FIFO_SIZE,
		SELECT_TIMEOUT)) == NULL) {
#endif
//****** JLB 26/09/2012 + 19/11/12 start

		pmLog("connectToStation(): RcSocketNew() for port %d failed!!\n", ci->port);
//		printf("connectToStation(): RcSocketNew() for port %d failed!!\n", ci->port);
		ci->sock = NULL;
		return false;
	}
	pmLog("%s - RcSocketNew() for %s on %s:%d successfully.\n", __FUNCTION__, ci->name, ci->ip, ci->port);

//	llout0<<__FUNCTION__<<" - RcSocketNew() for "<<ci->name<<" on "<<ci->ip<<":"<<ci->port<<" successfully.\n";
//	fprintf(pmlogf, "%s - %s - RcSocketNew() for %s on %s:%d successfully.\n", getTimeStringNow(), __FUNCTION__, ci->name, ci->ip, ci->port);

	lsConnectedCount++;
//	dumpSockOpt(ci->sock);
	return true;
}
#else
bool connectToStation(connInfo* ci) {
#ifdef KIT_COMMS
	//fake connection! deceive to be "connected". the ip can be whatever, port is 0
	//But can this fake connection coexist with all configs (e.g. autoDAQ)?...to be 
	//if (strncmp (ci->name, "KITCOMMS", 8) == 0) {
	if (ci->port == 0) {
		ci->sock = 0; //not real connection, ensures no harm could be done

		llout0<<__FUNCTION__<<" - NO OpenInetClient() for "<<ci->name<<" on "<<ci->ip<<":"<<ci->port<<" . fake connection, socket "<<ci->sock<<endl;
		fprintf(pmlogf, "%s - %s - NO OpenInetClient() for %s on %s:%d successfully. fake connection, socket %d\n", getTimeStringNow(), __FUNCTION__, ci->name, ci->ip, ci->port, ci->sock);
		lsConnectedCount++;
		
		IsLsKitCommsArr[ci->lsid%LSID_RANGE] = true;
		return true;
	}
#endif //KIT_COMMS
	if ((ci->sock = OpenInetClient(ci->port,ci->ip)) == 0) {
//		llout0<<__FUNCTION__<<" - OpenInetClient() for port "<<ci->port<<" failed!!\n";
		ci->sock = 0;
		return false;
	}
	llout0<<__FUNCTION__<<" - OpenInetClient() for "<<ci->name<<" on "<<ci->ip<<":"<<ci->port<<" successfully. Have socket "<<ci->sock<<endl;
	fprintf(pmlogf, "%s - %s - OpenInetClient() for %s on %s:%d successfully. Have socket %d\n", getTimeStringNow(), __FUNCTION__, ci->name, ci->ip, ci->port, ci->sock);

	lsConnectedCount++;
	dumpSockOpt(ci->sock);
	return true;
}
#endif

void connectToStations() {
	FUNC_BEGIN
// TODO: close sockets before?
	for (uint32_t i=0;i<lsCount;i++) {
		if (LsConn[i].sock != 0) {
			llout0<<__FUNCTION__<<" - sock != 0, already connected? Closing and connecting new ...\n";
#ifdef USE_RC
			LsConn[i].sock = NULL;
#else
			close(LsConn[i].sock);
			LsConn[i].sock = 0;
#endif
		}
		if (connectToStation(&LsConn[i])) {
//			llout0<<"connectToStations() - successfully connected to "<<LsConn[i].name<<" with socket "<<LsConn[i].sock<<", now checking for autoStartDaq ..."<<endl;
#ifdef USE_RC
			pmLog("%s - successfully connected to  %s, now checking for autoStartDaq ...\n", __FUNCTION__, LsConn[i].name);
//			fprintf(pmlogf, "%s - %s - successfully connected to  %s, now checking for autoStartDaq ...\n", getTimeStringNow(), __FUNCTION__, LsConn[i].name);
#else
			pmLog("%s - successfully connected to  %s with socket %d, now checking for autoStartDaq ...\n", __FUNCTION__, LsConn[i].name, LsConn[i].sock);
//			fprintf(pmlogf, "%s - %s - successfully connected to  %s with socket %d, now checking for autoStartDaq ...\n", getTimeStringNow(), __FUNCTION__, LsConn[i].name, LsConn[i].sock);
#endif
//			llout0<<__FUNCTION__<<" - there are now "<<lsConnectedCount<<" connected stations ...\n";

			lsConnCar[i].actCount++;
			lsConnCar[i].totalCount++;
			lsConnCar[i].startTime = time(NULL);
//			if (lsDisConnTimesCar[i].startTime == 0) lsDisConnTimesCar[i].startTime = time(NULL);		// set disconnection times in relation to "total" connection time

			if (autoStartDaq) {		// send a start msg to the station, this is valid on startup of postmaster
				pmLog("autoStartDaq is true, writing start command to station\n");
				sendStartToLS(&LsConn[i]);
			} else pmLog("autoStartDaq is false\n");
		}
	}
	FUNC_END
}

void initEbSocket() {
#ifdef USE_RC
	if ((EbConn.sock != NULL)) {
		pmLog("%s - Eb socket is connected - calling anyway RcSocketNew() again\n", __FUNCTION__);
	}
//****** JLB 26/09/2012 + 19/11/12 start
#ifndef USE_RC6
	if ((EbConn.sock = RcSocketNew( EbConn.name,
                RcSocketRecoveryExtended,
		NULL,
		EbConn.port,
		EBUI_INPUT_BUFFER_SIZE,
		INPUT_FIFO_SIZE,
		INPUT_QUICKLOOK,
		EBUI_OUTPUT_BUFFER_SIZE,
		OUTPUT_FIFO_SIZE,
		OUTPUT_QUICKLOOK)) == NULL) {
#else		// USE_RC6
	if ((EbConn.sock = RcSocketNew( EbConn.name,
                0x0001, // JLBY 24/09/12 + 19/11/12.
		NULL, EbConn.port,
                EBPM_BUFFER_SIZE, INPUT_FIFO_SIZE,
		TIMEOUT,
		PMEB_BUFFER_SIZE, OUTPUT_FIFO_SIZE,
		SELECT_TIMEOUT ) ) == NULL) {
#endif
//****** JLB 26/09/2012 + 19/11/12 end
		pmLog("%s - RcSocketNew() for Eb port %d failed!!\n", __FUNCTION__, EbConn.port);
//		llout0<<__FUNCTION__<<" - RcSocketNew() for Eb port "<<EbConn.port<<" failed!!\n";
//		fprintf(pmlogf, "%s - %s - RcSocketNew() for Eb port %d failed!!\n", getTimeStringNow(), __FUNCTION__, EbConn.port);
		return;
	}
	pmLog("%s - RcSocketNew() for EB port %d successfully\n", __FUNCTION__, EbConn.port);
//	llout0<<__FUNCTION__<<" - RcSocketNew() for EB port "<<EbConn.port<<" successfully\n";
//	fprintf(pmlogf, "%s - %s - RcSocketNew() for EB port %d successfully\n", getTimeStringNow(), __FUNCTION__, EbConn.port);
#else
	if ((EbConn.sock != 0)) {
		pmLog("%s - Eb socket is %d\n", __FUNCTION__, EbConn.sock);
		close(EbConn.sock);
	}
	if ((EbConn.servProto != 0)) close(EbConn.servProto);
	if ((EbConn.servProto = OpenInetProtoServer(EbConn.port, &EbConn.servAddress)) == 0) {
		pmLog("%s - OpenInetProtoServer() for Eb port %d failed!!\n", __FUNCTION__, EbConn.port);
//		llout0<<__FUNCTION__<<" - OpenInetProtoServer() for Eb port "<<EbConn.port<<" failed!!\n";
//		fprintf(pmlogf, "%s - %s - OpenInetProtoServer() for Eb port %d failed!!\n", getTimeStringNow(), __FUNCTION__, EbConn.port);
		return;
	}
	pmLog("%s - OpenInetProtoServer() for EB port %d successfully\n", __FUNCTION__, EbConn.port);
//	llout0<<__FUNCTION__<<" - OpenInetProtoServer() for EB port "<<EbConn.port<<" successfully\n";
//	fprintf(pmlogf, "%s - %s - OpenInetProtoServer() for EB port %d successfully\n", getTimeStringNow(), __FUNCTION__, EbConn.port);
#endif
}

void initGuiSocket() {
#ifdef USE_RC
//	dumpConnInfo(&GuiConn);
	if ((GuiConn.sock != NULL)) {
		pmLog("%s - Gui socket is connected\n", __FUNCTION__);
	}
//****** JLB 26/09/2012 + 19/11/12 start
#ifndef USE_RC6
	if ((GuiConn.sock = RcSocketNew( GuiConn.name,
                RcSocketRecoveryExtended,
		NULL,
		GuiConn.port,
		EBUI_INPUT_BUFFER_SIZE,
		INPUT_FIFO_SIZE,
		INPUT_QUICKLOOK,
		EBUI_OUTPUT_BUFFER_SIZE,
		OUTPUT_FIFO_SIZE,
		OUTPUT_QUICKLOOK)) == NULL) {
#else
	if ((GuiConn.sock = RcSocketNew( GuiConn.name,
                0x0001, // JLBY 24/09/12 + 19/11/12.
		NULL,
		GuiConn.port,
		UIPM_BUFFER_SIZE, INPUT_FIFO_SIZE,
		TIMEOUT,
		PMUI_BUFFER_SIZE, OUTPUT_FIFO_SIZE,
		SELECT_TIMEOUT)) == NULL) {
#endif
//****** JLB 26/09/2012 + 19/11/12 end
		pmLog("%s - RcSocketNew() for Gui server port %d failed!!\n", __FUNCTION__, GuiConn.port);
//		llout0<<__FUNCTION__<<" - RcSocketNew() for Gui port "<<GuiConn.port<<" failed!!\n";
//		fprintf(pmlogf, "%s - %s - RcSocketNew() for Gui server port %d failed!!\n", getTimeStringNow(), __FUNCTION__, GuiConn.port);
		return;
	}
	pmLog("%s - RcSocketNew() for Gui server port %d successfully\n", __FUNCTION__, GuiConn.port);
//	llout0<<__FUNCTION__<<" - RcSocketNew() for Gui server port "<<GuiConn.port<<" successfully\n";
//	fprintf(pmlogf, "%s - %s - RcSocketNew() for Gui server port %d successfully\n", getTimeStringNow(), __FUNCTION__, GuiConn.port);
#else
	dumpConnInfo(&GuiConn);
	if ((GuiConn.sock != 0)) {
		pmLog("%s - Gui socket is %d\n", __FUNCTION__, GuiConn.sock);
		close(GuiConn.sock);
	}
	if ((GuiConn.servProto != 0)) close(GuiConn.servProto);
	if ((GuiConn.servProto = OpenInetProtoServer(GuiConn.port, &GuiConn.servAddress)) == 0) {
		pmLog("%s - OpenInetProtoServer() for Gui port %d failed!!\n", __FUNCTION__, GuiConn.port);
		return;
	}
	pmLog("%s - OpenInetProtoServer() for Gui port %d successfully\n", __FUNCTION__, GuiConn.port);
#endif
}

void initPromptSocket() {
	FUNC_BEGIN
#ifdef USE_RC
//	dumpConnInfo(&PromptConn);
	if ((PromptConn.sock != NULL)) {
		pmLog("%s - Prompt socket is connected\n", __FUNCTION__);
	}
//****** JLB 26/09/2012 + 19/11/12 start
#ifndef USE_RC6
	if ((PromptConn.sock = RcSocketNew( PromptConn.name,
                RcSocketRecoveryExtended,
		NULL,
		PromptConn.port,
		EBUI_INPUT_BUFFER_SIZE,
		INPUT_FIFO_SIZE,
		INPUT_QUICKLOOK,
		EBUI_OUTPUT_BUFFER_SIZE,
		OUTPUT_FIFO_SIZE,
		OUTPUT_QUICKLOOK)) == NULL) {
#else
	if ((PromptConn.sock = RcSocketNew( PromptConn.name,
                0x0001, // JLBY 24/09/12 + 19/11/12.
		NULL, PromptConn.port,
		PRPM_BUFFER_SIZE, INPUT_FIFO_SIZE,
		TIMEOUT,
		PMPR_BUFFER_SIZE, OUTPUT_FIFO_SIZE,
		SELECT_TIMEOUT)) == NULL) {
#endif
//****** JLB 26/09/2012 + 19/11/12 end
		pmLog("%s - RcSocketNew() for Prompt server port %d failed!!\n", __FUNCTION__, PromptConn.port);
		return;
	}
	pmLog(" %s - RcSocketNew() for Prompt server port %d successfully\n", __FUNCTION__, PromptConn.port);
#else
	if ((PromptConn.sock != 0)) close(PromptConn.sock);
	if ((PromptConn.servProto != 0)) close(PromptConn.servProto);
	if ((PromptConn.servProto = OpenInetProtoServer(PromptConn.port, &PromptConn.servAddress)) == 0) {
//		printf("OpenInetProtoServer() for prompt %d failed!!\n", PromptConn.port);
		pmLog("%s - %s - OpenInetProtoServer() for prompt port %d failed!!\n", getTimeStringNow(), __FUNCTION__, PromptConn.port);
		return;
	}
//	printf("OpenInetProtoServer() proto = %d for prompt port %d successfully\n", PromptConn.servProto, PromptConn.port);
	fprintf(pmlogf, "%s - %s - OpenInetProtoServer() proto = %d for prompt port %d successfully\n", getTimeStringNow(), __FUNCTION__, PromptConn.servProto, PromptConn.port);
	FUNC_END
#endif
}

void initT3Socket() {
	FUNC_BEGIN
#ifdef USE_RC
	//****** JLB 26/09/2012 start
#ifndef USE_RC6
	if ((T3Conn.sock = RcSocketNew( T3Conn.name,
		RcSocketRecoveryExtended,
		NULL,
		T3Conn.port,
		PMCL_INPUT_BUFFER_SIZE,		// AeraRcDefs.h
		INPUT_FIFO_SIZE,
		INPUT_QUICKLOOK,
		PMCL_OUTPUT_BUFFER_SIZE,
		OUTPUT_FIFO_SIZE,
		OUTPUT_QUICKLOOK)) == NULL) {
#else
	if ((T3Conn.sock = RcSocketNew( T3Conn.name,
                0x0001, // JLBY 24/09/12 + 19/11/12.
		NULL, T3Conn.port,
                T3PM_BUFFER_SIZE,
                INPUT_FIFO_SIZE,
		TIMEOUT,
		PMT3_BUFFER_SIZE,
                OUTPUT_FIFO_SIZE,
		SELECT_TIMEOUT)) == NULL) {
#endif
//****** JLB 26/09/2012 + 19/11/12 end
//	dumpConnInfo(&T3Conn);
	if ((T3Conn.sock != NULL)) {
		llout0<<__FUNCTION__<<" - T3 socket is connected\n";
	}
		llout0<<__FUNCTION__<<" - RcSocketNew() for T3 server port "<<T3Conn.port<<" failed!!\n";
		fprintf(pmlogf, "%s - %s - RcSocketNew() for T3 server port %d failed!!\n", getTimeStringNow(), __FUNCTION__, T3Conn.port);
		return;
	}
	llout0<<__FUNCTION__<<" - RcSocketNew() for T3 server port "<<T3Conn.port<<" successfully\n";
	fprintf(pmlogf, "%s - %s - RcSocketNew() for T3 server port %d successfully\n", getTimeStringNow(), __FUNCTION__, T3Conn.port);
#else
	if ((T3Conn.sock != 0)) close(T3Conn.sock);
	if ((T3Conn.servProto != 0)) close(T3Conn.servProto);
	if ((T3Conn.servProto = OpenInetProtoServer(T3Conn.port, &T3Conn.servAddress)) == 0) {
//		printf("OpenInetProtoServer() for T3 port %d failed!!\n", T3Conn.port);
		fprintf(pmlogf, "%s - %s - OpenInetProtoServer() for T3 port %d failed!!\n", getTimeStringNow(), __FUNCTION__, T3Conn.port);
		return;
	}
	printf("OpenInetProtoServer() for T3 port %d successfully\n", T3Conn.port);
	fprintf(pmlogf, "%s - %s - OpenInetProtoServer() for T3 port %d successfully\n", getTimeStringNow(), __FUNCTION__, T3Conn.port);
#endif
	FUNC_END
}

void reConnectLostStations() {
//	FUNC_BEGIN
	for (uint32_t i=0;i<lsCount;i++) {
//		llout0<<__FUNCTION__<<" checking station "<<LsConn[i].name<<", port = "<<LsConn[i].port<<", sock = "<<LsConn[i].sock<<" ..."<<endl;
		if ((LsConn[i].port) != 0) {
#ifdef USE_RC
			if(LsConn[i].sock == NULL) {
#else
			if(LsConn[i].sock == 0) {
#endif
//				llout0<<"Lost connection to "<<LsConn[i].name<<", trying to reconnect..."<<endl;
				if (connectToStation(&LsConn[i])) {
					llout0<<"Reconnected to "<<LsConn[i].name<<endl;
					fprintf(pmlogf, "%s - %s - Reconnected to  %s!!\n", getTimeStringNow(), __FUNCTION__, LsConn[i].name);
					lsConnCar[i].startTime = time(NULL);
					lsConnCar[i].actCount++;
					lsConnCar[i].totalCount++;
					int timeInt = difftime(time(NULL), lsDisConnTimesCar[i].actTime);
					lsDisConnTimesCar[i].actCount += timeInt;
					lsDisConnTimesCar[i].totalCount += timeInt;
					fprintf(pmlogf, "%s - %s - Station %s reconnected after %d seconds !!\n", getTimeStringNow(), __FUNCTION__, LsConn[i].name, timeInt);
					lsDisConnTimesCar[i].calcNow();
					lsDisConnTimesCar[i].print();
					lsDisConnTimesCar[i].print2File(pmlogf);		// printf to the given file
					lsDisConnTimesCar[i].resetActNow();

					if (daqStarted) {
						llout0<<"daq is running => sending start to reconnected station "<<LsConn[i].name<<endl;
						fprintf(pmlogf, "%s - %s - daq is running => sending start to reconnected station  %s!!\n", getTimeStringNow(), __FUNCTION__, LsConn[i].name);
						sendStartToLS(&LsConn[i]);
					}
				} else {
//					llout0<<"Reconnecting lost station "<<LsConn[i].name<<" failed, trying later ..."<<endl;
				}
			} else {
//				llout0<<LsConn[i].name<<" is still connected"<<endl;
			}
		} else {
			llout0<<"conn at index "<<i<<" is not to be connected - not in config file"<<endl;
		}
	}
//	FUNC_END
}

#ifdef KIT_COMMS
// this is copied from connectToStation(), without "lsConnectedCount++;"
// better to write a co-used function.
bool connectToKitCommsSock(connInfo* ci) {
	if ((ci->sock = OpenInetClient(ci->port,ci->ip)) == 0) {
//		llout0<<__FUNCTION__<<" - OpenInetClient() for port "<<ci->port<<" failed!!\n";
		ci->sock = 0;
		return false;
	}
	llout0<<__FUNCTION__<<" - OpenInetClient() for "<<ci->name<<" on "<<ci->ip<<":"<<ci->port<<" successfully. Have socket "<<ci->sock<<endl;
	fprintf(pmlogf, "%s - %s - OpenInetClient() for %s on %s:%d successfully. Have socket %d\n", getTimeStringNow(), __FUNCTION__, ci->name, ci->ip, ci->port, ci->sock);

	dumpSockOpt(ci->sock);
	return true;
}

// reused for connect and re-Connect
// no problem, as readConfigFile() is called before (.port != 0), 
// sock == 0 whenever sock is reseted or is disconnected during operation e.g by readSocket()
// Special feature: Could be limited or unlimited try count! set in pm.init
void reConnectToKitComms() {
	static uint32_t reConnectCount = 0;
	if ((KitCommsRTConn.port) != 0) {
		if(KitCommsRTConn.sock == 0) {
			if ((reConnectCount < reConnectTryLimit) || (reConnectTryLimit == 0)) { //unlimited try or reconnect limit not reached --> reconnect
				if (connectToKitCommsSock(&KitCommsRTConn)) {
					llout0<<"(re)-connected to "<<KitCommsRTConn.name<<endl;
				} else { //reconnect fail
					if (reConnectTryLimit > 0) { //if not unlimited try
						reConnectCount++; //incr here
						if (reConnectCount == reConnectTryLimit) {
							printf("%s - Give up connect to kit comms after %d times retry, kit comms SBC is determined to be not reacheable though configured\n", __FUNCTION__, reConnectCount);
						}
					}
				}
			} else {
			}
		} else { //still connected
		}
	} else { //unconfiged in pm.init
	}
	
	if ((KitCommsHQConn.port) != 0) {
		if(KitCommsHQConn.sock == 0) {
			if ((reConnectCount < reConnectTryLimit) || (reConnectTryLimit == 0)) { //unlimited try or reconnect limit not reached --> reconnect
				if (connectToKitCommsSock(&KitCommsHQConn)) {
					llout0<<"(re)-connected to "<<KitCommsHQConn.name<<endl;
				} else { //reconnect fail
				}
			}
		} else { //still connected
		}
	} else { //unconfiged in pm.init
	}
}
#endif

void initConnections() {
	connectToStations();
	#ifdef KIT_COMMS
		if (useKitComms) {
			llout0<<"KIT_COMMS used, version:"<<KitCommsVersion<<endl;
			llout0<<"description:"<<KitCommsDescription<<endl;
			reConnectToKitComms(); //reused function for connect and reconnect
		}
	#endif //KIT_COMMS
// make and manage daq connections
	if (useT3) initT3Socket();
	if (useGui) initGuiSocket();
	if (usePr) initPromptSocket();
	if (useEb) initEbSocket();
}

#ifndef USE_RC
void acceptClients() {
//	FUNC_BEGIN
	fflush(stdout);
	int maxAcceptNum = 0;		// holds first max_fd for select, than return of select
	struct timeval acceptClientsTimeval = {0,100};			/* AW: 0 seconds, 100 microseconds => accepting should be fast*/
	unsigned int nSocketSize = sizeof(struct sockaddr_in);
	int acceptCount = 0;
	bool needToAcceptClients = false;

	FD_ZERO(&acceptClientFDS);		// blank the mask

	// add sockets without connection to mask
	if (useEb) {
		if ((EbConn.servProto != 0)  && (EbConn.sock == 0))  {
			FD_SET(EbConn.servProto,&acceptClientFDS);
			maxAcceptNum = MAX(maxAcceptNum, EbConn.servProto);
	//		printf("%s Eb not connected, trying to accept ...\n", __FUNCTION__);
			needToAcceptClients = true;
			acceptCount++;
		} else if ((EbConn.servProto == 0)  && (EbConn.sock == 0)) initEbSocket();
	}
	if (useT3) {
		if ((T3Conn.servProto != 0)  && (T3Conn.sock == 0)) {
			FD_SET(T3Conn.servProto,&acceptClientFDS);
			maxAcceptNum = MAX(maxAcceptNum, T3Conn.servProto);
	//		printf("%s T3 not connected, trying to accept ...\n", __FUNCTION__);
			needToAcceptClients = true;
			acceptCount++;
		} else if ((T3Conn.servProto == 0)  && (T3Conn.sock == 0)) initT3Socket();
	}
	if (useGui) {
		if ((GuiConn.servProto != 0) && (GuiConn.sock == 0)) {
			FD_SET(GuiConn.servProto,&acceptClientFDS);
			maxAcceptNum = MAX(maxAcceptNum, GuiConn.servProto);
	//		printf("%s Gui not connected, trying to accept ...\n", __FUNCTION__);
			needToAcceptClients = true;
			acceptCount++;
		} else if ((GuiConn.servProto == 0)  && (GuiConn.sock == 0)) initGuiSocket();
	}
	if (usePr) {
		if ((PromptConn.servProto != 0) && (PromptConn.sock == 0)) {
			FD_SET(PromptConn.servProto,&acceptClientFDS);
			maxAcceptNum = MAX(maxAcceptNum, PromptConn.servProto);
	//		printf("%s Prompt not connected, trying to accept ...\n", __FUNCTION__);
			needToAcceptClients = true;
			acceptCount++;
		} else if ((PromptConn.servProto == 0)  && (PromptConn.sock == 0)) initPromptSocket();
	}
//	printf("%s calling accept for %d sockets...\n", __FUNCTION__, acceptCount);fflush(stdout);
	if (needToAcceptClients) {
		if ((maxAcceptNum = select(maxAcceptNum+1,&acceptClientFDS,NULL,NULL,&acceptClientsTimeval)) > 0) {
			llout1<<__FUNCTION__<<"() - select returned "<<maxAcceptNum<<" clients to accept ..."<<endl;
			if (maxAcceptNum > 0) {			// some sockets are ready to accept
//				printf("Accepting a client - select() returned %d, trying to accept ...\n", maxAcceptNum);
				// accept dis/unconnected T3 client ...
				if (FD_ISSET(T3Conn.servProto, &acceptClientFDS)) {
					if (T3Conn.servProto != 0 && T3Conn.sock == 0) {
//						printf("%s - trying to accept T3 client ...\n", __FUNCTION__);
						if ((T3Conn.sock = accept(T3Conn.servProto,T3Conn.servAddress,&nSocketSize))<0) {
							if (T3Conn.sock <= 0) {
								if (isSocketError(T3Conn.sock)) {
								  // ... whatever is necessary to do on errors
									disconnectSocket(&T3Conn);
								}
//								printf("Accepting a T3 client failed - but select() returned %d!!???\n", maxAcceptNum);
								T3Conn.sock = 0;
							}
						} else {
							fprintf(pmlogf, "%s - %s - Accepted a T3 client on sock %d\n", getTimeStringNow(), __FUNCTION__, T3Conn.sock);
							llout0<<__FUNCTION__<<" - Accepted a T3 client on sock "<<T3Conn.sock<<endl;
						}
					}
				}
				// accept dis/unconnected EB client ...
				if (FD_ISSET(EbConn.servProto, &acceptClientFDS)) {
					if (EbConn.servProto != 0 && EbConn.sock == 0) {
						printf("%s - trying to accept Eb client ...\n", __FUNCTION__);
						if ((EbConn.sock = accept(EbConn.servProto,EbConn.servAddress,&nSocketSize))<0) {
							if (EbConn.sock <= 0) {
								if (isSocketError(EbConn.sock)) {
								  // ... whatever is necessary to do on errors
								}
								printf("Accepting a EB client failed - but select() returned %d!!???\n", maxAcceptNum);
								EbConn.sock = 0;
							}
						} else {
							fprintf(pmlogf, "%s - %s - Accepted a EB client on sock %d\n", getTimeStringNow(), __FUNCTION__, EbConn.sock);
							printf("Accepted a EB client on sock %d\n", EbConn.sock);
						}
					}
				}
				// accept dis/unconnected Gui client ...
				if (FD_ISSET(GuiConn.servProto, &acceptClientFDS)) {
					if (GuiConn.servProto != 0 && GuiConn.sock == 0) {
						printf("%s - trying to accept Gui client ...\n", __FUNCTION__);
						if ((GuiConn.sock = accept(GuiConn.servProto,GuiConn.servAddress,&nSocketSize))<0) {
							if (GuiConn.sock <= 0) {
								if (isSocketError(GuiConn.sock)) {
								  // ... whatever is necessary to do on errors
								}
								printf("Accepting a Gui client failed - but select() returned %d!!???\n", maxAcceptNum);
								GuiConn.sock = 0;
							}
						} else {
							fprintf(pmlogf, "%s - %s - Accepted a Gui client on sock %d\n", getTimeStringNow(), __FUNCTION__, GuiConn.sock);
							printf("Accepted a Gui client on sock %d\n", GuiConn.sock);
						}
					}
				}
				// accept dis/unconnected prompt client ...
				if (FD_ISSET(PromptConn.servProto, &acceptClientFDS)) {
					if (PromptConn.servProto != 0 && PromptConn.sock == 0) {
						printf("%s - trying to accept Prompt client ...\n", __FUNCTION__);
						if ((PromptConn.sock = accept(PromptConn.servProto,PromptConn.servAddress,&nSocketSize))<0) {
							if (PromptConn.sock <= 0) {
								if (isSocketError(PromptConn.sock)) {
								  // ... whatever is necessary to do on errors
								}
								printf("Accepting a Prompt client failed - but select() returned %d!!???\n", maxAcceptNum);
								PromptConn.sock = 0;
							}
						} else {
							fprintf(pmlogf, "%s - %s - Accepted a Prompt client on sock %d\n", getTimeStringNow(), __FUNCTION__, PromptConn.sock);
							printf("Accepted a Prompt client on sock %d\n", PromptConn.sock);
						}
					}
				}
			}
		}// else llout1<<__FUNCTION__<<"() - select returned "<<maxAcceptNum<<" => no clients to accept!"<<endl;
	} else {
//		printf("%s no need to accept!\n", __FUNCTION__);
	}
//	FUNC_END
}
#endif

void checkConnections() {		// check for lost sockets of stations and DAQ processes
//	FUNC_BEGIN
	// check station connections for validity
	checkStationConnections();

	// check and reconnect lost stations
	reConnectLostStations();		// check if connections to stations are there or still there!
	
	#ifdef KIT_COMMS
		if (useKitComms) reConnectToKitComms();
	#endif //KIT_COMMS

	// check if last message of a connection timed out - makes only sense for sockets with high communication rate, that means only for T3 (out of daq processes)
	time_t now = time(NULL);
	if ((T3Conn.lastMsgTime > 0) && (difftime(now, T3Conn.lastMsgTime) > noMsgConnTimeout)) {
		printf("%s - last message of T3 %s more than %d seconds ago => resetting connection\n", __FUNCTION__, T3Conn.name, noMsgConnTimeout);
		fprintf(pmlogf, "%s - %s - last message of T3 %s more than %d seconds ago => resetting connection\n", getTimeStringNow(), __FUNCTION__, T3Conn.name, noMsgConnTimeout);
		disconnectSocket(&T3Conn);
		T3Conn.lastMsgTime = 0;
	}


	// check if one of the server ports has gone
#ifdef USE_RC
	if (useEb) if (EbConn.port != 0 && EbConn.sock == NULL) {
		initEbSocket();
#else
	if (useEb) if (EbConn.port != 0 && EbConn.sock == 0) {
#endif
		llout1<<EbConn.name<<" is not connected"<<endl;
	}
#ifdef USE_RC
	if (useT3) if (T3Conn.port != 0 && T3Conn.sock == NULL) {
		initT3Socket();
#else
	if (useT3) if (T3Conn.port != 0 && T3Conn.sock == 0) {
#endif
		llout1<<T3Conn.name<<" is not connected"<<endl;
	}
#ifdef USE_RC
	if (useGui) if (GuiConn.port != 0 && GuiConn.sock == NULL) {
		initGuiSocket();
#else
	if (useGui) if (GuiConn.port != 0 && GuiConn.sock == 0) {
#endif
		llout1<<GuiConn.name<<" is not connected"<<endl;
	}
#ifdef USE_RC
	if (usePr) if (PromptConn.port != 0 && PromptConn.sock == NULL) {
		initPromptSocket();
#else
	if (usePr) if (PromptConn.port != 0 && PromptConn.sock == 0) {
#endif
		llout1<<PromptConn.name<<" is not connected"<<endl;
	}

#ifndef USE_RC
	acceptClients();				// check for any client, that wants to (re-)connect (aevb, T3, prompt, Gui)
#endif
//	FUNC_END
}

void checkGui() {
#ifdef USE_RC
	if (GuiConn.sock == NULL) return;
//	printf("checkGui() socket %s : %d \n", GuiConn.ip, GuiConn.port);
//    RcBuffer16   inputBuffer = NULL;
    UINT16* inputBuffer = NULL;
   inputBuffer = RcSocketGetInputBufferToBeEmptied (GuiConn.sock);
   if ( inputBuffer != NULL ){
	    printf("checkGui() - received data from GUI\n");
		readSocket(&GuiConn);
   }
   RcSocketSetInputBufferEmptied ( GuiConn.sock);
   return;
#else
	int maxReadNum = 0;
	int readSockCount = 0; //, socketsReadCount = 0;
	int readTimeout = 10000;
	struct timeval readGuiTimeval = {0,readTimeout};			/* AW: 0 seconds, 100 microseconds => reading should be fast*/
	Msg *getMsg;

//	llout1<<"checkGui() ..."<<endl;

	FD_ZERO(&readGuiFDS);		// blank the mask
	if (GuiConn.sock != 0) {
		FD_SET(GuiConn.sock,&readGuiFDS);
		maxReadNum = MAX(maxReadNum, GuiConn.sock);
//		llout1<<"checkGui()  Gui socket is connected => adding to read mask ..."<<endl;
	}
//	llout1<<"checkGui() - maxReadNum is "<<maxReadNum<<endl;

	readSockCount = 0;

	if ((readSockCount = select(maxReadNum+1,&readGuiFDS,NULL,NULL,&readGuiTimeval)) > 0) {
    	if (readSockCount != 0) {			// some sockets are ready to read
			if (GuiConn.sock != 0) {
//				if (!(getMsg = ReadSockMsg(GuiConn.sock))) {
				if (!(getMsg = ReadSockMsgSM_TO(GuiConn.sock, readTimeout))) {
				  if (isSocketError(GuiConn.sock) == 1) {
					  // ... whatever is necessary to do on errors
						llout0<<__FUNCTION__<<" - Gui socket has error "<<endl;
						socketHasError(&GuiConn);
						disconnectSocket(&GuiConn);
				  } else {
						printf("%s - reading message from ready socket failed, but no error??? Probably read EOF due to lost connection\n", __FUNCTION__);
						// if the connection is broken, reading will return EOF, which endsup here, so it is an error
						socketHasError(&GuiConn);
						disconnectSocket(&GuiConn);
				  }
				} else {	// have a message
					printf("received message on Gui socket \n");
					printf("msg length = %d \n", getMsg->length);
					AMSG *getAmsg = (AMSG*) &getMsg->data;
					printf("AMSG length = %d\n", getAmsg->length);
					printf("AMSG tag    = %d\n", getAmsg->tag);
					// subtags were only defined in AeraTags.h
/*					if (getAmsg->tag != GUI_TO_EVENTBUILDER) {
						printf("tag from Gui was not GUI_TO_EVENTBUILDER = %d, but %d. Processing skipped!\n", GUI_TO_EVENTBUILDER, getAmsg->tag);

					} else {
						UINT16 subtag = getAmsg->body[0];
						switch (subtag) {*/
						switch (getAmsg->tag) {
							case GUI_START_RUN:			// LS_START:
								printf("received start from Gui, starting daq ...\n");
								fprintf(pmlogf, "%s - %s - received start from Gui, starting daq ...\n", getTimeStringNow(), __FUNCTION__);
								startDaq();
								break;
							case GUI_STOP_RUN:			// LS_STOP:
								printf("received stop from Gui, stopping daq ...\n");
								fprintf(pmlogf, "%s - %s - received stop from Gui, stopping daq ...\n", getTimeStringNow(), __FUNCTION__);
								stopDaq();
								break;
							case GUI_INITIALIZE:
								printf("received GUI_INITIALIZE from Gui, initializing stations ...\n");
								fprintf(pmlogf, "%s - %s - received GUI_INITIALIZE from Gui, initializing daq ...\n", getTimeStringNow(), __FUNCTION__);
								sendLSinitialize();
								break;
							default:
//								printf("%s - tag %d not processed by postmaster, msg discarded!\n", __FUNCTION__,  subtag);
								printf("%s - tag %d not processed by postmaster, msg discarded!\n", __FUNCTION__,  getAmsg->tag);
								fprintf(pmlogf, "%s - %s - tag %d not processed by postmaster, msg discarded!\n", getTimeStringNow(), __FUNCTION__,  getAmsg->tag);
								break;
						}
//					}
					free(getMsg);
				}
			}
    	}
    }
//	llout0<<"checkGui() - select returned "<<readSockCount<<" sockets for read ..."<<endl;
#endif
}


void checkPrompt() {
#ifdef USE_RC
	if (PromptConn.sock == NULL) return;
//	printf("checkPrompt() : socket %s : %d \n", PromptConn.ip, PromptConn.port);
#ifndef USE_RC5
	RcBuffer16   inputBuffer = NULL;
#else
	UINT16 *inputBuffer = NULL;
#endif

   inputBuffer = RcSocketGetInputBufferToBeEmptied (PromptConn.sock);
   if ( inputBuffer != NULL ){
	    printf("checkPrompt() - received data from prompt\n");
		readSocket(&PromptConn);
   }
   RcSocketSetInputBufferEmptied ( PromptConn.sock);
   return;
#else
	int maxReadNum = 0;
	int readSockCount = 0; //, socketsReadCount = 0;
	int readTimeout = 100;
	struct timeval readPromptTimeval = {0,readTimeout};			/* AW: 0 seconds, 100 microseconds => reading should be fast*/
	Msg *getMsg;

//	llout1<<"checkPrompt() ..."<<endl;

	FD_ZERO(&readPromptFDS);		// blank the mask
	// add connected station sockets  to mask
	for (uint32_t i=0;i<lsCount;i++) {
//		printf("%s - sock of connection at index %d is %d\n", __FUNCTION__ , i , PromptConn.sock);
		if (PromptConn.sock != 0) {
			FD_SET(PromptConn.sock,&readPromptFDS);
			maxReadNum = MAX(maxReadNum, PromptConn.sock);
//			llout0<<"checkPrompt()  socket at index "<<i<<" to station "<<PromptConn.name<<" is connected => adding to read mask ..."<<endl;
		}
	}
//	llout0<<"checkPrompt() - maxReadNum is "<<maxReadNum<<endl;

	readSockCount = 0;

	if ((readSockCount = select(maxReadNum+1,&readPromptFDS,NULL,NULL,&readPromptTimeval)) > 0) {
    	if (readSockCount != 0) {			// some sockets are ready to read
			if (PromptConn.sock != 0) {
//				if (!(getMsg = ReadSockMsg(PromptConn.sock))) {
				if (!(getMsg = ReadSockMsgSM_TO(PromptConn.sock, readTimeout))) {
				  if (isSocketError(PromptConn.sock) == 1) {
					  // ... whatever is necessary to do on errors
						llout0<<__FUNCTION__<<" - prompt socket has error "<<endl;
						socketHasError(&PromptConn);
						disconnectSocket(&PromptConn);
				  } else {
						printf("%s - reading message from ready socket failed, but no error??? Probably read EOF due to lost connection\n", __FUNCTION__);
						// if the connection is broken, reading will return EOF, which endsup here, so it is an error
						socketHasError(&PromptConn);
						disconnectSocket(&PromptConn);
				  }
				} else {	// have a message
					printf("received message on prompt socket \n");
					printf("msg length = %d \n", getMsg->length);
					AMSG *getAmsg = (AMSG*) &getMsg->data;
					printf("AMSG length = %d\n", getAmsg->length);
					printf("AMSG tag    = %d\n", getAmsg->tag);
					if (getAmsg->tag != PROMPT_TO_EVENTBUILDER) {
						printf("tag from prompt was not PROMPT_TO_EVENTBUILDER = %d, but %d. Processing skipped!\n", PROMPT_TO_EVENTBUILDER, getAmsg->tag);

					} else {
						UINT16 subtag = getAmsg->body[0];
						switch (subtag) {
							case START_RUN:			// LS_START:
								printf("received start from prompt, starting daq ...\n");
								fprintf(pmlogf, "%s - %s - received start from prompt, starting daq ...\n", getTimeStringNow(), __FUNCTION__);
								startDaq();
								break;
							case STOP_RUN:			// LS_STOP:
								printf("received stop from prompt, stopping daq ...\n");
								fprintf(pmlogf, "%s - %s - received stop from prompt, stopping daq ...\n", getTimeStringNow(), __FUNCTION__);
								stopDaq();
								break;
							default:
								printf("%s - tag %d not processed by postmaster, msg discarded!\n", __FUNCTION__,  subtag);
								fprintf(pmlogf, "%s - %s - tag %d not processed by postmaster, msg discarded!\n", getTimeStringNow(), __FUNCTION__,  subtag);
								break;
						}
					}
					free(getMsg);
				}
			}
    	}
    }

//	llout0<<"checkPrompt() - select returned "<<readSockCount<<" sockets for read ..."<<endl;
#endif
}

// check all connections, if an ALIVE_ACK is pending / has not been sent
bool checkAliveAck() {
	time_t now = time(NULL);
	bool allAcksReceived = true;
	if (aliveSentTimeT3 != 0) {
		if ((now - aliveSentTimeT3) > MAX_ALIVE_ACK_ANSWER_INTERVAL) {
			printf("%s - ALIVE has been sent to T3maker as %s at %d is outdated since %d seconds\n", __FUNCTION__, T3Conn.name, (int)aliveSentTimeT3, (int)(now-aliveSentTimeT3));
		} 	else {	// a sent one, but neither answered nor outdated
			allAcksReceived = false;
		}
	}
	for (int i=0; i<MAX_LS;i++ ){
		if (aliveSentTimeLS[i] != 0) {
			if ((now - aliveSentTimeLS[i]) > MAX_ALIVE_ACK_ANSWER_INTERVAL) {
				printf("%s - ALIVE has been sent to station %s at %d is outdated since %d seconds\n", __FUNCTION__, LsConn[i].name, (int)aliveSentTimeLS[i], (int)(now-aliveSentTimeLS[i]));
			} 	else {	// a sent one, but neither answered nor outdated
				allAcksReceived = false;
			}
		}
	}
	return allAcksReceived;
}


// do a read test on sockets, which will give an error on disconnected sockets, so its mainly a connection test
// lost sockets are set = 0, and re-connected in reConnectLostStations
void checkStationConnections() {
    struct sockaddr_in InetSocketAddress;
  	struct sockaddr *SocketAddress = (struct sockaddr *)&InetSocketAddress;
  	socklen_t slen= sizeof(struct sockaddr_in);
// 	int getpeername(int socket, struct sockaddr *restrict address, ssocklen_t *restrict address_len);

	int ret = 0, sockerr;
//	Msg *getMsg;
//	FUNC_BEGIN
//	printf("%s - checking station sockets for valid connection...\n", __FUNCTION__ );
	for (uint32_t i=0;i<lsCount;i++) {
//		printf("%s - station sock of connection at index %d is %d\n", __FUNCTION__ , i , LsConn[i].sock);
#ifdef USE_RC
		if (LsConn[i].sock != NULL) {
#else
		if (LsConn[i].sock != 0) {
#endif
			time_t now = time(NULL);
			if ((LsConn[i].lastMsgTime > 0) && (difftime(now, LsConn[i].lastMsgTime) > noMsgConnTimeout)) {
//				printf("%s - last message of station %s more than %d seconds ago => resetting connection\n", __FUNCTION__, LsConn[i].name, noMsgConnTimeout);
				fprintf(pmlogf, "%s - %s - last message of station %s more than %d seconds ago => resetting connection\n", getTimeStringNow(), __FUNCTION__, LsConn[i].name, noMsgConnTimeout);
				disconnectSocket(&LsConn[i]);
				lsDisConnCar[i].startTime = time(NULL);
				lsDisConnCar[i].actCount++;
				lsDisConnCar[i].totalCount++;
				lsDisConnTimesCar[i].actTime = now;		// interval of disconnection starts at deconnection, and ends at reConnection
				LsConn[i].lastMsgTime = 0;
				// disconnected due to reset!
				lsResetCar[i].actCount++;
				lsResetCar[i].totalCount++;
				continue;						// this connection already is reset
			}

#ifndef USE_RC
			ret = getpeername(LsConn[i].sock, SocketAddress, &slen);
			if (ret == -1) {		// Upon successful completion, 0 shall be returned. Otherwise, -1 shall be returned and errno set to indicate the error.
				sockerr = errno;
				switch (sockerr) {
					case EBADF:												// The socket argument is not a valid file descriptor.
					case EINVAL:											// The socket has been shut down.
					case ENOTCONN:											// The socket is not connected or otherwise has not had the peer pre-specified.
					case ENOTSOCK:											// The socket argument does not refer to a socket.
					case EOPNOTSUPP:										// The operation is not supported for the socket protocol.
					case ENOBUFS:											// Insufficient resources were available in the system to complete the call.
						perror("checkStationConnections()");
						break;
					default:
						break;
				}
				llout0<<__FUNCTION__<<" - station sock of connection at index "<<i<<" is "<<LsConn[i].sock<<" has an error!!! Closing!\n";
				fprintf(pmlogf, "%s - %s - station sock of connection at index %d is %d has an error!!! Closing!\n", getTimeStringNow(), __FUNCTION__, i , LsConn[i].sock);
				socketHasError(&LsConn[i]);
				disconnectSocket(&LsConn[i]);
				lsDisConnCar[i].startTime = time(NULL);
				lsDisConnCar[i].actCount++;
				lsDisConnCar[i].totalCount++;
				lsDisConnTimesCar[i].actTime = now;		// interval of disconnection starts at deconnection, and ends at reConnection
				lsConnectedCount--;
				llout0<<__FUNCTION__<<" - there are "<<lsConnectedCount<<" connected stations left ...\n";

			}
#endif

/*
			if (!(getMsg = ReadSockMsgTO(LsConn[i].sock, 1))) {			// try to read with a timeslot of one microsecond
			  if (isSocketError(LsConn[i].sock)) {
				  // ... whatever is necessary to do on errors
					llout0<<"checkStationConnections()  socket at index "<<i<<" to station "<<LsConn[i].name<<" is connected, checking for read returned "<<ret<<endl;
//					socketHasError(&LsConn[i]);
			  }
			} else {
				printf("%s - reading message from ready socket failed, but no error??? Probably read EOF due to lost connection\n", __FUNCTION__);
				// if the connection is broken, reading will return EOF, which endsup here, so it is an error
				socketHasError(&LsConn[i]);
			}
*/
//			if ((ret = TestSocketForWrite(LsConn[i].sock)) < 0) {
/*			if ((ret = TestSocketForRead(LsConn[i].sock)) < 0) {
				llout0<<"checkStationConnections()  socket at index "<<i<<" to station "<<LsConn[i].name<<" is connected, checking for write returned "<<ret<<endl;
				LsConn[i].sock = 0;
				lsConnectedCount--;
				printf("%s - there are %d connected stations left ...\n", __FUNCTION__, lsConnectedCount);
			}
*/		}
	}
//	FUNC_END
}

// check other sockets (all, except the stations) for read
void checkClientsForRead() {
	int readSockCount = 0;
#ifdef USE_RC
	if (T3Conn.sock != NULL) {
		checkT3();
	}
	if (GuiConn.sock != NULL) {
		checkGui();
	}
	if (PromptConn.sock != NULL) {
		checkPrompt();
	}
	if (EbConn.sock != NULL) {
//		checkEb();					// nothing expected yet from Eb
	}
#else
	if (T3Conn.sock != 0) {
		FD_SET(T3Conn.sock,&readClientFDS);
		maxReadNumClients = MAX(maxReadNumClients, T3Conn.sock);
//		llout0<<__FUNCTION__<<"  T3 socket is connected => adding to read mask ..."<<endl;
	}
	if (EbConn.sock != 0) {
		FD_SET(EbConn.sock,&readClientFDS);
		maxReadNumClients = MAX(maxReadNumClients, EbConn.sock);
//		llout0<<__FUNCTION__<<"  Eb socket is connected => adding to read mask ..."<<endl;
	}
	if (GuiConn.sock != 0) {
		FD_SET(GuiConn.sock,&readClientFDS);
		maxReadNumClients = MAX(maxReadNumClients, GuiConn.sock);
//		llout0<<__FUNCTION__<<"  Gui socket is connected => adding to read mask ..."<<endl;
	}
	if (PromptConn.sock != 0) {
		FD_SET(PromptConn.sock,&readClientFDS);
		maxReadNumClients = MAX(maxReadNumClients, PromptConn.sock);
//		llout0<<__FUNCTION__<<"  prompt socket is connected => adding to read mask ..."<<endl;
	}
	if ((readSockCount = select(maxReadNumClients+1,&readClientFDS,NULL,NULL,&readClientsTimeval)) > 0) {	// check all connected stations for read
    	if (readSockCount != 0) {			// some sockets are ready to read
 //   		printf("%s - select returned %d client sockets ready to be read ...\n", __FUNCTION__, readSockCount);
    		// check T3 socket
			if (FD_ISSET(T3Conn.sock, &readClientFDS)) readSocket(&T3Conn);			// this socket is in the list of readable sockets
    		// check Eb socket
			if (FD_ISSET(EbConn.sock, &readClientFDS)) readSocket(&EbConn);			// this socket is in the list of readable sockets
    		// check Gui socket
			if (FD_ISSET(GuiConn.sock, &readClientFDS)) readSocket(&GuiConn);			// this socket is in the list of readable sockets
    		// check prompt socket
			if (FD_ISSET(PromptConn.sock, &readClientFDS)) readSocket(&PromptConn);	// this socket is in the list of readable sockets
    	}
	}
#endif
}

#ifdef KIT_COMMS //"temporary" functions here, they may be removed in future
void checkKitCommsForRead() {
	int maxReadNum = 0;
	int readSockCount = 0;
	struct timeval readTimeval = {0, selKitCommsTimeout};

//	FUNC_BEGIN
//	llout0<<"checkKitCommsForRead() ..."<<endl;

	FD_ZERO(&readKitCommsFDS);		// blank the mask
	if (KitCommsRTConn.sock != 0) { //if the client sock is established --> include to read, if not (whatever unconfigured im pm.init or configured but not reacheable) --> causes no harm
		FD_SET(KitCommsRTConn.sock,&readKitCommsFDS);
		maxReadNum = MAX(maxReadNum, KitCommsRTConn.sock);
	}
	if (KitCommsHQConn.sock != 0) {
		FD_SET(KitCommsHQConn.sock,&readKitCommsFDS);
		maxReadNum = MAX(maxReadNum, KitCommsHQConn.sock);
	}	

	readSockCount = 0;

	if ((readSockCount = select(maxReadNum+1,&readKitCommsFDS,NULL,NULL,&readTimeval)) > 0) {	// check all connected stations for read
    	if (readSockCount != 0) {			// some sockets are ready to read
    		llout2<<__FUNCTION__<<" - select returned "<<readSockCount<<" sockets ready to be read ...\n";
			//real job happened here in readSocket()! unfortunately, the readSocket does EVERYTHING: read, analyze, forward, stat-build....
			//its's better to give the possibility, to tell readSocket() (actually processMsg function) if it should -->
			//forward T2, process T2, forward T3 and process T3
			//this is critical, as these message from kit comms cannot directly go into aevb and t3maker! if only one PM process exists
			//other possiblity without changing the code is: create new readSocketKitComms(), processMsgKitComms().... but it's awkward in software
			if (FD_ISSET(KitCommsRTConn.sock, &readKitCommsFDS)) readSocket(&KitCommsRTConn);	// this socket is in the list of readable sockets
			if (FD_ISSET(KitCommsHQConn.sock, &readKitCommsFDS)) readSocket(&KitCommsHQConn);	// this socket is in the list of readable sockets
    	}
    }
}
#endif //KIT_COMMS

// used "#ifdef	CHECK_SOCKETS_SEPARATELY", meaning one check for all stations, one for other sockets, check for stations sending T2 or data
void checkStationsForRead() {
#ifdef USE_RC
/****** Start JLBY 24/09/12 *****/
RcSocket  *socket = NULL;
UINT16*   inputBuffer = NULL;
UINT8     iSocket = 0;
UINT8        readySocket = 0;
//struct timeval  timeOut = {0, selLsTimeout};
struct timeval  timeOut = {1,0};
//struct timeval  timeOut = {0,1000};

// JLB 14/01/13 Start
//#ifdef USE_RC5
#ifndef USE_RC6
// JLB 14/01/13 End
	for (int i=0;i<MAX_LS;i++) {
		if (LsConn[i].sock != NULL) {
//			printf("checkStationsForRead() : checking socket %s : %d = %s ...\n", LsConn[i].ip, LsConn[i].port, LsConn[i].name);
//			fprintf(pmlogf, "checkStationsForRead() : checking socket %s : %d = %s ...\n", LsConn[i].ip, LsConn[i].port, LsConn[i].name);
			readSocket(&LsConn[i]);
		}
	}
#else
            socket = RcSocketSelectGroup ( 1, 0, &readySocket );
            for ( iSocket = 0; iSocket < readySocket; iSocket++ )
            {
                inputBuffer = RcSocketGetInputBufferToBeEmptied ( socket[iSocket] );
                if ( inputBuffer != NULL )
                {
                    processRcBuffer(inputBuffer);
                    RcSocketSetInputBufferEmptied ( socket[iSocket] );
                }
            }

#endif
/****** End JLBY 24/09/12 *****/
#else
//	int msgReadCount = 0;
	int readSockCount = 0, socketsReadCount = 0;
	int sockerr = 0;
	struct timeval readSelStationsTimeval = {0, selLsTimeout};			/* AW: 0 seconds, 100 microseconds => reading should be fast*/
	Msg *getMsg;
	UINT16 *msbp;
	int j;
//	llout0<<"MSG_RCVBUFF_LENGTH:"<<MSG_RCVBUFF_LENGTH<<endl;

//	FUNC_BEGIN
//	llout0<<"checkStationsForRead() ..."<<endl;

#define USE_COMMON_SELECT
#ifdef USE_COMMON_SELECT
	FD_ZERO(&readStationsFDS);		// blank the mask
	// add connected station sockets  to mask
	for (uint32_t i=0;i<MAX_LS;i++) {
//		printf("%s - sock of connection at index %d is %d\n", __FUNCTION__ , i , LsConn[i].sock);
		if (LsConn[i].sock != 0) {
			FD_SET(LsConn[i].sock,&readStationsFDS);
			maxReadNumStations = MAX(maxReadNumStations, LsConn[i].sock);
//			llout0<<"checkStationsForRead()  socket at index "<<i<<" to station "<<LsConn[i].name<<" is connected => adding to read mask ..."<<endl;
		}
	}
//	llout0<<"checkStationsForRead() - maxReadNum is "<<maxReadNum<<endl;

	readSockCount = 0;
	socketsReadCount = 0;

	if ((readSockCount = select(maxReadNumStations+1,&readStationsFDS,NULL,NULL,&readSelStationsTimeval)) > 0) {	// check all connected stations for read
    	if (readSockCount != 0) {			// some sockets are ready to read
    		llout2<<__FUNCTION__<<" - select returned "<<readSockCount<<" sockets ready to be read ...\n";
    		for (int i=0;i<MAX_LS;i++) {
    			if (LsConn[i].sock != 0) {
    				if (!FD_ISSET(LsConn[i].sock, &readStationsFDS)) continue;		// this socket is not in the list of readable sockets
    				if (socketsReadCount == readSockCount) {
//        				printf("%s - read from all ready sockets, skipping checking rest\n", __FUNCTION__);
        				break;
    				}
//    				printf("%s - LsConn at index %d has sock_fd %d, trying to read ...\n", __FUNCTION__, i, LsConn[i].sock);
    				readSocket(&LsConn[i]);
    			}
    		}
    	}
    }
#else			// call select for each station socket - called internally in socklibs read-functions
	for (int i=0;i<MAX_LS;i++) {
		if (LsConn[i].sock != 0) {
//			printf("%s - LsConn at index %d has sock_fd %d, trying to read ...\n", __FUNCTION__, i, LsConn[i].sock);
			errno = 0;			// set errno here explicitly. to be sure only errors on reading appear?!
			if (!(getMsg = ReadSockMsgSM_TO(LsConn[i].sock, 1))) {			// reading with static memory
//					if (!(getMsg = ReadSockMsg(LsConn[i].sock))) {
//				printf("%s - LsConn at index %d has sock_fd %d, read returned NULL!\n", __FUNCTION__, i, LsConn[i].sock);
				sockerr = isSocketError();
				if (sockerr == 1) {			// only =1 is a real socket error
					// ... whatever is necessary to do on errors
					llout0<<__FUNCTION__<<" - socket at index "<<i<<" to station "<<LsConn[i].name<<" has socket error "<<endl;
					socketHasError(&LsConn[i]);
				} else if (sockerr == 0){
//					printf("%s - reading message from ready socket failed, but no error??? Probably read EOF due to lost connection\n", __FUNCTION__);
					// if the connection is broken, reading will return EOF, which endsup here, so it is an error
//					socketHasError(&LsConn[i]);
				} else{
//					printf("%s - reading message from ready socket failed, but no socket error (%d). Tried maybe to read on not ready socket, doing nothing\n", __FUNCTION__, sockerr);
				}
			} else {	// have a message
				socketsReadCount++;
				llout1<<__FUNCTION__<<" - received message on socket with index "<<i<<" = station "<<LsConn[i].name<<", socket "<<LsConn[i].sock<<endl;
				llout1<<__FUNCTION__<<" - msg length = "<<getMsg->length<<endl;
				AMSG *getAmsg = (AMSG*) &getMsg->data;
				llout1<<__FUNCTION__<<" -AMSG length = "<<getAmsg->length<<endl;
				llout1<<__FUNCTION__<<" - AMSG tag    = "<<getAmsg->tag<<endl;
				if (!daqStarted) llout1<<__FUNCTION__<<" - DAQ not running - ignoring messages from stations!\n";
				else {
					llout1<<__FUNCTION__<<" - DAQ running - processing messages from stations ...\n";
					switch (getAmsg->tag) {
						case LS_T2:
							if (t2StartTime == 0) t2StartTime = time(NULL);
							// at first (and only during runtimes?) forward the msg to t3maker
							msbp = (UINT16*)getMsg;
							for (j=0; j<getAmsg->length+1;j++) {
//									printf("Amsg(%d) : %hu = 0x%X\n", j, msbp[j], msbp[j]);
							}
//									forwardT2toT3(getMsg);
							processT2Msg(getMsg);	processT2Msg accepts only AMSG!!!
							break;
						default:
							printf("%s - tag %d not processed by postmaster, msg discarded!\n");
							break;
					}
				}
//						free(getMsg);			// do not forget to free the message after ReadSockMsg()!
			}
		}
	}
#endif
#endif	// #ifdef USE_RC
//	llout0<<"checkStationsForRead() - select returned "<<readSockCount<<" sockets for read ..."<<endl;
//	FUNC_END
}


// read a message from that socket
#ifdef USE_RC
void readSocket(rcConnInfo *cip) {		// Rc: functions is called immediateley, to see, if something has been received
	if (cip->sock == NULL) return;
//	printf("readSocket() socket %s - %s : %d \n", cip->name, cip->ip, cip->port);
#ifndef USE_RC5
	RcBuffer16   inputBuffer = NULL;
#else
	UINT16   *inputBuffer = NULL;
#endif

	inputBuffer = RcSocketGetInputBufferToBeEmptied (cip->sock);
	if ( inputBuffer != NULL ){
//		printf("readSocket() - received something from %s\n", cip->name);
#ifndef USE_RC5
		UINT16 rsize = RcBuffer16GetUsedSize(inputBuffer);
#else
		UINT16 rsize = inputBuffer[0];
#endif
//		printf("readSocket() -  data of size %hu (= count of shorts) in buffer from %s\n", rsize, cip->name);
		fprintf(pmlogf, "readSocket() -  data of size %hu (= count of shorts) in buffer from %s\n", rsize, cip->name);
		int ret = processRcBuffer(inputBuffer);
// JLB 10/1/13 Start - AW 21/03/13 - activated it again, stations want to have ALIVE messages
		if (ret == ALIVE) {
			llout0<<__FUNCTION__<<" - received ALIVE from socket "<<cip->name<<endl;
			fprintf(pmlogf, "%s -  %s\n", __FUNCTION__, cip->name);
			sendAliveAck(cip);
		} else if (ret == ALIVE_ACK) {
			llout0<<__FUNCTION__<<" - received ALIVE_ACK from socket "<<cip->name<<endl;
			fprintf(pmlogf, "%s -  %s\n", __FUNCTION__, cip->name);
			if (ret == ALIVE) {
				llout0<<__FUNCTION__<<" - received ALIVE from socket "<<cip->name<<endl;
				fprintf(pmlogf, "%s -  %s\n", __FUNCTION__, cip->name);
				sendAliveAck(cip);
			} else if (ret == ALIVE_ACK) {
				llout0<<__FUNCTION__<<" - received ALIVE_ACK from socket "<<cip->name<<endl;
				fprintf(pmlogf, "%s -  %s\n", __FUNCTION__, cip->name);
				if (cip->sock == T3Conn.sock) {
					llout0<<__FUNCTION__<<" - received ALIVE_ACK from socket "<<cip->name<<endl;
				} else if (cip->sock == T3Conn.sock) {
					aliveSentTimeT3 = 0;
					llout0<<__FUNCTION__<<" - resetted aliveSentTime for "<<cip->name<<endl;
				} else if (cip->sock == EbConn.sock) {
					aliveSentTimeEb = 0;
					llout0<<__FUNCTION__<<" - resetted aliveSentTime for "<<cip->name<<endl;
				} else if (cip->sock == GuiConn.sock) {
					aliveSentTimeGui = 0;
					llout0<<__FUNCTION__<<" - resetted aliveSentTime for "<<cip->name<<endl;
				} else if (cip->sock == PromptConn.sock) {
					aliveSentTimePrompt = 0;
					llout0<<__FUNCTION__<<" - resetted aliveSentTime for "<<cip->name<<endl;
				} else {
					for (int i=0; i<MAX_LS; i++) {
						if (cip->sock == LsConn[i].sock) {
							aliveSentTimeLS[i] = 0;
							llout0<<__FUNCTION__<<" - resetted aliveSentTime for "<<cip->name<<endl;
						}
					}
				}
			}
		}
// JLB 10/1/13 End - AW 21/03/13

		RcSocketSetInputBufferEmptied ( cip->sock);
//		fprintf(pmlogf, "readSocket() -  set buffer from %s emptied\n", cip->name);
	}
#else
void readSocket(connInfo *cip) {		// using this function, reading from station or other sockets is done with same timeout and error handling!!
	Msg *getMsg;
	int sockerr = 0;

	llout2<<__FUNCTION__<<" - trying to read from socket "<<cip->name<<" ...\n";
	memset((void*)msgRcvBuff, 0, MSG_RCVBUFF_LENGTH);
	errno = 0;			// set errno here explicitly. to be sure only errors on reading appear?!
//	if (!(getMsg = ReadSockMsgSM(LsConn[i].sock))) {								// reading with static memory
	if (!(getMsg = ReadSockMsgSM_TO(cip->sock, readLsTimeout))) {					// reading with static memory and timeout, using timeout for stations, cause I do not know, what socket this is, and is the most critical
//	if (!(getMsg = RecvSockMsgSM(cip->sock))) {										// reading with static memory, timeout is set on opening socket before connecting
//	if (!(getMsg = ReadSockMsgEM_TO(cip->sock, 1, msgRcvBuff))) {					// reading with memory provide from postmaster
//	if (!(getMsg = ReadSockMsg(cip->sock))) {					// remember to activate free()
		llout1<<__FUNCTION__<<" - Conn "<<cip->name<<" has sock_fd "<<cip->sock<<", read returned NULL!\n";
		sockerr = isSocketError(cip->sock);
		if (sockerr == 1) {			// only =1 is a real socket error
			// ... whatever is necessary to do on errors
			llout0<<__FUNCTION__<<" - socket "<<cip->name<<" has socket error "<<endl;
			fprintf(pmlogf, "%s - %s - socket %s has socket error\n", getTimeStringNow(), __FUNCTION__, cip->name);
			socketHasError(cip);
			disconnectSocket(cip);
			// belongs the socket to a station !?!?!
//			lsConnectedCount--;
//			printf("%s - there are %d connected stations left ...\n", __FUNCTION__, lsConnectedCount);
		} else if (sockerr == 0){
			llout0<<__FUNCTION__<<" - reading message from ready socket failed, but no error??? Probably read EOF due to lost connection\n";
			fprintf(pmlogf, "%s - %s - reading message from ready socket %s failed, but no error??? Probably read EOF due to lost connection\n", getTimeStringNow(), __FUNCTION__ , cip->name);
			// if the connection is broken, reading will return EOF, which endsup here, so it is an error
			socketHasError(cip);
			disconnectSocket(cip);
			// belongs the socket to a station !?!?!
//			lsConnectedCount--;
//			printf("%s - there are %d connected stations left ...\n", __FUNCTION__, lsConnectedCount);
		} else {
			llout0<<__FUNCTION__<<" - reading message from ready socket failed, but no socket error ("<<sockerr<<"). Tried maybe to read on not ready socket, doing nothing\n";
			fprintf(pmlogf, "%s - reading message from ready socket failed, but no socket error (%d). Tried maybe to read on not ready socket, doing nothing\n", __FUNCTION__, sockerr);
		}
	} else {	// have a message
/*						int *ip = (int*) getMsg;
		for (int z=0; z<(getMsg->length/sizeof(int));z++) {
			llout0<<"int "<<setw(4)<<dec<<z<<" as int : "<<*ip<<" as hex : 0x"<<hex<<*ip<<endl;
			ip++;
		}*/
//						if (getMsg->length > 10000) llout0<<"This is the received message as UINT16 dump:"<<dumpMemUINT16((UINT16*)getMsg, getMsg->length/sizeof(UINT16));
		cip->lastMsgTime = time(NULL);
//		printf("received message on socket  %s, socket %d\n", cip->name, cip->sock);
//						printf("msg length = %d, message points to %d \n", getMsg->length, (int*)getMsg);
//						printf("msg->data = %d, &msg->data = %d \n", getMsg->data, getMsg->data);

/* can't use this at the moment, cause this function can not easily distinguish between station or other sockets
		if (!daqStarted) printf("%s - DAQ not running - ignoring messages from stations!\n", __FUNCTION__);
		else {
			printf("%s - DAQ running - processing message ...\n", __FUNCTION__);
			processMsg(getMsg);
		}
*/
		int ret = processMsg(getMsg);

		if (ret == ALIVE) {
			llout0<<__FUNCTION__<<" - received ALIVE from socket "<<cip->name<<endl;
			fprintf(pmlogf, "%s -  %s\n", __FUNCTION__, cip->name);
			sendAliveAck(cip);
		} else if (ret == ALIVE_ACK) {
			llout0<<__FUNCTION__<<" - received ALIVE_ACK from socket "<<cip->name<<endl;
			fprintf(pmlogf, "%s -  %s\n", __FUNCTION__, cip->name);
			if (cip->sock == T3Conn.sock) {
				llout0<<__FUNCTION__<<" - received ALIVE_ACK from socket "<<cip->name<<endl;
			} else if (cip->sock == T3Conn.sock) {
				aliveSentTimeT3 = 0;
				llout0<<__FUNCTION__<<" - resetted aliveSentTime for "<<cip->name<<endl;
			} else if (cip->sock == EbConn.sock) {
				aliveSentTimeEb = 0;
				llout0<<__FUNCTION__<<" - resetted aliveSentTime for "<<cip->name<<endl;
			} else if (cip->sock == GuiConn.sock) {
				aliveSentTimeGui = 0;
				llout0<<__FUNCTION__<<" - resetted aliveSentTime for "<<cip->name<<endl;
			} else if (cip->sock == PromptConn.sock) {
				aliveSentTimePrompt = 0;
				llout0<<__FUNCTION__<<" - resetted aliveSentTime for "<<cip->name<<endl;
			} else {
				for (int i=0; i<MAX_LS; i++) {
					if (cip->sock == LsConn[i].sock) {
						aliveSentTimeLS[i] = 0;
						llout0<<__FUNCTION__<<" - resetted aliveSentTime for "<<cip->name<<endl;
					}
				}
			}
		}

// be sure to free getMsg, depending on the socklib function used to read the Msg
//		free(getMsg);
	}
#endif
}

#ifdef USE_RC
void checkSocketsForRead() {
	printf("no checkSocketsForRead() for Rc so far!\n");
}
#else
// this function is foreseen to have one common select for all pending sockets, redirecting messages for processing
// TODO:  check all sockets at once and process single messages out of it
void checkSocketsForRead() {
	int maxReadNum = 0;
	int readSockCount = 0, socketsReadCount = 0;
	struct timeval readTimeval = {0,1};			/* AW: 0 seconds, microseconds => reading should be fast*/


	FD_ZERO(&readFDS);		// blank the mask

	// add connected station sockets  to mask
	for (uint32_t i=0;i<MAX_LS;i++) {
//		printf("%s - sock of connection at index %d is %d\n", __FUNCTION__ , i , LsConn[i].sock);
		if (LsConn[i].sock != 0) {
			FD_SET(LsConn[i].sock,&readFDS);
			maxReadNum = MAX(maxReadNum, LsConn[i].sock);
//			llout0<<"checkStationsForRead()  socket at index "<<i<<" to station "<<LsConn[i].name<<" is connected => adding to read mask ..."<<endl;
		}
	}
	if (T3Conn.sock != 0) {
		FD_SET(T3Conn.sock,&readFDS);
		maxReadNum = MAX(maxReadNum, T3Conn.sock);
//		llout0<<__FUNCTION__<<"  T3 socket is connected => adding to read mask ..."<<endl;
	}
	if (EbConn.sock != 0) {
		FD_SET(EbConn.sock,&readFDS);
		maxReadNum = MAX(maxReadNum, EbConn.sock);
//		llout0<<__FUNCTION__<<"  Eb socket is connected => adding to read mask ..."<<endl;
	}
	if (GuiConn.sock != 0) {
		FD_SET(GuiConn.sock,&readFDS);
		maxReadNum = MAX(maxReadNum, GuiConn.sock);
//		llout0<<__FUNCTION__<<"  Gui socket is connected => adding to read mask ..."<<endl;
	}
	if (PromptConn.sock != 0) {
		FD_SET(PromptConn.sock,&readFDS);
		maxReadNum = MAX(maxReadNum, PromptConn.sock);
//		llout0<<__FUNCTION__<<"  prompt socket is connected => adding to read mask ..."<<endl;
	}
	#ifdef KIT_COMMS
	if (KitCommsRTConn.sock != 0) { //if the client sock is established --> include to read, if not (whatever unconfigured im pm.init or configured but not reacheable) --> causes no harm
		FD_SET(KitCommsRTConn.sock,&readFDS);
		maxReadNum = MAX(maxReadNum, KitCommsRTConn.sock);
	}
	if (KitCommsHQConn.sock != 0) {
		FD_SET(KitCommsHQConn.sock,&readFDS);
		maxReadNum = MAX(maxReadNum, KitCommsHQConn.sock);
	}	
	#endif //KIT_COMMS


	readSockCount = 0;				// count of readable sockets returned by select
	socketsReadCount = 0;			// count of sockets already checked for read. Used to break loop of socket check. But what is faster: FD_ISSET for some sockets or incrementing a counter and check always its value??

	if ((readSockCount = select(maxReadNum+1,&readFDS,NULL,NULL,&readTimeval)) > 0) {	// check all connected stations for read
    	if (readSockCount != 0) {			// some sockets are ready to read
    		llout1<<__FUNCTION__<<" - select returned "<<readSockCount<<" sockets ready to be read ...\n";
    		for (int i=0;i<MAX_LS;i++) {
    			if (LsConn[i].sock != 0) {
    				if (!FD_ISSET(LsConn[i].sock, &readFDS)) continue;		// this socket is not in the list of readable sockets
    				else {
    					readSocket(&LsConn[i]);
    					socketsReadCount++;
    				}
    				if (socketsReadCount >= readSockCount) break;
    			}
    		}
    		// check T3 socket
			if (FD_ISSET(T3Conn.sock, &readFDS)) readSocket(&T3Conn);			// this socket is in the list of readable sockets
    		// check Eb socket
			if (FD_ISSET(EbConn.sock, &readFDS)) readSocket(&EbConn);			// this socket is in the list of readable sockets
    		// check Gui socket
			if (FD_ISSET(GuiConn.sock, &readFDS)) readSocket(&GuiConn);			// this socket is in the list of readable sockets
    		// check prompt socket
			if (FD_ISSET(PromptConn.sock, &readFDS)) readSocket(&PromptConn);	// this socket is in the list of readable sockets

			#ifdef KIT_COMMS
			//real job happened here in readSocket()! unfortunately, the readSocket does EVERYTHING: read, analyze, forward, stat-build....
			//its's better to give the possibility, to tell readSocket() (actually processMsg function) if it should -->
			//forward T2, process T2, forward T3 and process T3
			//this is critical, as these message from kit comms cannot directly go into aevb and t3maker! if only one PM process exists
			//other possiblity without changing the code is: create new readSocketKitComms(), processMsgKitComms().... but it's awkward in software
			if (FD_ISSET(KitCommsRTConn.sock, &readFDS)) readSocket(&KitCommsRTConn);	// this socket is in the list of readable sockets
			if (FD_ISSET(KitCommsHQConn.sock, &readFDS)) readSocket(&KitCommsHQConn);	// this socket is in the list of readable sockets
			#endif //KIT_COMMS
    	}
	}
}
#endif

void checkT3() {
#ifdef USE_RC
	if (T3Conn.sock == NULL) return;
//	printf("checkT3() socket %s : %d \n", T3Conn.ip, T3Conn.port);
#ifndef USE_RC5
	RcBuffer16   inputBuffer = NULL;
#else
	UINT16   *inputBuffer = NULL;
#endif

/*
   inputBuffer = RcSocketGetInputBufferToBeEmptied (T3Conn.sock);
   if ( inputBuffer != NULL ){
	    printf("checkT3() - received data from T3\n");
		readSocket(&T3Conn);
		RcSocketSetInputBufferEmptied ( T3Conn.sock);
   }
*/
	readSocket(&T3Conn);
	return;
#else
	int maxReadNum = 0;
	int readSockCount = 0; //, socketsReadCount = 0;
	struct timeval readT3Timeval = {0,100};			/* AW: 0 seconds, 100 microseconds => reading should be fast */
	Msg *getMsg;

	if (T3Conn.sock == 0) return;

//	FUNC_BEGIN
//	llout0<<"checkT3() ..."<<endl;

	FD_ZERO(&readT3FDS);		// blank the mask
	// add T3 socket  to mask
//	printf("%s - sock of T3 connection is %d\n", __FUNCTION__ , T3Conn.sock);
	if (T3Conn.sock != 0) {
		FD_SET(T3Conn.sock,&readT3FDS);
		maxReadNum = MAX(maxReadNum, T3Conn.sock);

		if ((readSockCount = select(maxReadNum+1,&readT3FDS,NULL,NULL,&readT3Timeval)) > 0) {
			if (readSockCount != 0) {			// some sockets are ready to read
				if (T3Conn.sock != 0) {
					// reading and processing the message elsewhere  ...
					readSocket(&T3Conn);

/*
//					if (!(getMsg = ReadSockMsg(T3Conn.sock))) {
					if (!(getMsg = ReadSockMsgSM_TO(T3Conn.sock,1))) {

					  if (isSocketError(T3Conn.sock) == 1) {
						  // ... whatever is necessary to do on errors
							llout0<<__FUNCTION__<<" - T3 socket has error "<<endl;
							socketHasError(&T3Conn);
							disconnectSocket(&T3Conn);
					  } else {
							printf("%s - reading message from ready T3 socket failed, but no error??? Probably read EOF due to lost connection\n", __FUNCTION__);
							fprintf(pmlogf, "%s - %s - reading message from ready T3 socket failed, but no error??? Probably read EOF due to lost connection\n", getTimeStringNow(), __FUNCTION__ );
							// if the connection is broken, reading will return EOF, which ends up here, so it is an error
							socketHasError(&T3Conn);
							disconnectSocket(&T3Conn);
					  }
					} else {	// have a message - but here only one/the first is processed!
						printf("received message on T3 socket \n");
						printf("msg length = %d \n", getMsg->length);
						AMSG *getAmsg = (AMSG*) &getMsg->data;
						printf("AMSG length = %d\n", getAmsg->length);
						printf("AMSG tag    = %d\n", getAmsg->tag);
						switch (getAmsg->tag) {
							case T3_EVENT_REQUEST_LIST:
							case T3_EXT_SD_EVENT_REQUEST_LIST:
							case T3_EXT_GUI_EVENT_REQUEST_LIST:
							case T3_EXT_FD_EVENT_REQUEST_LIST:
									if (t3car.startTime == 0) t3car.startTime = time(NULL);
									t3MsgtotalCount++;
									printf("received tag %d from T3, processing ...\n",  getAmsg->tag);
									fprintf(pmlogf, "%s - %s - received tag %d from T3, processing ...\n", getTimeStringNow(), __FUNCTION__,  getAmsg->tag );
									// distribute T3 message to stations
									sendT3reqToLS(getAmsg);
									processT3Msg(getAmsg);
									break;
							default:
								printf("%s - tag %d from T3 not processed by postmaster, msg discarded!\n", __FUNCTION__,  getAmsg->tag);
								fprintf(pmlogf, "%s - %s - tag %d from T3 not processed by postmaster, msg discarded!\n", getTimeStringNow(), __FUNCTION__,  getAmsg->tag );
								break;
				lsid		}
//						free(getMsg);		// only necessary for ReadSockMsg, ReadSockMsgSM_TO uses static memories
					}
*/
				}
			}
		}
	}
//	FUNC_END
#endif
}

#ifdef USE_RC
void disconnectSocket(rcConnInfo *ci) {	// Rc does not support a disconnection. This means: mark as disconnected
	if (ci == NULL) return;
	fprintf(pmlogf, "%s - %s - disconnecting socket %s ...\n", getTimeStringNow(), __FUNCTION__,  ci->name);
	ci->sock = NULL;
}
#else
void disconnectSocket(connInfo *ci) {
	if (ci == NULL) return;
	fprintf(pmlogf, "%s - %s - disconnecting socket %s ...\n", getTimeStringNow(), __FUNCTION__,  ci->name);
	if (ci->sock != 0) close(ci->sock);
	ci->sock = 0;
}
#endif

void doStationStatistics() {
	for (int i = 0; i< MAX_LS; i++) {
		if (LsConn[i].port == 0) continue;		// unused place in field
		if (LsConn[i].sock == 0) llout1<<__FUNCTION__<<" Station "<< LsConn[i].name<<" at "<< LsConn[i].ip<<":"<< LsConn[i].port<<" not connected\n";		// unused place in field
		else llout1<<__FUNCTION__<<" Station"<< LsConn[i].name<<" at "<< LsConn[i].ip<<":"<< LsConn[i].port<<" is connected\n";
		lsT2Car[i].calcNow();
		lsT2Car[i].print();
		lsT2Car[i].print2File(pmlogf);		// printf to the given file
		lsT2Car[i].resetActNow();

		lsT3Car[i].calcNow();
		lsT3Car[i].print();
		lsT3Car[i].print2File(pmlogf);		// printf to the given file
		lsT3Car[i].resetActNow();

		lsDataCar[i].calcNow();
		lsDataCar[i].print();
		lsDataCar[i].print2File(pmlogf);		// printf to the given file
		lsDataCar[i].resetActNow();

		lsConnCar[i].calcNow();
		lsConnCar[i].print();
		lsConnCar[i].print2File(pmlogf);		// printf to the given file
		lsConnCar[i].resetActNow();

		lsDisConnCar[i].calcNow();
		lsDisConnCar[i].print();
		lsDisConnCar[i].print2File(pmlogf);		// printf to the given file
		lsDisConnCar[i].resetActNow();

		lsDisConnTimesCar[i].calcNow();
		lsDisConnTimesCar[i].print();
		lsDisConnTimesCar[i].print2File(pmlogf);		// printf to the given file
//		lsDisConnTimesCar[i].resetActNow();			// do NOT reset the actTime, as this is the time since the station is disconnected, we want to keep that until next connection, so reset it on reConnection!

		lsResetCar[i].calcNow();
		lsResetCar[i].print();
		lsResetCar[i].print2File(pmlogf);		// printf to the given file
		lsResetCar[i].resetActNow();
	}
}

void doT2Statistics() {
	if (t2car.totalCount == 0) {
		fprintf(pmlogf, "\t T2 messages count total: %6d => no rates available\n", t2car.totalCount);
	} else {
		t2car.calcNow();
		t2car.print();					// prints only to stdout!
		t2car.print2File(pmlogf);		// printf to the given file
	}
	t2car.resetActNow();

	if (t2sNotSentCar.totalCount == 0) {
		fprintf(pmlogf, "\t All T2s so far forwarded to T3maker!!\n");
	} else {
		t2sNotSentCar.calcNow();
		t2sNotSentCar.print();					// prints only to stdout!
		t2sNotSentCar.print2File(pmlogf);		// printf to the given file
	}
	t2sNotSentCar.resetActNow();


/*
	time_t statTime = time(NULL);
	t2rateTotal = (double) t2totalCount/difftime(statTime, t2StartTime);
	printf("%s - doing T2 statistics at %d:\n", __FUNCTION__, (int)statTime);
	fprintf(pmlogf, "%s - %s - doing T2 statistics at %d:\n", getTimeStringNow(), __FUNCTION__,  (int)statTime);
	printf("\t T2 messages count total: %6d\n",t2MsgtotalCount );
	fprintf(pmlogf, "\t T2 messages count total: %6d\n", t2MsgtotalCount);
	printf("\t T2 count total: %6d - T2 rate total: %4f\n",t2totalCount, t2rateTotal );
	fprintf(pmlogf, "\t T2 count total: %6d - T2 rate total: %4f\n", t2totalCount, t2rateTotal);
	printf("%s - errors in received T2s: %d\n", __FUNCTION__, t2errorCount);
	fprintf(pmlogf, "\t errors in received T2s: %d\n", t2errorCount);
*/
}

void doT3Statistics() {
	if (t3car.totalCount == 0) {
		fprintf(pmlogf, "\t T3 messages count total: %6d => no rates available\n", t3car.totalCount);
	} else {
		t3car.calcNow();
		t3car.print();
		t3car.print2File(pmlogf);		// printf to the given file
	}
	t3car.resetActNow();

/*
	if (t3totalCount == 0) return;
	time_t statTime = time(NULL);
	t3rateTotal = (double) t3totalCount/difftime(statTime, t3StartTime);
	printf("%s - doing T3 statistics at %d:\n", __FUNCTION__, (int)statTime);
	fprintf(pmlogf, "%s - %s - doing T3 statistics at %d:\n", getTimeStringNow(), __FUNCTION__,  (int)statTime);
	printf("\t T3 messages count total: %6d\n",t3MsgtotalCount );
	fprintf(pmlogf, "\t T3 messages count total: %6d\n", t3MsgtotalCount);
	printf("\t T3 count total: %6d - T3 rate total: %4f\n",t3totalCount, t3rateTotal );
	fprintf(pmlogf, "\t T3 count total: %6d - T3 rate total: %4f\n", t3totalCount, t3rateTotal);
	printf("%s - errors in received T3s: %d\n", __FUNCTION__, t3errorCount);
	fprintf(pmlogf, "\t errors in received T3s: %d\n", t3errorCount);
	*/
}

int getIndexFromLSID(int id) {
	if (id < 1 || id > LS_MAX_ID) {
		llout0<<__FUNCTION__<<" - invalid station ID: >>"<<id<<"<< error!\n";
		fprintf(pmlogf, "%s - %s - invalid station ID: >>%d<< error!\n", getTimeStringNow(), __FUNCTION__, id);
	} else {
		for (int i=0;i<MAX_LS;i++) {
			if (id == LsConn[i].lsid) {
				return i;
			}
		}
	}
	return -1;
}

#ifdef USE_RC
void forwardEvToAevb(AMSG *amsgp) {				// at the moment, send each paket separately. Collecting AMSGs and filling up buffers may be implemented later
	if (EbConn.sock == NULL) return;
#ifndef USE_RC5
	RcBuffer16   outputBuffer = NULL;
#else
	UINT16   *outputBuffer = NULL;
#endif
	outputBuffer = RcSocketGetOutputBufferToBeFilled (EbConn.sock);
	if ( outputBuffer == NULL ) {
	  llout0<<__FUNCTION__<<" - RcSocketGetOutputBufferToBeFilled() returned NULL!\n";
	  return;
	}
#ifndef USE_RC5
	UINT16* buffPoint = RcBuffer16GetWords ( outputBuffer );
#else
	UINT16* buffPoint = outputBuffer+1;
#endif

	if (buffPoint == NULL) {
	//      llout0<<"sendT3Message() - RcBuffer16GetWords() returned NULL!"<<endl;
	  llout1<<__FUNCTION__<<" - RcBuffer16GetWords() returned NULL!\n";
	  return;
	}

	memcpy(buffPoint, amsgp, amsgp->length*sizeof(UINT16));
#ifndef USE_RC5
	RcBuffer16SetUsedSize ( outputBuffer, amsgp->length);// size is count of UINT16
#else
	outputBuffer[0] = amsgp->length;		// size is count of UINT16
#endif
//****** JLB 25/09/12 Start
                        if ( !( buffPoint[3] % EVNO_MON ) ) printf ( "pkt T3Evt(%dw): ls%d ev%d sec %d\n", amsgp->length, 0xff & buffPoint[4], buffPoint[3], 0xff & buffPoint[6]);
//****** JLB 25/09/12 End
	RcSocketSetOutputBufferFilled(EbConn.sock);

	llout1<<__FUNCTION__<<" - sent message with UINT16count "<<amsgp->length<<" to AEVB \n";
//	fprintf(pmlogf, "%s - forwardEvToAevb() - sent message with UINT16count %d to AEVB \n", getTimeStringNow(), amsgp->length);


}
#else
void forwardEvToAevb(AMSG *amsgp) {				// at the moment, send each paket separately. Collecting AMSGs and filling up buffers may be implemented later
	int written;
	if (amsgp == NULL) {
		llout1<<__FUNCTION__<<" - got null pointer!!!\n";
		return;
	}
	if (EbConn.sock != 0) {
		UINT16 msglength = amsgp->length*sizeof(UINT16);
		memcpy(msgSndBuff, &msglength, sizeof(msglength));
		memcpy((msgSndBuff+sizeof(msglength)), amsgp, msglength);
		if ((written = WriteSockMsg(EbConn.sock, (Msg*)msgSndBuff)) != 0) {
			llout1<<__FUNCTION__<<" - error writing to socket!\n";
			if (written < 0) {
				if (isSocketError(EbConn.sock)) {
					socketHasError(&EbConn);
					disconnectSocket(&EbConn);
				}
			} else {
				llout1<<__FUNCTION__<<" - couldn't write all bytes to socket, "<<(msglength-written)<<" of length "<<msglength<<" left, but no error?\n";
				fprintf(pmlogf, "%s - %s - couldn't write all bytes to socket, %d of length %d left, but no error?\n", getTimeStringNow(), __FUNCTION__, (msglength-written), msglength);
			}
		}
		llout1<<__FUNCTION__<<" - sent message with UINT16count "<<amsgp->length<<" to AEVB \n";
//		fprintf(pmlogf, "%s - forwardEvToAevb() - sent message with UINT16count %d to AEVB \n", getTimeStringNow(), amsgp->length);
	} else {
		llout1<<__FUNCTION__<<" - Eb not connected, couldn't forward event msg\n";
		fprintf(pmlogf, "%s - %s - Eb not connected, couldn't forward event msg\n", getTimeStringNow(), __FUNCTION__);
	}
}
/** not yet implemented to use this function - would need plein buffers!!!
void forwardEvToAevb(Msg *msgp) {
	int written;
	if (msgp == NULL) {
		printf("%s - got null pointer!!!\n", __FUNCTION__);
		return;
	}
	if (EbConn.sock != 0) {
		if ((written = WriteSockMsg(EbConn.sock, msgp)) != 0) {
			printf("%s - error writing to socket!\n", __FUNCTION__);
			if (written < 0) {
				if (isSocketError(EbConn.sock)) {
					socketHasError(&EbConn);
					disconnectSocket(&EbConn);
				}
			} else {
				printf("%s - couldn't write all bytes to socket, %d of length %d left, but no error?\n", __FUNCTION__, written, msgp->length);
				fprintf(pmlogf, "%s - %s - couldn't write all bytes to socket, %d of length %d left, but no error?\n", getTimeStringNow(), __FUNCTION__, written, msgp->length);
			}
		}
		fprintf(pmlogf, "%s - %s - forwarded %d bytes of data to aevb\n", getTimeStringNow(), __FUNCTION__, msgp->length);
	} else {
		printf("%s - Eb not connected, couldn't forward event msg\n", __FUNCTION__);
		fprintf(pmlogf, "%s - %s - Eb not connected, couldn't forward event msg\n", getTimeStringNow(), __FUNCTION__);
	}
}
*/
#endif

void forwardT2toT3(AMSG *amsgp) {				// at the moment, send each packet separately. Collecting AMSGs and filling up buffers may be implemented later
	if (amsgp == NULL) {
		llout1<<__FUNCTION__<<" - got null pointer!!!\n";
		fprintf(pmlogf, "%s - got null pointer!!!\n", __FUNCTION__);
		return;
	}
#ifdef USE_RC
	if (T3Conn.sock == NULL) return;
	else {
#ifndef USE_RC5
		RcBuffer16   outputBuffer = NULL;
#else
		UINT16   *outputBuffer = NULL;
#endif
		outputBuffer = RcSocketGetOutputBufferToBeFilled (T3Conn.sock);
		if ( outputBuffer == NULL ) {
		  llout1<<__FUNCTION__<<" - RcSocketGetOutputBufferToBeFilled() returned NULL!\n";
		  fprintf(pmlogf, "forwardT2toT3() - RcSocketGetOutputBufferToBeFilled() returned NULL!\n");
		  return;
		}
#ifndef USE_RC5
		UINT16* buffPoint = RcBuffer16GetWords ( outputBuffer );
#else
		UINT16* buffPoint =  ( outputBuffer+1 );
#endif
		if (buffPoint == NULL) {
		  llout1<<__FUNCTION__<<" - RcBuffer16GetWords() returned NULL!\n";
		  fprintf(pmlogf, "forwardT2toT3() - RcBuffer16GetWords() returned NULL!\n");
		  return;
		}

		memcpy(buffPoint, amsgp, (amsgp->length*sizeof(UINT16)));
#ifndef USE_RC5
		RcBuffer16SetUsedSize ( outputBuffer, amsgp->length);// size is count of UINT16
#else
		outputBuffer[0] = amsgp->length;// size is count of UINT16
#endif
		// JLB Start 19/2/13
		UINT32  second = *(UINT32*)( buffPoint + 3 );
		T2SSEC* subSecond1 = (T2SSEC*) ( buffPoint + 5 );
		UINT32  nanoSecond1 = T2NSEC ( subSecond1 );
		T2SSEC* subSecond2 = (T2SSEC*) ( buffPoint + buffPoint[0] - 2 );
		UINT32  nanoSecond2 = T2NSEC ( subSecond2 );
		if ( 0xff & buffPoint[2] == LSID_MON ) printf ( "pkt T2 with %d t2: ls%d %d (%d) [%d...%d]\n", ( buffPoint[0] - 5 ) / 2, 0xff & buffPoint[2], second, 0xff & second, nanoSecond1, nanoSecond2 );
		/*
		for (int i=0; i<amsgp->length; i++) {
//			  printf("forwardT2toT3() - outputBuffer[%d] = %hu\n", i, outputBuffer[i]);
//			  fprintf(pmlogf, "forwardT2toT3() - outputBuffer[%d] = %hu\n", i, outputBuffer[i]);
		}
		*/
		// JLB Stop 19/2/13.
		RcSocketSetOutputBufferFilled(T3Conn.sock);
#else
	static bool t3NotConnectedMsgSent = false;
	static time_t t3NotConnectedMsgSentTime = 0;
	if (T3Conn.sock == 0) {
		if (t2sNotSentCar.startTime == 0) t2sNotSentCar.startTime = time(NULL);		// first time, T2s couldn't be sent
		t2sNotSentCar.actCount+= getT2countFromAmsg(amsgp);
		t2sNotSentCar.totalCount+= getT2countFromAmsg(amsgp);
		if (t3NotConnectedMsgSent == true) if (difftime(time(NULL), t3NotConnectedMsgSentTime) > 60) t3NotConnectedMsgSent = false;
		if (t3NotConnectedMsgSent == false) {
//			llout0<<"difftime(time(NULL), t3NotConnectedMsgSentTime) [s]: "<<difftime(time(NULL),t3NotConnectedMsgSentTime)<<endl;
			t3NotConnectedMsgSentTime = time(NULL);
			llout0<<__FUNCTION__<<getTimeStringNow()<<" : - t3 not connected, couldn't forward T2 msg\n";
			fprintf(pmlogf, "%s - %s - t3 not connected, couldn't forward T2 msg\n", getTimeStringNow(), __FUNCTION__);
			t3NotConnectedMsgSent = true;
		}
		return;
	} else {
		t3NotConnectedMsgSent = false;
		int written;
		UINT16 msglength = amsgp->length*sizeof(UINT16);
		memcpy(msgSndBuff, &msglength, sizeof(msglength));
		memcpy((msgSndBuff+sizeof(msglength)), amsgp, msglength);
		if ((written = WriteSockMsg(T3Conn.sock, (Msg*)msgSndBuff)) != 0) {
			llout0<<__FUNCTION__<<" - error writing to T3 socket!\n";
			fprintf(pmlogf, "%s - %s - error writing to T3 socket!\n", getTimeStringNow(),  __FUNCTION__);
			if (written < 0) {
				if (isSocketError(T3Conn.sock)) {
					socketHasError(&T3Conn);
					disconnectSocket(&T3Conn);
				}
			} else {
				llout1<<__FUNCTION__<<" - couldn't write all bytes to socket, "<<(amsgp->length-written)<<" of length "<<amsgp->length<<" left, but no error?\n";
				fprintf(pmlogf, "%s - %s - couldn't write all bytes to socket, %d of length %d left, but no error?\n", getTimeStringNow(), __FUNCTION__, (amsgp->length-written), amsgp->length);
			}
		}
#endif

//		printf("%s - %s  - sent message with UINT16count %d to T3 \n", getTimeStringNow(), __FUNCTION__, amsgp->length);
//		fprintf(pmlogf, "%s - %s  - sent message with UINT16count %d to T3 \n", getTimeStringNow(), __FUNCTION__, amsgp->length);
	}
}

void pmLog(char *fmt, ...)		/* AW: writing message to log and/or syslog */
{
	int length;
	va_list ap;

	FUNC_BEGIN

	va_start(ap,fmt);
	length = vsprintf(logbuff,fmt,ap)+1;
	va_end(ap);

	llout(logbuff);

	if (useSyslog) {
		switch(loglevel) {		// map the loglevels of lloglib to the AERA syslog levels
			case (0):
				cout<<logbuff<<endl;
				fprintf(pmlogf, "%s", logbuff);
				break;
			case (1):
				cout<<logbuff<<endl;
				fprintf(pmlogf, "%s", logbuff);
				break;
			case (2):
				cout<<logbuff<<endl;
				fprintf(pmlogf, "%s", logbuff);
				break;
			case (3):
				cout<<logbuff<<endl;
				fprintf(pmlogf, "%s", logbuff);
				break;
			default:
				break;
		}
	}
	FUNC_END
}

void processEvMsg(AMSG *amsgp) {
	if ((amsgp->tag != LS_EVENT) && (amsgp->tag != LS_NO_EVENT) && (amsgp->tag != LS_GET_EXT_SD_EVENT) && (amsgp->tag != LS_GET_EXT_GUI_EVENT) && (amsgp->tag != LS_GET_EXT_FD_EVENT)) {
		llout1<<__FUNCTION__<<" - received tag "<<amsgp->tag<<" but processing only the LS_EVENT tags => processing aborted!\n";
		fprintf(pmlogf, "%s - %s - received tag %d but processing only the LS_EVENT tags => processing aborted!\n", getTimeStringNow(), __FUNCTION__, amsgp->tag);
		return;
	}

	EVENTBODY *evbody = (EVENTBODY*) amsgp->body;
	int runevno = evbody->event_nr;
	int lsid = LSPOS(evbody->LS_id);
	int lshwtype = LSHWTYPE(evbody->LS_id);
	int lshwversion = LSHWVERSION(evbody->LS_id);
	int lsTime = evbody->GPSseconds;

	if ((amsgp->tag == LS_EVENT) || (amsgp->tag != LS_GET_EXT_SD_EVENT) || (amsgp->tag != LS_GET_EXT_GUI_EVENT) || (amsgp->tag != LS_GET_EXT_FD_EVENT)) {
		llout0<<"processEvMsg() : received data with tag "<<amsgp->tag<<" and length "<<dec<<evbody->length<<" of ls/hwtype/hwvers "<<dec<<lsid<<"/"<<lshwtype<<"/"<<lshwversion<<" with running event no "<<runevno<<" and GPS time "<<lsTime<<" at "<<getSecondNow()<<","<<getMicrosecondNow()<<endl;
		fprintf(pmlogf, "%s - %s - received data with tag %d and length %d of ls/hwtype/hwvers %d/%d/%d with running event no %d and GPS time %d at %d, %d\n", getTimeStringNow(), __FUNCTION__, amsgp->tag, evbody->length, lsid, lshwtype, lshwversion, runevno, lsTime, getSecondNow(), getMicrosecondNow());
//		llout1<<dumpLSData(evbody)<<endl;
//		unsigned char* ucp = (unsigned char*) evbody->info_ADCbuffer;
		for (int z = 0;z<50;z++) {
	//		llout1<<"unsigned char ["<<dec<<z<<"] : of data as hex: 0x"<<hex<<setw(2)<<setfill('0')<<(int)*ucp++<<endl;
		}
	} else if (amsgp->tag == LS_NO_EVENT) {
		// be careful: if LS_NO_EVENT, the body of the AMSG has NOT the EVENTBODY format
		LS_NO_EVENTBODY *nevbody = (LS_NO_EVENTBODY*) amsgp->body;
		llout1<<"processEvMsg() : received LS_NO_EVENT = empty data with length "<<amsgp->length<<" of ls/hwtype/hwvers "<<dec<<lsid<<"/"<<lshwtype<<"/"<<lshwversion<<" with running event no "<<nevbody->event_nr<<" and GPS time "<<lsTime<<endl;
		fprintf(pmlogf, "%s - %s - received LS_NO_EVENT = empty data with length length %d of ls/hwtype/hwvers %d/%d/%d with running event no %d and GPS time %d at %d, %d\n", getTimeStringNow(), __FUNCTION__, amsgp->length, lsid, lshwtype, lshwversion, nevbody->event_nr, lsTime, getSecondNow(), getMicrosecondNow());
	} else {
		llout1<<getTimeStringNow()<<" - processEvMsg() - unexpected tag "<<amsgp->tag<<", ignoring message"<<endl;
		fprintf(pmlogf, "%s - %s - unexpected tag %d, ignoring message\n", getTimeStringNow(), __FUNCTION__, amsgp->tag);
		return;
	}

	// here we have a valid data packet
	int index = getIndexFromLSID(lsid);
	if (index == -1) {
		llout0<<__FUNCTION__<<" - LSID "<<lsid<<" not found in connected stations - could not increment data count\n";
		fprintf(pmlogf, "%s - %s - LSID %d not found in connected stations - could not increment data count\n", getTimeStringNow(), __FUNCTION__, lsid);
	} else {
		if (lsDataCar[index].startTime == 0) lsDataCar[index].startTime = time(NULL);
		lsDataCar[index].totalCount++;
		lsDataCar[index].actCount++;
	}

	if (writeT3File) {	// dump the T3 to a file. One file per LS
		int connIndex = getIndexFromLSID(lsid);
		if (connIndex == -1) {
			llout1<<__FUNCTION__<<" - LSID "<<lsid<<" not found in connected stations - T3 not saved\n";
			fprintf(pmlogf, "%s - %s - LSID %d not found in connected stations - T3 not saved\n", getTimeStringNow(), __FUNCTION__, lsid);
		} else {
			if (t3Files[connIndex] != NULL) {
				// dump the complete event body
				fwrite(evbody, evbody->length*sizeof(UINT16), 1, t3Files[connIndex]);
			}
		}
	}
}

#ifdef USE_RC
#ifdef USE_RC5
int processRcBuffer(UINT16 *buffer) {
#else
int processRcBuffer(RcBuffer16 buffer) {
#endif
	if (buffer == NULL) {
		llout1<<__FUNCTION__<<" : got NULL buffer, returning\n";
		return -1;
	}

	char msgBuffer[256];
	bool rcBufferQuickLookCalled = false;
	int t3InBufferCount = 0;
	int lsInBufferCount = 0;

	bool receivedAlive = false;
	bool receivedAliveAck = false;

//	block_signals(1);

	// starting with RcLight a buffer may contain more than 64k words (range of UINT16)
#ifdef USE_RC5
	int bShortCount = buffer[0]; //CT UNIT16
#else
	int bShortCount = RcBuffer16GetUsedSize(buffer); //CT UNIT16
#endif
	int bsize = bShortCount * sizeof(UINT16);
//	printf("processRcBuffer() : processing buffer of size %d (wordcount = %hu)\n", bsize, bShortCount);
//	fprintf(pmlogf, "%s - processRcBuffer() : processing buffer of size %d (wordcount = %hu)\n", getTimeStringNow(), bsize, bShortCount);
//****** JLB 19/11/12 start
/*
	if (bShortCount >= PMSV_OUTPUT_BUFFER_SIZE) {
//		printf("%s - processRcBuffer() - buffer size %d >= PMSV_OUTPUT_BUFFER_SIZE %d! Processing the buffer is skipped!\n", getTimeStringNow(), bShortCount, PMSV_OUTPUT_BUFFER_SIZE);
		fprintf(pmlogf, "%s - processRcBuffer() - buffer size %d >= PMSV_OUTPUT_BUFFER_SIZE %d! Processing the buffer is skipped!\n", getTimeStringNow(), bShortCount, PMSV_OUTPUT_BUFFER_SIZE);
		return -1;
	}
	if (bShortCount<= 1) {
//		printf("%s - processRcBuffer() - buffer size %d <= PMSV_OUTPUT_BUFFER_SIZE %d! Processing the buffer is skipped!\n", getTimeStringNow(), bShortCount, PMSV_OUTPUT_BUFFER_SIZE);
		fprintf(pmlogf, "%s - processRcBuffer() - buffer size %d <= PMSV_OUTPUT_BUFFER_SIZE %d! Processing the buffer is skipped!\n", getTimeStringNow(), bShortCount, PMSV_OUTPUT_BUFFER_SIZE);
		return -1;
	}
*/
	if (bShortCount >= MAX_AERA_BUF_LEN) {
		printf("%s - processRcBuffer() - buffer size %d >= MAX_AERA_BUF_LEN %d! Processing the buffer is skipped!\n", getTimeStringNow(), bShortCount, MAX_AERA_BUF_LEN);
		fprintf(pmlogf, "%s - processRcBuffer() - buffer size %d >= MAX_AERA_BUF_LEN %d! Processing the buffer is skipped!\n", getTimeStringNow(), bShortCount, MAX_AERA_BUF_LEN);
		return -1;
	}
	if (bShortCount<= 1) {
		printf("%s - processRcBuffer() - buffer size %d <= MAX_AERA_BUF_LEN %d! Processing the buffer is skipped!\n", getTimeStringNow(), bShortCount, MAX_AERA_BUF_LEN);
		fprintf(pmlogf, "%s - processRcBuffer() - buffer size %d <= MAX_AERA_BUF_LEN %d! Processing the buffer is skipped!\n", getTimeStringNow(), bShortCount, MAX_AERA_BUF_LEN);
		return -1;
	}
//****** 19/11/12 start

#ifdef USE_RC5
	UINT16* buffPoint = (buffer+1);
#else
	UINT16* buffPoint = RcBuffer16GetWords ( buffer );
#endif
	if (buffPoint == NULL) {
		 llout0<<__FUNCTION__<<" - RcBuffer16GetWords() returned NULL!\n";
		 fprintf(pmlogf, "processRcBuffer() - RcBuffer16GetWords() returned NULL!\n");
		 return -1;
	}
	// loop through content of buffer
	int shortsProcessed = 0; //CT UINT16
	AMSG * amsgp = (AMSG*) buffPoint;
	static UINT16 lasttag=9999, lastlength=9999, lastBuffSize=9999;
	static int firstT3Cnt = 0, firstLSCnt = 0;
	int length; //CT from UINT16
	UINT16 tag;
	time_t now = time(NULL);

	while (shortsProcessed < bShortCount) {
		length = amsgp->length;
		if (length == 0) {
//			printf("%s - processRcBuffer() - Message length == 0?! Processing the buffer is skipped!\n", getTimeStringNow());
			fprintf(pmlogf, "%s - processRcBuffer() - Message length == 0?! Processing the buffer is skipped!\n", getTimeStringNow());
			break;
		}
		tag = amsgp->tag;
//		printf("processRcBuffer() - processing message in buffer of length %d and tag %hu\n", length, tag);
//		fprintf(pmlogf, "%s - processRcBuffer() - processing message in buffer of length %d and tag %hu\n", getTimeStringNow() , length, tag);
//****** JLB 19/11/12 start

		if ((shortsProcessed + length) > MAX_AERA_BUF_LEN) {
//			printf("%s - processRcBuffer() -Message length exceeds buffer boundary! (shortsProcessed + next msg length) = %hu > MAX_AERA_BUF_LEN = %hu. Processing the buffer is skipped!\n", getTimeStringNow(), (shortsProcessed + length), MAX_AERA_BUF_LEN);
			fprintf(pmlogf, "%s - processRcBuffer() -Message length exceeds buffer boundary! (shortsProcessed + next msg length) = %hu > MAX_AERA_BUF_LEN = %hu. Processing the buffer is skipped!\n", getTimeStringNow(), (shortsProcessed + length), MAX_AERA_BUF_LEN);
			return -1;
		}

//****** 19/11/12 start
		switch (tag) {
		case LS_T2:
			forwardT2toT3(amsgp);
			processT2Msg(amsgp);
			break;
		case LS_EVENT:
		case LS_NO_EVENT:
		case LS_GET_EXT_SD_EVENT:
		case LS_GET_EXT_GUI_EVENT:
		case LS_GET_EXT_FD_EVENT:
			forwardEvToAevb(amsgp);
			processEvMsg(amsgp);
			break;
		case T3_EVENT_REQUEST_LIST:
		case T3_EXT_SD_EVENT_REQUEST_LIST:
		case T3_EXT_GUI_EVENT_REQUEST_LIST:
		case T3_EXT_FD_EVENT_REQUEST_LIST:
			if (t3car.startTime == 0) t3car.startTime = time(NULL);
			t3MsgtotalCount++;
//			printf("received tag %d from T3, processing ...\n",  amsgp->tag);
//			fprintf(pmlogf, "%s - %s - received tag %d from T3, processing ...\n", getTimeStringNow(), __FUNCTION__,  amsgp->tag );
//****** JLB 25/09/12 Start
			if ( !( buffPoint[2] % EVNO_MON ) )
			{
			    int nT3r, iT3r;
			    nT3r = ( bShortCount - 3 ) / 3;
			    printf ( "pkt T3Lst with %d ls: ev%d\n", nT3r, buffPoint[2] );
			    for ( iT3r = 0; iT3r < nT3r; iT3r++ ) printf ( "T3Lst %d %d\n", buffPoint[3 + 3 * iT3r], 0xff & buffPoint[4 + 3 * iT3r] );
			}
//****** JLB 25/09/12 End
			// distribute T3 message to stations
			sendT3reqToLS(amsgp);
			processT3Msg(amsgp);
			break;
		case LS_PARAM:
//			saveLsParam(amsgp);
			break;
		case GUI_UPDATE_DB:
		case GUI_INITIALIZE:
		case GUI_START_RUN:
		case GUI_STOP_RUN:
		case GUI_TRIGGER:
		case GUI_DELETE_RUN:
//			processGuiMsg(amsgp);
			break;
		case PROMPT_TO_EVENTBUILDER:		// see AeraTags.h
//			processPromptMsg(amsgp);
			break;
		case ALIVE:
			printf("received ALIVE, returning RECEIVED_ALIVE\n");
			fprintf(pmlogf, "%s - %s - received ALIVE, returning RECEIVED_ALIVE\n", getTimeStringNow(), __FUNCTION__);
			receivedAlive = true;
			break;
		case ALIVE_ACK:
			printf("received ALIVE_ACK\n");
			fprintf(pmlogf, "%s - %s - received ALIVE_ACK\n", getTimeStringNow(), __FUNCTION__);
			receivedAliveAck = true;
			break;
		default:
			llout1<<"processRcBuffer() - no processor (yet?) for processing message with tag "<<tag<<"(length = "<<length<<", buffsize = "<<bShortCount<<". Last tag was "<<lasttag<<"(lastlength = "<<lastlength<<", buffsize = "<<lastBuffSize<<endl;
			fprintf(pmlogf, "processRcBuffer() - no processor (yet?) for processing message with tag %hu / length = %hu, buffsize = %hu. Last tag was %hu, lastlength = %hu, lastbuffsize = %hu\n", tag, length, bShortCount, lasttag, lastlength, lastBuffSize);
			break;
		}
		lasttag = tag;
		lastlength = length;
		shortsProcessed += length;
		// goto next message in buffer, or exit
		if (shortsProcessed < bShortCount) {
//			llout1<<"processRcBuffer() - there are "<<bShortCount - shortsProcessed<<" shorts left in buffer, processing next message ..."<<endl;
//			fprintf(pmlogf, "%s - processRcBuffer() - there are %hu shorts left in buffer, processing next message ...\n", getTimeStringNow(), (bShortCount - shortsProcessed));
			UINT16* tmp = (UINT16 *)amsgp;
			tmp += length;
			amsgp = (AMSG*) (tmp);
		} else {
//			llout1<<"processRcBuffer() - bShortCount = "<<bShortCount<<", shortsProcessed = "<<shortsProcessed<<" => nothing more in buffer"<<endl;
//			fprintf(pmlogf, "%s - processRcBuffer() - bShortCount = %hu, shortsProcessed = %hu => nothing more in buffer\n", getTimeStringNow(), bShortCount, shortsProcessed);
			break;
		}
	}
	rcBufferQuickLookCalled = false;
	lastBuffSize = bShortCount;

	if (receivedAlive) return ALIVE;
	if (receivedAliveAck) return ALIVE_ACK;
	else return 0;

//	block_signals(0);
}
#else
/**
 * AW, 19.11.2012: for ALIVE-Msgs (see transport layer definitions), a return value is necessary, as no socket is known any more on processing the msg ...
 */
int processMsg(Msg *msgp) {
	AMSG *amsgp = (AMSG*) msgp->data;		// this points to the first AMSG in Msg
	UINT16 subtag;
	int t3CountInBuffer = 0;
	bool receivedAlive = false;
	bool receivedAliveAck = false;

	if (msgp == NULL) {
		llout0<<__FUNCTION__<<" - received NULL pointer!\n";
		fprintf(pmlogf, "%s - %s - received NULL pointer!\n", getTimeStringNow(), __FUNCTION__);
		return -1;
	}

	if (msgp->length <= 2) {
//		printf("%s - received message with invalid length %d => processing aborted!\n", __FUNCTION__, msgp->length);
		fprintf(pmlogf, "%s - %s - received message with invalid length %d => processing aborted!\n", getTimeStringNow(), __FUNCTION__, msgp->length);
		return -1;
	}

	// loop through content of Msg, which may contain several Amsg
	int shortsProcessed = 0, length;
	int bShortCount = msgp->length/sizeof(UINT16);

	// several AMSG may be in one Msg, but the type should not be mixed to speed up forwarding! Forwarding decision is made on first tag!
	// the separate AMSG in the Msg are only processed after forwarding for debug and statistics!!!
	while (shortsProcessed < bShortCount) {
		length = amsgp->length;
		if (length == 0) {
			fprintf(pmlogf, "%s - %s - AMessage length == 0?! Processing the buffer is skipped!\n", getTimeStringNow(), __FUNCTION__);
			llout0<<getTimeStringNow()<<" - "<<__FUNCTION__<<"AMessage length == 0?!  Processing the buffer is skipped!"<<endl;
			return -1;
		}
//		llout0<<getTimeStringNow()<<" - "<<__FUNCTION__<<" - processing Amessage in buffer of length "<<length<<" and tag "<<amsgp->tag<<endl;
		if ((shortsProcessed + length) > MAX_AERA_BUF_LEN) {
			fprintf(pmlogf, "%s - %s - Message length exceeds buffer boundary! (shortsProcessed + next msg length) = %uhd > MAX_AERA_BUF_LEN = %uhd. Processing the buffer is skipped!\n", getTimeStringNow(), __FUNCTION__, (shortsProcessed + length), MAX_AERA_BUF_LEN);
			llout0<<getTimeStringNow()<<" - "<<__FUNCTION__<<" - Message length exceeds buffer boundary! (shortsProcessed + next msg length) = "<<(shortsProcessed + length)<<" > MAX_AERA_BUF_LEN = "<<MAX_AERA_BUF_LEN<<". Processing the buffer is skipped!"<<endl;
			return -1;
		}
//		printf("amsg = %d \n", getAmsg);
//		printf("AMSG length = %d\n", getAmsg->length);
//		printf("AMSG tag    = %d\n", getAmsg->tag);
		switch (amsgp->tag) {
			case LS_T2:
				forwardT2toT3(amsgp);
				processT2Msg(amsgp);
				break;
			case LS_EVENT:
			case LS_NO_EVENT:
			case LS_GET_EXT_SD_EVENT:
			case LS_GET_EXT_GUI_EVENT:
			case LS_GET_EXT_FD_EVENT:
				t3CountInBuffer++;
				forwardEvToAevb(amsgp);
				processEvMsg(amsgp);
				break;
			case T3_EVENT_REQUEST_LIST:
			case T3_EXT_SD_EVENT_REQUEST_LIST:
			case T3_EXT_GUI_EVENT_REQUEST_LIST:
			case T3_EXT_FD_EVENT_REQUEST_LIST:
					if (t3car.startTime == 0) t3car.startTime = time(NULL);
					t3MsgtotalCount++;
					// distribute T3 message to stations
					sendT3reqToLS(amsgp);
					#ifdef KIT_COMMS
						if (sendT3reqList == true) {
							sendT3reqToLsKitComms(amsgp);
						}
					#endif //KIT_COMMS
					processT3Msg(amsgp);
					break;
			case PROMPT_TO_EVENTBUILDER:
				printf("tag from prompt: PROMPT_TO_EVENTBUILDER = %d, = %d. Processing ...\n", PROMPT_TO_EVENTBUILDER, amsgp->tag);
				subtag = amsgp->body[0];
				switch (subtag) {
					case START_RUN:			// LS_START:
						printf("received start from prompt, starting daq ...\n");
						fprintf(pmlogf, "%s - %s - received start from prompt, starting daq ...\n", getTimeStringNow(), __FUNCTION__);
						startDaq();
						break;
					case STOP_RUN:			// LS_STOP:
						printf("received stop from prompt, stopping daq ...\n");
						fprintf(pmlogf, "%s - %s - received stop from prompt, stopping daq ...\n", getTimeStringNow(), __FUNCTION__);
						stopDaq();
						break;
					default:
						printf("%s - tag %d not processed by postmaster, msg discarded!\n", __FUNCTION__,  subtag);
						fprintf(pmlogf, "%s - %s - tag %d not processed by postmaster, msg discarded!\n", getTimeStringNow(), __FUNCTION__,  subtag);
						break;
				}
			case ALIVE:
				printf("received ALIVE, returning RECEIVED_ALIVE\n");
				fprintf(pmlogf, "%s - %s - received ALIVE, returning RECEIVED_ALIVE\n", getTimeStringNow(), __FUNCTION__);
				receivedAlive = true;
				break;
			case ALIVE_ACK:
				printf("received ALIVE_ACK\n");
				fprintf(pmlogf, "%s - %s - received ALIVE_ACK\n", getTimeStringNow(), __FUNCTION__);
				receivedAliveAck = true;
				break;
			default:
				printf("%s - tag %d not processed in package, mixed up tags??? msg discarded!\n", __FUNCTION__, amsgp->tag);
				fprintf(pmlogf, "%s - %s - tag %d not processed by postmaster, msg discarded!\n", getTimeStringNow(), __FUNCTION__ ,  amsgp->tag);
				break;
		}
		shortsProcessed += length;
		// goto next message in buffer, or exit
		if (shortsProcessed < bShortCount) {
//			collout1getTimeStringNow()<<" - "<<__FUNCTION__<<" - there are "<<bShortCount - shortsProcessed<<" shorts left in buffer, processing next message ..."<<endl;
			UINT16* tmp = (UINT16 *)amsgp;
			tmp += length;
			amsgp = (AMSG*) (tmp);
		} else {
//			llout1<<getTimeStringNow()<<" - "<<__FUNCTION__<<" - bShortCount = "<<bShortCount<<", shortsProcessed = "<<shortsProcessed<<" => nothing more in buffer"<<endl;
			break;
		}
	}

	if (t3CountInBuffer > 0) {
//		printf("%s - processed %d T3s in this msg of length %d bytes\n", __FUNCTION__, t3CountInBuffer, msgp->length);
//		fprintf(pmlogf, "%s - %s - processed %d T3s in this msg of length %d bytes\n", getTimeStringNow(), __FUNCTION__ , t3CountInBuffer, msgp->length);
	}


//	FUNC_END
	if (receivedAlive) return ALIVE;
	if (receivedAliveAck) return ALIVE_ACK;
	else return 0;
}
#endif

void processT2Msg(AMSG *amsgp) {
	if (amsgp->tag != LS_T2) {
//		printf("%s - received tag %d but processing only LS_T2 => processing aborted!\n", __FUNCTION__, amsgp->tag);
		fprintf(pmlogf, "%s - %s - received tag %d but processing only LS_T2 => processing aborted!\n", getTimeStringNow(), __FUNCTION__, amsgp->tag);
		return;
	}
	static uint32_t prevT2Second = 0;

//	printf("amsg = %d \n", getAmsg);
//	printf("AMSG length = %d\n", getAmsg->length);
//	printf("AMSG tag    = %d\n", getAmsg->tag);

//	printf("%s - %s - received T2 tag %d \n", getTimeStringNow(), __FUNCTION__, amsgp->tag);
//	fprintf(pmlogf, "%s - %s - received T2 tag %d \n", getTimeStringNow(), __FUNCTION__, amsgp->tag);
	if (t2car.startTime == 0) t2car.startTime = time(NULL);
	t2MsgtotalCount++;
	UINT16 *msbp = (UINT16*)amsgp;
//	for (int j=0; j<amsgp->length+1;j++) {
	for (int j=0; j<10;j++) {
//		printf("Amsg(%d) : %hu = 0x%X\n", j, msbp[j], msbp[j]);
	}
	// process the single AMSG
	T2BODY *t2bp = (T2BODY*) amsgp->body;
//	uint32_t gpstime = GET_T0_FROM_T2MSG(((int)t2bp->t0));
//	uint32_t gpstime = SWAP32(((int)t2bp->t0));
	uint32_t gpstime = (((int)t2bp->t0[0]) | ((t2bp->t0[1])<<16));

	// for a rough test of duplicate T2s we consider only T2s of the same second, so empty the T2 vector on new seconds, but do a checkfor duplicates before
	if (prevT2Second == 0) prevT2Second = gpstime;
	else if (gpstime > prevT2Second) {	// we are in next second
		printf("new GPS second: %d => checking for duplicate T2s\n", gpstime);
		checkForDuplicateT2s();
		emptyT2Vector();
		prevT2Second = gpstime;
	}

	uint32_t t2count = getT2countFromAmsg(amsgp);

//	if ((gpstime % 100) == 0) llout1<<"CHECKDOUBLET2: processT2Msg() - received and forwarded T2 message with UINT16count = "<<amsgp->length<<" and GPS time "<<gpstime<<" from LS "<<t2bp->LS_id<<endl;
//	printf("%s - %s - length = %5d, tag = %3d, lsid = %3d, t2count = %5d, second = %d, 8-bit second %d\n", getTimeStringNow(), __FUNCTION__, amsgp->length, amsgp->tag, t2bp->LS_id, t2count, gpstime, (gpstime & 0xFF));

	fprintf(pmlogf, "%s - %s - length = %5d, tag = %3d, lsid = %3d, t2count = %5d, second = %d, 8-bit second %d\n", getTimeStringNow(), __FUNCTION__, amsgp->length, amsgp->tag, t2bp->LS_id, t2count, gpstime, (gpstime & 0xFF));

// check T2 for consistency
	if (gpstime < MIN_GPS_TIME) {
//		printf("%s - %s - inconsistent gps second: %d\n", getTimeStringNow(), __FUNCTION__, gpstime);
		fprintf(pmlogf, "%s - %s - inconsistent gps second: %d\n", getTimeStringNow(), __FUNCTION__, gpstime);
	}

	int t2ns;
	lst2 t2tmp;
	for (int i=0; i<t2count;i++) {
		t2ns = T2NSEC((T2SSEC *)&t2bp->t2ssec[i]);
//		printf("\t T2[%3d] = %d = 0x%X\n", i, T2NSEC((T2SSEC *)&t2bp->t2ssec[i]), *(int*)(&t2bp->t2ssec[i]));		//
//		fprintf(pmlogf, "\t T2[%3d] = %d = 0x%X\n", i, T2NSEC((T2SSEC *)&t2bp->t2ssec[i]), *(int*)(&t2bp->t2ssec[i]));		//
//		fprintf(pmlogf, "\t T2[%3d] = %d ( = 0x%X)\n", i, t2bp->t2ssec[i], t2bp->t2ssec[i]);		//
		t2array[t2arrayIndex].gpssecond = gpstime;
		t2array[t2arrayIndex].nsecond = t2ns;
		t2array[t2arrayIndex].lsid = t2bp->LS_id;
		t2arrayIndex++;
	}

	t2car.totalCount += t2count;
	t2car.actCount += t2count;
	int index  =  getIndexFromLSID(t2bp->LS_id);
	if (index == -1) {
//		printf("%s - LSID %d not found in connected stations - could not increment T2 count\n", __FUNCTION__, t2bp->LS_id);
		fprintf(pmlogf, "%s - %s - LSID %d not found in connected stations - could not increment T2 count\n", getTimeStringNow(), __FUNCTION__, t2bp->LS_id);
	} else {
		if (lsT2Car[index].startTime == 0) {
			lsT2Car[index].startTime = time(NULL);
			fprintf(pmlogf, "%s - %s - set start time for station with id %d to now = %d\n",  getTimeStringNow(), __FUNCTION__, t2bp->LS_id, time(NULL));
		}
		lsT2Car[index].totalCount += t2count;
		lsT2Car[index].actCount += t2count;
		lastT2Second[index] = gpstime;
//		printf("%s - %s - now: lsT2Car[%d].totalCount = %d,  lsT2Car[%d].actCount = %d, have been increased by t2count = %5d\n", getTimeStringNow(), __FUNCTION__, index, lsT2Car[index].totalCount, index, lsT2Car[index].actCount, t2count);
//		fprintf(pmlogf, "%s - %s - now: lsT2Car[%d].totalCount = %d,  lsT2Car[%d].actCount = %d, have been increased by t2count = %5d\n", getTimeStringNow(), __FUNCTION__, index, lsT2Car[index].totalCount, index, lsT2Car[index].actCount, t2count);
	}

	T2SSEC *t2s = t2bp->t2ssec;
//	T2SSEC *t2s_2 = t2bp->t2ssec;

#define USE_SWAP
#ifdef USE_SWAP
	for (uint32_t i=0;i<t2count; i++) {
/*		UINT16 *tmpp = (UINT16*) &t2s[i];
		uint32_t tempt2 = (0xffff & tmpp[0]) | (tmpp[1]<<16);
		printf("%s - composed int %d = 0x%X\n", __FUNCTION__ , tempt2, tempt2);
		tempt2 = SWAP32(tempt2);
		printf("%s - swapped int %d = 0x%X\n", __FUNCTION__ , tempt2, tempt2);
		tempt2 = tempt2 >> 8;
		printf("%s - swapped and shifted subsecond %d = 0x%X\n", __FUNCTION__ , tempt2, tempt2);
		printf("%s - Charles subsecond %d = 0x%X\n", __FUNCTION__ , T2NSEC(t2s_2), T2NSEC(t2s_2));
		t2s_2++;
		*/
		uint32_t tmp, *tmpp = (uint32_t*) &t2s[i];
//		printf("%s - working on T2S %d = 0x%X\n", __FUNCTION__ , *tmpp, *tmpp);
//		*tmpp = SWAP_UL(*tmpp);
//		printf("%s - SWAP_UL %d = 0x%X\n", __FUNCTION__ , *tmpp, *tmpp);
		tmp = (SWAP32(*tmpp) >> 8);
//		printf("%s - swapped and shifted subsecond %d = 0x%X\n", __FUNCTION__ , tmp, tmp);
#ifdef CHECK_T2_NUMBERS
		uint32_t new_number, prev_number;
		if (i==0) {
			new_number = prev_number = *tmpp;
		} else {
			new_number =  *tmpp;
			if (new_number != prev_number +1) {
				fprintf(pmlogf, "%s - ERROR: new_number = %d, prev_number = %d => numbers are no successors!!!!!\n", __FUNCTION__, new_number, prev_number);
				t2errorCount++;
			} //else printf("%s - Number ok: new number %d is successor of prev number %d\n", __FUNCTION__, new_number, prev_number);
			prev_number = new_number;
		}
#endif
//		printf("%s - T2 %d = %d\n", __FUNCTION__, i, GET_T0_FROM_T2MSG((&(t2bp->t2ssec[i]))));
//		printf("%s - T2 %d = %hd, ADC = %d\n", __FUNCTION__, i, T2NSEC((T2SSEC*)(&t2bp->t2ssec[i])), (int)T2ADC((T2SSEC*)(&t2bp->t2ssec[i])));
	}
#else
	uint32_t new_number, prev_number;
	for (uint32_t i=0;i<t2count; i++) {
		if (i==0) {
			new_number = prev_number = *(uint32_t*)(&t2bp->t2ssec[i]);
		} else {
			new_number =  *(uint32_t*)(&t2bp->t2ssec[i]);
			if (new_number != prev_number +1) {
//				printf("%s - ERROR: new_number = %d, prev_number = %d => numbers are no successors!!!!!\n", __FUNCTION__, new_number, prev_number);
			} else prev_number = new_number;
		}
//		printf("%s - T2 %d = %d\n", __FUNCTION__, i, GET_T0_FROM_T2MSG((&(t2bp->t2ssec[i]))));
//		printf("%s - T2 %d = %hd, ADC = %d\n", __FUNCTION__, i, T2NSEC((T2SSEC*)(&t2bp->t2ssec[i])), (int)T2ADC((T2SSEC*)(&t2bp->t2ssec[i])));
	}
#endif

	if (writeT2File) {	// dump the T2 to a file. One file per LS
		int connIndex = getIndexFromLSID(t2bp->LS_id);
		if (connIndex == -1) {
//			printf("%s - LSID %d not found in connected stations - T2 not saved\n", __FUNCTION__, t2bp->LS_id);
			fprintf(pmlogf, "%s - %s - LSID %d not found in connected stations - T2 not saved\n", getTimeStringNow(), __FUNCTION__, t2bp->LS_id);
		} else {
			if (t2Files[connIndex] != NULL) {
				for (uint32_t j=0;j<t2count; j++) {		// write T0 and T2 subsecond for each T2
//					printf("%s - written T0 %d and T2 %d to file at index %d / id = %d\n", __FUNCTION__, gpstime, *(int*)&t2bp->t2ssec[j], connIndex, t2bp->LS_id);

//					fwrite(&gpstime, sizeof(gpstime), 1, t2Files[connIndex]);
					fprintf(t2Files[connIndex], "gps: %d ", gpstime);
#define SAVE_FORMATTED
#ifdef SAVE_FORMATTED
					uint32_t t2subsec = T2NSEC((T2SSEC*)(&t2bp->t2ssec[j]));
					if (writeT2FileInAscii) {
						fprintf(t2Files[connIndex], "ns: %d\n", t2subsec);
					} else {
						fwrite(&t2subsec, sizeof(t2subsec), 1, t2Files[connIndex]);
					}
#else
					if (writeT2FileInAscii) {
						fprintf(t2Files[connIndex], "ns: 0x%X\n", t2bp->t2ssec[j]);
					} else {
						fwrite(&t2bp->t2ssec[j], 2*sizeof(UINT16), 1, t2Files[connIndex]);
					}
#endif
				}
			}
		}
	}
//	FUNC_END
}

void processT3Msg(AMSG *amsgp) {
	FUNC_BEGIN;
	t3car.totalCount++;				// there is only one T3 per T3 request so far
	t3car.actCount++;				// there is only one T3 per T3 request so far

//	printf("received tag %d from T3, processing ...\n",  amsgp->tag);
	fprintf(pmlogf, "%s - %s - received t3list with length %d count of UINT16 and tag %d from T3, processing ...\n", getTimeStringNow(), __FUNCTION__,  amsgp->length, amsgp->tag );


	FUNC_END;
}

#ifdef USE_RC
void sendAlive(rcConnInfo *conn) {
	if (conn->sock == NULL) return;
	llout3<<__FUNCTION__<<" 1 "<<endl;
#ifdef USE_RC5
	UINT16   *outputBuffer = NULL;
#else
	RcBuffer16   outputBuffer = NULL;
#endif
	outputBuffer = RcSocketGetOutputBufferToBeFilled (conn->sock);
	if ( outputBuffer == NULL ) {
		llout3<<__FUNCTION__<<" - RcSocketGetOutputBufferToBeFilled() returned NULL for connection to "<<conn->name<<"!"<<endl;
		return;
	}
#ifdef USE_RC5
	UINT16* buffPoint =  ( outputBuffer+1 );
#else
	UINT16* buffPoint = RcBuffer16GetWords ( outputBuffer );
#endif
	if (buffPoint == NULL) {
		llout3<<__FUNCTION__<<" - RcBuffer16GetWords() returned NULL!"<<endl;
		 return;
	}
	UINT16 usedSize = 2;
	llout3<<__FUNCTION__<<" 2 "<<endl;
	buffPoint[0] = usedSize;									// length in UINT16 words  of Amsg including Amsg->length
	llout3<<__FUNCTION__<<" 3 "<<endl;
	buffPoint[1] = ALIVE;							// tag ALIVE
	llout3<<__FUNCTION__<<" 4 "<<endl;

#ifdef USE_RC5
	outputBuffer[0] = usedSize;		// size is count of UINT16
	llout3<<__FUNCTION__<<" 6 "<<endl;
#else
	RcBuffer16SetUsedSize ( outputBuffer, usedSize);		// size is count of UINT16
#endif
	RcSocketSetOutputBufferFilled(conn->sock);
	llout3<<__FUNCTION__<<" 7 "<<endl;

	llout1<<__FUNCTION__<<" - send ALIVE message with UINT16count = "<<usedSize<<" to "<<conn->name<<endl;
	fprintf(pmlogf, "%s - %s - writing ALIVE to %s ...\n", getTimeStringNow(), __FUNCTION__, conn->name);
}
#else
void sendAlive(connInfo *conn) {
	UINT16 msg[4];
	msg[0] = 2 * sizeof(UINT16);				// length in bytes of Msg without Msg->length
	msg[1] = 2;									// length in UINT16 words  of Amsg including Amsg->length
	msg[2] = ALIVE;							// tag ALIVE_ACK, answering ALIVE request
	llout1<<"writing ALIVE (bytelength/uint16 count/tag) "<< msg[0]<<"/"<<msg[1]<<"/"<<msg[2]<<" to "<<conn->name<<endl;
	fprintf(pmlogf, "%s - %s - writing ALIVE to %s ...\n", getTimeStringNow(), __FUNCTION__, conn->name);
	WriteSockMsg(conn->sock, (Msg*)&msg);
}
#endif


#ifdef USE_RC
void sendAliveAck(rcConnInfo *conn) {
	if (conn->sock == NULL) return;
	llout3<<__FUNCTION__<<" 1 "<<endl;
#ifdef USE_RC5
	UINT16   *outputBuffer = NULL;
#else
	RcBuffer16   outputBuffer = NULL;
#endif
	outputBuffer = RcSocketGetOutputBufferToBeFilled (conn->sock);
	if ( outputBuffer == NULL ) {
		llout3<<__FUNCTION__<<" - RcSocketGetOutputBufferToBeFilled() returned NULL for connection to "<<conn->name<<"!"<<endl;
		return;
	}
#ifdef USE_RC5
	UINT16* buffPoint =  ( outputBuffer+1 );
#else
	UINT16* buffPoint = RcBuffer16GetWords ( outputBuffer );
#endif
	if (buffPoint == NULL) {
		llout3<<__FUNCTION__<<" - RcBuffer16GetWords() returned NULL!"<<endl;
		 return;
	}
	UINT16 usedSize = 2;
	llout3<<__FUNCTION__<<" 2 "<<endl;
	buffPoint[0] = usedSize;									// length in UINT16 words  of Amsg including Amsg->length
	llout3<<__FUNCTION__<<" 3 "<<endl;
	buffPoint[1] = ALIVE_ACK;							// tag ALIVE_ACK
	llout3<<__FUNCTION__<<" 4 "<<endl;

#ifdef USE_RC5
	outputBuffer[0] = usedSize;		// size is count of UINT16
	llout3<<__FUNCTION__<<" 6 "<<endl;
#else
	RcBuffer16SetUsedSize ( outputBuffer, usedSize);		// size is count of UINT16
#endif
	RcSocketSetOutputBufferFilled(conn->sock);
	llout3<<__FUNCTION__<<" 7 "<<endl;

	llout1<<"writing ALIVE_ACK command (uint16 count/tag) "<< buffPoint[0]<<"/"<<buffPoint[1]<<" to "<<conn->name<<endl;
	fprintf(pmlogf, "%s - %s - writing ALIVE_ACK to %s ...\n", getTimeStringNow(), __FUNCTION__, conn->name);
}
#else
void sendAliveAck(connInfo *conn) {
	UINT16 msg[4];
	msg[0] = 2 * sizeof(UINT16);				// length in bytes of Msg without Msg->length
	msg[1] = 2;									// length in UINT16 words  of Amsg including Amsg->length
	msg[2] = ALIVE_ACK;							// tag ALIVE_ACK, answering ALIVE request
	llout1<<"writing ALIVE_ACK command (bytelength/uint16 count/tag) "<< msg[0]<<"/"<<msg[1]<<"/"<<msg[2]<<" to "<<conn->name<<endl;
	fprintf(pmlogf, "%s - %s - writing ALIVE_ACK to %s ...\n", getTimeStringNow(), __FUNCTION__, conn->name);
	WriteSockMsg(conn->sock, (Msg*)&msg);
}
#endif


#ifdef USE_RC
void sendLSinitialize(rcConnInfo *lsconn) {
	if (lsconn->sock == NULL) return;
#ifdef USE_RC5
	UINT16   *outputBuffer = NULL;
#else
	RcBuffer16   outputBuffer = NULL;
#endif
	outputBuffer = RcSocketGetOutputBufferToBeFilled (lsconn->sock);
	if ( outputBuffer == NULL ) {
		llout0<<"sendLSinitialize() - RcSocketGetOutputBufferToBeFilled() returned NULL for LS "<<lsconn->name<<"!"<<endl;
		return;
	}
#ifdef USE_RC5
	UINT16* buffPoint =  ( outputBuffer+1 );
#else
	UINT16* buffPoint = RcBuffer16GetWords ( outputBuffer );
#endif
	if (buffPoint == NULL) {
		 llout0<<"sendLSinitialize() - RcBuffer16GetWords() returned NULL!"<<endl;
		 return;
	}
	UINT16 usedSize = 3;
	buffPoint[0] = usedSize;									// length in UINT16 words  of Amsg including Amsg->length
	buffPoint[1] = LS_START;							// start tag
	buffPoint[2] = lsconn->lsid;						// destination lsid

#ifdef USE_RC5
	outputBuffer[0] = usedSize;		// size is count of UINT16
#else
	RcBuffer16SetUsedSize ( outputBuffer, usedSize);		// size is count of UINT16
#endif
	RcSocketSetOutputBufferFilled(lsconn->sock);

	llout1<<"sendLSinitialize() - send start message with UINT16count = "<<usedSize<<" to LS "<<lsconn->name<<endl;
}
#else
void sendLSinitialize() {
	FUNC_BEGIN
	printf("sending initialize to stations ...\n");
	fprintf(pmlogf, "%s - %s - sending initialize to stations ...\n", getTimeStringNow(), __FUNCTION__);
	// send initialize to all connected station, LS_ID 0 addresses all, but is not yet implemented
	for (uint32_t i=0;i<lsCount;i++) {
//		printf("%s - sock of connection at index %d is %d\n", __FUNCTION__ , i , LsConn[i].sock);
		if (LsConn[i].sock != 0) {
			llout1<<__FUNCTION__<<" - send initialize to   "<<LsConn[i].name<<endl;
			UINT16 msg[4];
			msg[0] = 3 * sizeof(UINT16);				// length in bytes of Msg without Msg->length
			msg[1] = 3;									// length in UINT16 words  of Amsg including Amsg->length
			msg[2] = LS_INITIALIZE;						// initialize tag
			msg[3] = LsConn[i].lsid;					// destination lsid
			llout1<<"writing initialize command (length/tag/lsid) "<< msg[0]<<"/"<<msg[1]<<"/"<<msg[2]<<" to station"<<endl;
			WriteSockMsg(LsConn[i].sock, (Msg*)&msg);
		}
	}
	FUNC_END
}
#endif

#ifdef USE_RC
void sendStartToLS(rcConnInfo *lsconn) {
	if (lsconn->sock == NULL) return;
	llout0<<__FUNCTION__<<" 1 "<<endl;
#ifdef USE_RC5
	UINT16   *outputBuffer = NULL;
#else
	RcBuffer16   outputBuffer = NULL;
#endif
	outputBuffer = RcSocketGetOutputBufferToBeFilled (lsconn->sock);
	if ( outputBuffer == NULL ) {
		llout0<<"sendStartToLS() - RcSocketGetOutputBufferToBeFilled() returned NULL for LS "<<lsconn->name<<"!"<<endl;
		return;
	}
#ifdef USE_RC5
	UINT16* buffPoint =  ( outputBuffer+1 );
#else
	UINT16* buffPoint = RcBuffer16GetWords ( outputBuffer );
#endif
	if (buffPoint == NULL) {
		 llout0<<"sendStartToLS() - RcBuffer16GetWords() returned NULL!"<<endl;
		 return;
	}
	UINT16 usedSize = 3;
	llout0<<__FUNCTION__<<" 2 "<<endl;
	buffPoint[0] = usedSize;									// length in UINT16 words  of Amsg including Amsg->length
	llout0<<__FUNCTION__<<" 3 "<<endl;
	buffPoint[1] = LS_START;							// start tag
	llout0<<__FUNCTION__<<" 4 "<<endl;
	buffPoint[2] = lsconn->lsid;						// destination lsid
	llout0<<__FUNCTION__<<" 5 "<<endl;

#ifdef USE_RC5
	outputBuffer[0] = usedSize;		// size is count of UINT16
	llout0<<__FUNCTION__<<" 6 "<<endl;
#else
	RcBuffer16SetUsedSize ( outputBuffer, usedSize);		// size is count of UINT16
#endif
	RcSocketSetOutputBufferFilled(lsconn->sock);
	llout0<<__FUNCTION__<<" 7 "<<endl;

	llout1<<"sendStartToLS() - send start message with UINT16count = "<<usedSize<<" to LS "<<lsconn->name<<endl;
}
#else
void sendStartToLS(connInfo *lsconn) {
	UINT16 msg[4];
	msg[0] = 3 * sizeof(UINT16);				// length in bytes of Msg without Msg->length
	msg[1] = 3;									// length in UINT16 words  of Amsg including Amsg->length
	msg[2] = LS_START;							// start tag
	msg[3] = lsconn->lsid;						// destination lsid
	llout1<<"writing start command (length/tag/lsid) "<< msg[0]<<"/"<<msg[1]<<"/"<<msg[2]<<" to station "<<msg[2]<<endl;
	fprintf(pmlogf, "%s - %s - writing start command to station %s ...\n", getTimeStringNow(), __FUNCTION__, lsconn->name);
	WriteSockMsg(lsconn->sock, (Msg*)&msg);
}
#endif

void sendStartToAllLS() {
	FUNC_BEGIN
	// send start to all connected station, LS_ID 0 addresses all, but is not yet implemented
	for (uint32_t i=0;i<lsCount;i++) {
//		printf("%s - sock of connection at index %d is %d\n", __FUNCTION__ , i , LsConn[i].sock);
		if (LsConn[i].sock != 0) {
			llout1<<__FUNCTION__<<" - send start to   "<<LsConn[i].name<<endl;
			fprintf(pmlogf, "%s - %s - send start to %s ...\n", getTimeStringNow(), __FUNCTION__, LsConn[i].name);
			sendStartToLS(&LsConn[i]);
		}
	}
	FUNC_END
}

#ifdef USE_RC
void sendStopToLS(rcConnInfo *lsconn) {
	if (lsconn->sock == NULL) return;
#ifdef USE_RC5
	UINT16   *outputBuffer = NULL;
#else
	RcBuffer16   outputBuffer = NULL;
#endif
	outputBuffer = RcSocketGetOutputBufferToBeFilled (lsconn->sock);
	if ( outputBuffer == NULL ) {
		llout0<<"sendStopToLS() - RcSocketGetOutputBufferToBeFilled() returned NULL for LS "<<lsconn->name<<"!"<<endl;
		return;
	}
#ifdef USE_RC5
	UINT16* buffPoint =  ( outputBuffer+1 );
#else
	UINT16* buffPoint = RcBuffer16GetWords ( outputBuffer );
#endif

	if (buffPoint == NULL) {
		 llout0<<"sendStopToLS() - RcBuffer16GetWords() returned NULL!"<<endl;
		 return;
	}
	UINT16 usedSize = 3;
	buffPoint[0] = usedSize;							// length in UINT16 words  of Amsg including Amsg->length
	buffPoint[1] = LS_STOP;								// stop tag
	buffPoint[2] = lsconn->lsid;						// destination lsid

#ifdef USE_RC5
	outputBuffer[0] = usedSize;		// size is count of UINT16
#else
	RcBuffer16SetUsedSize ( outputBuffer, usedSize);		// size is count of UINT16
#endif
	RcSocketSetOutputBufferFilled(lsconn->sock);

	llout1<<"sendStopToLS() - send start message with UINT16count = "<<usedSize<<" to LS "<<lsconn->name<<endl;
}
#else
void sendStopToLS(connInfo *lsconn) {
	UINT16 msg[4];
	msg[0] = 3 * sizeof(UINT16);				// length in bytes of Msg without Msg->length
	msg[1] = 3;									// length in UINT16 words  of Amsg including Amsg->length
	msg[2] = LS_STOP;							// stop tag
	msg[3] = lsconn->lsid;						// destination lsid
	llout1<<"writing stop command (length/tag/lsid) "<< msg[0]<<"/"<<msg[1]<<"/"<<msg[2]<<" to station"<<endl;
	fprintf(pmlogf, "%s - %s - writing stop command to station %s ...\n", getTimeStringNow(), __FUNCTION__, lsconn->name);
	WriteSockMsg(lsconn->sock, (Msg*)&msg);
}
#endif

void sendStopToAllLS() {
	FUNC_BEGIN
	// send stop to all connected station, LS_ID 0 addresses all, but is not yet implemented on station level
	for (uint32_t i=0;i<lsCount;i++) {
//		printf("%s - sock of connection at index %d is %d\n", __FUNCTION__ , i , LsConn[i].sock);
		if (LsConn[i].sock != 0) {
			llout1<<__FUNCTION__<<" - send stop to   "<<LsConn[i].name<<endl;
			fprintf(pmlogf, "%s - %s - send stop to %s ...\n", getTimeStringNow(), __FUNCTION__, LsConn[i].name);
			sendStopToLS(&LsConn[i]);
		}
	}
	FUNC_END
}

#ifdef KIT_COMMS
//broadcast send over RT
void sendT3reqToLsKitComms(AMSG *amsgt3p) {
	FUNC_BEGIN
	static int CntIntSkip = 0; //counter
	static int CntExtMBiasSkip = 0; //counter
	static int CntExtOthersSkip = 0; //counter
	
	int ret = 0;

	if (KitCommsRTConn.sock == 0) {
		printf("%s - not connected to Kit Comms RT - sending T3 request is skipped\n", __FUNCTION__);
		fprintf(pmlogf, "%s - %s -  not connected to Kit Comms RT - sending T3 request is skipped\n", getTimeStringNow(), __FUNCTION__);
		return;			// this station is not connected, so try next
	}
	#define MAX_REQS_T3LIST 1024
	UINT16 msg[1+6*MAX_REQS_T3LIST];
	
	if (amsgt3p == NULL) {
		printf("%s - received NULL AMSG pointer!!\n", __FUNCTION__);
		return;
	}
	printf("%s - received AMSG with T3, dumping T3 list:\n", __FUNCTION__);
	printf("%s\n", (dumpT3List(amsgt3p)).c_str());

	int lsc = getLSInfCountFromT3ListRcMsg(amsgt3p);
	if (lsc < 0) {
		llout1<<"sendT3reqToLsKitComms() : error in getLSInfCountFromT3ListRcMsg()"<<endl;
		return;
	} else if (lsc == 0) {
		llout1<<"sendT3reqToLsKitComms() : no station informations in list"<<endl;
		return;
	} else if (lsc > MAX_REQS_T3LIST) {
		printf("sendT3reqToLsKitComms() : list too big, lost %d T3 requests!!!\n", lsc-MAX_REQS_T3LIST);
		lsc = MAX_REQS_T3LIST;
	}
	
	T3BODY *t3bodyp = (T3BODY*) &(amsgt3p->body);

	if (amsgt3p->tag == T3_EVENT_REQUEST_LIST)  { //definitely internal T3
		if (++CntIntSkip == IntSkipKitComms) { //no skip
			CntIntSkip = 0;
		} else {
			printf("sendT3reqToLsKitComms() : internal T3 skipped, tag: %d, evno: %d, lsc: %d\n", amsgt3p->tag, t3bodyp->event_nr, lsc);
			return;
		}
	} else {
		if ((amsgt3p->tag == T3_EXT_SD_EVENT_REQUEST_LIST) ||
		    (amsgt3p->tag == T3_EXT_GUI_EVENT_REQUEST_LIST) ||
		    (amsgt3p->tag == T3_EXT_FD_EVENT_REQUEST_LIST)) { //flags for external T3s, actually only "SD" used
			//check the subsucond counter of the first station...all the rest should be the same 
			int tmpNs = t3bodyp->t3station[0].NS1<<16 | t3bodyp->t3station[0].NS2<<8 | t3bodyp->t3station[0].NS3<<0;
			if ((tmpNs < subsecRangeMBias) || (tmpNs > (1000000000-subsecRangeMBias))) {   //VERY possibly a MBias!
				if (++CntExtMBiasSkip == ExtMBiasSkipKitComms) { //no skip
					CntExtMBiasSkip = 0;
				} else {
					printf("sendT3reqToLsKitComms() : MBias T3 skipped, tag: %d, evno: %d, lsc: %d, tmpNs: %d\n", amsgt3p->tag, t3bodyp->event_nr, lsc, tmpNs);
					return;
				}
			} else { //almost impossibly a MBias!
				if (++CntExtOthersSkip == ExtOthersSkipKitComms) { //no skip
					CntExtOthersSkip = 0;
				} else {
					printf("sendT3reqToLsKitComms() : Ext T3 (not MBias) skipped, tag: %d, evno: %d, lsc: %d, tmpNs: %d\n", amsgt3p->tag, t3bodyp->event_nr, lsc, tmpNs);
					return;
				}				
			}
		} else {
			printf("sendT3reqToLsKitComms() : Unkown Tag?, tag: %d, evno: %d, lsc: %d\n", amsgt3p->tag, t3bodyp->event_nr, lsc);
		}
	}

	
	int j = 0;
	for (int i=0; i<lsc; i++) {
		if (IsLsKitCommsArr[(t3bodyp->t3station[i].LS_id)%LSID_RANGE] == true) { //if this is kitcomms station
			msg[1+j*6] = 6;						// length in UINT16 words  of Amsg including Amsg->length
			if (amsgt3p->tag == T3_EVENT_REQUEST_LIST)              msg[2+j*6] = LS_GETEVENT;
			else if (amsgt3p->tag == T3_EXT_SD_EVENT_REQUEST_LIST)  msg[2+j*6] = LS_GET_EXT_SD_EVENT;
			else if (amsgt3p->tag == T3_EXT_GUI_EVENT_REQUEST_LIST) msg[2+j*6] = LS_GET_EXT_GUI_EVENT;
			else if (amsgt3p->tag == T3_EXT_FD_EVENT_REQUEST_LIST)  msg[2+j*6] = LS_GET_EXT_FD_EVENT;
			msg[3+j*6] = t3bodyp->t3station[i].LS_id;				// destination lsid
			msg[4+j*6] = t3bodyp->event_nr;
			msg[5+j*6] = ((UINT16*)&t3bodyp->t3station[i])[1];
			msg[6+j*6] = ((UINT16*)&t3bodyp->t3station[i])[2];
			llout1<<"writing T3 request (length/tag/lsid) "<< msg[1+j*6]<<"/"<<msg[2+j*6]<<"/"<<msg[3+j*6]<<" to station"<<endl;
			llout1<<__FUNCTION__<<" - dumping the T3 request before sending it: "<<dumpLSGetEvent((AMSG*)&msg[1+j*6])<<endl;		
			j++;
		}
	}
	if (j > 0) { //if some t3reqs are for LS over kitcomms
		msg[0] = 6 * sizeof(UINT16) * j; // length in bytes of Msg without Msg->length
	
		ret = WriteSockMsg(KitCommsRTConn.sock, (Msg*)&msg);
		if (ret != 0) {
			llout1<<"writing T3 request list "<<" to kit comms failed! WriteSockMsg() returned rest "<<ret<<endl;
		} else {
			printf("%s - writing %d T3 requests to LS over kit comms\n", __FUNCTION__, j); //debug
		}
	} else {
		printf("%s - no T3 requests for LS over kit comms found", __FUNCTION__); //debug
	}

	FUNC_END
}
#endif //KIT_COMMS

void sendT3reqToLS(AMSG *amsgt3p) {
	FUNC_BEGIN
	int ret = 0;

	if (amsgt3p == NULL) {
		printf("%s - received NULL AMSG pointer!!\n", __FUNCTION__);
		return;
	}
	printf("%s - received AMSG with T3, dumping T3 list:\n", __FUNCTION__);
	printf("%s\n", (dumpT3List(amsgt3p)).c_str());

	int lsc = getLSInfCountFromT3ListRcMsg(amsgt3p);
	if (lsc < 0) {
		llout1<<"sendT3reqToLS() : error in getLSInfCountFromT3ListRcMsg()"<<endl;
		return;
	} else if (lsc == 0) {
		llout1<<"sendT3reqToLS() : no station informations in list"<<endl;
		return;
	}
	llout1<<"Have "<<dec<<lsc<<" station informations in list"<<endl;
	T3BODY *t3bodyp = (T3BODY*) &(amsgt3p->body);
	llout1<<"evno : "<<t3bodyp->event_nr<<", length: "<<amsgt3p->length<<" => "<<lsc<<" LS informations in T3 list"<<endl;
	int tmpNs = 0, lsindex;
	for (int i=0; i<lsc; i++) {
		tmpNs = t3bodyp->t3station[i].NS1<<16 | t3bodyp->t3station[i].NS2<<8 | t3bodyp->t3station[i].NS3<<0;
		llout1<<dec<<setw(3)<<i<<": LS "<<setw(3)<<t3bodyp->t3station[i].LS_id<<" has time (sec + ns) 0x"<<hex<<(int) t3bodyp->t3station[i].sec<<": 0x"<<tmpNs<<" (as dec: "<<dec<<(int) t3bodyp->t3station[i].sec<<":"<<(int)tmpNs<<" ) "<<endl;
		fprintf(pmlogf, "%s - %s - %d : LS %d has time %d\n", getTimeStringNow(), __FUNCTION__, i, t3bodyp->t3station[i].LS_id, (int) t3bodyp->t3station[i].sec);
		if ((lsindex = getIndexFromLSID(t3bodyp->t3station[i].LS_id))< 0) {
			printf("%s - failed to find connection of station %d\n", __FUNCTION__, t3bodyp->t3station[i].LS_id);
		} else {
#ifdef USE_RC
			if (LsConn[lsindex].sock == NULL) {
#else
			if (LsConn[lsindex].sock == 0) {
#endif
				printf("%s - not connected to LS %d - sending T3 request is skipped\n", __FUNCTION__, t3bodyp->t3station[i].LS_id);
				fprintf(pmlogf, "%s - %s -  not connected to LS %d - sending T3 request is skipped\n", getTimeStringNow(), __FUNCTION__, t3bodyp->t3station[i].LS_id);
				continue;			// this station is not connected, so try next
			}

#ifdef USE_RC
#ifdef USE_RC5
			UINT16   *outputBuffer = NULL;
#else
			RcBuffer16   outputBuffer = NULL;
#endif
			outputBuffer = RcSocketGetOutputBufferToBeFilled (LsConn[lsindex].sock);
			if ( outputBuffer == NULL ) {
				llout0<<"sendT3reqToLS() - RcSocketGetOutputBufferToBeFilled() returned NULL for LS "<<LsConn[lsindex].name<<"!"<<endl;
				fprintf(pmlogf, "%s - %s - RcSocketGetOutputBufferToBeFilled() returned NULL for LS %s!\n", getTimeStringNow(), __FUNCTION__, LsConn[lsindex].name);
				return;
			}
#ifdef USE_RC5
			UINT16* buffPoint =  ( outputBuffer+1 );
#else
			UINT16* buffPoint = RcBuffer16GetWords ( outputBuffer );
#endif

			if (buffPoint == NULL) {
				 llout0<<"sendT3reqToLS() - RcBuffer16GetWords() returned NULL!"<<endl;
				fprintf(pmlogf, "%s - %s - RcBuffer16GetWords() returned NULL for LS %s!\n", getTimeStringNow(), __FUNCTION__, LsConn[lsindex].name);
				 return;
			}
			// fprintf(pmlogf, "%s - %s - Have output buffer for station %s at index %d, going to write T3 request ...\n", getTimeStringNow(), __FUNCTION__, LsConn[lsindex].name, lsindex);

			UINT16 usedSize = 6;
			buffPoint[0] = usedSize;									// length in UINT16 words  of Amsg including Amsg->length
			if (amsgt3p->tag == T3_EVENT_REQUEST_LIST)              buffPoint[1] = LS_GETEVENT;
			else if (amsgt3p->tag == T3_EXT_SD_EVENT_REQUEST_LIST)  buffPoint[1] = LS_GET_EXT_SD_EVENT;
			else if (amsgt3p->tag == T3_EXT_GUI_EVENT_REQUEST_LIST) buffPoint[1] = LS_GET_EXT_GUI_EVENT;
			else if (amsgt3p->tag == T3_EXT_FD_EVENT_REQUEST_LIST)  buffPoint[1] = LS_GET_EXT_FD_EVENT;
			buffPoint[2] = t3bodyp->t3station[i].LS_id;				// destination lsid
			buffPoint[3] = t3bodyp->event_nr;
			buffPoint[4] = ((UINT16*)&t3bodyp->t3station[i])[1];
			buffPoint[5] = ((UINT16*)&t3bodyp->t3station[i])[2];

#ifdef USE_RC5
			outputBuffer[0] = usedSize;		// size is count of UINT16
#else
			RcBuffer16SetUsedSize ( outputBuffer, usedSize);		// size is count of UINT16
#endif
//****** JLB 25/09/12 start
                        if ( !( buffPoint[3] % EVNO_MON ) )
                        {
			    ls_geteventbody*  getT3 = NULL;
			    getT3 = (ls_geteventbody*) ( buffPoint + 2 );
			    printf ( "pkt GetT3: ls%d ev%d\n", getT3->LS_id, getT3->event_nr );
                        }
//****** JLB 25/09/12 end
			RcSocketSetOutputBufferFilled(LsConn[lsindex].sock);

			llout1<<"sendT3reqToLS() - send T3 message with UINT16count = "<<usedSize<<" to LS "<<LsConn[lsindex].name<<endl;
			fprintf(pmlogf, "%s - %s - writing T3 request (length/tag/lsid) %d/%d/%d  with run evno %d to station %s\n", getTimeStringNow(), __FUNCTION__, buffPoint[0], buffPoint[1], buffPoint[2], buffPoint[3], LsConn[lsindex].name);
#else
			UINT16 msg[7];
			msg[0] = 6 * sizeof(UINT16);				// length in bytes of Msg without Msg->length
			msg[1] = 6;									// length in UINT16 words  of Amsg including Amsg->length
			if (amsgt3p->tag == T3_EVENT_REQUEST_LIST)              msg[2] = LS_GETEVENT;
			else if (amsgt3p->tag == T3_EXT_SD_EVENT_REQUEST_LIST)  msg[2] = LS_GET_EXT_SD_EVENT;
			else if (amsgt3p->tag == T3_EXT_GUI_EVENT_REQUEST_LIST) msg[2] = LS_GET_EXT_GUI_EVENT;
			else if (amsgt3p->tag == T3_EXT_FD_EVENT_REQUEST_LIST)  msg[2] = LS_GET_EXT_FD_EVENT;
			msg[3] = t3bodyp->t3station[i].LS_id;				// destination lsid
			msg[4] = t3bodyp->event_nr;
			msg[5] = ((UINT16*)&t3bodyp->t3station[i])[1];
			msg[6] = ((UINT16*)&t3bodyp->t3station[i])[2];
			llout1<<"writing T3 request (bytes/length/tag/lsid) "<< msg[0]<<"/"<<msg[1]<<"/"<<msg[2]<<"/"<<msg[3]<<" to station"<<endl;
			fprintf(pmlogf, "%s - %s - writing T3 request (bytes/length/tag/lsid) %d/%d/%d/%d  with run evno %d to station %s\n", getTimeStringNow(), __FUNCTION__, msg[0], msg[1], msg[2], msg[3], msg[4], LsConn[lsindex].name);

			llout1<<__FUNCTION__<<" - dumping the T3 request before sending it: "<<dumpLSGetEvent((AMSG*)&msg[1])<<endl;

			ret = WriteSockMsg(LsConn[lsindex].sock, (Msg*)&msg);
			if (ret != 0) {
				llout1<<"writing T3 request (bytes/length/tag/lsid) "<< msg[0]<<"/"<<msg[1]<<"/"<<msg[2]<<"/"<<msg[3]<<" to station failed! WriteSockMsg() returned rest "<<ret<<endl;
				fprintf(pmlogf, "%s - %s - writing T3 request (bytes/length/tag/lsid) %d/%d/%d/%d  with run evno %d to station %s failed! WriteSockMsg() returned rest %d\n", getTimeStringNow(), __FUNCTION__, msg[0], msg[1], msg[2], msg[3], msg[4], LsConn[lsindex].name, ret);
			}
#endif

			if (lsT3Car[lsindex].startTime == 0) lsT3Car[lsindex].startTime = time(NULL);
			lsT3Car[lsindex].totalCount ++;
			lsT3Car[lsindex].actCount ++;

			actT3Second[lsindex] =	t3bodyp->t3station[i].sec;


		}
	}
	FUNC_END
}

void showConnectionStatus() {
	time_t now = time(NULL);
	printf("%s - %s - showing connections at %d\n", getTimeStringNow(), __FUNCTION__, (int)now);

	char msg[200];
	for (uint32_t i=0;i<lsCount;i++) {
#ifdef USE_RC
		if (LsConn[i].sock != NULL) {
			sprintf(msg, "%s - %s - station  at index %d with ID %3d has port %d  => connected since %10.1f s\n", getTimeStringNow(), __FUNCTION__ , i , LsConn[i].lsid, LsConn[i].port, difftime(now, lsConnCar[i].startTime));
		} else {
			sprintf(msg,"%s - %s - station  at index %d with ID %3d has port %d  => disconnected since %10.1f s\n", getTimeStringNow(), __FUNCTION__ , i , LsConn[i].lsid, LsConn[i].port, difftime(now, lsDisConnCar[i].startTime));
		}
#else
		if (LsConn[i].sock != 0) {
			sprintf(msg, "%s - %s - station  at index %d with ID %3d has port %d and sock %3d => connected since %10.1f s\n", getTimeStringNow(), __FUNCTION__ , i , LsConn[i].lsid, LsConn[i].port, LsConn[i].sock, difftime(now, lsConnCar[i].startTime));
		} else {
			sprintf(msg,"%s - %s - station  at index %d with ID %3d has port %d and sock %3d => disconnected since %10.1f s\n", getTimeStringNow(), __FUNCTION__ , i , LsConn[i].lsid, LsConn[i].port, LsConn[i].sock, difftime(now, lsDisConnCar[i].startTime));
		}
#endif
		printf("%s", msg);
		fprintf(pmlogf, "%s", msg);
	}
	FUNC_END

}

#ifndef USE_RC
void socketHasError(connInfo *ci) {
	int ret = 0, sockerr = 0;
	printf("%s - socket with FD %d = %s has errors ...\n", __FUNCTION__, ci->sock, ci->name);

	printf("testing write/read/getpeername for some time ...\n");

	for (int i=0;i<0;i++) {
			printf("testing socket in loop %d\n", i);
		// just playing with network things - both tests for read and write return TRUE on lost sockets!!! Tests not usable to verify connection!!!!
		printf("%s - Trying a write on the socket ...\n", __FUNCTION__);
		ret = TestSocketForWrite(ci->sock);
		printf("%s - Writing on the socket returned %d\n", __FUNCTION__, ret);

		printf("%s - Trying a read on the socket ...\n", __FUNCTION__);
		ret = TestSocketForRead(ci->sock);
		printf("%s - Reading on the socket returned %d\n", __FUNCTION__, ret);

		unsigned int nSocketSize = sizeof(struct sockaddr_in);
		struct sockaddr_in InetSocketAddress;
		struct sockaddr *SocketAddress = (struct sockaddr *)&InetSocketAddress;
		ret = getpeername(ci->sock, SocketAddress, (socklen_t *)(&nSocketSize));
		sockerr = errno;
		perror("getpeername() - ");
		printf("%s - getpeername on the socket returned %d, errno is %d\n", __FUNCTION__, ret, sockerr);
		if (ret<0) exit(0);
//		sleep(1);
	}
}
#endif

void startDaq() {
	printf("starting daq ...\n");
	fprintf(pmlogf, "%s - %s - starting daq ...\n", getTimeStringNow(), __FUNCTION__);
	daqStarted = true;
	printf("sending start to stations ...\n");
	sendStartToAllLS();		// to all connected? if lsid = 0
	if (writeT2File) {
		llout0<<__FUNCTION__<<" - writeT2File is set => opening files to write T2s "<<(writeT2FileInAscii?" in ASCII":" binary")<<endl;
		openT2Files();
	} else 		llout0<<__FUNCTION__<<" - writeT2File is not set => opening files to write T2s "<<endl;
	if (writeT3File) {
		llout0<<__FUNCTION__<<" - writeT3File is set => opening files to write T3s "<<(writeT3FileInAscii?" in ASCII":" binary")<<endl;
		openT3Files();
	} else 		llout0<<__FUNCTION__<<" - writeT3File is not set => opening files to write T3s "<<endl;

	if (daqStartedTime == 0) daqStartedTime = time(NULL);
}

void stopDaq() {
	printf("stopping daq ...\n");
	fprintf(pmlogf, "%s - %s - stopping daq ...\n", getTimeStringNow(), __FUNCTION__);
	int runTime = difftime(time(NULL), daqStartedTime);
	fprintf(pmlogf, "%s - %s - daq has been running for %d seconds \n", getTimeStringNow(), __FUNCTION__, runTime);
	daqStarted = false;
	daqStartedTime = 0;

	printf("sending stop to stations ...\n");
	sendStopToAllLS();		// to all connected?  if lsid = 0
	closeT2Files();
	closeT3Files();

	printf("%s : Doing statistics...\n", getTimeStringNow());
	doT2Statistics();
	doT3Statistics();
	doStationStatistics();
	for (int i = 0; i< MAX_LS; i++) {
		lsDisConnTimesCar[i].resetActNow();
	}
	showConnectionStatus();


	// some statistics
}


/**
 * @brief called on specific signals to terminatePM the application
 * @param sig - the signal received and passed by pm_sighandler()
 */
void terminatePM(int sig) {
	printf("terminating ... \n");
	fprintf(pmlogf, "%s - %s - terminating ...\n", getTimeStringNow(), __FUNCTION__);

	doT2Statistics();
	doT3Statistics();
	closeT2Files();
	closeT3Files();
#ifdef USE_RC
	if (T3Conn.sock != NULL) {
		T3Conn.sock = NULL;
	}
	if (EbConn.sock != NULL) {
		EbConn.sock = NULL;
	}
	if (GuiConn.sock != NULL) {
		GuiConn.sock = NULL;
	}
	resetRcConnInfo(&EbConn);
	resetRcConnInfo(&T3Conn);
	resetRcConnInfo(&GuiConn);
#else
	if (T3Conn.sock != 0) {
		close(T3Conn.sock);
		T3Conn.sock = 0;
	}
	if (EbConn.sock != 0) {
		close(EbConn.sock);
		EbConn.sock = 0;
	}
	if (GuiConn.sock != 0) {
		close(GuiConn.sock);
		GuiConn.sock = 0;
	}
	if (T3Conn.servProto != 0) {
		close(T3Conn.servProto);
		T3Conn.servProto = 0;
	}
	if (EbConn.servProto != 0) {
		close(EbConn.servProto);
		EbConn.servProto = 0;
	}
	if (GuiConn.servProto != 0) {
		close(GuiConn.servProto);
		GuiConn.servProto = 0;
	}
	// mainly free the sockadress structures allocated by OpenInetProtoServer - but this is freed anyway on exiting the program - and freed inside socklib
//	resetConnInfo(&EbConn);
//	resetConnInfo(&T3Conn);
//	resetConnInfo(&GuiConn);
#endif

#ifdef USE_MYSQL
	// TODO: update DB before closing
	closeDB();
#endif		// #ifdef USE_MYSQL

	printf("Good bye Argentina...\n");
	fprintf(pmlogf, "%s - %s - Good bye Argentina...\n", getTimeStringNow(), __FUNCTION__);
	fflush(stdout);

	if (pmlogf) fclose(pmlogf);
	exit(sig);
}

/**
 * @brief the installed signal handler to process signals from OS
 * @param sig - the signal to process
 */
void pm_sighandler(int sig) {
	printf("pm signal handler processing signal %d...\n", sig);fflush(stdout);
	fprintf(pmlogf, "%s - %s - pm signal handler processing signal %d...\n", getTimeStringNow(), __FUNCTION__, sig);

	switch(sig) {
		case SIGSEGV:
			printf("I'm so sorry, but the programmer is insane ...\n");fflush(stdout);
			terminatePM(sig);
			break;
		case SIGINT:
			printf("Received break from User ...\n");fflush(stdout);
			terminatePM(sig);
			break;
		case SIGKILL:
		case SIGTERM:
			printf("Received SIGKILL from User ...\n");fflush(stdout);
			terminatePM(sig);
			break;
		case SIGALRM:
			/*alarm(TimeSlice);*/
			break;
		case SIGPIPE:
			printf("Socke verloren!\n");fflush(stdout);
			break;
		case SIGFPE:
			printf("Floating point exception caught, what happens now?\n");fflush(stdout);
			break;
		default:
			printf("Exit ...\n");fflush(stdout);
			terminatePM(sig);
			break;
		}
}

/**
 * @brief dedicate handling of some system signals to own signal handler method
 */
void installSignalHandler() {
	llout0<<"Installing signal handler ...";
	signal(SIGINT,pm_sighandler);
	signal(SIGKILL,pm_sighandler);
	signal(SIGTERM,pm_sighandler);
	signal(SIGPIPE,pm_sighandler);
	llout0<<"done.\n";
}


void exitTest()
{
    cout<<__FUNCTION__<<" - exiting alssim\n";
#ifdef USE_RC5
    system("ssh 192.168.1.9 killall alssim_Rc6");
    system("killall aevb_Rc6");
    system("killall t3maker_Rc6");
#endif
#ifdef USE_MSG
    system("ssh 192.168.1.9 killall alssim");
    system("killall aevb");
    system("killall t3maker_SL");
#endif
    exit(0);
}


int main() {
	int loopCount=0;
	time_t now;
	bool aliveSent = false;

	bool t2StatisticsDone = false;
	bool t10SecondsDone = false;
	bool actStatisticsDone = false;
	mainStartTime = time(NULL);

#ifdef USE_RC
	llout0 << "postmaster running in Rc-Mode..." << endl;
#else
	llout0 << "postmaster running in socklib-Mode..." << endl;
#endif

	printf("sizeof(T2SSEC) : %d\n", sizeof(T2SSEC));
	printf("sizeof(T2BODY) : %d\n", sizeof(T2BODY));
	printf("sizeof(AMSG) : %d\n", sizeof(AMSG));

	installSignalHandler();

	initPostmaster();

// now we have logfile available
#ifdef USE_RC
	fprintf(pmlogf, "postmaster running in Rc-Mode...\n");
#else
	fprintf(pmlogf, "postmaster running in socklib-Mode...\n");
#endif
	confFile = openConfigFile("r");
	readConfigFile();
	fclose(confFile);

	arrayFile = openArrayFile("r");
	readArrayFile();
	fclose(arrayFile);

	initConnections();

	llout0<<"postmaster - starting main() in loop "<<dec<<loopCount<<endl;
	fprintf(pmlogf, "%s - %s - starting main() in loop %d...\n", getTimeStringNow(), __FUNCTION__, loopCount);


	if (autoStartDaq) {
		if (daqStartedTime == 0) daqStartedTime = time(NULL);
		startDaq();
	}
	while (true) {			// endless loop
		if (loopCount % 1000 == 0) {
			llout0<<"postmaster - main() in loop "<<dec<<loopCount<<endl;
		}

#define CHECK_SOCKETS_SEPARATELY
#ifdef	CHECK_SOCKETS_SEPARATELY		// one select for all stations and one for each daq process
		// check for stations sending T2 or data
		checkStationsForRead();			// got new T2 or data from LS?
		checkT3();						// got new T3 from T3maker?
		if (loopCount % 20 == 0) {
			checkPrompt();					// got new commands from prompt? Lower priority
			checkGui();						// got new commands from Gui? Lower priority
		}
		#ifdef KIT_COMMS
		checkKitCommsForRead();
		#endif //KIT_COMMS
#else									// one select for ALL sockets, this may be used, to work with a blocking select, so far only with socklib
		checkSocketsForRead();
#endif
		// look for lost stations and Eb,T3,Gui to be accepted
		if (loopCount % 100 == 0) {
//			checkConnections();
			if (daqStarted) {
//				if (writeT2File) flushT2Files();
			}
		}


		if (loopCount % 10 == 0) {
					fflush(stdout);
				}

		loopCount++;

		// some time controlled tasks
		now = time(NULL);
//		if ((((int)difftime(now, mainStartTime) % connCheckInterval) == 0) || (difftime(now, lastConnCheckTime) > connCheckInterval)) {
//		if ((((int)difftime(now, mainStartTime) % connCheckInterval) == 0)) {
		if ((((int)(now) % connCheckInterval) == 0)) {
			if (!t10SecondsDone) {
				lastConnCheckTime = now;
				t10SecondsDone = true;
				checkConnections();
				if (daqStarted) {
					if (writeT2File) flushT2Files();
				}
				for (int i=0;i<MAX_LS;i++) {
					if (LsConn[i].lsid == 0) continue;		// this station has not been in init file
					else {
//						printf("%s - Station[%d]/LSID=%d: lastT2time: %d (8-bit/&0xFF: %d), actualT3time: %d, difference: %d\n", getTimeStringNow(), i, LsConn[i].lsid, lastT2Second[i], (lastT2Second[i] & 0xFF), actT3Second[i], ((lastT2Second[i] & 0xFF) - (actT3Second[i])));
						fprintf(pmlogf, "%s - Station[%d]/LSID=%d: lastT2time: %d (8-bit/&0xFF: %d), actualT3time: %d, difference: %d\n", getTimeStringNow(), i, LsConn[i].lsid, lastT2Second[i], (lastT2Second[i] & 0xFF), actT3Second[i], ((lastT2Second[i] & 0xFF) - (actT3Second[i])));
					}
				}

			}
		} else t10SecondsDone = false;

//		if ((((int)difftime(now, mainStartTime) % actStatisticsInterval) == 0) || (difftime(now, lastActStatisticsTime) > actStatisticsInterval)) {
//		if ((((int)difftime(now, mainStartTime) % actStatisticsInterval) == 0)) {
		if ((((int)(now) % actStatisticsInterval) == 0)) {
			if (!actStatisticsDone) {
				lastActStatisticsTime = now;
				actStatisticsDone = true;
//				printf("%s : Doing statistics...\n", getTimeStringNow());
				int runtime = difftime(now, daqStartedTime);
				fprintf(pmlogf, "%s : Count and Rate - Doing statistics after %d s since daq start...\n", getTimeStringNow(), runtime);
				fprintf(pmlogf, " Count and Rate - The interval used for actual rates is               %3d \n", actStatisticsInterval);
				fprintf(pmlogf, " Count and Rate - The time waited until a connection is resetted is   %3d\n", noMsgConnTimeout);
				fprintf(pmlogf, " Count and Rate - The time interval between (re-)connection checks is %3d\n", connCheckInterval);
				doT2Statistics();
				doT3Statistics();
				doStationStatistics();
				showConnectionStatus();

				checkLogFileSize();
			}
		} else actStatisticsDone = false;

#ifdef USE_AERA_TRANSPORT_DEFINITIONS		// do not waste too much time on it now
		static time_t lastAliveReqTime = time(NULL);
		if ((now - lastAliveReqTime) > MIN_ALIVE_SEND_INTERVAL) {
			llout0<<__FUNCTION__<<" - last alive request is "<< (now - lastAliveReqTime)<<" seconds ago = more than min interval "<<MIN_ALIVE_SEND_INTERVAL<<", => requesting alive from t3maker"<<endl;
//			sendAlive(&T3Conn);		// TODO: AW, 21.03.2013: t3maker does not yet support ALIVE messages, deactivate those
			aliveSentTimeT3 = now;
			for (int i=0;i<MAX_LS;i++) {
#ifdef USE_RC
				if (LsConn[i].sock != NULL) {
#else
				if (LsConn[i].sock != 0) {
#endif
					llout1<<"sending alive to station "<<LsConn[i].name<<endl;
					sendAlive(&LsConn[i]);
					aliveSentTimeLS[i] = now;
				}
			}
			lastAliveReqTime = now;
			aliveSent = true;
		}
		static time_t lastAliveCheckTime = time(NULL);
		if (aliveSent) {
			if ((now - lastAliveCheckTime) > 5) {
				checkAliveAck();
				lastAliveCheckTime = now;
			}

//				TODO:  check how often for missing ack
		}
#endif

#ifdef USE_MYSQL
		if (((int)difftime(now, start_time)%GUI_EVNO_UP_INT) == 0) {
			if (!guiEvNoUpdated) {
				if (useDB) updateGuiEventNo();
				guiEvNoUpdated = true;
			}
		} else guiEvNoUpdated = false;
		if (((int)difftime(now, start_time)%GUI_DB_UP_INT) == 0) {
			if (!guiDBUpdated) {
				llout1<<getTimeStringNow()<<" AeraEventBuilder::mainLoop() - updating DB"<<endl;
				if (useDB) updateDB();
				guiDBUpdated = true;
//              sendT3Message();       // put sending an empty buffer to T3 here for tests
			}
		} else guiDBUpdated = false;
#endif		// #ifdef USE_MYSQL

// JLB 30/01/13 Start
		/*
		if (difftime(now, daqStartedTime) >= TEST_MAX_RUNTIME)
		{
		    // llout0<<"Total run time = "<<difftime(now, daqStartedTime)<<" reached maximum time = "<<TEST_MAX_RUNTIME<<"s, => exiting program"<<endl;
		    exitTest();
		}
		*/
// JLB 30/01/13 End


		usleep(50);
	}


	return 0;
}
