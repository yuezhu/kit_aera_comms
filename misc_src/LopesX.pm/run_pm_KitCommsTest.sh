#!/bin/sh

ctrl_c()
{
echo -en "\n************ctrl-C out**********\n"
export HOME=/home/daq
echo $HOME
exit $?
}

trap ctrl_c SIGINT

export HOME=/home/daq/temp/fake-home/daq
echo $HOME
ln -sf $HOME/postmaster_kitcomms_test1 $HOME/postmaster
ln -sf $HOME/conf/array.conf_kitcomms_test1 $HOME/conf/array.conf
ln -sf $HOME/conf/pm.init_kitcomms_test1 $HOME/conf/pm.init
$HOME/postmaster
#/home/daq/AERA/trunk/lopesX/trunk/postmaster/Debug/postmaster
export HOME=/home/daq
echo $HOME
