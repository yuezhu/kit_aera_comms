/** @file accumulator.h
 *  @brief
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#ifndef ACCUMULATOR_H_
#define ACCUMULATOR_H_

typedef struct {
	int N;
	int inst;
	int sigma;
}sigma_t;

void add_sigma(sigma_t* a, int val);
void reset_sigma(sigma_t* a);
float cal_avg_sigma(sigma_t* a);

typedef struct {
	int N;
	int inst;
	int min_reg;
	int max_reg;
	int sigma;
}minmaxsigma_t;

void add_minmaxsigma(minmaxsigma_t* a, int val);
void reset_minmaxsigma(minmaxsigma_t* a);
float cal_avg_minmaxsigma(minmaxsigma_t* a);

typedef struct {
	int N;
	int inst;
	int min_reg;
	int max_reg;
	int sigma;
	int sigma2; //sigma (power(x,2))
}minmaxsigma2_t;

void add_minmaxsigma2(minmaxsigma2_t* a, int val);
void reset_minmaxsigma2(minmaxsigma2_t* a);
float cal_avg_minmaxsigma2(minmaxsigma2_t* a);
float cal_stddev_minmaxsigma2(minmaxsigma2_t* a);

#define CREATE_HISTO1D(x,name) \
typedef struct { \
	int bin[x]; \
}name##_histo1d_t; \
void add_histo1d_##name(name##_histo1d_t* a, int val); \
void reset_histo1d_##name(name##_histo1d_t* a); \
void cal_percent_histo1d_##name(name##_histo1d_t* a, name##_histo1d_t* b);

#define CREATE_HISTO1D_C(x,name) \
void add_histo1d_##name(name##_histo1d_t* a, int val) { \
	a->bin[val]++; \
} \
void reset_histo1d_##name(name##_histo1d_t* a) { \
	int i; \
    for (i = 0; i<x; i++) { \
        a->bin[i] = 0; \
    } \
}\
void cal_percent_histo1d_##name(name##_histo1d_t* a, name##_histo1d_t* b) { \
	int i, sigma; \
	sigma = 0; \
    for (i = 0; i<x; i++) { \
        sigma += a->bin[i]; \
    } \
    for (i=0; i<x; i++) { \
    	if (sigma != 0) { \
			b->bin[i] = a->bin[i]*100/sigma; \
		} else { \
			b->bin[i] = 0; \
		} \
	} \
}

#endif /* ACCUMULATOR_H_ */
