/*
 * timer.c
 *
 *  Created on: Aug 14, 2012
 *      Author: dev
 */

#include <signal.h>
#include <sys/time.h>
#include "timer.h"

uint32_t sys_now(struct timeval* tv)
{
  uint32_t sec, usec, msec;
  gettimeofday(tv, NULL);

  sec = (uint32_t)(tv->tv_sec);
  usec = (uint32_t)(tv->tv_usec);
  msec = sec * 1000 + usec / 1000;

  return msec; //resolution derived from jiffies
}

/***************/
/* tLayerTimer */
/***************/
void init_tLayerTimer(struct tLayerTimer* timer, tLayerTimeout_handler handler, void *handler_arg)
{
	timer->h = handler;
	timer->arg = handler_arg;
	timer->isActive = false;
}

void reset_tLayerTimer(struct tLayerTimer* timer, uint32_t timeout_duration_msec)
{
	timer->isActive = true;
	timer->timeout_duration = timeout_duration_msec;
	kick_tLayerTimer(timer);
}

void stop_tLayerTimer(struct tLayerTimer* timer)
{
	timer->isActive = false;
}

void kick_tLayerTimer(struct tLayerTimer* timer)
{
	struct timeval tv;
	timer->time_lastkick = sys_now(&tv);
}

bool checkTimeout_tLayerTimer(struct tLayerTimer* timer)
{
	if (timer->isActive == true) {
		struct timeval tv;
		if (sys_now(&tv)-timer->time_lastkick > timer->timeout_duration) {
			tLayerTimeout_handler handler = timer->h;
			if (handler != NULL) {
				handler(timer->arg);
	        }
			return true; //timeout
		}
		return false;
	}
	return false;
}

/********************/
/* performanceTimer */
/********************/
void print_tv(struct timeval* tv, FILE* out)
{
	struct tm      *tm;
	tm = localtime(&(tv->tv_sec));

	fprintf(out, "%d:%02d:%02d.%ld, ", tm->tm_hour, tm->tm_min, tm->tm_sec, tv->tv_usec/1000);
}

void reset_performanceTimer(struct performanceTimer* timer, bool ifPrint, FILE* out)
{
	struct timeval tv;
	timer->start = sys_now(&tv);
	if (ifPrint == true) {
		print_tv(&tv, out);
		//fprintf(out, "STARTED]\n");
	}
}

uint32_t snapshot_performanceTimer(struct performanceTimer* timer, bool ifPrint, FILE* out)
{
	struct timeval tv;
	uint32_t stop = sys_now(&tv);
	uint32_t diff = stop - timer->start;
	if (ifPrint == true) {
		print_tv(&tv, out);
		//fprintf(out, "%lu ms]", diff);
	}
	return diff;
}

/********************/
/* start-stop timer */
/********************/
void start_timer_startstop(comms_timer_startstop* timer, bool ifPrint)
{
	if (ifPrint == true) {
		printf("comms-startstop-timer start: ");
	}
	timer->daysec_start = snapshot_timer_startstop(ifPrint);
	timer->daysec_stop	= timer->daysec_start;
}

void stop_timer_startstop(comms_timer_startstop* timer, bool ifPrint)
{
	if (ifPrint == true) {
		printf("comms-startstop-timer stop: ");
	}
	timer->daysec_stop = snapshot_timer_startstop(ifPrint);
}

long get_time_from_start_to_stop(comms_timer_startstop* timer)
{
	return (timer->daysec_stop - timer->daysec_start);
}

long snapshot_timer_startstop(bool ifPrint)
{
	time_t unix_seconds = time(NULL);
	struct tm *UTC_time = gmtime(&unix_seconds);
	if (ifPrint == true) {
		printf("%d.%d.%d.%dh%d:%d\n",
    		UTC_time->tm_year+1900,UTC_time->tm_mon+1,UTC_time->tm_mday,UTC_time->tm_hour,UTC_time->tm_min,UTC_time->tm_sec);
	}
	return (UTC_time->tm_mday)*3600*24 + (UTC_time->tm_hour)*3600 + (UTC_time->tm_min)*60 + (UTC_time->tm_sec);
}

/*****************/
/* oneshot timer */
/*****************/
void create_timer_oneshot(int sig_nr, timer_t* p_timer)
{
    struct sigevent sigev;

    // Create the POSIX timer to generate signo
    sigev.sigev_notify = SIGEV_SIGNAL;
    sigev.sigev_signo = sig_nr;
    sigev.sigev_value.sival_ptr = p_timer;
    //create a relative timer
    //CLOCK_REALTIME represents the machine's best-guess as to the current wall-clock, time-of-day time. As Ignacio and MarkR say,
    //this means that CLOCK_REALTIME can jump forwards and backwards as the system time-of-day clock is changed, including by NTP.
    //CLOCK_MONOTONIC represents the absolute elapsed wall-clock time since some arbitrary, fixed point in the past.
    //It isn't affected by changes in the system time-of-day clock.
    //If you want to compute the elapsed time between two events observed on the one machine without an intervening reboot, CLOCK_MONOTONIC is the best option.
    if (timer_create(1, &sigev, p_timer) != 0) { //arg0: CLOCK_MONOTONIC = 1,
        perror("timer_create error!");
    }
}

void arm_timer_oneshot(int msec, timer_t* p_timer)
{
    struct itimerspec itval;
	//timeout value
    itval.it_value.tv_sec =  (time_t) msec / 1000;
    itval.it_value.tv_nsec =  (long) (msec % 1000) * (1000000L);
    //period value: =0 means one shot timer
    itval.it_interval.tv_sec = 0;
    itval.it_interval.tv_nsec = 0;
	if (timer_settime(*p_timer, 0, &itval, NULL) != 0) {
	    perror("time_settime error!");
	}
}

void disarm_timer_oneshot(timer_t* p_timer)
{
    struct itimerspec itval;
    //timeout value = 0 disarms the timer
    itval.it_value.tv_sec =  0;
    itval.it_value.tv_nsec =  0;
    //period value: =0 means one shot timer
    itval.it_interval.tv_sec = 0;
    itval.it_interval.tv_nsec = 0;
	if (timer_settime(*p_timer, 0, &itval, NULL) != 0) {
	    perror("time_settime error!");
	}
}

void delete_timer_oneshot(timer_t* p_timer)
{
	timer_delete(*p_timer);
}

long get_timer_oneshot_resttime_to_next_exp(timer_t* p_timer)
{
	struct itimerspec tv;
	timer_gettime (*p_timer, &tv); //tv.it_value is the remaining time
	return (tv.it_value.tv_nsec/1000 + tv.it_value.tv_sec*1000000L);
}
