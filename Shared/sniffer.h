/*
 * sniffer.h
 *
 *  Created on: Feb 18, 2013
 *      Author: dev
 */

#ifndef SNIFFER_H_
#define SNIFFER_H_

//default no full info
void setfullinfo();

void ProcessPacket(unsigned char* buffer, int size);

void ProcessPacketOut(unsigned char* buffer, int size);

void get_ipv4_daddr_in_arr(unsigned char* Buffer, unsigned char ipv4[4]);

#endif /* SNIFFER_H_ */
