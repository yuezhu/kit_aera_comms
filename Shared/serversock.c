/** @file comms_socket_connection.c
 *  @brief server socket connection fabric sitting on top of socklib, typedef and function implementation
 *  @author Y.Zhu
 *  @bug No known bugs.
 */

#include "serversock.h"
#include "objwrapper.h"
#include "socklib/netprog.h"
#include "socklib/socklib.h"

#include "amsg_c.h"
#include "timer.h"

#define ROLE "SVR"
#define TO_RDSOCK_MSEC 125 //milli seconds

extern struct performanceTimer PTimer;

//static void resetConnection(void* _self, void* arg);
static void acceptConnection(void* _self, void* arg);
static void readFromConnection(void* _self, void* arg);

static void handleReadConnFail(struct connInfo* conn);

void clearConnInfoStateVar(struct connInfo* conn)
{
	conn->socketCurrent = 0;
	conn->serverProtoSocket = 0;
	conn->serverAddress = NULL;
	conn->state = SERVER_DOWN;
	conn->actionOnEvent = NULL;
	conn->arg = NULL;
}

//Use this only on startup! when connInfo is never initialized
void initConnInfo(struct connInfo* conn, char* name, int port)
{
	conn->name = name;
	conn->port = port;
	clearConnInfoStateVar(conn);
}

int initConnection(struct connInfo* conn)
{
	if ((conn->serverProtoSocket == 0) && (conn->socketCurrent == 0)) {

		conn->serverProtoSocket = OpenInetProtoServer(conn->port, &(conn->serverAddress));
		if (conn->serverProtoSocket == 0) {
			snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s:%s - %s socket: OpenInetProtoServer() for port %d failed!!\n", ROLE, __FUNCTION__, conn->name, conn->port);
			return -1;
		}
		snapshot_performanceTimer(&PTimer, true, stdout); printf("%s:%s - %s socket: OpenInetProtoServer() for port %d successfully\n", ROLE, __FUNCTION__, conn->name, conn->port);
		conn->socketCurrent = conn->serverProtoSocket;
		conn->state = SERVER_UP_NOCLIENT;
		conn->actionOnEvent = acceptConnection;
	/*
		// Get buffer size
		socklen_t optlen;
		optlen = sizeof(sendbuff);
		if (getsockopt(serverProtoSocket, SOL_SOCKET, SO_SNDBUF, &sendbuff, &optlen)<0){
			printf("%s:%s: Error getsockopt one\n",ROLE, __FUNCTION__);
			return -1;
		}
		printf("%s:%s: send buffer size = %d\n", ROLE, __FUNCTION__, sendbuff);

		optval = 1;
		optlen = sizeof(optval);
		if (setsockopt(serverProtoSocket, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen) < 0) {
			printf("%s:%s: Error set SO_KEEPALIVE\n", ROLE, __FUNCTION__);
			return -2;
		}

		optval = 1;
		optlen = sizeof(optval);
		if (setsockopt(serverProtoSocket, SOL_SOCKET, SOCK_SEQPACKET , &optval, optlen) < 0) {
			printf("%s:%s: Error set SOCK_SEQPACKET\n", ROLE, __FUNCTION__);
			exit(EXIT_FAILURE);
		}

		optlen = sizeof(optval);
		if (getsockopt(serverProtoSocket, SOL_SOCKET, SOCK_SEQPACKET, &optval, &optlen)<0){
			printf("%s:%s: Error getsockopt one\n",ROLE, __FUNCTION__);
			return -3;
		}
		printf("%s:%s: SOCK_SEQPACKET enable = %d\n", ROLE, __FUNCTION__, optval);

		optlen = sizeof(timeout);
		if (setsockopt(serverProtoSocket, SOL_SOCKET, SO_SNDTIMEO, &timeout, optlen) < 0) {
			printf("%s:%s: Error set SO_SNDTIMEO\n", ROLE, __FUNCTION__);void teardownConnection(struct connInfo* self);
			return -4;
		}
	*/
	}
	else {
		snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s:%s - %s socket: Init not possible\n",ROLE, __FUNCTION__, conn->name);  //should not go here
		return -2;
	}
	return 0;
}

void teardownConnection(struct connInfo* conn)
{
	if ((conn->socketCurrent != 0) && (conn->socketCurrent != conn->serverProtoSocket)) { //the same condition as "SERVER_UP_ACCEPTED"
		snapshot_performanceTimer(&PTimer, true, stdout); printf("%s:%s - %s: close accepted socket %d\n", ROLE, __FUNCTION__, conn->name, conn->socketCurrent);
		close(conn->socketCurrent);
	}

	if (conn->serverProtoSocket != 0) { //the same condition as !SERVER_DOWN
		snapshot_performanceTimer(&PTimer, true, stdout); printf("%s:%s - %s: close serverProtoSocket %d\n", ROLE, __FUNCTION__, conn->name, conn->serverProtoSocket);
		close(conn->serverProtoSocket);
	}

	if (conn->serverAddress != NULL) { //this is malloced! in case the server is up (whatever accepted client or not)
		free(conn->serverAddress);
	}
	clearConnInfoStateVar(conn);
}

/*
static void resetConnection(void* _self, void* arg)
{
	struct connInfo* self= (struct connInfo*)_self;
	arg = NULL;
	teardownConnection(self);
	initConnection(self);
}
*/

static void acceptConnection(void* _self, void* arg)
{
	struct connInfo* self= (struct connInfo*)_self;
	if (self->serverProtoSocket != 0) { //the same condition as !SERVER_DOWN
		unsigned int nSocketSize = sizeof(struct sockaddr_in);
		int sock;
		if ((sock = ADW_accept(self->serverProtoSocket, self->serverAddress, &nSocketSize))<0) {
			ADW_close(sock); //TODO?
			snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s:%s - %s socket: Accept fail, closed\n",ROLE, __FUNCTION__, self->name);
		} else {
			self->socketCurrent = sock; //this sock must be different from serverProtoSocket
			self->state = SERVER_UP_ACCEPTED;
			self->actionOnEvent = readFromConnection; //if accepted, next step is read
			snapshot_performanceTimer(&PTimer, true, stdout); printf("%s:%s - %s socket: Accepted a client\n",ROLE, __FUNCTION__, self->name);
		}
	}
	else {
		snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s:%s - %s socket: Accept not possible\n",ROLE, __FUNCTION__, self->name);  //should not go here
	}
}


static void readFromConnection(void* _self, void* arg)
{
	Msg *getMsg;
	struct connInfo* self= (struct connInfo*)_self;
	if ((self->socketCurrent != 0) && (self->socketCurrent != self->serverProtoSocket)) { //the same condition as "SERVER_UP_ACCEPTED"
		errno = 0;			// set errno here explicitly. to be sure only errors on reading appear?!
		if (!(getMsg = ReadSockMsgTO(self->socketCurrent, TO_RDSOCK_MSEC*1000))) {				// MALLOC!
			handleReadConnFail(self);
		} else {	// have a message
			//put it into a referenced counted wrapper
			objwrapper_t* rc_amsg_c_toforward = objwrapper_create((void*)getMsg, free); //wrap the data field with a reference count "rc_"
			if (rc_amsg_c_toforward != NULL) {
				self->arg = (void*)rc_amsg_c_toforward;
				//objwrapper_retain(rc_amsg_c_toforward); //retrained by self->arg
				//objwrapper_release(rc_amsg_c_toforward); //release by this function
				snapshot_performanceTimer(&PTimer, true, stdout); printf("%s: got %d Bytes from %s\n", ROLE, get_pyldLength(objwrapper_get_object(rc_amsg_c_toforward)), self->name);
			}
			else {
				snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "MEM_ERR!%s - socket connection recv data objwrapper malloc fail!\n", __FUNCTION__);
				free(getMsg);
			}
		}
	} else {
		snapshot_performanceTimer(&PTimer, true, stdout); fprintf(stdout, "%s:%s - %s socket: not ready socket, doing nothing\n", ROLE, __FUNCTION__, self->name);  //should not go here
	}
}

int writeToServerConnection(struct connInfo* conn, void* arg)
{
	int ret_code = 0;
	if (arg!= NULL){
		objwrapper_t* rc_amsg_c_tosend = (objwrapper_t*)arg;
		if ((conn->socketCurrent != 0) && (conn->socketCurrent != conn->serverProtoSocket)) {  //the same condition as "SERVER_UP_ACCEPTED"
			if(WriteSockMsg(conn->socketCurrent, (Msg* )objwrapper_get_object(rc_amsg_c_tosend))!=0){
				snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s:%s - %s socket: buffer_send error\n", ROLE, __FUNCTION__, conn->name);
				ret_code = -1;
			} else {
				snapshot_performanceTimer(&PTimer, true, stdout); printf("%s: send %d Bytes to %s\n", ROLE, get_pyldLength(objwrapper_get_object(rc_amsg_c_tosend)), conn->name);
				ret_code = 0;
			}
		} else {
			snapshot_performanceTimer(&PTimer, true, stdout); fprintf(stdout, "%s:%s - %s socket: not ready socket, doing nothing OK\n", ROLE, __FUNCTION__, conn->name);  //should not go here
			ret_code = -2;
		}
	} else {
		ret_code = -3;
	}
	return ret_code;
}

int sendSimpleSockServer(struct connInfo* conn, unsigned char* buffer, unsigned short n)
{
	int ret_code = 0;
	unsigned short nwrite;
	if (buffer!= NULL){
		if ((conn->socketCurrent != 0) && (conn->socketCurrent != conn->serverProtoSocket)) {  //the same condition as "SERVER_UP_ACCEPTED"
			unsigned short plength = htons(n);
			if ((nwrite = cwrite(conn->socketCurrent, (char *)&plength, sizeof(plength))) <= 0) { // 0? write > 0 bytes but nothing written? considered error
				snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s:%s - %s socket: buffer_send length error\n", ROLE, __FUNCTION__, conn->name);
				ret_code = -1;
			} else {
				if ((nwrite = cwrite(conn->socketCurrent, (char*)buffer, n)) <= 0) {
					snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s:%s - %s socket: buffer_send content error\n", ROLE, __FUNCTION__, conn->name);
					ret_code = -4;
				} else { //now ok
					snapshot_performanceTimer(&PTimer, true, stdout); printf("%s: send %d Bytes to %s\n", ROLE, nwrite, conn->name);
					ret_code = 0;
				}
			}
		} else {
			snapshot_performanceTimer(&PTimer, true, stdout); fprintf(stdout, "%s:%s - %s socket: not ready socket, doing nothing OK\n", ROLE, __FUNCTION__, conn->name);  //should not go here
			ret_code = -2;
		}
	} else {
		ret_code = -3;
	}
	return ret_code;
}

int recvSimpleSockServer(struct connInfo* conn, unsigned char* buffer, unsigned short* n)
{
	unsigned short plength;
	unsigned short nread;

	if ((conn->socketCurrent != 0) && (conn->socketCurrent != conn->serverProtoSocket)) { //the same condition as "SERVER_UP_ACCEPTED"
		errno = 0;			// set errno here explicitly. to be sure only errors on reading appear?!
		  // Read length
		if ((nread = read_n(conn->socketCurrent, (char *)&plength, sizeof(plength))) <= 0) {
			// == 0: nothing read? ctrl-c at the other end? <0: socket error?
			handleReadConnFail(conn);
			*n = 0;
			return -2; //disconnect at length-read phase by all means, ctrl-c, crash etc...
		} else {	// have a message
		    /* read packet */
			unsigned short length = ntohs(plength);
			if (length > *n) { //*n has the buffer length
				snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s:%s - %s socket: IP packet has %d bytes too big to fit, forget to set MTU?\n", ROLE, __FUNCTION__, conn->name, length);
				*n = 0;
			    return -3; //get a too big packet even for internal buffer!, this is for sure bigger than transportable
			} else {
				if ((nread = read_n(conn->socketCurrent, (char*)buffer, length)) <= 0) {
					handleReadConnFail(conn);
					*n = 0;
					return -4; //disconnect at content-read phase
				} else {
					snapshot_performanceTimer(&PTimer, true, stdout); printf("%s: got %d Bytes from %s\n", ROLE, nread, conn->name);

					*n = nread;
				    return 0; //now ok! the content in buffer is valid
				}
			}
		}
	} else {
		snapshot_performanceTimer(&PTimer, true, stdout); fprintf(stdout, "%s:%s - %s socket: not ready socket, doing nothing\n", ROLE, __FUNCTION__, conn->name);  //should not go here
		return -1; //socket not ready, should not happen, as this should be excluded by code
	}
}

static void handleReadConnFail(struct connInfo* conn)
{
	int sockerr = isSocketError(conn->socketCurrent);
	snapshot_performanceTimer(&PTimer, true, stderr);
	if (sockerr == 1) {			// only =1 is a real socket error
		// ... whatever is necessary to do on errors
		fprintf(stderr, "%s:%s - %s socket: socket has socket error. Tried maybe to read on not ready socket, doing nothing\n", ROLE, __FUNCTION__, conn->name);
	} else {
		fprintf(stderr, "%s:%s - %s socket: reading message from ready socket failed, but no error??? Probably read EOF due to lost connection\n", ROLE, __FUNCTION__, conn->name);
		// if the connection is broken, reading will return EOF, which endsup here, so it is an error
	}
	conn->state = SERVER_UP_NOCLIENT; //change state
	ADW_close(conn->socketCurrent); //terminate in any case
	conn->socketCurrent = conn->serverProtoSocket; //downgrade
	conn->actionOnEvent = acceptConnection; //if read error, next step is reaccept
}
