#include <stdlib.h>
#include <stdio.h>

#include "objwrapper.h"
//#define DBG_MEM

struct objwrapper_s {
	void					*data;
	unsigned				reference_count;
	objwrapper_destructor_t destructor;
};

objwrapper_t *objwrapper_create(void *a_data, objwrapper_destructor_t a_destructor)
{
	/* allocate object wrapper */
	objwrapper_t *objwrapper = malloc(sizeof(objwrapper_t));
	if (objwrapper != NULL) {

		/* set data and destructor */
		objwrapper->data		= a_data;
		objwrapper->destructor	= a_destructor;

		/* set reference count */
		objwrapper->reference_count = 1;
	
#ifdef DBG_MEM
		printf("DBG_MEM:%s - created object %p (new rc = 1)\n", __FUNCTION__, objwrapper);
#endif
	}
	return objwrapper;
}

void *objwrapper_get_object(objwrapper_t *a_objwrapper)
{
	return a_objwrapper->data;
}

void objwrapper_retain(objwrapper_t *a_objwrapper)
{
	/* increase reference count */
	++a_objwrapper->reference_count;

#ifdef DBG_MEM
	printf("DBG_MEM:%s - retained object %p (new rc = %u)\n", __FUNCTION__, a_objwrapper, a_objwrapper->reference_count);
#endif
}

void objwrapper_release(objwrapper_t *a_objwrapper)
{
	/* decrease reference count */
	--a_objwrapper->reference_count;
	
	/* destroy object and wrapper if necessary */
	if(0 == a_objwrapper->reference_count)
	{
#ifdef DBG_MEM
		printf("DBG_MEM:%s - released object %p (new rc = 0; destroyed)\n", __FUNCTION__, a_objwrapper);
#endif
		
		/* deallocate object */
		if(a_objwrapper->destructor)
			a_objwrapper->destructor(a_objwrapper->data);
		
		/* deallocate object wrapper */
		free(a_objwrapper);
	}
	else
	{
#ifdef DBG_MEM
		printf("DBG_MEM:%s - released object %p (new rc = %u)\n", __FUNCTION__, a_objwrapper, a_objwrapper->reference_count);
#endif
	}
}

objwrapper_t *objwrapper_realloc(objwrapper_t *a_objwrapper, size_t size, objwrapper_destructor_t a_destructor)
{
	/* set data and destructor */
	void* data	= realloc(a_objwrapper->data, size);
	/* failed realloc */
	if (data == NULL)
	{
		objwrapper_release(a_objwrapper);
		return NULL;
	}
	else
	{
#ifdef DBG_MEM
		printf("DBG_MEM:%s - realloced object %p (new rc = %u)\n", __FUNCTION__, a_objwrapper, a_objwrapper->reference_count);
#endif
		a_objwrapper->data		= data;
		a_objwrapper->destructor	= a_destructor;
		/* no change on destructor */
		return a_objwrapper;
	}
}
