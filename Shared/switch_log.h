/*
 * switch_log.h
 *
 *  Created on: Nov 6, 2012
 *      Author: dev
 */

#ifndef SWITCH_LOG_H_
#define SWITCH_LOG_H_

typedef struct {
	char*	filename;
	char*	filename_switch;
	char* 	title;
	int maxLine;

	int cnt;
	int isSwitchFile;
} switch_logger;

void init_switch_logger(switch_logger* logger, char* filename, char* filename_switch, char* title, int maxLine);
void switchlogging(switch_logger* logger, char* buffer, int lines);

FILE* getfile_switchlogger(switch_logger* logger, int* is_reopen);
void registerLineWrite_switchlogger(switch_logger* logger, FILE* log_fd);


#endif /* SWITCH_LOG_H_ */
