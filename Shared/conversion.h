/*
 * conversion.h
 *
 *  Created on: Mar 23, 2012
 *      Author: dev
 */

#ifndef CONVERSION_H_
#define CONVERSION_H_

__inline__ static unsigned long conv_u8_arr(unsigned char* dat, unsigned char len) //BIG ENDIAN
{
	unsigned long ret = 0;
    int i;
    for (i=0;i<len;i++)
    {
        ret = ret|(unsigned long)(dat[i]<<(8*(len-i-1)));
    }
    return ret;
}

__inline__ static void conv_to_u8_arr(unsigned char* dat, unsigned long val, unsigned char len) //BIG ENDIAN
{
    int i;
    for (i=0;i<len;i++)
    {
        dat[i] = (unsigned char)(val>>(8*(len-i-1)));
    }
}

__inline__ static int convert_uchar2int(unsigned char byte) {
	int nativeInt;
	const int negative = (byte & (1 << 7)) != 0;
	if (negative) nativeInt = byte | ~((1 << 8) - 1);
	else nativeInt = byte;
	return nativeInt;
}

#endif /* CONVERSION_H_ */
