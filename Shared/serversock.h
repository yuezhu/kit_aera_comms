/** @file comms_socket_connection.h
 *  @brief server socket connection fabric sitting on top of socklib, typedef and function prototypes
 *
 *  this module is used to cover possible socklib changes
 *	and avoid ugly code by direct calling socklib in application
 *	how to use: first create connInfo obj --> initConnInfo() --> periodic do obj->actionOnEvent()
 *	The actionOnEvent() creates listen socket, does listen and does reading depending on (server state)
 *	User doensn't need to manipulate these, but in case of a data write or teardown, do explicit function call
 *
 *  @author Y.Zhu
 *  @bug No known bugs.
 */

#ifndef COMMS_SOCKET_CONNECTION_H_
#define COMMS_SOCKET_CONNECTION_H_

#include "socklib/socklib.h"

/** @brief enum for server socket state
 */
enum conn_states {SERVER_DOWN, SERVER_UP_NOCLIENT, SERVER_UP_ACCEPTED};

/** @brief typedef for state-dependent actions, implemented as callback
 *
 *	NOTE! change this should affect server coding, client coding, 
 *	and documentation at the same time
 */
typedef void (*actionOnEvent_t)(void* _self, void* arg);

/** @brief struct for connection
 */
struct connInfo {
	char* name;
	int port;
	int socketCurrent;
	int serverProtoSocket;
	//int serverSocket;
	struct sockaddr* serverAddress;
	enum conn_states state;

	actionOnEvent_t actionOnEvent;
	void *arg;
};

/** @brief initialization for connInfo
 */
void initConnInfo(struct connInfo* conn, char* name, int port);

/** @brief establish certain connection
 */
int initConnection(struct connInfo* conn);

/** @brief write certain connection
 *	@param conn	connection pointer
 *	@param arg	Cautious! please give a objwrapper_t* (casted internally) --> not nice, maybe change
 */
int writeToServerConnection(struct connInfo* conn, void* arg);

/** @brief tear down certain connection
 */
void teardownConnection(struct connInfo* conn);

/** @brief for "simpleSock" read and write, as a very "light" version compared to AERA socklib, used for e.g. IP bridge
 */
int sendSimpleSockServer(struct connInfo* conn, unsigned char* buffer, unsigned short n);
int recvSimpleSockServer(struct connInfo* conn, unsigned char* buffer, unsigned short* n);

#endif /* COMMS_SOCKET_CONNECTION_H_ */
