/*
 * syslogger.
 *
 *  Created on: Nov 9, 2012
 *      Author: dev
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "syslogger.h"

static FILE *log_file = NULL;
unsigned char Message_flag = 0; // flags indicating console output respectively system logging

void init_syslogger(char* path)
{
	time_t current_time;
	struct tm *local_time;
	char timestamp[18];
	char filename[256];

	current_time = time(NULL);
	local_time = localtime(&current_time);
	strftime(&timestamp[0], 17,"%Y-%m-%d_%H-%M", local_time);
	sprintf(filename, "%s/running_%s.log", path, timestamp);
	log_file = fopen(filename, "a+"); //just under current directory
	if(log_file == NULL) {
		fprintf(stderr, "Error opening running log\n");
		exit(EXIT_FAILURE);
	}
}

void print_message(int facility_priority, char* message) {
	// facility_priority may have the values (from lowest to highest):
	// LOG_DEBUG, LOG_INFO, LOG_NOTICE, LOG_WARNING, LOG_ERR, LOG_CRIT, LOG_ALERT, LOG_EMERG
	time_t current_time;
	struct tm *local_time;
	char timestamp[21];
	char message_type[20];
	// bit0 of Message_flag indicates console output
	if ((Message_flag & 0x01) == 1) {
		printf(message);
		fflush(stdout);
	}
	// bit1 of Message_flag indicates output into syslog
	if ((Message_flag & 0x02) == 0x02) {
		//setlogmask (LOG_UPTO (LOG_DEBUG));
		//openlog ("Statistic", LOG_PID | LOG_NDELAY, LOG_USER);
		if(log_file != NULL) {
			current_time = time(NULL);
			local_time = localtime(&current_time);
			strftime(&timestamp[0], 20,"%Y-%m-%d_%H:%M:%S", local_time);
			switch (facility_priority) {
			case LOG_EMERG:  	sprintf(message_type, "Emergency"); break;
			case LOG_ALERT:  	sprintf(message_type, "Alert"); break;
			case LOG_CRIT:  	sprintf(message_type, "Critical"); break;
			case LOG_ERR:  		sprintf(message_type, "Error"); break;
			case LOG_WARNING:  	sprintf(message_type, "Warning"); break;
			case LOG_NOTICE:  	sprintf(message_type, "Notice"); break;
			case LOG_INFO:  	sprintf(message_type, "Information"); break;
			case LOG_DEBUG:  	sprintf(message_type, "Debug"); break;
			default: 			sprintf(message_type, "DEBUG"); break;
			}
			// strip new_line at beginning and end of the message string
			//char *out = message;
			if (message[0] == '\n') strcpy(message, message+1);
			int message_size = strlen(message);
			if (message[message_size-1] == '\n') message[message_size-1] = '\0';
			fprintf(log_file, "%s: [%s] %s \n", timestamp, message_type, message);
			//fclose(log_file);
		}
	}
}
