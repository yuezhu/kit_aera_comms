/*
 * linux_process_ctrl.h
 *
 *  Created on: Jan 9, 2013
 *      Author: dev
 */

#ifndef LINUX_PROCESS_CTRL_H_
#define LINUX_PROCESS_CTRL_H_

int getProcessID(const char* procname, int pid_list[], int* len);

void killProcessID(int pid_list[], int len);

#endif /* LINUX_PROCESS_CTRL_H_ */
