/*
 * timer.h
 *
 *  Created on: Aug 14, 2012
 *      Author: dev
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>

#ifdef __cplusplus
extern "C" {
#endif

/***************/
/* tLayerTimer */
/***************/
typedef void (* tLayerTimeout_handler)(void *arg);

struct tLayerTimer {
  bool isActive;
  uint32_t time_lastkick; //msec
  uint32_t timeout_duration; //msec
  tLayerTimeout_handler h;
  void *arg;
};

void init_tLayerTimer(struct tLayerTimer* timer, tLayerTimeout_handler handler, void *handler_arg);
void reset_tLayerTimer(struct tLayerTimer* timer, uint32_t timeout_duration_msec);
void stop_tLayerTimer(struct tLayerTimer* timer);
void kick_tLayerTimer(struct tLayerTimer* timer);
bool checkTimeout_tLayerTimer(struct tLayerTimer* timer);

/********************/
/* performanceTimer */
/********************/
struct performanceTimer {
  uint32_t start; //msec
};

void reset_performanceTimer(struct performanceTimer* timer, bool ifPrint, FILE* out);
uint32_t snapshot_performanceTimer(struct performanceTimer* timer, bool ifPrint, FILE* out);

/********************/
/* start-stop timer */
/********************/
//obsolete, only one second accuracy
//day seconds, resolution: seconds, dynamic range: 1 year
typedef struct {
	long daysec_start;
	long daysec_stop;
} comms_timer_startstop;

void start_timer_startstop(comms_timer_startstop* timer, bool ifPrint);
void stop_timer_startstop(comms_timer_startstop* timer, bool ifPrint);
long get_time_from_start_to_stop(comms_timer_startstop* timer);//return in sec
long snapshot_timer_startstop(bool ifPrint); //return day seconds, resolution: seconds, dynamic range: 1 year

/*****************/
/* oneshot timer */
/*****************/
void create_timer_oneshot(int sig_nr, timer_t* p_timer);
void arm_timer_oneshot(int msec, timer_t* p_timer);
void disarm_timer_oneshot(timer_t* p_timer);
void delete_timer_oneshot(timer_t* p_timer);
long get_timer_oneshot_resttime_to_next_exp(timer_t* p_timer); //return usecs

#ifdef __cplusplus
}
#endif

#endif /* TIMER_H_ */
