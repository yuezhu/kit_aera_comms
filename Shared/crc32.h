#ifndef CRC32_H_
#define CRC32_H_

#include <stdbool.h>

unsigned long chksum_crc32 (unsigned char *block, unsigned int length);
void chksum_crc32gentab ();
unsigned long chksum_crc32_cont(unsigned long crc, unsigned char *block, unsigned int length, bool is_last);

#endif /*CRC32_H_*/

