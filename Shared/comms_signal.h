/*
 * comms_signal.h
 *
 *  Created on: Dec 23, 2011
 *      Author: dev
 */

#ifndef COMMS_SIGNAL_H_
#define COMMS_SIGNAL_H_

#include "signal.h"

typedef void (*sighandler_t)(int);

sighandler_t assign_signal(int sig_nr, sighandler_t signalhandler);

sighandler_t add_signal(int sig_nr, sighandler_t signalhandler);

#endif /* COMMS_SIGNAL_H_ */
