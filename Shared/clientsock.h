/*
 * comms_socket.h
 *
 *  Created on: Sep 3, 2012
 *      Author: dev
 */

#ifndef COMMS_SOCKET_CONNECTION_H_
#define COMMS_SOCKET_CONNECTION_H_

#include <stdbool.h>
#include <stdint.h>

typedef void (*actionOnEvent_t)(void* _self, void* arg);
struct connCliInfo {
	char* name;
	char* ip;
	int port;
	int socket;
};

/* give pointer to persistant char buffer, no strcpy() within */
void initConnCliInfo(struct connCliInfo* conn, char* name, char* ip, int port);

bool clientConnect(struct connCliInfo* conn);

void readFromClientConnections(struct connCliInfo conn[], uint8_t numConns, void** arg, uint8_t* connIndex, int timeout_usec);

int writeToClientConnection(struct connCliInfo* conn, void* arg);

void recoverConnectionIfLost(struct connCliInfo* conn);

int sendSimpleSockClient(struct connCliInfo* conn, unsigned char* buffer, unsigned short n);

int recvSimpleSockClient(struct connCliInfo* conn, unsigned char* buffer, unsigned short* n);

#endif /* COMMS_SOCKET_CONNECTION_H_ */
