/** @file amsg_c.c
 *  @brief manipulation functions for amsg container implementation
 *  @author Y.Zhu
 *  @bug No known bugs.
 */

#include <assert.h>
#include "amsg_c.h"
#include "crc32.h"
#include "timer.h"

static uint16_t Running_pyld_number_RT = 1;
static uint16_t Running_pyld_number_HQ = 1;

extern struct performanceTimer PTimer;
extern struct performanceTimer LatencyTimer;

void fill_testdata(uint8_t* amsg_c, uint16_t pyldLength, bool isRT, bool ifPrint);

uint16_t get_pyldLength(uint8_t* amsg_c)
{
	uint16_t* amsg_c2 = (uint16_t*)amsg_c;
	return amsg_c2[0];
}

void fill_testdata(uint8_t* amsg_c, uint16_t pyldLength, bool isRT, bool ifPrint)
{
	if (amsg_c != NULL) {
		int memLength = SOCKLEN_CONV(pyldLength);
		assert (memLength>=SOCKLEN_CONV(sizeof(uint16_t)+sizeof(uint32_t)+sizeof(unsigned long)));
		uint16_t* amsg_c2 = (uint16_t*)amsg_c;
		amsg_c2[0] = pyldLength;

		//data:
		uint16_t running_number = (isRT==true)?(Running_pyld_number_RT++):(Running_pyld_number_HQ++);
		amsg_c2[1] = running_number;

		uint32_t add_time = snapshot_performanceTimer(&LatencyTimer, false, stdout);
		amsg_c2[2] = (uint16_t)(add_time>>16);
		amsg_c2[3] = (uint16_t)add_time;

		int i;
		for (i=sizeof(uint16_t)*4; i<memLength-sizeof(unsigned long); i++) {
			amsg_c[i] = rand()%0xffff;
		}
		unsigned long crc32 = chksum_crc32(amsg_c, (memLength-sizeof(unsigned long)));
		amsg_c[memLength-4] = (crc32 >> 24)&0xff;
		amsg_c[memLength-3] = (crc32 >> 16)&0xff;
		amsg_c[memLength-2] = (crc32 >> 8)&0xff;
		amsg_c[memLength-1] = (crc32 >> 0)&0xff;
		if (ifPrint) {
			snapshot_performanceTimer(&PTimer, true, stdout); printf("[%s SEND_PREP!] payload length: %d, run_no: %d, crc: 0x%lx\n",  (isRT==true)?"RT":"HQ", pyldLength, running_number, crc32);
		}
	}
}

bool autocheck_testdata(uint8_t* amsg_c, bool isRT, uint32_t* delay, bool ifPrint)
{
	if (amsg_c != NULL) {
		uint16_t* amsg_c2 = (uint16_t*)amsg_c;
		uint16_t pyldLength = amsg_c2[0];
		int memLength = SOCKLEN_CONV(pyldLength);
		assert (memLength>=SOCKLEN_CONV(sizeof(uint16_t)+sizeof(uint32_t)+sizeof(unsigned long)));
		uint16_t running_number = amsg_c2[1];

		uint32_t add_time = ((uint32_t)(amsg_c2[2])<<16)|(uint32_t)(amsg_c2[3]);
		uint32_t recv_time = snapshot_performanceTimer(&LatencyTimer, false, stdout);
		*delay = recv_time - add_time;

		unsigned long crc32_cal = chksum_crc32(amsg_c, (memLength-sizeof(unsigned long)));
		unsigned long crc32 = ((uint32_t)(amsg_c[memLength-4])<<24)|((uint32_t)(amsg_c[memLength-3])<<16)|((uint32_t)(amsg_c[memLength-2])<<8)|(uint32_t)(amsg_c[memLength-1]);
		if (ifPrint) {
			snapshot_performanceTimer(&PTimer, true, stdout); printf("[%s RECV_CHK!] payload length: %d, run_no: %d, crc_cal: 0x%lx, crc_get: 0x%lx, delay: %u ms, %s\n", (isRT==true)?"RT":"HQ", pyldLength, running_number, crc32_cal, crc32, *delay, (crc32==crc32_cal)?"OK":"ERROR");
		}
		if (crc32==crc32_cal) {
			return true;
		}
	}
	return false;
}

uint8_t* create_test_amsg_c(uint16_t pyldLength, bool isRT)
{
	uint32_t sockLen = SOCKLEN_CONV(pyldLength);
	uint8_t* amsg_c = (uint8_t*)malloc(sockLen);
	if (amsg_c != NULL) {
		fill_testdata(amsg_c, pyldLength, isRT, true);
	}
	return amsg_c;
}

/*
static inline uint16_t get_pyldLength(uint8_t* amsg_c)
{
	uint16_t* amsg_c2 = (uint16_t*)amsg_c;
	return amsg_c2[0];
}

static inline void fill_testdata(uint8_t* amsg_c, uint16_t pyldLength, bool ifPrint)
{
	if (amsg_c != NULL) {
		int memLength = SOCKLEN_CONV(pyldLength);
		assert (memLength>=SOCKLEN_CONV(1)+sizeof(unsigned long));
		uint16_t* amsg_c2 = (uint16_t*)amsg_c;
		amsg_c2[0] = pyldLength;

		//data:
		uint16_t running_number = Running_pyld_number++;
		amsg_c2[1] = running_number;
		int i;
		for (i=sizeof(uint16_t)*2; i<memLength-sizeof(unsigned long); i++) {
			amsg_c[i] = rand()%0xffff;
		}
		unsigned long crc32 = chksum_crc32(amsg_c, (memLength-sizeof(unsigned long)));
		amsg_c[memLength-4] = (crc32 >> 24)&0xff;
		amsg_c[memLength-3] = (crc32 >> 16)&0xff;
		amsg_c[memLength-2] = (crc32 >> 8)&0xff;
		amsg_c[memLength-1] = (crc32 >> 0)&0xff;
		if (ifPrint) {
			snapshot_performanceTimer(&PTimer, true, stdout); printf("[SEND_PREP!] payload length: %d, run_no: %d, crc: 0x%lx\n", pyldLength, running_number, crc32);
		}
	}
}

static inline void autocheck_testdata(uint8_t* amsg_c, bool ifPrint)
{
	if (amsg_c != NULL) {
		uint16_t* amsg_c2 = (uint16_t*)amsg_c;
		uint16_t pyldLength = amsg_c2[0];
		int memLength = SOCKLEN_CONV(pyldLength);
		assert (memLength>=SOCKLEN_CONV(1)+sizeof(unsigned long));
		uint16_t running_number = amsg_c2[1];
		unsigned long crc32_cal = chksum_crc32(amsg_c, (memLength-sizeof(unsigned long)));
		unsigned long crc32 = ((uint32_t)(amsg_c[memLength-4])<<24)|((uint32_t)(amsg_c[memLength-3])<<16)|((uint32_t)(amsg_c[memLength-2])<<8)|(uint32_t)(amsg_c[memLength-1]);
		if (ifPrint) {
			snapshot_performanceTimer(&PTimer, true, stdout); printf("[RECV_CHK!] payload length: %d, run_no: %d, crc_cal: 0x%lx, crc_get: 0x%lx, %s\n", pyldLength, running_number, crc32_cal, crc32, (crc32==crc32_cal)?"OK":"ERROR");
		}
	}
}
*/
