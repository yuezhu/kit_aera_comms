/*
 * linux_process_ctrl.c
 *
 *  Created on: Jan 9, 2013
 *      Author: dev
 */

#include <math.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/time.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>

#define SIGNR_CTRLC SIGINT

/************************************************************************
 *
 * Returns the pid of the process name which is given as the function
 * argument.
 * In case no process found with the given name -1 will be returned.
 *
 *************************************************************************/
int getProcessID(const char* procname, int pid_list[], int* len) {
	FILE *shellCommand;
	char path[1035], command[100];

	/*build up the pgrep command like it would be used directly in the shell, -x is an argument for pgrep, for only returning PID of exact matches, for more check manual for pgrep*/
	//sprintf(command,"%s","pgrep -x u12rec");//prepare to lookup pid for a program called "u12rec" with pgrep

	sprintf(command, "ps -eo pid,user,comm | awk \'/[%c]%s/{print $1}\'", procname[0], &procname[1]);
	//now another generic version using ps awk, works both for LS with busybox "ps" and SBC debian linux using ps -o formatter
	printf("---- command: %s\n", command);

	shellCommand = popen(command, "r");
	if (shellCommand == NULL) {
		fprintf(stderr, "%s: command \"%s\" cannot be executed or other errors\n", __FUNCTION__, command);
		return -1;
	}

	int max_len = *len;
	*len = 0;
	while (fgets(path, sizeof(path)-1, shellCommand) != NULL) {
		sscanf(path, "%d", &pid_list[*len]);
		(*len)++;
		if (*len == max_len) {
			break;
		}
	}

	pclose(shellCommand);
	return 0;
}

void killProcessID(int pid_list[], int len) {
	int i;
	for (i=0; i<len; i++) {
		if (kill(pid_list[i], SIGNR_CTRLC) < 0) {
			fprintf(stderr, "%s: kill pid: %d fail\n", __FUNCTION__, pid_list[i]);
		}
	}
}
/*
int main(int argc, char *argv[])
{
	FILE *pp=NULL;
	mode_t mode;
	struct stat buf;
	int rc=0, receiverPid=0, i=0, quit=0;
	char pipeName[100], pidString[10], input[100];

	sprintf("pid of program: %d",getProcessID());

	exit(0);
}
*/

