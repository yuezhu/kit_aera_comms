//from internet

#ifndef __OBJWRAPPER_H__
#define __OBJWRAPPER_H__

#ifdef __cplusplus
extern "C" {
#endif

/* destructor for an object wrapper */
typedef void (*objwrapper_destructor_t)(void *a_data);

/* object wrapper type */
typedef struct objwrapper_s objwrapper_t;

/* creates an object wrapper */
objwrapper_t *objwrapper_create(void *a_data, objwrapper_destructor_t a_destructor);

/* returns the object inside a wrapper */
void *objwrapper_get_object(objwrapper_t *a_objwrapper);

/* retains the object inside a wrapper */
void objwrapper_retain(objwrapper_t *a_objwrapper);

/* releases the object inside a wrapper */
void objwrapper_release(objwrapper_t *a_objwrapper);

/* added! this is dangerous */
/* realloc disobey the principle of reference counting! (one referencer change something without let other referencers know..) */
/* use is only you are sure that the change is ok for all! */
objwrapper_t *objwrapper_realloc(objwrapper_t *a_objwrapper, size_t size, objwrapper_destructor_t a_destructor);

#ifdef __cplusplus
}
#endif

#endif /* __OBJWRAPPER_H__ */
