/*
 * switch_log.c
 *
 *  Created on: Nov 6, 2012
 *      Author: dev
 */
#include <stdio.h>
#include <stdlib.h>
#include "switch_log.h"

void init_switch_logger(switch_logger* logger, char* filename, char* filename_switch, char* title, int maxLine)
{
	logger->filename = filename;
	logger->filename_switch = filename_switch;
	logger->title = title;
	logger->maxLine = maxLine;
	logger->cnt = 0;
	logger->isSwitchFile = 0; //start from normal file
}

void switchlogging(switch_logger* logger, char* buffer, int lines)
{
	FILE* log_fd = NULL;

	if (logger->cnt == 0) {
		if (logger->isSwitchFile == 0) {
			log_fd = fopen(logger->filename, "w"); //wr open with truncate
		} else {
			log_fd = fopen(logger->filename_switch, "w"); //wr open with truncate
		}
		//write title
		fputs(logger->title, log_fd);
	}
	else {
		if (logger->isSwitchFile == 0) {
			log_fd = fopen(logger->filename, "a"); //wr open for append
		} else {
			log_fd = fopen(logger->filename_switch, "a"); //wr open with truncate
		}
	}
	if (log_fd == NULL) {
		fprintf(stderr, "%s - cannot open file to write\n", __FUNCTION__);
		exit(EXIT_FAILURE);
	}

	fputs(buffer, log_fd);

	fclose(log_fd);

	if (logger->cnt >= logger->maxLine - 1) {
		logger->cnt=0;
		logger->isSwitchFile = ~logger->isSwitchFile; //switch file
	} else {
		logger->cnt++;
	}
}

FILE* getfile_switchlogger(switch_logger* logger, int* is_reopen)
{
	FILE* log_fd = NULL;

	*is_reopen = 0;
	if (logger->cnt == 0) {
		if (logger->isSwitchFile == 0) {
			log_fd = fopen(logger->filename, "w"); //wr open with truncate
		} else {
			log_fd = fopen(logger->filename_switch, "w"); //wr open with truncate
		}
		*is_reopen = 1;
	}
	else {
		if (logger->isSwitchFile == 0) {
			log_fd = fopen(logger->filename, "a"); //wr open for append
		} else {
			log_fd = fopen(logger->filename_switch, "a"); //wr open with truncate
		}
		*is_reopen = 0;
	}
	return log_fd;
}

void registerLineWrite_switchlogger(switch_logger* logger, FILE* log_fd)
{
	fclose(log_fd);

	if (logger->cnt >= logger->maxLine - 1) {
		logger->cnt=0;
		logger->isSwitchFile = ~logger->isSwitchFile; //switch file
	} else {
		logger->cnt++;
	}
}

