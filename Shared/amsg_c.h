/** @file amsg_c.h
 *  @brief manipulation functions for amsg container prototype
 *  @author Y.Zhu
 *  @bug No known bugs.
 */

#ifndef __AMSG_C_H__
#define __AMSG_C_H__

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#include "socklib/socklib.h"

/** @brief convert amsg payload length to actual length of bytes transfered on socket
 *
 *	2 bytes difference, because the embedded MsgLength does not count itself
 */
#define SOCKLEN_CONV(pyldLength)	TotalMsgLength(pyldLength) //(pyldLength+2)

/** @brief convert actual length of bytes transfered on socket back to amsg payload length
 *
 *	2 bytes difference, because the embedded MsgLength does not count itself
 */
#define PYLDLENGTH_CONV(sockLen)	MsgDataLength(sockLen) //convert back


/** @brief get the embedded payload length
 */
uint16_t get_pyldLength(uint8_t* amsg_c);

/** @brief data check amsg against the test CRC32
 *	@param [in] amsg_c 	amsg to check
 *	@param [in] isRT	the amsg has no idea, if it is RT or HQ, this is just for print
 *	@param [out] delay	gives the delay time in ms of this message
 *	@param [in]	ifPrint	if print some debug info to stdout
 *	@return bool	if testdata valid or not (CRC32)	
 */
bool autocheck_testdata(uint8_t* amsg_c, bool isRT, uint32_t* delay, bool ifPrint);

/** @brief generate random data filled amsg container with running no, embedded CRC32, creation time for delay measurement (internal malloc)
 *	@param [in] pyldLength 	payload length of the amsg_c, at least 10 bytes!!!!
 *	@param [in] isRT	RT or HQ has separate counter for running number
 *	@return uint8_t*	pointer to the malloced amsg container
 */
uint8_t* create_test_amsg_c(uint16_t pyldLength, bool isRT);

#ifdef __cplusplus
}
#endif

#endif /* __AMSG_C_H__ */
