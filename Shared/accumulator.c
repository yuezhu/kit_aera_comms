/** @file accumulator.c
 *  @brief
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#include "accumulator.h"
#include "auxiliary.h"
#include <math.h>

void add_sigma(sigma_t* a, int val) {
	a->inst = val;
	a->sigma += val;
	a->N++;
}

void reset_sigma(sigma_t* a) {
	a->inst = 0;
	a->N = 0;
	a->sigma = 0;
}

float cal_avg_sigma(sigma_t* a) {
	return a->N==0?0.0:(float)a->sigma/(float)a->N;
}

void add_minmaxsigma(minmaxsigma_t* a, int val) {
	a->inst = val;
	a->sigma += val;
	if (a->N > 0) {
		a->max_reg = MAX_COMMS(a->max_reg, val);
		a->min_reg = MIN_COMMS(a->min_reg, val);
	} else { //N==0
		a->max_reg = val;
		a->min_reg = val;
	}
	a->N++;
}

void reset_minmaxsigma(minmaxsigma_t* a) {
	a->inst = 0;
	a->N = 0;
	a->sigma = 0;
	a->max_reg = 0;
	a->min_reg = 0;
}

float cal_avg_minmaxsigma(minmaxsigma_t* a) {
	return a->N==0?0.0:(float)a->sigma/(float)a->N;
}

void add_minmaxsigma2(minmaxsigma2_t* a, int val) {
	a->inst = val;
	a->sigma += val;
	a->sigma2 += val*val;
	if (a->N > 0) {
		a->max_reg = MAX_COMMS(a->max_reg, val);
		a->min_reg = MIN_COMMS(a->min_reg, val);
	} else { //N==0
		a->max_reg = val;
		a->min_reg = val;
	}
	a->N++;
}

void reset_minmaxsigma2(minmaxsigma2_t* a) {
	a->inst = 0;
	a->N = 0;
	a->sigma = 0;
	a->sigma2  = 0;
	a->max_reg = 0;
	a->min_reg = 0;
}

float cal_avg_minmaxsigma2(minmaxsigma2_t* a) {
	return a->N==0?0.0:(float)a->sigma/(float)a->N;
}

float cal_stddev_minmaxsigma2(minmaxsigma2_t* a) {
	if (a->N==0) return 0.0;
	float avg = (float)a->sigma/(float)a->N;
	float stddev = sqrt( (float)a->sigma2 / (float)a->N - avg*avg );
	return stddev;
}
