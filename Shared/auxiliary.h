/*
 * auxiliary.h
 *
 *  Created on: Dec 4, 2012
 *      Author: dev
 */

#ifndef AUXILIARY_H_
#define AUXILIARY_H_

#define MIN_COMMS(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX_COMMS(X,Y) ((X) > (Y) ? (X) : (Y))

#endif /* AUXILIARY_H_ */
