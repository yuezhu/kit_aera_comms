#include "def.h"

/**
 * Convert an alt_u16 from host- to network byte order.
 *
 * @param n alt_u16 in host byte order
 * @return n in network byte order
 */

unsigned short htons_comms(unsigned short n)
{
  return ((n & 0xff) << 8) | ((n & 0xff00) >> 8);
}

/**
 * Convert an alt_u16 from network- to host byte order.
 *
 * @param n alt_u16 in network byte order
 * @return n in host byte order
 */
unsigned short ntohs_comms(unsigned short n)
{
  return htons_comms(n);
}

/**
 * Convert an alt_u32 from host- to network byte order.
 *
 * @param n alt_u32 in host byte order
 * @return n in network byte order
 */
unsigned long htonl_comms(unsigned long n)
{
  return ((n & 0xff) << 24) |
    ((n & 0xff00) << 8) |
    ((n & 0xff0000UL) >> 8) |
    ((n & 0xff000000UL) >> 24);
}

/**
 * Convert an alt_u32 from network- to host byte order.
 *
 * @param n alt_u32 in network byte order
 * @return n in host byte order
 */
unsigned long ntohl_comms(unsigned long n)
{
  return htonl_comms(n);
}
