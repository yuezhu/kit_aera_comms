#ifndef __DEF_H__
#define __DEF_H__

#ifdef __cplusplus
extern "C" {
#endif

__inline__ static unsigned long conv_u8_arr(unsigned char* dat, unsigned char len) //BIG ENDIAN
{
	unsigned long ret = 0;
    int i;
    for (i=0;i<len;i++)
    {
        ret = ret|(unsigned long)(dat[i]<<(8*(len-i-1)));
    }
    return ret;
}

__inline__ static void conv_to_u8_arr(unsigned char* dat, unsigned long val, unsigned char len) //BIG ENDIAN
{
    int i;
    for (i=0;i<len;i++)
    {
        dat[i] = (unsigned char)(val>>(8*(len-i-1)));
    }
}

unsigned short htons_comms(unsigned short n);
unsigned short ntohs_comms(unsigned short n);
unsigned long htonl_comms(unsigned long n);
unsigned long ntohl_comms(unsigned long n);

#ifdef __cplusplus
}
#endif

#endif /* __DEF_H__ */

