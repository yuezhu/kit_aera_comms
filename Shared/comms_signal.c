/*
 * comms_signal.c
 *
 *  Created on: Dec 23, 2011
 *      Author: dev
 */
#include "comms_signal.h"
#include "stdio.h" /* for NULL */

sighandler_t assign_signal(int sig_nr, sighandler_t signalhandler)
{
	struct sigaction new_sig, old_sig;
	//sigaction structure:
	//sighandler_t sa_handler: function pointer to signal handler function or SIG_DFL(system standard action) or SIG_IGN (ignore)
	//sigset_t sa_mask: signals, that will be blocked during the execution of signal handler function
	//sa_flags: special flags
	new_sig.sa_handler = signalhandler;
	sigemptyset(&new_sig.sa_mask); //allow all signals (block no signals) during the execution of signal handler function
	new_sig.sa_flags = SA_RESTART; //SA_RESTART: interrupted at a system call (such as write()), the system call will be restarted
	if (sigaction(sig_nr, &new_sig, &old_sig)<0) { //link the signal (sig_nr) to the signalhandler!! and update the old signalhandler of this signal
		return SIG_ERR;
	}
	return old_sig.sa_handler;
}

sighandler_t add_signal(int sig_nr, sighandler_t signalhandler)
{
	struct sigaction add_sig;
	if(sigaction(sig_nr, NULL, &add_sig)<0) { //update the old signalhandler of this signal
		return SIG_ERR;
	}
	add_sig.sa_handler = signalhandler;
	sigaddset(&add_sig.sa_mask, sig_nr); //block the newly added signal during the execution of signal handler function
	add_sig.sa_flags = SA_RESTART; //SA_RESTART: interrupted at a system call (such as write()), the system call will be restarted
	if (sigaction(sig_nr, &add_sig, NULL)<0) { //link the signal (sig_nr) to the new signalhandler!!
		return SIG_ERR;
	}
	return add_sig.sa_handler;
}
