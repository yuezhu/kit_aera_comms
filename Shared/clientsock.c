/*
 * comms_socket_connection.c
 *
 *  Created on: Sep 3, 2012
 *      Author: dev
 */

#include "clientsock.h"
#include "socklib/socklib.h"
#include "socklib/netprog.h"

#include "amsg_c.h"
#include "timer.h"

#define ROLE "CLI"

extern struct performanceTimer PTimer;

static fd_set readFDS;					// fd_set to check if there is something from Gui to be read

static void handleReadConnFail(struct connCliInfo* conn);

void initConnCliInfo(struct connCliInfo* conn, char* name, char* ip, int port)
{
	conn->name = name;
	conn->ip = ip;
	conn->port = port;
	conn->socket = 0;
}

bool clientConnect(struct connCliInfo* conn) {
	if (!(conn->socket = OpenInetClient(conn->port, conn->ip))) {
		snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "connectToStation(): OpenInetClient() for port %d failed!!\n", conn->port);
		conn->socket = 0;
		return false;
	}
	snapshot_performanceTimer(&PTimer, true, stdout); printf("connectToStation(): OpenInetClient() for %s on %s:%d successfully. Have socket %d\n", conn->name, conn->ip, conn->port, conn->socket);
	//dumpSockOpt(conn->socket);
	return true;
}

void readFromClientConnections(struct connCliInfo conn[], uint8_t numConns, void** arg, uint8_t* connIndex, int timeout_usec) {
//	int msgReadCount = 0;
	int maxReadNum = 0;
	int readSockCount = 0;
	struct timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = timeout_usec;
	Msg *getMsg;
	int i;
	*arg = NULL;
	*connIndex = 0;

	FD_ZERO(&readFDS);		// blank the mask
	for (i=0;i<numConns;i++) {
//		printf("%s:%s - sock of connection at index %d is %d\n", ROLE, __FUNCTION__ , i , LsConn[i].sock);
		if (conn[i].socket != 0) {
			FD_SET(conn[i].socket,&readFDS);
			maxReadNum = MAX(maxReadNum, conn[i].socket);
		}
	}

//	cout<<"checkStationsForRead() - maxReadNum is "<<maxReadNum<<endl;

	readSockCount = 0;

	if ((readSockCount = select(maxReadNum+1,&readFDS,NULL,NULL,&timeout)) > 0) {	// check all connected stations for read
    	if (readSockCount != 0) {			// some sockets are ready to read
    		//printf("%s:%s - %s socket: select returned %d sockets ready to be read ...\n", ROLE, __FUNCTION__, conn[i].name, readSockCount);
    		for (i=0;i<numConns;i++) {
    			if (conn[i].socket != 0) {
    				if (!FD_ISSET(conn[i].socket, &readFDS)) continue;		// this socket is not in the list of readable sockets
    				//printf("%s:%s - %s socket: conn at index %d has sock_fd %d, trying to read ...\n", ROLE, __FUNCTION__, conn[i].name, i, conn[i].socket);
    				errno = 0;			// set errno here explicitly. to be sure only errors on reading appear?!
    				if (!(getMsg = ReadSockMsgTO(conn[i].socket, timeout.tv_usec))) {
						snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s:%s - %s socket: conn at index %d has sock_fd %d, read returned NULL!\n", ROLE, __FUNCTION__, conn[i].name, i, conn[i].socket);
						handleReadConnFail(&conn[i]);
					} else {	// have a message
						*arg = (void*)getMsg;
						*connIndex = i;
						snapshot_performanceTimer(&PTimer, true, stdout); printf("%s: got %d Bytes from %s\n", ROLE, get_pyldLength(*arg), conn->name);
						break; //break after one msg is got
						// do not forget to free the message after ReadSockMsg()!
					}
    			}
    		}
    	}
    }
}

int writeToClientConnection(struct connCliInfo* conn, void* arg)
{
	int ret_code = 0;
	if (arg!= NULL){
		if (conn->socket != 0) {  //the same condition as "SERVER_UP_ACCEPTED"
			if(WriteSockMsg(conn->socket, (Msg* )arg)!=0){
				snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s:%s - %s socket: buffer_send error\n", ROLE, __FUNCTION__, conn->name);
				ret_code = -1;
			} else {
				snapshot_performanceTimer(&PTimer, true, stdout); printf("%s: send %d Bytes to %s\n", ROLE, get_pyldLength(arg), conn->name);
				ret_code = 0;
			}
		} else {
			snapshot_performanceTimer(&PTimer, true, stdout); fprintf(stdout, "%s:%s - %s socket: not ready socket, doing nothing OK\n", ROLE, __FUNCTION__, conn->name);  //should not go here
			ret_code = -2;
		}
	} else {
		ret_code = -3;
	}
	return ret_code;
}

int sendSimpleSockClient(struct connCliInfo* conn, unsigned char* buffer, unsigned short n)
{
	int ret_code = 0;
	unsigned short nwrite;
	if (buffer!= NULL){
		if (conn->socket != 0) {
			unsigned short plength = htons(n);
			if ((nwrite = cwrite(conn->socket, (char *)&plength, sizeof(plength))) <= 0) { // 0? write > 0 bytes but nothing written? considered error
				snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s:%s - %s socket: buffer_send length error\n", ROLE, __FUNCTION__, conn->name);
				ret_code = -1;
			} else {
				if ((nwrite = cwrite(conn->socket, (char*)buffer, n)) <= 0) {
					snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s:%s - %s socket: buffer_send content error\n", ROLE, __FUNCTION__, conn->name);
					ret_code = -4;
				} else { //now ok
					snapshot_performanceTimer(&PTimer, true, stdout); printf("%s: send %d Bytes to %s\n", ROLE, nwrite, conn->name);
					ret_code = 0;
				}
			}
		} else {
			snapshot_performanceTimer(&PTimer, true, stdout); fprintf(stdout, "%s:%s - %s socket: not ready socket, doing nothing OK\n", ROLE, __FUNCTION__, conn->name);  //should not go here
			ret_code = -2;
		}
	} else {
		ret_code = -3;
	}
	return ret_code;
}

int recvSimpleSockClient(struct connCliInfo* conn, unsigned char* buffer, unsigned short* n)
{
	unsigned short plength;
	unsigned short nread;

	if (conn->socket != 0) {
		errno = 0;			// set errno here explicitly. to be sure only errors on reading appear?!
		  // Read length
		if ((nread = read_n(conn->socket, (char *)&plength, sizeof(plength))) <= 0) {
			// == 0: nothing read? ctrl-c at the other end? <0: socket error?
			handleReadConnFail(conn);
			*n = 0;
			return -2; //disconnect at length-read phase by all means, ctrl-c, crash etc...
		} else {	// have a message
		    /* read packet */
			unsigned short length = ntohs(plength);
			if (length > *n) { //*n has the buffer length
				snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s:%s - %s socket: IP packet has %d bytes too big to fit, forget to set MTU?\n", ROLE, __FUNCTION__, conn->name, length);
				*n = 0;
			    return -3; //get a too big packet even for internal buffer!, this is for sure bigger than transportable
			} else {
				if ((nread = read_n(conn->socket, (char*)buffer, length)) <= 0) {
					handleReadConnFail(conn);
					*n = 0;
					return -4; //disconnect at content-read phase
				} else {
					snapshot_performanceTimer(&PTimer, true, stdout); printf("%s: got %d Bytes from %s\n", ROLE, nread, conn->name);
					*n = nread;
				    return 0; //now ok! the content in buffer is valid
				}
			}
		}
	} else {
		snapshot_performanceTimer(&PTimer, true, stdout); fprintf(stdout, "%s:%s - %s socket: not ready socket, doing nothing\n", ROLE, __FUNCTION__, conn->name);  //should not go here
		return -1; //socket not ready, should not happen, as this should be excluded by code
	}
}


void recoverConnectionIfLost(struct connCliInfo* conn)
{
	if(conn->socket == 0) {
		if (clientConnect(conn)) {
			snapshot_performanceTimer(&PTimer, true, stdout); printf("%s:%s - %s socket: reconnected\n", ROLE, __FUNCTION__, conn->name);
		} else {
			snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s:%s - %s socket: reconnect fail\n", ROLE, __FUNCTION__, conn->name);
		}
	}
}

static void handleReadConnFail(struct connCliInfo* conn)
{
	int sockerr = isSocketError(conn->socket);
	snapshot_performanceTimer(&PTimer, true, stderr);
	if (sockerr == 1) {			// only =1 is a real socket error
		// ... whatever is necessary to do on errors
		snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s:%s - %s socket: socket has socket error. Tried maybe to read on not ready socket, doing nothing\n", ROLE, __FUNCTION__, conn->name);
		ADW_close(conn->socket);//important! also close is needed or the socket is in CLOSE_WAIT!
		conn->socket = 0;
	} else if (sockerr == 0){
		snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s:%s - %s socket: reading message from ready socket failed, but no error??? Probably read EOF due to lost connection\n", ROLE, __FUNCTION__, conn->name);
		// if the connection is broken, reading will return EOF, which endsup here, so it is an error
		ADW_close(conn->socket);//important! also close is needed or the socket is in CLOSE_WAIT!
		conn->socket = 0;
	} else{
		snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s:%s - %s socket: reading message from ready socket failed, but no socket error (%d). Tried maybe to read on not ready socket, doing nothing\n", ROLE, __FUNCTION__, conn->name, sockerr);
	}
}
