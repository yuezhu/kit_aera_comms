/*
 * syslogger.h
 *
 *  Created on: Nov 9, 2012
 *      Author: dev
 */

#ifndef SYSLOGGER_H_
#define SYSLOGGER_H_

#include <syslog.h> /* required to include to use the syslogger */
#include <stdio.h>	 /* required to include to use the syslogger */

extern unsigned char Message_flag;

void print_message(int facility_priority, char* message);

void init_syslogger(char* path);

#endif /* SYSLOGGER_H_ */
