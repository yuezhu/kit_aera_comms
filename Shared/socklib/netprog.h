#include  <netinet/in.h>
#include  <netinet/tcp.h>
#include  <sys/socket.h>
#include  <sys/un.h>
#include  <arpa/inet.h>
#include  <netdb.h>
#include  <sys/wait.h>
#include  <sys/stat.h>
#include  <sys/select.h>
#include  <sys/poll.h>
//#include  <stropts.h>
#include  <fcntl.h>
#include  <signal.h>
#include  <errno.h>
#include  "eighdr.h"

#define   MAXSOCKADDR  128

typedef void  sigfunc(int);  /* f�r Signalhandler */

int         connecttimeout(int sockfd, const struct sockaddr *sa,
                           socklen_t len, int sek);
int         mkadr(char *adr_str, char *protocol, void *sa, socklen_t *salen);
int         ADW_accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
void        ADW_bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
void        ADW_close(int fd);
void        ADW_connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
void        ADW_getpeername(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
void        ADW_getsockname(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
const char *ADW_inet_ntop(int family, const void *str, char *nw, size_t len);
void        ADW_inet_pton(int family, const char *str, void *nw);
void        ADW_listen(int sockfd, int backlog);
int         ADW_poll(struct pollfd *fdarray, unsigned int nfds, int timeout);
ssize_t     ADW_read(int fd, void *puff, size_t count, const char *meld);
ssize_t     ADW_recv(int sockfd, void *puff, size_t n, int flags);
ssize_t     ADW_recvfrom(int sockfd, void *puff, size_t len, int flags,
                        struct sockaddr *from, socklen_t *fromlen);
int         ADW_select(int maxfd, fd_set *lesefds, fd_set *schreibfds,
                                 fd_set *exceptfds, struct timeval *timeout);
void        ADW_send(int sockfd, const void *puff, ssize_t n, int flags);
void        ADW_sendto(int sockfd, const void *puff, size_t len, int flags,
                      const struct sockaddr *to, socklen_t tolen);
void        ADW_shutdown(int fd, int wie);
sigfunc    *ADW_signal(int signr, sigfunc *func);
int         ADW_socket(int family, int type, int protocol);
void        ADW_socketpair(int domain, int typ, int protocol, int sv[2]);
char       *ADW_sock_ntop(const struct sockaddr *sa, socklen_t salen);
int         ADW_tcp_connect(const char *host, const char *service);
int         ADW_tcp_listen(const char *host, const char *service, socklen_t *addrlen);
int         ADW_udp_client(const char *host, const char *service,
                          struct sockaddr **sa, socklen_t *len);
int         ADW_udp_connect(const char *host, const char *service);
int         ADW_udp_server(const char *host, const char *service, socklen_t *addrlen);
void        ADW_write(int fd, const void *puff, size_t count, const char *meld);
ssize_t     readline_sock(int fd, void *puffer, size_t maxlen);
ssize_t     read_sock(int fd, void *puffer, size_t n);
int         selecttimeout(int fd, int sek);
int         sockatmark(int fd);
int         sockfd_family(int sockfd);
ssize_t     write_sock(int fd, const void *puffer, size_t n);

int isSocketError(int sockfd);

int cread(int fd, char *buf, int n);
int cwrite(int fd, char *buf, int n);
int read_n(int fd, char *buf, int n);
