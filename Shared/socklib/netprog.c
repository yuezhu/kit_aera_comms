#include  <ctype.h>
#include  "netprog.h"

/*--------------------------------------------------------------- connecttimeout */
static void connectsignal(int signr) { return; /* nur connect() unterbrechen */ }

int
connecttimeout(int sockfd, const struct sockaddr *sa, socklen_t len, int sek)
{
   sigfunc *sighandler;
   int     n;

   sighandler = ADW_signal(SIGALRM, connectsignal); /* Signalhandler installieren */
   if (alarm(sek) != 0)
      error_mess(WARNUNG, "Timeout schon eingerichtet");

   if ( (n = connect(sockfd, (struct sockaddr *) sa, len)) < 0) {
      close(sockfd);
      if (errno == EINTR)
         errno = ETIMEDOUT;
   }
   alarm(0);  /* Alarm wieder ausschalten */
   ADW_signal(SIGALRM, sighandler);  /* vorherigen Signalhandler wieder einrichten */

   return n;
}

/*------------------------------------------------------------------------ mkadr */
int
mkadr(char *adr_str, char *protocol, void *sa, socklen_t *salen)
{
   char  *tmp_addr = strdup(adr_str); /* String 'adr_str' duplizieren */
   char  *host     = strtok(tmp_addr, ":");
   char  *port     = strtok(NULL, "\n");
   struct sockaddr_in *sin = (struct sockaddr_in *)sa;
   struct hostent     *hname = NULL;
   struct servent     *sname = NULL;
   char   *rest;
   long   portnr;

   /*... Voreinstellungen evtl. setzen */
   if (host == NULL) host = "*";
   if (port == NULL) port = "*";
   if (protocol == NULL) protocol  = "udp";

   /*... Initialisieren der Adre�strukturen */
   memset(sin, 0, *salen);
   sin->sin_family = AF_INET;
   sin->sin_port   = 0;
   sin->sin_addr.s_addr = htons(INADDR_ANY);

   /*... Host-Adresse f�llen */
   if (isdigit(*host)) { /*... Numerische Adresse */
      if (inet_pton(AF_INET, host, &sin->sin_addr) <= 0)
         return -1;

   } else if (strcmp(host, "*")) { /*... Symbolische Adresse (nicht '*') */
      if ( (hname = gethostbyname(host)) == NULL || hname->h_addrtype != AF_INET)
         return -1;
      sin->sin_addr = *(struct in_addr *)hname->h_addr_list[0];
   }

   /*... Portnummer festlegen */
   if (isdigit(*port)) { /*... Numerische Portnummer */
      portnr = strtol(port, &rest, 10);
      if ( (rest != NULL && *rest) || (portnr < 0L || portnr >= 32768))
         return -2;
      sin->sin_port = htons((short)portnr);
   } else if (strcmp(port,"*")) { /*... Symbolische Portnr. (nicht '*') */
      if ( (sname = getservbyname(port, protocol)) == NULL)
         return -2;
      sin->sin_port = (short)sname->s_port;
   }
   *salen = sizeof(*sin);

   free(tmp_addr);
   return 0;
}

/*------------------------------------------------------------ ADW_...-Funktionen */
int
ADW_accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen)
{
   int  n;

   while ( (n = accept(sockfd, addr, addrlen)) < 0)
      if (errno != EPROTO && errno != ECONNABORTED)
         error_mess(FATAL_SYS, "accept-Fehler");
   return n;
}

void
ADW_bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
   if (bind(sockfd, addr, addrlen) < 0)
      error_mess(FATAL_SYS, "bind-Fehler");
}

void
ADW_close(int fd)
{
   if (close(fd) == -1)
      error_mess(FATAL_SYS, "close-Fehler");
}

void
ADW_connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
   if (connect(sockfd, addr, addrlen) < 0)
      error_mess(FATAL_SYS, "connect-Fehler");
}

void
ADW_getpeername(int sockfd, struct sockaddr *addr, socklen_t *addrlen)
{
   if (getpeername(sockfd, addr, addrlen) < 0)
      error_mess(FATAL_SYS, "getpeername-Fehler");
}

void
ADW_getsockname(int sockfd, struct sockaddr *addr, socklen_t *addrlen)
{
   if (getsockname(sockfd, addr, addrlen) < 0)
      error_mess(FATAL_SYS, "getsockname-Fehler");
}

const char *
ADW_inet_ntop(int family, const void *str, char *nw, size_t len)
{
   const char *string;

   if (nw == NULL)
      error_mess(FATAL, "NULL als 3. Argument bei inet_ntop");
   if ( (string = inet_ntop(family, str, nw, len)) == NULL)
      error_mess(FATAL_SYS, "inet_ntop-Fehler");
   return string;
}

void
ADW_inet_pton(int family, const char *str, void *nw)
{
   int  n;

   if ( (n = inet_pton(family, str, nw)) < 0)
      error_mess(FATAL_SYS, "inet_pton-Fehler f�r '%s'", str);
   else if (n == 0)
      error_mess(FATAL, "inet_pton-Fehler f�r '%s'", str);
}

void
ADW_listen(int sockfd, int backlog)
{
   if (listen(sockfd, backlog) < 0)
      error_mess(FATAL_SYS, "listen-Fehler");
}

int
ADW_poll(struct pollfd *fdarray, unsigned int nfds, int timeout)
{
   int  n;

   if ( (n = poll(fdarray, nfds, timeout)) < 0)
      error_mess(FATAL_SYS, "poll-Fehler");

   return n;
}

ssize_t
ADW_read(int fd, void *puff, size_t count, const char *meld)
{
   int  n;
   if ( (n = read(fd, puff, count)) < 0 )
      error_mess(FATAL_SYS, "read-Fehler %s", meld);
   return n;
}

ssize_t
ADW_recv(int sockfd, void *puff, size_t n, int flags)
{
   ssize_t  s;

   if ( (s = recv(sockfd, puff, n, flags)) < 0)
      error_mess(FATAL_SYS, "recv-Fehler");
   return s;
}

ssize_t
ADW_recvfrom(int sockfd, void *puff, size_t len, int flags,
            struct sockaddr *from, socklen_t *fromlen)
{
   ssize_t  n;

   if ( (n = recvfrom(sockfd, puff, len, flags, from, fromlen)) < 0)
      error_mess(FATAL_SYS, "recvfrom-Fehler");
   return n;
}

int
ADW_select(int maxfd, fd_set *lesefds, fd_set *schreibfds, fd_set *exceptfds,
          struct timeval *timeout)
{
   int  n;

   if ( (n = select(maxfd, lesefds, schreibfds, exceptfds, timeout)) < 0)
      error_mess(FATAL_SYS, "select-Fehler");
   return n;
}

void
ADW_send(int sockfd, const void *puff, ssize_t n, int flags)
{
   if (send(sockfd, puff, n, flags) != n)
      error_mess(FATAL_SYS, "send-Fehler");
}

void
ADW_sendto(int sockfd, const void *puff, size_t len, int flags,
          const struct sockaddr *to, socklen_t tolen)
{
   if ((size_t)sendto(sockfd, puff, len, flags, to, tolen) != len)
      error_mess(FATAL_SYS, "sendto-Fehler");
}

void
ADW_shutdown(int fd, int wie)
{
   if (shutdown(fd, wie) < 0)
      error_mess(FATAL_SYS, "shutdown-Fehler");
}

sigfunc *
ADW_signal(int signr, sigfunc *func)
{
   struct sigaction  act, oldact;

   act.sa_handler = func;
   sigemptyset(&act.sa_mask);
   act.sa_flags = 0;
#ifdef  SA_RESTART
   if (signr == SIGALRM)
      act.sa_flags |= SA_RESTART; /* unterbrochenen Systemaufruf
                                     automatisch wieder starten */
#endif
   if (sigaction(signr, &act, &oldact) < 0)
      error_mess(FATAL_SYS, "sigaction-Fehler");

   return oldact.sa_handler;
}

int
ADW_socket(int family, int type, int protocol)
{
   int  n;

   if ( (n = socket(family, type, protocol)) < 0)
       error_mess(FATAL_SYS, "socket-Fehler");
   return n;
}

void
ADW_socketpair(int domain, int typ, int protocol, int sv[2])
{
   if (socketpair(domain, typ, protocol, sv) == -1)
      error_mess(FATAL_SYS, "socketpair-Fehler");
}

char *
ADW_sock_ntop(const struct sockaddr *sa, socklen_t salen)
{
   char        portnr[7];
   static char str[128];

   switch (sa->sa_family) {
      case AF_INET: {
         struct sockaddr_in *sin = (struct sockaddr_in *)sa;

         if (inet_ntop(AF_INET, &sin->sin_addr, str, sizeof(str)) == NULL)
            error_mess(FATAL_SYS, "addr_ntop-Fehler");
         if (ntohs(sin->sin_port) != 0) {
            snprintf(portnr, sizeof(portnr), "/%d", ntohs(sin->sin_port));
            strcat(str, portnr);
         }
         return str;
      }

#ifdef  IPV6
      case AF_INET6: {
         struct sockaddr_in6 *sin6 = (struct sockaddr_in6 *)sa;

         if (inet_ntop(AF_INET6, &sin6->sin6_addr, str, sizeof(str)) == NULL)
            error_mess(FATAL_SYS, "addr_ntop-Fehler");
         if (ntohs(sin6->sin6_port) != 0) {
            snprintf(portnr, sizeof(portnr), "/%d", ntohs(sin6->sin6_port));
            strcat(str, portnr);
         }
         return str;
      }
#endif

#ifdef  AF_UNIX
      case AF_UNIX: {
         struct sockaddr_un *unp = (struct sockaddr_un *)sa;

         if (unp->sun_path[0] == 0)
            strcpy(str, "(kein Pfadname zugeordnet)");
         else
            snprintf(str, sizeof(str), "%s", unp->sun_path);
         return str;
      }
#endif

      default:
         snprintf(str, sizeof(str), "ADW_sock_ntop: AF_xxx unbekannt: %d, len %d",
                                    sa->sa_family, salen);
         return str;
   }
   error_mess(FATAL_SYS, "ADW_sock_ntop-Fehler");
   return NULL;
}

int
ADW_tcp_connect(const char *host, const char *service)
{
   int              sock, n;
   struct addrinfo  hints, *r, *alt_result;

   memset(&hints, 0, sizeof(struct addrinfo));
   hints.ai_family   = AF_UNSPEC;
   hints.ai_socktype = SOCK_STREAM;

   if ( (n = getaddrinfo(host, service, &hints, &r)) != 0)
      error_mess(FATAL, "ADW_tcp_connect-Fehler f�r %s (%s): %s",
                         host, service, gai_strerror(n));
   alt_result = r;

   do { /*... Liste durchlaufen, und f�r Eintr�ge Verbindungsaufbau versuchen */
      if ( (sock = socket(r->ai_family, r->ai_socktype, r->ai_protocol)) < 0)
         continue; /* n�chsten Eintrag versuchen */

      if (connect(sock, r->ai_addr, r->ai_addrlen) == 0)
         break;    /* erfolgreich verbunden --> Durchlaufen der Liste beenden */

      ADW_close(sock);  /* keine Verbindung -> Socket wieder schliessen */
   } while ( (r = r->ai_next) != NULL);

   if (r == NULL) /* Ende der Liste ohne erfolgreiche Verbindung erreicht */
      error_mess(FATAL_SYS, "ADW_tcp_connect-Fehler f�r %s (%s)", host, service);

   freeaddrinfo(alt_result);

   return sock;
}

int
ADW_tcp_listen(const char *host, const char *service, socklen_t *addrlen)
{
   int              horchfd, n;
   const int        an = 1;
   struct addrinfo  hints, *r, *alt_result;

   memset(&hints, 0, sizeof(struct addrinfo));
   hints.ai_flags    = AI_PASSIVE;
   hints.ai_family   = AF_UNSPEC;
   hints.ai_socktype = SOCK_STREAM;

   if ( (n = getaddrinfo(host, service, &hints, &r)) != 0)
      error_mess(FATAL, "ADW_tcp_listen-Fehler f�r %s (%s): %s",
                         host, service, gai_strerror(n));
   alt_result = r;

   do { /*... Liste durchlaufen, und f�r Eintr�ge Verbindungsaufbau versuchen */
      if ( (horchfd = socket(r->ai_family, r->ai_socktype, r->ai_protocol)) < 0)
         continue; /* n�chsten Eintrag versuchen */

      if (setsockopt(horchfd, SOL_SOCKET, SO_REUSEADDR, &an, sizeof(an)) < 0)
         error_mess(FATAL_SYS, "setsockopt-Fehler");

      if (bind(horchfd, r->ai_addr, r->ai_addrlen) == 0)
         break;    /* erfolgreich verbunden --> Durchlaufen der Liste beenden */

      ADW_close(horchfd);  /* keine Verbindung -> Socket wieder schliessen */
   } while ( (r = r->ai_next) != NULL);

   if (r == NULL) /* Ende der Liste ohne erfolgreiche Verbindung erreicht */
      error_mess(FATAL_SYS, "ADW_tcp_listen-Fehler f�r %s (%s)", host, service);
      
   ADW_listen(horchfd, 1024);

   if (addrlen != NULL)
      *addrlen = r->ai_addrlen; /* R�ckgabewert: Gr��e der Protokolladr. */

   freeaddrinfo(alt_result);

   return horchfd;
}

int
ADW_udp_client(const char *host, const char *service,
              struct sockaddr **sa, socklen_t *len)
{
   int              sock, n;
   struct addrinfo  hints, *r, *alt_result;

   memset(&hints, 0, sizeof(struct addrinfo));
   hints.ai_family   = AF_UNSPEC;
   hints.ai_socktype = SOCK_DGRAM;

   if ( (n = getaddrinfo(host, service, &hints, &r)) != 0)
      error_mess(FATAL, "ADW_udp_client-Fehler f�r %s (%s): %s",
                         host, service, gai_strerror(n));
   alt_result = r;

   do {
      sock = socket(r->ai_family, r->ai_socktype, r->ai_protocol);
   } while (sock < 0 && (r = r->ai_next) != NULL);

   if (r == NULL) /* Ende der Liste ohne erfolgreiches Anlegen eines Sockets */
      error_mess(FATAL_SYS, "ADW_udp_client-Fehler f�r %s (%s)", host, service);

   if ( (*sa = malloc(*len = r->ai_addrlen)) == NULL)
      error_mess(FATAL_SYS, "Speicherplatzmangel");
   memcpy(*sa, r->ai_addr, r->ai_addrlen);

   freeaddrinfo(alt_result);

   return sock;
}

int
ADW_udp_connect(const char *host, const char *service)
{
   int              sock, n;
   struct addrinfo  hints, *r, *alt_result;

   memset(&hints, 0, sizeof(struct addrinfo));
   hints.ai_family   = AF_UNSPEC;
   hints.ai_socktype = SOCK_DGRAM;

   if ( (n = getaddrinfo(host, service, &hints, &r)) != 0)
      error_mess(FATAL, "ADW_udp_connect-Fehler f�r %s (%s): %s",
                         host, service, gai_strerror(n));
   alt_result = r;

   do { /*... Liste durchlaufen, und f�r Eintr�ge Verbindungsaufbau versuchen */
      if ( (sock = socket(r->ai_family, r->ai_socktype, r->ai_protocol)) < 0)
         continue; /* n�chsten Eintrag versuchen */

      if (connect(sock, r->ai_addr, r->ai_addrlen) == 0)
         break;    /* erfolgreich verbunden --> Durchlaufen der Liste beenden */

      ADW_close(sock);  /* keine Verbindung -> Socket wieder schliessen */
   } while ( (r = r->ai_next) != NULL);

   if (r == NULL) /* Ende der Liste ohne erfolgreiches Anlegen eines Sockets */
      error_mess(FATAL_SYS, "ADW_udp_connect-Fehler f�r %s (%s)", host, service);

   freeaddrinfo(alt_result);

   return sock;
}

int
ADW_udp_server(const char *host, const char *service, socklen_t *addrlen)
{
   int              sock, n;
   struct addrinfo  hints, *r, *alt_result;

   memset(&hints, 0, sizeof(struct addrinfo));
   hints.ai_flags    = AI_PASSIVE;
   hints.ai_family   = AF_UNSPEC;
   hints.ai_socktype = SOCK_DGRAM;

   if ( (n = getaddrinfo(host, service, &hints, &r)) != 0)
      error_mess(FATAL, "ADW_udp_server-Fehler f�r %s (%s): %s",
                         host, service, gai_strerror(n));
   alt_result = r;

   do { /*... Liste durchlaufen, und f�r Eintr�ge Verbindungsaufbau versuchen */
      if ( (sock = socket(r->ai_family, r->ai_socktype, r->ai_protocol)) < 0)
         continue; /* n�chsten Eintrag versuchen */

      if (bind(sock, r->ai_addr, r->ai_addrlen) == 0)
         break;    /* erfolgreich verbunden --> Durchlaufen der Liste beenden */

      ADW_close(sock);  /* keine Verbindung -> Socket wieder schliessen */
   } while ( (r = r->ai_next) != NULL);

   if (r == NULL) /* Ende der Liste ohne erfolgreiches Anlegen eines Sockets */
      error_mess(FATAL_SYS, "ADW_udp_server-Fehler f�r %s (%s)", host, service);

   if (addrlen != NULL)
      *addrlen = r->ai_addrlen;  /* R�ckgabewert: Gr��e der Protokolladr. */

   freeaddrinfo(alt_result);

   return sock;
}

void
ADW_write(int fd, const void *puff, size_t count, const char *meld)
{
   if (write(fd, puff, count) < 0)
      error_mess(FATAL_SYS, "write-Fehler %s", meld);
}

/*--------------------------------------------- lies_ein_puffer_zeichen (static) */
static ssize_t
lies_ein_puffer_zeichen(int fd, char *zgr)
{
   static int    noch_im_puffer = 0;
   static char   lese_puffer[MAX_ZEICHEN];
   static char  *lese_zgr;

   if (noch_im_puffer <= 0) {
      while ( (noch_im_puffer = read(fd, lese_puffer, sizeof(lese_puffer))) < 0)
         if (errno != EINTR)
            return -1;

      if (noch_im_puffer == 0)
         return 0;

      lese_zgr = lese_puffer;
   }
   noch_im_puffer--;
   *zgr = *lese_zgr++;
   return 1;
}

/*---------------------------------------------------------------- readline_sock */
ssize_t
readline_sock(int fd, void *puffer, size_t maxlen)
{
   int    i, n;
   char   zeich, *zgr = puffer;

   for (i=1; i < (int)maxlen; i++) {
      if ( (n = lies_ein_puffer_zeichen(fd, &zeich)) == 1) {
         *zgr++ = zeich;
         if (zeich == '\n')
            break;   /* Zeilenende */
      } else if (n == 0) {
         if (i == 1)
            return 0; /* EOF gleich am Anfang (keine Daten) */
         else
            break;    /* EOF; es wurden Daten gelesen */
      } else
         error_mess(FATAL_SYS, "readline-Fehler");
   }
   *zgr = '\0';  /* 0 terminieren */
   return i;
}

/*-------------------------------------------------------------------- read_sock */
ssize_t
read_sock(int fd, void *puffer, size_t n)
{
   size_t   verbleiben = n;
   ssize_t  gelesen;
   char    *zgr = puffer;

   while (verbleiben > 0) { /* liest n Bytes von fd */
      if ( (gelesen = read(fd, zgr, verbleiben)) < 0) {
         if (errno == EINTR)
            gelesen = 0;  /* neuer read-Versuch */
         else
            error_mess(FATAL_SYS, "read_sock-Fehler");
      } else if (gelesen == 0)
         break; /* EOF */
      verbleiben -= gelesen;
      zgr        += gelesen;
   }
   return n-verbleiben;
}

/*---------------------------------------------------------------- selecttimeout */
int
selecttimeout(int fd, int sek)
{
   fd_set          lese_menge;
   struct timeval  tv;

   FD_ZERO(&lese_menge);
   FD_SET(fd, &lese_menge);

   tv.tv_sec  = sek;
   tv.tv_usec = 0;

   return select(fd+1, &lese_menge, NULL, NULL, &tv);
}
/*---------------------------------------------------------------- sockfd_family */
int
sockfd_family(int sockfd)
{
   union {
      struct sockaddr sa;                 /* MAXSOCKADDR = maximal m�gliche   */
      char            daten[MAXSOCKADDR]; /*    Socket-Adre�struktur in Bytes */
   } un;
   socklen_t  len = MAXSOCKADDR;

   if (getsockname(sockfd, (struct sockaddr *)un.daten, &len) < 0)
      return -1;
   return un.sa.sa_family;
}

/*------------------------------------------------------------------- write_sock */
ssize_t
write_sock(int fd, const void *puffer, size_t n)
{
   size_t      verbleiben = n;
   ssize_t     geschrieben;
   const char *zgr = puffer;

   while (verbleiben > 0) { /* schreibt n Bytes nach fd */
      if ( (geschrieben = write(fd, zgr, verbleiben)) <= 0) {
         if (errno == EINTR)
            geschrieben = 0;  /* neuer write-Versuch */
         else
            error_mess(FATAL_SYS, "write_sock-Fehler");
      }
      verbleiben -= geschrieben;
      zgr        += geschrieben;
   }
   return n;
}

/**************************************************************************
 * cread: read routine that checks for errors and exits if an error is    *
 *        returned.                                                       *
 **************************************************************************/
int cread(int fd, char *buf, int n){

  int nread;

  if((nread=read(fd, buf, n)) < 0){
    perror("Reading data");
    return nread;
  }
  return nread;
}

/**************************************************************************
 * cwrite: write routine that checks for errors and exits if an error is  *
 *         returned.                                                      *
 **************************************************************************/
int cwrite(int fd, char *buf, int n){

  int nwrite;

  if((nwrite=write(fd, buf, n)) < 0){
    perror("Writing data");
    return nwrite;
  }
  return nwrite;
}

/**************************************************************************
 * read_n: ensures we read exactly n bytes, and puts them into "buf".     *
 *         (unless EOF, of course)                                        *
 **************************************************************************/
int read_n(int fd, char *buf, int n) {

  int nread, left = n;

  while(left > 0) {
    if ((nread = cread(fd, buf, left)) == 0){
      return 0;
    } else if (nread < 0) { //read error
    	perror("read_n ceases");
    	return -1;
    } else {
      left -= nread;
      buf += nread;
    }
  }
  return n;
}

int isSocketError(int sockfd) {
	if (sockfd == 0) return 0;

	int sockerr = errno;

//	perror("isSocketError");
//	printf("%s:%s - have errno %d \n", ROLE, __FUNCTION__, sockerr);

	switch (sockerr) {
		case EBADF:
		case ENETDOWN:
		case EPIPE:
		case ECONNREFUSED:
		case EAGAIN:
		case ENOTCONN:
			return 1;
			break;
		default:
			break;
	}
//   if (sockerr > 0) return 1;		// do not consider all errno here, but only network errors
    return sockerr;
}


