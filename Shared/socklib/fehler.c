#include  <errno.h>
#include  <stdarg.h>
#include  <syslog.h>
#include  "eighdr.h"

int  debug; /* Aufrufer von log_meld oder log_open muss debug setzen:
               0, wenn interaktiv; 1, wenn Daemon-Prozess */

/*---- Lokale Routinen zur Abarbeitung der Argumentliste --------------------*/
static void  error_message(int sys_meld, const char *fmt, va_list az)
{
   int  fehler_nr = errno;
   char puffer[MAX_ZEICHEN];

   vsprintf(puffer, fmt, az);
   if (sys_meld)
      sprintf(puffer+strlen(puffer), ": %s ", strerror(fehler_nr));
   fflush(stdout);   /* fuer Fall, dass stdout und stderr gleich sind */
   fprintf(stderr, "%s\n", puffer);
   fflush(NULL);  /* alle Ausgabepuffer flushen */
   return;
}

static void  log_message(int sys_meld, int prio, const char *fmt, va_list az)
{
   int  fehler_nr = errno;
   char puffer[MAX_ZEICHEN];

   vsprintf(puffer, fmt, az);
   if (sys_meld)
      sprintf(puffer+strlen(puffer), ": %s ", strerror(fehler_nr));
   if (debug==0) {
      fflush(stdout);   /* fuer Fall, dass stdout und stderr gleich sind */
      fprintf(stderr, "%s\n", puffer);
      fflush(NULL);  /* alle Ausgabepuffer flushen */
   } else {
      strcat(puffer, "\n");
      syslog(prio, puffer);
   }
   return;
}

/*---- Global aufrufbare Fehlerroutinen -------------------------------------*/
void  error_mess(int kennung, const char *fmt, ...)
{
   va_list     az;

   va_start(az, fmt);
   switch (kennung) {
      case WARNUNG:
    	  error_message(1, fmt, az);
    	  break;
      case FATAL:
              error_message(0, fmt, az);
              break;
      case WARNUNG_SYS:
      case FATAL_SYS:
      case DUMP:
              error_message(1, fmt, az);
              break;
       default:
              error_message(1, "Falscher Aufruf von fehler_meld...", az);
              printf("exiting now!\n");
              exit(3);
   }
   va_end(az);

   if (kennung==WARNUNG || kennung==WARNUNG_SYS)
      return;
   else if (kennung==DUMP)
      abort();  /* core dump */
   printf("exiting now!\n");
   exit(1);
}

void  log_mess(int kennung, const char *fmt, ...)
{
   va_list     az;

   va_start(az, fmt);
   switch (kennung) {
      case WARNUNG:
      case FATAL:
              log_message(0, LOG_WARNING, fmt, az);
              break;
      case WARNUNG_SYS:
      case FATAL_SYS:
              log_message(1, LOG_WARNING, fmt, az);
              break;
       default:
              log_message(1, LOG_WARNING, "Falscher Aufruf von fehler_meld...", az);
              exit(3);
   }
   va_end(az);

   if (kennung==WARNUNG || kennung==WARNUNG_SYS)
      return;
   printf("exiting now!\n");
   exit(2);
}

/*---- log_open ---------------------------------------------------------------
           initialisiert syslog() bei einem Daemon-Prozess                   */
void  log_open(const char *kennung, int option, int facility)
{
   if (debug)
      openlog(kennung, option, facility);
}
