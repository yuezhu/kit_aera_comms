/***************************************************/
/* Prototypes for opening sockets (INET) and
 * reading/writing messages.
 */
/***************************************************/

/*
 * History:
 * 18.02.2013: Using version. Implemented AERA transport layer in functions ReadSockMsgTO, ReadSockMsgSM_TO, WriteSockMsg. Can be switched with USE_AERA_TRANSPORT_DEFINITIONS
 * 19.01.2012: Fixed bug in socklib.h, which resulted in fragmented data blocks for long messages
 */


#ifndef _SOCKLIB_H
#define _SOCKLIB_H

#include <stdint.h>          	// AW: for int and other types with explicit sizes in bits
#include <stdlib.h>				// malloc() defined here
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
//#include "def.h"
#include <netdb.h>
#include <stddef.h>

#define SOCK_LIB_VERSION					2.3
#define SOCK_LIB_MAJOR_VERSION		2
#define SOCK_LIB_MINOR_VERSION		3
#define SOCK_LIB_VERSION_STRING	"2.3"


#define MIN(a,b)   ((a) < (b) ? (a) : (b))
#define MAX(a,b)   ((a) > (b) ? (a) : (b))

// read / write messages
//#define CLUSTER_TYPE int
#define MSGTYPE_LENGTH	uint16_t
#define MSG_DATA_TYPE	uint16_t
#define TAGTYPE_LENGTH 	uint16_t
#define TAGTYPE_TAGNO 	uint16_t

#define READ_TIMEOUT_US		1000		// timeout for select before each(!) read call, if message is not yet complete. On timeout, NULL is returned!
#define WRITE_TIMEOUT_US	50000		// timeout for select before each(!) write call, if message is not yet complete. On timeout, NULL is returned!

#define RCVTIMEOUT_US		1000		// timeout set on socket before connection, to avoid blocking read/recv and calling select before
#define SNDTIMEOUT_US		1000		// timeout set on socket before connection, to avoid blocking write/send and calling select before


#define READ_LOOP_LIMIT		20		// how many times reading is tried (if read returns zero)
#define WRITE_LOOP_LIMIT	20		// how many times writing is tried (if write returns zero)

#define MSG_RCVBUFF_LENGTH			132000		// size in bytes of buffer, where incoming messages are buffered to avoid malloc()

#define MAXBUFF_UNIX 32768			// maximum socket buffer sizes for sockets in Unix/Linux? Use it at first like this

typedef struct {
	TAGTYPE_LENGTH length;			// length of Msg in bytes without(!) sizeof(Msg->length)!
	char data[1];
} Msg;

#define TotalMsgLength(l) (l+offsetof(Msg,data))
#define MsgDataLength(l)  (l-offsetof(Msg,data))


//#ifdef _cplusplus // if compiled with C++ compiler.  Note that this is not
// standard, check your compiler's docs and other header files to find out
// the correct macro it uses for c++ versus C compilations.
//extern "C" {
//#endif
//
// put C function prototypes here
	// access version information
	const float getSocklibVersion();
	const int getSocklibMajorVersion();
	const int getSocklibMinorVersion();
	const char *getSocklibVersionString();

	// open sockets
	int waitForWriteOk(int, int);
	int OpenInetServer(int port);
	int OpenInetProtoServer(int, struct sockaddr **);
	int SearchForPortNumber(struct sockaddr_in *inetsockaddr,int nSocket,char *ServerName);
	int OpenInetClient(int port,char *ServerName);


	int SetSocketSndTimeout(int Socket, int time);
	int SetSocketRcvTimeout(int Socket, int time);

	int WriteSockMsg(int,Msg *);
	void SendStateSock(TAGTYPE_TAGNO,int);
	char *prpos(char *,int);
//	Msg * ReadSockMsg ( int Socket );
//	Msg * ReadSockMsgSM ( int Socket );
	Msg * ReadSockMsgTO ( int Socket,int time );
	Msg * ReadSockMsgSM_TO ( int Socket,int time  );
//	Msg * ReadSockMsgEM_TO ( int Socket, int time, char* mp );

//	Msg * RecvSockMsgSM ( int sock );

	int WriteSockMsg ( int Socket, Msg *sendmsg );

	int TestRSocket(int Socket);
	void TextPrint(int *logSocket,char *fmt, ...);
	char *JDtoString(time_t);
	int TestSocketForWrite(int);
	int TestSocketForRead(int sock);
	Msg *ReadSockMsgWithTextFilter(int ls);
// other function prototypes here


//#ifdef _cplusplus // if compiled with C++ compiler
//} // end of extern "C" block
//#endif





#endif		// #ifndef _SOCKLIB_H


