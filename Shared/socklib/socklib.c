/*********************************************************/
/* Functions for opening sockets, and read and write */

/* History:
 * - AW, 16.11.2012: added aera daq transport definitions: this mainly limits the size of a Msg and requests the chars A,E,R,A as a Msg-terminator
 *                   does the definition implement, that the library tries to find a terminator in stream on problems?
 * - used in lopes / aera and modified by A. Weindl
 * - originally used in KASCADE DAQ, created by H. Matthes and/or T. Wiegert?
 */

/*********************************************************/

#ifndef _SOCKLIB_H
#include "socklib.h"
#endif

// TODO: create "initSocklib()" or make some parameters adjustable
// TODO: select with timeout = NULL means "no timeout" = wait forever. timeout.sec=0 and timeout.usec = 0 means: return immediately- cause functions should not introduce such things themselves. May infer with nonblocking sockets. May be set in init()??
// TODO: does an interrupted read raise E_INT...?
// TODO: clean up use of name sock and/or socket

#include <unistd.h>		// read / write
#include <time.h>		// localtime
#include <stdio.h>		/* AW: added for debug */
#include <arpa/inet.h>	// inet_addr()
#include <limits.h>		// for SHRT_MAX
#include <sys/time.h>	// for timezone

#define USE_AERA_TRANSPORT_DEFINITIONS
#define AERA_MAGIC 0x65698265
char AERA_MAGIC_CHARS[] = {"AERA"};
#define COPY_MSG_ON_WRITE	// for WriteSockMsg in USE_AERA_TRANSPORT_DEFINITIONS mode. Cause the Msg->length has to be changed due to adding AERA_MAGIC, the Msg is copied before
#ifdef COPY_MSG_ON_WRITE
char msgWriteBuff[MSG_RCVBUFF_LENGTH];
#endif

char msgRcvBuff[MSG_RCVBUFF_LENGTH];

#define SOCKET_SEND_TIMEOUT	20000
int socketSndTimeoutInUse = 0;			// = 0 for not in use, = 1 for in use!!
#define SOCKET_RECV_TIMEOUT	20000
int socketRcvTimeoutInUse = 0;			// = 0 for not in use, = 1 for in use!!

static int printedAeraTransportDefined = 0;

char msg[256];

// access version information
const float getSocklibVersion() {
	return SOCK_LIB_VERSION;
}
const int getSocklibMajorVersion() {
	return SOCK_LIB_MAJOR_VERSION;
}
const int getSocklibMinorVersion() {
	return SOCK_LIB_MINOR_VERSION;
}
const char *getSocklibVersionString() {
	return SOCK_LIB_VERSION_STRING;
}

char *getSecondMicrosecondNow() {
	static char actTime[100];
	static char* actTimeP = (char*)actTime;
	struct timeval tv;
	struct timezone tz;
	gettimeofday(&tv, &tz);
//	struct tm *tm;
//	tm=localtime(&tv.tv_sec);
//	printf(" %d:%02d:%02d %d \n", tm->tm_hour, tm->tm_min, tm->tm_sec, tv.tv_usec);
//	return tv.tv_usec;
	sprintf(actTime, "%02d:%d", tv.tv_sec, (int)tv.tv_usec);
//	printf("%s - have actual time: %s\n", __FUNCTION__, actTime);
	return (char*)&actTime;
}

// print the actual second and microsecond
void printSecondMicrosecondNow() {
//	struct tm *tm;
//	tm=localtime(&tv.tv_sec);
//	printf(" %d:%02d:%02d %d \n", tm->tm_hour, tm->tm_min, tm->tm_sec, tv.tv_usec);
//	return tv.tv_usec;
//	printf(" %02d %d \n", tv.tv_sec, tv.tv_usec);
	printf("%s\n", getSecondMicrosecondNow());
}

/**
 * @brief clean up the internal static memory used in ReadSockMsgSM() and ReadSockMsgSM_TO() - used internally!
 */
void cleanMsgRcvBuff() {
	memset((void*)&msgRcvBuff, 0, MSG_RCVBUFF_LENGTH);
}

/**
 * @brief helper / debug function : dump out the content of some memory space
 * @param memp pointer to the memory to dump
 * @param intCount count of integers in memory that should be dumped
 * @return returns a string with the dumped memory
 */
#define UINT16		unsigned short int			// to be independent of other headers
void dumpMemUINT16 (UINT16* memp, int intCount) {
	int i;
	if (memp == NULL) {
		printf("dumpMem() received NULL pointer to memory ...\n");
		return;
	}
	if (intCount == 0) {
		printf("dumpMem() should dump 0 ints - doing nothing ...\n");
		return;
	}
	UINT16 *uip2 = memp;
	printf("Dumping memory of size %d times sizeof(UINT16) = %d ...\n", intCount, sizeof(UINT16));
	for (i=0;i< intCount;i++, uip2++) {
		printf("no %05d:  hex : 0x%08X dec : %d \n", i, *uip2, *uip2);
	}
	printf("\n");
	return;
}

int SetSocketRcvTimeout(int sock_fd, int time) {
	struct timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = time;
	// this tells the socket not to block on read/recv, but to return with timeout = -1
	if (setsockopt(sock_fd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout,  sizeof timeout)) {
		perror("SetSocketRcvTimeout - setsockopt");
		return -1;
	}
	socketRcvTimeoutInUse = 1;
	return 0;
}

int SetSocketSndTimeout(int sock_fd, int time) {
	struct timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = time;
	// this tells the socket not to block on write/send, but to return with timeout = -1
	if (setsockopt(sock_fd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,  sizeof timeout)) {
		perror("SetSocketSndTimeout - setsockopt");
		return -1;
	}
	socketSndTimeoutInUse = 1;
	return 0;
}

/**
 * @brief opens a listening server proto socket on the given port
 * @param port to open for listening
 * @param SocketAddress
 * @return returns 0 on errors and the sock_fd of the proto socket otherwise
 */
int OpenInetProtoServer(int port, struct sockaddr **SocketAddress)		/* AW: Opening internet socket with port number and empty socket address. Proto means no accept called!!! */
/* returns Prototype, you have to accept, if a client will connect */
{
	unsigned int nSocketSize;
  	int nSocket,nRetVal, on;
        struct sockaddr_in *InetSocketAddress = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));

   	*SocketAddress = (struct sockaddr *)InetSocketAddress;

    printf("OpenInetProtoServer(): Using port: %d\n", port);								/* AW: added for debug */

        nSocket = socket(AF_INET,SOCK_STREAM,0);
        printf("OpenInetProtoServer(): Return of nSocket: %d\n", nSocket);					/* AW: added for debug. Returns descriptor (int) of new socket */

        if (nSocket < 0) return 0;

        on = 1;
        nRetVal = setsockopt( nSocket, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on) );

        nSocketSize = sizeof(struct sockaddr_in);

        memset(InetSocketAddress,0,nSocketSize);

       	InetSocketAddress->sin_family = AF_INET;
       	InetSocketAddress->sin_addr.s_addr = htonl(INADDR_ANY);
       	InetSocketAddress->sin_port = htons(port);
/*
		if (SetSocketRcvTimeout(nSocket, SOCKET_RECV_TIMEOUT) < 0) {
			printf("SetSocketRcvTimeout() for port %d failed!\n", port);
		} else  printf("SetSocketRcvTimeout() set SOCKET_RECV_TIMEOUT to %d micro seconds\n", SOCKET_RECV_TIMEOUT);

		if (SetSocketSndTimeout(nSocket, SOCKET_SEND_TIMEOUT) < 0) {
			printf("SetSocketSndTimeout() for port %d failed!\n", port);
		} else  printf("SetSocketSndTimeout() set SOCKET_SEND_TIMEOUT to %d micro seconds\n", SOCKET_SEND_TIMEOUT);
*/
        nRetVal = bind(nSocket,*SocketAddress,nSocketSize);
        printf("OpenInetProtoServer(): Return of bind(socket): %d. (=0 is ok)\n", nRetVal);	/* AW: added for debug. Return =0 means no error */

      	if (nRetVal < 0) {
            	return 0;
       	}

        nRetVal = listen(nSocket,5);
        printf("OpenInetProtoServer(): Return of listen(socket): %d. (=0 is ok)\n", nRetVal);			/* AW: added for debug */

    	if (nRetVal < 0) {
            	return 0;
       	}

       	return nSocket; /* return Prototype */
}

/**
 * @brief opens a listening server proto socket on the given port and calls accept afterwards
 * @param port to open for listening
 * @return returns 0 on errors and the sock_fd of the connected socket otherwise
 */
int OpenInetServer(int port)
{
	unsigned int nSocketSize;
  	int nSocket,nRetVal;
        struct sockaddr_in InetSocketAddress;
   	struct sockaddr *SocketAddress = (struct sockaddr *)&InetSocketAddress;

   	printf("Using port: %d\n", port);								/* AW: added for debug */

	nSocket = socket(AF_INET,SOCK_STREAM,0);
	printf("OpenInetServer(): Return of nSocket: %d\n", nSocket);					/* AW: added for debug */

	if (nSocket < 0) return 0;

	nSocketSize = sizeof(struct sockaddr_in);

	memset(&InetSocketAddress,0,nSocketSize);

	InetSocketAddress.sin_family = AF_INET;
	InetSocketAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	InetSocketAddress.sin_port = htons(port);
/*
	if (SetSocketRcvTimeout(nSocket, SOCKET_RECV_TIMEOUT) < 0) {
		printf("SetSocketRcvTimeout() for port %d failed!\n", port);
	} else  printf("SetSocketRcvTimeout() set SOCKET_RECV_TIMEOUT to %d micro seconds\n", SOCKET_RECV_TIMEOUT);

	if (SetSocketSndTimeout(nSocket, SOCKET_SEND_TIMEOUT) < 0) {
		printf("SetSocketSndTimeout() for port %d failed!\n", port);
	} else  printf("SetSocketSndTimeout() set SOCKET_SEND_TIMEOUT to %d micro seconds\n", SOCKET_SEND_TIMEOUT);
*/
	nRetVal = bind(nSocket,SocketAddress,nSocketSize);
	printf("OpenInetServer(): Return of bind(socket): %d\n", nRetVal);			/* AW: added for debug */

	if (nRetVal < 0) {
			return 0;
	}

	nRetVal = listen(nSocket,5);											/* AW: listening for a maximum of 5 connections */
	printf("OpenInetServer(): Return of listen(socket): %d\n", nRetVal);						/* AW: added for debug */

	if (nRetVal < 0) {
			return 0;
	}

	int maxAcceptLoop = 100, i=0;

	for (i=0; i<maxAcceptLoop; i++) {
		nRetVal = accept(nSocket,SocketAddress,&nSocketSize);
		printf("OpenInetServer(): Loop %d : Return of accept(socket): %d\n", i, nRetVal);						/* AW: added for debug */
		if (nRetVal != -1) break;
		sleep(1);
	}
	if (nRetVal < 0) {
			return 0;
	}
	return nRetVal;
}

/**
 * @brief connects to the socket on the server, to be used, if socket() has been called separately before
 * @param inetsockaddr
 * @param nSocket.the sock_fd returned by socket()
 * @param ServerName is the name of the server to connect to
 * @return returns -1 on errors and 0 on successful connection
 */
int SearchForPortNumber(struct sockaddr_in *inetsockaddr,int nSocket,char *ServerName)
/* ServerName must be an Machine name, there is no check for it */
{
	int nRetVal;
	struct hostent *gethostent;
	gethostent = gethostbyname(ServerName);
	memcpy(&inetsockaddr->sin_addr,gethostent->h_addr,gethostent->h_length);
	nRetVal = connect(nSocket,(struct sockaddr *)inetsockaddr,sizeof(struct sockaddr_in));
	if (nRetVal < 0) {
		printf("Portnumber %d not found on Server %s!\n",htons(inetsockaddr->sin_port),ServerName);
		return -1;
	}
	return nRetVal; /* Success connection complete */
}

/**
 * @brief establishes a client connection to the given server and port
 * @param port the port to connect to
 * @param ServerName is the name of the server to connect to
 * @return returns 0 on errors and the connected sock_fd on successful connection
 */
int OpenInetClient(int port,char *ServerName)
{
		unsigned int inetaddr;
		//	struct hostent *gethostent;
		unsigned int nSocketSize;
		int nSocket,nRetVal;
		struct sockaddr_in InetSocketAddress;
		struct sockaddr *SocketAddress = (struct sockaddr *)&InetSocketAddress;

		if (port == 0) {
			printf("%s - ERROR: called with port == 0, aborted! \n ", __FUNCTION__);
			return 0;
		}
		int snl = strlen(ServerName);
		if ( snl <= 1) {
			printf("%s - ERROR: called with ServerName of length %d, ServerName = >>%s<<, aborted! \n ", __FUNCTION__, snl, ServerName);
			return 0;
		}
		printf("%s - opening connection to server: %s :: port %d \n ", __FUNCTION__, ServerName, port);

		nSocket = socket(AF_INET,SOCK_STREAM,0);
		if (nSocket < 0) {
				return 0;
		}

		nSocketSize = sizeof(struct sockaddr_in);

		memset(&InetSocketAddress,0,nSocketSize);

		InetSocketAddress.sin_family = AF_INET;
		inetaddr = inet_addr(ServerName);
		InetSocketAddress.sin_port = htons(port);
		if (inetaddr == INADDR_NONE) { /* Ooh, the ServerName wasn't an IP */
			if ((nRetVal = SearchForPortNumber(&InetSocketAddress,nSocket,ServerName)) != -1) return nSocket;
				close(nSocket);
				return 0;
			} else {
				InetSocketAddress.sin_addr.s_addr = inetaddr;
/*
		if (SetSocketRcvTimeout(nSocket, SOCKET_RECV_TIMEOUT) < 0) {
			printf("SetSocketRcvTimeout() for port %d failed!\n", port);
		} else  printf("SetSocketRcvTimeout() set SOCKET_RECV_TIMEOUT to %d micro seconds\n", SOCKET_RECV_TIMEOUT);

		if (SetSocketSndTimeout(nSocket, SOCKET_SEND_TIMEOUT) < 0) {
			printf("SetSocketSndTimeout() for port %d failed!\n", port);
		} else  printf("SetSocketSndTimeout() set SOCKET_SEND_TIMEOUT to %d micro seconds\n", SOCKET_SEND_TIMEOUT);
*/
		printf("OpenInetClient() - Connecting at ");printSecondMicrosecondNow();printf("\n");
		nRetVal = connect(nSocket,SocketAddress,nSocketSize);	// gar keine Antwort => ETIMEDOUT, kein wartender Serverprozess => ECONNREFUSED, bei Routing/Netzwerkfehler => ENETUNREACH oder EHOSTUNREACH
   		if (nRetVal < 0) {
   			perror("OpenInetClient() - connect: ");
   			printf("OpenInetClient() - Connection error at ");printSecondMicrosecondNow();printf("\n");
			close(nSocket);
            return 0;
       	}
	}
		printf("OpenInetClient() - Connected at ");printSecondMicrosecondNow();printf("\n");
        return nSocket;
}


int waitForWriteOk(int ms, int s)
{
        fd_set mask;
        struct timeval timeout = {0,0};

	timeout.tv_usec = ms*1000;
        if (s < 0) return -1;
        FD_ZERO(&mask);
        FD_SET(s,&mask);
       	return select(s+1,NULL,&mask,NULL,&timeout);
}

FILE *evb_log = NULL;

char *prpos(char *file, int line)
{
  static char posbuffer[256];
  int n;
  sprintf(posbuffer,"%s",file);
  n = strlen(posbuffer);
  sprintf(posbuffer+n,"%d",line);
  return posbuffer;
}

int readAndVerifyAeraMagic(Msg *retmsg) {
//	  printf("%s - AERA_MAGIC = %s, has sizeof() = %d\n", __FUNCTION__, AERA_MAGIC_CHARS, sizeof(AERA_MAGIC));
//	  printf("%s - Msg has length %d \n", __FUNCTION__, retmsg->length);
	  char *startOfMagic = (char*)&retmsg->data[retmsg->length - sizeof(AERA_MAGIC)];
	  char chMagic[sizeof(AERA_MAGIC) + 1];
	  int i=0, j=0;
	  for (i=0;i<sizeof(AERA_MAGIC);i++) {
//		  printf("Magic[%d] = %c, in hex: 0x%X\n", i, startOfMagic[i], startOfMagic[i]);
		  chMagic[i] = startOfMagic[i];
		  if (startOfMagic[i] != AERA_MAGIC_CHARS[i]) {
//			  fprintf(stderr, "%s - Msg of length %d does not terminate with AERA_MAGIC! Last %d bytes:\n", __FUNCTION__, retmsg->length, sizeof(AERA_MAGIC));
			  printf("%s - Msg of length %d does not terminate with AERA_MAGIC! Last %d bytes:\n", __FUNCTION__, retmsg->length, sizeof(AERA_MAGIC));
			  for (j=0;j<sizeof(AERA_MAGIC);j++) {
			  	  printf("Magic[%d] = %c, in hex: 0x%X\n", j, startOfMagic[j], startOfMagic[j]);
			  }
			  return -1;			// error on checking magic!
		  }
	  }
	  chMagic[sizeof(AERA_MAGIC)] = '\0';
//	  printf("%s - AERA Magic = %s is verified!!\n", __FUNCTION__, chMagic);
//	  printf("%s - length of Msg including AERA_MAGIC = %d, now reducing by sizeof(AERA_MAGIC) = %d\n", __FUNCTION__, retmsg->length, sizeof(AERA_MAGIC));
	  retmsg->length -= sizeof(AERA_MAGIC);
//	  printf("%s - length of resulting Msg = %d\n", __FUNCTION__, retmsg->length);

	  return 0;			// everything ok
}

// writing the AERA magic word to the socket - as a terminator of AERA buffers as defined in the transport proposal
int writeAeraMagic (int sock) {
	int temp =0;
//	  printf("%s - USE_AERA_TRANSPORT_DEFINITIONS is defined, writing AERA_MAGIC_CHARS\n", __FUNCTION__);
	  temp = write(sock,(char *)AERA_MAGIC_CHARS,sizeof(AERA_MAGIC));
	  if (temp < 0) {	// do not sent these few bytes in a loop, just react on problems
	    char msg[256];
		sprintf(msg, "[%s, %s] %s on socket %d: ", JDtoString(0), getSecondMicrosecondNow(), __FUNCTION__, sock);
	    perror(msg);
	    return -1;
	  } else {
//		  printf("%s - writing AERA_MAGIC_CHARS returned %d \n", __FUNCTION__, temp);
	  }
	  return 0;
}


/**
 * @brief reads a message from the given socket, to avoid blocking on a blocking socket, select() with a pre-defined timeout is called before each read attempt
 * @param sock the sock_fd to read from
 * @return returns NULL on errors or a pointer to the newly allocated memory with the read Msg. DO NOT FORGET TO FREE THIS MSG AFTER PROCESSING IT !
 */
Msg * ReadSockMsg ( int sock )
/* here is a critical section of code: select() != 0, but no data in the socket -> read() returns with EOF */
{
  int temp,rest,ptr = 0,reps = 0, readZeros=0, n;
  MSGTYPE_LENGTH length = 0;
  Msg *retmsg;
  fd_set readfds;
  struct timeval timeout;
  FD_ZERO(&readfds);
  FD_SET(sock,&readfds);

  timeout.tv_sec = 0;
  timeout.tv_usec =  READ_TIMEOUT_US;

  length = 0;

  /*if (!TestAndRepairStream(sock)) return NULL;*/

  while(ptr < sizeof(length)) {
	n = select(sock+1,&readfds,NULL,NULL,&timeout);		// call select even before reading the length
	if (!n) {		// error or timeout
	  printf("%s - socket %d not ready to read length of total %d bytes!!! Returning NULL\n", __FUNCTION__, sock, sizeof(length));
	  return NULL;
	}
    temp = read(sock,(char *)&length+ptr,sizeof(length)-ptr);
    if (temp < 0) {
// AW, 21.09.2011 - added '=' to detect lost connections. read returns -1 on errors and 0 for EOF
// from my experience, sometimes read returns ECONNRES (connection reset by peer) and sometimes EOF on lost connections, so EOF is an error here!
/* removed '=': sometimes there is an EOF despite the select() != 0. Socket status at this stage is CLOSE_WAIT */
/* this is a unix kernel problem! */
      perror("ReadSockMsg() - reading length ");
      return NULL;
    } else ptr += temp;
/* the following part is for test only */
    reps++;
    if (reps > 20) {
    	printf("ReadSockMsg() - tried 20 times to read length, but failed => probably end of file EOF => returning NULL. Setting errno to ENOTCONN\n");fflush(stdout);
    	errno = ENOTCONN;
    	return NULL;
    }
  }

  rest = length;
  if ((rest <=0) || (rest > SHRT_MAX)) {
    printf("ReadSockMsg() - Severe: bad length! %d on Socket %d\n",length,sock);
    return NULL;
  }

  printf("%s, allocating memory of size %d bytes\n", __FUNCTION__, (rest+sizeof(MSGTYPE_LENGTH)));
  retmsg = (Msg *) malloc(rest+sizeof(MSGTYPE_LENGTH));
  if (retmsg == NULL) {
	  printf("%s, allocating memory of size %d bytes failed!!!!\n", __FUNCTION__, (rest+sizeof(MSGTYPE_LENGTH)));
	  return NULL;
  }
  retmsg->length = length;

  while(rest) {
// as read may block on connection problems, call again select before each read
	n = select(sock+1,&readfds,NULL,NULL,&timeout);
	if (!n) {		// error or timeout
	//	  perror("ReadSockMsg() ");
	  printf("%s - socket %d not ready to read on reading rest bytes = %d of total %d bytes!!! Returning NULL\n", __FUNCTION__, sock, rest, length);
	  return NULL;
	}
    temp = read(sock, retmsg->data + length - rest,MIN(rest,MAXBUFF_UNIX));
    if (temp < 0) {
      perror("ReadSockMsg() - error in reading data : ");
      fflush(stderr);
      free(retmsg);					// AW, 10.08.2011 - added to free on read failure
      return NULL;
    } else   if (temp == 0) {
      readZeros++;
  	  printf("ReadSockMsg() reading Msg-length returned 0 the %d time!\n", readZeros);
  	  if (readZeros == READ_LOOP_LIMIT) {
  		  printf("ReadSockMsg() reading Msg-length returned 0 the %d time! => reading aborted, returning NULL\n", readZeros);
  	      return NULL;
  	  }
    }
    rest -= temp;
  }
//  printf("ReadSockMsg(): msg length = %d, message points to %d \n", retmsg->length, (int*)retmsg);

  return retmsg;
}

/**
 * @brief reads a message from the given socket, to avoid blocking on a blocking socket, select() with a pre-defined timeout is called before each read attempt
 * @param sock the sock_fd to read from
 * @return returns NULL on errors or a pointer to the static memory with the read Msg. This memory is re-used, so be sure, to process the message before calling the function again!!! The previous message will be overwritten!!!
 */
Msg * ReadSockMsgSM ( int sock )
/* here is a critical section of code: select() != 0, but no data in the socket -> read() returns with EOF */
{
  int temp,rest,ptr = 0,reps = 0, readZeros=0, blockCount=0;
  MSGTYPE_LENGTH length = 0;
  Msg *retmsg;

  length = 0;

  /*if (!TestAndRepairStream(sock)) return NULL;*/

  while(ptr < sizeof(length)) {
    temp = read(sock,(char *)&length+ptr,sizeof(length)-ptr);
    if (temp < 0) {
// AW, 21.09.2011 - added '=' to detect lost connections. read returns -1 on errors and 0 for EOF
// from my experience, sometimes read returns ECONNRES (connection reset by peer) and sometimes EOF on lost connections, so EOF is an error here!
/* removed '=': sometimes there is an EOF despite the select() != 0. Socket status at this stage is CLOSE_WAIT */
/* this is a unix kernel problem! */
      perror("ReadSockMsgSM() - reading length ");
      return NULL;
    } else ptr += temp;
/* the following part is for test only */
    reps++;
    if (reps > 20) {
    	printf("ReadSockMsgSM() - tried 20 times to read length, but failed => probably end of file EOF => returning NULL. Setting errno to ENOTCONN\n");fflush(stdout);
    	errno = ENOTCONN;
    	return NULL;
    }
  }

  rest = length;
  if ((rest <=0) || (rest > SHRT_MAX)) {
    printf("ReadSockMsgSM() - Severe: bad length! %d on Socket %d\n",length,sock);
    return NULL;
  }

//  retmsg = (Msg *) malloc(rest+sizeof(MSGTYPE_LENGTH));
  cleanMsgRcvBuff();
  retmsg = (Msg *) msgRcvBuff;
  retmsg->length = length;

  fd_set readfds;
  int n;
  struct timeval timeout;
  FD_ZERO(&readfds);
  FD_SET(sock,&readfds);

  timeout.tv_sec = 0;
  timeout.tv_usec =  READ_TIMEOUT_US;

  while(rest) {
	blockCount++;
// as read may block on connection problems, call again select before each read
	n = select(sock+1,&readfds,NULL,NULL,&timeout);
	if (!n) {		// error or timeout
	//	  perror("ReadSockMsg() ");
	  printf("%s - socket %d not ready to read on reading rest bytes = %d of total %d bytes!!! Returning NULL\n", __FUNCTION__, sock, rest, length);
	  return NULL;
	}

    temp = read(sock, retmsg->data + length - rest,MIN(rest,MAXBUFF_UNIX));
    if (temp < 0) {
      perror("ReadSockMsgSM() - error in reading data : ");
      fflush(stderr);
//      free(retmsg);					// AW, 10.08.2011 - added to free on read failure
      return NULL;
    } else   if (temp == 0) {
      readZeros++;
  	  printf("ReadSockMsgSM() reading Msg-length returned 0 the %d time!\n", readZeros);
  	  if (readZeros == READ_LOOP_LIMIT) {
  		  printf("ReadSockMsgSM() reading Msg-length returned 0 the %d time! => reading aborted, returning NULL\n", readZeros);
  	      return NULL;
  	  }
    }
    rest -= temp;
//    printf("ReadSockMsgSM() read block no. %d with length of %d bytes of message with total length %d, %d bytes of rest missing \n", blockCount, temp, length, rest);
  }
  return retmsg;
}

/**
 * @brief reads a message from the given socket, to avoid blocking on a blocking socket, select() with the given timeout is called before each read attempt
 * @param sock the sock_fd to read from
 * @param time the timeout used for select() - call preceeding the read() - call in micro seconds
 * @return returns NULL on errors or a pointer to the newly allocated memory with the read Msg.  DO NOT FORGET TO FREE THIS MSG AFTER PROCESSING IT !
 */
Msg * ReadSockMsgTO ( int sock, int time )
{
  int n,temp,rest, readZeros=0, blockCount=0;
  MSGTYPE_LENGTH length;
  Msg *retmsg;
  fd_set readfds;
  struct timeval timeout;
  FD_ZERO(&readfds);
  FD_SET(sock,&readfds);

  timeout.tv_sec = 0;
  timeout.tv_usec = time;

#ifdef USE_AERA_TRANSPORT_DEFINITIONS
  if (printedAeraTransportDefined == 0) {
	  printf("%s - USE_AERA_TRANSPORT_DEFINITIONS is defined!\n", __FUNCTION__);
	  printedAeraTransportDefined = 1;
  }
#endif

  n = select(sock+1,&readfds,NULL,NULL,&timeout);
  if (!n) {		// error or timeout
	  return NULL;
  }
  temp = read(sock,(char *)&length,sizeof(length));
  if (temp < 0) { /* '=' means EOF, which is returned on reading more than available and sometimes on broken connections */
    perror("ReadSockMsgTO() ");
    return NULL;
  } else   if (temp == 0) {
	  printf("ReadSockMsgTO() reading Msg-length returned 0! Reading Msg aborted, returning NULL\n");
	  return NULL;
  } else if (temp != sizeof(length)) {
	  printf("ReadSockMsgTO() reading Msg-length failed, read returned only %d bytes instead of %d! Reading Msg aborted, returning NULL\n", temp, sizeof(length));
  }

#ifdef USE_AERA_TRANSPORT_DEFINITIONS
  rest = length*sizeof(UINT16);
//  printf("%s - USE_AERA_TRANSPORT_DEFINITIONS: expecting message of length %d (count of UINT16) = lengthBytes %d\n", __FUNCTION__, length, rest);
  length = rest; 		// length is used as offset for reading in retmsg-buffer, so it has to be byte count
  // in transport definitions, the length is no longer count of bytes, but count of UINT16
#else
  rest = length;
#endif

  retmsg = (Msg *) malloc(rest+sizeof(MSGTYPE_LENGTH));
  if (retmsg == NULL) {
	  fprintf(stderr, "MEM_ERR!%s - malloc %d bytes fail!\n", __FUNCTION__, (rest+sizeof(MSGTYPE_LENGTH)));
	  return NULL;
  }

  retmsg->length = rest;

  if (length == 0) {
	  printf("%s - read length == 0!?!? Forbidden, taking it as an error, returning NULL\n", __FUNCTION__);
	  free(retmsg);
	  return NULL;
  }

  timeout.tv_usec =  time;

  while(rest) {
	blockCount++;
// as read may block on connection problems, call again select before each read
	n = select(sock+1,&readfds,NULL,NULL,&timeout);
	if (n<=0) {		// error or timeout
	  perror("ReadSockMsg() - select() in read-loop: ");
	  printf("%s - socket %d not ready to read on reading rest bytes = %d of total %d bytes!!! Returning NULL\n", __FUNCTION__, sock, rest, length);
	  free(retmsg);
	  return NULL;
	}

    temp = read(sock, retmsg->data + length - rest,MIN(rest,MAXBUFF_UNIX));
    if (temp < 0) {
      perror("ReadSockMsgTO() - read in read loop: ");
      free(retmsg);
      return NULL;
    } else   if (temp == 0) {
      readZeros++;
  	  printf("ReadSockMsgTO() reading Msg-length returned 0 the %d time!\n", readZeros);
  	  if (readZeros == READ_LOOP_LIMIT) {
  		  printf("ReadSockMsgTO() reading Msg-length returned 0 the %d time! => reading aborted, returning NULL\n", readZeros);
  		  free(retmsg);
  		  return NULL;
  	  }
    }
    rest -= temp;
//    printf("ReadSockMsgTO() read block no. %d with length of %d bytes of message with total length %d, %d bytes of rest missing \n", blockCount, temp, length, rest);
  }

#ifdef USE_AERA_TRANSPORT_DEFINITIONS
  // confirm AERA_MAGIC and cut it from Msg
  if (readAndVerifyAeraMagic(retmsg) < 0) {
	  printf("%s - error checking AERA magic!\n", __FUNCTION__);
	  free(retmsg);
	  return NULL;
  }
/*  printf("%s - AERA_MAGIC = %s, has sizeof() = %d\n", __FUNCTION__, AERA_MAGIC_CHARS, sizeof(AERA_MAGIC));
  char *startOfMagic = (char*)&retmsg->data[length - sizeof(AERA_MAGIC)];
  int i=0, j=0;
  for (i=0;i<sizeof(AERA_MAGIC);i++) {
	  printf("Magic[%d] = %c, in hex: 0x%X\n", i, startOfMagic[i], startOfMagic[i]);
	  if (startOfMagic[i] != AERA_MAGIC_CHARS[i]) {
		  fprintf(stderr, "%s - Msg of length %d does not terminate with AERA_MAGIC! Last 4 bytes:\n", __FUNCTION__, retmsg->length);
		  for (j=0;j<sizeof(AERA_MAGIC);j++) {
		  	  printf("Magic[%d] = %c, in hex: 0x%X\n", j, startOfMagic[j], startOfMagic[j]);
		  }
	  }
  }
  printf("%s - length of Msg including AERA_MAGIC = %d, now reducing by sizeof(AERA_MAGIC) = %d\n", __FUNCTION__, retmsg->length, sizeof(AERA_MAGIC));
  retmsg->length -= sizeof(AERA_MAGIC);
  printf("%s - length of resulting Msg = %d\n", __FUNCTION__, retmsg->length);
*/
#endif // USE_AERA_TRANSPORT_DEFINITIONS

  return retmsg;
}


/**
 * @brief reads a message from the given socket, to avoid blocking on a blocking socket, select() with the given timeout is called before each read attempt
 * @param sock the sock_fd to read from
 * @param time the timeout used for select() - call preceeding the read() - call in micro seconds
 * @return returns NULL on errors or a pointer to the static memory with the read Msg. This memory is re-used, so be sure, to process the message before calling the function again!!! The previous message will be overwritten!!!
 */
Msg * ReadSockMsgSM_TO ( int sock, int time )
{
  int n,temp,rest, readZeros=0, blockCount=0;
  MSGTYPE_LENGTH length;
  Msg *retmsg;
  fd_set readfds;
  struct timeval timeout;
  FD_ZERO(&readfds);
  FD_SET(sock,&readfds);

  timeout.tv_sec = 0;
  timeout.tv_usec = time;

//  printf("%s - USE_AERA_TRANSPORT_DEFINITIONS is defined!\n", __FUNCTION__);

#ifdef USE_AERA_TRANSPORT_DEFINITIONS
  if (printedAeraTransportDefined == 0) {
	  printf("%s - USE_AERA_TRANSPORT_DEFINITIONS is defined!\n", __FUNCTION__);
	  printedAeraTransportDefined = 1;
  }
#endif

// if select is call before calling ReadSockMsgSM_TO(), this select here can be skipped
/*	if (socketRcvTimeoutInUse != 1) {
	  n = select(sock+1,&readfds,NULL,NULL,&timeout);
	  if (!n) {			// error or timeout
	//	  perror("ReadSockMsg() ");
	//	  printf("%s - sock %d not ready to read\n", __FUNCTION__, sock);
		  return NULL;
	  }
	}*/
  // reading the length, which is still count of bytes without and count of UINT16 with transport layer
  temp = read(sock,(char *)&length,sizeof(length));

  if (temp < 0) { /* '=' means EOF */
	  sprintf(msg, "[%s, %s] WriteSockMsg() - select on socket %d: ", JDtoString(0), getSecondMicrosecondNow(), sock);
	  perror("ReadSockMsgSM_TO() - read msg length - ");
    return NULL;
  } else   if (temp == 0) {
	  printf("ReadSockMsgSM_TO() reading Msg-length returned 0, means EOF! Reading Msg aborted, returning NULL\n");
	  return NULL;
  } else if (temp != sizeof(length)) {
	  printf("ReadSockMsgSM_TO() reading Msg-length failed, read returned only %d bytes instead of %d! Reading Msg aborted, returning NULL\n", temp, sizeof(length));
  }

#ifdef USE_AERA_TRANSPORT_DEFINITIONS
  rest = length*sizeof(UINT16);
//  printf("%s - USE_AERA_TRANSPORT_DEFINITIONS: expecting message of length %d (count of UINT16) = lengthBytes %d\n", __FUNCTION__, length, rest);
  length = rest; 		// length is used as offset for reading in retmsg-buffer, so it has to be byte count
  // in transport definitions, the length is no longer count of bytes, but count of UINT16
#else
  rest = length;
#endif

  cleanMsgRcvBuff();
  retmsg = (Msg *) msgRcvBuff;		// use static memory
  // the returned message always gets the count in bytes, independant of transport layer
  retmsg->length = rest;

  if (length == 0) {
	  printf("%s - read length == 0!?!? Forbidden, taking it as an error, returning NULL\n", __FUNCTION__);
	  return NULL;
  }

//  timeout.tv_usec =  READ_TIMEOUT_US;

  while(rest) {
	blockCount++;
	if (socketRcvTimeoutInUse != 1) {
// as read may block on connection problems, call again select before each read
//		printf("using select, socket has no SO_RCVTIMEOUT set\n");
		n = select(sock+1,&readfds,NULL,NULL,&timeout);
		if (n < 0) {		// error
		  perror("ReadSockMsgSM_TO() - select - ");
		  printf("%s - select returned %d => socket %d not ready to read on reading rest bytes = %d of total %d bytes!!! Returning NULL\n", __FUNCTION__, n, sock, rest, length);
		  fprintf(stderr, "%s - select returned %d => socket %d not ready to read on reading rest bytes = %d of total %d bytes!!! Returning NULL\n", __FUNCTION__, n, sock, rest, length);
		  return NULL;
		} else if (n == 0) {	// timeout
		  perror("ReadSockMsgSM_TO() - select - ");
		  printf("%s - select returned %d => timeout (= %d us) on socket %d, not ready to read on reading rest bytes = %d of total %d bytes!!! Returning NULL\n", __FUNCTION__, n, time, sock, rest, length);
		  return NULL;
		}
	} //else printf("not using select, socket has a SO_RCVTIMEOUT set\n");
    temp = read(sock, retmsg->data + length - rest,MIN(rest,MAXBUFF_UNIX));
    if (temp < 0) {
    	int sockerr = errno;
    	perror("ReadSockMsgSM_TO() - reading msg - ");
      if (sockerr == EAGAIN)  {
    	  printf("%s - reading returned %d, errno = %d = EAGAIN => trying again ...\n",  __FUNCTION__, temp, sockerr);
    	  continue;
      } else return NULL;
    } else   if (temp == 0) {
      readZeros++;
  	  printf("%s - ReadSockMsgSM_TO() reading Msg-length returned 0 the %d time!\n",  __FUNCTION__, readZeros);
  	  if (readZeros == READ_LOOP_LIMIT) {
  		  printf("%s - ReadSockMsgSM_TO() reading Msg-length returned 0 the %d time! => reading aborted, returning NULL\n",  __FUNCTION__, readZeros);
  	      return NULL;
  	  }
    } else rest -= temp;

//    printf("ReadSockMsgSM_TO() read block no. %d with length of %d bytes of message with total length %d, %d bytes of rest missing from socket %d\n", blockCount, temp, length, rest, sock);
  }
  if (readZeros < READ_LOOP_LIMIT) errno = 0;		// this is not taken as an error condition

#ifdef USE_AERA_TRANSPORT_DEFINITIONS
  // confirm AERA_MAGIC and cut it from Msg
  if (readAndVerifyAeraMagic(retmsg) < 0) {
	  printf("%s - error checking AERA magic!\n", __FUNCTION__);
	  return NULL;
  }
/*  printf("%s - AERA_MAGIC = %s, has sizeof() = %d\n", __FUNCTION__, AERA_MAGIC_CHARS, sizeof(AERA_MAGIC));
  char *startOfMagic = (char*)&retmsg->data[length - sizeof(AERA_MAGIC)];
  int i=0, j=0;
  for (i=0;i<sizeof(AERA_MAGIC);i++) {
	  printf("Magic[%d] = %c, in hex: 0x%X\n", i, startOfMagic[i], startOfMagic[i]);
	  if (startOfMagic[i] != AERA_MAGIC_CHARS[i]) {
		  fprintf(stderr, "%s - Msg of length %d does not terminate with AERA_MAGIC! Last 4 bytes:\n", __FUNCTION__, retmsg->length);
		  for (j=0;j<sizeof(AERA_MAGIC);j++) {
		  	  printf("Magic[%d] = %c, in hex: 0x%X\n", j, startOfMagic[j], startOfMagic[j]);
		  }
	  }
  }
  printf("%s - length of Msg including AERA_MAGIC = %d, now reducing by sizeof(AERA_MAGIC) = %d\n", __FUNCTION__, retmsg->length, sizeof(AERA_MAGIC));
  retmsg->length -= sizeof(AERA_MAGIC);
  printf("%s - length of resulting Msg = %d\n", __FUNCTION__, retmsg->length);
*/
#endif // USE_AERA_TRANSPORT_DEFINITIONS

  return retmsg;
}

/**
 * @brief reads a message from the given socket into the given memory, to avoid blocking on a blocking socket, select() with the given timeout is called before each read attempt
 * @param sock the sock_fd to read from
 * @param time the timeout used for select() - call preceding the read() - call in micro seconds
 * @param mp a pointer to a memory space, where the message is written to. Make sure, its size is sufficient!!!
 * @return returns NULL on errors or returns back the pointer to the external static memory with the read Msg.
 */
Msg * ReadSockMsgEM_TO ( int sock, int time, char* mp )
{
  int n,temp,rest, readZeros=0, blockCount=0;
  MSGTYPE_LENGTH length;
  Msg *retmsg;
  fd_set readfds;
  struct timeval timeout;
  FD_ZERO(&readfds);
  FD_SET(sock,&readfds);

  timeout.tv_sec = 0;
  timeout.tv_usec = time;

  n = select(sock+1,&readfds,NULL,NULL,&timeout);
  if (!n) {			// error or timeout
	  perror("ReadSockMsgEM_TO() ");
	  printf("%s - socket %d not ready to read\n", __FUNCTION__, sock);
	  return NULL;
  }
  temp = read(sock,(char *)&length,sizeof(length));

  if (temp <= 0) { /* '=' means EOF */
    perror("ReadSockMsgEM_TO() ");
    return NULL;
  } else   if (temp == 0) {
	  printf("ReadSockMsgEM_TO() reading Msg-length returned 0! Reading Msg aborted, returning NULL\n");
	  return NULL;
  }

  rest = length;
  retmsg = (Msg *) mp;		// use the external memory
  retmsg->length = length;
  timeout.tv_usec =  READ_TIMEOUT_US;

  while(rest) {
	blockCount++;
// as read may block on connection problems, call again select before each read
	n = select(sock+1,&readfds,NULL,NULL,&timeout);
	if (!n) {		// error or timeout
	//	  perror("ReadSockMsgEM_TO() ");
	  printf("%s - socket %d not ready to read on reading rest bytes = %d of total %d bytes!!! Returning NULL\n", __FUNCTION__, sock, rest, length);
	  return NULL;
	}
	temp = read(sock, retmsg->data + length - rest,MIN(rest,MAXBUFF_UNIX));			// this adress calculation is only valid for character pointers!!
    if (temp < 0) {
      perror("ReadSockMsgEM_TO() ");
      return NULL;
    } else if (temp == 0) {
      readZeros++;
  	  printf("ReadSockMsgEM_TO() reading Msg-length returned 0 the %d time!\n", readZeros);
  	  if (readZeros == READ_LOOP_LIMIT) {
  		  printf("ReadSockMsgEM_TO() reading Msg-length returned 0 the %d time! => reading aborted, returning NULL\n", readZeros);
  	      return NULL;
  	  }
    }
    rest -= temp;
  }
  return retmsg;
}


/**
 * @brief writes the message referenced by the pointer to the given socket. To avoid blocking on a blocking socket, select() with a predefined timeout is called before each write attempt
 * @param sock the sock_fd to write to
 * @param sendMsg pointer to the Msg, that should be written
 * @return returns 0 on success, -1 on errors or a positive number giving the rest of bytes not written to socket
 */
#ifndef USE_AERA_TRANSPORT_DEFINITIONS
int WriteSockMsg ( int sock, Msg *sendmsg )
{
  int rest,temp, writeZeros = 0, totMsgLength;
  fd_set writefds;
  int maxEAGAINloops = 20;

  if (!sendmsg->length) return 0;

#ifdef USE_AERA_TRANSPORT_DEFINITIONS
  int orgMsgLength = sendmsg->length;				// this is the original message length. It is increased cause the AERA_MAGIC is added, but has to be restored, cause the Msg may be used by application afterwards!
#endif

  rest = TotalMsgLength(sendmsg->length);
  totMsgLength = TotalMsgLength(sendmsg->length);
  if (!sock) return rest;

#ifdef USE_AERA_TRANSPORT_DEFINITIONS
  if (printedAeraTransportDefined == 0) {
	  printf("%s - USE_AERA_TRANSPORT_DEFINITIONS is defined!\n", __FUNCTION__);
	  printedAeraTransportDefined = 1;
  }
  printf("%s - USE_AERA_TRANSPORT_DEFINITIONS is defined - sendmsg->length = %d has to be increased by sizeof(AERA_MAGIC) = %d\n", __FUNCTION__, sendmsg->length,sizeof(AERA_MAGIC));
  sendmsg->length += sizeof(AERA_MAGIC);
//  printf("%s - sendmsg->length = %d after increasing\n", __FUNCTION__, sendmsg->length);
#endif

/* AW: added for protocol debug */
//  printf("WriteSockMsg(): writing message with TotalMsgLength %d (length before %d) to socket %d\n", rest, sendmsg->length, sock);fflush(stdout);
//  printf("WriteSockMsg(): msg length = %d, message points to %d \n", sendmsg->length, (int*)sendmsg);

  FD_ZERO(&writefds);
  FD_SET(sock, &writefds);
  int n, eagainLoopCount = 0;
  struct timeval timeout;
  timeout.tv_sec = 0;
  timeout.tv_usec =  WRITE_TIMEOUT_US;
  while(rest) {
// as write may block on connection problems, call again select before each write, but only if SO_SNDTIMEO is not set
	if (socketSndTimeoutInUse != 1) {
		n = select(sock+1,NULL,&writefds,NULL,&timeout);
		if (!n) {		// error or timeout
		  sprintf(msg, "[%s, %s] WriteSockMsg() - select on socket %d: ", JDtoString(0), getSecondMicrosecondNow(), sock);
		  perror(msg);
#ifdef USE_AERA_TRANSPORT_DEFINITIONS
		  sendmsg->length = orgMsgLength;				// restore the original message length. It was increased cause the AERA_MAGIC is added, but has to be restored, cause the Msg may be used by application afterwards!
#endif
	//	  printf("%s - socket %d not ready to write on writing rest bytes = %d of total %d bytes!!! Returning NULL\n", __FUNCTION__, sock, rest, sendmsg->length);
		  return -1;
		}
	} // else printf("not using select, socket has a SO_SNDTIMEOUT set\n");
    temp = write(sock,(char *)sendmsg + totMsgLength - rest,MIN(rest,MAXBUFF_UNIX));
    if (temp < 0) {
      if (errno == EAGAIN) {
    	  if (eagainLoopCount < maxEAGAINloops) {
  		  printf("WriteSockMsg() writing returned %d == EAGAIN => trying again the %d time ...\n", temp, eagainLoopCount);
    		  eagainLoopCount++;
 //   		  usleep(100);
    		  continue;
    	  } else {
   		 	  printf("WriteSockMsg() writing returned %d == EAGAIN the %d time => taking it as an error!\n", temp, eagainLoopCount);
    	  }
      }
      printSecondMicrosecondNow();
	  sprintf(msg, "[%s, %s] WriteSockMsg() on socket %d: ", JDtoString(0), getSecondMicrosecondNow(), sock);
      perror(msg);
#ifdef USE_AERA_TRANSPORT_DEFINITIONS
		  sendmsg->length = orgMsgLength;				// restore the original message length. It was increased cause the AERA_MAGIC is added, but has to be restored, cause the Msg may be used by application afterwards!
#endif
      return -1;
    } else   if (temp == 0) {
      writeZeros++;
//  	  printf("WriteSockMsg() writing returned 0 the %d time!\n", writeZeros);
  	  if (writeZeros == WRITE_LOOP_LIMIT) {
  		  printf("WriteSockMsg() writing returned 0 the %d time! => writing aborted, returning 0\n", writeZeros);
#ifdef USE_AERA_TRANSPORT_DEFINITIONS
		  sendmsg->length = orgMsgLength;				// restore the original message length. It was increased cause the AERA_MAGIC is added, but has to be restored, cause the Msg may be used by application afterwards!
#endif
  	      return rest;
  	  }
    }
    rest -= temp;
  }
#ifdef USE_AERA_TRANSPORT_DEFINITIONS
  if (writeAeraMagic(sock) < 0) {
	  printf("%s - error writing AERA magic to socket %d\n", __FUNCTION__, sock);
	  sendmsg->length = orgMsgLength;				// restore the original message length. It was increased cause the AERA_MAGIC is added, but has to be restored, cause the Msg may be used by application afterwards!
	  return -1;
  }
 /*
  printf("%s - USE_AERA_TRANSPORT_DEFINITIONS is defined, writing AERA_MAGIC_CHARS\n", __FUNCTION__);
  temp = write(sock,(char *)AERA_MAGIC_CHARS,sizeof(AERA_MAGIC));
  if (temp < 0) {	// do not sent these few bytes in a loop, just react on problems
	sprintf(msg, "[%s,%d] WriteSockMsg() on socket %d: ", JDtoString(0), (int)time(NULL), sock);
    perror(msg);
    return -1;
  } else {
	  printf("%s - writing AERA_MAGIC_CHARS returned %d \n", __FUNCTION__, temp);
  }
 */
  sendmsg->length = orgMsgLength;				// restore the original message length. It was increased cause the AERA_MAGIC is added, but has to be restored, cause the Msg may be used by application afterwards!
  #endif

  return rest;
}
#else		// USE_AERA_TRANSPORT_DEFINITIONS
#ifdef COPY_MSG_ON_WRITE
int WriteSockMsg ( int sock, Msg *sendmsg )
{
  int rest,temp, writeZeros = 0, totMsgLength;
  fd_set writefds;
  char msg[256];
  int maxEAGAINloops = 20;
  Msg* sendMsgWriteBuff = (Msg*) msgWriteBuff;

  if (!sendmsg->length) return 0;
  if (!sock) return rest;

  // the message to send has length in count of bytes, independant of transport layer
  rest = (TotalMsgLength(sendmsg->length));
  totMsgLength = (TotalMsgLength(sendmsg->length));

  if (printedAeraTransportDefined == 0) {
	  printf("%s - USE_AERA_TRANSPORT_DEFINITIONS is defined! And using a copy of msg to send...\n", __FUNCTION__);
	  printedAeraTransportDefined = 1;
  }

// for transport layer, the original size of the message has to be increased by the sizeof(AERA_MAGIC), not to destroy the original message (may be re-used!), a copy is made, which then is modified
//  printf("%s - msgWriteBuff has address = 0x%X\n", __FUNCTION__, ((int)&msgWriteBuff));
  memcpy(msgWriteBuff, sendmsg, TotalMsgLength(sendmsg->length));
//  printf("%s - TotalMsgLength = %d of sendmsg->length = %d. Copy AERA_MAGIC to msgWriteBuff at address = 0x%X\n", __FUNCTION__, TotalMsgLength(sendmsg->length), sendmsg->length, (int) &msgWriteBuff[TotalMsgLength(sendmsg->length)]);
  memcpy(&msgWriteBuff[TotalMsgLength(sendmsg->length)], &AERA_MAGIC_CHARS  , sizeof(AERA_MAGIC));
#ifdef NOT_USED
  int x;
  for (x=0; x<sizeof(AERA_MAGIC);x++) {
	  msgWriteBuff[TotalMsgLength(sendmsg->length) + x] = AERA_MAGIC_CHARS[x];
  }
  for (x=(TotalMsgLength(sendmsg->length)-2); x<(TotalMsgLength(sendmsg->length)+sizeof(AERA_MAGIC));x++) {
  	  printf("Byte[%d] = %c, in hex: 0x%X\n", x, msgWriteBuff[x], msgWriteBuff[x]);
  }
#endif

//  printf("%s - USE_AERA_TRANSPORT_DEFINITIONS is defined - sendmsg->length = %d has to be increased by sizeof(AERA_MAGIC) = %d\n", __FUNCTION__, sendMsgWriteBuff->length,sizeof(AERA_MAGIC));
  sendMsgWriteBuff->length = (totMsgLength - sizeof(sendmsg->length) + sizeof(AERA_MAGIC))/sizeof(UINT16);
  rest += sizeof(AERA_MAGIC);
  totMsgLength += sizeof(AERA_MAGIC);
//  printf("%s - sendMsgWriteBuff->length = %d after increasing\n", __FUNCTION__, sendMsgWriteBuff->length);

/* AW: added for protocol debug */
//  printf("WriteSockMsg(): writing message with TotalMsgLength %d (length before %d) to socket %d\n", rest, sendMsgWriteBuff->length, sock);fflush(stdout);
//  printf("WriteSockMsg(): msg length = %d, message points to %d \n", sendmsg->length, (int*)sendmsg);

  FD_ZERO(&writefds);
  FD_SET(sock, &writefds);
  int n, eagainLoopCount = 0;
  struct timeval timeout;
  timeout.tv_sec = 0;
  timeout.tv_usec =  WRITE_TIMEOUT_US;
  while(rest) {
// as write may block on connection problems, call again select before each write, but only if SO_SNDTIMEO is not set
	if (socketSndTimeoutInUse != 1) {
		n = select(sock+1,NULL,&writefds,NULL,&timeout);
		if (!n) {		// error or timeout
		  sprintf(msg, "[%s, %s] WriteSockMsg() - select on socket %d: ", JDtoString(0), getSecondMicrosecondNow(), sock);
		  perror(msg);
	//	  printf("%s - socket %d not ready to write on writing rest bytes = %d of total %d bytes!!! Returning NULL\n", __FUNCTION__, sock, rest, sendmsg->length);
		  return -1;
		}
	} // else printf("not using select, socket has a SO_SNDTIMEOUT set\n");
    temp = write(sock,(char *)msgWriteBuff + totMsgLength - rest,MIN(rest,MAXBUFF_UNIX));
    if (temp < 0) {
      if (errno == EAGAIN) {
    	  if (eagainLoopCount < maxEAGAINloops) {
    		  printf("WriteSockMsg() writing returned %d == EAGAIN => trying again the %d time ...\n", temp, eagainLoopCount);
    		  eagainLoopCount++;
 //   		  usleep(100);
    		  continue;
    	  } else {
   		 	  printf("WriteSockMsg() writing returned %d == EAGAIN the %d time => taking it as an error!\n", temp, eagainLoopCount);
    	  }
      }
      printSecondMicrosecondNow();
	  sprintf(msg, "[%s, %s] WriteSockMsg() on socket %d: ", JDtoString(0), getSecondMicrosecondNow(), sock);
      perror(msg);
      return -1;
    } else   if (temp == 0) {
      writeZeros++;
  	  printf("WriteSockMsg() writing returned 0 the %d time!\n", writeZeros);
  	  if (writeZeros == WRITE_LOOP_LIMIT) {
  		  printf("WriteSockMsg() writing returned 0 the %d time! => writing aborted, returning 0\n", writeZeros);
  	      return rest;
  	  }
    }
    rest -= temp;
  }
 /*
  printf("%s - USE_AERA_TRANSPORT_DEFINITIONS is defined, writing AERA_MAGIC_CHARS\n", __FUNCTION__);
  temp = write(sock,(char *)AERA_MAGIC_CHARS,sizeof(AERA_MAGIC));
  if (temp < 0) {	// do not sent these few bytes in a loop, just react on problems
	sprintf(msg, "[%s,%d] WriteSockMsg() on socket %d: ", JDtoString(0), (int)time(NULL), sock);
    perror(msg);
    return -1;
  } else {
	  printf("%s - writing AERA_MAGIC_CHARS returned %d \n", __FUNCTION__, temp);
  }
 */

  return rest;
}
#else	// COPY_MSG_ON_WRITE
int WriteSockMsg ( int sock, Msg *sendmsg )
{
  int rest,temp, writeZeros = 0, totMsgLength;
  fd_set writefds;
  char msg[256];
  int maxEAGAINloops = 20;

  if (!sendmsg->length) return 0;
  int orgMsgLength = sendmsg->length;				// this is the original message length. It is increased cause the AERA_MAGIC is added, but has to be restored, cause the Msg may be used by application afterwards!

  rest = TotalMsgLength(sendmsg->length);
  totMsgLength = TotalMsgLength(sendmsg->length);
  if (!sock) return rest;

  if (printedAeraTransportDefined == 0) {
	  printf("%s - USE_AERA_TRANSPORT_DEFINITIONS is defined!\n", __FUNCTION__);
	  printedAeraTransportDefined = 1;
  }
  printf("%s - USE_AERA_TRANSPORT_DEFINITIONS is defined - sendmsg->length = %d has to be increased by sizeof(AERA_MAGIC) = %d\n", __FUNCTION__, sendmsg->length,sizeof(AERA_MAGIC));
  sendmsg->length += sizeof(AERA_MAGIC);
//  printf("%s - sendmsg->length = %d after increasing\n", __FUNCTION__, sendmsg->length);

/* AW: added for protocol debug */
//  printf("WriteSockMsg(): writing message with TotalMsgLength %d (length before %d) to socket %d\n", rest, sendmsg->length, sock);fflush(stdout);
//  printf("WriteSockMsg(): msg length = %d, message points to %d \n", sendmsg->length, (int*)sendmsg);

  FD_ZERO(&writefds);
  FD_SET(sock, &writefds);
  int n, eagainLoopCount = 0;
  struct timeval timeout;
  timeout.tv_sec = 0;
  timeout.tv_usec =  WRITE_TIMEOUT_US;
  while(rest) {
// as write may block on connection problems, call again select before each write, but only if SO_SNDTIMEO is not set
	if (socketSndTimeoutInUse != 1) {
		n = select(sock+1,NULL,&writefds,NULL,&timeout);
		if (!n) {		// error or timeout
		  sprintf(msg, "[%s, %s] WriteSockMsg() - select on socket %d: ", JDtoString(0), getSecondMicrosecondNow(), sock);
		  perror(msg);
		  sendmsg->length = orgMsgLength;				// restore the original message length. It was increased cause the AERA_MAGIC is added, but has to be restored, cause the Msg may be used by application afterwards!
	//	  printf("%s - socket %d not ready to write on writing rest bytes = %d of total %d bytes!!! Returning NULL\n", __FUNCTION__, sock, rest, sendmsg->length);
		  return -1;
		}
	} // else printf("not using select, socket has a SO_SNDTIMEOUT set\n");
    temp = write(sock,(char *)sendmsg + totMsgLength - rest,MIN(rest,MAXBUFF_UNIX));
    if (temp < 0) {
      if (errno == EAGAIN) {
    	  if (eagainLoopCount < maxEAGAINloops) {
  		  printf("WriteSockMsg() writing returned %d == EAGAIN => trying again the %d time ...\n", temp, eagainLoopCount);
    		  eagainLoopCount++;
 //   		  usleep(100);
    		  continue;
    	  } else {
   		 	  printf("WriteSockMsg() writing returned %d == EAGAIN the %d time => taking it as an error!\n", temp, eagainLoopCount);
    	  }
      }
	  sprintf(msg, "[%s, %s] WriteSockMsg() on socket %d: ", JDtoString(0), getSecondMicrosecondNow(), sock);
      perror(msg);
		  sendmsg->length = orgMsgLength;				// restore the original message length. It was increased cause the AERA_MAGIC is added, but has to be restored, cause the Msg may be used by application afterwards!
      return -1;
    } else   if (temp == 0) {
      writeZeros++;
//  	  printf("WriteSockMsg() writing returned 0 the %d time!\n", writeZeros);
  	  if (writeZeros == WRITE_LOOP_LIMIT) {
  		  printf("WriteSockMsg() writing returned 0 the %d time! => writing aborted, returning 0\n", writeZeros);
		  sendmsg->length = orgMsgLength;				// restore the original message length. It was increased cause the AERA_MAGIC is added, but has to be restored, cause the Msg may be used by application afterwards!
  	      return rest;
  	  }
    }
    rest -= temp;
  }
  if (writeAeraMagic(sock) < 0) {
	  printf("%s - error writing AERA magic to socket %d\n", __FUNCTION__, sock);
	  sendmsg->length = orgMsgLength;				// restore the original message length. It was increased cause the AERA_MAGIC is added, but has to be restored, cause the Msg may be used by application afterwards!
	  return -1;
  }
 /*
  printf("%s - USE_AERA_TRANSPORT_DEFINITIONS is defined, writing AERA_MAGIC_CHARS\n", __FUNCTION__);
  temp = write(sock,(char *)AERA_MAGIC_CHARS,sizeof(AERA_MAGIC));
  if (temp < 0) {	// do not sent these few bytes in a loop, just react on problems
	sprintf(msg, "[%s,%d] WriteSockMsg() on socket %d: ", JDtoString(0), (int)time(NULL), sock);
    perror(msg);
    return -1;
  } else {
	  printf("%s - writing AERA_MAGIC_CHARS returned %d \n", __FUNCTION__, temp);
  }
 */

  sendmsg->length = orgMsgLength;				// restore the original message length. It was increased cause the AERA_MAGIC is added, but has to be restored, cause the Msg may be used by application afterwards!
  return rest;
}
#endif		// COPY_MSG_ON_WRITE
#endif		// USE_AERA_TRANSPORT_DEFINITIONS




/**
 * @brief test if the given socket is ready for writing - just calls select() internally with a timeout of {0,0}
 * @param sock the socket to test for writing
 */
int TestSocketForWrite(int sock)
{
  struct timeval timeout = {0,0};
  fd_set writefds;
  FD_ZERO(&writefds);
  FD_SET(sock,&writefds);

  if (sock) return select(sock+1,NULL,&writefds,NULL,&timeout);
  return 0;
}

/**
 * @brief test if the given socket is ready for reading - just calls select() internally with a timeout of {0,0}
 * @param sock the socket to test for reading
 */
int TestSocketForRead(int sock)
{
  struct timeval timeout = {0,0};
  fd_set readfds;
  FD_ZERO(&readfds);
  FD_SET(sock,&readfds);

  if (sock) return select(sock+1,&readfds,NULL,NULL,&timeout);
  return 0;
}

char *JDtoString(time_t mytime)
{
  static char ret[256];
  time_t ti;
  struct tm *timestruct;

  if (!mytime) ti = time(NULL);
  else ti = mytime;
  timestruct = localtime(&ti);
  strftime(ret,14,"%y%m%d_%H%M%S",timestruct);		/* AW: converting timestruct to a string */
  return ret;
}

// #########   SAME FUNCTIONS BUT USING SETSOCKOPT()+RECV() INSTEAD OF COMBINATION SELECT()+READ()
// as an attempt to increase performance, replace the combination select()+read() with recv() and use the recv() timeout option
/**
 * @brief reads a message from the given socket, to avoid blocking on a blocking socket, select() with the given timeout is called before each read attempt
 * @param sock the sock_fd to read from
 * @param time the timeout used for select() - call preceeding the read() - call in micro seconds
 * @return returns NULL on errors or a pointer to the static memory with the read Msg. This memory is re-used, so be sure, to process the message before calling the function again!!! The previous message will be overwritten!!!
 */
Msg * RecvSockMsgSM ( int sock )
{
  int temp,rest, readZeros=0, blockCount=0;
  MSGTYPE_LENGTH length;
  Msg *retmsg;
  fd_set readfds;
  FD_ZERO(&readfds);
  FD_SET(sock,&readfds);

  temp = recv(sock,(char *)&length,sizeof(length), 0);

  if (temp <= 0) { /* '=' means EOF */
    perror("RecvSockMsgSM_TO() ");
    return NULL;
  } else   if (temp == 0) {
	  printf("RecvSockMsgSM_TO() reading Msg-length returned 0! Reading Msg aborted, returning NULL\n");
	  return NULL;
  } else if (temp != sizeof(length)) {
	  printf("RecvSockMsgSM_TO() reading Msg-length failed, read returned only %d bytes instead of %d! Reading Msg aborted, returning NULL\n", temp, sizeof(length));
  }

  rest = length;
//  retmsg = (Msg *) malloc(rest+sizeof(MSGTYPE_LENGTH));
  cleanMsgRcvBuff();
  retmsg = (Msg *) msgRcvBuff;		// use static memory

  retmsg->length = length;

  while(rest) {
	blockCount++;
// this recv may block, if SO_RCVTIMEO is not set
    temp = recv(sock, retmsg->data + length - rest,MIN(rest,MAXBUFF_UNIX),0);
    if (temp < 0) {
    	int sockerr = errno;
      perror("RecvSockMsgSM_TO() ");
      if (sockerr == EINTR)  {
    	  printf("reading returned %d, errno = %d = EINTR => trying again ...\n", temp, sockerr);
    	  continue;
      } else return NULL;
    } else   if (temp == 0) {
      readZeros++;
  	  printf("RecvSockMsgSM_TO() reading Msg-length returned 0 the %d time!\n", readZeros);
  	  if (readZeros == READ_LOOP_LIMIT) {
  		  printf("RecvSockMsgSM_TO() reading Msg-length returned 0 the %d time! => reading aborted, returning NULL\n", readZeros);
  	      return NULL;
  	  }
    } else rest -= temp;

//    printf("RecvSockMsgSM_TO() read block no. %d with length of %d bytes of message with total length %d, %d bytes of rest missing from socket %d\n", blockCount, temp, length, rest, sock);
  }
  return retmsg;
}


// the following is disabled and not used!!!!!
#ifdef NOT_USED
void TextPrint(int *logSocket,char *fmt, ...)		/* AW: writing message with time to log socket or file and stderr */
{
  static char buffer[1024];
  Tag *tag;
  SHTAGTYPE_LENGTH length;
  va_list ap;
  int i,look_at_me = false;

  va_start(ap,fmt);
  length = (TAGTYPE_LENGTH)vsprintf(buffer,fmt,ap)+1;
  va_end(ap);

  printf("TextPrint(): Content of buffer is: >>%s<< (length = %d?)\n", buffer, length);	/* AW: added for debug */

  tag = setTag(0,ALIGN(length+sizeof(SHTAGTYPE_LENGTH),int),TEXT,ARG_END);
  memcpy(tag->data,buffer,length);
  for (i=0;i<MAX_HOST_CLIENTS;i++) {
    if (logSocket[i]) {									/* AW: test, if socket handle != 0 */
      look_at_me = true;
      if (!AddBuffer((Msg *)tag,logbuffer[i])) {
        CloseBuffer(logbuffer[i]);
        WriteSockMsg(logSocket[i],(Msg *)logbuffer[i]);
        ResetBuffer(logbuffer[i]);
        AddBuffer((Msg *)tag,logbuffer[i]);
      }

/* AW: replacing buffer handling with my version 2 */
/*	printf("TextPrint() printing()...\n");
      if (!AddBufferAW2((Msg *)tag,logbuffer[i])) {
        CloseBufferAW2(logbuffer[i]);
        WriteSockMsg(logSocket[i],(Msg *)GetBufferAW2(logbuffer[i]));
        ResetBufferAW2(logbuffer[i]);
        AddBufferAW2((Msg *)tag,logbuffer[i]);
      }
*/
/* AW: replacing buffer handling with my version */
/*	printf("TextPrint() printing()...\n");
      if (!AddBufferAW((Msg *)tag,logbuffer[i])) {
        CloseBufferAW(logbuffer[i]);
        WriteSockMsg(logSocket[i],(Msg *)logbuffer[i]);
        ResetBufferAW(logbuffer[i]);
        AddBufferAW((Msg *)tag,logbuffer[i]);
      }
*/    }
  }
  if (!look_at_me) {									/* AW: no valid log socket */
    if (evb_log) fprintf(evb_log,"%s",buffer);			/* AW: if log file exists, write to file */
    fprintf(stderr,"%s",buffer);						/* AW: write to standard error output stderr (terminal) */
  }
  free(tag);
}

void TextPrintWithGT(int *logSocket,char *fmt, ...)		/* AW: writing message with time to log socket or file and stderr */
{
  static char buffer[1024];
  Tag *tag;
  SHTAGTYPE_LENGTH length;
  va_list ap;
  int i,p = 0,look_at_me = false;
/* AW: added BEGIN */
	int j;
  char *cp;
  char ca[50];
/* AW: added END */

  p = sprintf(buffer,"[%s,%d: %s] ",JDtoString(0),time(NULL), getSecondMicrosecondNow());
  va_start(ap,fmt);
  length = vsprintf(buffer+p,fmt,ap)+p+1;
  va_end(ap);
  printf("TextPrintWithGT(): Content of buffer is: >>%s<< (length = %d?)\n", buffer, length);	/* AW: added for debug */
/*  for (i=length-5;i<length;i++) printf("Buffer[%2d]:>>%c<< as int: %d\n", i, buffer[i], (int)buffer[i]);*/

  tag = setTag(0,ALIGN(length+sizeof(SHTAGTYPE_LENGTH),int),TEXT,ARG_END);
/*  tag = setTag(0,length+sizeof(SHTAGTYPE_LENGTH),TEXT,ARG_END);*/
  memcpy(tag->data,buffer,length);

/* AW: added BEGIN */


/*	debugMsg(MSG_PROTOCOL, "Sizeof(Msg) is : %d\n", sizeof(Msg));
	debugMsg(MSG_PROTOCOL, "Offset of TAGTYPE_LENGTH in Msg is : %d\n", offsetof(Msg, length));
	debugMsg(MSG_PROTOCOL, "Offset of data[1] in Msg is : %d\n", offsetof(Msg, data));
	debugMsg(MSG_PROTOCOL, "\n");
	debugMsg(MSG_PROTOCOL, "Sizeof(SHTag) is : %d\n", sizeof(SHTag));
	debugMsg(MSG_PROTOCOL, "Offset of SHTAGTYPE_LENGTH in SHTag is : %d\n", offsetof(SHTag, length));
	debugMsg(MSG_PROTOCOL, "Offset of tagno in SHTag is : %d\n", offsetof(SHTag, tagno));
	debugMsg(MSG_PROTOCOL, "Offset of data[1] in SHTag is : %d\n", offsetof(SHTag, data));
	debugMsg(MSG_PROTOCOL, "\n");
/* my own prepared message */
/*	if (tag->length == 40) {
		debugMsg(MSG_PROTOCOL, "TextPrintWithGT(): analyzing Tag of length %d (as byte array):\n", tag->length);
		cp = (char *) tag;
		for (i=0; i < (tag->length + sizeof(TAGTYPE_LENGTH)); i++) {
			ca[i] = * (cp + i*sizeof(char));
		}
		debugMsg(MSG_PROTOCOL, "TextPrintWithGT(): length is: %d\n", (short) ca[0]);
		debugMsg(MSG_PROTOCOL, "TextPrintWithGT(): tagno is: %d\n", (int) ca[2]);
		debugMsg(MSG_PROTOCOL, "TextPrintWithGT(): cluster is: %d\n", (int) ca[4]);
		debugMsg(MSG_PROTOCOL, "TextPrintWithGT(): data[0] is: %d\n", (int) ca[8]);
		for (j=0; j < (tag->length + sizeof(TAGTYPE_LENGTH)); j++) {
			debugMsg(MSG_PROTOCOL, "TextPrintWithGT(): data[%2d] is: >>%c<< (as char) = >>%d<< (as char)\n", j, ca[j], ca[j]);
		}
		fflush(stdout);
/*		debugMsg("LogCB: ...message read, analyzing as Tag:\n");
		debugMsg("LogCB: Tag length is: %d\n", tag->length);
		debugMsg("LogCB: Tag tagno is: %d\n", tag->tagno);
		debugMsg("LogCB: Tag cluster is: %d\n", tag->cluster);
		cp = (char*) tag->data;
		debugMsg("LogCB: Tag first data is as char: %c)\n", *cp);
		debugMsg("\n");
		debugMsg("Sizeof(Tag) is : %d\n", sizeof(Tag));
		debugMsg("Offset of TAGTYPE_LENGTH in Tag is : %d\n", offsetof(Tag, length));
		debugMsg("Offset of TAGTYPE_TAGNO in Tag is : %d\n", offsetof(Tag, tagno));
		debugMsg("Offset of CLUSTER_TYPE in Tag is : %d\n", offsetof(Tag, cluster));
		debugMsg("Offset of data[1] in Tag is : %d\n", offsetof(Tag, data));

	}
*/
/* AW: added END */

  for (i=0;i<MAX_HOST_CLIENTS;i++) {
    if (logSocket[i]) {									/* AW: test, if socket handle != 0 */
/*	  printf("TextPrintWithGT(): printing message of text length %d to log socket %d\n", length, logSocket[i]);	/* AW: added for debug */
/*	  fflush(stdout);	/* AW: added */
/*      WriteSockMsg(logSocket[i],(Msg *)tag);	/* AW: added, there is something wrong with the following buffering, so skip */
      look_at_me = true;
/* AW: replacing buffer handling with my version 2 */
/*	printf("TextPrint() printing()...\n");
      if (!AddBufferAW2((Msg *)tag,logbuffer[i])) {
        CloseBufferAW2(logbuffer[i]);
        WriteSockMsg(logSocket[i],(Msg *)GetBufferAW2(logbuffer[i]));
        ResetBufferAW2(logbuffer[i]);
        AddBufferAW2((Msg *)tag,logbuffer[i]);
      }
*/
/* AW: using modified versions of buffer methods */
/*      if (!AddBufferAW((Msg *)tag,logbuffer[i])) {
        CloseBufferAW(logbuffer[i]);
        WriteSockMsg(logSocket[i],(Msg *)logbuffer[i]);
        ResetBufferAW(logbuffer[i]);
        AddBufferAW((Msg *)tag,logbuffer[i]);
      }
*/
/* AW: this is the DM version */
      if (!AddBuffer((Msg *)tag,logbuffer[i])) {
        CloseBuffer(logbuffer[i]);
        WriteSockMsg(logSocket[i],(Msg *)logbuffer[i]);
        ResetBuffer(logbuffer[i]);
        AddBuffer((Msg *)tag,logbuffer[i]);
      }

    }
  }
  if (!look_at_me) {									/* AW: no valid log socket */
    if (evb_log) fprintf(evb_log,"%s",buffer);			/* AW: if log file exists, write to file */
    fprintf(stderr,"%s",buffer);						/* AW: write to standard error output stderr (terminal) */
  }
  free(tag);
}


void SendStateSock(TAGTYPE_TAGNO tagno, int client)
{
  Eventinterface *up;

  up = setEventinterface(0,tagno,ARG_END);
  WriteSockMsg(client,(Msg *)up);
  free(up);
}

int TestAndRepairStream(int sock)
{
  int temp;
  int ok = false;
  Tag tag;
  char shift_byte;

  while (!ok) {
    temp = recv(sock,(char *)&tag,sizeof(Tag)-sizeof(int),MSG_PEEK);

    if (temp <= 0) { /* '=' means EOF */
      perror("ReadSockMsg() ");
      return false;
    }

    if (!tag.cluster) TextPrint(evbsockets.log,"Cluster=0 in Tag %d!\n",tag.tagno);
    else if (runmask && !InMask(&tag,resourcemask)) {
      ok = false;
      TextPrint(evbsockets.log,"InMask!\n");
      if (read(sock,&shift_byte,1) == -1) return false;
      continue;
    }
    if (!InTagNo(&tag)) {
      ok = false;
      TextPrint(evbsockets.log,"TagNo!\n");
      if (read(sock,&shift_byte,1) == -1) return false;
      continue;
    }
    if ((tag.length > 65535) || (tag.length < 3)) {
      ok = false;
      TextPrint(evbsockets.log,"Malloc()!\n");
      if (read(sock,&shift_byte,1) == -1) return false;
      continue;
    }
    ok = true;
  }
  return true;
}

Msg *ReadSockMsgWithTextFilter(int ls)
{
        Tag *tag;
        tag = (Tag *)ReadSockMsg(ls);
        if (!tag) return NULL;
        while (tag->tagno == TEXT) {
                printf("%s",tag->data);
                free(tag);
                tag = (Tag *)ReadSockMsg(ls);
                if (!tag) return NULL;
        }
        return (Msg *)tag;
}


int TestRSocket(int Socket)
{
  Msg *sendmsg;
  if ((sendmsg = ReadSockMsgTO(Socket,0))) {
    printf("tagno %d link %d\n",((Tag *)sendmsg)->tagno,((Tag *)sendmsg)->cluster-1);
    free(sendmsg);
    return 1;
  }
  return 0;
}
#endif		// NOT_USED
