#ifndef __EIGHDR
#define __EIGHDR

/*-- Header-Datei, die alle wichtigen System-Header-Dateien included und ----*/
/*--               wichtige Konstanten und Makros definiert              ----*/
/*--    (sollte  nach  allen System-Header-Dateien included werden)      ----*/

#include  <sys/types.h>
#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  <unistd.h>

#define  MAX_ZEICHEN   4096    /*--- Maximale Pufferlaenge */

#define  WARNUNG      0   /*--- Kennungen fuer unterschiedl. Fehlerarten */
#define  WARNUNG_SYS  1
#define  FATAL        2
#define  FATAL_SYS    3
#define  DUMP         4

extern int  debug; /* Aufrufer von log_meld oder log_open muss debug setzen:
                      0, wenn interaktiv; 1, wenn Daemon-Prozess */

/*------------ Nuetzliche Makros --------------------------------------*/
#define min(x,y)    ((x) < (y) ? (x) : (y))
#define max(x,y)    ((x) > (y) ? (x) : (y))

/*------------ Eigene Typdefinitionen ---------------------------------*/
//typedef enum  { FALSE=0, TRUE=1 }  bool;
typedef void  sigfunk(int);  /* Datentyp fuer Signalhandler */

/*------------ Zentrale Fehlerroutinen --------------------------------*/
extern void    error_mess(int kennung, const char *fmt,...);
extern void    log_mess(int kennung, const char *fmt,...);

/*------------ log_open -------------------------------------------------
                     initialisiert syslog() bei einem Daemon-Prozess   */
extern void    log_open(const char *kennung, int option, int facility);

/*---------- Synchronisation-Routinen ---------------------------------*/
extern void  INIT_SYNCH(void);      /* Synchronisation initialisieren       */
extern void  HALLO_PAPA(pid_t pid); /* Kind signal. Elternpr., dass fertig  */
extern void  WARTE_AUF_PAPA(void);  /* Kind wartet auf Signal vom Elternpr. */
extern void  HALLO_KIND(pid_t pid); /* Elternpr. signal. Kind, dass fertig  */
extern void  WARTE_AUF_KIND(void);  /* Elternpr. wartet auf Signal vom Kind */

/*------------- Funktionen aus sperre.c -------------------------------*/
extern int  sperre_einaus(int fd, int kdo, int sperr_typ,
                          off_t offset, int wie, off_t laenge);
extern pid_t  sperre_testen(int fd, int sperr_typ,
                            off_t offset, int wie, off_t laenge);

/*------------ Einrichten einer Sperre ----------------------------------*/
#define lese_sperre(fd,offset,wie,laenge) \
           sperre_einaus(fd, F_SETLK, F_RDLCK, offset, wie, laenge)
#define lesewarte_sperre(fd,offset,wie,laenge) \
           sperre_einaus(fd, F_SETLKW, F_RDLCK, offset, wie, laenge)
#define schreib_sperre(fd,offset,wie,laenge) \
           sperre_einaus(fd, F_SETLK, F_WRLCK, offset, wie, laenge)
#define schreibwarte_sperre(fd,offset,wie,laenge) \
           sperre_einaus(fd, F_SETLKW, F_WRLCK, offset, wie, laenge)

/*------------ Aufheben einer Sperre ------------------------------------*/
#define sperre_aufheben(fd,offset,wie,laenge) \
           sperre_einaus(fd, F_SETLK, F_UNLCK, offset, wie, laenge)

/*------------ Testen einer Sperre --------------------------------------*/
#define lesesperre_vorhanden(fd,offset,wie,laenge) \
           sperre_testen(fd, F_RDLCK, offset, wie, laenge)
#define schreibsperre_vorhanden(fd,offset,wie,laenge) \
           sperre_testen(fd, F_WRLCK, offset, wie, laenge)

#endif
