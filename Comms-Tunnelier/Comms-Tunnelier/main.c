#include "port_settings.h"
#include "id.h"

/* Shared */
#include "tunlib.h"
#include "clientsock.h"
#include "timer.h"
#include "linux_process_ctrl.h"
#include "clientsock.h"
#include "sniffer.h" //for ip packet printout
#include "comms_signal.h"
#include "socklib/netprog.h"

#define BUFSIZE 2000

static char* Version = "2.3"; //new clientsock.c in library, should solve CLOSE_WAIT problem; fixed a bug of false port, fixed a bug if timeout

static struct connCliInfo IPConn;
struct performanceTimer PTimer;
struct performanceTimer LatencyTimer;

int tun_fd;
static int tun2net = 0, net2tun = 0;

static void exit_program(int signo)
{
	long diff = snapshot_performanceTimer(&PTimer, true, stdout); printf("Program exit, runtime: %ld seconds\n", diff);
	close(tun_fd);
	if (IPConn.socket != 0) close(IPConn.socket);
	printf("Tun2net: %d, Net2tun: %d\n", tun2net, net2tun);
	exit(EXIT_SUCCESS);
}

/**************************************************************************
 * usage: prints usage and exits.                                         *
 **************************************************************************/
static void usage(char *s)
{
	static char *usage_text  = "\
    -t just test TUN\n\
	-D extra debug for ip packet\n\
	-k kill existing processes\n\
	-h this help text\n\
	";

	printf("*********************************\n");
	printf("*Comms-Tunnelier, ver %s *\n", Version);
	printf("*********************************\n");
	printf("usage: %s [options]\n", s);
    printf("%s", usage_text);
}

int main(int argc, char **argv)
{
	extern char *optarg;
	extern int optind;
	int c;
	int test_tun = 0;
	int extra_debug = 0;

	//parse command line
	/* strip the path from app-name to "pname" */
    char *pch = strtok(argv[0], "./~");
    char *pname = pch;
    while (pch != NULL) {
    	pname = pch;
    	pch = strtok(NULL, "./~"); //continues
    }
    int pid_list[10];
    int pid_list_len = 10;

    while((c = getopt(argc, argv, "tDkh")) > 0) {
      switch(c) {
      	  case 't':
      		  test_tun = 1;
      		  break;
      	  case 'D':
      		  extra_debug = 1;
      		  break;
		  case 'k':
			  if (getProcessID(pname, pid_list, &pid_list_len) >= 0) {
				  if (pid_list_len > 0) {
					  printf("%d existing processes found\n", pid_list_len-1);
					  killProcessID(pid_list, pid_list_len-1);
				  }
			  }
			exit(EXIT_SUCCESS);
			break;
      	  case 'h':
      	  default: usage(pname); exit(0);
      }
    }
    reset_performanceTimer(&PTimer, true, stdout);

    assign_signal(SIGINT, exit_program); //ctrl-c signal

	char if_name[16] = "tun0";

    /* initialize tun/tap interface */
    if ( (tun_fd = tun_alloc(if_name, IFF_TUN|IFF_NO_PI)) < 0 ) { //without IFF_NO_PI, no more pure ip-packets but 4 bytes additional info at the beginning
    	fprintf(stderr, "Error connecting to tun interface %s!\n", if_name);
    	exit(EXIT_FAILURE);
    }
    printf("Successfully connected to TUN interface\n");

	/* get SBC ip */
    int id;
	FILE *fp;
	char line[200];                               // dummy name and LS_id
	fp = fopen("./ip_sbc.conf","r"); //same directory
	if (fp == NULL) {
		fprintf(stderr, "ip_sbc.conf not found under current folder, supposed i'm LS --> connect to localhost!\n");
		sprintf(line, "127.0.0.1");
		id = get_myId();
	} else {
		if (fgets(line,200,fp) == NULL) {
			fprintf(stderr, "ip_sbc.conf blank!\n");
			exit(EXIT_FAILURE);
		}
		id = SBC_ID;
	}
	printf("ip is %s, my ID is %d\n", line, id); //line has the ip

	setup_tun(if_name, id, (540-4)); //IP segment biggest: 536

    if (test_tun == 1) {
        while(1) {} //inendless
    }

	initConnCliInfo(&IPConn, "IP_CONN", line, PORT_NUMBER_IP);

	int loopCount = 0;
	struct timeval timeout;

	int ret;
	fd_set rd_set;
	int maxfd;
	uint16_t nread, nwrite, plength;
	char buffer[BUFSIZE];
    while(1) {
		// loop to recover
		if (loopCount % 100 == 0) {
			recoverConnectionIfLost(&IPConn);
		}

    	FD_ZERO(&rd_set);
    	FD_SET(tun_fd, &rd_set);
    	FD_SET(IPConn.socket, &rd_set);

		maxfd = (tun_fd > IPConn.socket)?tun_fd:IPConn.socket;

		timeout.tv_sec = 0;
		timeout.tv_usec = 250000; //250ms
    	ret = select(maxfd + 1, &rd_set, NULL, NULL, &timeout);
		if(FD_ISSET(tun_fd, &rd_set)) {
			// data from tun/tap: just read it and write it to the network
			nread = cread(tun_fd, buffer, BUFSIZE);

			// write length + packet to network
			sendSimpleSockClient(&IPConn, (unsigned char*)buffer, nread);

			tun2net++;
			// debug
			if (extra_debug) {
				printf("Tun --> Net, len: %d, tot: %d\n", nread, tun2net);
				ProcessPacketOut((unsigned char*)buffer, nread);
			}
		}

		if(FD_ISSET(IPConn.socket, &rd_set)) {
			// data from network: just read length, then packet
			plength = BUFSIZE; //maximal BUFSIZE
			if (recvSimpleSockClient(&IPConn, (unsigned char*)buffer, &plength) >= 0) {
				//  write packet to tun/tap
				nwrite = cwrite(tun_fd, buffer, plength);

				net2tun++;
				// debug
				if (extra_debug) {
					printf("Net --> Tun, length: %d, tot: %d\n", nwrite, net2tun);
					ProcessPacket((unsigned char*)buffer, nwrite);
				}
			}
		}

		loopCount++;
    }
}
