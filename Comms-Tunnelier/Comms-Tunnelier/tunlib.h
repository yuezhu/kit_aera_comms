/*
 * tunlib.h
 *
 *  Created on: Dec 14, 2012
 *      Author: dev
 */

#ifndef TUNLIB_H_
#define TUNLIB_H_

#include <linux/if_tun.h>

int tun_alloc(char *dev, int flags);

int setup_tun(char *dev, int id, int mtu);

#endif /* TUNLIB_H_ */
