/*
 * tunlib.c, edited from simpletun.c
 *
 *  Created on: Dec 14, 2012
 *      Author: dev
 */

/**************************************************************************
 * simpletun.c                                                            *
 *                                                                        *
 * A simplistic, simple-minded, naive tunnelling program using tun/tap    *
 * interfaces and TCP. DO NOT USE THIS PROGRAM FOR SERIOUS PURPOSES.      *
 *                                                                        *
 * You have been warned.                                                  *
 *                                                                        *
 * (C) 2010 Davide Brini.                                                 *
 *                                                                        *
 * DISCLAIMER AND WARNING: this is all work in progress. The code is      *
 * ugly, the algorithms are naive, error checking and input validation    *
 * are very basic, and of course there can be bugs. If that's not enough, *
 * the program has not been thoroughly tested, so it might even fail at   *
 * the few simple things it should be supposed to do right.               *
 * Needless to say, I take no responsibility whatsoever for what the      *
 * program might do. The program has been written mostly for learning     *
 * purposes, and can be used in the hope that is useful, but everything   *
 * is to be taken "as is" and without any kind of warranty, implicit or   *
 * explicit. See the file LICENSE for further details.                    *
 *************************************************************************/

#include "tunlib.h"

#include <net/if.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>

/**************************************************************************
 * tun_alloc: allocates or reconnects to a tun/tap device. The caller     *
 *            must reserve enough space in *dev.                          *
 **************************************************************************/
int tun_alloc(char *dev, int flags) {

  struct ifreq ifr;
  int fd, err;
  char *clonedev = "/dev/net/tun";

  if( (fd = open(clonedev , O_RDWR)) < 0 ) {
    perror("Opening /dev/net/tun");
    return fd;
  }

  memset(&ifr, 0, sizeof(ifr));

  ifr.ifr_flags = flags;

  if (*dev) {
    strncpy(ifr.ifr_name, dev, IFNAMSIZ);
  }

  if( (err = ioctl(fd, TUNSETIFF, (void *)&ifr)) < 0 ) {
    perror("ioctl(TUNSETIFF)");
    perror("not started by *sudo* or by root? ");
    close(fd);
    return err;
  }

  int opt = 0;
  if( (err =  ioctl(fd, FIONBIO, &opt)) < 0 ) {
    perror("ioctl(FIONBIO), set to block read fail");
    close(fd);
    return err;
  }

  //strcpy(dev, ifr.ifr_name);

  return fd;
}

int run_command(char* command)
{
	FILE *shellCommand;
	printf("---- command: %s\n", command);
	shellCommand = popen(command, "r");
	if (shellCommand == NULL) {
		fprintf(stderr, "%s: command \"%s\" cannot be executed or other errors\n", __FUNCTION__, command);
		return -1;
	}
	pclose(shellCommand);
	return 0;
}
//used to register tun0 to ifconfig after successful tun_alloc
int setup_tun(char *dev, int id, int mtu)
{
	char command[100];

	sprintf(command, "ifconfig %s 2.2.2.%d", dev, id);
	if (run_command(command) < 0) return -1;

	sprintf(command, "route add -net 2.2.2.0/24 dev %s", dev);
	if (run_command(command) < 0) return -1;

	sprintf(command, "ifconfig %s mtu %d", dev, mtu);
	if (run_command(command) < 0) return -1;

	return 0;
}

/*
int main(int argc, char *argv[]) {

  int tap_fd, option;
  int flags = IFF_TUN;
  int maxfd;
  uint16_t nread, nwrite, plength;
  char buffer[BUFSIZE];
  struct sockaddr_in local, remote;
  char remote_ip[16] = "";            // dotted quad IP string
  unsigned short int port = PORT;
  int sock_fd, net_fd, optval = 1;
  socklen_t remotelen;
  int cliserv = -1;    // must be specified on cmd line
  unsigned long int tap2net = 0, net2tap = 0;

  progname = argv[0];

  argv += optind;
  argc -= optind;

  if(argc > 0) {
    my_err("Too many options!\n");
    usage();
  }

  int tun_fd;
  // initialize tun/tap interface
  if ( (tun_fd = tun_alloc("tunMsgBridge", IFF_TUN | IFF_NO_PI)) < 0 ) {
    my_err("Error connecting to tun interface %s!\n", if_name);
    exit(1);
  }

  if(cliserv == CLIENT) {
	// Client, try to connect to server

	// assign the destination address
    memset(&remote, 0, sizeof(remote));
    remote.sin_family = AF_INET;
    remote.sin_addr.s_addr = inet_addr(remote_ip);
    remote.sin_port = htons(port);

    // connection request
    if (connect(sock_fd, (struct sockaddr*) &remote, sizeof(remote)) < 0) {
      perror("connect()");
      exit(1);
    }

    net_fd = sock_fd;
    do_debug("CLIENT: Connected to server %s\n", inet_ntoa(remote.sin_addr));

  } else {
	// Server, wait for connections

	// avoid EADDRINUSE error on bind()
    if(setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, (char *)&optval, sizeof(optval)) < 0) {
      perror("setsockopt()");
      exit(1);
    }

    memset(&local, 0, sizeof(local));
    local.sin_family = AF_INET;
    local.sin_addr.s_addr = htonl(INADDR_ANY);
    local.sin_port = htons(port);
    if (bind(sock_fd, (struct sockaddr*) &local, sizeof(local)) < 0) {
      perror("bind()");
      exit(1);
    }

    if (listen(sock_fd, 5) < 0) {
      perror("listen()");
      exit(1);
    }

    // wait for connection request
    remotelen = sizeof(remote);
    memset(&remote, 0, remotelen);
    if ((net_fd = accept(sock_fd, (struct sockaddr*)&remote, &remotelen)) < 0) {
      perror("accept()");
      exit(1);
    }

    do_debug("SERVER: Client connected from %s\n", inet_ntoa(remote.sin_addr));
  }



  return(0);
}
*/
