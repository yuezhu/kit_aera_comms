/** @file myid.c
 *  @brief get_myId() implementation.
 *
 *  @author Y.Zhu
 *  @bug No known bugs.
 */

#include "id.h"
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

bool Is_station_ID_loaded = false;
staId_t station_ID;

int get_conf_data(int *id)
{
	FILE *fp;
	char ls_name[100];
	char line[200], ls_number[20];
	strcpy(ls_name,"dummy");                                 // dummy name and LS_id
	*id = 255;

	fp = fopen("/mnt/LS.conf","r");
	if (fp == NULL) return 0;
	while (line==fgets(line,199,fp)) {              // read all lines
		if ( line[0] =='/' ) continue;				//comments start with /
		if ((line[0] == '#')) {                         // lines starting with # are usually comments in NL stations
														// for DE stations lines start with #
			if(strncmp(line,"#Name:",6) == 0){            // lines with #Name: they are followed by name and id
				sscanf(&(line[6]),"%s %s",ls_name,ls_number);
				*id=atoi(ls_number);
				sprintf(ls_name,"%s%s",ls_name,ls_number);
				return 1; //found jump out
			}
		}
	}
	return 0; //no found
}

staId_t get_myId()
{
	int id;
	if (Is_station_ID_loaded == false) {
		if(!get_conf_data(&id)){
			fprintf(stderr, "error reading /mnt/LS.conf or no ID there, exiting! \n");
			exit(EXIT_FAILURE);
		} else {
			Is_station_ID_loaded = true;
			station_ID = (staId_t)id;
			printf("Station ID: %d\n", station_ID);
			return station_ID;
		}
	}
	else {
		return station_ID;
	}
}
