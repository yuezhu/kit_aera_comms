/** @file datap_io.c
 *  @brief io class for data packet in-out from and to the base station, function implementation
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include "def.h"

#include "datap_io.h"
#include "io.h"
#include "tdma_protocol.h" /* for MAX_LS_DATA_LENGTH */

#define NET_MONITOR_DATA_OFS 2
#define PACKET_ID_OFS 22
#define LS_DATA_LENGTH_OFS 23
#define LS_DATA_OFS 25
#define CRC2_OFS 569

int is_send_acceptable(struct daru_buffer_control_packed* p, unsigned char service_id) {
	//check feasibility of service_id, depending on buffer- and slot-status
	if (service_id < 4) {  // if normal packet?
		if ( (((p->_bufferstatus >> service_id) & 0x01) == 1) || //normal packet buffer full
			 (((p->_slotstatus >> service_id) & 0x01) == 0) ) { //slot not opened
			return 0;
		}
	}
	else if (service_id < 8) { // if extra packet?
		if ( ((p->_bufferstatus >> 4) == 0) || //extra packet (circular)-buffer no space left
			 ((p->_slotstatus >> 4) <= (service_id - 4)) ) {	//service_id >= slots_opened+4
			return 0;
		}
	}
	else {
		return 0; //service_id invalid
	}
	return 1;
}

int is_not_stall(struct daru_buffer_control_packed* p) {
	return ((p->_darustatus & MSK_DARU_FULL) == 0);
}

int write_datap(struct datap_o_packed *p_hdr, unsigned char* p, unsigned short ls_data_length)
{
	unsigned char hs;
	int result = -1;
	for (;;) {
		if (Is_fh_extern_opened == 0) {
			if ((Fh = open("/dev/GPIO-driver", O_RDWR)) == -1) {
				perror("Can't open /dev/GPIO-driver!");
				break;
			}
		}

		// write daru_data_handshake to BS to indicate the begin of packet transfer
		if (lseek(Fh, DARU_DATA_HANDSHAKE_OFS, SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		hs = MSK_DARU_START;
		if (write(Fh, &hs, 1) == -1) {
			perror("write");
			break;
		}

		// send data packet header to BS
		if (lseek(Fh, (DATA_ASSEMBLY_RAM_UP_OFS+PACKET_ID_OFS), SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		if (write(Fh, p_hdr, sizeof(*p_hdr)) == -1) {
			perror("write");
			break;
		}
		// write real payload
		// no lseek() is needed as file offset follows last write() seamlessly
		// no memory out-of-bound protect, responsibility of length and pointer provider
		// crc1 should already be provided, zero filling is omitted
		if (write(Fh, p, ls_data_length+CRC1_BYTES) == -1) {
			perror("write");
			break;
		}

		// write daru_data_handshake to BS to indicate the end of packet transfer
		if (lseek(Fh, DARU_DATA_HANDSHAKE_OFS, SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		//set bit 0, side effect: implicit set MSK_DARU_START and other status bits zero
		hs = MSK_DARU_FULL;
		if (write(Fh, &hs, 1) == -1) {
			perror("write");
			break;
		}
		//success
		result = 0;
		break;
	}
	//clean up
	if (Is_fh_extern_opened == 0) {
		if (Fh != -1) close(Fh);
		Fh = -1;
	}
	return result;
}

int try_write_datap(struct datap_o_packed *p_hdr, unsigned char* p, unsigned short ls_data_length,
		unsigned char service_id)
{
	struct daru_buffer_control_packed daru_buf_ctrl;
	unsigned char hs;
	int result = -1;
	for (;;) {
		if (Is_fh_extern_opened == 0) {
			if ((Fh = open("/dev/GPIO-driver", O_RDWR)) == -1) {
				perror("Can't open /dev/GPIO-driver!");
				break;
			}
		}

		// read_daru_buffer_control_packed
		if (lseek(Fh, DARU_DATA_HANDSHAKE_OFS, SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		if (read(Fh, &daru_buf_ctrl, sizeof(daru_buf_ctrl)) == -1) {
			perror("read");
			break;
		}

		if  ( (is_send_acceptable(&daru_buf_ctrl, service_id) == 0) ||
			  (is_not_stall(&daru_buf_ctrl) == 0) ){ //try fail, not acceptable or still stall
			result = -2;
			break;
		}

		/* give some debug, if both error_bit and warning_bit set, only error is shown*/
		if ((daru_buf_ctrl._darustatus & MSK_DARU_MAYBE_LOST) != 0) {
			fprintf(stderr, "%s - last send has error (write overframe and maybe write during send --> evtl. lost) \n", __FUNCTION__);
		}
		else if ((daru_buf_ctrl._darustatus & MSK_DARU_OVERFRAME_WARNING) != 0) {
			fprintf(stderr, "%s - last send has warning (write overframe?)\n", __FUNCTION__);
		}

		// write daru_data_handshake to BS to indicate the begin of packet transfer
		if (lseek(Fh, DARU_DATA_HANDSHAKE_OFS, SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		hs = MSK_DARU_START;
		if (write(Fh, &hs, 1) == -1) {
			perror("write");
			break;
		}

		// send data packet header to BS
		if (lseek(Fh, (DATA_ASSEMBLY_RAM_UP_OFS+PACKET_ID_OFS), SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		if (write(Fh, p_hdr, sizeof(*p_hdr)) == -1) {
			perror("write");
			break;
		}
		// write real payload
		// no lseek() is needed as file offset follows last write() seamlessly
		// no memory out-of-bound protect, responsibility of length and pointer provider
		// crc1 should already be provided, zero filling is omitted
		if (write(Fh, p, ls_data_length+CRC1_BYTES) == -1) {
			perror("write");
			break;
		}

		// write daru_data_handshake to BS to indicate the end of packet transfer
		if (lseek(Fh, DARU_DATA_HANDSHAKE_OFS, SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		//set bit 0, side effect: implicit set MSK_DARU_START and other status bits zero
		hs = MSK_DARU_FULL;
		if (write(Fh, &hs, 1) == -1) {
			perror("write");
			break;
		}
		//success
		result = 0;
		break;
	}
	//clean up
	if (Is_fh_extern_opened == 0) {
		if (Fh != -1) close(Fh);
		Fh = -1;
	}
	return result;
}

int read_datap(struct datap_i_packed *p_hdr, struct su_timing_packed* p_timing,
				unsigned char* p, unsigned short* p_ls_data_length) {
	unsigned char hs;
	int result = -1;
	for (;;) {
		/* Fridtjof
		if (Is_interrupt_registed) {
			//clear sw-interrupt, reallow sw-interrupt, must before handshake!
			//(hardware interrupt reallow), see below
			break;
		}
		*/

		if (Is_fh_extern_opened == 0) {
			if ((Fh = open("/dev/GPIO-driver", O_RDWR)) == -1) {
				perror("Can't open /dev/GPIO-driver!");
				break;
			}
		}

		//read data header part
		if (lseek(Fh, DATA_ASSEMBLY_RAM_DOWN_OFS, SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		if (read(Fh, p_hdr, sizeof(*p_hdr)) == -1) {
			perror("read");
			break;
		}
		//interpret length
		*p_ls_data_length = ntohs_comms(p_hdr->_ls_data_length);
		if (*p_ls_data_length > MAX_LS_DATA_LENGTH) *p_ls_data_length = MAX_LS_DATA_LENGTH; //limit

		// read real payload to a memory indicated by higher level protocol (memcpy avoiding)
		// no lseek() is needed as file offset follows last read() seamlessly
		// no memory out-of-bound protect, responsibility of pointer provider
		if (read(Fh, p, *p_ls_data_length+CRC1_BYTES) == -1) {
			perror("read");
			break;
		}

		//read packet timing
		if (lseek(Fh, DARD_DATA_TIMING_OFS, SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		if (read(Fh, p_timing, sizeof(*p_timing)) == -1) {
			perror("read");
			break;
		}

		// indicate read finished to BS, this action reallows hardware interrupt on BS, Fridtjof
		hs = 0;
		if (lseek(Fh, DARD_DATA_HANDSHAKE_OFS, SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		if (write(Fh, &hs, 1) == -1) {
			perror("write");
			break;
		}
		//success
		result = 0;
		break;
	}
	//clean up
	if (Is_fh_extern_opened == 0) {
		if (Fh != -1) close(Fh);
		Fh = -1;
	}
	return result;
}

