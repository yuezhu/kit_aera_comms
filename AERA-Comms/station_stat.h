/** @file station_stat.h
 *  @brief
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#ifndef STATION_STAT_H_
#define STATION_STAT_H_

#include <stdio.h>
#include "accumulator.h"
#include "networkmon.h"

#include "id.h"

CREATE_HISTO1D(5,pktrecv)
CREATE_HISTO1D(5,linkstat)

typedef struct {
	/* source: NWM*/
	int current_frame_count; //used to eliminate duplicated entries in one frame
	int	N; // counter
	minmaxsigma2_t su_rssi_acc;
	minmaxsigma2_t su_rssi_bg_acc;
	minmaxsigma_t temperature_in_cel_acc;
	minmaxsigma_t tx_power_acc;

	double_value_t firmware_version_inst; //invalid cond -1
	double_value_t software_version_inst; //invalid cond -1
	loopback_mode_t loopback_mode_inst; //su specific, invalid cond: LOOPBACK_UNKNOWN
	int recv_delay_inst; //su specific, invalid cond: -1
	int rx_lna_mode_inst; //su specific, invalid cond: -1
	int rx_vga_gain_inst; //su specific, invalid cond: -1

	int rx_total_sloterrors; //SER
	int rx_total_slots;
	int rx_dn_pkterrors; //PER
	int rx_dn_pkts;
	int rx_ncp_pkterrors; //PER
	int rx_ncp_pkts;
	/* source: statistictab*/
	minmaxsigma2_t bs_rssi_acc;
	pktrecv_histo1d_t pkt_recv_acc;
	int num_arq_slots_acc;
	/* source: this program- d_layer (datap)*/
	int throughput_acc; //throughputs
	int bandwidth_acc; //bandwidth
	/* source: this program- link_status_fsm */
	linkstat_histo1d_t link_status_acc;
} station_stat_t;

typedef struct {
	unsigned short id;
	int staId; //-1 is invalid

	unsigned char link_stat_percentage[5];
	char link_stat_summary;

	float SER;
	float PER_dn;
	float PER_ncp;
	float PER;

	float SER_up;

	float arq_slots_per_sec;
	int throughput;
	int bandwidth;

	char bs_rssi_avg;
	float bs_rssi_stddev;
	char su_rssi_avg;
	float su_rssi_stddev;
	char su_rssi_bg_avg;
	float su_rssi_bg_stddev;
	char txpower_avg;
	char temperature_avg;
}station_stat_yield_t;

void reset_station_stat(station_stat_t* p);
void yield_station_stat(station_stat_yield_t* y, station_stat_t* p, unsigned short id, int seconds);
void print_station_stat(FILE* out, station_stat_t* p, station_stat_yield_t* y);
void print_station_stat_title(FILE* out);

#endif /* STATION_STAT_H_ */
