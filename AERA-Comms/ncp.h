/** @file ncp.h
 *  @brief IF (interface) class for Network Control Packet/List (NCP or NCL) in-out from and to the base station, function prototypes
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#ifndef NCP_H_
#define NCP_H_

#include <time.h> /* for time_t */
#include "slot_attribute.h" /* for station_attr_hash */
#include "station_attr_table.h" /* for slot_attr_t */
#include "tdma_protocol.h" /* for MAX_NO_OF_SLOTS */

// substructure of NCP aif
typedef struct {
	unsigned char tx_power_low;
	unsigned char tx_power_medium;
	unsigned char tx_power_high;
	unsigned char tx_power_max;
	unsigned int  max_block_length;
	unsigned char future_freq_channel;
	unsigned char current_freq_channel;
	unsigned char number_of_frames;
	unsigned char number_of_NCP_slots;
	unsigned short no_up_slots;
	unsigned char no_down_slots; //repetition in one fold
	unsigned char no_ARQ_slots; //in one fold
	unsigned char no_std_download_packets; //fold
	unsigned int  BS_firmware_version;
} aif_t;
unsigned short get_num_slots_per_frame(aif_t* p_aif);

typedef struct  {
	/* full slots table of absolute slot index, only the slot assignments part aka. "uplink" have effect for BS
	 * the correctness (e.g. offset including NCP-slots of e.g. 2) is the responsibility of XML file*/
	slot_attr_t slot[MAX_NO_OF_SLOTS];
	/* range (from slot 0) of slots assignment
	 * e.g. if the highest slot assigned is 166, then size is 167  */
	unsigned short size;
} slot_tab_t;

void init_slot_tab(slot_tab_t* p);
int get_num_slots_assigned(int id, slot_tab_t* p);
int get_num_pkts_to_recv(int id, slot_tab_t* p);
int is_slot_uplink(int ind, slot_tab_t* p);

typedef struct {
	time_t	time;
	aif_t aif;
	slot_tab_t	slot_tab;
	/* caution: each ncp_t obj has one and only one station_table
	 * the ncp obj should be explicitly cleaned up if allocated on stack
	 * because of this table (malloc when add, explicite delete when free)*/
	struct station_attr_hash* p_station_table;
} ncp_t; //better only one obj (singleton) on stack

void init_ncp(ncp_t* p);
void cleanup_ncp(ncp_t* p);
int is_ncp_valid(ncp_t* p);
// function to verify a packed NCP

int deliver_ncp(ncp_t* p);
int fetch_and_print_ncp(ncp_t* p);
int is_bs_available();
int is_bs_need_ncp();

#endif /* NCP_H_ */
