/********************************************************************/
/*																	*/
/*  mon_db.c														*/
/*  =========														*/
/*																	*/
/*  Abstracted Monitoring database access functions implementation	*/
/*																	*/
/*																	*/
/*  Author:		S.Okedara, Y.Zhu									*/
/*  Date: 															*/
/*	Version:														*/
/*																	*/
/********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "mon_db.h"
#include "sqldata.h"

static MYSQL *Conn;       /* pointer to database connection handler */
static char Dbname[256]; //name of the dbname that will be recorded once on "real_connect" and later "used"

char null_mysql[256];

static int read_line(FILE *in, char *buffer, size_t max)
{
  return fgets(buffer, max, in) == buffer;
}

void conv_cell_datetime_t_to_time_t(cell_datetime_t *ts, time_t *ttDate)
{
	if (!*(char *)(ts)) {
		*ttDate = 0;
		return;
	}
	
	gmtime (ttDate);
	struct tm *a_tm_struct = localtime(ttDate);
	a_tm_struct->tm_year = ts->year - 1900;
	a_tm_struct->tm_mon = ts->month - 1;
	a_tm_struct->tm_mday = ts->day;
	a_tm_struct->tm_hour = ts->hour;
	a_tm_struct->tm_min = ts->minute;
	a_tm_struct->tm_sec = ts->second;
	*ttDate = mktime(a_tm_struct);
}

void convert_time_t_to_cell_datetime_t(time_t *ttDate, cell_datetime_t *ts)
{
	if (!*ttDate) {
		memset(ts, 0, sizeof(cell_datetime_t));
		return;
	}

	struct tm *a_tm_struct = gmtime(ttDate);
	ts->year = a_tm_struct->tm_year + 1900;
	ts->month = a_tm_struct->tm_mon + 1;
	ts->day = a_tm_struct->tm_mday;
	ts->hour = a_tm_struct->tm_hour;
	ts->minute = a_tm_struct->tm_min;
	ts->second = a_tm_struct->tm_sec;
	ts->second_part = 0;
	ts->neg = 0;
}

//get db setup of {ip, username, pw and dbname} from a file
static int get_db_setup_from_file(const char* filename, char* ip, char* username, char* pw, char* dbname)
{
	FILE *f;
	char line[256], type[256], value[256];
	char mask = 0;
	if((f = fopen(filename, "rt")) != NULL)
	{
		while (read_line(f, line, sizeof(line))) {
			sscanf(line, "%s %s", type, value);
			if (!strcmp(type, "ip")) {
				strcpy(ip, value);
				mask = mask|0x1;
			}
			else if (!strcmp(type, "username")) {
				strcpy(username, value);
				mask = mask|0x2;
			}
			else if (!strcmp(type, "password")) {
				strcpy(pw, value);
				mask = mask|0x4;
			}
			else if (!strcmp(type, "dbname")) {
				strcpy(dbname, value);
				mask = mask|0x8;
			}
		}
		fclose(f);
		
		if ((mask&0xf) == 0xf) {
			printf("DB setup successfully loaded. ip: %s, username: %s, pw: %s, type: %s\n", ip, username, pw, dbname);
			return 0;
		}
		fprintf(stderr, "setup in %s is not complete or incorrect file structure\n", filename);
		return -1;
	}
	fprintf(stderr, "%s not found\n", filename);
	return -2;
}

int is_db_exists(const char* dbname) {
	MYSQL_RES* result;
	MYSQL_ROW row;
	result = mysql_list_dbs(Conn, NULL);
	if (result == NULL) {
		fprintf(stderr, "Failed to list databases, error: %s\n", mysql_error(Conn));
		exit(EXIT_FAILURE);
	}
	while ((row = mysql_fetch_row(result)) != NULL) {
		if (strcmp(dbname, row[0]) == 0) {
			return 1; //on found
		}
	}
	mysql_free_result(result);
	return 0;
}

int create_mon_db_new(const char* dbname) {
	char stmt[256];
	sprintf(stmt, "create database %s", dbname);
	if (mysql_query(Conn, stmt)) {
		fprintf(stderr, "Failed to create database, error: %s\n", mysql_error(Conn));
		return -1;
	}
	return 0;
}

int open_mon_db(int db_version) {
	/* initialize connection handler */
	Conn = mysql_init (NULL);
	if (Conn == NULL) {
		perror("mysql_init() failed (probably out of memory)");
		return -1;
	}
	
	/* get db setup from file */
	char ip[256], username[256], pw[256], dbname_no_ver[256];
	if (!get_db_setup_from_file("/root/mysql_login.txt", ip, username, pw, dbname_no_ver)) {

		if (!mysql_real_connect(Conn, ip, username, pw, NULL, 0, NULL, 0)) {
			fprintf(stderr, "Failed to connect to database server, error: %s\n", mysql_error(Conn));
			return -2;
		}

		// generate Dbname
		strcpy(Dbname, dbname_no_ver); //version
		char str_db_version[5];
		sprintf(str_db_version, "_%02d", db_version);
		strcat(Dbname, str_db_version);

		int is_new_db;
		/* if not exist, create a new */
		if (is_db_exists(Dbname) == 0) {
			printf("the DB \"%s\" not exists in server, create it...", Dbname);
			if (create_mon_db_new(Dbname) < 0) {
				return -4;
			}
			printf("successful\n"); //debug
			is_new_db = 1; //tell caller that the db is new
		} else {
			printf("the DB \"%s\" exists in server, just populate it\n", Dbname);
			is_new_db = 0;
		}
		mysql_select_db(Conn, Dbname);
		/* connect to the created or existing DB */
		/*if (!mysql_real_connect(Conn, ip, username, pw, Dbname, 0, NULL, 0)) {
			fprintf(stderr, "Failed to connect to DB \"%s\", error: %s\n", Dbname, mysql_error(Conn));
			return -5;
		}*/
		return is_new_db;
	}
	return -3;
}

int close_mon_db()
{
	mysql_close(Conn);
	return 0;
}

int create_table(char* tablename, struct row_mon_db* row)
{
	char stmt[2048];
	sprintf(stmt, "CREATE TABLE %s", tablename);
	strcat(stmt, "(");
	int i;
	for(i=0; i<row->columns; i++) {
		strcat(stmt, row->cells[i].cell_name);
		strcat(stmt, " "); strcat(stmt, get_cell_type_translation(row->cells[i].cell_type));

		if (row->cells[i].cell_sign == CELL_UNSIGNED) {
			if ((row->cells[i].cell_type == CELL_8B) ||
				(row->cells[i].cell_type == CELL_16B) ||
				(row->cells[i].cell_type == CELL_32B) ||
				(row->cells[i].cell_type == CELL_FLOAT)) {
				strcat(stmt, " "); strcat(stmt, "UNSIGNED");
			}
		}

		if (row->cells[i].is_cell_neverNull == 1) {
			strcat(stmt, " "); strcat(stmt, "NOT NULL");
		}

		if (i < row->columns-1) { //if not the last column
			strcat(stmt, ", ");
		}
	}
	strcat(stmt, ")");
	printf("statement: %s\n", stmt); //Debug
	if (mysql_query(Conn, stmt)) {
		fprintf(stderr, "Failed to create table, error: %s\n", mysql_error(Conn));
		return -1;
	}
	return 0;
}

int insert_row(char* tablename, struct row_mon_db* row)
{
	MYSQL_BIND    param[256];
	my_bool is_null[256];
	int i;
	for (i=0; i<row->columns; i++) {
		param[i].buffer_type = (enum enum_field_types)(row->cells[i].cell_type); //compile time mapping, 1-1 mapped by #define
		if (row->cells[i].cell_data == NULL) {
			param[i].buffer		= NULL;
			is_null[i] 			= 1;
		} else {
			param[i].buffer		= (void*)(row->cells[i].cell_data);
			is_null[i] 			= 0;
		}
		param[i].is_null	 = &(is_null[i]);
		param[i].is_unsigned = (my_bool)(row->cells[i].cell_sign); //enum direct mapping enabled by enum coding
		
		if (row->cells[i].cell_type == CELL_STRING) { //by string, sql need to know the how much buffer memory allocated and how long is the string in "mysql_stmt_execute()"
			//param[i].length	= strlen((char*)(row->cells[i].cell_data));
			param[i].buffer_length = row->cells[i].cell_data_bytes;
		}
	}
	prepared_statements(Conn, Dbname, tablename, param, row->columns);
	return 0;
}
