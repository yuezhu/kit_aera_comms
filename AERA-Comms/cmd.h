/** @file cmd.h
 *  @brief IF (interface) class for cmd out to the base station, function prototypes
 *
 *	Note that the command reponse comes from statistictab.h (BS) or datap.h "NWM" channel (SU)
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#ifndef CMD_H_
#define CMD_H_

#include "common_set.h" /* for command_type_t */

typedef enum {CMD_FOR_SU = 1, CMD_FOR_BS = 2, CMD_FOR_ALL_SU} command_dest_t;

char* dest2string(command_dest_t dest);
command_dest_t string2dest(char* dest_string);

typedef struct{
	command_dest_t dest;
	command_type_t type;
	unsigned short  SU_ID;
	unsigned short content;
} cmd_t;

int deliver_cmd(cmd_t* p);

#endif /* CMD_H_ */
