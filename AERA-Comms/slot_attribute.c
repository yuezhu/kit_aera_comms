/** @file slot_attribute.c
 *  @brief slot_attribute method implementation
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#include "slot_attribute.h"

void init_slot_attr(slot_attr_t* p, link_direction_t ld, unsigned char spn, unsigned int id) {
	p->link_direction = ld;
	p->sub_packet_no = spn;
	p->SUorBS_ID = id;
}

int is_slot_attr_valid(slot_attr_t* p) {
	return ( ((p->link_direction == up) && IS_SUorBS_ID_SUID(p->SUorBS_ID)) ||
			 ((p->link_direction == down) && IS_SUorBS_ID_VALID(p->SUorBS_ID)) );
}

void pack_slot_attr(unsigned char to[2], slot_attr_t* from) {
     to[0] = ((from->link_direction)<<7)|(((from->sub_packet_no)&0x07)<<4)|(((from->SUorBS_ID)>>8)&0x0f);
     to[1] = (from->SUorBS_ID) & 0xff;		 // set lower part if id
}

void unpack_slot_attr(slot_attr_t* to, unsigned char from[2]) {
    to->link_direction = (link_direction_t)(from[0]>>7);
    to->SUorBS_ID      = ((unsigned short)(from[0]&0x0f)<<8) | (unsigned short)from[1];
    to->sub_packet_no  = (from[0]>>4)&0x07;
}
