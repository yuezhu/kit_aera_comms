/** @file cmd.c
 *  @brief IF (interface) class for cmd out to the base station, function implementation
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#include <stdio.h>
#include <string.h>

#include "def.h"

#include "cmd.h"
#include "io.h" /* for io */
#include "slot_attribute.h" /* for SUorBS_ID check */

#define ID_CMD_DEST_BROADCAST 0xffff

/************/
/*	for IO	*/
/************/
#define SBCBUF_COMMAND_OFS 1572
#define SBCBUF_COMMAND_HANDSHAKE_OFS 1580
	#define OFS_SBCBUF_COMMAND_HANDSHAKE_READY 0

#define packed_data __attribute__((__packed__))

struct cmd_packed {
	unsigned char _dest; 			//byte0
	unsigned short _id; 	//byte1 and 2; bit 11 - 0, 0xffff for all
	unsigned char _type; 			//byte3
	unsigned short _content; 	//byte 4 and 5
}packed_data;

/************/
/*	for IF	*/
/************/
char* dest2string(command_dest_t dest) {
	char *dest_string;
	switch (dest) {
		case CMD_FOR_SU: dest_string = "lokal_station"; break;
		case CMD_FOR_BS: dest_string = "base_station"; break;
		case CMD_FOR_ALL_SU: dest_string = "all_stations"; break;
		default:	dest_string = ""; break;
	}
	return dest_string;
}

command_dest_t string2dest(char* dest_string) {
	command_dest_t dest = CMD_FOR_SU;
	if (strcmp(dest_string, "lokal_station") == 0) {
		dest = CMD_FOR_SU;
	} else if (strcmp(dest_string, "base_station") == 0) {
		dest = CMD_FOR_BS;
	} else if (strcmp(dest_string, "all_stations") == 0) {
		dest = CMD_FOR_ALL_SU;
	}
	return dest;
}

int deliver_cmd(cmd_t* p) {
	struct cmd_packed packed;

	if (p->dest == CMD_FOR_ALL_SU) {
		packed._dest = (unsigned char)CMD_FOR_SU;
		packed._id = htons_comms(ID_CMD_DEST_BROADCAST);
	} else if (p->dest == CMD_FOR_SU){
		packed._dest = (unsigned char)(p->dest);
		if (IS_SUorBS_ID_SUID(p->SU_ID) == 0) {
			fprintf(stderr, "%s: su id %d for command invalid\n", __FUNCTION__, p->SU_ID);
			return -2; //fail
		}
		packed._id = htons_comms(p->SU_ID);
	} else { //CMD_FOR_BS
		packed._dest = (unsigned char)(p->dest);
		packed._id = 0; //whatever value, 0 will do least harm when "dest" is miss-interpreted
	}
	packed._type = (unsigned char)(p->type); //NOP ok
	packed._content = htons_comms(p->content); // content as sent

	//write
	if (write_address_handshake(SBCBUF_COMMAND_OFS, &packed, sizeof(packed),
								SBCBUF_COMMAND_HANDSHAKE_OFS,
								OFS_SBCBUF_COMMAND_HANDSHAKE_READY) < 0) {
		return -1; //fail
	}
	return 0;
}
