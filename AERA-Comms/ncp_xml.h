/** @file ncp_xml.h
 *  @brief helper class that loads xml file to ncp structure, function prototypes
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#ifndef NCP_XML_H_
#define NCP_XML_H_

#include "ncp.h"

int find_ncp_xml(const char* path, char* ncp_filename, int* ncp_version);

// as the name
int load_ncp_from_xml(char *filename, ncp_t* p);

#endif /* NCP_XML_H_ */
