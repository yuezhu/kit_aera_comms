/*
 * infobase.h
 *
 *  Created on: Dec 21, 2012
 *      Author: dev
 */

#ifndef INFOBASE_H_
#define INFOBASE_H_


#include <stdio.h>

#include "rthq_infobase.h" /* transparent, for higher-level infos */
#include "statistictab.h"
#include "station_stat_table.h"

CREATE_HISTO1D(8,bsrssi) //-100 to -40 dbm with 10 db-level

typedef struct {
	/* source: main() */
	int ncp_version;
	/* source: statistictab*/
	int	N;
	minmaxsigma2_t bs_rssi_bg_acc;
	minmaxsigma_t temperature_in_cel_acc;
	minmaxsigma_t tx_power_acc;
	sigma_t	arq_list_size_acc;

	int num_slots_evaluated;
	pktrecv_histo1d_t pkt_recv_acc;
	bsrssi_histo1d_t bs_rssi_acc;
	int slot_attr_unmatch_acc;

	double_value_t firmware_version_inst; //invalid: -1
	double_value_t software_version_inst; //invalid: -1
	loopback_mode_t loopback_mode_inst;
	int bs_id_inst; //bs specific, invalid: -1

	/* source: this program- link_status_fsm */
	linkstat_histo1d_t link_status_acc;
} bs_stat_t;

extern bs_stat_t Bs_stat; //singleton

extern struct station_stat_hash* Su_stat_table; //singleton

void reset_bs_stat();
void print_bs_stat(FILE* out);
void print_bs_stat_title(FILE* out);

#endif /* INFOBASE_H_ */
