/** @file slot_attribute.h
 *  @brief structure for slot_attribute and method prototype
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#ifndef SLOT_ATTRIBUTE_H_
#define SLOT_ATTRIBUTE_H_

/** @brief link_direction_t type, network <-> logical identical
 */
typedef enum {up = 0, down = 1} link_direction_t;

/** @brief slot_attr_t type.
 */
typedef struct {
	link_direction_t link_direction;
	unsigned char sub_packet_no;
#define MAX_SUB_PKT_NO	0x7
	unsigned short  SUorBS_ID; // 12 bit ID: BS between 1 and 31, SU between 32 and 4000; used to identify the station_attr
#define MAX_SUorBS_ID	0xfff
#define IS_SUorBS_ID_VALID(id)	((id > 0) && (id <= MAX_SUorBS_ID))
#define IS_SUorBS_ID_BSID(id)	((id > 0) && (id < 11))
#define IS_SUorBS_ID_SUID(id)	((id >= 11) && (id <= MAX_SUorBS_ID))
} slot_attr_t;

/** @brief initialization slot_attr_t
 */
void init_slot_attr(slot_attr_t* p, link_direction_t ld, unsigned char spn, unsigned int id);

/** @brief validate slot_attr_t
 *
 * 	for "uplink" should be SU id, for "downlink" valid id (BS or SU) enough
 *
 * 	@param [in]	p
 * 	@return int  1 for success, 0 for fail
 */
int is_slot_attr_valid(slot_attr_t* p);

/** @brief encode to network format
 */
void pack_slot_attr(unsigned char to[2], slot_attr_t* from);

/** @brief decode to logical format
 */
void unpack_slot_attr(slot_attr_t* from, unsigned char to[2]);

#endif /* SLOT_ATTRIBUTE_H_ */
