/** @file statistictab.h
 *  @brief IF (interface) class for statistictab read from the base station, function prototypes
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#ifndef STATISTICTAB_H_
#define STATISTICTAB_H_

#include "common_set.h"	/* for common_monitoring_set_t */
#include "slot_attribute.h" /* for slot_attr_t */
#include "tdma_protocol.h" /* for protocol-dependent defines */

typedef enum {UNKNOWN = 0, CORRECT_WITH_NO_FEC = 1,  CRC_ERR = 2, PREAMBLE_ERR = 3, CORRECT_WITH_FEC = 4} recv_stat;

/* Table from base station */
typedef struct {
	int rssi;
	recv_stat slot_status;
	int on_air;
} bs_slot_status_t;

typedef struct {
	unsigned char frame_count;
	unsigned char second_count;
} bs_timing_t;

//no invalid condition
typedef struct {
	bs_slot_status_t slot_stat_tab[MAX_NO_OF_SLOTS];
	int slot_stat_tab_size;
	slot_attr_t arq_slots[MAX_NO_ARQ_SLOTS_TOTAL];
	int num_arq_slots;
	int arq_list_size;
	common_monitoring_set_t cm_set;
	bs_timing_t timing;
	unsigned short bs_id;
} bs_status_tab_t;

int is_frame_end_hard();
int fetch_bs_status_tab(bs_status_tab_t* p);

#endif /* STATISTICTAB_H_ */
