/** @file infobase.c
 *  @brief
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */
#include <math.h> /* for nan.h */
#include "infobase.h"
#include "accumulator.h"

bs_stat_t Bs_stat;

struct station_stat_hash* Su_stat_table;

CREATE_HISTO1D_C(8,bsrssi) //-100 to -40 dbm with 10 db-level

void reset_bs_stat()
{
	//reset_histo1d_linkstat(&Bs_stat.link_status_acc); //reseted elsewhere
	//Bs_stat.ncp_version = 0; //no need to reset

	Bs_stat.firmware_version_inst.value[0] = -1;
	Bs_stat.firmware_version_inst.value[1] = -1;
	Bs_stat.software_version_inst.value[0] = -1;
	Bs_stat.software_version_inst.value[1] = -1;
	Bs_stat.loopback_mode_inst = LOOPBACK_UNKNOWN;
	Bs_stat.bs_id_inst = -1;

	Bs_stat.N = 0;
	reset_sigma(&Bs_stat.arq_list_size_acc);
	reset_minmaxsigma(&Bs_stat.tx_power_acc);
	reset_minmaxsigma(&Bs_stat.temperature_in_cel_acc);
	reset_minmaxsigma2(&Bs_stat.bs_rssi_bg_acc);
	Bs_stat.num_slots_evaluated = 0;
	Bs_stat.slot_attr_unmatch_acc = 0;
	reset_histo1d_pktrecv(&Bs_stat.pkt_recv_acc);
	reset_histo1d_bsrssi(&Bs_stat.bs_rssi_acc);
}

void print_bs_stat(FILE* out) {
	fprintf(out, "%d, bs, "
			"%d, %d, "
			"%d.%d, %d.%d, "
			"%d, %d, %d, %d, %d, "
			"%d, "
			"%4.2f, "
			"%d, %d, %d, "
			"%d, %d, %d, "
			"%d, %d, %d, %5.3f, "
			"%d, %d, "
			"%d, %d, %d, %d, %d, "
			"%d, %d, %d, %d, %d, %d, %d, %d, ",
	Bs_stat.ncp_version,
	Bs_stat.bs_id_inst, Bs_stat.loopback_mode_inst,
	Bs_stat.firmware_version_inst.value[0], Bs_stat.firmware_version_inst.value[1], Bs_stat.software_version_inst.value[0], Bs_stat.software_version_inst.value[1],
	Bs_stat.link_status_acc.bin[0], Bs_stat.link_status_acc.bin[1], Bs_stat.link_status_acc.bin[2], Bs_stat.link_status_acc.bin[3], Bs_stat.link_status_acc.bin[4],
	Bs_stat.N,
	cal_avg_sigma(&Bs_stat.arq_list_size_acc),
	Bs_stat.tx_power_acc.min_reg, Bs_stat.tx_power_acc.max_reg, (int)cal_avg_minmaxsigma(&Bs_stat.tx_power_acc),
	Bs_stat.temperature_in_cel_acc.min_reg, Bs_stat.temperature_in_cel_acc.max_reg, (int)cal_avg_minmaxsigma(&Bs_stat.temperature_in_cel_acc),
	Bs_stat.bs_rssi_bg_acc.min_reg, Bs_stat.bs_rssi_bg_acc.max_reg, (int)cal_avg_minmaxsigma2(&Bs_stat.bs_rssi_bg_acc), cal_stddev_minmaxsigma2(&Bs_stat.bs_rssi_bg_acc),
	Bs_stat.num_slots_evaluated, Bs_stat.slot_attr_unmatch_acc,
	Bs_stat.pkt_recv_acc.bin[0], Bs_stat.pkt_recv_acc.bin[1], Bs_stat.pkt_recv_acc.bin[2], Bs_stat.pkt_recv_acc.bin[3], Bs_stat.pkt_recv_acc.bin[4],
	Bs_stat.bs_rssi_acc.bin[0], Bs_stat.bs_rssi_acc.bin[1], Bs_stat.bs_rssi_acc.bin[2], Bs_stat.bs_rssi_acc.bin[3], Bs_stat.bs_rssi_acc.bin[4], Bs_stat.bs_rssi_acc.bin[5], Bs_stat.bs_rssi_acc.bin[6], Bs_stat.bs_rssi_acc.bin[7]
	);
}

void print_bs_stat_title(FILE* out) {
	fprintf(out, "%s, type, "
			"%s, %s, "
			"%s, %s, "
			"%s, %s, %s, %s, %s, "
			"%s, "
			"%s, "
			"%s, %s, %s, "
			"%s, %s, %s, "
			"%s, %s, %s, %s, "
			"%s, %s, "
			"%s, %s, %s, %s, %s, "
			"%s, %s, %s, %s, %s, %s, %s, %s, ",
	"ncp_version",
	"bs_id", "loopback_mode",
	"firmware", "software",
	"awaiting", "established", "broken", "unused", "unknown",
	"entries",
	"arq slots per frame",
	"tx_power.min", "tx_power.max", "tx_power.avg",
	"temperature.min", "temperature.max", "temperature.avg",
	"bs_rssi_bg.min", "bs_rssi_bg.max", "bs_rssi_bg.avg", "bs_rssi_bg.stddev",
	"num_slots_evaluated", "slot_attr_unmatch",
	"upslots:unknown", "upslots:ok", "upslots:crc2 error", "upslots:preamble error", "upslots:ok by FEC",
	"<=-100", "(-100 ~ -90]", "(-90 ~ -80]", "(-80 ~ -70]", "(-70 ~ -60]", "(-60 ~ -50]", "(-50 ~ -40]", ">-40"
	);
}


