/*
 * statistic_writer.h
 *
 *  Created on: Nov 9, 2012
 *      Author: dev
 */

#ifndef STATISTIC_WRITER_H_
#define STATISTIC_WRITER_H_

#include "statistictab.h"
#include "networkmon.h"
#include "ncp.h"
#include "datap.h" /* for monitoring_info_aggr_t */

void store_statistictab(bs_status_tab_t* from, ncp_t* p);

void store_nmd(monitoring_info_aggr_t* from, ncp_t* p);

#endif /* STATISTIC_WRITER_H_ */
