/** @file station_attribute.h
 *  @brief structure for station_attribute and method prototype
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#ifndef STATION_ATTRIBUTE_H_
#define STATION_ATTRIBUTE_H_

/** @brief link_status_t type, network <-> logical identical
 */
typedef enum {awaiting_connection = 0, established = 1, broken = 2, unused = 3} link_status_t;

/** @brief functions for link_status print
 */
char* link_status2string(link_status_t ls);

/** @brief functions for link_status print
 */
char* link_status2letter(link_status_t ls);

/** @brief station_attr_t type.
 */
typedef struct {
	link_status_t link_status;
	unsigned char transmission_delay; // 0..15
#define MAX_TRANS_DELAY 0xf
	unsigned char transmission_power; // 0..3
#define MAX_TRANS_POWER 0x3
	unsigned char num_uplink_packets_per_frame; //0 ~ 8
	unsigned short valid_uplink_packets_counter;
} station_attr_t;

/** @brief initialization station_attr_t
 */
void init_station_attr(station_attr_t* p, link_status_t ls, unsigned char tx_delay, unsigned char tx_power);

/** @brief validate station_attr_t
 *
 * 	@param [in]	p
 * 	@return int  1 for success, 0 for fail
 */
int is_station_attr_valid(station_attr_t* p);

/** @brief encode to network format
 */
void pack_station_attr(unsigned char* to, station_attr_t* from);

/** @brief decode to logical format
 */
void unpack_station_attr(station_attr_t* to, unsigned char from);

#endif /* STATION_ATTRIBUTE_H_ */
