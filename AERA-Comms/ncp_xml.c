/** @file ncp_xml.c
 *  @brief helper class that loads xml file to ncp structure, function implementation
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <assert.h>
#include <glob.h> /* for filename wildcard-search */
#include <stdio.h>
#include <stdlib.h>

#include "syslogger.h"

#include "ncp_xml.h"

#include "infobase.h" /* for Su_stat_table */

#define NCP_XML_FILE_NAME "NCP.xml"

int find_ncp_xml(const char* path, char* ncp_filename, int* ncp_version) {
	glob_t globbuf; //storing result
	char pattern[100];
	strcpy(pattern, path);
	strcat(pattern, "/");
	strcat(pattern, NCP_XML_FILE_NAME);
	strcat(pattern, ".[0-9][0-9][0-9][0-9][0-9]");

	glob(pattern, 0, NULL, &globbuf);
	if (globbuf.gl_pathc > 1) {
		printf("more than 1 valid files found, i pick just one from them\n");
	}
	if (globbuf.gl_pathc > 0) {
		strcpy(ncp_filename, globbuf.gl_pathv[0]);

		char pattern2[100];
		char str_ncp_version[10];
		strcpy(pattern2, path);
		strcat(pattern2, "/");
		strcat(pattern2, NCP_XML_FILE_NAME);
		strcat(pattern2, ".%s");

		sscanf(ncp_filename, pattern2, str_ncp_version);
		*ncp_version = atoi(str_ncp_version);

		globfree(&globbuf);
		return 1;
	}
	return 0;
}


/****************************************************************/
/* XML functions												*/
/****************************************************************/

// Search XML-nodes using XPath expressions
xmlXPathObjectPtr getnodeset(xmlDocPtr doc, xmlChar *xpath){
	xmlXPathContextPtr context;
	xmlXPathObjectPtr result;
	context = xmlXPathNewContext(doc);
	if (context == NULL) {
		printf("Error in xmlXPathNewContext\n");
		return NULL;
	}
	result = xmlXPathEvalExpression(xpath, context);
	xmlXPathFreeContext(context);
	if (result == NULL) {
		printf("Error in xmlXPathEvalExpression\n");
		return NULL;
	}
	if(xmlXPathNodeSetIsEmpty(result->nodesetval)){
		xmlXPathFreeObject(result);
                printf("No result\n");
		return NULL;
	}
	return result;
}

int xmlValueToInt(xmlDoc *doc, xmlChar *xpath) {
	int result;
	xmlChar *value;

	xmlXPathObjectPtr resultnodes = getnodeset(doc, xpath); // resultnodes holds all nodes in resultnodes->nodesetval->nodeTab
	value = xmlNodeGetContent(resultnodes->nodesetval->nodeTab[0]);
	result = atoi(value);

	xmlFree(value);
	xmlXPathFreeObject(resultnodes);
	return result;
}

double xmlValueToFloat(xmlDoc *doc, xmlChar *xpath) {
	double result;
	xmlChar *value;

	xmlXPathObjectPtr resultnodes = getnodeset(doc, xpath); // resultnodes holds all nodes in resultnodes->nodesetval->nodeTab
	value = xmlNodeGetContent(resultnodes->nodesetval->nodeTab[0]);
	result = atof(value);

	xmlFree(value);
	xmlXPathFreeObject(resultnodes);
	return result;
}

//load a new ncp_t obj after initialization!
int load_ncp_from_xml(char *filename, ncp_t* p) {
	char message[256];
	sprintf(message, "Reading XML-file %s ... \n", filename);
	print_message(LOG_INFO, message);
	int i;
	xmlDoc 	*doc = NULL;
	xmlNode *root_element = NULL, *cur = NULL;

	//doc = xmlParseFile(filename);
	doc = xmlReadFile(filename, NULL, XML_PARSE_NOBLANKS);
	if (doc == NULL ) {
		sprintf(message,"Document %s not parsed successfully. \n", filename);
		print_message(LOG_ERR, message);
		return -1;
	}
	cur = xmlDocGetRootElement(doc);
	if (cur == NULL) {
		sprintf(message,"empty document\n");
		print_message(LOG_ERR, message);
		xmlFreeDoc(doc);
		return -2;
	}
	if (xmlStrcmp(cur->name, (const xmlChar *) "NCP")) {
		sprintf(message,"document of the wrong type, root node != NCP");
		print_message(LOG_ERR, message);
		xmlFreeDoc(doc);
		return -3;
	}
	// Get the root element node
	root_element = xmlDocGetRootElement(doc);

	// Parse XML-tree and write values into NCP-object
	p->aif.tx_power_low = xmlValueToInt(doc, (xmlChar*) "//AIF/TxPowerLow");
	p->aif.tx_power_medium = xmlValueToInt(doc, (xmlChar*) "//AIF/TxPowerMedium");
	p->aif.tx_power_high = xmlValueToInt(doc, (xmlChar*) "//AIF/TxPowerHigh");
	p->aif.tx_power_max = xmlValueToInt(doc, (xmlChar*) "//AIF/TxPowerMax");
	p->aif.max_block_length = xmlValueToInt(doc, (xmlChar*) "//AIF/MaxBlockLength");
	p->aif.future_freq_channel = xmlValueToInt(doc, (xmlChar*) "//AIF/FutureFrequencyChannel");
	p->aif.current_freq_channel = xmlValueToInt(doc, (xmlChar*) "//AIF/CurrentFrequencyChannel");
	p->aif.number_of_frames = xmlValueToInt(doc, (xmlChar*) "//AIF/NoOfFrames");
	p->aif.number_of_NCP_slots = xmlValueToInt(doc, (xmlChar*) "//AIF/NoOfNcpSlots");
	p->aif.no_up_slots = xmlValueToInt(doc, (xmlChar*) "//AIF/NoOfUpSlots");
	p->aif.no_down_slots = xmlValueToInt(doc, (xmlChar*) "//AIF/NoOfDownSlots");
	p->aif.no_ARQ_slots = xmlValueToInt(doc, (xmlChar*) "//AIF/NoOfArqSlots");
	p->aif.no_std_download_packets = xmlValueToInt(doc, (xmlChar*) "//AIF/NoOfStdDownloadPackets");
	double BS_firmware_version = xmlValueToFloat(doc, (xmlChar*) "//AIF/BsFirmwareVersion");  //TODO: firmware version can be used as SBC software version
	unsigned int fw_version = (int) BS_firmware_version * 1000;
	p->aif.BS_firmware_version = fw_version;

	//TODO: aif validation

	// Read values of the stations from XML-tree
	xmlXPathObjectPtr resultnodes;
	xmlNodeSetPtr nodeset;
	xmlNodePtr cur_node = NULL;
	xmlNodePtr child_node = NULL;
	xmlChar *attribute;
	int station_id, slot_id;
	xmlChar *value;
	xmlNsPtr ns;
	char* charvalue;

	//init slot tab
	init_slot_tab(&(p->slot_tab));

	slot_attr_t slot_attr;
	int highest_slot_id = -1;
	// Read values in the slots
	xmlChar *xpath = (xmlChar*) "//Slots/Slot";
	resultnodes = getnodeset(doc, xpath); // resultnodes holds all slots in resultnodes->nodesetval->nodeTab
	//struct ncp_slot_attribute_t log_ncp_slot[resultnodes->nodesetval->nodeNr];
	char key5[] = "up";
	char key6[] = "down";
	if (resultnodes) {
		nodeset = resultnodes->nodesetval;
		for (i=0; i < nodeset->nodeNr; i++) {
			if(nodeset->nodeTab[i]->type == XML_NAMESPACE_DECL) {
				ns = (xmlNsPtr)nodeset->nodeTab[i];
				cur_node = (xmlNodePtr)ns->next;
			} else if(nodeset->nodeTab[i]->type == XML_ELEMENT_NODE) {
				cur_node = nodeset->nodeTab[i];
			} else {
				cur_node = nodeset->nodeTab[i];
			}
			attribute = xmlGetNoNsProp(cur_node, (const xmlChar *) "No");
			slot_id = atoi(attribute);
			assert(slot_id >= 0 && slot_id < MAX_NO_OF_SLOTS); //ensure indexing
			//record highest slot id
			highest_slot_id = (slot_id > highest_slot_id)? slot_id : highest_slot_id;

			// Read the values of the child nodes
			child_node = cur_node->xmlChildrenNode; //children;
			//child_node = child_node->next;
			value = xmlNodeGetContent(child_node);
			charvalue = (char*) value;
			if (strcmp(charvalue, key5) == 0) slot_attr.link_direction = up;
			if (strcmp(charvalue, key6) == 0) slot_attr.link_direction = down;
			xmlFree(value);
			child_node = child_node->next;
			value = xmlNodeGetContent(child_node);
			slot_attr.SUorBS_ID = atoi((char*) value);
			xmlFree(value);
			child_node = child_node->next;
			value = xmlNodeGetContent(child_node);
			slot_attr.sub_packet_no = atoi((char*) value);
			xmlFree(attribute);
			xmlFree(value);
			//ensure
			assert (is_slot_attr_valid(&slot_attr) == 1);
			p->slot_tab.slot[slot_id] = slot_attr; //obj copy
		}
		xmlXPathFreeObject(resultnodes);
	}
	p->slot_tab.size = (unsigned short)(highest_slot_id + 1); //at least 0, at most MAX_NO_OF_SLOTS


	//init station_attr_table, (always reload)
	if (p->p_station_table != NULL) {
		delete_station_attr_table(&(p->p_station_table));
	}
	//init station_stat_table, (always reload)
	if (Su_stat_table != NULL) {
		delete_station_stat_table(&(Su_stat_table));
	}
	// Define variable to hold station attributes
	station_attr_t station_attr;
	xpath = (xmlChar*) "//Stations/Station";
	resultnodes = getnodeset(doc, xpath); // resultnodes holds all slots in resultnodes->nodesetval->nodeTab
	if (resultnodes) {
		nodeset = resultnodes->nodesetval;
		for (i=0; i < nodeset->nodeNr; i++) {
			if(nodeset->nodeTab[i]->type == XML_NAMESPACE_DECL) {
				ns = (xmlNsPtr)nodeset->nodeTab[i];
				cur_node = (xmlNodePtr)ns->next;
			} else if(nodeset->nodeTab[i]->type == XML_ELEMENT_NODE) {
				cur_node = nodeset->nodeTab[i];
			} else {
				cur_node = nodeset->nodeTab[i];
			}
			attribute = xmlGetNoNsProp(cur_node, (const xmlChar *) "No");
			station_id = atoi(attribute);
			assert (IS_SUorBS_ID_SUID(station_id) == 1);

			// Read the values of the childnodes
			child_node = cur_node->xmlChildrenNode; //children;
			//child_node = child_node->next;
			value = xmlNodeGetContent(child_node);
			//charvalue = (char *) value;
			//station_attr.link_status	= unused; //as default
			//if (strcmp(charvalue, key1) == 0) station_attr.link_status = awaiting_connection;
			//if (strcmp(charvalue, key2) == 0) station_attr.link_status = awaiting_connection; //established in NCP reset to awaiting_connection
			//if (strcmp(charvalue, key3) == 0) station_attr.link_status = awaiting_connection; //broken in NCP reset to awaiting_connection
			//if (strcmp(charvalue, key4) == 0) station_attr.link_status = unused;
			child_node = child_node->next;
			station_attr.transmission_delay = atoi(xmlNodeGetContent(child_node));
			child_node = child_node->next;
			station_attr.transmission_power = atoi(xmlNodeGetContent(child_node));
			//calculate the the num_pkts_per_frame for the station from the slot_tab
			station_attr.num_uplink_packets_per_frame = get_num_pkts_to_recv(station_id, &(p->slot_tab));
			//initialize link_status, (awaiting_connection if any packet assigned)
			if (station_attr.num_uplink_packets_per_frame > 0) {
				station_attr.link_status	= awaiting_connection;
			} else {
				station_attr.link_status	= unused;
			}
			//valid uplink packets counter initialization;
			station_attr.valid_uplink_packets_counter = 0;
			//ensure
			assert (is_station_attr_valid(&station_attr) == 1);
			// add the station attribute to the hash table
			add_station_attr_to_table(&(p->p_station_table), station_id, &station_attr); //"duplication" protected id --> the first add valid
			// add the station stat to the hash table
			add_station_stat_to_table(&Su_stat_table, station_id);

//printf("Add Station: %d, Status: %s, TxDelay: %d, TxPower: %d \n", station_id, link_status2string(station_attribute.link_status), station_attribute.transmission_delay, station_attribute.transmission_power);
			xmlFree(attribute);
			xmlFree(value);
		}
		xmlXPathFreeObject(resultnodes);
	}

	// free the document
	xmlFreeDoc(doc);
	// Free the global variables that may have been allocated by the parser.
	xmlCleanupParser();

	//set current time to ncp
	p->time = time(NULL);
	return 0;
}
