/** @file station_stat.c
 *  @brief
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#include "station_stat.h"
#include "staid_table.h"

CREATE_HISTO1D_C(5,pktrecv)
CREATE_HISTO1D_C(5,linkstat)

void reset_station_stat(station_stat_t* p)
{
	p->current_frame_count = -1;

	p->N = 0;
	reset_minmaxsigma2(&p->su_rssi_acc);
	reset_minmaxsigma2(&p->su_rssi_bg_acc);
	reset_minmaxsigma(&p->temperature_in_cel_acc);
	reset_minmaxsigma(&p->tx_power_acc);

	p->firmware_version_inst.value[0] = -1; //NAN;
	p->firmware_version_inst.value[1] = -1; //NAN;
	p->software_version_inst.value[0] = -1; //NAN;
	p->software_version_inst.value[1] = -1; //NAN;
	p->loopback_mode_inst = LOOPBACK_UNKNOWN;

	p->recv_delay_inst	= -1; //NAN;
	p->rx_lna_mode_inst = -1; //NAN;
	p->rx_vga_gain_inst = -1; //NAN;

	p->rx_total_sloterrors = 0;
	p->rx_total_slots = 0;
	p->rx_dn_pkterrors = 0;
	p->rx_dn_pkts = 0;
	p->rx_ncp_pkterrors = 0;
	p->rx_ncp_pkts = 0;

	reset_minmaxsigma2(&p->bs_rssi_acc);
	reset_histo1d_pktrecv(&p->pkt_recv_acc);
	p->num_arq_slots_acc = 0;

	p->throughput_acc = 0;
	p->bandwidth_acc = 0;

	reset_histo1d_linkstat(&p->link_status_acc);
}

void yield_station_stat(station_stat_yield_t* y, station_stat_t* p, unsigned short id, int seconds) {

	y->id = id;
	/* loopup associated staId */
	y->staId = find_staId(id);

	linkstat_histo1d_t percentage;
	cal_percent_histo1d_linkstat(&p->link_status_acc, &percentage);
	if (percentage.bin[1] == 100) { //100 percent established
		y->link_stat_summary = 'e'; //established
	} else if ((percentage.bin[1] == 0) && (percentage.bin[2] == 0)) { //no established, no broken (== all unused or awaiting)
		y->link_stat_summary = 'u'; //unconnected
	} else { //any other case: as long as 1% time is broken, or awaiting or unused
		y->link_stat_summary = 'i'; //instable
	}

	int i;
	for (i=0; i<5; i++) {
		y->link_stat_percentage[i] = (unsigned char)(percentage.bin[i]);
	}

	y->SER = (float)p->rx_total_sloterrors/(float)p->rx_total_slots;
	y->PER_dn = (float)p->rx_dn_pkterrors/(float)p->rx_dn_pkts;
	y->PER_ncp = (float)p->rx_ncp_pkterrors/(float)p->rx_ncp_pkts;
	int totalerrpackets = p->rx_ncp_pkterrors + p->rx_dn_pkterrors;
	int totalpackets = p->rx_ncp_pkts + p->rx_dn_pkts;
	y->PER = (float)totalerrpackets/(float)totalpackets;

	y->SER_up = (float)(p->pkt_recv_acc.bin[2]+p->pkt_recv_acc.bin[3]) /
				(float)(p->pkt_recv_acc.bin[0]+p->pkt_recv_acc.bin[1]+p->pkt_recv_acc.bin[2]+p->pkt_recv_acc.bin[3]+p->pkt_recv_acc.bin[4]);

	//seconds couldn't be 0
	y->arq_slots_per_sec = (float)p->num_arq_slots_acc/seconds;
	y->throughput	=  p->throughput_acc/seconds;
	y->bandwidth	=  p->bandwidth_acc/seconds;

	y->bs_rssi_avg = (signed char)cal_avg_minmaxsigma2(&p->bs_rssi_acc);
	y->bs_rssi_stddev = cal_stddev_minmaxsigma2(&p->bs_rssi_acc);
	y->su_rssi_avg = (signed char)cal_avg_minmaxsigma2(&p->su_rssi_acc);
	y->su_rssi_stddev = cal_stddev_minmaxsigma2(&p->su_rssi_acc);
	y->su_rssi_bg_avg = (signed char)cal_avg_minmaxsigma2(&p->su_rssi_bg_acc);
	y->su_rssi_bg_stddev = cal_stddev_minmaxsigma2(&p->su_rssi_bg_acc);
	y->txpower_avg = (signed char)cal_avg_minmaxsigma(&p->tx_power_acc);
	y->temperature_avg = (signed char)cal_avg_minmaxsigma(&p->temperature_in_cel_acc);
}

void print_station_stat(FILE* out, station_stat_t* p, station_stat_yield_t* y) {

	fprintf(out, "su, %d, %d, "
			"%d, %d, "
			"%d, %d, %d, %d, %d, "
			"%4.2f, %d, %d, "
			"%d, %d, %d, %5.3f, "
			"%d, %d, %d, %d, %d, "

			"%d, %d, %d, %4.3f, %4.3f, %4.3f, %4.3f, "
			"%d, %d, %d, %5.3f, "
			"%d, %d, %d, %5.3f, "
			"%d, %d, %d, "
			"%d, %d, %d, "
			"%d.%d, %d.%d, "
			"%d, %d, %d, %d, ",
	y->id, y->staId,
	p->current_frame_count, p->N,
	p->link_status_acc.bin[0], p->link_status_acc.bin[1], p->link_status_acc.bin[2], p->link_status_acc.bin[3], p->link_status_acc.bin[4],
	y->arq_slots_per_sec, y->throughput, y->bandwidth,
	p->bs_rssi_acc.min_reg, p->bs_rssi_acc.max_reg, (int)y->bs_rssi_avg, y->bs_rssi_stddev,
	p->pkt_recv_acc.bin[0], p->pkt_recv_acc.bin[1], p->pkt_recv_acc.bin[2], p->pkt_recv_acc.bin[3], p->pkt_recv_acc.bin[4],

	p->rx_total_slots, p->rx_dn_pkts, p->rx_ncp_pkts, y->SER, y->PER_dn, y->PER_ncp, y->PER,
	p->su_rssi_acc.min_reg, p->su_rssi_acc.max_reg, (int)y->su_rssi_avg, y->su_rssi_stddev,
	p->su_rssi_bg_acc.min_reg, p->su_rssi_bg_acc.max_reg, (int)y->su_rssi_bg_avg, y->su_rssi_bg_stddev,
	p->tx_power_acc.min_reg, p->tx_power_acc.max_reg, (int)y->txpower_avg,
	p->temperature_in_cel_acc.min_reg, p->temperature_in_cel_acc.max_reg, (int)y->temperature_avg,
	p->firmware_version_inst.value[0], p->firmware_version_inst.value[1], p->software_version_inst.value[0], p->software_version_inst.value[1],
	p->loopback_mode_inst, p->recv_delay_inst, p->rx_lna_mode_inst, p->rx_vga_gain_inst);
}

void print_station_stat_title(FILE* out) {
	fprintf(out, "type, %s, %s, "
			"%s, %s, "
			"%s, %s, %s, %s, %s, "
			"%s, %s, %s, "
			"%s, %s, %s, %s, "
			"%s, %s, %s, %s, %s, "

			"%s, %s, %s, %s, %s, %s, %s, "
			"%s, %s, %s, %s, "
			"%s, %s, %s, %s, "
			"%s, %s, %s, "
			"%s, %s, %s, "
			"%s, %s, "
			"%s, %s, %s, %s, ",
	"id", "AERA-id associated",
	"current frame", "N",
	"awaiting", "established", "broken", "unused", "unknown",
	"arq slots per sec", "throughput (Bps)", "bandwidth (Bps)",
	"bs_rssi.min", "bs_rssi.max", "bs_rssi.avg", "bs_rssi.stddev",
	"upslots:unknown", "upslots:ok", "upslots:crc2 error", "upslots:preamble error", "upslots:ok by FEC",
	/* the following part are invalid if SU is not connected (no uplink packets received) */
	"downlink: total slots", "downlink: total dn pkts", "downlink: total ncp pkts", "downlink: SER", "downlink: PER_dn", "downlink: PER_ncp", "downlink: PER_total",
	"su_rssi.min", "su_rssi.max", "su_rssi.avg", "su_rssi.stddev",
	"su_rssi_bg.min", "su_rssi_bg.max", "su_rssi_bg.avg", "su_rssi_bg.stddev",
	"tx_power.min", "tx_power.max", "tx_power.avg",
	"temperature.min", "temperature.max", "temperature.avg",
	"firmware", "software",
	"loopback_mode", "recv_delay", "rx_lna_mode", "rx_vga_gain");
}
