/** @file station_stat_table.c
 *  @brief hash table handling method implementation
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#include <stdio.h>

#include "station_stat_table.h"

int add_station_stat_to_table(struct station_stat_hash** p_table, int id) {
	struct station_stat_hash *s;
	HASH_FIND_INT(*p_table, &id, s);
    if (s == NULL) { //unique
        s = (struct station_stat_hash*)malloc(sizeof(struct station_stat_hash));
        if (s == NULL) {
        	fprintf(stderr, "%s: malloc error, struct station_stat_hash\n", __FUNCTION__);
        	exit(EXIT_FAILURE);
        }
        s->hash_id = id;
        reset_station_stat(&s->station_stat); //just reset it
        HASH_ADD_INT(*p_table, hash_id, s); //a strange #define in uthash.h: &s->hash_id referenced
        return 0; //success on added
    }
    return -1;  //failure on found existing
}

void find_station_stat_in_table(struct station_stat_hash* table, int id, station_stat_t** pp_stat) {
    struct station_stat_hash *s;
    HASH_FIND_INT(table, &id, s);
    if (s != NULL) {
    	*pp_stat = &(s->station_stat);
    } else {
    	*pp_stat = NULL;
    }
}

int find_station_stat_in_table_copy(struct station_stat_hash* table, int id, station_stat_t* p_stat) {
    struct station_stat_hash *s;
    HASH_FIND_INT(table, &id, s);
    if (s != NULL) {
    	*p_stat = s->station_stat; //object copy
    	return 0; //success on found
    }
    return -1; //fail on not found
}

int modify_station_stat_in_table(struct station_stat_hash* table, int id, station_stat_t* p_stat) {
    struct station_stat_hash *s;
    HASH_FIND_INT(table, &id, s);
    if (s != NULL) {
    	s->station_stat = *p_stat;  //object copy
    	return 0; //success on found and update
    }
    return -1; //fail, not found
}

int delete_station_stat_in_table(struct station_stat_hash** p_table, int id) {
    struct station_stat_hash *s;
    HASH_FIND_INT(*p_table, &id, s);
    if (s != NULL) {
    	HASH_DEL(*p_table, s);
    	free(s);
    	return 0; //success on found and update
    }
    return -1; //fail, not found
}

void delete_station_stat_table(struct station_stat_hash** p_table) {
  struct station_stat_hash *s, *tmp;
  HASH_ITER(hh, *p_table, s, tmp) {
    HASH_DEL(*p_table, s); 	/* delete; table advances to next */
    free(s);  /* malloc */
  }
  *p_table = NULL;
}
