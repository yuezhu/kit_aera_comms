/** @file datap.c
 *  @brief IF (interface) class for data packet in-out from and to the base station, function implementation
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "def.h"
#include "crc32.h"

#include "datap.h"
#include "datap_io.h"
#include "link_status_fsm.h"

#include "rthq_CONFIG.h"
#include "rthq_infobase.h"

#include "d_layer.h" /*function implementation for d_layer */

#define TIMESTAMP_TEST_OFS	0
#define RUNNINGNO_TEST_OFS	8

//for duplicated packet "dupack" detection
static slot_attr_t Received_packet_attr_list[MAX_NO_OF_SLOTS];
static unsigned char Received_packet_frame_count_list[MAX_NO_OF_SLOTS];
static int Received_packet_list_size = 0;

//for new-frame detection
static int Current_frame_count;   // current- and last_frame_count are used to detect "soft" frame end using data package information
static int Last_frame_count = -1;
static int New_frame = 0; //handshake with main()

//for "frame-aware" sequence-send state variables
static unsigned char Next_service_id = 0;
static int Send_sequence_fin = 0; //handshake with main()

//for test packet sending and recv check
static unsigned char Send_run_no = 0;
static short Last_recv_run_no = -1;

//for export and used in main()
static datap_i_t Hdr;
static su_timing_t	Timing;
static datap_monitoring_t DatapMon;

static network_monitor_data_t* P_nmd = NULL;

extern int Loopback_test;

void unpack_su_slot_status(su_slot_status_t* to, unsigned char from) {
	to->nth_slot_succ = (slot_succ_t)(from & 0x07); //bit0, 1, 2; direct conversion
	to->is_crc_err	  = (from >> 3) & 0x07;  // bit3, 4, 5
	to->num_symerr	  = (symerr_t)((from >> 6) & 0x03); // bit6, 7
}

void unpack_network_monitor_data(network_monitor_data_t* to, struct network_monitor_data_packed* from) {
	//1. cm_set
	to->cm_set.rssi_bg				= (int)(from->_rssi_bg); //char->int, sign taken care automatically
	to->cm_set.tx_power				= (int)(from->_tx_power); //char->int, sign taken care automatically
	to->cm_set.command_response 	= (command_type_t)(from->_command_response);
	to->cm_set.loopback_mode 		= (loopback_mode_t)(from->_loopback_mode);

	//2. some other SU-only info
	to->rssi = (int)(from->_rssi);
	int i;
	for (i = 0; i < MAX_NO_DN_PACKETS; i++) {
		unpack_su_slot_status(&(to->dnlink_stat[i]), from->_dnlink_stat[i]);
	}
	unpack_su_slot_status(&(to->ncp_stat), from->_ncp_stat);
	to->recv_delay = ntohs_comms(from->_recv_delay);

	//some values within cm_set and SU-only info are "switched" (one at a time) by _misc_info, others are "invalid"
	//these "invalid" are related to "null" or "blank" in storage
	to->cm_set.temperature_in_cel	= 0;
	for (i=0; i<2; i++) {
		to->cm_set.firmware_version.value[i]	= 0;
		to->cm_set.software_version.value[i]	= 0;
	}
	to->rx_lna_mode = 0;
	to->rx_vga_gain = 0;

	to->misc_info = (misc_info_type)(from->_misc_info);
	switch(to->misc_info) {
		case FIRMWARE_VER:
			to->cm_set.firmware_version.value[0] = (int)(from->_misc_data[0]);
			to->cm_set.firmware_version.value[1] = (int)(from->_misc_data[1]);
			break;
		case SOFT_VER:
			to->cm_set.software_version.value[0] = (int)(from->_misc_data[0]);
			to->cm_set.software_version.value[1] = (int)(from->_misc_data[1]);
			break;
		case RF_TEMPERATURE:
			to->cm_set.temperature_in_cel	= (int)(from->_misc_data[0]);
			break;
		case RX_LNAVGA_SETTINGS:
			to->rx_lna_mode	= (int)((lna_mode_t)(from->_misc_data[0]));
			to->rx_vga_gain = (int)(from->_misc_data[1]);
			break;
		default:
			break;
	}
}

int is_frame_end_soft() {
	if (New_frame == 1) {
		New_frame = 0; //handshake reset
		return 1;
	}
	return 0;
}

int is_send_sequence_fin() {
	if (Send_sequence_fin == 1) {
		Send_sequence_fin = 0; //handshake reset
		return 1;
	}
	return 0;
}

void get_monitoring_info(monitoring_info_aggr_t* p_mon_info_aggr) {
	//for export and used in main(), pointer assignment
	p_mon_info_aggr->id = Hdr.slot_attr.SUorBS_ID;
	p_mon_info_aggr->p_nmd = P_nmd;
	p_mon_info_aggr->p_datapmon = &DatapMon;
	p_mon_info_aggr->p_timing = &Timing;
}

int is_dupack(slot_attr_t* p, unsigned char frame_count, int new_frame)
{
	int retval = 0;
	int i;

	//if "soft" frame change: list reset!
	if (new_frame == 1) {
		Received_packet_list_size = 0;
	}

	// packet already in list?
	for (i = 0; i < Received_packet_list_size; i++) { //forward check(from oldest to latest), higher chance
		if ( (p->SUorBS_ID == Received_packet_attr_list[i].SUorBS_ID) &&
			 (p->sub_packet_no == Received_packet_attr_list[i].sub_packet_no) &&
			 (frame_count == Received_packet_frame_count_list[i]) ) { //if all these identical --> duplicated packets!
			retval = 1;
			break;
			//printf("Double with list no: %i",i);
		}
	}
	// packet is not in the list, add dupack_remover_helper to the list
	if (retval == 0 && Received_packet_list_size < MAX_NO_OF_SLOTS) {
		Received_packet_attr_list[Received_packet_list_size] = *p; //object copy!
		Received_packet_frame_count_list[Received_packet_list_size] = frame_count;
		Received_packet_list_size++;
	}
	return retval;
}

unsigned long get_crc1(unsigned char *p, unsigned int ls_data_length) {
	return conv_u8_arr(&p[ls_data_length], CRC1_BYTES);
}

unsigned long check_crc1(struct datap_i_packed* p_hdr, unsigned char *p, unsigned short ls_data_length) {
	unsigned long crc = 0xffffffff;
	crc = chksum_crc32_cont(crc, &(p_hdr->_packet_id), 3, false); //first crc the last part of header, 3 is packet id plus ls_data_length
	crc = chksum_crc32_cont(crc, p, ls_data_length, true); // ls_data[0..540]
	return crc;
}

void set_crc1(struct datap_o_packed* p_hdr, unsigned char *p, unsigned short ls_data_length) {
	unsigned long crc = 0xffffffff;
	crc = chksum_crc32_cont(crc, &(p_hdr->_packet_id), 3, false); //first crc the last part of header, 3 is packet id plus ls_data_length
	crc = chksum_crc32_cont(crc, p, ls_data_length, true); // ls_data[0..540]
	conv_to_u8_arr(&p[ls_data_length], crc, CRC1_BYTES);
}

int recv_next(unsigned char* p, unsigned short* p_id)
{
	struct datap_i_packed _hdr;
	struct su_timing_packed _timing;

	P_nmd = NULL;
	DatapMon.throughput_in_byte = 0;

	//read packet from driver
	if (read_datap(&_hdr, &_timing, p, &(Hdr.ls_data_length)) < 0) {
		D_layer_acc.D_recvRdErr++;
		return -1; //read fail
	}

	//unpack timing and part of header for "soft frame end" detection
	Timing.frame_count = _timing._frame_count;
	Timing.slot_count = ntohs_comms(_timing._slot_count);
	unpack_slot_attr(&Hdr.slot_attr, _hdr._slot_attribute);
	*p_id = Hdr.slot_attr.SUorBS_ID;

	//do soft frame end detection
	int new_frame = 0;
	Current_frame_count = Timing.frame_count;
	if (Current_frame_count != Last_frame_count && Last_frame_count != -1) {
		new_frame = 1;
		New_frame = 1; //for handshake
	}
	Last_frame_count = Current_frame_count;

	//check if is duplicated packet "dupack", if its duplicated, nothing should be done
	if ( is_dupack(&Hdr.slot_attr, Timing.frame_count, new_frame) ) { //duplicated
		D_layer_acc.D_recvDupack++;
		return -2; //dupack
	}

	//unpack rest of the header
	Hdr.destination = (destination_t)(_hdr._packet_id >> OFS_PKT_ID_DESTINATION);
	Hdr.service_id	= _hdr._packet_id & MSK_PKT_ID_SERVICE;
	unpack_network_monitor_data(&Hdr.nmd, &_hdr._nmd);
	P_nmd = &Hdr.nmd;

	//do ls data length check: null packet
	if (Hdr.ls_data_length == 0) {
		D_layer_acc.D_recvNullPack++;
		increase_valid_packet_counter(Hdr.slot_attr.SUorBS_ID); //null packet counts as valid
		return -3; //null packet
	}

	//do crc1 check
	unsigned long crc1_recv = get_crc1(p, Hdr.ls_data_length);
	unsigned long crc1_cal  = check_crc1(&_hdr, p, Hdr.ls_data_length);
	if (crc1_recv != crc1_cal) {
		D_layer_acc.D_recvCRC1fail++;
		return -4; //crc1 error packet
	}
	DatapMon.throughput_in_byte = Hdr.ls_data_length; //statistic
	increase_valid_packet_counter(Hdr.slot_attr.SUorBS_ID); //non null packet with crc1 correct counts as valid

	return 0;
}

int send_next(unsigned char* p, unsigned short payload_length) {
	struct datap_o_packed _hdr;
	destination_t destination = (Loopback_test == 1)?LOCAL_GATEWAY:REMOTE_HOST;

	//set packet id
	_hdr._packet_id = (destination<<OFS_PKT_ID_DESTINATION)|(Next_service_id&MSK_PKT_ID_SERVICE);
	//set ls_data_length
	_hdr._ls_data_length = htons_comms(payload_length);
	//set crc1
	set_crc1(&_hdr, p, payload_length); //no boundary check, caller's duty

	// send data_packet to base station
	// try_write_datap() is more cautious, but slower (check again if stall and if service_id acceptable)
	if (try_write_datap(&_hdr, p, payload_length, Next_service_id) < 0){
		D_layer_acc.D_sendfail++;
		fprintf(stderr, "%s: \nCould *not* send data packet! maybe flow control malfunction or write fail\n", __FUNCTION__);
		return -1;
	}

	//log wireless stat to a log file
	//int n = get_slot_opened();
	//Station_stat.bandwidth_in_bytes_max_acc += n*;
	//todo

	Next_service_id ++;

	return 0; //successful
}

int is_datap_to_read() {
	return (is_bit_set(DARD_DATA_HANDSHAKE_OFS, OFS_DARD_DATA_HANDSHAKE_FULL));
}

/****************************************************************/
/* Functions for sending data packets to base station			*/
/****************************************************************/
void prepare_packet_send(int mode)
{
	int i;
	struct datap_o_packed _hdr;

	datap_o_t hdr;
	hdr.destination = (Loopback_test == 1)?LOCAL_GATEWAY:REMOTE_HOST;
	hdr.service_id	= 0; //only 0 is always feasible
	hdr.ls_data_length = 540; //for test

	unsigned char p[1024];

	static int BW_limit_counter = 0;

	struct daru_buffer_control_packed packed;
	if (read_address(DARU_DATA_HANDSHAKE_OFS, &packed, sizeof(packed)) >= 0) { //successful read
		if (is_not_stall(&packed)) { //not pending
			if (BW_limit_counter == 8) {
				BW_limit_counter = 0;
				Send_sequence_fin = 1;
			}
			//set packet id
			_hdr._packet_id = (hdr.destination << OFS_PKT_ID_DESTINATION) | (hdr.service_id & MSK_PKT_ID_SERVICE);
			//set ls_data_length
			_hdr._ls_data_length = htons_comms(hdr.ls_data_length);

			//fill data with all random
			for (i = 0; i < hdr.ls_data_length; i++) {
				p[i] = rand()%256;
			}
			//set timestamp
			conv_to_u8_arr(&p[TIMESTAMP_TEST_OFS], clock(), sizeof(clock_t));
			//set running number
			conv_to_u8_arr(&p[RUNNINGNO_TEST_OFS], Send_run_no, sizeof(Send_run_no));

			//set crc1
			set_crc1(&_hdr, p, hdr.ls_data_length); //no boundary check, caller's duty

			// send data_packet to base station
			// try_write_datap() is more cautious, but slower (check again if stall and if service_id acceptable)
			if (try_write_datap(&_hdr, p, hdr.ls_data_length, hdr.service_id) < 0){
				fprintf(stderr, "%s: \nCould *not* send data packet! flow control malfunction\n", __FUNCTION__);
			}

			Send_run_no++; //auto wrapping
		}
	} else {
		fprintf(stderr, "%s: read daru buffer control\n", __FUNCTION__);
		BW_limit_counter = 0;
		Send_sequence_fin = 1; //error fin
	}
}

void congest_packet_recv(int mode)
{
	unsigned char p[1024];
	unsigned short id;
	int ret = recv_next(p, &id);
	if (ret >= 0) {
		unsigned char recv_run_no = conv_u8_arr(&p[RUNNINGNO_TEST_OFS], sizeof(recv_run_no));
		clock_t send_time = conv_u8_arr(&p[TIMESTAMP_TEST_OFS], sizeof(clock_t));
		float diff = (clock() - send_time)/(float)CLOCKS_PER_SEC;

		printf("run_no: %d, latency: %.2f\n", recv_run_no, diff);

		if ( (Last_recv_run_no != -1) && (recv_run_no != (Last_recv_run_no+1)%256) ) {
			fprintf(stderr, "%s: !!!Test packet loss detected on lost running no!!!\n", __FUNCTION__);
		}
		Last_recv_run_no = recv_run_no;
	}
	else if (ret == -1) {
		fprintf(stderr, "%s: read_datap fail\n", __FUNCTION__);
	}
	else if (ret == -2) {
		fprintf(stderr, "%s: dupack\n", __FUNCTION__);
	}
	else if (ret == -3) {
		fprintf(stderr, "%s: null packet\n", __FUNCTION__);
	}
	else if (ret == -4) {
		fprintf(stderr, "%s: crc1 error\n", __FUNCTION__);
	}
	else {
		fprintf(stderr, "%s: unknown error\n", __FUNCTION__);
	}
}


#ifndef SIM
void init_d_layer() {}

//combined flow control
int d_get_send_rdy() {
	unsigned char service_id;
	int is_acceptable_id_found = 0;

	struct daru_buffer_control_packed packed;
	//read status
	if (read_address(DARU_DATA_HANDSHAKE_OFS, &packed, sizeof(packed)) < 0) { //insuccessful read
		fprintf(stderr, "%s: read daru buffer control\n", __FUNCTION__);
		return -1;
	}
	//check daru readiness
	if (is_not_stall(&packed) == 0) { //DARU still full (BS not finished with the packet before)
		return 0;
	}
	// search next acceptable service id
	for (service_id = Next_service_id; service_id < 8; service_id++) {
		if (is_send_acceptable(&packed, service_id) == 1) {
			is_acceptable_id_found = 1;
			break;
		}
	}
	//if no acceptable id found --> no more packets can be sent in this frame
	if (is_acceptable_id_found == 0){
		Next_service_id = 0; //reset state variable
		Send_sequence_fin = 1; //mark send sequence finish
		return 0;
	}
	//otherwise save it to Next_service_id for the use of send_next()
	Next_service_id = service_id;
	return 1;
}

//return packets per second
int d_get_bandwidth() {
	unsigned char result = 0;
	unsigned char service_id;
	struct daru_buffer_control_packed packed;
	if (read_address(DARU_DATA_HANDSHAKE_OFS, &packed, sizeof(packed)) < 0) {
		fprintf(stderr, "%s: read daru buffer control\n", __FUNCTION__);
		return -1;
	}

	for (service_id = 0; service_id < 4; service_id++) {
		result += (packed._slotstatus >> service_id) & 0x01; //slots opened for normal packets
	}
	result += packed._slotstatus >> 4; //slots opened for extra packets

	return result;
}

void d_recv(post_t* post, uint8_t *p, unsigned short* p_id)
{
	if (p != NULL) {
		int ret = recv_next(p, p_id);
		if (ret >= 0) {
			D_layer_acc.D_recvok++;
			set_post(post, LAYER_D, MSG_NOTE, NOTE_RECV_OK);
			return;
		} //ret < 0
		D_layer_acc.D_recvartifacts++;
		set_post(post, LAYER_D, MSG_NOTE, NOTE_RECV_NULL);
		return;
	}
	set_post(post, LAYER_D, MSG_ERR, ERR_NULLPTR);
}

void d_send(post_t* post, uint8_t *p, unsigned short payload_length)
{
	if (p != NULL) {
		int ret = send_next(p, payload_length);
		if (ret >= 0) {
			set_post(post, LAYER_D, MSG_NOTE, NOTE_SEND_OK);
			return;
		}
		set_post(post, LAYER_D, MSG_WARN, WARN_SENDBUFFBLOCKED); //lost! should be avoided
		return;
	}
	set_post(post, LAYER_D, MSG_ERR, ERR_NULLPTR);
}

#endif

