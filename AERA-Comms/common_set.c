/** @file common_set.c
 *  @brief common types used for netmon, statistictab and cmd (only partly), implementation
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */
#include <string.h>

#include "common_set.h"

char* cmd2string(command_type_t cmd) {
	char *cmd_string;
	switch (cmd) {
		case NOP: cmd_string = "no_operation"; break;
		case RESET: cmd_string = "reset"; break;
		case LOOPBACK: cmd_string = "loop_back"; break;
		default: cmd_string = ""; break;
	}
	return cmd_string;
}

command_type_t string2cmd(char* cmd_string) {
	command_type_t cmd = NOP;
	if (strcmp(cmd_string, "no_operation") == 0) {
		cmd = NOP;
	} else if (strcmp(cmd_string, "reset") == 0) {
		cmd = RESET;
	} else if (strcmp(cmd_string, "loop_back") == 0) {
		cmd = LOOPBACK;
	}
	return cmd;
}
