/** @file datap.h
 *  @brief IF (interface) class for data packet in-out from and to the base station, function prototypes
 *
 *	A funtionally complicated class, provide IO/flowcontrol service for call-over-RT/HQ protocol
 *	and plain test packets transceiving. The functions:
 *
 *	- IO functions for upper layer call and call by main()
 *	- IO stalling for flow control
 *	- "middle-out" handshake functions as simple event-signaling with main()
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#ifndef DATAP_H
#define DATAP_H

#include "common_set.h"	/* for common_monitoring_set_t */
#include "slot_attribute.h" /* for slot_attr */
#include "networkmon.h" /* for network_monitoring_data_t */

/************************************************/
/* Type and class declarations for data-packet	*/
/************************************************/
typedef enum {REMOTE_HOST = 0, LOCAL_HOST = 1, REMOTE_GATEWAY = 2, LOCAL_GATEWAY = 3} destination_t;

typedef struct {
	slot_attr_t	slot_attr;
	network_monitor_data_t nmd;
	unsigned char	service_id;
	destination_t	destination;
	unsigned short ls_data_length;
} datap_i_t;

typedef struct {
	unsigned char	service_id;
	destination_t	destination;
	unsigned short ls_data_length;
} datap_o_t;

typedef struct {
	unsigned char frame_count;
	unsigned short slot_count;
}su_timing_t; //3B

typedef struct {
	int throughput_in_byte;
} datap_monitoring_t;

typedef struct {
	unsigned short id;
	network_monitor_data_t* p_nmd;
	datap_monitoring_t* p_datapmon;
	su_timing_t*	p_timing;
} monitoring_info_aggr_t;

/** @brief [IO functions] prepare an out-going test packet (or send it right away, implementation flavor) for "main"
 *
 *	With fixed typical length (540B), fixed service_id = 0, fixed destinatio
 *
 *	@param	mode		"variety" selector, define and implement as you wish e.g. for random/iterating data generation etc...
 */
void prepare_packet_send(int mode);

/** @brief [IO functions] interpret an incoming test packet for "main"
 *	@param	mode		"variety" selector, define and implement as you wish e.g. for random/iterating data generation etc...
 */
void congest_packet_recv(int mode);

/** @brief [IO stalling] send stalling function, occupies driver
 *	@return	int 	1 sendable, 0 not sendable, <0 different insuccess
 */
int is_datap_to_read();

/** @brief [handshake functions] is frame switched? detected by packets frame information for main()
 *	@return	int 	1 is signal, 0 no signal
 */
int is_frame_end_soft();

/** @brief [handshake functions] is send sequence finished? signals that main() should cease sending
 *	@return	int 	1 is signal, 0 no signal
 */
int is_send_sequence_fin();

void get_monitoring_info(monitoring_info_aggr_t* p_mon_info_aggr);

#endif
