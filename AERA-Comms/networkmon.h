/** @file networkmon.h
 *  @brief types for NetWorkMonitoring "NWM" used for statistic and datap
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#ifndef NETWORKMON_H_
#define NETWORKMON_H_

#include "tdma_protocol.h" /* for protocol-dependent defines */
#include "common_set.h" /* for common_monitoring_set_t */

typedef enum {NONE = 0, FIRMWARE_VER = 1, SOFT_VER = 2, RF_TEMPERATURE = 3, RX_LNAVGA_SETTINGS = 4} misc_info_type;
typedef enum {UNINITIALIZED = 0, SUCC_1, SUCC_2, SUCC_3, SUCC_4, SUCC_5, SUCC_6, FAIL} slot_succ_t;
typedef enum {NO_SYMERR = 0, SINGLE_SYMERR = 1, MULTI_SYMERR = 2, MAX_SYMERR = 3} symerr_t;
typedef enum {LNA_READOUT_ERR = 0, LNA_LOW_MODE = 1, LNA_MEDIUM_MODE = 2, LNA_HIGH_MODE = 3} lna_mode_t;

typedef struct {
	slot_succ_t nth_slot_succ;
	unsigned char is_crc_err;
#define IS_CRC_ERR_OFS_MAX(nth_slot_succ) (((nth_slot_succ-1)<=3)?(nth_slot_succ-1):3)
#define IS_CRC_ERR(ofs) ((is_crc_err >> OFS) & 0x01)
	symerr_t num_symerr;
} su_slot_status_t;

// a substructure of datap:
typedef struct {
	common_monitoring_set_t cm_set;
	su_slot_status_t dnlink_stat[MAX_NO_DN_PACKETS];
	su_slot_status_t ncp_stat;
	misc_info_type misc_info;
	int rssi;
	int recv_delay;
	int rx_lna_mode;
	int rx_vga_gain;
} network_monitor_data_t;

#endif /* NETWORKMON_H_ */
