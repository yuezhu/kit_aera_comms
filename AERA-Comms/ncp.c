/** @file ncp.c
 *  @brief IF (interface) class for Network Control Packet/List (NCP or NCL) in-out from and to the base station, function prototypes
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#include <assert.h>

#include "syslogger.h"
#include "def.h"

#include "ncp.h"
#include "io.h"

#define RELATIVE_INDEXING_OFS 2 /* used for non-dynamic BS/SU hardware, as memory map uses indexing of uplink slot
(absolute from 2....), deactivate this for dynamic hardware!!!!todo, don't forget */

/************/
/*	for IO	*/
/************/
#define SBCBUF_NCP_OFS	0
//#define SBCBUF_NCP_SLOT_NUM_OFS 1564
#define SBCBUF_NCP_HANDSHAKE_OFS 1568
	#define OFS_SBCBUF_NCP_HANDSHAKE_REQUEST	1
	#define OFS_SBCBUF_NCP_HANDSHAKE_READY		0
#define BS_POWER_ON_OFS 4095

#define packed_data __attribute__((__packed__))

struct slot_packed {
	unsigned char _slot_attribute[2];
	unsigned char _station_attribute;
} packed_data; //3B

struct ncp_packed {
	unsigned char _time[4]; //sub_ofs = 0, 4B
	unsigned char _aif[23];	//sub_ofs = 4, 23B
#define AIF_TXPOWER_LOW_OFS 0
#define AIF_TXPOWER_MEDIUM_OFS 1
#define AIF_TXPOWER_HIGH_OFS 2
#define AIF_TXPOWER_MAX_OFS 3
#define AIF_MAX_BLOCKL_OFS 4
#define AIF_FUT_FREQU_OFS 6
#define AIF_CURR_FREQU_OFS 7
#define AIF_NO_FRAMES_OFS 12
#define AIF_NO_NCP_SLOTS_OFS 13
#define AIF_NO_UP_SLOTS_OFS 14
#define AIF_NO_DOWN_SLOTS_OFS 16
#define AIF_NO_ARQ_SLOTS_OFS 17
#define AIF_NO_DOWNARQ_FOLDS_OFS 18
#define AIF_FW_VERSION_OFS 20
	struct slot_packed _slot_tab[512]; //sub_ofs = 27, 512*3B = 1536B
	unsigned char _space1; // no data, 1B
	unsigned char _slot_tab_size[2]; // sub_ofs = 1564, 2B
}packed_data; //4+23+512*3 = 1563

void init_slot(struct slot_packed *ptr_ncp_slot) {
	ptr_ncp_slot->_slot_attribute[0] = 0;
	ptr_ncp_slot->_slot_attribute[1] = 0;
	ptr_ncp_slot->_station_attribute = 0;
}

/************/
/*	for IF	*/
/************/
unsigned short get_num_slots_per_frame(aif_t* p_aif)
{
	//Protocol dependent calculation!
	unsigned short num = p_aif->number_of_NCP_slots + p_aif->no_up_slots + \
				(p_aif->no_down_slots + p_aif->no_ARQ_slots)*(unsigned short)(p_aif->no_std_download_packets);
	return num;
}

int get_num_slots_assigned(int id, slot_tab_t* p) {
	int count = 0;
	int i;
	for (i = 0; i < p->size; i++) {
		if ((p->slot[i].SUorBS_ID == id) && (p->slot[i].link_direction == up)) count ++;
	}
	return count;
}

int get_num_pkts_to_recv(int id, slot_tab_t* p) {
	int count = 0;
	int i;
	int is_sub_pkt_no_assigned[MAX_NO_DN_PACKETS] = {0, 0, 0, 0, 0, 0, 0, 0};
	unsigned char spn;
	for (i = 0; i < p->size; i++) {
		if ((p->slot[i].SUorBS_ID == id) && (p->slot[i].link_direction == up)) {
			spn = p->slot[i].sub_packet_no;
			is_sub_pkt_no_assigned[spn] = 1;
		}
	}
	for (i=0; i<MAX_NO_DN_PACKETS; i++) {
		count += is_sub_pkt_no_assigned[i];
	}
	return count;
}

int is_slot_uplink(int ind, slot_tab_t* p) {
	if ( (ind >= 0) && (ind < p->size) ) {
		if (IS_SUorBS_ID_SUID(p->slot[ind].SUorBS_ID) && (p->slot[ind].link_direction == up)) {
			return 1;
		}
	}
	return 0;
}

void pack_aif(struct ncp_packed* to, aif_t* from) {
	to->_aif[AIF_TXPOWER_LOW_OFS]		= from->tx_power_low;
	to->_aif[AIF_TXPOWER_MEDIUM_OFS]	= from->tx_power_medium;
	to->_aif[AIF_TXPOWER_HIGH_OFS] 		= from->tx_power_high;
	to->_aif[AIF_TXPOWER_MAX_OFS] 		= from->tx_power_max;
	conv_to_u8_arr(&(to->_aif[AIF_MAX_BLOCKL_OFS]), from->max_block_length, 2); //2 bytes to big endian network order
	to->_aif[AIF_FUT_FREQU_OFS]			= from->future_freq_channel;
	to->_aif[AIF_CURR_FREQU_OFS]		= from->current_freq_channel;
	to->_aif[AIF_NO_FRAMES_OFS] 		= from->number_of_frames;
	to->_aif[AIF_NO_NCP_SLOTS_OFS]		= from->number_of_NCP_slots;
	conv_to_u8_arr(&(to->_aif[AIF_NO_UP_SLOTS_OFS]), from->no_up_slots, 2); //2 bytes to big endian network order
	to->_aif[AIF_NO_DOWN_SLOTS_OFS]		= from->no_down_slots;
	to->_aif[AIF_NO_ARQ_SLOTS_OFS]		= from->no_ARQ_slots;
	to->_aif[AIF_NO_DOWNARQ_FOLDS_OFS]	= from->no_std_download_packets;
}

void unpack_aif(aif_t* to, struct ncp_packed *from) {
	to->tx_power_low		= from->_aif[AIF_TXPOWER_LOW_OFS];
	to->tx_power_medium	= from->_aif[AIF_TXPOWER_MEDIUM_OFS];
	to->tx_power_high		= from->_aif[AIF_TXPOWER_HIGH_OFS];
	to->tx_power_max		= from->_aif[AIF_TXPOWER_MAX_OFS];
	to->max_block_length	= (unsigned int)conv_u8_arr(&(from->_aif[AIF_MAX_BLOCKL_OFS]), 2);
	to->future_freq_channel	= from->_aif[AIF_FUT_FREQU_OFS];
	to->current_freq_channel	= from->_aif[AIF_CURR_FREQU_OFS];
	to->number_of_frames		= from->_aif[AIF_NO_FRAMES_OFS];
	to->number_of_NCP_slots	= from->_aif[AIF_NO_NCP_SLOTS_OFS];
	to->no_up_slots			= (unsigned short)conv_u8_arr(&from->_aif[AIF_NO_UP_SLOTS_OFS], 2);
	to->no_down_slots			= from->_aif[AIF_NO_DOWN_SLOTS_OFS];
	to->no_ARQ_slots			= from->_aif[AIF_NO_ARQ_SLOTS_OFS];
	to->no_std_download_packets	= from->_aif[AIF_NO_DOWNARQ_FOLDS_OFS];
}

void print_aif(aif_t* p_aif) {
	char message[256];
	sprintf(message, "AIF tx_power_low         : %i \n", p_aif->tx_power_low); print_message(LOG_INFO, message);
	sprintf(message, "AIF tx_power_medium      : %i \n", p_aif->tx_power_medium); print_message(LOG_INFO, message);
	sprintf(message, "AIF tx_power_high        : %i \n", p_aif->tx_power_high); print_message(LOG_INFO, message);
	sprintf(message, "AIF tx_power_max         : %i \n", p_aif->tx_power_max); print_message(LOG_INFO, message);
	sprintf(message, "AIF max_block_length     : %i \n", p_aif->max_block_length); print_message(LOG_INFO, message);
	sprintf(message, "AIF future_freq_channel  : %i \n", p_aif->future_freq_channel); print_message(LOG_INFO, message);
	sprintf(message, "AIF current_freq_channel : %i \n", p_aif->current_freq_channel); print_message(LOG_INFO, message);
	sprintf(message, "AIF number_of_frames     : %i \n", p_aif->number_of_frames); print_message(LOG_INFO, message);
	sprintf(message, "AIF number of NCP slots  : %i \n", p_aif->number_of_NCP_slots); print_message(LOG_INFO, message);
	sprintf(message, "AIF number of UP slots   : %i \n", p_aif->no_up_slots); print_message(LOG_INFO, message);
	sprintf(message, "AIF number of DOWN slots : %i \n", p_aif->no_down_slots); print_message(LOG_INFO, message);
	sprintf(message, "AIF number of ARQ slots  : %i \n", p_aif->no_ARQ_slots); print_message(LOG_INFO, message);
	sprintf(message, "AIF number of std packets: %i \n", p_aif->no_std_download_packets); print_message(LOG_INFO, message);
}

void init_slot_tab(slot_tab_t* p)
{
	int i;
	// initialize slot table of NCP
	for (i = 0; i < MAX_NO_OF_SLOTS; i++) {
		init_slot_attr(&(p->slot[i]), up, 0, 0);
	}
	p->size = 0;
}

void pack_slot_tab_with_station_table(struct ncp_packed *to, slot_tab_t *from,
		struct station_attr_hash* p_station_table)  {
	int i, ind;
	station_attr_t *p;
	conv_to_u8_arr(to->_slot_tab_size, (from->size - RELATIVE_INDEXING_OFS), 2); //pack up_slot_table_size CORRECTED!, the slot_size!
	for (i = RELATIVE_INDEXING_OFS; i < from->size; i++) {
		ind = i - RELATIVE_INDEXING_OFS;
		pack_slot_attr(to->_slot_tab[ind]._slot_attribute, &(from->slot[i]));
		if (is_slot_uplink(i, from)) { //for all valid uplink slots
			//search station_attribute from station_attribute hash table with ID as key (no obj copy version)
			find_station_attr_in_table(p_station_table, from->slot[i].SUorBS_ID, &p);
			//abort on not found, should not happen,
			//unless load_ncp_from_xml() is not called or xml not valid
			//use is_ncp_valid() to ensure a ncp is valid (if xml is trustworthy, this overhead could be avoided)
			assert(p != NULL);
			//save attribute (can be duplicated, when one SU is assigned to multiple slots)
			pack_station_attr(&(to->_slot_tab[ind]._station_attribute), p);
		}
		else { //fill zero, if fetched, these will show as "awaiting..., 0, 0"
			to->_slot_tab[ind]._station_attribute = 0;
		}
	}
}

void unpack_slot_tab(slot_tab_t *to, station_attr_t to_s_a_arr[], struct ncp_packed *from)  {
	int i, ind;
	to->size = conv_u8_arr(from->_slot_tab_size, 2);
	if (to->size > (MAX_NO_OF_SLOTS-RELATIVE_INDEXING_OFS)) to->size = (MAX_NO_OF_SLOTS-RELATIVE_INDEXING_OFS); //ensure
	for (ind = 0; ind < to->size; ind++) {
		i = ind + RELATIVE_INDEXING_OFS;
		//get slot attribute from each slot
		unpack_slot_attr(&(to->slot[i]), from->_slot_tab[ind]._slot_attribute);
		//get slot attribute from each slot
		unpack_station_attr(&to_s_a_arr[i], from->_slot_tab[ind]._station_attribute);
	}
}

//only once (a must) for a ncp_t obj, not used as reset
void init_ncp(ncp_t* p) {
	//station_attr_table
	p->p_station_table = NULL;
}

//don't use this for uninitialized ncp_t obj
//the pointer intialization is not guranteed to be NULL
//(insecure for library call as its behavior is unknown)
void cleanup_ncp(ncp_t* p) {
	//station_attr_table
	if (p->p_station_table != NULL) {
		delete_station_attr_table(&(p->p_station_table));
		p->p_station_table = NULL; //ensure
	}
}

//check if all uplink slots has su_id already having an entry in station_table or no uplink slots assigned
int is_ncp_valid(ncp_t* p) {
	int i;
	station_attr_t* p_station_attr;
	for (i = 0; i < p->slot_tab.size; i++) {
		if ( (p->slot_tab.slot[i].link_direction == up) &&
			 IS_SUorBS_ID_SUID(p->slot_tab.slot[i].SUorBS_ID) ) { //for all valid uplink slots
			//search station_attribute from station_attribute hash table with ID as key
			find_station_attr_in_table(p->p_station_table, p->slot_tab.slot[i].SUorBS_ID, &p_station_attr);
			if (p_station_attr == NULL) return 0; //
		}
	}
	return 1;
}

int deliver_ncp(ncp_t* p) {
	struct ncp_packed packed;
	memset(&packed, 0, sizeof(struct ncp_packed));

	//packing
	conv_to_u8_arr(packed._time, (unsigned long)(p->time), 4); //pack time
	pack_aif(&packed, &(p->aif));
	pack_slot_tab_with_station_table(&packed, &(p->slot_tab), p->p_station_table);

	//write
	//side-effect is known and correct: SBCBUF_NCP_HANDSHAKE_REQUEST set back to 0 implicitly
	if (write_address_handshake(SBCBUF_NCP_OFS, &packed, sizeof(packed),
								SBCBUF_NCP_HANDSHAKE_OFS, OFS_SBCBUF_NCP_HANDSHAKE_READY) < 0) {
		return -1; //fail
	}
	return 0;
}

int fetch_and_print_ncp(ncp_t* p) {
	struct ncp_packed packed;
	station_attr_t s_a_arr[MAX_NO_OF_SLOTS];

	//read
	if (read_address(SBCBUF_NCP_OFS, &packed, sizeof(packed)) < 0) {
		return -1; //fail
	}

	//unpacking
	unpack_aif(&(p->aif), &packed);
	unpack_slot_tab(&(p->slot_tab), s_a_arr, &packed);
	p->time = (time_t)conv_u8_arr(packed._time, 4); //unpack time

	//print
	int i;
	char message[256];
	unsigned short num = get_num_slots_per_frame(&(p->aif));
	//print a title
	sprintf(message, "NCP: %i slots per frame with %i uplink-slots\n", num, p->aif.no_up_slots); print_message(LOG_INFO, message);
	//print time
	//print aif
	print_aif(&(p->aif));

	//print up_slot_table
	for (i = 0; i < p->slot_tab.size; i++) {
		//print both at the same time
		sprintf(message, "Slot %i: ID: %i, sub_packet_no: %i, link dir: %s, Link status: %s, Tx-delay: %i, Tx-power: %i \n",
				i, p->slot_tab.slot[i].SUorBS_ID, p->slot_tab.slot[i].sub_packet_no, (p->slot_tab.slot[i].link_direction == up)?"up":"down",
				link_status2string(s_a_arr[i].link_status), s_a_arr[i].transmission_delay, s_a_arr[i].transmission_power);
		print_message(LOG_INFO, message);
	}
	return 0;
}


int is_bs_available() {
	unsigned char c;
	//read
	if (read_address(BS_POWER_ON_OFS, &c, sizeof(c)) < 0) {
		return -1; //fail
	}
	if (c == BS_POWER_ON_IDENTIFIER) return 1;
	return 0;
}

int is_bs_need_ncp() {
	return (is_bit_set(SBCBUF_NCP_HANDSHAKE_OFS,
				   	   OFS_SBCBUF_NCP_HANDSHAKE_REQUEST));
}
