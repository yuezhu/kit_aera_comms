/** @file station_stat_table.h
 *  @brief hash structure of station_stat for and hash table handling method prototype
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#ifndef STATION_STAT_TABLE_H_
#define STATION_STAT_TABLE_H_

#include "uthash.h" /* hash library */
#include "station_stat.h"

/** @brief wrapper-like structure arround station_stat_t, needed for hash
 */
struct station_stat_hash {
	int hash_id;  // SUorBS_ID from ncp_slot_statibute will be used here
	station_stat_t station_stat;
	UT_hash_handle hh; // make this structure hashable
};

/** @brief add a station_stat_t obj and the key to the handle, uniqueness guranteed, obj copy
 *
 *  This function has malloc! the delete_xxx() should be manually done, especially
 *  when the table handle is created on stack. Suggest to have the handle as global variable
 *  or on heap (malloc)
 *
 * 	@param [out] p_table	pointer to the handle for the table, the handle can be changed
 * 	@param [in]	id			the hash key
 * 	return	int  1 for success, 0 for fail (key exists)
 */
int add_station_stat_to_table(struct station_stat_hash** p_table, int id);

/** @brief find a station_stat_t obj with SU id as the key, pointer to obj in hash table returned
 * 	@param [in]	table		handle to the table
 * 	@param [in]	id			the hash key
 * 	@param [out] pp_stat	pointer to obj handle in hash table, overwritten on found, otherwise NULL
 */
void find_station_stat_in_table(struct station_stat_hash* table, int id, station_stat_t** pp_stat);

/** @brief find a station_stat_t obj with SU id as the key, obj copy
 * 	@param [in]	table		handle to the table
 * 	@param [in]	id			the hash key
 * 	@param [out] p_stat		obj handle in hash table overwritten on found
 * 	return	int  1 for success, 0 for fail (not found)
 */
int find_station_stat_in_table_copy(struct station_stat_hash* table, int id, station_stat_t* p_stat);

/** @brief find a station_stat_t obj with SU id as the key, overwrite it with the given obj, obj copy
 * 	@param [in]	table		handle to the table
 * 	@param [in]	id			the hash key
 * 	@param [in] p_stat		obj handle to overwrite the hash obj found
 * 	return	int  1 for success, 0 for fail (not found)
 */
int modify_station_stat_in_table(struct station_stat_hash* table, int id, station_stat_t* p_stat);


/** @brief find a station_stat_t obj with SU id as the key, delete it
 *
 * This function has free! used pairwise for the corresponding add_xxx()
 *
 * 	@param [in, out] p_table	pointer to the handle for the table, the handle can be changed
 * 	@param [in]	id			the hash key
 * 	return	int  1 for success, 0 for fail (not found)
 */
int delete_station_stat_in_table(struct station_stat_hash** p_table, int id);

/** @brief delete all obj in table
 *
 * This function has free! used once for before table handle is dead
 *
 * 	@param [in, out] p_table	pointer to the handle for the table, the handle can be changed
 */
void delete_station_stat_table(struct station_stat_hash** p_table);

#endif /* STATION_STAT_TABLE_H_ */
