/** @file station_attr_table.h
 *  @brief hash structure of station_attr for and hash table handling method prototype
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#ifndef STATION_ATTR_TABLE_H_
#define STATION_ATTR_TABLE_H_

#include "uthash.h" /* hash library */
#include "station_attribute.h"

/** @brief wrapper-like structure arround station_attr_t, needed for hash
 */
struct station_attr_hash {
	int hash_id;  // SUorBS_ID from ncp_slot_attribute will be used here
	station_attr_t station_attr;
	UT_hash_handle hh; // make this structure hashable
};

/** @brief add a station_attr_t obj and the key to the handle, uniqueness guranteed, obj copy
 *
 *  This function has malloc! the delete_xxx() should be manually done, especially
 *  when the table handle is created on stack. Suggest to have the handle as global variable
 *  or on heap (malloc)
 *
 * 	@param [out] p_table	pointer to the handle for the table, the handle can be changed
 * 	@param [in]	id			the hash key
 * 	@param [in] p_attr		obj handle
 * 	return	int  1 for success, 0 for fail (key exists)
 */
int add_station_attr_to_table(struct station_attr_hash** p_table, int id, station_attr_t* p_attr);

/** @brief find a station_attr_t obj with SU id as the key, pointer to obj in hash table returned
 * 	@param [in]	table		handle to the table
 * 	@param [in]	id			the hash key
 * 	@param [out] pp_attr	pointer to obj handle in hash table, overwritten on found, otherwise NULL
 */
void find_station_attr_in_table(struct station_attr_hash* table, int id, station_attr_t** pp_attr);

/** @brief find a station_attr_t obj with SU id as the key, obj copy
 * 	@param [in]	table		handle to the table
 * 	@param [in]	id			the hash key
 * 	@param [out] p_attr		obj handle in hash table overwritten on found
 * 	return	int  1 for success, 0 for fail (not found)
 */
int find_station_attr_in_table_copy(struct station_attr_hash* table, int id, station_attr_t* p_attr);

/** @brief find a station_attr_t obj with SU id as the key, overwrite it with the given obj, obj copy
 * 	@param [in]	table		handle to the table
 * 	@param [in]	id			the hash key
 * 	@param [in] p_attr		obj handle to overwrite the hash obj found
 * 	return	int  1 for success, 0 for fail (not found)
 */
int modify_station_attr_in_table(struct station_attr_hash* table, int id, station_attr_t* p_attr);


/** @brief find a station_attr_t obj with SU id as the key, delete it
 *
 * This function has free! used pairwise for the corresponding add_xxx()
 *
 * 	@param [in, out] p_table	pointer to the handle for the table, the handle can be changed
 * 	@param [in]	id			the hash key
 * 	return	int  1 for success, 0 for fail (not found)
 */
int delete_station_attr_in_table(struct station_attr_hash** p_table, int id);

/** @brief delete all obj in table
 *
 * This function has free! used once for before table handle is dead
 *
 * 	@param [in, out] p_table	pointer to the handle for the table, the handle can be changed
 */
void delete_station_attr_table(struct station_attr_hash** p_table);

#endif /* STATION_ATTR_TABLE_H_ */
