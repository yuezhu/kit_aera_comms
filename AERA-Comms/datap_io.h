/** @file datap_io.h
 *  @brief io class for data packet in-out from and to the base station, function prototypes
 *
 *	An extended IO class from io.h/io.c to implement special I/O functions
 *	this IO class has more "information-awareness" than the basic IO class
 *	for higher performance and security in packet transfer.
 *	It does interpretation of
 *	- handshake bits
 *	- send "acceptance" and "stalling"
 *	- ls_data_length and service_id
 *	The interpretation of these are offered as service for IF class (IF class don't do it again)
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#ifndef DATAP_IO_H_
#define DATAP_IO_H_

#include "io.h" /* make basic I/O functions transparent */

#define DATA_ASSEMBLY_RAM_UP_OFS 8192
#define DARU_DATA_HANDSHAKE_OFS 9212 // 8192 + 1020
#define DATA_ASSEMBLY_RAM_DOWN_OFS 12288
#define DARD_DATA_HANDSHAKE_OFS 13308 // 12288 + 1020
	#define OFS_DARD_DATA_HANDSHAKE_FULL	0
#define DARD_DATA_TIMING_OFS 13309 // 12288 + 1020 + 1

#define packed_data __attribute__((__packed__))

struct daru_buffer_control_packed {
	unsigned char _darustatus;
#define MSK_DARU_FULL		0x01
#define MSK_DARU_START		0x02
#define MSK_DARU_MAYBE_LOST	0x04
#define MSK_DARU_OVERFRAME_WARNING	0x08
	unsigned char _bufferstatus;
	unsigned char _slotstatus;
}packed_data;

int is_send_acceptable(struct daru_buffer_control_packed* p, unsigned char service_id);
int is_not_stall(struct daru_buffer_control_packed* p);

struct su_timing_packed {
	unsigned char _frame_count; //byte 0
	unsigned short _slot_count; //byte 1, 2
}packed_data; //3B

struct network_monitor_data_packed {
	unsigned char _dnlink_stat[8]; //sub-ofs = 0
	unsigned char _ncp_stat;    //sub-ofs = 8
	signed char _rssi;	//sub-ofs = 9
	signed char _rssi_bg; //sub-ofs = 10
	unsigned short _recv_delay;  //sub-ofs = 11, 2B
	unsigned char _command_response; //sub-ofs = 13
	unsigned char _loopback_mode; //sub-ofs = 14
	signed char _tx_power; //sub-ofs = 15
	unsigned char _misc_info; //sub-ofs = 16
	unsigned char _misc_data[2]; //sub-ofs = 17, 2B
	unsigned char _space; //no data, 1B
}packed_data; //20B

struct datap_i_packed {
	unsigned char _slot_attribute[2];
	struct network_monitor_data_packed _nmd;
	unsigned char _packet_id;
#define OFS_PKT_ID_DESTINATION 6
#define MSK_PKT_ID_SERVICE	0x07
	unsigned short _ls_data_length;
}packed_data;

struct datap_o_packed {
	unsigned char _packet_id;
	unsigned short _ls_data_length;
}packed_data;

#define CRC1_BYTES 4

int write_datap(struct datap_o_packed *p_hdr, unsigned char* p, unsigned short ls_data_length);

int try_write_datap(struct datap_o_packed *p_hdr, unsigned char* p, unsigned short ls_data_length,
		unsigned char service_id);

int read_datap(struct datap_i_packed *p_hdr, struct su_timing_packed* p_timing,
				unsigned char* p, unsigned short* p_ls_data_length);

//int register_interrupt(int inttype, .....); //Fridtjof

#endif /* DATAP_IO_H_ */
