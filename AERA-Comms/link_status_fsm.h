/** @file link_status_fsm.h
 *  @brief helper class for to handle link status based on station_attr_hash, function prototypes
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#ifndef LINK_STATUS_FSM_H_
#define LINK_STATUS_FSM_H_

#include "station_attr_table.h" /* for struct station_attr_hash */

void init_link_status_fsm(struct station_attr_hash* table, unsigned char number_of_frame);

int increase_valid_packet_counter(unsigned short id);

int update_link_status();


#endif /* LINK_STATUS_FSM_H_ */
