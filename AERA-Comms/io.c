/** @file io.c
 *  @brief general IO class for basic IO mode, function implementation
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

#include "io.h"

//Fridtjof

/* file descriptor for gpio (i-)driver, exported to *io.c */
int Fh = -1;

/* if the "fh" is opened externally aka. in main().
 * in this case, io functions don't need to open() and close()
 * it on each io operation
 */
int Is_fh_extern_opened = 0;

int is_bit_set(off_t hs_addr, unsigned char bit_ofs) {
	unsigned char hs;
	int result = -1;
	for (;;) {
		if (Is_fh_extern_opened == 0) { //Fridtjof
			if ((Fh = open("/dev/GPIO-driver", O_RDWR)) == -1) {
				perror("Can't open /dev/GPIO-driver!");
				break;
			}
		}
		if (lseek(Fh, hs_addr, SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		if (read(Fh, &hs, 1) == -1) {
			perror("read");
			break;
		}
		//success
		result = (hs >> bit_ofs) & 0x01;
		break;
	}
	//clean up
	if (Is_fh_extern_opened == 0) { //Fridtjof
		if (Fh != -1) close(Fh);
		Fh = -1;
	}
	return result;
}

int read_address(off_t addr, void* to, size_t size) {
	int result = -1;
	for (;;) {
		if (Is_fh_extern_opened == 0) {
			if ((Fh = open("/dev/GPIO-driver", O_RDWR)) == -1) {
				perror("Can't open /dev/GPIO-driver!");
				break;
			}
		}

		if (lseek(Fh, addr, SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		if (read(Fh, to, size) == -1) {
			perror("read");
			break;
		}
		result = 0;
		break;
	}
	//clean up
	if (Is_fh_extern_opened == 0) {
		if (Fh != -1) close(Fh);
		Fh = -1;
	}
	return result;
}

int write_address(off_t addr, void* from, size_t size)	{
	int result = -1;
	for (;;) {
		if (Is_fh_extern_opened == 0) {
			if ((Fh = open("/dev/GPIO-driver", O_RDWR)) == -1) {
				perror("Can't open /dev/GPIO-driver!");
				break;
			}
		}
		if (lseek(Fh, addr, SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		if (write(Fh, from, size) == -1) {
			perror("write");
			break;
		}
		//success
		result = 0;
		break;
	}
	//clean up
	if (Is_fh_extern_opened == 0) {
		if (Fh != -1) close(Fh);
		Fh = -1;
	}
	return result;
}

int read_address_handshake(off_t addr, void* to, size_t size, off_t hs_addr, unsigned char bit_ofs) {
	unsigned char hs;
	int result = -1;
	for (;;) {
		if (Is_fh_extern_opened == 0) {
			if ((Fh = open("/dev/GPIO-driver", O_RDWR)) == -1) {
				perror("Can't open /dev/GPIO-driver!");
				break;
			}
		}
		//is handshake bit set?
		if (lseek(Fh, hs_addr, SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		if (read(Fh, &hs, 1) == -1) {
			perror("read");
			break;
		}
		if (((hs >> bit_ofs) & 0x01) == 0) {
			result = -2;
			break;
		}
		//set, data available! --> read from BS
		if (lseek(Fh, addr, SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		if (read(Fh, to, size) == -1) {
			perror("read");
			break;
		}
		// finish --> indicate read finished to BS
		if (lseek(Fh, hs_addr, SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		hs = 0; //just write back 0! take care of side-effect (if other bits are defined)
		if (write(Fh, &hs, 1) == -1) {
			perror("write");
			break;
		}
		//success
		result = 0;
		break;
	}
	//clean up
	if (Is_fh_extern_opened == 0) {
		if (Fh != -1) close(Fh);
		Fh = -1;
	}
	return result;
}

int write_address_handshake(off_t addr, void* from, size_t size, off_t hs_addr, unsigned char bit_ofs) {
	unsigned char hs;
	int result = -1;
	for (;;) {
		if (Is_fh_extern_opened == 0) {
			if ((Fh = open("/dev/GPIO-driver", O_RDWR)) == -1) {
				perror("Can't open /dev/GPIO-driver!");
				break;
			}
		}
		//is BS unfinished with the last transfer?
		if (lseek(Fh, hs_addr, SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		if (read(Fh, &hs, 1) == -1) {
			perror("read");
			break;
		}
		if (((hs >> bit_ofs) & 0x01) == 1) {  //handshake bit still not set back to 0 by BS
			//printf("BS busy!\n");
			result = -2;
			break;
		}
		//bs can accept new! --> write new
		if (lseek(Fh, addr, SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		if (write(Fh, from, size) == -1) {
			perror("write");
			break;
		}
		//finish --> indicate write finished to BS
		if (lseek(Fh, hs_addr, SEEK_SET) == -1) {
			perror("lseek");
			break;
		}
		hs = 0x01 << bit_ofs; //just overwrite the full byte! take care of side-effect (if other bits are defined)
		if (write(Fh, &hs, 1) == -1) {
			perror("write");
			break;
		}
		//success
		result = 0;
		break;
	}
	//clean up
	if (Is_fh_extern_opened == 0) {
		if (Fh != -1) close(Fh);
		Fh = -1;
	}
	return result;
}
