/********************************************************************/
/*																	*/
/*  mon_db.h														*/
/*  =========														*/
/*																	*/
/*  Abstracted Monitoring database access functions					*/
/*																	*/
/*																	*/
/*  Author:		S.Okedara, Y.Zhu									*/
/*  Date: 															*/
/*	Version:														*/
/*																	*/
/********************************************************************/

#ifndef MON_DB_H_
#define MON_DB_H_

#include <stdint.h>
#include <mysql.h>

//mon_db specific cell data types (just simple mapping)
typedef MYSQL_TIME	cell_datetime_t;
typedef enum {CELL_8B = MYSQL_TYPE_TINY, CELL_16B = MYSQL_TYPE_SHORT, CELL_32B = MYSQL_TYPE_LONG, CELL_STRING = MYSQL_TYPE_VARCHAR, CELL_DATETIME = MYSQL_TYPE_DATETIME, CELL_FLOAT = MYSQL_TYPE_FLOAT} cell_type_t;
typedef enum {CELL_SIGNED = 0, CELL_UNSIGNED = 1} cell_sign_t;

struct cell_mon_db {
	char* cell_name;
	cell_type_t cell_type;
	cell_sign_t cell_sign;
	int is_cell_neverNull;
	void* cell_data;
	unsigned long cell_data_bytes;
};

static __inline__ char* get_cell_type_translation(cell_type_t t) {
	switch(t) {
		case CELL_8B: return "TINYINT";
		case CELL_16B: return "SMALLINT";
		case CELL_32B: return "INT";
		case CELL_STRING: return "VARCHAR(7)";
		case CELL_DATETIME: return "DATETIME";
		case CELL_FLOAT: return "FLOAT";
		default: return NULL;
	}
}

static __inline__ void init_cell(struct cell_mon_db* cell,
		char* cell_name,
		cell_type_t cell_type,
		cell_sign_t cell_sign,
		int is_cell_neverNull,
		void* cell_data,
		unsigned long cell_data_bytes)
{
	cell->cell_name = cell_name;
	cell->cell_type = cell_type;
	cell->cell_sign = cell_sign;
	cell->is_cell_neverNull = is_cell_neverNull;
	cell->cell_data = cell_data;
	cell->cell_data_bytes = cell_data_bytes;
}

//specific row structure of mon_db consisting cells
struct row_mon_db {
	struct cell_mon_db cells[256];
	int columns;
};

//db access functions
int open_mon_db(int db_version);
int create_table(char* tablename, struct row_mon_db* row);
int close_mon_db();
int insert_row(char* tablename, struct row_mon_db* row);

//help functions
void conv_cell_datetime_t_to_time_t(cell_datetime_t *ts, time_t *ttDate);
void convert_time_t_to_cell_datetime_t(time_t *ttDate, cell_datetime_t *ts);

#endif
