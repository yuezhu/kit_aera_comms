/** @file station_attr_table.c
 *  @brief hash table handling method implementation
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#include <stdio.h>
#include <stdlib.h>

#include "station_attr_table.h"

int add_station_attr_to_table(struct station_attr_hash** p_table, int id, station_attr_t* p_attr) {
	struct station_attr_hash *s;
	HASH_FIND_INT(*p_table, &id, s);
    if (s == NULL) { //unique
        s = (struct station_attr_hash*)malloc(sizeof(struct station_attr_hash));
        if (s == NULL) {
        	fprintf(stderr, "%s: malloc error, struct station_attr_hash\n", __FUNCTION__);
        	exit(EXIT_FAILURE);
        }
        s->hash_id = id;
        s->station_attr = *p_attr; //pointer-less object element copy legal in c
        HASH_ADD_INT(*p_table, hash_id, s); //a strange #define in uthash.h: &s->hash_id referenced
        return 0; //success on added
    }
    return -1;  //failure on found existing
}

void find_station_attr_in_table(struct station_attr_hash* table, int id, station_attr_t** pp_attr) {
    struct station_attr_hash *s;
    HASH_FIND_INT(table, &id, s);
    if (s != NULL) {
    	*pp_attr = &(s->station_attr);
    } else {
    	*pp_attr = NULL;
    }
}

int find_station_attr_in_table_copy(struct station_attr_hash* table, int id, station_attr_t* p_attr) {
    struct station_attr_hash *s;
    HASH_FIND_INT(table, &id, s);
    if (s != NULL) {
    	*p_attr = s->station_attr; //object copy
    	return 0; //success on found
    }
    return -1; //fail on not found
}

int modify_station_attr_in_table(struct station_attr_hash* table, int id, station_attr_t* p_attr) {
    struct station_attr_hash *s;
    HASH_FIND_INT(table, &id, s);
    if (s != NULL) {
    	s->station_attr = *p_attr;  //object copy
    	return 0; //success on found and update
    }
    return -1; //fail, not found
}

int delete_station_attr_in_table(struct station_attr_hash** p_table, int id) {
    struct station_attr_hash *s;
    HASH_FIND_INT(*p_table, &id, s);
    if (s != NULL) {
    	HASH_DEL(*p_table, s);
    	free(s);
    	return 0; //success on found and update
    }
    return -1; //fail, not found
}

void delete_station_attr_table(struct station_attr_hash** p_table) {
  struct station_attr_hash *s, *tmp;
  HASH_ITER(hh, *p_table, s, tmp) {
    HASH_DEL(*p_table, s); 	/* delete; table advances to next */
    free(s);  /* malloc */
  }
  *p_table = NULL;
}
