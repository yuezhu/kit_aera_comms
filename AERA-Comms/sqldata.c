/********************************************************************/
/*																	*/
/*  sqldata.c														*/
/*  =========														*/
/*																	*/
/*  Monitoring database table and data element specific access impl	*/
/*																	*/
/*																	*/
/*  Author:		S.Okedara, Y.Zhu									*/
/*  Date: 															*/
/*	Version:														*/
/*																	*/
/********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sqldata.h"

static void
print_error (MYSQL *conn, char *message)
{
	fprintf (stderr, "%s\n", message);
	if (conn != NULL) {
		fprintf (stderr, "Error %u (%s): %s\n", mysql_errno (conn), mysql_sqlstate (conn), mysql_error (conn));
	}
}

/* #@ _PRINT_STMT_ERROR_ */
static void
print_stmt_error (MYSQL_STMT *stmt, char *message)
{
	fprintf (stderr, "%s\n", message);
	if (stmt != NULL) {
		fprintf (stderr, "Error %u (%s): %s\n",
				 mysql_stmt_errno (stmt),
				 mysql_stmt_sqlstate (stmt),
				 mysql_stmt_error (stmt));
	}
}
/* #@ _PRINT_STMT_ERROR_ */

static void
insert_row_in_table(MYSQL_STMT *stmt, char* tablename, MYSQL_BIND* param, int param_length)
{
	char insert_stmt[512];

	if (param_length < 1) {
		perror("param length at least 1!");
		return;
	}
	
	strcpy (insert_stmt, "INSERT INTO ");
	strcat (insert_stmt, tablename); //Bs_stat, Station_stat, Frame_stat

	/*insert state info string, add the same number of ? as param_length*/
	strcat (insert_stmt, " VALUES(");
	int i;
	for (i=0; i<param_length-1; i++) {
		strcat (insert_stmt, "?,");
	}
	strcat (insert_stmt, "?)");
	//printf("statement: %s\n", insert_stmt);

	if (mysql_stmt_prepare (stmt, insert_stmt, strlen (insert_stmt)) != 0) {
		print_stmt_error (stmt, "Could not prepare INSERT statement");
		return;
	}
/*
	printf("param_list:\n");
	for (i=0; i<param_length; i++) {
		printf("	param %d, %d, %d, %d, %d, %lu\n", i, param[i].buffer_type, (int)(param[i].buffer), param[i].is_unsigned, (int)(param[i].is_null), param[i].buffer_length);
	}
*/
	if (mysql_stmt_bind_param (stmt, param) != 0) {
		print_stmt_error (stmt, "Could not bind parameters for INSERT");
		return;
	}

	if (mysql_stmt_execute (stmt) != 0) {
		print_stmt_error (stmt, "Could not execute statement");
	}
}

void prepared_statements(MYSQL *conn, char* dbname, char* tablename, MYSQL_BIND* param, int param_length)
{
	MYSQL_STMT *stmt;
	
/*
	char use_stmt[64]; //TODO: no use
	strcpy (use_stmt, "USE ");
	strcat (use_stmt, dbname); //Aera_datalog
	//printf("statement: %s\n", use_stmt);
*/
	/* select database and populate the exact table */
/*
	if (mysql_query (conn, use_stmt) != 0) {
		print_error (conn, "Could not set up table");
		return;
	}
*/

	/* call the statement handler */
	stmt = mysql_stmt_init(conn);  
	if (stmt == NULL) {
		print_error (conn, "Could not initialize statement handler");
		return;
	}

	/* insert some records */
	insert_row_in_table(stmt, tablename, param, param_length);

	/* release the statement handler */
	mysql_stmt_close (stmt); 	
}
