/** @file AERA_comms.c
 *  @brief main() as master contoller
 *
 *  Program to control and analyse the AERA-communication
 *  between SBC (single board computer) and BS (base station).
 *  After sending an NCP described in NCP.xml to the BS the
 *  program begins to analyse incoming data packets from the BS.
 *  Raw data and the results of statistical analysis will be
 *  written into files in CSV-format. For every frame a row with
 *  data will be added to the file. After 60 minutes a new file
 *  will be created.
 *
 *  @author Fridtjof Feldbusch, Yue Zhu
 *  @version	2.0
 *  @bug No known bugs.
 */
/* system */
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <string.h>
#include <poll.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h> /* no need */

/* Shared */
#include "crc32.h"
#include "timer.h"
#include "syslogger.h" //using syslogger should also include <syslog.h>
#include "serversock.h"
#include "linux_process_ctrl.h"
#include "comms_signal.h"
#include "sniffer.h" //for ip packet printout

/* rthq */
#include "rthq_CONFIG.h"
#include "port_settings.h"
#include "t_layer.h"
#include "post.h"
#include "id.h"
#include "rthq_infobase.h"

/* this project */
#include "ncp_xml.h" /* ncp load, ncp IF class*/
#include "datap.h"	/* data packet IF class*/
#include "statistictab.h"	/* statistictab (bs) IF class*/
#include "cmd.h" /* command IF class*/
#include "link_status_fsm.h" /* link status fsm facility*/
#include "io.h" /* in case of gpio-idriver, need file descriptor */
#include "statistic_writer.h" /*for major part of storing (incl. reduction) various status-table readout from BS into infobase */
#include "infobase.h" /*where various status-table are stored for statistics-build*/
#include "statistic_output.h" /*to export infobase to statistic files and db */

#include "amsg_c.h"

#define SIGNR_CTRLC SIGINT

static char* Version = "4.91";			 // Version completed statistics
static char* New_features = "-5. ncp and stat bug fix, -4. stat bug -retrans -3. a bug! -2. assertion removal in rthq -1. hqrecv stat included 0. A bug fix in pcb.c about free 1. cancel RT_ACK to save bandwidth for ACK 2. malloc protection in socklib (ReadSockMsgTO) 3. SIGPIPE to SIG_IGN to avoid pipe broken quit";
static char* Main_todos = "1. GPIO-idriver integration";

int Pipefd[2];
int Pipefd_pktRead[2];
static struct connInfo ConnInfoArray[3] = {{"RT_CONN", PORT_NUMBER_RT, 0, 0, 0, SERVER_DOWN},
									  	   {"HQ_CONN", PORT_NUMBER_HQ, 0, 0, 0, SERVER_DOWN},
									  	   {"IP_CONN", PORT_NUMBER_IP, 0, 0, 0, SERVER_DOWN}};

int Loopback_test = 0;

struct performanceTimer PTimer;
struct performanceTimer LatencyTimer;
static struct performanceTimer Program_timer;

void close_socket_connections()
{
    teardownConnection(&ConnInfoArray[0]);
    teardownConnection(&ConnInfoArray[1]);
    teardownConnection(&ConnInfoArray[2]);
}

void exit_program()
{
	long diff = snapshot_performanceTimer(&Program_timer, true, stdout); printf("Program exit, runtime: %ld seconds\n", diff);
	close_socket_connections();
	if (Is_fh_extern_opened == 1) { //Fridtjof
		close(Fh);
	}
	exit(EXIT_SUCCESS);
}

void open_socket_connections()
{
	int turns;
	int i;
	close_socket_connections();
	for (i = 0; i<3; i++) {
	    turns = 0;
	    while (initConnection(&ConnInfoArray[i]) < 0) { //try open 0:RT 1:HQ 2:IP
	    	usleep(50000);
	    	if (turns >= 3) {
	    		exit_program();
	    	}
	    	turns++;
	    }
	}
}

static void sig_handler1(int signo)
{
	exit_program();
}

static void usage(char *s)
{
	static char *usage_text  = "\
    -m x mode: x=1: random data test mode; x=2: undefined (now same as 1); x=3: ethernet over rthq (-l for loopback, no -l for bridge); default is 3\n\
	-p x poll() timeout in millisecond, default is 50ms\n\
	-D extra debug printouts, e.g. IP packets\n\
	-d developer version using /home/nfs-data as data directory (in- and output) default: /mnt/cf_usb \n\
	-l switch OFF logging into the file 'running_xxx.log' under data directory\n\
	-v verbose\n\
	-o switch OFF output statistics to files under data directory\n\
    -b switch OFF output statistics to database, configured in \"/root/mysql_login.txt\"\n\
	-s switch ON short stat to stdout for e.g. HW-test or quick debug\"\n\
	-I interval in seconds to output to statistics, default is 600\n\
	-L set loopback test\n\
	-C activate flow [C]ontrol, default off: 2k buffer for RT, 32k buffer for HQ at default, if full, no data-pull out on socket\n\
	-S switch OFF sorted RT recv, default: on\n\
	-k kill existing processes\n\
	";

	printf("***********************************************\n");
	printf("*KIT_comms application, ver %s, DB version: %d*\n", Version, DB_VERSION);
	printf("***********************************************\n");
	printf("usage: %s [options]\n", s);
	printf("added features: %s\n", New_features);
	printf("Main TODOs: %s\n", Main_todos);
    printf("%s", usage_text);
}

void segfault1(char* a) //double free
{
	a = malloc(1000);
	sleep(1);
	free(a);
}

void segfault2(char* a) //error ptr
{
	a[999] = 0;
}

void segfault3() //stack overflow
{
	int a[1000];
	int i;
	for (i=0; i<2000;i++) a[i]=0;
}

void segfault4() //will corrupt stack??
{
	uint64_t a[1000];
	memset(a, 0, 1000*8);
	return;
}



/****************************************************************/
/* Main program													*/
/****************************************************************/

int main(int argc, char *argv[])
{
	signal(SIGPIPE, SIG_IGN);

	char message[256];
	int developer_version = 0;  // flag = 1: use /home/nfs-data instead of /mnt/cf_usb as data directory

	int file_flag = 1;		// flag for if database access //YZ added 30.08.2012
	int database_flag = 1;		// flag for if database access //YZ added 30.08.2012
	int shortstat_flag = 0;		// flag for if database access //YZ added 30.08.2012

	int mode = 3;
	int interval = 600; //60 seconds as default statistic output interval
	bool flowCtrl = false;
	bool sortedRTRecv = true;
	int to_poll_msec = 20; //milliseconds for main poll() timeout
	int extra_debug = 0;
	int c;

	/* strip the path from app-name to "pname" */
    char *pch = strtok(argv[0], "./~");
    char *pname = pch;
    while (pch != NULL) {
    	pname = pch;
    	pch = strtok(NULL, "./~"); //continues
    }

    int pid_list[10];
    int pid_list_len = 10;
    Message_flag = 0x2; //system logging enabled default
    while ((c = getopt(argc, argv, "m:p:DdlvobsI:LCSkh")) != EOF) {
		switch (c)
		{
			case 'm':
				mode = atoi(optarg);
				break;
			case 'p':
				to_poll_msec = atoi(optarg);
				break;
			case 'D':
				extra_debug = 1;
				break;
			case 'd': // developer version
				developer_version = 1;
				break;
			case 'l': // disable system logging
				Message_flag = Message_flag & (~0x2);
				break;
			case 'v': // enable output of messages to console
				Message_flag = Message_flag | 0x1;
				break;
			case 'o': //output to statistics file
				file_flag = 0;
				break;
			case 'b': //with database access
				database_flag = 0;
				break;
			case 's': //with database access
				shortstat_flag = 1;
				break;
			case 'I': //interval to output statistics
				interval = atoi(optarg);
				break;
			case 'L':
				Loopback_test = 1;
				break;
			case 'C':
				flowCtrl = true;
				break;
			case 'S':
				sortedRTRecv = false;
				break;
			case 'k':
				if (getProcessID(pname, pid_list, &pid_list_len) >= 0) {
					if (pid_list_len > 0) {
						printf("%d existing processes found\n", pid_list_len-1);
						killProcessID(pid_list, pid_list_len-1);
					}
				}
				exit(EXIT_SUCCESS);
				break;
			case 'h':
			default: usage(pname); exit(EXIT_SUCCESS);
		}
    }
	fflush(stdout);

	reset_performanceTimer(&Program_timer, true, stdout);
	reset_performanceTimer(&PTimer, true, stdout);

	assign_signal(SIGNR_CTRLC, sig_handler1); //ctrl-c signal

    if (pipe(Pipefd) == -1) { //[0]: read; [1]: write
        perror("pipe frame");
        exit(EXIT_FAILURE);
    }

    if (pipe(Pipefd_pktRead) == -1) { //[0]: read; [1]: write
        perror("pipe packet read");
        exit(EXIT_FAILURE);
    }

    initConnInfo(&ConnInfoArray[0], "RT_CONN", PORT_NUMBER_RT);
    initConnInfo(&ConnInfoArray[1], "HQ_CONN", PORT_NUMBER_HQ);
    initConnInfo(&ConnInfoArray[2], "IP_CONN", PORT_NUMBER_IP);

	//MuxIO by polling begin
	struct pollfd pfds[5];
	pfds[0].fd = Pipefd_pktRead[0];	// event 0: packet read
	pfds[0].events = POLLIN;
	pfds[1].fd = Pipefd[0]; // event 1: send sequence start
	pfds[1].events = POLLIN;
	int poll_count = 2; //default
	//MuxIO by polling end

    chksum_crc32gentab();
    time_t seed = time(NULL);
    srand (1345138369);
    fprintf(stdout, "random seed: %u,\n"
    		"poll timeout? %d msec,\n"
    		"is development? %d,\n"
    		"short-stat printout? %d,\n"
    		"full-stat to database? %d,\n"
    		"full-stat to file? %d,\n"
    		"stat interval %d,\n"
    		"system-logging to file/console: %d, %d, \n"
    		"is loopback test? %d,\n"
    		"is flow control activated? %d\n"
    		"is RT recv sorted? %d\n", (unsigned int)seed, to_poll_msec, developer_version, shortstat_flag, database_flag, file_flag, interval,
    		Message_flag&0x2, Message_flag&0x1,
    		 Loopback_test, flowCtrl, sortedRTRecv);

    init_t_layer(); //called d and n layer
    if (sortedRTRecv) set_sorted_rt_recv();
	/********************************************/
	/* Initialization							*/
	/********************************************/
	char ncp_filename[80];
	int ncp_version;
	char path[80];
	ncp_t ncp;
	bs_status_tab_t bs_status_tab;

	if (developer_version == 0) {
		strcpy(&path[0], "/mnt/cf_usb");
		int isfind = find_ncp_xml(path, ncp_filename, &ncp_version);
		if (isfind == 0) {
			printf("NCP.xml.xxxxx not found! copy a NCP.xml with unique five digits as additive suffix for NCP version.\n");
			exit(EXIT_FAILURE);
		}
		printf("\nStatistic program version %s for AERA in Argentina, NCP.xml version: %d\n", Version, ncp_version);
		printf("=================================================== \n\n");
	} else {
		strcpy(&path[0], "/home/nfs-data");

		int isfind = find_ncp_xml(path, ncp_filename, &ncp_version);
		if (isfind == 0) {
			printf("NCP.xml.xxxxx not found! copy a NCP.xml with unique five digits as additive suffix for NCP version.\n");
			exit(EXIT_FAILURE);
		}
		printf("\nStatistic program development version %s, NCP.xml version: %d\n", Version, ncp_version);
		printf("========================================= \n\n");
	}
	init_syslogger(path);

	// read NCP from XML file
	init_ncp(&ncp); //first init, then load
	if (load_ncp_from_xml(ncp_filename, &ncp) < 0) {
		sprintf(message, "Could not read %s! \n", ncp_filename); print_message(LOG_ERR, message);
		exit(EXIT_FAILURE);
	}
	init_link_status_fsm(ncp.p_station_table, ncp.aif.number_of_frames);

	// init statistic output, this has to be put after ncp as Su_stat_table is initialized within
	init_statistic_output(path, interval, file_flag, database_flag, shortstat_flag, Su_stat_table);

	// Check BS availability
	sprintf(message, "Check BS availability ... "); print_message(LOG_INFO, message);
	while (is_bs_available() == 0) {}; // Wait: BS available? Exit when true
	sprintf(message, "BS available! open RT and HQ IO-sockets\n"); print_message(LOG_INFO, message);

	/* Fridtjof
	//in case of gpio-idriver, open it here
	if ((Fh = open("/dev/GPIO-driver", O_RDWR)) == -1) {
		sprintf(message, "Can't open /dev/GPIO-driver!\n"); print_message(LOG_ERR, message);
		exit(EXIT_FAILURE);
	}
	Is_fh_extern_opened = 1;
	*/

	//open ethernet connection only after BS available
	open_socket_connections();
	pfds[2].fd = ConnInfoArray[0].socketCurrent;
	pfds[2].events = POLLIN;
	pfds[3].fd = ConnInfoArray[1].socketCurrent;
	pfds[3].events = POLLIN;
	pfds[4].fd = ConnInfoArray[2].socketCurrent;
	pfds[4].events = POLLIN;
	poll_count = 5;

	//load and send NCP to BS
	if (deliver_ncp(&ncp) < 0) {
		perror("deliver_ncp() error on load from xml");
	}
	//regiester ncp_version into infobase
	Bs_stat.ncp_version = ncp_version;

	//readback and print for check
	ncp_t ncp_read_back;
	init_ncp(&ncp_read_back);
	/*
	if (fetch_and_print_ncp(&ncp_read_back) < 0) {
		perror("fetch_and_print_ncp() error");
	}*/

	if (Loopback_test == 1) {
		printf("...CHANGED the local gate way to loopback_local MODE...!\n");
		cmd_t cmd;
	    cmd.dest = CMD_FOR_BS;
	    cmd.type = LOOPBACK;
	    cmd.SU_ID = 0; //whatever;
	    cmd.content = (unsigned short)LOOPBACK_LOCAL << 8;
	    if (deliver_cmd(&cmd) < 0) {
		    perror("deliver_cmd() error");
	    }
	}

	sprintf(message, "Running..."); print_message(LOG_INFO, message);

	int ind;
	char f = 'f';

	/************************************************/
	/*  Endless loop								*/
	/* 	Every loop writes one statistic file set	*/
	/************************************************/
	while(1) {
    	poll(pfds, poll_count, to_poll_msec); //so that the high level timer is 250ms granularity
    	if (pfds[1].revents & POLLIN) //io poll send sequence start
		{
    		if (mode < 3) {
				//prepare data to be sent
				prepare_packet_send(mode);
			}
			else {
				post_t sp;
				poll_send(&sp); //ip bridge included
		    	if (sp.msgType == MSG_WARN || sp.msgType == MSG_ERR) {
		    		snapshot_performanceTimer(&PTimer, true, stderr);
		    		fprintf(stderr, "<poll_send>:");
	        		print_post(&sp, stderr);
		    	}
			}
			if (is_send_sequence_fin()) {
			    char buf[64];
				read(Pipefd[0], &buf, 64); //flush
			}
		}
		if (pfds[0].revents & POLLIN) //io poll packet receive
		{
		    char buf[64];
			read(Pipefd_pktRead[0], &buf, 64); //flush

			if (mode < 3){
				congest_packet_recv(mode);
			}
			else {
				post_t rp; //recv post
				objwrapper_t* rc_amsg_c_r = NULL; //reference-counted_amsg _container_receive
				uint8_t tId_r;
				staId_t senderId_r;
				Qos_t	qos_r;
				poll_recv(&rp, &rc_amsg_c_r, &tId_r, &qos_r, &senderId_r);
				if (rp.msgType == MSG_WARN || rp.msgType == MSG_ERR) {
					snapshot_performanceTimer(&PTimer, true, stderr);
					fprintf(stderr, "<poll_recv>:");
					print_post(&rp, stderr);
				}
				if (rp.layer == LAYER_T && rp.msgType == MSG_NOTE && rp.msg == NOTE_RECV_OK) {
					uint8_t* amsg_c_r = objwrapper_get_object(rc_amsg_c_r);
					if (amsg_c_r != NULL) {
						uint8_t ind = (qos_r==Qos_RT)?0:1;
						writeToServerConnection(&ConnInfoArray[ind], rc_amsg_c_r); //write to the corresponding connInfo
					}
					objwrapper_release(rc_amsg_c_r); //release, should destroy
				}
				if (rp.layer == LAYER_T && rp.msgType == MSG_NOTE && rp.msg == NOTE_RECV_IP) {
					//already declared in t_layer.h
					//extern uint8_t Ip_bridge_rxbuf[1024];
					//extern uint16_t Ip_bridge_rxbuf_len;
					if (extra_debug == 1) {
						printf("\n*****got a IP packet from wireless*****\n");
						ProcessPacket((unsigned char*)Ip_bridge_rxbuf, Ip_bridge_rxbuf_len);
					}
					//forward
					sendSimpleSockServer(&ConnInfoArray[2],Ip_bridge_rxbuf, Ip_bridge_rxbuf_len);
				}

			    /********************************************/
			    /* Frame end detected from data packets 	*/
			    /* ("soft" frame end is not counted)		*/
			    /********************************************/
				//if (is_frame_end_soft()) { }

			    /****************************************************/
			    /* deal with network monitoring block (statistics) 	*/
			    /****************************************************/
			    monitoring_info_aggr_t mon_info_aggr;
			    get_monitoring_info(&mon_info_aggr);
			    store_nmd(&mon_info_aggr, &ncp);
			}
		}
		for (ind = 0; ind <2; ind++) {
			if (pfds[ind+2].revents & POLLIN) {//io poll connection accept or receive
				Qos_t qos_send = (ind == 0)?Qos_RT:Qos_HQ;
				bool isCongestable = true;
				unsigned long tLenTotal_RT, tLenTotal_HQ; //for print
				int num_RT, num_HQ; //for print
				if (flowCtrl == true) {
					isCongestable = check_congestion_ctrl(qos_send, &tLenTotal_RT, &tLenTotal_HQ, &num_RT, &num_HQ);
				}
				if (isCongestable == true) {
					if (ConnInfoArray[ind].actionOnEvent != NULL) { // do the corresponding action (read or accept)
						ConnInfoArray[ind].actionOnEvent(&ConnInfoArray[ind], NULL);
					}
					if (ConnInfoArray[ind].arg != NULL) { //if it is read, the arg should point to the amsg_c
						objwrapper_t* rc_amsg_c_toforward = (objwrapper_t*)(ConnInfoArray[ind].arg); //the amsg_c is realloced to be able to fill CRC in add_send()!


						post_t ap;
						staId_t recverId = (Loopback_test == 1)? get_myId() : STAID_BROADCAST; //if loopback test?, otherwise broadcast!
						add_send(&ap, rc_amsg_c_toforward, qos_send, recverId); //rc_amsg_c taken over by add_send
						if (ap.msgType == MSG_WARN || ap.msgType == MSG_ERR) {
							snapshot_performanceTimer(&PTimer, true, stderr);
							fprintf(stderr, "<add_send>:");
							print_post(&ap, stderr);
						}

						objwrapper_release(rc_amsg_c_toforward); //release only frees the buffer if add_send fails (ref_cnt=1)
						ConnInfoArray[ind].arg = NULL;
					}

					//update the current socket! since it can change to accept or receive refering to different socket value (updated in socketCurrent)
					//NOTE! if the socketCurrent is changed to 0 by handler (SERVER_DOWN) then the polling will not visit here anymore -->needa main loop function
					pfds[ind+2].fd = ConnInfoArray[ind].socketCurrent;
				} else { //not congestable --> do nothing --> should stall at the other end of socket
					snapshot_performanceTimer(&PTimer, true, stdout);
					fprintf(stdout, "stall, %s-buf full: %d transfers of tot %lu bytes present\n", (qos_send==Qos_RT)?"RT":"HQ",
							(qos_send==Qos_RT)?num_RT:num_HQ,
							(qos_send==Qos_RT)?tLenTotal_RT:tLenTotal_HQ);
				}
			}
		}
		if (pfds[4].revents & POLLIN) {
			if (Ip_bridge_txbuf_pending == 0) { //no pending of IP bridge tx buffer
				//already defined in t_layer.h
				//extern uint8_t Ip_bridge_txbuf[1024];
				//extern uint16_t Ip_bridge_txbuf_len;
				//extern int Ip_bridge_txbuf_pending;

				if (ConnInfoArray[2].actionOnEvent != NULL) { // do the corresponding action (read or accept)
					if (ConnInfoArray[2].state == SERVER_UP_ACCEPTED) { //special!!! use simpleSock read explicitly instead of "actionOnEvent"-read from AERA "socklib"
						#include "tdatagram.h" //for IP_HDRBLEN, but unsauber!, so that Ip_bridge_txbuf can be used straitforward to avoid one memcpy. we sacrifice "code" for performance
						Ip_bridge_txbuf_len = 1024 - IP_HDRBLEN; //give the max length for check
						if (recvSimpleSockServer(&ConnInfoArray[2], &Ip_bridge_txbuf[IP_HDRBLEN], &Ip_bridge_txbuf_len) >= 0) { //ok
							unsigned char ipv4[4];
							get_ipv4_daddr_in_arr(&Ip_bridge_txbuf[IP_HDRBLEN], ipv4);
							Ip_bridge_txbuf_recvId = ipv4[3];
							Ip_bridge_txbuf_pending = 1;
							if (extra_debug == 1) { //no FullInfo
								printf("\n*****got a IP packet from local for %d.%d.%d.%d*****\n", ipv4[0], ipv4[1], ipv4[2], ipv4[3]);
								ProcessPacketOut((unsigned char*)(&Ip_bridge_txbuf[IP_HDRBLEN]), Ip_bridge_txbuf_len);
							}
						}
					} else { //maybe accept
						ConnInfoArray[ind].actionOnEvent(&ConnInfoArray[ind], NULL);
					}
				}

				//update the current socket! since it can change to accept or receive refering to different socket value (updated in socketCurrent)
				//NOTE! if the socketCurrent is changed to 0 by handler (SERVER_DOWN) then the polling will not visit here anymore -->needa main loop function
				pfds[4].fd = ConnInfoArray[2].socketCurrent;
			} else {
				snapshot_performanceTimer(&PTimer, true, stdout);
				fprintf(stdout, "stall, ip internal buffer full, application send too quickly?\n");
			}
		}


		/****************************************************************/
		/* Begin analysis of data packets and base station information	*/
		/****************************************************************/
		// Poll: data packet available?
		if (is_datap_to_read()) { // data packet on base station available
			if (write(Pipefd_pktRead[1], &f, sizeof(char)) == -1) { //any char, 'f' indicate packet waiting to be read
				perror("write(Pipefd_pktRead[1])");
			}
		}

		/********************************************/
		/* Frame end signaled by base station 		*/
		/* ("hard" frame end)						*/
		/********************************************/
		if (is_frame_end_hard()) {
			if (write(Pipefd[1], &f, sizeof(char)) == -1) { //any char, 'f' indicate frame
				perror("write(Pipefd[1])");
			}

			if (fetch_bs_status_tab(&bs_status_tab) < 0) {
				perror("fetch_bs_status_tab() error");
			} else {
				store_statistictab(&bs_status_tab, &ncp);
			}

		    //update link status for each station on a low frequency
		    int link_status_modification = update_link_status();
		    // when there are changes in one or more stations send an NCP
		    if (link_status_modification == 1) {
			    if (deliver_ncp(&ncp) < 0) {
				    perror("deliver_ncp() error on modification");
			    }
		    }
		}

	    /* poll timeout timer! */
		poll_checkTimeout();

		/* delayed forward for RT recv, not within the select-ifs, but happened after timeout*/
		if (get_sorted_rt_recv() == true) {
			objwrapper_t* rc_amsg_c_arr[SORT_DEPTH];
			int num = 0;
			bool ready = get_rt_forward_ready(rc_amsg_c_arr, &num);
			while(ready) { //forward all the "ready" and "sorted" transfers from certain send
				int i;
				for(i=0; i<num; i++) { //forward all the "ready" and "sorted" transfers from certain sender
					uint8_t* amsg_c_r = objwrapper_get_object(rc_amsg_c_arr[i]);
					if (amsg_c_r != NULL) {
						writeToServerConnection(&ConnInfoArray[0], rc_amsg_c_arr[i]); //write to RT
					}
					objwrapper_release(rc_amsg_c_arr[i]); //release, should destroy
				}
				ready = get_rt_forward_ready(rc_amsg_c_arr, &num);
			}
		}

		/* export the infobase, timing controlled within the function*/
		output_statistics();

	} // endless loop
	exit_program();
}
