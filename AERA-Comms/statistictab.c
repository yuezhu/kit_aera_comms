/** @file statistictab.c
 *  @brief IF (interface) class for statistictab read from the base station, function implementation
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#include <stdio.h> /* for printf debug, remove if dbg removed*/

#include "def.h"

#include "statistictab.h"
#include "io.h"

/************/
/*	for IO	*/
/************/
//4K block number = 0, block-ofs = 0, region-ofs 2048
#define STAT_TAB_OFS 				2048
#define STAT_TAB_LENGTH_OFS 		3072 // 2048 + 1024
#define BS_STAT_HANDSHAKE_OFS 		3076 // 2048 + 1028
	#define OFS_BS_STAT_HANDSHAKE		0
// #define BS_TIMING_SLOT_COUNT_OFS 3080 // 2048 + 1032
#define BS_TIMING_FRAME_COUNT_OFS 	3080 // 2048 + 1032 + 0
#define BS_TIMING_SECOND_COUNT_OFS i	3081 // 2048 + 1032 + 1
#define BS_ARQ_TABLE_OFS 			3084 // 2048 + 1036
#define BS_ARQ_TABLE_SIZE_OFS 		3180 // 2048 + 1132
#define BS_MISC_INFO_OFS 			3184 // 2048 + 1136

#define packed_data __attribute__((__packed__))

struct bs_slot_status_packed {
	signed char _rssi; // byte 0
	unsigned char _bs_stat; // byte 1, bit3~0: bs_recv_stat, bit4:  on_air_id_matched
#define MSK_BS_RECV_STAT 0x07 //0~7, 0~4 are used
#define OFS_ON_AIR_ID_MATCHED (4)
}packed_data; //2 bytes

struct bs_timing_packed {
	unsigned char _frame_count; //byte 0
	unsigned char _second_count; //byte 1
}packed_data; //2 bytes

struct arq_slot_packed {
	unsigned char _slot_attribute[2];
}packed_data;

struct bs_misc_packed {
	signed char _rssi_bg;
	signed char _tx_power;
	signed char _temperature_in_cel;
	unsigned char _bs_id[2];
	unsigned char _command_response;
	unsigned char _loopback_mode;
	unsigned char _firmware_version[2];
	unsigned char _software_version[2];
	unsigned char _arq_list_size;
}packed_data;

struct bs_status_tab_packed {
	struct bs_slot_status_packed _slot_stat_tab[512]; //sub-ofs = 0, 1024B, memory layout limited
	unsigned short _slot_stat_tab_size;    //length of _slot_stat_tab, normally 200, sub-ofs = 1024, 2B
	unsigned char _space1[2]; // no data, 2B
	unsigned char _bsbuf_slot_stat_handshake; //sub-ofs = 1028, 1B
	unsigned char _space2[3]; // no data, 3B
	struct bs_timing_packed _bs_timing; //sub-ofs = 1032, 2B
	unsigned char _space3[2]; // no data, 2B
	struct arq_slot_packed _arq_slots[48]; //sub_ofs = 1036, 96B, memory layout limited
	unsigned char _num_arq_slots; //sub_ofs = 1132, 1B
	unsigned char _space4[3]; //no data, 3B
	struct bs_misc_packed _misc_info; //sub_ofs = 1136, 16B
}packed_data;

/************/
/*	for IF	*/
/************/
static bs_timing_t Last_bs_timing = {255, 255};

void unpack_bs_timing(bs_timing_t* to, struct bs_timing_packed* from) {
	to->frame_count  = (int) from->_frame_count;
	to->second_count = (int) from->_second_count;
}

int is_bs_stat_available() {
	return (is_bit_set(BS_STAT_HANDSHAKE_OFS, OFS_BS_STAT_HANDSHAKE));
}

int is_frame_end_hard() {
	//another implementation variation: use is_bs_stat_available() as new frame indicator,
	//as long as get_bs_stat_tab_packed() is called every time for the correct handshake. (an implicit correlation)
	int retval = 0;
	struct bs_timing_packed packed;
	bs_timing_t now;
	if (read_address(BS_TIMING_FRAME_COUNT_OFS, &packed, sizeof(packed)) < 0) {
		return -1;
	}

	unpack_bs_timing(&now, &packed);

	if ((Last_bs_timing.frame_count != now.frame_count) || (Last_bs_timing.second_count != now.second_count)) {
		if ((Last_bs_timing.frame_count != 255) && (Last_bs_timing.second_count != 255)) { //exclude initial state
			 retval = 1; //is frame end
		}
		//update
		Last_bs_timing.frame_count = now.frame_count;
		Last_bs_timing.second_count = now.second_count;
	}
	return retval;
}

int fetch_bs_status_tab(bs_status_tab_t* p) {
	struct bs_status_tab_packed packed;
	if (read_address_handshake(STAT_TAB_OFS, &packed, sizeof(packed),
								BS_STAT_HANDSHAKE_OFS, OFS_BS_STAT_HANDSHAKE) < 0) {
		return -1; //read bs_stat_tab fail
	}

	//interpret bs_stat_tab, 1. table_size and slot
	int i;
	int tabsize = (int)ntohs_comms(packed._slot_stat_tab_size); //switch byte order
	if (tabsize > MAX_NO_OF_SLOTS) tabsize = MAX_NO_OF_SLOTS; //saturation
	p->slot_stat_tab_size = tabsize;
	for (i = 0; i < tabsize; i++) {
		p->slot_stat_tab[i].rssi = (int)(packed._slot_stat_tab[i]._rssi); //char->int, sign taken care automatically
		p->slot_stat_tab[i].slot_status = (recv_stat)(packed._slot_stat_tab[i]._bs_stat & MSK_BS_RECV_STAT);
		p->slot_stat_tab[i].on_air = (int)((packed._slot_stat_tab[i]._bs_stat >> OFS_ON_AIR_ID_MATCHED) & 0x01);
	}

	//2. timing
	unpack_bs_timing(&p->timing, &packed._bs_timing);

	//3. arq_size and arq_slots
	int num = (int)packed._num_arq_slots;
	if (num > MAX_NO_ARQ_SLOTS_TOTAL) num = MAX_NO_ARQ_SLOTS_TOTAL;
	p->num_arq_slots = num;
	for (i = 0; i < num; i++) {
		unpack_slot_attr(&(p->arq_slots[i]), packed._arq_slots[i]._slot_attribute);
	}
	p->arq_list_size = (int)packed._misc_info._arq_list_size;

	//4. cm_set
	p->cm_set.rssi_bg				= (int)(packed._misc_info._rssi_bg); //char->int, sign taken care automatically
	p->cm_set.tx_power				= (int)(packed._misc_info._tx_power);
	p->cm_set.temperature_in_cel	= (int)(packed._misc_info._temperature_in_cel);
	p->cm_set.command_response 		= (command_type_t)(packed._misc_info._command_response);
	p->cm_set.loopback_mode 		= (loopback_mode_t)(packed._misc_info._loopback_mode);
	for (i=0; i<2; i++) {
		p->cm_set.firmware_version.value[i]	= (int)(packed._misc_info._firmware_version[i]);
		p->cm_set.software_version.value[i]	= (int)(packed._misc_info._software_version[i]);
	}

	//5. bs_id
	p->bs_id	= (unsigned short)conv_u8_arr(packed._misc_info._bs_id, 2);

	//debug temp
	if (p->cm_set.command_response != NOP) {
		printf("!!CMD RESP: %d \n", p->cm_set.command_response);
	}

/*
	printf("bs_id: %d, lpback: %d, fw: %d.%d, sw: %d.%d ", p->bs_id, p->cm_set.loopback_mode,
			p->cm_set.firmware_version.value[0], p->cm_set.firmware_version.value[1],
			p->cm_set.software_version.value[0], p->cm_set.software_version.value[1]);
*/
	return 0; //success
}
