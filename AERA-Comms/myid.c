/** @file myid.c
 *  @brief get_myId() implementation.
 *
 *  @author Y.Zhu
 *  @bug No known bugs.
 */

#include "id.h"


staId_t get_myId()
{
	return SBC_ID;
}
