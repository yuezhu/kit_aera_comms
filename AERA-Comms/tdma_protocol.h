/** @file tmda_protocol.h
 *  @brief any parameter, parameter limitation considered as from tdma-protocol in specification
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */


#ifndef TDMA_PROTOCOL_H_
#define TDMA_PROTOCOL_H_

#define BS_POWER_ON_IDENTIFIER	0x55

#define MAX_NO_OF_SLOTS	512

#define MAX_NO_DN_PACKETS 8		//or max dnlink-arq folds
#define MAX_NO_ARQ_SLOTS  6		//or max arq slots in a fold
#define MAX_NO_ARQ_SLOTS_TOTAL (MAX_NO_DN_PACKETS*MAX_NO_ARQ_SLOTS)	//here size of base station table for ARQ slots

#define MAX_LS_DATA_LENGTH	987 //1020-2-20-3-4-4
#endif /* TDMA_PROTOCOL_H_ */
