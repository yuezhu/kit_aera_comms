/** @file common_set.h
 *  @brief common types used for netmon, statistictab and cmd (only partly)
 *
 *  Not extremely hierachical, as cmd only sees command_type_t, not
 *  common_monitoring_set_t.
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#ifndef COMMON_SET_H_
#define COMMON_SET_H_

typedef enum {NOP = 0, RESET = 1, LOOPBACK = 2} command_type_t;

char* cmd2string(command_type_t cmd);
command_type_t string2cmd(char* cmd_string);

typedef enum {NO_LOOPBACK = 0, LOOPBACK_ONAIR = 1, LOOPBACK_LOCAL = 2, SOURCE_LOCAL = 3, SOURCE_ONAIR = 4, LOOPBACK_UNKNOWN = -1} loopback_mode_t;

typedef struct {
	int value[2];
} double_value_t;

typedef struct {
	/* by device measurement */
	int rssi_bg; //no invalid condition
	int tx_power; //no invalid condition
	int temperature_in_cel;
	/* by config */
	loopback_mode_t loopback_mode;
	/* by control */
	command_type_t command_response;
	/* by default */
	double_value_t firmware_version;
	double_value_t software_version;
} common_monitoring_set_t;


#endif /* COMMON_SET_H_ */
