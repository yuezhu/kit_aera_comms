/** @file link_status_fsm.c
 *  @brief helper class for to handle link status based on station_attr_hash, function implementation
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#include <assert.h>

#include "syslogger.h"

#include "link_status_fsm.h"
#include "infobase.h"

#define LINK_STATUS_THRESHOLD 50 // percentage of minimum valid received packets for an established connection
#define LINK_STATUS_UPDATE 30    // Time in seconds for determining the link status of a station and sending an NCP to the base station

int Link_status_frame_counter = 0;

static struct station_attr_hash* Table = NULL; //global
int Frame_limit = LINK_STATUS_UPDATE * 3; //default

void init_link_status_fsm(struct station_attr_hash* table, unsigned char number_of_frame) {
	Frame_limit = LINK_STATUS_UPDATE * number_of_frame;
	Table = table; //repoint
}

int increase_valid_packet_counter(unsigned short id) {
	station_attr_t* p;
	find_station_attr_in_table(Table, id, &p);
	//packet recved from an unknown su_id! strange
	if (p == NULL) {
		fprintf(stderr, "%s: packet received from unknown su_id %d,\n", __FUNCTION__, id);
		return -1;
	}
	if (p->num_uplink_packets_per_frame == 0) {
		fprintf(stderr, "%s: unexpected packet received from known su_id %d\n", __FUNCTION__, id);
		return -2;
	}
	p->valid_uplink_packets_counter++; //should not exceed, so no check
	return 0;
}

/* use recved-packets/expected-packets ratio, compared to a threshold to indicate link_status
 * a side-effect is that some packets of frame n could come delayed in frame n+1 or even later,
 * depending on the the buffer depth on base station (now = 4). This should only has limited effect
 * at the program start, as some packets are already in the BS buffer.  After that,
 * recv-packet readout by SBC should be stable.
 */
int update_link_status() {
	int link_status_modification = 0;
	int criteriaA, criteriaB;
	link_status_t current_link_status, next_link_status;
	int num_valid_packets, num_packets_expected;
	float ratio_valid_packets;

	char message[256];

	/********************************************/
	/* Update of the link_status				*/
	/********************************************/
	struct station_attr_hash *s;
	station_stat_t *p_stat;
	if (Link_status_frame_counter >= Frame_limit) { //number of frames reached
		//clear Bs_stat here as Bs_stat.link_acc stores the momentum
		reset_histo1d_linkstat(&Bs_stat.link_status_acc);

		// compute link status for every station
	    for(s=Table; s != NULL; s=s->hh.next) { //non-deletion-safe iteration over the whole table
	        //printf("user id %d: name %s\n", s->station_attr., s->name);
			current_link_status = s->station_attr.link_status;
			next_link_status = current_link_status; // default case
			if (current_link_status != unused) {
				//load the counter
				num_valid_packets = s->station_attr.valid_uplink_packets_counter;
				//load the expected
				num_packets_expected = s->station_attr.num_uplink_packets_per_frame * Frame_limit;
				assert(num_packets_expected != 0); //num_uplink_packets_per_frame should not be 0, or link_status is unused, Frame_limit also
				//calculate ratio
				ratio_valid_packets = (float)num_valid_packets/(float)num_packets_expected;

				//compare threshold (CriteriaA) and check if link completely lost (CriteriaB)
				if (ratio_valid_packets * 100 > LINK_STATUS_THRESHOLD) criteriaA = 1; else criteriaA = 0;
				if (num_valid_packets == 0) criteriaB = 1; else criteriaB = 0;

				switch (current_link_status) {
					case awaiting_connection: 	if (criteriaA) next_link_status = established; else //establish if above threshold
													if (!criteriaB) next_link_status = broken; //>= 1 packets but not threshold, broken
												break;
					case established:			if (criteriaB) next_link_status = awaiting_connection; else
													if (!criteriaA) next_link_status = broken;
												break;
					case broken: 				if (criteriaA) next_link_status = established; else
													if (criteriaB) next_link_status = awaiting_connection;
												break;
					default:					next_link_status = current_link_status;
												break;
				}

				if (current_link_status != next_link_status) {
					s->station_attr.link_status = next_link_status; //update value!
					link_status_modification = 1;
					sprintf(message, "\nLocal station %d: %s -> %s, rxed: %d, exp: %d, ratio: %.2f, th: %d%%\n", s->hash_id, link_status2string(current_link_status), link_status2string(next_link_status),
							num_valid_packets, num_packets_expected, ratio_valid_packets, LINK_STATUS_THRESHOLD);
					print_message(LOG_INFO, message);
				}
				//update counter! --> clear 0 for next update
				s->station_attr.valid_uplink_packets_counter = 0;
			} //else unused, no need to do change status

			/* register infobase */
			find_station_stat_in_table(Su_stat_table, s->hash_id, &p_stat);
			if (p_stat == NULL) { //packet recved from an unknown su_id! strange
				fprintf(stderr, "%s: p_station_table in ncp object has unknown su_id %d,\n", __FUNCTION__,s->hash_id);
				continue;
			}

			switch (next_link_status) {
				case awaiting_connection: 	add_histo1d_linkstat(&p_stat->link_status_acc, 0);
											add_histo1d_linkstat(&Bs_stat.link_status_acc, 0); //map to bin 0
											break;
				case established:           add_histo1d_linkstat(&p_stat->link_status_acc, 1);
											add_histo1d_linkstat(&Bs_stat.link_status_acc, 1); //map to bin 1
											break;
				case broken:				add_histo1d_linkstat(&p_stat->link_status_acc, 2);
											add_histo1d_linkstat(&Bs_stat.link_status_acc, 2); //map to bin 2
											break;
				case unused:                add_histo1d_linkstat(&p_stat->link_status_acc, 3);
											add_histo1d_linkstat(&Bs_stat.link_status_acc, 3); //map to bin 3
											break;
				default:                    add_histo1d_linkstat(&p_stat->link_status_acc, 4);
											add_histo1d_linkstat(&Bs_stat.link_status_acc, 4); //map to bin 4. unknown
											break;
			}
	    }
		Link_status_frame_counter = 0;
	}
	Link_status_frame_counter++;
	return link_status_modification;
}
