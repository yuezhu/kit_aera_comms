/*
 * statistic_writer.c
 *
 *  Created on: Nov 9, 2012
 *      Author: dev
 */
#include "statistic_writer.h"
#include "infobase.h" /* for Su_stat_table and Bs_stat*/

void store_statistictab(bs_status_tab_t* from, ncp_t* p)
{
	Bs_stat.N ++;
	add_minmaxsigma2(&Bs_stat.bs_rssi_bg_acc, from->cm_set.rssi_bg);
	add_minmaxsigma(&Bs_stat.temperature_in_cel_acc, from->cm_set.temperature_in_cel);
	add_minmaxsigma(&Bs_stat.tx_power_acc, from->cm_set.tx_power);
	add_sigma(&Bs_stat.arq_list_size_acc, from->arq_list_size);

	Bs_stat.firmware_version_inst.value[0] = from->cm_set.firmware_version.value[0];
	Bs_stat.firmware_version_inst.value[1] = from->cm_set.firmware_version.value[1];

	Bs_stat.software_version_inst.value[0] = from->cm_set.software_version.value[0];
	Bs_stat.software_version_inst.value[1] = from->cm_set.software_version.value[1];
	Bs_stat.loopback_mode_inst = from->cm_set.loopback_mode;
	Bs_stat.bs_id_inst = from->bs_id; //bs specific

	int i;
	station_stat_t* s;
	unsigned short id;
	//problem: unfortunately, as the BS do not refresh the slot_stat for the slot it didn't receive anything, obsolete values of last frame stays forever
	//for ARQ-slots and normal slots (when ncp changes)
	//therefore, we have to use NCP to distinguish non-obsolete uplink slots in slot_tab
	//and use from->arq_slots to distinguish non-obsolete ARQ-slots in slot_tab (complicated index translation, done in older version, but not well readable)
	//--> not evaluated now, for todo?
	for (i = 0; i < from->slot_stat_tab_size; i++)  {
		if ( is_slot_uplink(i, &p->slot_tab) ) {
			/* store to Bs_stat */
			//store pkt_recv histogram
			Bs_stat.num_slots_evaluated++;
			assert((int)from->slot_stat_tab[i].slot_status < 5);
			add_histo1d_pktrecv(&Bs_stat.pkt_recv_acc, (int)from->slot_stat_tab[i].slot_status);

			//store slot_attr_unmatch histogram
			if (from->slot_stat_tab[i].on_air == 0) {
				Bs_stat.slot_attr_unmatch_acc ++;
			}

			//store bs rssi histogram
			int rssi = from->slot_stat_tab[i].rssi;
			if (rssi < -100) {
				add_histo1d_bsrssi(&Bs_stat.bs_rssi_acc, 0);
			} else if (rssi < -90) {
				add_histo1d_bsrssi(&Bs_stat.bs_rssi_acc, 1);
			} else if (rssi < -80) {
				add_histo1d_bsrssi(&Bs_stat.bs_rssi_acc, 2);
			} else if (rssi < -70) {
				add_histo1d_bsrssi(&Bs_stat.bs_rssi_acc, 3);
			} else if (rssi < -60) {
				add_histo1d_bsrssi(&Bs_stat.bs_rssi_acc, 4);
			} else if (rssi < -50) {
				add_histo1d_bsrssi(&Bs_stat.bs_rssi_acc, 5);
			} else {
				add_histo1d_bsrssi(&Bs_stat.bs_rssi_acc, 6);
			}

			/* store to Su_stat_table */
			id = p->slot_tab.slot[i].SUorBS_ID;
			find_station_stat_in_table(Su_stat_table, id, &s);
			//packet recved from an unknown su_id! strange
			if (s == NULL) {
				fprintf(stderr, "%s: slot_stat_tab has unknown su_id %d,\n", __FUNCTION__, id);
			} else {
				// store pkt_recv histogram
				add_histo1d_pktrecv(&(s->pkt_recv_acc), (int)from->slot_stat_tab[i].slot_status);
				// store bs rssi
				add_minmaxsigma2(&s->bs_rssi_acc, rssi);
			}
		}
	}
	assert(from->num_arq_slots <= MAX_NO_ARQ_SLOTS_TOTAL && from->num_arq_slots >= 0);
	for (i = 0; i < from->num_arq_slots; i++)  {
		/* store to Su_stat_table */
		id = from->arq_slots[i].SUorBS_ID;
		find_station_stat_in_table(Su_stat_table, id, &s);
		//packet recved from an unknown su_id! strange
		if (s == NULL) {
			fprintf(stderr, "%s: arq_slots_tab has unknown su_id %d,\n", __FUNCTION__, id);
		} else {
			// store num_arq_slots_acc
			s->num_arq_slots_acc += from->num_arq_slots;
		}
	}
}

void store_nmd(monitoring_info_aggr_t* from, ncp_t* p)
{
	station_stat_t* s;
	/* store to Su_stat_table */
	find_station_stat_in_table(Su_stat_table, from->id, &s);
	//packet recved from an unknown su_id! strange
	if (s == NULL) {
		fprintf(stderr, "%s: nwm has unknown su_id %d,\n", __FUNCTION__, from->id);
	} else {
		if (from->p_nmd != NULL) { //something to store
			// store num_arq_slots_acc
			assert (from->p_timing != NULL); //when p_nmd not null, p_timing should also not null, see datap.c
			if (s->current_frame_count != from->p_timing->frame_count) {

				s->current_frame_count = from->p_timing->frame_count;
				s->N++;

				add_minmaxsigma2(&s->su_rssi_acc, from->p_nmd->rssi);
				add_minmaxsigma2(&s->su_rssi_bg_acc, from->p_nmd->cm_set.rssi_bg);
				add_minmaxsigma(&s->tx_power_acc, from->p_nmd->cm_set.tx_power);
				s->loopback_mode_inst = from->p_nmd->cm_set.loopback_mode;
				s->recv_delay_inst	= from->p_nmd->recv_delay;

				switch(from->p_nmd->misc_info) {
					case FIRMWARE_VER:
						s->firmware_version_inst.value[0] = from->p_nmd->cm_set.firmware_version.value[0];
						s->firmware_version_inst.value[1] = from->p_nmd->cm_set.firmware_version.value[1];
						break;
					case SOFT_VER:
						s->software_version_inst.value[0] = from->p_nmd->cm_set.software_version.value[0];
						s->software_version_inst.value[1] = from->p_nmd->cm_set.software_version.value[1];
						break;
					case RF_TEMPERATURE:
						add_minmaxsigma(&s->temperature_in_cel_acc, from->p_nmd->cm_set.temperature_in_cel);
						break;
					case RX_LNAVGA_SETTINGS:
						s->rx_lna_mode_inst = from->p_nmd->rx_lna_mode;
						s->rx_vga_gain_inst = from->p_nmd->rx_vga_gain;
						break;
					default:
						break;
				}
				int i;
				slot_succ_t nth_slot_succ;
				for (i=0; i<MAX_NO_DN_PACKETS; i++) {
					nth_slot_succ = from->p_nmd->dnlink_stat[i].nth_slot_succ;
					if (nth_slot_succ != UNINITIALIZED) { //slot used, normally dedicated 0, 1
						s->rx_dn_pkts++;
						if (nth_slot_succ != FAIL) {
							s->rx_total_sloterrors += nth_slot_succ-1; //1st slot, no error; 2nd slot, 1 error ....
							s->rx_total_slots += nth_slot_succ; //normally 3
						} else { //if failed, need to check how many repeatance
							s->rx_total_sloterrors += p->aif.no_down_slots; //normally 3
							s->rx_total_slots += p->aif.no_down_slots; //normally 3
							s->rx_dn_pkterrors++;
						}
					}
				}
				nth_slot_succ = from->p_nmd->ncp_stat.nth_slot_succ;
				if (nth_slot_succ != UNINITIALIZED) { //slot used, normally dedicated 0, 1
					s->rx_ncp_pkts++;
					if (nth_slot_succ != FAIL) {
						s->rx_total_sloterrors += nth_slot_succ-1; //1st slot, no error; 2nd slot, 1 error ....
						s->rx_total_slots += nth_slot_succ; //normally 3
					} else { //if failed, need to check how many repeatance
						int n_ncp_slots_per_frame = p->aif.number_of_NCP_slots * p->aif.number_of_frames; //normally 3*2 = 6 //todo: note imcompatibility possible, as number of NCP has changed meaning in new version
						s->rx_total_sloterrors += n_ncp_slots_per_frame;
						s->rx_total_slots += n_ncp_slots_per_frame;
						s->rx_ncp_pkterrors++;
					}
				}
			} //else duplicated entries, do nothing
		}

		//independent info
		if (from->p_datapmon != NULL) {
			s->throughput_acc += from->p_datapmon->throughput_in_byte;
		}
	}
}
