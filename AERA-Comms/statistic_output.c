/*
 * statistic_output.c
 *
 *  Created on: Jan 4, 2013
 *      Author: dev
 */

#include <time.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h> // to retrieve NCP file description

#include "statistic_output.h"
#include "syslogger.h"
#include "infobase.h"
#include "timer.h"
#include "mon_db.h"
#include "staid_table.h"

static char Path[80];
static int filecounter = 1;
static int last_day = 0;

static char pre_filename2[] = "StationStatistic";
static char pre_filename3[] = "BS-Statistic";
static char post_filename[] = "csv";

static FILE *stat_file2 = NULL;
static FILE *stat_file3 = NULL;

static int Interval = 60; //60 seconds as init value
static int IsFile = 0;
static int IsDB = 0;
static int IsShortStat = 0;

static int Num_SUs = 0;
static struct station_stat_hash* Pointer;

static struct performanceTimer Statistic_timer;

// Create and jump into a new directory
int makedir(char *dir) {
   if(mkdir(dir, 0777) != -1)
      if(chdir(dir) != -1)
         return 0;
   return -1;
}

int write_station_stat_dbtable(time_t current_time, station_stat_t* p, station_stat_yield_t* y) {
	struct row_mon_db row;
	int col = 0;
	cell_datetime_t d_time;
	convert_time_t_to_cell_datetime_t(&current_time, &d_time);
	init_cell(&(row.cells[col++]), "datetime", CELL_DATETIME, CELL_UNSIGNED, 1, &d_time, sizeof(CELL_DATETIME)); //the last 2 member elements no effects for table creation
	init_cell(&(row.cells[col++]), "su_id", CELL_16B, CELL_UNSIGNED, 1, &y->id, sizeof(CELL_16B));
	init_cell(&(row.cells[col++]), "AERA_id", CELL_16B, CELL_UNSIGNED, 0, (y->staId == -1)? NULL : &y->staId, 2); //new, nullable

	init_cell(&(row.cells[col++]), "link_status", CELL_STRING, CELL_UNSIGNED, 1, &y->link_stat_summary, sizeof(y->link_stat_summary));	//changed meaning

	init_cell(&(row.cells[col++]), "awaiting_percent", CELL_8B, CELL_UNSIGNED, 1, &y->link_stat_percentage[0], sizeof(unsigned char)); //new
	init_cell(&(row.cells[col++]), "established_percent", CELL_8B, CELL_UNSIGNED, 1, &y->link_stat_percentage[1], sizeof(unsigned char)); //new
	init_cell(&(row.cells[col++]), "broken_percent", CELL_8B, CELL_UNSIGNED, 1, &y->link_stat_percentage[2], sizeof(unsigned char)); //new
	init_cell(&(row.cells[col++]), "unused_percent", CELL_8B, CELL_UNSIGNED, 1, &y->link_stat_percentage[3], sizeof(unsigned char)); //new

	init_cell(&(row.cells[col++]), "uplink_throughput_Bps", CELL_32B, CELL_UNSIGNED, 1, &(y->throughput), sizeof(y->throughput)); //new
	init_cell(&(row.cells[col++]), "uplink_bandwidth_Bps", CELL_32B, CELL_UNSIGNED, 1, &(y->bandwidth), sizeof(y->bandwidth)); //new
	init_cell(&(row.cells[col++]), "uplink_arq_slots_per_sec", CELL_FLOAT, CELL_UNSIGNED, 1, &(y->arq_slots_per_sec), sizeof(y->arq_slots_per_sec)); //new

	pktrecv_histo1d_t percentage2;
	cal_percent_histo1d_pktrecv(&p->pkt_recv_acc, &percentage2);
	init_cell(&(row.cells[col++]), "uplink_ok_percent", CELL_8B, CELL_UNSIGNED, 1, &percentage2.bin[1], sizeof(unsigned char)); //new
	init_cell(&(row.cells[col++]), "uplink_ok_by_FEC_percent", CELL_8B, CELL_UNSIGNED, 1, &percentage2.bin[4], sizeof(unsigned char)); //new
	init_cell(&(row.cells[col++]), "uplink_crc2_error_percent", CELL_8B, CELL_UNSIGNED, 1, &percentage2.bin[2], sizeof(unsigned char)); //new
	init_cell(&(row.cells[col++]), "uplink_preamble_error_percent", CELL_8B, CELL_UNSIGNED, 1, &percentage2.bin[3], sizeof(unsigned char)); //new

	int totalpackets = p->rx_ncp_pkts + p->rx_dn_pkts;
	unsigned char SER_permil = (unsigned char)(y->SER*1000);
	unsigned char PER_permil = (unsigned char)(y->PER*1000);
	init_cell(&(row.cells[col++]), "downlink_SER_percent", CELL_8B, CELL_UNSIGNED, 0, (p->rx_total_slots==0)?NULL:&SER_permil, sizeof(unsigned char)); //new, in per mil, nullable
	init_cell(&(row.cells[col++]), "downlink_PER_percent", CELL_8B, CELL_UNSIGNED, 0, (totalpackets==0)?NULL:&PER_permil, sizeof(unsigned char)); //new, in per mil, nullable

	init_cell(&(row.cells[col++]), "bs_rssi_min", CELL_8B, CELL_SIGNED, 0, (p->bs_rssi_acc.N>0)?&p->bs_rssi_acc.min_reg:NULL, 1); //new, nullable
	init_cell(&(row.cells[col++]), "bs_rssi_max", CELL_8B, CELL_SIGNED, 0, (p->bs_rssi_acc.N>0)?&p->bs_rssi_acc.max_reg:NULL, 1); //new, nullable
	init_cell(&(row.cells[col++]), "bs_rssi_avg", CELL_8B, CELL_SIGNED, 0, (p->bs_rssi_acc.N>0)?&y->bs_rssi_avg:NULL, 1); //new, nullable
	init_cell(&(row.cells[col++]), "bs_rssi_stddev", CELL_FLOAT, CELL_UNSIGNED, 0, (p->bs_rssi_acc.N>0)?&y->bs_rssi_stddev:NULL, 4); //new, nullable

	init_cell(&(row.cells[col++]), "su_rssi_min", CELL_8B, CELL_SIGNED, 0, (p->su_rssi_acc.N>0)?&p->su_rssi_acc.min_reg:NULL, 1); //new, nullable
	init_cell(&(row.cells[col++]), "su_rssi_max", CELL_8B, CELL_SIGNED, 0, (p->su_rssi_acc.N>0)?&p->su_rssi_acc.max_reg:NULL, 1); //new, nullable
	init_cell(&(row.cells[col++]), "su_rssi_avg", CELL_8B, CELL_SIGNED, 0, (p->su_rssi_acc.N>0)?&y->su_rssi_avg:NULL, 1); //new, nullable
	init_cell(&(row.cells[col++]), "su_rssi_stddev", CELL_FLOAT, CELL_UNSIGNED, 0, (p->su_rssi_acc.N>0)?&y->su_rssi_stddev:NULL, 4); //new, nullable

	init_cell(&(row.cells[col++]), "su_rssi_bg_min", CELL_8B, CELL_SIGNED, 0, (p->su_rssi_bg_acc.N>0)?&p->su_rssi_bg_acc.min_reg:NULL, 1); //new, nullable
	init_cell(&(row.cells[col++]), "su_rssi_bg_max", CELL_8B, CELL_SIGNED, 0, (p->su_rssi_bg_acc.N>0)?&p->su_rssi_bg_acc.max_reg:NULL, 1); //new, nullable
	init_cell(&(row.cells[col++]), "su_rssi_bg_avg", CELL_8B, CELL_SIGNED, 0, (p->su_rssi_bg_acc.N>0)?&y->su_rssi_bg_avg:NULL, 1); //new, nullable
	init_cell(&(row.cells[col++]), "su_rssi_bg_stddev", CELL_FLOAT, CELL_UNSIGNED, 0, (p->su_rssi_bg_acc.N>0)?&y->su_rssi_bg_stddev:NULL, 4); //new, nullable

	init_cell(&(row.cells[col++]), "tx_power_min", CELL_8B, CELL_SIGNED, 0, (p->tx_power_acc.N>0)?&p->tx_power_acc.min_reg:NULL, 1); //new, nullable
	init_cell(&(row.cells[col++]), "tx_power_max", CELL_8B, CELL_SIGNED, 0, (p->tx_power_acc.N>0)?&p->tx_power_acc.max_reg:NULL, 1); //new, nullable
	init_cell(&(row.cells[col++]), "tx_power_avg", CELL_8B, CELL_SIGNED, 0, (p->tx_power_acc.N>0)?&y->txpower_avg:NULL, 1); //new, nullable

	init_cell(&(row.cells[col++]), "temperature_min", CELL_8B, CELL_SIGNED, 0, (p->temperature_in_cel_acc.N>0)?&p->temperature_in_cel_acc.min_reg:NULL, 1); //new, nullable
	init_cell(&(row.cells[col++]), "temperature_max", CELL_8B, CELL_SIGNED, 0, (p->temperature_in_cel_acc.N>0)?&p->temperature_in_cel_acc.max_reg:NULL, 1); //new, nullable
	init_cell(&(row.cells[col++]), "temperature_avg", CELL_8B, CELL_SIGNED, 0, (p->temperature_in_cel_acc.N>0)?&y->temperature_avg:NULL, 1); //new, nullable

	char fw_ver_str[30], sw_ver_str[30];
	sprintf(fw_ver_str, "%d.%d", p->firmware_version_inst.value[0], p->firmware_version_inst.value[1]);
	sprintf(sw_ver_str, "%d.%d", p->software_version_inst.value[0], p->software_version_inst.value[1]);

	init_cell(&(row.cells[col++]), "firmware_version", CELL_STRING, CELL_UNSIGNED, 0, (p->firmware_version_inst.value[0] == -1)?NULL:fw_ver_str, strlen(fw_ver_str)); //nullable
	init_cell(&(row.cells[col++]), "software_version", CELL_STRING, CELL_UNSIGNED, 0, (p->software_version_inst.value[0] == -1)?NULL:sw_ver_str, strlen(sw_ver_str)); //nullable
	init_cell(&(row.cells[col++]), "loopback_mode", CELL_8B, CELL_UNSIGNED, 0, (p->loopback_mode_inst == LOOPBACK_UNKNOWN)?NULL:&p->loopback_mode_inst, 1); //new, nullable
	init_cell(&(row.cells[col++]), "recv_delay", CELL_16B, CELL_UNSIGNED, 0, (p->recv_delay_inst == -1)?NULL:&p->recv_delay_inst, 2); //nullable
	init_cell(&(row.cells[col++]), "lna_mode", CELL_8B, CELL_UNSIGNED, 0, (p->rx_lna_mode_inst == -1)?NULL:&p->rx_lna_mode_inst, 1); //nullable
	init_cell(&(row.cells[col++]), "rx_gain", CELL_8B, CELL_UNSIGNED, 0, (p->rx_vga_gain_inst == -1)?NULL:&p->rx_vga_gain_inst, 1); //nullable

	row.columns = col;
	insert_row("Station_stat", &row);


	/* todo: bs table */
	return 0;
}

int create_tables()
{
	struct row_mon_db row;
	int col = 0;
	init_cell(&(row.cells[col++]), "datetime", CELL_DATETIME, CELL_UNSIGNED, 1, NULL, 0); //the last 2 member elements no effects for table creation
	init_cell(&(row.cells[col++]), "su_id", CELL_16B, CELL_UNSIGNED, 1, NULL, 0);
	init_cell(&(row.cells[col++]), "AERA_id", CELL_16B, CELL_UNSIGNED, 0, NULL, 0); //new, nullable

	init_cell(&(row.cells[col++]), "link_status", CELL_STRING, CELL_UNSIGNED, 1, NULL, 0);	//changed meaning
	init_cell(&(row.cells[col++]), "awaiting_percent", CELL_8B, CELL_UNSIGNED, 1, NULL, 0); //new
	init_cell(&(row.cells[col++]), "established_percent", CELL_8B, CELL_UNSIGNED, 1, NULL, 0); //new
	init_cell(&(row.cells[col++]), "broken_percent", CELL_8B, CELL_UNSIGNED, 1, NULL, 0); //new
	init_cell(&(row.cells[col++]), "unused_percent", CELL_8B, CELL_UNSIGNED, 1, NULL, 0); //new

	init_cell(&(row.cells[col++]), "uplink_throughput_Bps", CELL_32B, CELL_UNSIGNED, 1, NULL, 0); //new
	init_cell(&(row.cells[col++]), "uplink_bandwidth_Bps", CELL_32B, CELL_UNSIGNED, 1, NULL, 0); //new
	init_cell(&(row.cells[col++]), "uplink_arq_slots_per_sec", CELL_FLOAT, CELL_UNSIGNED, 1, NULL, 0); //new

	init_cell(&(row.cells[col++]), "uplink_ok_percent", CELL_8B, CELL_UNSIGNED, 1, NULL, 0); //new
	init_cell(&(row.cells[col++]), "uplink_ok_by_FEC_percent", CELL_8B, CELL_UNSIGNED, 1, NULL, 0); //new
	init_cell(&(row.cells[col++]), "uplink_crc2_error_percent", CELL_8B, CELL_UNSIGNED, 1, NULL, 0); //new
	init_cell(&(row.cells[col++]), "uplink_preamble_error_percent", CELL_8B, CELL_UNSIGNED, 1, NULL, 0); //new

	init_cell(&(row.cells[col++]), "downlink_SER_percent", CELL_8B, CELL_UNSIGNED, 0, NULL, 0); //new, in percent, nullable
	init_cell(&(row.cells[col++]), "downlink_PER_percent", CELL_8B, CELL_UNSIGNED, 0, NULL, 0); //new, in percent, nullable

	init_cell(&(row.cells[col++]), "bs_rssi_min", CELL_8B, CELL_SIGNED, 0, NULL, 0); //new, nullable
	init_cell(&(row.cells[col++]), "bs_rssi_max", CELL_8B, CELL_SIGNED, 0, NULL, 0); //new, nullable
	init_cell(&(row.cells[col++]), "bs_rssi_avg", CELL_8B, CELL_SIGNED, 0, NULL, 0); //new, nullable
	init_cell(&(row.cells[col++]), "bs_rssi_stddev", CELL_FLOAT, CELL_UNSIGNED, 0, NULL, 0); //new, nullable

	init_cell(&(row.cells[col++]), "su_rssi_min", CELL_8B, CELL_SIGNED, 0, NULL, 0); //new, nullable
	init_cell(&(row.cells[col++]), "su_rssi_max", CELL_8B, CELL_SIGNED, 0, NULL, 0); //new, nullable
	init_cell(&(row.cells[col++]), "su_rssi_avg", CELL_8B, CELL_SIGNED, 0, NULL, 0); //new, nullable
	init_cell(&(row.cells[col++]), "su_rssi_stddev", CELL_FLOAT, CELL_UNSIGNED, 0, NULL, 0); //new, nullable

	init_cell(&(row.cells[col++]), "su_rssi_bg_min", CELL_8B, CELL_SIGNED, 0, NULL, 0); //new, nullable
	init_cell(&(row.cells[col++]), "su_rssi_bg_max", CELL_8B, CELL_SIGNED, 0, NULL, 0); //new, nullable
	init_cell(&(row.cells[col++]), "su_rssi_bg_avg", CELL_8B, CELL_SIGNED, 0, NULL, 0); //new, nullable
	init_cell(&(row.cells[col++]), "su_rssi_bg_stddev", CELL_FLOAT, CELL_UNSIGNED, 0, NULL, 0); //new, nullable

	init_cell(&(row.cells[col++]), "tx_power_min", CELL_8B, CELL_SIGNED, 0, NULL, 0); //new, nullable
	init_cell(&(row.cells[col++]), "tx_power_max", CELL_8B, CELL_SIGNED, 0, NULL, 0); //new, nullable
	init_cell(&(row.cells[col++]), "tx_power_avg", CELL_8B, CELL_SIGNED, 0, NULL, 0); //new, nullable

	init_cell(&(row.cells[col++]), "temperature_min", CELL_8B, CELL_SIGNED, 0, NULL, 0); //new, nullable
	init_cell(&(row.cells[col++]), "temperature_max", CELL_8B, CELL_SIGNED, 0, NULL, 0); //new, nullable
	init_cell(&(row.cells[col++]), "temperature_avg", CELL_8B, CELL_SIGNED, 0, NULL, 0); //new, nullable

	init_cell(&(row.cells[col++]), "fimware_version", CELL_STRING, CELL_UNSIGNED, 0, NULL, 0); //nullable
	init_cell(&(row.cells[col++]), "software_version", CELL_STRING, CELL_UNSIGNED, 0, NULL, 0); //nullable
	init_cell(&(row.cells[col++]), "loopback_mode", CELL_8B, CELL_UNSIGNED, 0, NULL, 0); //new, nullable
	init_cell(&(row.cells[col++]), "recv_delay", CELL_16B, CELL_UNSIGNED, 0, NULL, 0); //nullable
	init_cell(&(row.cells[col++]), "lna_mode", CELL_8B, CELL_UNSIGNED, 0, NULL, 0); //nullable
	init_cell(&(row.cells[col++]), "rx_gain", CELL_8B, CELL_UNSIGNED, 0, NULL, 0); //nullable

	row.columns = col;
	int ret = create_table("Station_stat", &row);
	return ret;
	/* todo: bs table */
}

void init_statistic_output(char* path, int interval, int isFile, int isDB, int isShortStat, struct station_stat_hash* table)
{
	strcpy(Path, path);
	Num_SUs = 0;

	Pointer = table;
	struct station_stat_hash *s;
    for(s=table; s != NULL; s=s->hh.next) { //non-deletion-safe iteration over the whole table
    	Num_SUs++;
    }

    reset_performanceTimer(&Statistic_timer, false, stdout);

    Interval = interval;
    IsFile = isFile;
    IsDB = isDB;
    IsShortStat = isShortStat;

	//open monitoring database from config file
	if (isDB == 1) {
		int ret = open_mon_db(DB_VERSION);
		if (ret < 0) {
			exit(EXIT_FAILURE);
		} else if (ret == 1){ //new db --> create tables
			if (create_tables() < 0) {
				exit(EXIT_FAILURE);
			}
			printf("tables created in mon db\n");
		} //old db
	}
}

int open_files(struct tm* local_time) {
	char timestamp[18];
	char datestamp[11];
	char directoryname[256];

	char filename2[256];
	char filename3[256];

	char message[256];

	int act_day = local_time->tm_mday;
	// When a day is over create a new directory for the statistic files
	if (act_day != last_day) {
		if (stat_file2 != NULL) fclose(stat_file2);
		if (stat_file3 != NULL) fclose(stat_file3);
		strftime(&datestamp[0], 11,"%Y-%m-%d", local_time);
		sprintf(directoryname, "%s/%s/", Path, datestamp);
		makedir(&directoryname[0]);
		if (opendir(&directoryname[0]) == NULL)
			datestamp[0] = '\0';  // Change into new directory was not successful

		// create and open new files
		filecounter++;
		if (filecounter > 32767) filecounter = 1;

		// open file for station statistic
		strftime(&timestamp[0], 17,"%Y-%m-%d_%H-%M", local_time);
		sprintf(filename2, "%s/%s/%s-%05d_%s.%s", Path, datestamp, pre_filename2, filecounter, timestamp, post_filename);
		stat_file2 = fopen(filename2, "w+");
		if(stat_file2 != NULL) {
			sprintf(message, "File %s opened\n", filename2);
			print_message(LOG_INFO, message);
		} else {
			sprintf(message, "Error opening %s\n", filename2);
			print_message(LOG_EMERG, message);
			return -1;
		}
		// open file for frame base station statistic
		sprintf(filename3, "%s/%s/%s-%05d_%s.%s", Path, datestamp, pre_filename3, filecounter, timestamp, post_filename);
		stat_file3 = fopen(filename3, "w+");
		if(stat_file3 != NULL) {
			sprintf(message, "File %s opened\n", filename3);
			print_message(LOG_INFO, message);
		} else {
			sprintf(message, "Error opening %s\n", filename3);
			print_message(LOG_EMERG, message);
			return -1;
		}

		last_day = act_day;
		return 1;
	}
	return 0;
}

void output_statistics()
{
	if (Interval > 0) {
		time_t current_time;
		struct tm *local_time;
		char timestamp[18];
		static int Bs_out_counter;

		int divider = Num_SUs>0?Num_SUs:1;
		if (snapshot_performanceTimer(&Statistic_timer, false, stdout) >= Interval*1000/divider) { //plot something

			current_time = time(NULL);
			local_time = localtime(&current_time);

			if (IsFile) {
				int isNewFileOpened = open_files(local_time);
				if (isNewFileOpened == 1) {
					fprintf(stat_file3, "time, "); print_bs_stat_title(stat_file3); print_rthq_infobase_title(stat_file3); fprintf(stat_file3, "\n"); //end line
					fprintf(stat_file2, "time, "); print_station_stat_title(stat_file2); fprintf(stat_file2, "\n"); //end line
				} else if (isNewFileOpened < 0) {
					fprintf(stderr, "%s: open statistic files fail\n", __FUNCTION__);
					return; //
				} //else: use old files
			}
			if (IsDB) {
			}

			strftime(&timestamp[0], 16,"%m.%d. %H:%M:%S", local_time);

			if (Pointer != NULL) {
				//print
				station_stat_yield_t yield;
				if (IsFile|IsDB|IsShortStat) {
					/* yield intermediate information (stored in yield) to reduce duplicated calculation for File and DB output */
					yield_station_stat(&yield, &Pointer->station_stat, Pointer->hash_id, Interval);

					if (IsFile) {
						fprintf(stat_file2, "%s, ", timestamp); print_station_stat(stat_file2, &Pointer->station_stat, &yield); fprintf(stat_file2, "\n"); //end line
						fflush(stat_file2);
					}
					if (IsDB) {
						write_station_stat_dbtable(current_time, &Pointer->station_stat, &yield);
					}

					if (IsShortStat) {
						if (yield.link_stat_summary == 'e') { //established
							printf("\nLocal station %d, %d-%d-%d-%d-%d, %c, "
														"bs_rssi: %d+-%5.2f, su_rssi: %d+-%5.2f, "
														"su_rssi_bg: %d+-%5.2f, "
														"tx_power: %d, temperature: %d, "
														"SER_up: %4.3f, SER_dn: %4.3f\n",
									Pointer->hash_id, yield.link_stat_percentage[0], yield.link_stat_percentage[1], yield.link_stat_percentage[2], yield.link_stat_percentage[3], yield.link_stat_percentage[4],
													  yield.link_stat_summary,
													  (signed char)yield.bs_rssi_avg, yield.bs_rssi_stddev,
													  (signed char)yield.su_rssi_avg, yield.su_rssi_stddev,
													  (signed char)yield.su_rssi_bg_avg, yield.su_rssi_bg_stddev,
													  (signed char) yield.txpower_avg, yield.temperature_avg,
													  yield.SER_up, yield.SER);
						}
					}
				}
				reset_station_stat(&Pointer->station_stat);

				//point to next
				Pointer = Pointer->hh.next;
				if (Pointer == NULL) { //back to head
					Pointer = Su_stat_table;
				}
			}

			//output
			Bs_out_counter++;
			if (Bs_out_counter >= Num_SUs) {
				if (IsFile) {
					fprintf(stat_file3, "%s, ", timestamp); print_bs_stat(stat_file3); print_rthq_infobase(stat_file3, Interval); fprintf(stat_file3, "\n"); //end line
					fflush(stat_file3);
				}
				if (IsDB) {
					//todo
				}
				reset_bs_stat(); //reset it
				reset_rthq_infobase(); //reset it

				Bs_out_counter = 0;
			}

			reset_performanceTimer(&Statistic_timer, false, stdout);
		}
	}
}

