/** @file io.h
 *  @brief general IO class for basic IO mode, function prototypes
 *
 *	IO class provided for IF classes. If the basic IO functions doesn't meet the need,
 *	IF class implementation can extend a specific xxx_io class and handles IO on his own.
 *	(unfortunately no polymorphism)
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#ifndef IO_H_
#define IO_H_

#include <sys/types.h>

extern int Fh;

extern int Is_fh_extern_opened;

int is_bit_set(off_t hs_addr, unsigned char bit_ofs);

int read_address(off_t addr, void* to, size_t size);

int write_address(off_t addr, void* from, size_t size);

int read_address_handshake(off_t addr, void* to, size_t size, off_t hs_addr, unsigned char bit_ofs);

int write_address_handshake(off_t addr, void* from, size_t size, off_t hs_addr, unsigned char bit_ofs);


#endif /* IO_H_ */
