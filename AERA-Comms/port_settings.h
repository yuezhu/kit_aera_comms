/** @file port_settings.h
 *  @brief port defines for RT and HQ, only included file by emulator project
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */


#ifndef PORT_SETTINGS_H_
#define PORT_SETTINGS_H_

/** @brief port number for RT transfer socket.
 *
 *	NOTE! change this should affect server coding, client coding,
 *	and documentation at the same time
 */
#define PORT_NUMBER_RT	5021

/** @brief port number for HQ transfer socket.
 *
 *	NOTE! change this should affect server coding, client coding,
 *	and documentation at the same time
 */
#define PORT_NUMBER_HQ	5022

/** @brief port number for  socket.
 *
 *	NOTE! change this should affect server coding, client coding,
 *	and documentation at the same time
 */
#define PORT_NUMBER_IP	5023

#endif /* PORT_SETTINGS_H_ */
