/********************************************************************/
/*																	*/
/*  sqldata.h														*/
/*  =========														*/
/*																	*/
/*  Monitoring database table and data element specific access		*/
/*																	*/
/*																	*/
/*  Author:		S.Okedara, Y.Zhu									*/
/*  Date: 															*/
/*	Version:														*/
/*																	*/
/********************************************************************/

#ifndef SQLDATA_H_
#define SQLDATA_H_

#include <mysql.h>

void prepared_statements(MYSQL *conn, char* dbname, char* tablename, MYSQL_BIND* param, int param_length);

#endif
