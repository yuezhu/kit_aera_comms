/** @file station_attribute.c
 *  @brief station_attribute method implementation
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#include <stdio.h>
#include "station_attribute.h"

char* link_status2string(link_status_t ls) {  // awaiting_connection, established, broken, unused
	char *lsstring;
	switch (ls) {
		case awaiting_connection: lsstring = "awaiting_connection"; break;
		case established: lsstring = "established"; break;
		case broken: lsstring = "broken"; break;
		case unused: lsstring = "unused"; break;
		default:	lsstring = ""; break;
	}
	return lsstring;
}

char* link_status2letter(link_status_t ls) {  // awaiting_connection, established, broken, unused
	char *lsstring;
	switch (ls) {
		case awaiting_connection: lsstring = "a"; break;
		case established: lsstring = "e"; break;
		case broken: lsstring = "b"; break;
		case unused: lsstring = "u"; break;
		default:	lsstring = ""; break;
	}
	return lsstring;
}

void init_station_attr(station_attr_t* p, link_status_t ls, unsigned char tx_delay, unsigned char tx_power) {
	p->link_status = ls;
	p->transmission_delay = tx_delay;
	p->transmission_power = tx_power;
	p->num_uplink_packets_per_frame = 0;
	p->valid_uplink_packets_counter = 0;
}

int is_station_attr_valid(station_attr_t* p) {
	return ( ((p->link_status >= awaiting_connection) && (p->link_status <= unused)) &&
			 (p->transmission_delay <= MAX_TRANS_DELAY) &&
			 (p->transmission_power <= MAX_TRANS_POWER));
}

void pack_station_attr(unsigned char* to, station_attr_t* from) {
	*to = ( (unsigned char)(from->link_status) << 6) | // set bits 7 and 6
			((from->transmission_delay & MAX_TRANS_DELAY) << 2) | // set bits 5,4,3 and 2
			(from->transmission_power & MAX_TRANS_POWER); // set bits 1 and 0
}


void unpack_station_attr(station_attr_t* to, unsigned char from) {
	to->link_status = (link_status_t)(from >> 6); // get bits 7 and 6
	to->transmission_delay = (from >> 2) & MAX_TRANS_DELAY; // get bits 5,4,3 and 2
	to->transmission_power = (from & MAX_TRANS_POWER); // get bits 1 and 0
}
