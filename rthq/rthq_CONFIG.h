/** @file CONFIG.h
 *  @brief all configurations used by comms_int
 *  @author Y.Zhu
 *  @bug No known bugs.
 */

#ifndef CONFIG_H_
#define CONFIG_H_

//#define DBG_MEM
//#define RT_ACK
//#define DBG_T
//#define DBG_N
//#define DBG_D
//#define SIM //channel simulation

#define T_FAC	10	//1 to accelerate debug, 10 is real
#define RX_FRAC_TIMEOUT_MSEC_DEFAULT	(300*T_FAC) //3 seconds
//this is used for rt recv to sort
#define RX_FRAC_DELAYED_MSEC_DEFAULT	(500) //0.5 seconds
#define TDELAY_MSEC_DEFAULT				(100*T_FAC) //1 second
#define TX_TIMEOUT_MSEC					(900*T_FAC)  //9 seconds
#define NUM_RECV_RETRANS_DEFAULT		4
#define NUM_RECV_RETRANS_MAX            5
#define FRAME_PER_SECOND				3 //3 frames per second, although should be "measured", but is just "assumed" here for easy calculation (!!!note could have incompatibility if this changed!)
#define TAT_MSEC						(200*T_FAC) //2 seconds turn-around time estimated

//general consideration: transmission delay (first frac sending till send finish and removed from pending buffer)
//is a mainly a function of message length (MsgLen), Turn-around-time (TAT), BW by means of sending slots opened (BW) and packet error rate (PER) in case of HQ transfer,
//and maximum allowed number of retransmissions (MaxRETR)
//(minor factors also includes quote of delayed "ACKs" in case of burst of ACKs that exceeds the fixed BW for ACK, but is not considered here or too complex)
//The value select here is to accept as many as possible pending transmissions that can be sent within a time-bounded duration (say e.g. 3 sec)
//consider PER=0, TAT = 2 sec(realistic), MaxRETR = 2
//let number of fraction Nf = ceil(MsgLen/k)
//duration of an RT-send: Nf/BW (k=456 bytes, MsgLen, BW are variable but known)
//duration of an HQ-send (without considering lost of ACK): HQ_DUR = Nf/BW + TAT+TDELAY + anr(Nf, PER)/BW + p(Nf, PER)*(TAT+TDELAY) + anr(anr(Nf,PER), PER)/BW + p(anr(Nf,PER), PER)*(TAT+TDELAY) .....
//HQ_DUR = sigma(x=1~MaxRETR)[anr^^x(Nf, PER)]/BW + sigma(x=1~MaxRETR)[p(anr^^x(Nf,PER), PER)]*TAT
//average number of retransmitted fractions:		anr(n, PER) = Sigma(x=1~n)[PER^^x*(1-PER)^^(n-x)*{n,x}*x]  (=approx?= PER*(1-PER)^^(n-1)*n =approx =)
//error rate of a failed transfer of n fractions:	p(n, PER) = 1-(1-PER)^^n
//....VERY COMPLEX....even without considering ACK lost
//extreme simplified: for PER = 0 (no PER measurement)
//HQ_DUR = Nf/BW + TAT + TDELAY
//a little bit complicated, by <1% PER (with PER measurement), assumption: maximal 1 retrans, retrans only 1 frac, and it always successful
//HQ_DUR = Nf/BW + TAT + TDELAY + PER*(1/BW+TAT+TDELAY)
//considering  ACK lost causing TXTMO no need, since for HQ uplink, PER_DN is pretty small, and there should be no HQ downlink:
//but considering lost of last frac during normal transfer:
//HQ_DUR = Nf/BW + TAT + TDELAY + PER*(1/BW+TAT+TDELAY+RXTMO-TDELAY)

#define BUFSIZE_RT_DEFAULT 2048 //2k byte
#define BUFSIZE_HQ_DEFAULT 32768 //32k bytes

//for test
#define FRAME_DELAY_MSEC				(33*T_FAC) //0.3 second

//any amsg written to this program via socket will be bounced back, "test_prog" --socket--> "comms" --socket--> "test_prog"
//#define TEST_SOCKET_LOOPBACK

//any amsg written to this program via socket will be bounced back, "test_prog" --socket--> "comms" --CAN--> "SU" --CAN--> "comms" --socket--> "test_prog"
//this is done by:
//- instruct SU (set pkt_id_destination=LOCAL_GATEWAY in tdma protocol) -- A
//- to loop all the 540B packet back (set SU in loopback_local mode) --B
//- within the Amsgs for myself (set recv_id = my_Id()) in RT/HQ protocol) --C
//A and C is set by this define, condition B should be taken care extra, do not use this option with mode 3
#define TEST_LOCAL_GATEWAY_LOOPBACK

#define TO_POLL_MSEC 250 //milli seconds

#endif /* CONFIG_H_ */
