/** @file t_layer.h
 *  @brief "transport-layer" of RT/HQ protocol service functions prototype.
 *
 *  This layer is the highest layer (sees n_layer), and the most complicated.
 *  - Send and receive service for reference counted amsg container (wrapped in objwrapper).
 *  - QOS for Real-Time (RT) and High-Quality (HQ) happens on this layer.
 *  - Buffer size can be set separately for RT/HQ along with simple congestion control "suggestion"
 *
 *  @author Y.Zhu
 *  @bug No known bugs.
 */

#ifndef __T_LAYER_H__
#define __T_LAYER_H__

#include <stdbool.h>
#include "post.h" /* for post_t */
#include "id.h" /* for Qos_t and staId_t */
#include "objwrapper.h" /* for objwrapper_t */

#ifdef __cplusplus
extern "C" {
#endif

#define IPHDR_OFFSET  IP_HDRBLEN

extern uint8_t Ip_bridge_txbuf[1024];
extern uint16_t Ip_bridge_txbuf_len;
extern staId_t Ip_bridge_txbuf_recvId;
extern int Ip_bridge_txbuf_pending;
extern uint8_t Ip_bridge_rxbuf[1024];
extern uint16_t Ip_bridge_rxbuf_len;

/** @brief initialization function for t_layer.
 */
void init_t_layer();

/** @brief receive function for t_layer.
 *
 * 	Note pointer to pointer is needed since *rc_amsg_c_pt (still pointer) is
 * 	internally assigned to a pointer pointing to a malloc-ed chunk. The application is responsible
 * 	to release the chunk afterwards with objwrapper_release(), and the objwrapper takes care of the eventual
 * 	free() after completely dereferenced
 * 	(post->layer == LAYER_T && post->msgType == MSG_NOTE && post->msg == NOTE_RECV_OK) indicating
 * 	rc_amsg_c_pt is filled with a valid amsg.
 *
 * 	@param [out]	post		 encoded return status instead of return value
 * 	@param [out]	rc_amsg_c_pt pointer to the pointer of reference counted amsg container
 * 	@param [out]	tId 		 pointer to the tId of the receive transfer
 * 	@param [out]	qos 		 pointer to the qos of the receive transfer
 * 	@param [out]	senderId 	 pointer to the senderId, who is the initiator of the transfer
 */
void poll_recv(post_t* post, objwrapper_t** rc_amsg_c_pt, uint8_t* tId, Qos_t* qos, staId_t* senderId);

/** @brief add a reference counted amsg to the pending queue of out-going amsgs.
 * 	@param [out]	post		 encoded return status instead of return value
 * 	@param [in]	rc_amsg_c	 pointer of reference counted amsg container
 * 	@param [in]	qos 		 QOS of the out-going transfer (go to which pending buffer)
 * 	@param [in]	senderId 	 recverId, the destination of the transfer
 * 	@return bool if is successful
 */
bool add_send(post_t* post, objwrapper_t *rc_amsg_c, Qos_t qos, staId_t recverId);

/** @brief send function for t_layer.
 *
 * 	Should be periodically called in application loop to empty the queue
 * 	of pending out-going amsgs. Bandwidth limitation happens on lower layers
 * 	QOS is prioritized here, now is implemented as fifty-fifty, if both RT and HQ
 * 	present in out-going queue
 *
 * 	@param [out]	post		 encoded return status instead of return value
 */
void poll_send(post_t* post);


/** @brief timeout timer refresher.
 *
 * 	Should be periodically called in application loop with good enough granularity
 * 	suggested better than 250ms
 */
void poll_checkTimeout();

/** @brief set the receive retransmission trials for HQ transfer.
 *
 * 	Default now is 4, bigger value could be performance killer for packet error rate > 5%
 *
 *	@param [in]	num	number of trials
 */
void set_num_recv_retrans(uint8_t num);

/** @brief get the buffer size of pending queue for outgoing amsgs.
 *	@param [in]	qos		select which buffer you want to get
 *	@return unsigned long buffer_size
 */
unsigned long get_buffer_size(Qos_t qos);

/** @brief set the buffer size of pending queue for outgoing amsgs.
 *	@param [in]	qos		select which buffer you want to set
 *	@param [in]	size	buffer size
 */
void set_buffer_size(Qos_t qos, unsigned long size);


void set_sorted_rt_recv();
bool get_sorted_rt_recv();


/** @brief get to know if it's suggested to add_send() or not.
 *
 *	Here are some old long considerations....
 *	the ultimate aim of congestion control is to tell the transmitter,
 *	if network will have "congestion problem" with the Amsg-c to be added
 *	the "congestion problem" here is the "latency", e.g. RT will costs more than e.g. 3 seconds in the buffer, that makes no sense to be sent out.
 *	however the estimation of the buffering time is hard (depends on many factor, see CONFIG.h. So may be the congestion_ctrl can make things worse...
 *	The idea is, we use settable RT buffer size and HQ buffer size
 *	the transmitter can honor this before add_send(), but do not have to.
 *	the algorithm will set "loose" conditions to have some
 *
 *	@param [in]	qos		select which service you want to know
 *	@return bool if is suggested
 */
bool check_congestion_ctrl(Qos_t qos,
		unsigned long* tLenTotal_RT, unsigned long* tLenTotal_HQ,
		int* num_RT, int* num_HQ);

#define SORT_DEPTH 32
bool get_rt_forward_ready(objwrapper_t* rc_amsg_c_arr[SORT_DEPTH], int* num);

#ifdef __cplusplus
}
#endif

#endif /* __T_LAYER_H__ */
