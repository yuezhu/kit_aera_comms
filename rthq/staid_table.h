/** @file staid_table.h
 *  @brief hash structure of staId: SBC_ID or station (LS)-ID bzw. AERA-ID
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#ifndef STAID_TABLE_H_
#define STAID_TABLE_H_

#include "id.h"

void register_staId(int id, staId_t staId);

int find_staId(int id);

void delete_staId_table();

#endif /* STAID_TABLE_H_ */
