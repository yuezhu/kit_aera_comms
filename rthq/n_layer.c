/** @file n_layer.c
 *  @brief "network-layer" of RT/HQ protocol service functions implementation.
 *
 *  @author Y.Zhu
 *  @bug No known bugs.
 */


#include "auxiliary.h"

#include "rthq_CONFIG.h"
#include "n_layer.h"
#include "d_layer.h"
#include "staid_table.h"
#include "tdatagram.h"
#include "timer.h"

#include "rthq_infobase.h"

#define DEPTH_SQ	100 //stores DEPTH_SQ-1
#define DEPTH_RQ	100 //stores DEPTH_RQ-1

#ifdef DBG_N
static uint32_t Npacket_running = 0;
#endif

extern struct performanceTimer PTimer;

// static variables
static ack_t Ack_send_queue[DEPTH_SQ];
static uint16_t Head_sq;
static uint16_t Tail_sq;

static ack_t Ack_recv_queue[DEPTH_RQ];
static uint16_t Head_rq;
static uint16_t Tail_rq;

//static function declarations
static bool n_enqueue_ack_recv(uint32_t rawAck); //layer intern
static bool n_dequeue_ack_send(uint32_t* rawAck);   //layer intern
static uint32_t getNumWaiting_ack_send();

//function definitions
void init_n_layer()
{
	Head_sq = 0;
	Tail_sq = 0;
	Head_rq = 0;
	Tail_rq = 0;

	init_d_layer();
}

uint32_t getNumWaiting_ack_send()
{
	if (Tail_sq >= Head_sq) {
		return (Tail_sq-Head_sq);
	}
	return (DEPTH_SQ+Tail_sq-Head_sq);
}

void n_send(post_t* post, uint8_t* p, unsigned short payload_length)
{
	//PSEUDOCODE: just send one fraction, plus outgoing-ack, no interest in others
	//upper layer should do polling and call n_send() if the sending is ready, so that outgoing-ack is sent frequently
	if (p != NULL) { //pointer validation check, but no boundary check!
		// build Ack statistics to understand the ultilization of the ACK-bandwidth
		uint32_t numWaiting = getNumWaiting_ack_send();

		/* update infobase */
		N_layer_acc.N_AckSendBWUsed  += MIN_COMMS(numWaiting, ACKNUM);
		N_layer_acc.N_AckSendBWTotal += ACKNUM;
		uint32_t numAckSendDelayedToNextSend = (numWaiting>ACKNUM?MIN_COMMS(numWaiting-ACKNUM, ACKNUM):0); //if delayed, next "transport" can also maximally be ACKNUM
		N_layer_acc.N_AckSendDelayed += numAckSendDelayedToNextSend;

		if (numAckSendDelayedToNextSend > 0) {
			snapshot_performanceTimer(&PTimer, true, stdout); printf("%s - note: %u number of ACKs have to be transfered later\n", __FUNCTION__, numAckSendDelayedToNextSend);
		}

		struct tDtgrm_hdr* thdr = (struct tDtgrm_hdr*)p;
		uint32_t rawAck;
		uint8_t ackEffNum = 0;
		int i;
		for (i=0; i<ACKNUM; i++) {
			if (n_dequeue_ack_send(&rawAck) == true) { //dequeued
				TDTGRMH_RAWACK_SET(thdr, ackEffNum, rawAck);
				ackEffNum++;
			}
			else { //queue empty
				break;
			}
		}

		//if (ackEffNum != 0 || TDTGRMH_TLEN(thdr) != 0) { //do not forward to d_layer if tdatagram is dummy (no info) and there is no ack to send
			TDTGRMH_HDRDWLENACKEFFNUM_SET(thdr, HDRBLEN>>2, ackEffNum); //set effective number of acks
			TDTGRMH_PTYPEPVER_SET(thdr, PROTOCOL_TYPE, PROTOCOL_VER); //set protocol type and version field

#ifdef DBG_N
			thdr->_reserved_u32_1 = htonl_comms(Npacket_running);
			snapshot_performanceTimer(&PTimer, true, stdout); printf("DBG_N:%s - no.%lu, acknum: %d\n", __FUNCTION__, Npacket_running, ackEffNum);
			Npacket_running++;
#endif
			build_hdrChksum(thdr); //build and fill header CRC
			d_send(post, p, payload_length);
		//}
		//else { //not nessesary to send the dummy, as there is no ack (null content there), hardware will send null-packet by themselves
		//	set_post(post, LAYER_N, MSG_NOTE, NOTE_SEND_DUMMYIGNORE); //received fractions for others
		//}
	}
	set_post(post, LAYER_N, MSG_ERR, ERR_NULLPTR); //received fractions for others
}

void n_recv(post_t* post, uint8_t *p)
{
	unsigned short id;
	d_recv(post, p, &id);
	if (post->layer == LAYER_D && post->msgType == MSG_NOTE && post->msg == NOTE_RECV_OK) { //complete
		struct tDtgrm_hdr* thdr = (struct tDtgrm_hdr*)p;
		if (TDTGRMH_PTYPE(thdr) == PROTOCOL_TYPE) {
			if (TDTGRMH_PVER(thdr) == PROTOCOL_VER) {
				if (verify_hdrChksum(thdr)) {
					uint8_t ackEffNum = TDTGRMH_ACKEFFNUM(thdr);
					N_layer_acc.N_AckRecved += ackEffNum;
#ifdef DBG_N
					snapshot_performanceTimer(&PTimer, true, stdout); printf("DBG_N:%s - no.%lu, acknum: %d\n", __FUNCTION__, ntohl_comms(thdr->_reserved_u32_1), ackEffNum);
#endif
					uint32_t rawack;
					int i;
					//enqueue receive
					for (i=0; i<MIN_COMMS(ACKNUM, ackEffNum); i++) { // safety
						rawack = TDTGRMH_RAWACK(thdr, i);
						if (RAWACK_SENDERID(rawack) == get_myId()) { //the ack is for me
							n_enqueue_ack_recv(rawack); //enqueue this ack for send pcb
						}
					}
					/* register staId <-> SUorBSid mapping */
					staId_t senderId = TDTGRMH_SENDERID(thdr);
					register_staId(id, senderId);

					//check destination
					staId_t recverId = TDTGRMH_RECVERID(thdr);
					if (recverId == STAID_BROADCAST || //broadcast
						recverId == get_myId()) { //or this is dedicated to me
						uint32_t tLen	= TDTGRMH_TLEN(thdr);
						if (tLen > 0) {
							set_post(post, LAYER_N, MSG_NOTE, NOTE_RECV_OK);
							return;
						}
						N_layer_acc.N_RecvDummy++;
						set_post(post, LAYER_N, MSG_NOTE, NOTE_RECV_DUMMY); //dummy transfer only for hq ack
						return;
					}
					N_layer_acc.N_RecvNot4Me++;
					set_post(post, LAYER_N, MSG_NOTE, NOTE_RECV_NOT4ME); //received fractions for others
					return;
				}
				N_layer_acc.N_RecvHdrChksumErr++;
				set_post(post, LAYER_N, MSG_WARN, WARN_RECV_HDRCRCERR); //received fractions but header crc error
				return;
			}
			N_layer_acc.N_RecvProtocolVerErr++;
			set_post(post, LAYER_N, MSG_WARN, WARN_RECV_PROTOVERUNMATCH); //received fractions but header crc error
			return;
		} else if (TDTGRMH_PTYPE(thdr) == IPBRIDGE_TYPE) { //received a ip packets!
			struct tIPgrm_hdr* iphdr = (struct tIPgrm_hdr*)p;
			staId_t recverId = TIPGRMH_RECVERID(iphdr);
			//filter!
			if (recverId == STAID_BROADCAST || //broadcast
				recverId == get_myId()) {
				set_post(post, LAYER_N, MSG_NOTE, NOTE_RECV_IP);
				return;
			}
			set_post(post, LAYER_N, MSG_NOTE, NOTE_RECV_NOT4ME); //received fractions for others
			return;
		}
		N_layer_acc.N_RecvProtocolTypeErr++;
		set_post(post, LAYER_N, MSG_WARN, WARN_RECV_PROTOTYPEUNMATCH); //received fractions but header crc error
		return;
	}
	//return, post has lower level information
}

bool n_enqueue_ack_send(ack_t* ack)
{
	uint16_t Tail_next = (Tail_sq+1)%DEPTH_SQ;
	if (Tail_next!= Head_sq) { //not full
		Ack_send_queue[Tail_sq] = *ack; //struct object copy
		Tail_sq = Tail_next;
#ifdef DBG_N
		snapshot_performanceTimer(&PTimer, true, stdout); printf("DBG_N:%s - EnqS,", __FUNCTION__);print_ack(ack);
#endif
		return true; //enqueque success
	}
	else {
		N_layer_acc.N_AckSendLoss++;
		snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s - warn: %u outgoing ACKs lost in enqueueing due to limited outgoing ACK buffer\n", __FUNCTION__, N_layer_acc.N_AckSendLoss);
		return false; //full --> enqueue fail
	}
}

bool n_dequeue_ack_recv(ack_t* ack)
{
	if (Head_rq!= Tail_rq) { //not empty
		*ack = Ack_recv_queue[Head_rq]; //struct object copy
		Head_rq = (Head_rq+1)%DEPTH_RQ;
#ifdef DBG_N
		snapshot_performanceTimer(&PTimer, true, stdout); printf("DBG_N:%s - DeqR,", __FUNCTION__);print_ack(ack);
#endif
		return true; //dequeque success
	}
	else {
		return false; //empty --> dequeue fail
	}
}

bool n_enqueue_ack_recv(uint32_t rawAck)
{
	uint16_t Tail_next = (Tail_rq+1)%DEPTH_RQ;
	if (Tail_next!= Head_rq) { //not full
		Ack_recv_queue[Tail_rq].senderId =	RAWACK_SENDERID(rawAck);
		Ack_recv_queue[Tail_rq].tId = RAWACK_TID(rawAck);
		Ack_recv_queue[Tail_rq].qos = RAWACK_TTYPE_QOS(rawAck);
		Ack_recv_queue[Tail_rq].fracId = RAWACK_FRACID(rawAck);
		Ack_recv_queue[Tail_rq].nakack = RAWACK_NAKACK(rawAck);
		Tail_rq = Tail_next;
		return true; //enqueque success
	}
	else {
		N_layer_acc.N_AckRecvLoss++;
		snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s - warn: %u incoming ACKs lost in enqueueing due to limited incoming ACK buffer\n", __FUNCTION__, N_layer_acc.N_AckRecvLoss);
		return false; //full --> enqueue fail
	}
}

bool n_dequeue_ack_send(uint32_t* rawAck)
{
	if (Head_sq!= Tail_sq) { //not empty
		RAWACK_SET((*rawAck), Ack_send_queue[Head_sq].senderId,
							Ack_send_queue[Head_sq].tId,
							Ack_send_queue[Head_sq].qos,
							Ack_send_queue[Head_sq].fracId,
							Ack_send_queue[Head_sq].nakack);  //struct object copy
		Head_sq = (Head_sq+1)%DEPTH_SQ;
		return true; //dequeque success
	}
	else {
		return false; //empty --> dequeue fail
	}
}

int n_get_send_rdy()
{
	//N-layer has no idea, request d-layer
	return d_get_send_rdy();
}

int n_get_bandwidth()
{
	return d_get_bandwidth();
}

void n_ip_send(post_t* post, uint8_t* p, unsigned short payload_length)
{
	//PSEUDOCODE: just send one fraction, plus outgoing-ack, no interest in others
	//upper layer should do polling and call n_send() if the sending is ready, so that outgoing-ack is sent frequently
	if (p != NULL) { //pointer validation check, but no boundary check!
		struct tIPgrm_hdr* iphdr = (struct tIPgrm_hdr*)p;

		TIPGRMH_PTYPE_SET(iphdr, IPBRIDGE_TYPE);

		d_send(post, p, payload_length);
	}
	set_post(post, LAYER_N, MSG_ERR, ERR_NULLPTR); //received fractions for others
}
