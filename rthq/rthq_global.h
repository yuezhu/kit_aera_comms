/** @file comms_global.h
 *  @brief system wide library include
 *  @author Y.Zhu
 *  @bug No known bugs.
 */

#ifndef COMMS_GLOBAL_H_
#define COMMS_GLOBAL_H_

#define VER_RTHQ	"1.0"

#endif /* COMMS_GLOBAL_H_ */
