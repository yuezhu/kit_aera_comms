/** @file id.h
 *  @brief typedef for station ID (LS or SBC) and get_myId() prototype
 *
 *  @author Y.Zhu
 *  @bug No known bugs.
 */

#ifndef ID_H_
#define ID_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/** @brief typedef for staId_t.
 */
typedef uint8_t staId_t; //antenna position 8 bits

/** @brief def of broadcast ID (both up/downlink).
 */
#define STAID_BROADCAST (staId_t)255

/** @brief def of SBC ID
 */
#define SBC_ID	(staId_t)254

/** @brief typedef for Quality Of Service QOS
 */
typedef enum {Qos_RT=0, Qos_HQ=1} Qos_t;

/** @brief return "my" Id
 *
 *	The Id is loaded from file on LS, or from #define in SBC
 *	In case of SBC, activate "#define I_AM_SBC 1"
 *
 *	@return staId_t
 */
staId_t get_myId();

#ifdef __cplusplus
}
#endif

#endif /* ID_H_ */
