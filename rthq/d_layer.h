/** @file d_layer.h
 *  @brief "datalink-layer" service functions prototype.
 *
 *  This layer is a very thin layer, just "forwarding services" between n_layer and datap.h,
 *	optimal for future extension
 *  - The only extra feature is the "simulation" by #define SIM in CONFIG.h
 *	- simulated packet flow-thru "channel" with artefacts is provided
 *	- to use this, please poll_checkTimeoutFrameTimer() periodically in the application loop
 *
 *  @author Y.Zhu
 *  @bug No known bugs.
 */

#ifndef __D_LAYER_H__
#define __D_LAYER_H__

#include <stdint.h>
#include <stdbool.h>
#include "rthq_CONFIG.h" /* for SIM */
#include "post.h"

#ifdef __cplusplus
extern "C" {
#endif

/** @brief initialization function for d_layer.
 */
void init_d_layer();

/** @brief receive function for d_layer.
 *
 * 	Note, memory for pointer \a p should be allocated by upper layer, no malloc beneath (and including) d_layer
 * 	(post->layer == LAYER_D && post->msgType == MSG_NOTE && post->msg == NOTE_RECV_OK) indicating
 * 	\a p is filled with a valid tdatagram.
 *
 * 	@param [out]	post	encoded return status instead of return value
 * 	@param [out]	p 		pointer to the tdatagram
 * 	@param [out]	p_id 	pointer to the SUorBSid for registration on higher layers
 */
void d_recv(post_t* post, uint8_t *p, unsigned short* p_id);

/** @brief send function for d_layer.
 * 	@param [out]	post	encoded return status instead of return value
 * 	@param [in]		p 		pointer to the tdatagram
 * 	@param [in]		payload_length	occupied bytes in *p
 */
void d_send(post_t* post, uint8_t *p, unsigned short payload_length);

/** @brief service function to inform if how many tdatagrams can be swallowed at the moment (forwarded)
 *	@return		bool
 */
int d_get_send_rdy();

/** @brief service function to inform how much bandwidth is granted (forwarded)
 *	@return		int		number of tdatagrams sendable per frame, <0 error
 */
int d_get_bandwidth();

//test
#ifdef SIM
/** @brief simulation related function to paramterize test channel
 * 	@param [in]	loss_rate	rate of packet loss, giving real value! not procent! (e.g. 0.01 = 1%)
 * 	@param [in]	dup_rate 	rate of packet duplication
 * 	@param [in]	delay_rate 	rate of packet delayed incoming
 */
	void init_testChannel(float loss_rate, float dup_rate, float delay_rate);

/** @brief simulation related function to keep FrameTimer running
 *
 *	call poll_checkTimeoutFrameTimer() periodically in the application loop if simulation is used
 */
	void poll_checkTimeoutFrameTimer();

	void set_frame_delay_msec(uint32_t frame_delay);
	void set_slot_opened(uint8_t slot_opened);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __D_LAYER_H__ */
