/** @file tdatagram.h
 *  @brief tdatagram struct layout and access method prototypes.
 *  @author Y.Zhu
 *  @bug No known bugs.
 */

#ifndef __TDATAGRAM_H__
#define __TDATAGRAM_H__

#include <stdint.h>
#include <stdbool.h>
#include "def.h"  /* for ntohl_comms() etc... */

/** @brief t_datagram protocol type.
 *
 *	unique that identifies this protocol, used as compatibility filter
 */
#define PROTOCOL_TYPE	1

/** @brief t_datagram protocol version.
 *
 *	unique that identifies the protocol version, value range 1-15
 *	used as compatibility filter
 */
#define PROTOCOL_VER	1

/** @brief maximal number of acknowledgements in a tdatagram.
 */
#define ACKNUM	16

/** @brief HeaDeR Byte LENgth
 *	@see ::tDtgrm_hdr
 */
#define HDRBLEN	(20 + ACKNUM*4)

/** @brief block length by TDMA protocol design
 */
#define TDMA_LEN	540 //todo: dynamic support?

/** @brief rest length of a tdatagram "fraction" in a 540 bytes block excluding header.
 */
#define FRAC_LEN_MAX	(TDMA_LEN - HDRBLEN)

/** @brief maximal number of fractions limiting fracId
 */
#define NUMFRAC_MAX		1024

/** @brief maximal number of tIds
 */
#define NUM_TID			64 //2^^6

/** @brief t_datagram protocol type.
 *
 *	unique that identifies the packet carrying an ip packets
 */
#define IPBRIDGE_TYPE	2

/** @brief IP HeaDeR Byte LENgth
 *	@see ::tIPgrm_hdr
 */
#define IP_HDRBLEN	4

/** @brief rest length of a tdatagram "fraction" in a 540 bytes block excluding header.
 */
#define IP_LEN_MAX	(TDMA_LEN - IP_HDRBLEN)


/** @brief t_datagram header layout in IO-memory.
 */
struct tDtgrm_hdr {
  /* P-Type / P-Version */
  uint8_t  _pType_pVer;
  /* T-ID / T-Type */
  uint8_t _tId_tType;
  /* Sender-ID */
  uint8_t _senderId;
  uint8_t _reserved_u8_1;
  /* T-Length / fraction ID */
  uint32_t _tLen_fracId;
  /* flags / fraction length */
  uint16_t _flags_fracLen;
  /* receiver-ID */
  uint8_t  _recverId;
  uint8_t  _reserved_u8_2;
  /* header-dword-length / effective number of ack */
  uint16_t _hdrDwLen_ackEffNum;
  /* header-crc16 */
  uint16_t _hdrChksum;
  uint32_t _reserved_u32_1;
  uint32_t _rawAck_bundle[ACKNUM];
} __attribute__((packed));


/** @brief get pType from a tdatagram header \a hdr.
 *
 *	get functions for other elements get also use define, omitted here.
 */
#define TDTGRMH_PTYPE(hdr)  	  	(((hdr)->_pType_pVer >> 0)&0x0f)	//4bits
#define TDTGRMH_PVER(hdr)  	  		((hdr)->_pType_pVer >> 4)	//4bits
#define TDTGRMH_TID(hdr)  	  		(((hdr)->_tId_tType >> 0)&0x3f)	//6bits
#define TDTGRMH_TTYPE_QOS(hdr) 		(((hdr)->_tId_tType >> 6)&0x01) //ttype 2 bits, qos 1 bit
#define TDTGRMH_SENDERID(hdr)		((hdr)->_senderId) //8bits
#define TDTGRMH_TLEN(hdr)	  		((ntohl_comms((hdr)->_tLen_fracId) >> 0)&0x003fffff) //22bits
#define TDTGRMH_FRACID(hdr)	  		((ntohl_comms((hdr)->_tLen_fracId) >> 22)&0x03ff) //10bits
#define TDTGRMH_FLAGS_LFRAC(hdr)	((ntohs_comms((hdr)->_flags_fracLen) >> 0)&0x01) //1bits
#define TDTGRMH_FRACLEN(hdr)		((ntohs_comms((hdr)->_flags_fracLen) >> 5)&0x07ff) //11bits
#define TDTGRMH_RECVERID(hdr)		((hdr)->_recverId) //8bits
#define TDTGRMH_HDRDWLEN(hdr)		((ntohs_comms((hdr)->_hdrDwLen_ackEffNum) >> 0)&0x03ff) //10bits
#define TDTGRMH_ACKEFFNUM(hdr)		(ntohs_comms((hdr)->_hdrDwLen_ackEffNum) >> 10) //6bits
#define TDTGRMH_HDRCHKSUM(hdr)		(ntohs_comms((hdr)->_hdrChksum));
#define TDTGRMH_RAWACK(hdr, ofs)	(ntohl_comms((hdr)->_rawAck_bundle[ofs])) //32bits
	#define RAWACK_NAKACK(rawAck)		((rawAck>>26)&0x00000003) //2 bits
	#define RAWACK_FRACID(rawAck)		((rawAck>>16)&0x000003ff) //10 bits
	#define RAWACK_TTYPE_QOS(rawAck)	((rawAck>>14)&0x00000001) //ttype 2 bits, qos 1 bit
	#define RAWACK_TID(rawAck)			((rawAck>>8)&0x0000003f) //6 bits
	#define RAWACK_SENDERID(rawAck)		(rawAck&0x000000ff) //8 bits

/** @brief set \a pType and \a pVer to a tdatagram header pointer \a hdr.
 *
 *	set functions for other elements get also use define, omitted here.
 */
#define TDTGRMH_PTYPEPVER_SET(hdr, pType, pVer)					((hdr)->_pType_pVer = ((pType&0x0f) << 0) | ((pVer&0x0f) << 4))
#define TDTGRMH_TIDTTYPE_SET(hdr, tId, tType_qos) 				((hdr)->_tId_tType = ((tId&0x3f) << 0) | ((tType_qos&0x01) << 6))
#define TDTGRMH_SENDERID_SET(hdr, senderId)						((hdr)->_senderId = senderId)
#define TDTGRMH_TLENFRACID_SET(hdr, tLen, fracId) 				((hdr)->_tLen_fracId = htonl_comms(((tLen&0x003fffff) << 0) | ((fracId&0x03ff) << 22)))
#define TDTGRMH_LFRACFRACLEN_SET(hdr, lFrac, fracLen)			((hdr)->_flags_fracLen = htons_comms(((lFrac&0x01) << 0) | ((fracLen&0x07ff) << 5)))
#define TDTGRMH_RECVERID_SET(hdr, recverId)						((hdr)->_recverId = recverId)
#define TDTGRMH_HDRDWLENACKEFFNUM_SET(hdr, hdrDwLen, ackEffNum) ((hdr)->_hdrDwLen_ackEffNum = htons_comms(((hdrDwLen&0x03ff) << 0) | ((ackEffNum&0x3f) << 10)))
#define TDTGRMH_HDRCHKSUM_SET(hdr, hdrChksum)					((hdr)->_hdrChksum = htons_comms(hdrChksum))
#define TDTGRMH_RAWACK_SET(hdr, ofs, rawAck)					((hdr)->_rawAck_bundle[ofs] = htonl_comms(rawAck))
	#define RAWACK_SET(rawack, senderId, tId, tType_qos, fracId, nakack)	(rawack = (senderId&0xff) | ((tId&0x3f)<<8) | ((tType_qos&0x01)<<14) | ((fracId&0x3ff)<<16) | ((nakack&0x03)<<26))

struct tIPgrm_hdr {
  /* P-Type*/
  uint8_t  _pType;
  /* length in bytes, note: without calculating this header! */
  uint16_t _len;
  /* receiver-ID */
  uint8_t  _recverId;
} __attribute__((packed));

#define TIPGRMH_PTYPE(hdr)  	(((hdr)->_pType >> 0)&0x0f)	//4bits
#define TIPGRMH_LEN(hdr)		(ntohs_comms((hdr)->_len)&0x07ff) //11bits
#define TIPGRMH_RECVERID(hdr)	((hdr)->_recverId) //8bits

#define TIPGRMH_PTYPE_SET(hdr, pType)	((hdr)->_pType = ((pType&0x0f) << 0))
#define TIPGRMH_LEN_SET(hdr, len)		((hdr)->_len = htons_comms(len&0x07ff))
#define TIPGRMH_RECVERID_SET(hdr, recverId)						((hdr)->_recverId = recverId)

/** @brief verify a tDtgrm_hdr if it's valid against the built-in crc16
 *  @param [in]	hdr	pointer to the tDtgrm_hdr
 *  @return bool
 */
bool verify_hdrChksum(struct tDtgrm_hdr* hdr);

/** @brief build and store the checksum for a filled tDtgrm_hdr
 *  @param [in,out]	hdr	pointer to the tDtgrm_hdr
 */
void build_hdrChksum(struct tDtgrm_hdr* hdr);

#ifdef __cplusplus
}
#endif


#endif /* __TDATAGRAM_H__ */
