/** @file staid_table.c
 *  @brief hash table handling method implementation
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#include <stdio.h>
#include <stdlib.h>
#include "staid_table.h"
#include "uthash.h" /* hash library */

/** @brief wrapper-like structure arround staId_t, needed for hash
 */
struct staId_hash {
	int hash_id;  // SUorBS_ID from ncp_slot_statibute will be used here
	staId_t staId;
	UT_hash_handle hh; // make this structure hashable
};

static struct staId_hash* Table = NULL;

void register_staId(int id, staId_t staId) {
	struct staId_hash *s;
	HASH_FIND_INT(Table, &id, s);
    if (s == NULL) { //not found --> add
        s = (struct staId_hash*)malloc(sizeof(struct staId_hash));
        if (s == NULL) {
        	fprintf(stderr, "%s: malloc error, struct staId_hash\n", __FUNCTION__);
        	exit(EXIT_FAILURE);
        }
        s->hash_id = id;
        s->staId = staId;
        HASH_ADD_INT(Table, hash_id, s); //a strange #define in uthash.h: &s->hash_id referenced
    } else { //found --> just modify
    	s->staId = staId;
    }
}

int find_staId(int id) {
    struct staId_hash *s;
    HASH_FIND_INT(Table, &id, s);
    if (s != NULL) {
    	return s->staId; //on found
    }
    return -1; //on not found
}

void delete_staId_table() {
	struct staId_hash *s, *tmp;
	HASH_ITER(hh, Table, s, tmp) {
		HASH_DEL(Table, s); 	/* delete; table advances to next */
		free(s);  /* malloc */
	}
	Table = NULL;
}
