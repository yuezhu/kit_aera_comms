/*
 * rthq_infobase.h
 *
 *  Created on: Dec 21, 2012
 *      Author: dev
 */

#ifndef RTHQ_INFOBASE_H_
#define RTHQ_INFOBASE_H_

#include <assert.h>
#include <stdio.h>
#include "accumulator.h"
#include "rthq_CONFIG.h"

CREATE_HISTO1D(4,sendstat)
CREATE_HISTO1D((NUM_RECV_RETRANS_MAX+2),recvretrans)

/* --> T infobase: statistic counter */
typedef struct {
	/* pkt send related */
	int T_send_throwaway; //dbg count
	int T_fillable; //bytes
	int T_fillRT; //bytes
	int T_fillHQ; //bytes

	sigma_t T_rtsend_tLen; // .N is count, .sigma is bytes
	sigma_t T_rtsend_duration; // for avg
	sigma_t T_hqsend_tLen;  // .N is count, .sigma is bytes
	sigma_t T_hqsend_duration; // for avg

	sendstat_histo1d_t T_hqsendstat;
	sigma_t T_hqsend_RTT; // for avg
	int T_hqsend_frac_retraned;
	int T_hqsend_fracs;

	/* pkt recv related */
	int T_recv_dupack; //dbg count
	sigma_t T_rtrecv_tLen; // .N is count, .sigma is bytes
	sigma_t T_hqrecv_tLen; // .N is count, .sigma is bytes
	recvretrans_histo1d_t T_hqrecv_retrans; //last bin as for "give-up"

	/* added in version 4.8 */
	sigma_t T_rtrecv_numFrac; // .N is count
	sigma_t T_hqrecv_numFrac; // .N is count
	int T_rtrecv_ok;
	int T_rtrecv_to;
	int T_rtrecv_crcerr;
	int T_hqrecv_ok;
	int T_hqrecv_to;
	int T_hqrecv_crcerr;
} t_layer_acc_t;
/* T infobase <-- */

extern t_layer_acc_t T_layer_acc; //singleton, defined in t_layer.c

/* --> N infobase */
typedef struct {
	/* Ack send related */
	int N_AckSendBWUsed; //count
	int N_AckSendBWTotal; //count
	int N_AckSendDelayed; //dbg count
	int N_AckSendLoss; //dbg count

	/* Ack recv related */
	int N_AckRecved; //count
	int N_AckRecvLoss; //dbg count

	/* pkt recv related */
	int N_RecvDummy; //count
	int N_RecvNot4Me; //count count
	int N_RecvHdrChksumErr; //dbg count
	int N_RecvProtocolVerErr; //dbg count
	int N_RecvProtocolTypeErr; //dbg count
} n_layer_acc_t;
/* N infobase <-- */

extern n_layer_acc_t N_layer_acc; //singleton, defined in t_layer.c

/* this implemented in server program */
/* --> D infobase*/
typedef struct {
	int D_sendfail; //count
	int D_recvok; //count
	int D_recvartifacts; //count: total errors of the following four counts
	int D_recvRdErr; //count
	int D_recvDupack; //count
	int D_recvNullPack; //count
	int D_recvCRC1fail; //count
} d_layer_acc_t;
/* infobase <-- */

extern d_layer_acc_t D_layer_acc; //singleton, defined in t_layer.c

/** @brief reset all accumulators, don't use it if you only want to reset several of them (manually instead
 */
void reset_rthq_infobase();
void print_rthq_infobase(FILE* out, int interval);
void print_rthq_infobase_title(FILE* out);

#endif /* RTHQ_INFOBASE_H_ */
