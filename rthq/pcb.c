/** @file pcb.c
 *  @brief the core transfer structs of transport-layer, types and manipulation function implementation.
 *
 *  @author Y.Zhu
 *  @bug No known bugs.
 */

//#include "global.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* for memset() */

#include "auxiliary.h"

#include "rthq_CONFIG.h"
#include "pcb.h"
#include "crc32.h"

extern struct performanceTimer PTimer;

struct pcb_send*	new_pcb_send(Qos_t qos, uint32_t tLen, uint8_t tId, staId_t recverId, uint16_t fracOffset, uint16_t numFrac, objwrapper_t *rc_tPayload)
{
	struct pcb_send* pcb = NULL;
	if (qos == Qos_RT) {
		pcb = (struct pcb_send*)malloc(sizeof(struct rt_pcb_send));
	}
	else {
		pcb = (struct pcb_send*)malloc(sizeof(struct hq_pcb_send));
	}
	if (pcb != NULL) {
		pcb->next = NULL;

		pcb->qos = qos;
		pcb->tId = tId;
		pcb->recverId = recverId;

		pcb->tLen = tLen;
		pcb->fracOffset = fracOffset;
		pcb->numFrac	= numFrac;

		pcb->rc_tPayload = rc_tPayload; //payload pointer take over

		reset_performanceTimer(&(pcb->duration_timer), false, stdout); //start calculate duration at the creation of send
		pcb->fracIdCurrent = 0;

#ifdef DBG_MEM
		printf("DBG_MEM:%s - send pcb malloc, tId: %d\n", __FUNCTION__, tId);
#endif
	} else {
		snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "MEM_ERR!%s - comms_int send pcb malloc fail!\n", __FUNCTION__);
	}
	return pcb;
}

struct pcb_recv*	new_pcb_recv(Qos_t qos, uint32_t tLen, uint8_t tId, staId_t senderId, uint16_t fracOffset, uint16_t numFrac, tLayerTimeout_handler handler)
{
	struct pcb_recv* pcb = NULL;
	if (qos == Qos_RT) {
		pcb = (struct pcb_recv*)malloc(sizeof(struct rt_pcb_recv));
	}
	else {
		pcb = (struct pcb_recv*)malloc(sizeof(struct hq_pcb_recv));
	}
	if (pcb != NULL) {
		bool isFreePcb = false;

		pcb->next = NULL;

		pcb->qos = qos;
		pcb->tId = tId;
		pcb->senderId = senderId;

		pcb->tLen = tLen;
		pcb->fracOffset = fracOffset;
		pcb->numFrac	= numFrac;

		pcb->lFracRecved = false;
		memset((void*)(pcb->listFracIdRecved), 0, (NUMFRAC_MAX/8));
		init_tLayerTimer(&(pcb->rx_timer), handler, pcb);

#ifdef DBG_MEM
		printf("DBG_MEM:%s - recv pcb malloc, tId: %d\n", __FUNCTION__, tId);
#endif
		uint8_t* tPayload = (uint8_t*)malloc(tLen); //tLen is size of bytes, freed in the rx timeout or poll_recv  after a rx is complete
		if (tPayload != NULL) {
			pcb->rc_tPayload = objwrapper_create(tPayload, free); //wrap the data field with a reference count
			if (pcb->rc_tPayload == NULL) { //objwrapper_create fail! no mem for ref count! not impossible, so check here
				snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "MEM_ERR!%s - comms_int recv data objwrapper malloc fail!\n", __FUNCTION__);
				free(tPayload);
				isFreePcb = true;
			}
		} else { //fail
			snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "MEM_ERR!%s - comms_int recv data malloc fail!\n", __FUNCTION__);
			isFreePcb = true;
		}
		if (isFreePcb == true) {
			free(pcb); //free the pcb
			pcb=NULL;
#ifdef DBG_MEM
			printf("DBG_MEM:%s - recv pcb free, tId: %d\n", __FUNCTION__, tId);
#endif
		}
	} else {
		snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "MEM_ERR!%s - comms_int recv pcb malloc fail!\n", __FUNCTION__);
	}
	return pcb;
}

void init_rt_pcb_recv(struct rt_pcb_recv *pcb, tLayerTimeout_handler handler, void *handler_arg)
{
	pcb->isCompleteAndCorrect = false; //new, this turns to true, if it's intact and ready for forward, but stays in buffer instead of prompt forward
	pcb->is2FwdASAP = false; //new
	init_tLayerTimer(&(pcb->rt_rx_timer), handler, handler_arg); //the same as above
}

void init_hq_pcb_recv(struct hq_pcb_recv *pcb)
{
	pcb->retrans_counter = 0;
	pcb->RTTattachedFracId = 0xffff; //meaning un-attached, valid values [0~NUMFRAC_MAX-1] meaning attached
}

void init_rt_pcb_send(struct rt_pcb_send *pcb) {} //do nothing

void init_hq_pcb_send(struct hq_pcb_send *pcb, tLayerTimeout_handler handler, void *handler_arg)
{
	init_tLayerTimer(&(pcb->hq_tx_timer), handler, handler_arg);
	pcb->isFreezing = false;
	pcb->RTT_meas_timer_started = false;
	pcb->RTT_time_msec = 0;
	pcb->n_frac_retraned = 0;
	pcb->how_am_i_finished = -1;
}

void remove_pcb_send(struct pcb_send *pcb, struct pcb_send** ll_pcb)
{
  struct pcb_send *pcb2;

  /* pcb to be removed is first in list? */
  if (*ll_pcb == pcb) {
    /* make list start at 2nd pcb */
    *ll_pcb = (*ll_pcb)->next;
    /* pcb not 1st in list */
  } else {
    for (pcb2 = *ll_pcb; pcb2 != NULL; pcb2 = pcb2->next) {
      /* find pcb in udp_pcbs list */
      if (pcb2->next != NULL && pcb2->next == pcb) {
        /* remove pcb from list */
        pcb2->next = pcb->next;
        break; //added in version AERA-Comms v4.51, LS-Comms v3.4
      }
    }
  }
  objwrapper_release(pcb->rc_tPayload);
#ifdef DBG_MEM
  printf("DBG_MEM:%s - send pcb free, tId: %d\n", __FUNCTION__, pcb->tId);
#endif
  free(pcb);
}

void remove_pcb_recv(struct pcb_recv *pcb, struct pcb_recv** ll_pcb)
{
  struct pcb_recv *pcb2;

  /* pcb to be removed is first in list? */
  if (*ll_pcb == pcb) {
    /* make list start at 2nd pcb */
    *ll_pcb = (*ll_pcb)->next;
    /* pcb not 1st in list */
  } else {
    for (pcb2 = *ll_pcb; pcb2 != NULL; pcb2 = pcb2->next) {
      /* find pcb in udp_pcbs list */
      if (pcb2->next != NULL && pcb2->next == pcb) {
        /* remove pcb from list */
        pcb2->next = pcb->next;
        break; //added in version AERA-Comms v4.51, LS-Comms v3.4
      }
    }
  }
  objwrapper_release(pcb->rc_tPayload);
#ifdef DBG_MEM
  printf("DBG_MEM:%s - recv pcb free, tId: %d\n", __FUNCTION__, pcb->tId);
#endif
  free(pcb);
}

bool check_completion(struct pcb_recv* pcb)
{
	uint16_t div = (pcb->numFrac)/64;
	uint16_t mod = (pcb->numFrac)%64;
	int i;
	for (i=0; i<div; i++) {
		if (pcb->listFracIdRecved[i] != 0xffffffffffffffffULL) {
			return false;
		}
	}
	if (pcb->listFracIdRecved[div] != ((0x1ULL<<mod) -1)) {
		return false;
	}
	return true; //if pcb->numFrac is 0, also return true, should not happen
}

void build_fracId_missing(struct pcb_recv* pcb, uint16_t list_fracId_missing[], uint16_t* num_fracId_missing)
{
	int i;
	uint16_t div = 0;
	uint16_t mod = 0;
	uint16_t number = 0;
	for (i=0; i<MIN_COMMS(pcb->numFrac, NUMFRAC_MAX); i++) {
		if ((((pcb->listFracIdRecved[div])>>mod)&0x01) == 0) { //missing
			list_fracId_missing[number] = (uint16_t)i; //no boundary check
			number++;
		}
		mod = (mod+1)%64;
		if (mod == 0) {
			div++;
		}
	}
	*num_fracId_missing = number;  //if number == 0 means "complete"
}

bool isfracIdRecved(struct pcb_recv* pcb, uint16_t offset)
{
	uint16_t div = offset/64;
	uint16_t mod = offset%64;
	if ((((pcb->listFracIdRecved[div])>>mod)&0x01) == 0) { //missing
		return false;
	}
	return true;
}

void set_fracIdRecved(struct pcb_recv* pcb, uint16_t offset)
{
	uint16_t div = offset/64;
	uint16_t mod = offset%64;
	pcb->listFracIdRecved[div] = pcb->listFracIdRecved[div] | (0x1ULL<<mod);
}

bool verify_tPayloadCRC(struct pcb_recv* pcb)
{
	uint8_t* tPayload = objwrapper_get_object(pcb->rc_tPayload);
	uint32_t tLen = pcb->tLen;
	uint32_t crc_tmp = (tPayload[tLen-4]<<24) | (tPayload[tLen-3]<<16) | (tPayload[tLen-2]<<8) | (tPayload[tLen-1]<<0); //ntohl
	uint32_t crc_cal = chksum_crc32(tPayload, tLen-4);
	if (crc_tmp == crc_cal) {
		return true;
	}
	return false;
}

void build_tPayloadCRC(struct pcb_send* pcb)
{
	uint32_t tLen = pcb->tLen;
	uint8_t* tPayload = objwrapper_get_object(pcb->rc_tPayload);
	uint32_t crc_cal = chksum_crc32(tPayload, tLen-4);
	tPayload[tLen-4] = crc_cal>>24;
	tPayload[tLen-3] = crc_cal>>16;
	tPayload[tLen-2] = crc_cal>>8;
	tPayload[tLen-1] = crc_cal>>0;
}

