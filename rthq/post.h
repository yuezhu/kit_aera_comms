#ifndef __POST_H__
#define __POST_H__

#include <stdio.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {MSG_NOTE, MSG_WARN, MSG_ERR} msgType_t;
typedef enum {LAYER_T, LAYER_N, LAYER_D} layer_t;
typedef int8_t msg_t;

typedef struct{
	layer_t	layer;
	msgType_t msgType;
	msg_t	msg;
} post_t;

#define NOTE_RECV_OK_BUT_DELAYED_FORWARD	4/* receive ok, but to be forwarded later, e.g. for sorting by RT*/
#define NOTE_RECV_IP		 3
#define NOTE_ADDSEND_OK		 2
#define NOTE_SEND_OK		 1
#define NOTE_RECV_OK		 0	  /* receive ok*/
#define NOTE_RECV_NULL     	-1    /* no receive. */
#define NOTE_RECV_INCOMP   	-2    /* received but incomplete. */
#define NOTE_SEND_NOTREADY  -3    /* send routine stalls */
#define NOTE_RECV_DUMMY		-4    /* receive dummys */
#define NOTE_RECV_NOT4ME	-5
#define NOTE_RECV_NOTYETLASTFRAC -6
#define NOTE_SEND_DUMMYIGNORE -7
#define NOTE_STALL		  -8


#define WARN_RECV_CRCERR		-1
#define WARN_RECV_TIMEOUT		-2
#define WARN_SEND_TOOLARGE		-3
#define WARN_SEND_LOST			-4
#define WARN_SENDBUFFBLOCKED	-5
#define WARN_NUM2SEND_ZERO		-6
#define WARN_RECV_HDRCRCERR 	-7
#define WARN_RECV_PROTOVERUNMATCH -8
#define WARN_RECV_PROTOTYPEUNMATCH -9
#define WARN_NO_TID_LEFT		-10
#define WARN_RECV_DUPACK		-11
#define WARN_RECV_TOOLARGE		-12

#define ERR_MEM				-1
#define ERR_NULLPTR			-2

static inline void set_post(post_t* post, layer_t layer, msgType_t msgType, msg_t msg) {
	post->layer = layer;
	post->msgType = msgType;
	post->msg = msg;
}

static inline void print_post(post_t* post, FILE* out) {
	if (post->msgType == MSG_NOTE) {
		fprintf(out, "note:");
	}
	else if (post->msgType == MSG_WARN) {
		fprintf(out, "warn:");
	}
	else if (post->msgType == MSG_ERR) {
		fprintf(out, "err:");
	}
	if (post->layer == LAYER_T) {
		fprintf(out, "T:");
	}
	else if (post->layer == LAYER_N) {
		fprintf(out, "N:");
	}
	else if (post->layer == LAYER_D) {
		fprintf(out, "D:");
	}
	fprintf(out, "%d\n", post->msg);
}


#ifdef __cplusplus
}
#endif

#endif /* __MPOST_H__ */
