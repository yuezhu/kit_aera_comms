/*
 * rthq_infobase.c
 *
 *  Created on: Jan 2, 2013
 *      Author: dev
 */

#include "rthq_infobase.h"

t_layer_acc_t T_layer_acc;
n_layer_acc_t N_layer_acc;
d_layer_acc_t D_layer_acc;

CREATE_HISTO1D_C(4,sendstat)
CREATE_HISTO1D_C((NUM_RECV_RETRANS_MAX+2),recvretrans)

void reset_rthq_infobase()
{
	T_layer_acc.T_send_throwaway = 0;
	T_layer_acc.T_fillable = 0;
	T_layer_acc.T_fillRT = 0;
	T_layer_acc.T_fillHQ = 0;

	reset_sigma(&T_layer_acc.T_rtsend_tLen);
	reset_sigma(&T_layer_acc.T_rtsend_duration);
	reset_sigma(&T_layer_acc.T_hqsend_tLen);
	reset_sigma(&T_layer_acc.T_hqsend_duration);

	reset_histo1d_sendstat(&T_layer_acc.T_hqsendstat);
	reset_sigma(&T_layer_acc.T_hqsend_RTT);
	T_layer_acc.T_hqsend_frac_retraned = 0;
	T_layer_acc.T_hqsend_fracs = 0;

	T_layer_acc.T_recv_dupack = 0;
	reset_sigma(&T_layer_acc.T_rtrecv_tLen);
	reset_sigma(&T_layer_acc.T_hqrecv_tLen);
	reset_histo1d_recvretrans(&T_layer_acc.T_hqrecv_retrans);

	reset_sigma(&T_layer_acc.T_rtrecv_numFrac);
	reset_sigma(&T_layer_acc.T_hqrecv_numFrac);
	T_layer_acc.T_rtrecv_ok = 0;
	T_layer_acc.T_rtrecv_to = 0;
	T_layer_acc.T_rtrecv_crcerr = 0;
	T_layer_acc.T_hqrecv_ok = 0;
	T_layer_acc.T_hqrecv_to = 0;
	T_layer_acc.T_hqrecv_crcerr = 0;
	/* T infobase <-- */

	/* --> N infobase */
	N_layer_acc.N_AckSendBWUsed = 0;
	N_layer_acc.N_AckSendBWTotal = 0;
	N_layer_acc.N_AckSendDelayed = 0;
	N_layer_acc.N_AckSendLoss = 0;

	N_layer_acc.N_AckRecved = 0;
	N_layer_acc.N_AckRecvLoss = 0;

	N_layer_acc.N_RecvDummy = 0;
	N_layer_acc.N_RecvNot4Me = 0;
	N_layer_acc.N_RecvHdrChksumErr = 0;
	N_layer_acc.N_RecvProtocolVerErr = 0;
	N_layer_acc.N_RecvProtocolTypeErr = 0;
	/* N infobase <-- */

	/* --> D infobase*/
	D_layer_acc.D_sendfail = 0;
	D_layer_acc.D_recvok = 0;
	D_layer_acc.D_recvartifacts = 0;
	D_layer_acc.D_recvRdErr = 0;
	D_layer_acc.D_recvDupack = 0;
	D_layer_acc.D_recvNullPack = 0;
	D_layer_acc.D_recvCRC1fail = 0;
	/* infobase <-- */
}

void print_rthq_infobase(FILE* out, int interval) {
	assert((NUM_RECV_RETRANS_MAX+2) >= 7);

	float ultilization_rt = (float)T_layer_acc.T_fillRT/(float)T_layer_acc.T_fillable;
	float ultilization_hq = (float)T_layer_acc.T_fillHQ/(float)T_layer_acc.T_fillable;
	float ultilization_total = ultilization_rt + ultilization_hq;

	fprintf(out, "rthq-T, %d, %5.3f, %5.3f, %5.3f, %d, "
			"%d, %d, %5.3f, "
			"%d, %d, %5.3f, "
			"%d, %d, %d, %d, "
			"%5.3f, %d, %d, %5.3f, "
			"%d, "
			"%d, %d, %d, %d, %d, %5.3f,"
			"%d, %d, %d, %d, %d, %5.3f,"
			"%d, %d, %d, "
			"%d, %d, %d, %d, ",
	T_layer_acc.T_send_throwaway,  ultilization_rt, ultilization_hq, ultilization_total, T_layer_acc.T_fillable/interval,
	T_layer_acc.T_rtsend_tLen.N, (int)cal_avg_sigma(&T_layer_acc.T_rtsend_tLen), cal_avg_sigma(&T_layer_acc.T_rtsend_duration),
	T_layer_acc.T_hqsend_tLen.N, (int)cal_avg_sigma(&T_layer_acc.T_hqsend_tLen), cal_avg_sigma(&T_layer_acc.T_hqsend_duration),
	T_layer_acc.T_hqsendstat.bin[0], T_layer_acc.T_hqsendstat.bin[1], T_layer_acc.T_hqsendstat.bin[2], T_layer_acc.T_hqsendstat.bin[3],
	cal_avg_sigma(&T_layer_acc.T_hqsend_RTT), T_layer_acc.T_hqsend_frac_retraned, T_layer_acc.T_hqsend_fracs, (float)T_layer_acc.T_hqsend_frac_retraned/(float)T_layer_acc.T_hqsend_fracs,
	T_layer_acc.T_recv_dupack,
	T_layer_acc.T_rtrecv_tLen.N, T_layer_acc.T_rtrecv_ok, T_layer_acc.T_rtrecv_to, T_layer_acc.T_rtrecv_crcerr, (int)cal_avg_sigma(&T_layer_acc.T_rtrecv_tLen), cal_avg_sigma(&T_layer_acc.T_rtrecv_numFrac),
	T_layer_acc.T_hqrecv_tLen.N, T_layer_acc.T_hqrecv_ok, T_layer_acc.T_hqrecv_to, T_layer_acc.T_hqrecv_crcerr, (int)cal_avg_sigma(&T_layer_acc.T_hqrecv_tLen), cal_avg_sigma(&T_layer_acc.T_hqrecv_numFrac),
	T_layer_acc.T_hqrecv_retrans.bin[0], T_layer_acc.T_hqrecv_retrans.bin[1], T_layer_acc.T_hqrecv_retrans.bin[2],
	T_layer_acc.T_hqrecv_retrans.bin[3], T_layer_acc.T_hqrecv_retrans.bin[4], T_layer_acc.T_hqrecv_retrans.bin[5], T_layer_acc.T_hqrecv_retrans.bin[6]);

	fprintf(out, "rthq-N, %d, %d, %d, %d, "
			"%d, %d, "
			"%d, %d, %d, %d, %d, ",
	N_layer_acc.N_AckSendBWUsed, N_layer_acc.N_AckSendBWTotal, N_layer_acc.N_AckSendDelayed, N_layer_acc.N_AckSendLoss,
	N_layer_acc.N_AckRecved, N_layer_acc.N_AckRecvLoss,
	N_layer_acc.N_RecvDummy, N_layer_acc.N_RecvNot4Me, N_layer_acc.N_RecvHdrChksumErr, N_layer_acc.N_RecvProtocolVerErr, N_layer_acc.N_RecvProtocolTypeErr);

	fprintf(out, "rthq-D, %d, %d, %d, %d, %d, %d, %d, ",
	D_layer_acc.D_sendfail, D_layer_acc.D_recvok , D_layer_acc.D_recvartifacts, D_layer_acc.D_recvRdErr, D_layer_acc.D_recvDupack, D_layer_acc.D_recvNullPack, D_layer_acc.D_recvCRC1fail);
}

void print_rthq_infobase_title(FILE* out) {
	assert((NUM_RECV_RETRANS_MAX+2) >= 7);
	fprintf(out, "type, %s, %s, %s, %s, %s, "
			"%s, %s, %s, "
			"%s, %s, %s, "
			"%s, %s, %s, %s, "
			"%s, %s, %s, %s, "
			"%s, "
			"%s, %s, %s, %s, %s, %s, "
			"%s, %s, %s, %s, %s, %s, "
			"%s, %s, %s, "
			"%s, %s, %s, %s, ",
	"T_send_throwaway", "ultilization_rt", "ultilization_hq", "ultilization_total", "total send bandwidth",
	"T_rtsend", "T_rtsend_tLen_avg", "T_rtsend_duration_avg",
	"T_hqsend", "T_hqsend_tLen_avg", "T_hqsend_duration_avg",
	"T_hqsend_ACK", "T_hqsend_GNAK", "T_hqsend_CNAK", "T_hqsend_NOACK",
	"T_hqsend_RTT_avg", "T_hqsend_frac_retraned", "T_hqsend_fracs", "retrans percent",
	"T_recv_dupack",
	"T_rtrecv", "T_rtrecv_ok", "T_rtrecv_to", "T_rtrecv_crcerr", "T_rtrecv_tLen_avg", "T_rtrecv_numFrac_avg",
	"T_hqrecv", "T_hqrecv_ok", "T_hqrecv_gnak", "T_hqrecv_cnak", "T_hqrecv_tLen_avg", "T_hqrecv_numFrac_avg",
	"T_hqrecv_retrans_0", "T_hqrecv_retrans_1", "T_hqrecv_retrans_2",
	"T_hqrecv_retrans_3", "T_hqrecv_retrans_4", "T_hqrecv_retrans_5", "T_hqrecv_retrans_allfail");

	fprintf(out, "type, %s, %s, %s, %s, "
			"%s, %s, "
			"%s, %s, %s, %s, %s, ",
	"N_AckSendBWUsed", "N_AckSendBWTotal", "N_AckSendDelayed", "N_AckSendLoss",
	"N_AckRecved", "N_AckRecvLoss",
	"N_RecvDummy", "N_RecvNot4Me", "N_RecvHdrChksumErr", "N_RecvProtocolVerErr", "N_RecvProtocolTypeErr");

	fprintf(out, "type, %s, %s, %s, %s, %s, %s, %s, ",
	"D_sendfail", "D_recvok","D_recvartifacts", "D_recvRdErr", "D_recvDupack", "D_recvNullPack", "D_recvCRC1fail");
}

