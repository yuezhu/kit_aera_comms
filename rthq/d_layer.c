/** @file d_layer.c
 *  @brief "datalink-layer" service functions implementation.
 *  @author Y.Zhu
 *  @bug No known bugs.
 */
#include "d_layer.h"

#ifdef SIM
#include <stdlib.h>
#include <stdio.h>
#include "timer.h"

static uint8_t SlotOpened = 3;
static uint8_t Num2send = 0;

uint32_t Frame_delay = FRAME_DELAY_MSEC;
struct tLayerTimer frame_timer; //test

static uint8_t Testbuftx[8][1024];
static bool Testbuftx_full[8] = {false,false,false,false,false,false,false,false};

#define PIPEDELAY	1
#define LEN_PBUF	10
static uint8_t Pipelinebufrx[LEN_PBUF][16][1024];
static bool Pipelinebufrx_full[LEN_PBUF][16];
static uint8_t Head_pbuf;
static uint8_t Tail_pbuf;
static uint8_t Tail_pbuf_pipedelay[PIPEDELAY];

static float Loss_rate;
static float Dup_rate;
static float Delay_rate;

void simulate_testChannel();

static void timeout_frame(void *arg)
{
	Num2send = SlotOpened;
	simulate_testChannel();
	reset_tLayerTimer(&frame_timer, Frame_delay); //endless frame_timer
	//printf("to.\n");
}

bool checkAcceptance(uint8_t i)
{
	return (i<SlotOpened?true:false);
}

void simulate_testChannel()
{
	int n;
	for(n=PIPEDELAY-1; n>0; n--) {
		Tail_pbuf_pipedelay[n] = Tail_pbuf_pipedelay[n-1];
	}
	Tail_pbuf_pipedelay[0] = Tail_pbuf;


	//fill the pbuf by txbuf with artefacts
	uint8_t Tail_pbuf_next = (Tail_pbuf+1)%LEN_PBUF;
	if (Tail_pbuf_next != Head_pbuf) { //not full
		int i;
		enum TYPE_ARTEFACT {OK, DELAYED, DUPLICATED} type_artefact[8];
		for (i=0; i<8; i++) {
			if (Testbuftx_full[i] == true) {
				if (rand() >= Loss_rate*(float)RAND_MAX) { // not lost
					if (rand() < Delay_rate*(float)RAND_MAX) { //delayed (and also can success)
						type_artefact[i] = DELAYED;
					}
					else { //not lost and (not delayed or delay insuccess)
						if (rand() < Dup_rate*(float)RAND_MAX) { //is duplication (and also can success)
							type_artefact[i] = DUPLICATED;
						}
						else {
							type_artefact[i] = OK;
						}
					}
				}
				else {
					Testbuftx_full[i] = false; //marked as processed
				}
			}
		}

		uint8_t mapping[16]; uint8_t mapping_index = 0;
		for (i=0; i<8; i++) {
			if (Testbuftx_full[i] == true) {
				if (type_artefact[i] == OK || type_artefact[i] == DUPLICATED) {
					mapping[mapping_index] = i;
					mapping_index++;
				}

				if (type_artefact[i] == DUPLICATED) { //added one more
					mapping[mapping_index] = i;
					mapping_index++;
				}
			}
		}
		for (i=0; i<8; i++) {
			if (Testbuftx_full[i] == true) {
				if (type_artefact[i] == DELAYED) {
					mapping[mapping_index] = i;
					mapping_index++;
				}
			}
		}

		for(i=0; i<mapping_index; i++) {
			memcpy(Pipelinebufrx[Tail_pbuf][i], Testbuftx[mapping[i]], 540); //memcpy with offset!
			Pipelinebufrx_full[Tail_pbuf][i] = true;
			Testbuftx_full[mapping[i]] = false; //marked as processed
		}

		Tail_pbuf = Tail_pbuf_next;
	}
}

void poll_checkTimeoutFrameTimer()
{
	//simulate timing sync
	checkTimeout_tLayerTimer(&frame_timer);
}


void init_testChannel(float loss_rate, float dup_rate, float delay_rate) {
	Loss_rate = loss_rate;
	Dup_rate = dup_rate;
	Delay_rate = delay_rate;
}

void set_frame_delay_msec(uint32_t frame_delay)
{
	Frame_delay = frame_delay;
}

void set_slot_opened(uint8_t slot_opened)
{
	SlotOpened = slot_opened;
}

void init_d_layer()
{
	init_tLayerTimer(&frame_timer, timeout_frame, NULL);

	Head_pbuf = 0;
	Tail_pbuf = 0;

	int i;
	for (i=0; i<LEN_PBUF; i++) {
		memset((void*)(Pipelinebufrx_full[i]), false, 8);
	}

	reset_tLayerTimer(&frame_timer, Frame_delay);
}

int d_get_bandwidth()
{
	return Num2send;
}

int d_get_send_rdy()
{
	return (Num2send>0);
}

void d_recv(post_t* post, uint8_t *p, unsigned short* p_id)
{
	if (p != NULL) {
		uint8_t tail = Tail_pbuf_pipedelay[PIPEDELAY-1]; //delayed indicator
		if (tail != Head_pbuf) { //not empty
			int i;
			for (i=0; i<16; i++) {
				if (Pipelinebufrx_full[Head_pbuf][i] == true) {
					memcpy(p, (void*)(Pipelinebufrx[Head_pbuf][i]), 540); //memcpy with offset!
					Pipelinebufrx_full[Head_pbuf][i] = false;
					set_post(post, LAYER_D, MSG_NOTE, NOTE_RECV_OK);
					return;
				}
			}
			Head_pbuf = (Head_pbuf+1)%LEN_PBUF; //not returned, the head_pbuf is finished
			return;
		}
		set_post(post, LAYER_D, MSG_NOTE, NOTE_RECV_NULL);
		return;
	}
	set_post(post, LAYER_D, MSG_ERR, ERR_NULLPTR);
}

void d_send(post_t* post, uint8_t *p, unsigned short payload_length)
{
	if (p != NULL) {
		if (d_get_send_rdy() > 0) {
			int i;
			for (i=0; i<8; i++) {
				if (checkAcceptance(i) == true) {
					if (Testbuftx_full[i] == false) {
						memcpy(Testbuftx[i], p, 540); //memcpy with offset!
						Testbuftx_full[i] = true;
						Num2send--;
						set_post(post, LAYER_D, MSG_NOTE, NOTE_SEND_OK);
						return;
					}
				}
			}
			set_post(post, LAYER_D, MSG_WARN, WARN_SENDBUFFBLOCKED);
			return;
		}
		set_post(post, LAYER_D, MSG_WARN, WARN_NUM2SEND_ZERO);
		return;
	}
	set_post(post, LAYER_D, MSG_ERR, ERR_NULLPTR);
}

#endif
