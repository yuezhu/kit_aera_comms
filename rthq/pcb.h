/** @file pcb.h
 *  @brief the core transfer structs of transport-layer, types and manipulation function prototypes.
 *
 *  @author Y.Zhu
 *  @bug No known bugs.
 */

#ifndef __PCB_H__
#define __PCB_H__

#include <stdint.h>
#include <stdbool.h>
#include "tdatagram.h" /* for NUMFRAC_MAX */
#include "objwrapper.h"  /* for post_t */
#include "timer.h" /* for tLayerTimeout_handler and performanceTimer*/
#include "id.h" /* for Qos_t */

#ifdef __cplusplus
extern "C" {
#endif

/** @brief general send transfer struct.
 */
struct pcb_send {
  struct pcb_send *next;
  
  Qos_t   qos;
  uint8_t  tId;
  staId_t recverId;
  
  uint32_t tLen; 
  uint16_t fracOffset;
  uint16_t numFrac;
  
  /** pointer to t-payload**/
  objwrapper_t* rc_tPayload; //tPayload = Amsg-container(TotalLength+n Amsg+Magic) + 4 bytes CRC (in network byte order)
  
  struct performanceTimer duration_timer;

  //state variable
  uint16_t fracIdCurrent;

  //for statistics
  uint32_t duration_msec;
};

/** @brief general recv transfer struct.
 */
struct pcb_recv {
  /** identifier of this pcb */
  //to form the linked list of rt_pcb
  struct pcb_recv *next;
  
  Qos_t   qos;
  uint8_t  tId;
  staId_t senderId;
  
  uint32_t tLen;
  uint16_t fracOffset;
  uint16_t numFrac;
  
  /** pointer to t-payload**/
  objwrapper_t* rc_tPayload;
  
  //state variable
  bool lFracRecved;
  uint64_t listFracIdRecved[NUMFRAC_MAX/64];
  struct tLayerTimer  rx_timer;
};

/** @brief RT send transfer struct "extends" general send transfer.
 */
struct rt_pcb_send {
  //common part of pcb
  struct pcb_send mpcb;

  //extra elements
};

/** @brief RT receive transfer struct "extends" general receive transfer.
 */
struct rt_pcb_recv {
  //common part of pcb
  struct pcb_recv mpcb;
  bool isCompleteAndCorrect; //new, used for sorted forward (with delay)
  struct tLayerTimer  rt_rx_timer; //the same as above
  bool is2FwdASAP; //new, used for sorted forward (with delay)
  //extra elements
};

/** @brief HQ send transfer struct "extends" general send transfer.
 */
struct hq_pcb_send {
  //common part of pcb
  struct pcb_send mpcb;

  //extra elements
  bool isFreezing; //is increment sending phase finished? (all fractions sended once in sequence?) --> freezing, only single fraction retrans by request
  struct tLayerTimer  hq_tx_timer;
  struct performanceTimer RTT_meas_timer;
  bool RTT_meas_timer_started;
  //for statistics
  uint32_t RTT_time_msec;
  uint32_t n_frac_retraned;
  int how_am_i_finished;
};

/** @brief HQ receive transfer struct "extends" general receive transfer.
 */
struct hq_pcb_recv {
  //common part of pcb
  struct pcb_recv mpcb;

  //extra elements
  uint8_t retrans_counter;
  struct performanceTimer RTT_meas_timer;
  uint16_t RTTattachedFracId; //0xffff meaning un-attached, valid values [0~NUMFRAC_MAX-1] meaning RTT_meas_timer attached to this fracId
};

/** @brief create a new send transfer with (internal malloc), return pointer to the object
 */
struct pcb_send*	new_pcb_send(Qos_t qos, uint32_t tLen, uint8_t tId, staId_t recverId, uint16_t fracOffset, uint16_t numFrac, objwrapper_t* rc_amsg_c);

/** @brief create a new recv transfer with (internal malloc), return pointer to the object
 */
struct pcb_recv*	new_pcb_recv(Qos_t qos, uint32_t tLen, uint8_t tId, staId_t senderId, uint16_t fracOffset, uint16_t numFrac, tLayerTimeout_handler handler);

/** @brief initialize a RT receive transfer
 */
void init_rt_pcb_recv(struct rt_pcb_recv *pcb, tLayerTimeout_handler handler, void *handler_arg);

/** @brief initialize a HQ receive transfer
 */
void init_hq_pcb_recv(struct hq_pcb_recv *pcb);

/** @brief initialize a RT send transfer
 */
void init_rt_pcb_send(struct rt_pcb_send *pcb);

/** @brief initialize a RT receive transfer
 */
void init_hq_pcb_send(struct hq_pcb_send *pcb, tLayerTimeout_handler handler, void *handler_arg);

/** @brief remove a send transfer from a linked list (internal free)
 */
void remove_pcb_send(struct pcb_send *pcb, struct pcb_send** ll_pcb);

/** @brief remove a receive transfer from a linked list (internal free)
 */
void remove_pcb_recv(struct pcb_recv *pcb, struct pcb_recv** ll_pcb);

/** @brief check if one frac in an on-going receive transfer is received
 */
bool isfracIdRecved(struct pcb_recv* pcb, uint16_t offset);

/** @brief inform an on-going receive transfer that one fraction of certain fracId (\a offset) is received
 */
void set_fracIdRecved(struct pcb_recv* pcb, uint16_t offset);

/** @brief check if an on-going receive transfer has received all the fractions completely
 */
bool check_completion(struct pcb_recv* pcb);

/** @brief fill in a list of missing fracId
 *
 *  Note, caller should use provide array of length NUMFRAC_MAX, no internal malloc
 *
 *  @param [in]	pcb	pointer to the receive transfer
 *  @param [out] list_fracId_missing
 *  @param [out] num_fracId_missing	pointer to the length of the list
 */
void build_fracId_missing(struct pcb_recv* pcb, uint16_t list_fracId_missing[], uint16_t* num_fracId_missing);

/** @brief verify a receive transfer if it's valid against the built-in crc32 of HQ/RT protocol
 *  @param [in]	pcb	pointer to the receive transfer
 *  @return bool
 */
bool verify_tPayloadCRC(struct pcb_recv* pcb);

/** @brief build and store the checksum for a filled send transfer
 *  @param [in,out]	pcb	pointer to the send transfer
 */
void build_tPayloadCRC(struct pcb_send* pcb);

#ifdef __cplusplus
}
#endif


#endif /* __PCB_H__ */
