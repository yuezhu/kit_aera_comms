/** @file t_layer.c
 *  @brief "transport-layer" of RT/HQ protocol service functions implementation.
 *
 *  @author Y.Zhu
 *  @bug No known bugs.
 */
#include <assert.h>

#include "rthq_CONFIG.h"
#include "t_layer.h"
#include "n_layer.h" /* use n_layer services */
#include "pcb.h"
#include "tdatagram.h"
#include "amsg_c.h"

#include "rthq_infobase.h"

/** @brief convert amsg payload length to actual length of bytes transfered on comms
 *
 *	(pyldLength+6), 4 bytes embedded CRC as "user data" at the message tail
 *
 */
#define TLEN_CONV(pyldLength)				(SOCKLEN_CONV(pyldLength)+sizeof(uint32_t))

/** @brief convert actual length of bytes transfered on comms back to amsg payload length
 *
 *	(pyldLength+6), 4 bytes embedded CRC as "user data" at the message tail
 *
 */
#define PYLDLENGTH_CONV_FROM_TLEN(tLen)		PYLDLENGTH_CONV(tLen-sizeof(uint32_t) //convert back

static struct pcb_recv *Ll_pcb_recv = NULL;
static struct pcb_send *Ll_pcb_send = NULL;
static Qos_t Last_pcb_send_qos = Qos_HQ;
static uint8_t Last_pcb_send_tId_RT = 0;
static uint8_t Last_pcb_send_tId_HQ = 0;

static uint32_t Rx_frac_timeout_msec;
static uint32_t Rx_frac_delayed_msec;
static uint32_t Tx_timeout_msec;
static uint32_t Tdelay_msec;
static uint8_t Num_recv_retrans;
static bool SortedRTRecv;

static uint32_t Bufsize_RT = BUFSIZE_RT_DEFAULT; //2k bytes
static uint32_t Bufsize_HQ = BUFSIZE_HQ_DEFAULT; //32k bytes

uint8_t Ip_bridge_txbuf[1024];
uint16_t Ip_bridge_txbuf_len = 0;
staId_t Ip_bridge_txbuf_recvId;
int Ip_bridge_txbuf_pending = 0;
uint8_t Ip_bridge_rxbuf[1024];
uint16_t Ip_bridge_rxbuf_len = 0;

extern struct performanceTimer PTimer;

void rt_send(post_t* post, struct pcb_send* pcb);
void hq_send(post_t* post, struct pcb_send* pcb);
void frac_recv(post_t* post, struct pcb_recv* pcb, objwrapper_t** rc_amsg_c_pt, uint8_t* tId, staId_t* senderId, bool islFrac);
void dummy_send(post_t* post);
uint16_t prepare_hq_recv_ack_send(struct pcb_recv* pcb, uint16_t* firstFracIdEnqueued, bool toEnqueueAck);
bool send_current_frac(post_t* post, struct pcb_send* pcb);
struct pcb_send* get_pcb_send_from_list(struct pcb_send* ll_pcb);
void get_pcb_tId_range(struct pcb_send* ll_pcb, int* tId_oldest_RT, int* tId_newest_RT,
												int* tId_oldest_HQ, int* tId_newest_HQ,
												unsigned long* tLenTotal_RT, unsigned long* tLenTotal_HQ,
												int* num_RT, int* num_HQ);
int get_next_pcb_tId(int tId_oldest, int tId_newest, uint8_t last_pcb_send_tId);
bool if_giveup_hq_recv(struct hq_pcb_recv* pcb);
void update_send_infobase(struct pcb_send* pcb);
void update_recv_infobase(struct pcb_recv* pcb);

void update_send_infobase(struct pcb_send* pcb)
{
	if (pcb->qos == Qos_RT) {
		add_sigma(&T_layer_acc.T_rtsend_tLen, (int)pcb->tLen);
		add_sigma(&T_layer_acc.T_rtsend_duration, (int)pcb->duration_msec);
	}
	else {
		add_sigma(&T_layer_acc.T_hqsend_tLen, (int)pcb->tLen);
		add_sigma(&T_layer_acc.T_hqsend_duration, (int)pcb->duration_msec);

        struct hq_pcb_send* p = (struct hq_pcb_send*)pcb;
        if (p->RTT_meas_timer_started == false) { //valid condition: if the send is finished with anything other than timeout, RTT is measured and valid
        	add_sigma(&T_layer_acc.T_hqsend_RTT, (int)p->RTT_time_msec);
        }
        T_layer_acc.T_hqsend_frac_retraned += p->n_frac_retraned;
        T_layer_acc.T_hqsend_fracs += pcb->numFrac;

        if (p->how_am_i_finished == (int)ACK) {
        	add_histo1d_sendstat(&T_layer_acc.T_hqsendstat, 0);
		} else if (p->how_am_i_finished == (int)GNAK) {
			add_histo1d_sendstat(&T_layer_acc.T_hqsendstat, 1);
		} else if (p->how_am_i_finished == (int)CNAK) {
			add_histo1d_sendstat(&T_layer_acc.T_hqsendstat, 2);
		} else if (p->how_am_i_finished == (int)NOACK) {
			add_histo1d_sendstat(&T_layer_acc.T_hqsendstat, 3);
		} else { //uninitialized or NAK? should never be
		}
	}
}

void update_recv_infobase(struct pcb_recv* pcb)
{
	if (pcb->qos == Qos_RT) {
		add_sigma(&T_layer_acc.T_rtrecv_tLen, (int)pcb->tLen);
		add_sigma(&T_layer_acc.T_rtrecv_numFrac, (int)pcb->numFrac);
	}
	else {
		add_sigma(&T_layer_acc.T_hqrecv_tLen, (int)pcb->tLen);
		add_sigma(&T_layer_acc.T_hqrecv_numFrac, (int)pcb->numFrac);

		struct hq_pcb_recv* p = (struct hq_pcb_recv*)pcb;
		uint8_t retrans = (p->retrans_counter < (NUM_RECV_RETRANS_MAX+2))?p->retrans_counter: (NUM_RECV_RETRANS_MAX+1); //saturate
		add_histo1d_recvretrans(&T_layer_acc.T_hqrecv_retrans, retrans); //bin index is p->retrans_counter
	}
}

uint16_t prepare_hq_recv_ack_send(struct pcb_recv* pcb, uint16_t* firstFracIdEnqueued, bool toEnqueueAck)
{
	//prepare NAK for retrans
	uint16_t list_fracId_missing[NUMFRAC_MAX];
	uint16_t num_fracId_missing;
	build_fracId_missing(pcb, list_fracId_missing, &num_fracId_missing);
	if (toEnqueueAck == true) {
		int i;
		ack_t hq_ack_send = {pcb->senderId, pcb->tId, Qos_HQ, 0, NAK};
		for (i=0; i<num_fracId_missing; i++) { //enqueue NAK (retrans request)
			hq_ack_send.fracId = list_fracId_missing[i]; //fill fracId
			n_enqueue_ack_send(&hq_ack_send);
		}
		if (num_fracId_missing>0) {
			*firstFracIdEnqueued = list_fracId_missing[0];
		}
	}
	return num_fracId_missing;
}

static void timeout_rt_recv(void *arg)
{
	struct pcb_recv* pcb = (struct pcb_recv*)arg;
	if (pcb->qos == Qos_RT) { //ensure RT
#ifdef DBG_T
		snapshot_performanceTimer(&PTimer, true, stdout); printf("DBG_T:%s - <|RX| RT timeout, tId: %d, senderId: %d>\n", __FUNCTION__, pcb->tId, pcb->senderId);
#endif
#ifdef RT_ACK
		ack_t rt_ack = {pcb->senderId, pcb->tId, Qos_RT, 0, GNAK}; //"group nak" meaning receiver "giving up" by timeout --> no patience
		n_enqueue_ack_send(&rt_ack);
#endif
		T_layer_acc.T_rtrecv_to++;
		update_recv_infobase(pcb);
		remove_pcb_recv(pcb, &Ll_pcb_recv); //free pcb
	}
}

static void timeout_rt_delayed_recv(void *arg)
{
	struct pcb_recv* pcb = (struct pcb_recv*)arg;
	if (pcb->qos == Qos_RT) { //ensure RT
#ifdef DBG_T
		snapshot_performanceTimer(&PTimer, true, stdout); printf("DBG_T:%s - <|RX| RT delayed timer timeout, tId: %d, senderId: %d>\n", __FUNCTION__, pcb->tId, pcb->senderId);
#endif
		struct rt_pcb_recv* p = (struct rt_pcb_recv*)pcb;
		p->is2FwdASAP = true;
	}
}

static void timeout_hq_recv(void *arg)
{
	struct pcb_recv* pcb = (struct pcb_recv*)arg;
	if (pcb->qos == Qos_HQ) { //ensure HQ
#ifdef DBG_T
		snapshot_performanceTimer(&PTimer, true, stdout); printf("DBG_T:%s - <|RX| HQ timeout, tId: %d, senderId: %d\n", __FUNCTION__, pcb->tId, pcb->senderId);
#endif
		struct hq_pcb_recv* p = (struct hq_pcb_recv*)pcb;
		bool if_giveup = if_giveup_hq_recv(p);
		uint16_t num_fracId_missing;
		if (if_giveup == true) {
			ack_t hq_ack_send = {pcb->senderId, pcb->tId, Qos_HQ, 0, GNAK};
			n_enqueue_ack_send(&hq_ack_send);
#ifdef DBG_T
			uint16_t firstFracIdEnqueued;
			num_fracId_missing = prepare_hq_recv_ack_send(pcb, &firstFracIdEnqueued, false);
			printf("-- GIVE UP!!!, #%d fracs left>\n", num_fracId_missing);
#endif
			p->retrans_counter = NUM_RECV_RETRANS_MAX+1; //set a specific value for give-up
			T_layer_acc.T_hqrecv_to++;
			update_recv_infobase(pcb);
			remove_pcb_recv(pcb, &Ll_pcb_recv);
		}
		else { //not given up, may because the Last_Frac is never sent
			uint16_t firstFracIdEnqueued;
			num_fracId_missing = prepare_hq_recv_ack_send(pcb, &firstFracIdEnqueued, true);
			if (num_fracId_missing > 0) { //at least one NAK sent
				p->retrans_counter++;
				//reactivate the rx_timer that is stopped before (last_frac first-time reception)
#ifdef DBG_T
				printf("-- to retrans %d time, #%d fracs>\n", p->retrans_counter, num_fracId_missing);
#endif
				reset_tLayerTimer(&(pcb->rx_timer), Rx_frac_timeout_msec);

				reset_performanceTimer(&(p->RTT_meas_timer), false, stdout); //start rx RTT_meas_timer
				p->RTTattachedFracId = firstFracIdEnqueued; //attach the RTT_meas_timer to a fracId that is just enqueued
			}
			else {
				//should not happen logically, this means timeout even the transfer is complete - return a GNAK, because the pcb is just removed (safety)
				//this could happen if timeout is implemented in IRQ that interrupts the non-atomic transfer function.
				ack_t hq_ack_send = {pcb->senderId, pcb->tId, Qos_HQ, 0, GNAK};
				n_enqueue_ack_send(&hq_ack_send);
#ifdef DBG_T
				printf("-- WARNING! tmr though complete>\n");
#endif
				T_layer_acc.T_hqrecv_to++;
				update_recv_infobase(pcb);
				remove_pcb_recv(pcb, &Ll_pcb_recv); //free pcb
			}
		}
	}
}

static void timeout_hq_send(void *arg)
{
	struct pcb_send* pcb = (struct pcb_send*)arg;
	if (pcb->qos == Qos_HQ) { //ensure HQ
		pcb->duration_msec = snapshot_performanceTimer(&(pcb->duration_timer), false, stdout);
#ifdef DBG_T
		snapshot_performanceTimer(&PTimer, true, stdout); printf("DBG_T:%s - <|TX| !!!HQ timeout, tId: %d, recverId: %d -- !!!GIVE UP!!! duration: %lu msec>\n", __FUNCTION__, pcb->tId, pcb->recverId, pcb->duration_msec);
#endif
		struct hq_pcb_send* p = (struct hq_pcb_send*)pcb;
		p->how_am_i_finished = (int)NOACK; //i am finished with timeout!
		update_send_infobase(pcb);
		remove_pcb_send(pcb, &Ll_pcb_send); //free pcb, the payload is not freed due to the previous retain
	}
}

void init_t_layer()
{
	Rx_frac_timeout_msec = RX_FRAC_TIMEOUT_MSEC_DEFAULT;
	Rx_frac_delayed_msec = RX_FRAC_DELAYED_MSEC_DEFAULT;
	Tdelay_msec = TDELAY_MSEC_DEFAULT;
	Tx_timeout_msec = TX_TIMEOUT_MSEC;
	Num_recv_retrans = NUM_RECV_RETRANS_DEFAULT;
	Bufsize_RT = BUFSIZE_RT_DEFAULT;
	Bufsize_HQ = BUFSIZE_HQ_DEFAULT;
	SortedRTRecv = false;

	init_n_layer();
}

unsigned long get_buffer_size(Qos_t qos)
{
	return (qos==Qos_RT)?Bufsize_RT:Bufsize_HQ;
}

void set_buffer_size(Qos_t qos, unsigned long size)
{
	qos==Qos_RT?(Bufsize_RT = size):(Bufsize_HQ = size);
}


void set_num_recv_retrans(uint8_t num)
{
	Num_recv_retrans = (num<NUM_RECV_RETRANS_MAX)?num:NUM_RECV_RETRANS_MAX;
}

void set_sorted_rt_recv()
{
	SortedRTRecv = true;
}

bool get_sorted_rt_recv()
{
	return SortedRTRecv;
}

void poll_checkTimeout()
{
	struct pcb_recv *pcb= Ll_pcb_recv;
	struct pcb_send *pcb2 = Ll_pcb_send;
	while(pcb != NULL) { //from recent to oldest
		struct pcb_recv* pcb_recv_next = pcb->next;
		if (pcb->qos == Qos_RT) { //sorted forward!
			struct rt_pcb_recv* p = (struct rt_pcb_recv*)pcb;
			checkTimeout_tLayerTimer(&(p->rt_rx_timer));
		}
		checkTimeout_tLayerTimer(&(pcb->rx_timer)); //should be here! or deep hiding bug! the pcb is freed and still used...
		pcb = pcb_recv_next;
	}

	while (pcb2 != NULL) {
		struct pcb_send* pcb_send_next = pcb2->next;
		if (pcb2->qos==Qos_HQ) {
			struct hq_pcb_send* p = (struct hq_pcb_send*)pcb2;
			checkTimeout_tLayerTimer(&(p->hq_tx_timer));
		}
		pcb2 = pcb_send_next;
	}
}

int get_tId_a_minus_b(uint8_t a, uint8_t b) //-32 ~ 31 rounding number
{
	uint8_t diff = (a>b)? a-b : NUM_TID+a-b;
	if (diff >= NUM_TID/2) return (int)diff-NUM_TID;
	else return (int)diff;
}

bool get_rt_forward_ready(objwrapper_t* rc_amsg_c_arr[SORT_DEPTH], int* num)
{
	*num = 0;
	//term: "early"-"late" at send side; "old"-"recent" at recv side
	//we should sort them from "old"-"recent" based on rx-time to "early"-"late" base on tId
	//now search for the "oldest" RT transfer in the queue of whatever sender
	bool isFwdASAPexist = false;
	struct pcb_recv *pcb= Ll_pcb_recv;
	struct pcb_recv *pcb_fwdASAP = NULL;
	while(pcb != NULL) { //found the recent
		struct pcb_recv* pcb_recv_next = pcb->next;
		if (pcb->qos == Qos_RT) { //sorted forward!
			struct rt_pcb_recv* p = (struct rt_pcb_recv*)pcb;
			if (p->is2FwdASAP == true) { //this RT transfer has to be forwarded immediately! this is surely the "oldest" transfer of some sender
				pcb_fwdASAP = pcb;
				isFwdASAPexist = true;
			}
		}
		pcb = pcb_recv_next;
	}
	if (isFwdASAPexist == false) {
		return false;
	}

	//ensure
	struct rt_pcb_recv* p = (struct rt_pcb_recv*)pcb_fwdASAP;
	if (p->is2FwdASAP != true) { //ensure again
		perror("is2FwdASAP found error\n");
		return false;
	}

	//record all recent rt of this senderId until the fwdASAP
	staId_t senderId = pcb_fwdASAP->senderId; //record the senderId
	uint8_t tId = pcb_fwdASAP->tId; //record the sender tId

	struct pcb_recv* pcb_tmp_arr[SORT_DEPTH]; //array of pointers to rt_pcb_recv of the same sender, which are ready, 32 is enough
	int ind = 0;
	pcb= Ll_pcb_recv; //from the head again
	while(pcb != pcb_fwdASAP && pcb != NULL) { //analyzing only more recent ones, n NULL is for security
		if  ((pcb->qos == Qos_RT) && (pcb->senderId == senderId)) { //record all RT of this senderId
			struct rt_pcb_recv* p_tmp = (struct rt_pcb_recv*)pcb;
			int tId_diff = get_tId_a_minus_b(pcb->tId, tId);
			if  ( (tId_diff < 0 && p_tmp->isCompleteAndCorrect == true) ||  //so called the ealier ones came late!//and also it's complete!
				  (p_tmp->is2FwdASAP == true) ) { //or this transfer should also be fwded immediately!
				pcb_tmp_arr[ind] = pcb;
				ind++;
				if (ind == SORT_DEPTH-1) {
					perror("SORT_DEPTH reached in delayed forward\n");
					break;
				}
			}
		} //else: just no other RT of this senderId
		pcb = pcb->next;
	}
	pcb_tmp_arr[ind] = pcb_fwdASAP; ind++; //put this one

	//sort, tId from smaller to big (earlier 2 late)
	int i, j;
	struct pcb_recv* swap;
	for (i=0; i < ind; i++) {
		for(j = i + 1; j < ind; j++){
			 int tId_diff = get_tId_a_minus_b(pcb_tmp_arr[i]->tId, pcb_tmp_arr[j]->tId);
			 if ( tId_diff > 0 ) {
				 swap = pcb_tmp_arr[j];
				 pcb_tmp_arr[j] = pcb_tmp_arr[i];
				 pcb_tmp_arr[i] = swap;
			 }
		 }
	}

	//debug print
	if (ind > 1) {
		snapshot_performanceTimer(&PTimer, true, stdout); printf("DBG_T:%s - <|RX| rt recv forward reorder! senderID: %d!>\n", __FUNCTION__, senderId);
	}

	for (i=0; i<ind ; i++) {
		rc_amsg_c_arr[i]	= pcb_tmp_arr[i]->rc_tPayload;
		objwrapper_retain(rc_amsg_c_arr[i]);
		//debug print
		if (ind > 1) {
			printf("%d, ", pcb_tmp_arr[i]->tId);
		}
		T_layer_acc.T_rtrecv_ok++;
		update_recv_infobase(pcb_tmp_arr[i]);
		remove_pcb_recv(pcb_tmp_arr[i], &Ll_pcb_recv); //the pcb_tmp_arr becomes dangling pointers...
	}
	*num = ind;
	return true;
}

void poll_recv(post_t* post, objwrapper_t** rc_amsg_c_pt, uint8_t* tId, Qos_t* qos, staId_t* senderId)
{
	uint8_t p[1024];
	n_recv(post, p);
	if (post->layer == LAYER_N && post->msgType == MSG_NOTE && post->msg == NOTE_RECV_OK) {
		struct tDtgrm_hdr* thdr = (struct tDtgrm_hdr*)p;
		/* convert src and dest ports to host byte order */
		uint8_t  tId_get 	 = TDTGRMH_TID(thdr);
		Qos_t	qos_get	     = (Qos_t)(TDTGRMH_TTYPE_QOS(thdr));
		uint32_t tLen_get	 = TDTGRMH_TLEN(thdr);
		uint8_t  lFrac_get	 = TDTGRMH_FLAGS_LFRAC(thdr);
		uint16_t fracLen_get	 = TDTGRMH_FRACLEN(thdr);
		staId_t senderId_get = (staId_t)TDTGRMH_SENDERID(thdr);
		//staId_t recverId_get = (staId_t)TDTGRMH_RECVERID(thdr);
		uint16_t fracId_get	 = TDTGRMH_FRACID(thdr);

		struct pcb_recv *pcb=NULL;
		for (pcb = Ll_pcb_recv; pcb != NULL; pcb = pcb->next) {
			/* compare PCB local addr+port to UDP destination addr+port */
			if (pcb->tId == tId_get && pcb->senderId == senderId_get && pcb->qos == qos_get) {
				break; //match one pcb! add data to this pcb
			}
		}

		if (pcb == NULL) { //no T-ID match --> a new transfer
			//init pcb
			uint16_t fracOffset = FRAC_LEN_MAX; //typical 456 bytes, this "assumption has to be done" in case the lastFrac comes first, the receiver has no idea of the fracOffset.
			uint16_t numFrac   = tLen_get/fracOffset + (((tLen_get%fracOffset)>0)?1:0); //ceil  --> how many fractions are needed?
			numFrac 		  = MIN(numFrac, NUMFRAC_MAX);

			pcb = new_pcb_recv(qos_get, tLen_get, tId_get, senderId_get, fracOffset, numFrac, (qos_get==Qos_RT?timeout_rt_recv:timeout_hq_recv)); //receive, no callback for RT, as the rt_rx_timer is multi-purpose: delay/delete, called manually
			if (pcb == NULL) { //no memory
				set_post(post, LAYER_T, MSG_ERR, ERR_MEM);
				return;
			}
			//initialize "inherited" object
			if (pcb->qos == Qos_RT) {
				init_rt_pcb_recv((struct rt_pcb_recv*)pcb, timeout_rt_delayed_recv, pcb);
			}
			else {
				init_hq_pcb_recv((struct hq_pcb_recv*)pcb);
			}

			//reset and activate the timer
			reset_tLayerTimer(&(pcb->rx_timer), Rx_frac_timeout_msec);

			//add pcb to head of ll_pcb_recv
			pcb->next = Ll_pcb_recv;
			Ll_pcb_recv = pcb;
		}
		else { //some T-ID match --> an existing transfer
			kick_tLayerTimer(&(pcb->rx_timer)); //reset and activate
		}

		if (isfracIdRecved(pcb, fracId_get)) {
			T_layer_acc.T_recv_dupack++;
			set_post(post, LAYER_T, MSG_WARN, WARN_RECV_DUPACK);
			//return; todo: feasible? or the msg_warn will be "masked out", or change the logic of set_post() is the correct way?
		}

		//copy data
		uint32_t byte_offset = fracId_get*(pcb->fracOffset);
		if (byte_offset+fracLen_get <= pcb->tLen) { //boundary check for memory copy
			if (pcb->rc_tPayload != NULL) {
				uint8_t* tPayload = objwrapper_get_object(pcb->rc_tPayload);
				if (tPayload != NULL) {
					memcpy(&(tPayload[byte_offset]), &(p[HDRBLEN]), fracLen_get); //memcpy with offset!
					set_fracIdRecved(pcb, fracId_get); //set this fracId as received
				}
			}
		}

		if (pcb->qos == Qos_HQ) {
			struct hq_pcb_recv* p = (struct hq_pcb_recv*)pcb;
			//the fraction is attached to a RTT_meas_timer that can be used to determine receive side RTT (NAK send->Retrans get)
			//qos, tLen, tId, senderId already match
			if (p->RTTattachedFracId == fracId_get) {
#ifdef DBG_T
				snapshot_performanceTimer(&PTimer, true, stdout); printf("DBG_T:%s - <|RX| RTT: %lu msec, attached fracId: %d>\n", __FUNCTION__, snapshot_performanceTimer(&(p->RTT_meas_timer), false, stdout), fracId_get); //evaluate rx RTT timer
#endif
			}
		}

		//do recv
		*qos = pcb->qos;
		bool islFrac = lFrac_get!=0?true:false;
		frac_recv(post, pcb, rc_amsg_c_pt, tId, senderId, islFrac);
		return;
	} else if (post->layer == LAYER_N && post->msgType == MSG_NOTE && post->msg == NOTE_RECV_IP) { //IP packet over bridge received!
		struct tIPgrm_hdr* iphdr = (struct tIPgrm_hdr*)p;
		/* convert src and dest ports to host byte order */
		uint16_t len	 = TIPGRMH_LEN(iphdr);
		if (len <= IP_LEN_MAX) {
			memcpy(Ip_bridge_rxbuf, &(p[IP_HDRBLEN]), len); //memcpy with offset!
			Ip_bridge_rxbuf_len = len;
			set_post(post, LAYER_T, MSG_NOTE, NOTE_RECV_IP);
		} else {
			set_post(post, LAYER_T, MSG_WARN, WARN_RECV_TOOLARGE);
		}
	}
	//return, post has lower level information
}

void frac_recv(post_t* post, struct pcb_recv* pcb, objwrapper_t** rc_amsg_c_pt, uint8_t* tId, staId_t* senderId, bool islFrac)
{
	struct hq_pcb_recv* p = NULL;
	if (pcb->qos == Qos_HQ) {
		p = (struct hq_pcb_recv*)pcb;
	}

	if (islFrac == true && pcb->lFracRecved == false) { //first received last_fraction
		pcb->lFracRecved = true; //set last fraction received

		//The Tdelay is shorter than receiver timeout based on the pre-knowledge (on this "unordered-delay")
		//this Tdelay is only activate once at the time of first reception of last_frac when no retrans happened
		if (p != NULL) { //is HQ
			if (p->retrans_counter == 0) {
				reset_tLayerTimer(&(pcb->rx_timer), Tdelay_msec);
			}
		}
#ifdef DBG_T
		snapshot_performanceTimer(&PTimer, true, stdout); printf("DBG_T:%s - <|RX| lFracRecved, qos: %d, tId: %d, senderId: %d>\n", __FUNCTION__, pcb->qos, pcb->tId, pcb->senderId);
#endif
	}
	if (pcb->lFracRecved == true) { //last fraction is received in this transfer
		if (check_completion(pcb)) { //complete
			if (verify_tPayloadCRC(pcb) == true) {
				*rc_amsg_c_pt	= pcb->rc_tPayload;
				*senderId	= pcb->senderId;
				*tId		= pcb->tId;
				if (pcb->qos == Qos_HQ) {
					//prepare feedback
					ack_t hq_ack_send = {pcb->senderId, pcb->tId, Qos_HQ, 0, ACK};
					n_enqueue_ack_send(&hq_ack_send);

					objwrapper_retain(*rc_amsg_c_pt);
					T_layer_acc.T_hqrecv_ok++;
					update_recv_infobase(pcb);
					remove_pcb_recv(pcb, &Ll_pcb_recv); //free pcb, the payload is retained
					set_post(post, LAYER_T, MSG_NOTE, NOTE_RECV_OK); //note the caller to be forwarded spontaneously
				}
				else { //RT
#ifdef RT_ACK
					ack_t rt_ack_send = {pcb->senderId, pcb->tId, Qos_RT, 0, ACK};
					n_enqueue_ack_send(&rt_ack_send);
#endif
					if (SortedRTRecv == true) {
						struct rt_pcb_recv* p2 = (struct rt_pcb_recv*)pcb;
						p2->isCompleteAndCorrect = true;
						//reset and activate the timer
						reset_tLayerTimer(&(p2->rt_rx_timer), Rx_frac_delayed_msec); //500 ms > 1 frame "durcheinander" time
						//important: delayed forward! no spontaneous forwarded by caller
						set_post(post, LAYER_T, MSG_NOTE, NOTE_RECV_OK_BUT_DELAYED_FORWARD);
					} else {
						objwrapper_retain(*rc_amsg_c_pt);
						T_layer_acc.T_rtrecv_ok++;
						update_recv_infobase(pcb);
						remove_pcb_recv(pcb, &Ll_pcb_recv); //free pcb, the payload is retained
						set_post(post, LAYER_T, MSG_NOTE, NOTE_RECV_OK); //note the caller to be forwarded spontaneously
					}
				}
			}
			else {
				if (pcb->qos == Qos_HQ) {
					ack_t hq_ack_send = {pcb->senderId, pcb->tId, Qos_HQ, 0, CNAK};
					n_enqueue_ack_send(&hq_ack_send);
					T_layer_acc.T_hqrecv_crcerr++;
				} else {
					T_layer_acc.T_rtrecv_crcerr++;
				}
#ifdef RT_ACK
				else {
					ack_t rt_ack_send = {pcb->senderId, pcb->tId, Qos_RT, 0, CNAK};
					n_enqueue_ack_send(&rt_ack_send);
				}
#endif
				set_post(post, LAYER_T, MSG_WARN, WARN_RECV_CRCERR);
				update_recv_infobase(pcb); //CNAK actually
				remove_pcb_recv(pcb, &Ll_pcb_recv); //free pcb, no retain so the payload is freed
			}
			return;
		}
		//though last fraction is received, not all fractions are received
		//do nothing till timeout
		set_post(post, LAYER_T, MSG_NOTE, NOTE_RECV_INCOMP);
		return;
	}
	//though last fraction is not yet received, no need to check completion
	//do nothing till timeout
	set_post(post, LAYER_T, MSG_NOTE, NOTE_RECV_NOTYETLASTFRAC);
}

void get_pcb_tId_range(struct pcb_send* ll_pcb, int* tId_oldest_RT, int* tId_newest_RT,
												int* tId_oldest_HQ, int* tId_newest_HQ,
												unsigned long* tLenTotal_RT, unsigned long* tLenTotal_HQ,
												int* num_RT, int* num_HQ);

bool check_congestion_ctrl(Qos_t qos,
		unsigned long* tLenTotal_RT, unsigned long* tLenTotal_HQ,
		int* num_RT, int* num_HQ)
{
	int tId_oldest_RT, tId_newest_RT, tId_oldest_HQ, tId_newest_HQ;
	get_pcb_tId_range(Ll_pcb_send, &tId_oldest_RT, &tId_newest_RT, &tId_oldest_HQ, &tId_newest_HQ, tLenTotal_RT, tLenTotal_HQ, num_RT, num_HQ);
	if ((qos == Qos_RT && *tLenTotal_RT > Bufsize_RT) || (qos == Qos_HQ && *tLenTotal_HQ > Bufsize_HQ)) {
		return false;
	}
	return true;
}

bool add_send(post_t* post, objwrapper_t *rc_amsg_c, Qos_t qos, staId_t recverId)
{
	struct pcb_send *pcb;
	//initialize a transfer
	if (rc_amsg_c != NULL) {
		//first check if the capacity is enough
		int tId_oldest_RT, tId_newest_RT, tId_oldest_HQ, tId_newest_HQ, num_RT, num_HQ;
		unsigned long tLenTotal_RT, tLenTotal_HQ;
		get_pcb_tId_range(Ll_pcb_send, &tId_oldest_RT, &tId_newest_RT, &tId_oldest_HQ, &tId_newest_HQ, &tLenTotal_RT, &tLenTotal_HQ, &num_RT, &num_HQ);

/* DEBUG
		printf("tLenTotal_RT: %lu, tLenTotal_HQ: %lu, num_RT: %d, num_HQ: %d\n", tLenTotal_RT, tLenTotal_HQ, num_RT, num_HQ);
		printf("***");
		struct pcb_send* pcb2 = Ll_pcb_send;
		while (pcb2 != NULL) { //traverse whole list
			if (pcb2->qos == Qos_RT) {
				printf("RT[%d:%lu]", pcb2->tId, pcb2->tLen);
			} else {
				printf("HQ[%d:%lu]", pcb2->tId, pcb2->tLen);
			}
			fflush(stdout);
			pcb2 = pcb2->next;
		}
		printf("***\n");
DEBUG END */
		int tId;
		if (qos == Qos_RT) {
			tId = get_next_pcb_tId(tId_oldest_RT, tId_newest_RT, Last_pcb_send_tId_RT);
		} else {
			tId = get_next_pcb_tId(tId_oldest_HQ, tId_newest_HQ, Last_pcb_send_tId_HQ);
		}
		if (tId < 0) { //if the flow control is used, should not happen
			set_post(post, LAYER_T, MSG_WARN, WARN_NO_TID_LEFT);
			T_layer_acc.T_send_throwaway++; //todo: put to correct place if flow control is changed
			return false;
		}

		//then check the length of the send
		uint16_t fracOffset = FRAC_LEN_MAX; //typical 456 bytes
		uint32_t tLen	   = TLEN_CONV(get_pyldLength(objwrapper_get_object(rc_amsg_c)));
		uint16_t numFrac	   = tLen/fracOffset + (((tLen%fracOffset)>0)?1:0); //ceil  --> how many fractions are needed?
		if (numFrac > NUMFRAC_MAX) {
			set_post(post, LAYER_T, MSG_WARN, WARN_SEND_TOOLARGE);
			return false;
		}

		//3rd realloc! dangerous! see comment of objwrapper_realloc()
		objwrapper_t* rc_amsg_c_realloced = objwrapper_realloc(rc_amsg_c, tLen, free);
		if (rc_amsg_c_realloced == NULL) {
			snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "MEM_ERR!%s - comms_int objwrapper realloc fail!\n", __FUNCTION__);
			set_post(post, LAYER_T, MSG_ERR, ERR_MEM);
			return false;
		}

		pcb = new_pcb_send(qos, tLen, (uint8_t)tId, recverId, fracOffset, numFrac, rc_amsg_c); //freed by complete transfer
		if (pcb == NULL) { //no memory
			set_post(post, LAYER_T, MSG_ERR, ERR_MEM);
			return false;
		}

		//initialize "inherited" object
		if (pcb->qos == Qos_RT) {
			//record the last send tId to increment next time
			Last_pcb_send_tId_RT = (uint8_t)tId;
			init_rt_pcb_send((struct rt_pcb_send*)pcb);
		}
		else {
			//record the last send tId to increment next time
			Last_pcb_send_tId_HQ = (uint8_t)tId;
			init_hq_pcb_send((struct hq_pcb_send*)pcb, timeout_hq_send, pcb);
		}

		//build tPayload CRC after the pcb object is created
		build_tPayloadCRC(pcb);

		//add pcb to head of ll_pcb_send (head->tail: newest->oldest)
		pcb->next = Ll_pcb_send;
		Ll_pcb_send = pcb;

		//increase ref counter. Putting it here not in new_pcb_send(), though no functional difference, is
		//because that it is the action of adding pcb - last two lines - that makes it referencable via pcb->rc_tPayload.
		//if the pcb is not correctly added  (though cannot happen), then pcb->rc_tPayload is also dangling.
		//An if-else should avoid this retain so that a release action somewhere else can free rc_tPayload (avoid leak)
		objwrapper_retain(rc_amsg_c);

		set_post(post, LAYER_T, MSG_NOTE, NOTE_ADDSEND_OK);
		return true;
	}
	set_post(post, LAYER_T, MSG_ERR, ERR_NULLPTR);
	return false;
}

//HQ RT prioritization happens here!
void get_pcb_tId_range(struct pcb_send* ll_pcb, int* tId_oldest_RT, int* tId_newest_RT,
												int* tId_oldest_HQ, int* tId_newest_HQ,
												unsigned long* tLenTotal_RT, unsigned long* tLenTotal_HQ,
												int* num_RT, int* num_HQ)
{
	struct pcb_send* pcb2 = ll_pcb;
	*tId_oldest_RT = -1;
	*tId_newest_RT = -1;
	*tId_oldest_HQ = -1;
	*tId_newest_HQ = -1;
	*tLenTotal_RT = 0;
	*tLenTotal_HQ = 0;
	*num_RT = 0;
	*num_HQ = 0;
	while (pcb2 != NULL) { //traverse whole list

		if (pcb2->qos == Qos_RT) {
			*tId_oldest_RT = pcb2->tId;
			if (*tId_newest_RT == -1) {
				*tId_newest_RT = pcb2->tId;
			}
			*tLenTotal_RT += pcb2->tLen;
			(*num_RT)++;
		} else {
			*tId_oldest_HQ = pcb2->tId;
			if (*tId_newest_HQ == -1) {
				*tId_newest_HQ = pcb2->tId;
			}
			*tLenTotal_HQ += pcb2->tLen;
			(*num_HQ)++;
		}
		pcb2 = pcb2->next;
	}
}

//HQ RT prioritization happens here!
int get_next_pcb_tId(int tId_oldest, int tId_newest, uint8_t last_pcb_send_tId)
{
	int next_tId = -1;
	int num_tId_left = 0;
	if (tId_newest == -1 || tId_oldest == -1) { //
		num_tId_left = NUM_TID; //no tId used
	}
	else {
		if (tId_newest >= tId_oldest) {
			num_tId_left = NUM_TID - (tId_newest-tId_oldest+1);
		} else {
			num_tId_left = tId_oldest - tId_newest - 1;
		}
	}
	// willkürlich! allow no more than 60 tIds in parallel, to ensure no "overlap" of tId in extreme case:
	// receiver still has the tId open, but sender "freed" it and assign it to another --> chaos
	if (num_tId_left > 4) {
		next_tId = (last_pcb_send_tId+1)%NUM_TID;
	}
	return next_tId; //if not enough tId can be assigned, -1 is returned
}

//HQ RT prioritization happens here!
struct pcb_send* get_pcb_send_from_list(struct pcb_send* ll_pcb) {
	struct pcb_send* rt_pcb_send_oldest = NULL;
	struct pcb_send* hq_pcb_send_oldest = NULL;
	struct pcb_send* pcb2 = ll_pcb;
	while (pcb2 != NULL) { //traverse whole list, find highest priority transfer (lastest RT or non-freezing HQ)
		if (pcb2->qos == Qos_HQ) {
			struct hq_pcb_send* p = (struct hq_pcb_send*)pcb2;
			if (p->isFreezing == false) { //in increment-sending phase (non-freezing)
				hq_pcb_send_oldest = pcb2;
			}
		}
		else {
			rt_pcb_send_oldest = pcb2;
		}
		pcb2 = pcb2->next;
	}
	if (rt_pcb_send_oldest == NULL && hq_pcb_send_oldest == NULL) {
		return NULL; //return NULL means the list is empty or the list is full of freezing HQ send
	} else if (rt_pcb_send_oldest != NULL && hq_pcb_send_oldest == NULL) {
		return rt_pcb_send_oldest;
	} else if (rt_pcb_send_oldest == NULL && hq_pcb_send_oldest != NULL) {
		return hq_pcb_send_oldest;
	} else { //both exist! both have half bandwidth - "switching"!
		if (Last_pcb_send_qos == Qos_HQ) {
			Last_pcb_send_qos = Qos_RT;
			return rt_pcb_send_oldest;
		} else {
			Last_pcb_send_qos = Qos_HQ;
			return hq_pcb_send_oldest;
		}
	}
}

void poll_send(post_t* post)
{
	struct pcb_send *pcb;
	//create and send fractions of existing send pcbs
	if (n_get_send_rdy() == 1) { //lower layer can accept fraction

		if (Ip_bridge_txbuf_pending == 1){ // priority: IP > retrans > rt > hq
			//avoided memcpy
			//uint8_t p[1024];
			//memcpy(&(p[IP_HDRBLEN]), Ip_bridge_txbuf, fracLen); //memcpy with offset
			if (Ip_bridge_txbuf_len <= IP_LEN_MAX) {
				struct tIPgrm_hdr* iphdr = (struct tIPgrm_hdr*)Ip_bridge_txbuf;
				TIPGRMH_LEN_SET(iphdr, Ip_bridge_txbuf_len);//set length
				TIPGRMH_RECVERID_SET(iphdr, Ip_bridge_txbuf_recvId);
				n_ip_send(post, Ip_bridge_txbuf, Ip_bridge_txbuf_len+IP_HDRBLEN); //ip bridge
			} else {
				//do nothing, just discard this pending packet
				set_post(post, LAYER_T, MSG_WARN, WARN_SEND_TOOLARGE);
			}
			Ip_bridge_txbuf_pending = 0; //handshake
		} else {

			ack_t ack_recv;
			bool isAFracRetransed = false;
			while (n_dequeue_ack_recv(&ack_recv)) {
				if (ack_recv.qos == Qos_RT) { //an RT-ack? do nothing, this is only for test
				}
				else { //Qos_HQ
					pcb = Ll_pcb_send;
					while(pcb != NULL) {
						if (pcb->qos == Qos_HQ && (get_myId() == ack_recv.senderId) && (pcb->tId == ack_recv.tId)) { //senderId, tId, qos all match
							break; //find a matching pcb! pcb is not NULL, break inner loop
						}
						pcb = pcb->next;
					}

					if (pcb == NULL) { //no matching pcb found! should seldom happen (could be HQ send is timeouted, and ACK is too late, indicating a too short HQ send timeout)
						snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s - warn: HQ ACKs received, but no associated transfer found!\n", __FUNCTION__);
					}
					else {
						struct hq_pcb_send* p = (struct hq_pcb_send*)pcb; //this associated transfer is definitely a HQ
						stop_tLayerTimer(&(p->hq_tx_timer)); //this send

						//measure sender side "RTT". the RTT meas involves receiver side timeouts from 0 to maximal Num_recv_retrans*RX_FRAC_TIMEOUT
						if (p->RTT_meas_timer_started == true) {
							p->RTT_time_msec = snapshot_performanceTimer(&(p->RTT_meas_timer), false, stdout);
	#ifdef DBG_T
							snapshot_performanceTimer(&PTimer, true, stdout); printf("DBG_T:%s - <|TX| RTT: %lu msec>\n", __FUNCTION__, p->RTT_time_msec); //evaluate
	#endif
							p->RTT_meas_timer_started = false; //only one measurement per
						}

						if (ack_recv.nakack == NAK && ack_recv.fracId < pcb->numFrac) { //retrans requested! and requested fracId is valid
	#ifdef DBG_T
							snapshot_performanceTimer(&PTimer, true, stdout); printf("DBG_T:%s - <|TX| retrans fracID: %d!>\n", __FUNCTION__, ack_recv.fracId);
	#endif
							pcb->fracIdCurrent = ack_recv.fracId;
							send_current_frac(post, pcb); //don't care if its last frac or not
							reset_tLayerTimer(&(p->hq_tx_timer), Tx_timeout_msec); //restart tx timer again by retransmitting a fraction
							isAFracRetransed = true;
							p->n_frac_retraned ++; //statistics

							break; //break the outer loop: no more dequeue ack_recv
						}
						else { //ACK, GNAK, CNAK finalizes send process, print
							if (ack_recv.nakack == ACK ||
								ack_recv.nakack == GNAK	||
								ack_recv.nakack == CNAK) {

								pcb->duration_msec = snapshot_performanceTimer(&(pcb->duration_timer), false, stdout);
	#ifdef DBG_T
								snapshot_performanceTimer(&PTimer, true, stdout); printf("DBG_T:%s -<|TX| HQ fin, tId: %d, recverId: %d, duration: %lu msec>\n", __FUNCTION__, pcb->tId, pcb->recverId, pcb->duration_msec);
	#endif
								p->how_am_i_finished = (int)(ack_recv.nakack);
								update_send_infobase(pcb);
								remove_pcb_send(pcb, &Ll_pcb_send); //important! the pcb is removed with tPayload freed
							} else {
								snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "%s - warn: unknown ack coding: %d or fracId: %d must smaller than %d!\n", __FUNCTION__, ack_recv.nakack, ack_recv.fracId, pcb->numFrac);
							}
						}
					}
				}
			}

			if (isAFracRetransed == false) { //no frac retrans, search for a fraction from a pcb to send
				pcb = get_pcb_send_from_list(Ll_pcb_send);
				if (pcb != NULL) { //send pcb found
					if (pcb->qos == Qos_RT) {
						rt_send(post, pcb);
					}
					else { //the freezing HQ send should be excluded by get_pcb_send_from_list()
						hq_send(post, pcb);
					}
				}
				else { //last send is dummy only to ensure ack
					dummy_send(post);
				}
			} else { //do nothing, as retrans packet is already sent in this function
			}
		}
	}
	set_post(post, LAYER_T, MSG_NOTE, NOTE_SEND_NOTREADY);
}

bool send_current_frac(post_t* post, struct pcb_send* pcb)
{
	uint8_t p[1024];
	struct tDtgrm_hdr* thdr = (struct tDtgrm_hdr*)p;

	//create header
	TDTGRMH_TLENFRACID_SET(thdr, pcb->tLen, pcb->fracIdCurrent);
	uint32_t byte_offset = (pcb->fracIdCurrent)*(pcb->fracOffset);
	uint16_t fracLen;
	uint8_t lFrac;

	uint32_t bytes_left = pcb->tLen - byte_offset;

	if (bytes_left <= pcb->fracOffset) { //last fraction
		fracLen = bytes_left;
		lFrac = 1;
	}
	else {
		fracLen = pcb->fracOffset;
		lFrac = 0;
	}
	TDTGRMH_TIDTTYPE_SET(thdr, pcb->tId, pcb->qos); //this qos should be HQ
	TDTGRMH_SENDERID_SET(thdr, get_myId());
	TDTGRMH_RECVERID_SET(thdr, pcb->recverId);
	TDTGRMH_LFRACFRACLEN_SET(thdr, lFrac, fracLen);

	if (pcb->rc_tPayload != NULL) {
		uint8_t* tPayload = objwrapper_get_object(pcb->rc_tPayload);
		if (tPayload != NULL) {
			memcpy(&(p[HDRBLEN]), &(tPayload[byte_offset]), fracLen); //memcpy with offset
		}
	}

	n_send(post, p, (HDRBLEN+fracLen)); //just suppose the n_send is correctly send!, no wait and repeat mechanism. be sure that the data in p is not lost

	/* update infobase */
	T_layer_acc.T_fillable += FRAC_LEN_MAX;
	if (pcb->qos == Qos_RT) {
		T_layer_acc.T_fillRT += fracLen;
	}
	else {
		T_layer_acc.T_fillHQ += fracLen;
	}

	return ((lFrac==1)?true:false);
}

void rt_send(post_t* post, struct pcb_send* pcb)
{
	bool islFrac = send_current_frac(post, pcb);
	if (islFrac) { //remove send after last
		pcb->duration_msec = snapshot_performanceTimer(&(pcb->duration_timer), false, stdout);
#ifdef DBG_T
		snapshot_performanceTimer(&PTimer, true, stdout); printf("DBG_T:%s - <|TX| RT fin, tId: %d, recverId: %d, duration: %lu msec>\n", __FUNCTION__, pcb->tId, pcb->recverId, pcb->duration_msec);
#endif
		update_send_infobase(pcb);
		remove_pcb_send(pcb, &Ll_pcb_send); //important! the pcb is removed with tPayload freed
	}
	else { //incrementing send phase
		pcb->fracIdCurrent++; //keep fracIdCurrent less than numFrac, since fracId as address can cause out-of-boundary problem
	}
}

void hq_send(post_t* post, struct pcb_send* pcb)
{
	struct hq_pcb_send* p = (struct hq_pcb_send*)pcb;
	if (p->isFreezing == false) { //in increment-sending phase (non-freezing)
		bool islFrac = send_current_frac(post, pcb);
		if (islFrac) {
			p->isFreezing = true; //freeze after sending lFrac
#ifdef DBG_T
			snapshot_performanceTimer(&PTimer, true, stdout); printf("DBG_T:%s - <|TX| HQ freeze, tId: %d, recverId: %d>\n", __FUNCTION__, pcb->tId, pcb->recverId);
#endif
			reset_tLayerTimer(&(p->hq_tx_timer), Tx_timeout_msec); //start tx timer at last fraction send
			reset_performanceTimer(&(p->RTT_meas_timer), false, stdout); //start tx RTT meas timer after sending lFrac, stopped by getting the first ack.
			p->RTT_meas_timer_started = true; //mark as started
		}
		else { //incrementing send phase
			pcb->fracIdCurrent++;
		}
	}
	else {
		//do nothing, suggested to be excluded by calling or inefficiency
	}
}

void dummy_send(post_t* post)
{
	uint8_t p[1024];
	struct tDtgrm_hdr* thdr = (struct tDtgrm_hdr*)p;
	TDTGRMH_TLENFRACID_SET(thdr, 0, 0); //tLen = 0 identifies a dummy tdatagram that will be checked by n-layer

	TDTGRMH_TIDTTYPE_SET(thdr, 0, Qos_RT);
	TDTGRMH_SENDERID_SET(thdr, get_myId());
	TDTGRMH_RECVERID_SET(thdr, STAID_BROADCAST);
	TDTGRMH_LFRACFRACLEN_SET(thdr, 0, 0);

	n_send(post, p, HDRBLEN); //fracLen == 0

	/* update infobase */
	T_layer_acc.T_fillable += FRAC_LEN_MAX;
}

bool if_giveup_hq_recv(struct hq_pcb_recv* pcb)
{
	if (pcb->retrans_counter >= Num_recv_retrans) {
		return true; //give up
	}
	return false;
}
