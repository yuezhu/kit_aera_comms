#include "firmware_tp.h"
#include "tdma_protocol.h"
#include "conversion.h"

firmware_logic_pkt_type get_type(comms_tp_packet* data)
{
    return (data->comms_tp_packet_data[FW_TYPE_OFS]);
}

bool is_firmware_packet(comms_tp_packet* data)
{
    uint32_t validation_code = conv_u8_arr(&data->comms_tp_packet_data[FW_VALIDATION_OFS], FW_VALIDATION_LEN);
    if (validation_code == FW_PKT_VALIDATION_CODE)
    {
        return true;
    }
    return false;
}

void unpack_raw(firmware_logic_pkt_struct* logic_pkt_ptr, comms_tp_packet* data)
{
    logic_pkt_ptr->pkt_type   = get_type(data);
    if (data->comms_tp_packet_id_destination == LOCAL_HOST) {
    	logic_pkt_ptr->source_type = LOCAL;
    }
    else { //only REMOTE_HOST is possible
    	logic_pkt_ptr->source_type = REMOTE;
    }

    if (logic_pkt_ptr->pkt_type == FW_START) {
        logic_pkt_ptr->start.sector_base    = data->comms_tp_packet_data[FW_SECTOR_BASE_OFS];
        logic_pkt_ptr->start.update_size      = conv_u8_arr(&data->comms_tp_packet_data[FW_UPDATE_SIZE_OFS], FW_UPDATE_SIZE_LEN);
    }
    else if (logic_pkt_ptr->pkt_type == FW_FRACTION) {
        logic_pkt_ptr->fraction.current_fraction = (uint16_t)conv_u8_arr(&data->comms_tp_packet_data[FW_CUR_FRACTION_OFS], FW_CUR_FRACTION_LEN);
        memcpy((void*)logic_pkt_ptr->fraction.data ,(void*)&data->comms_tp_packet_data[FW_FRACTION_OFS], 512); //memcpy!
    }
    else if (logic_pkt_ptr->pkt_type == FW_STOP) {
        logic_pkt_ptr->stop.firmware_crc32          = conv_u8_arr(&data->comms_tp_packet_data[FW_CRC32_OFS], FW_CRC32_LEN);
        logic_pkt_ptr->stop.is_reconfig_requested   = (data->comms_tp_packet_data[FW_RECONFIG_REQ_OFS]>0)?true:false;
    }
    else if (logic_pkt_ptr->pkt_type == FW_FB_START) {
        logic_pkt_ptr->fb_start.start_ack      = (start_ack_type)data->comms_tp_packet_data[FW_START_ACK_OFS];
        logic_pkt_ptr->fb_start.current_sector = data->comms_tp_packet_data[FW_CUR_SECTOR_OFS];
        logic_pkt_ptr->fb_start.total_sector   = data->comms_tp_packet_data[FW_TOTAL_SECTOR_OFS];
    }
    else if (logic_pkt_ptr->pkt_type == FW_FB_ERR) {
        logic_pkt_ptr->fb_error.debug_code     = data->comms_tp_packet_data[FW_DEBUGCODE_OFS];
    }
    else if (logic_pkt_ptr->pkt_type == FW_FB_STOP) {
        logic_pkt_ptr->fb_stop.fw_crc_ok       = (bool)data->comms_tp_packet_data[FW_FWCRC_OK_OFS];
        logic_pkt_ptr->fb_stop.reconfig_ack    = (bool)data->comms_tp_packet_data[FW_RECONFIG_ACK_OFS];
        logic_pkt_ptr->fb_stop.num_fracs_retrans = (uint16_t)conv_u8_arr(&data->comms_tp_packet_data[FW_NUM_FRACS_RETRANS_OFS], FW_NUM_FRACS_RETRANS_LEN);
        logic_pkt_ptr->fb_stop.num_fracs_retrans = (logic_pkt_ptr->fb_stop.num_fracs_retrans>256)?256:logic_pkt_ptr->fb_stop.num_fracs_retrans;
        int i;
        for (i=0; i<logic_pkt_ptr->fb_stop.num_fracs_retrans; i++) //memory copy!
        {
            logic_pkt_ptr->fb_stop.fracs_retrans[i] = (uint16_t)conv_u8_arr(&data->comms_tp_packet_data[FW_FRACS_RETRANS_OFS+i*FW_CUR_FRACTION_LEN], FW_CUR_FRACTION_LEN);
        }
    }
}

void pack_logic(comms_tp_packet* data, firmware_logic_pkt_struct* logic_pkt_ptr)
{
    conv_to_u8_arr(&data->comms_tp_packet_data[FW_VALIDATION_OFS], FW_PKT_VALIDATION_CODE, FW_VALIDATION_LEN);
    data->comms_tp_packet_data[FW_TYPE_OFS]    = logic_pkt_ptr->pkt_type;
    if (logic_pkt_ptr->pkt_type == FW_FB_START) {
        data->comms_tp_packet_data[FW_START_ACK_OFS]        = (uint8_t)logic_pkt_ptr->fb_start.start_ack;
        data->comms_tp_packet_data[FW_CUR_SECTOR_OFS]      = (uint8_t)logic_pkt_ptr->fb_start.current_sector;
        data->comms_tp_packet_data[FW_TOTAL_SECTOR_OFS]    = (uint8_t)logic_pkt_ptr->fb_start.total_sector;
        data->comms_tp_packet_length = FW_FB_START_PKT_BYTES;
    }
    else if (logic_pkt_ptr->pkt_type == FW_FB_ERR) {
        data->comms_tp_packet_data[FW_DEBUGCODE_OFS]       = (uint8_t)logic_pkt_ptr->fb_error.debug_code;
        data->comms_tp_packet_length 					   = FW_FB_ERR_PKT_BYTES;
    }
    else if (logic_pkt_ptr->pkt_type == FW_FB_STOP) {
        data->comms_tp_packet_data[FW_FWCRC_OK_OFS]        = (uint8_t)logic_pkt_ptr->fb_stop.fw_crc_ok;
        data->comms_tp_packet_data[FW_RECONFIG_ACK_OFS]    = (uint8_t)logic_pkt_ptr->fb_stop.reconfig_ack;
        logic_pkt_ptr->fb_stop.num_fracs_retrans = (logic_pkt_ptr->fb_stop.num_fracs_retrans>256)?256:logic_pkt_ptr->fb_stop.num_fracs_retrans;
        conv_to_u8_arr(&data->comms_tp_packet_data[FW_NUM_FRACS_RETRANS_OFS], (uint32_t)logic_pkt_ptr->fb_stop.num_fracs_retrans, FW_NUM_FRACS_RETRANS_LEN);
        int i;
        for (i=0; i<logic_pkt_ptr->fb_stop.num_fracs_retrans; i++) //memory copy!
        {
            conv_to_u8_arr(&data->comms_tp_packet_data[FW_FRACS_RETRANS_OFS+i*FW_CUR_FRACTION_LEN], (uint32_t)(logic_pkt_ptr->fb_stop.fracs_retrans[i]), FW_CUR_FRACTION_LEN);
        }
        data->comms_tp_packet_length	= FW_FB_STOP_PKT_BYTES;
    }
    //for self-test
    else if (logic_pkt_ptr->pkt_type == FW_START) {
    	data->comms_tp_packet_data[FW_SECTOR_BASE_OFS]	= logic_pkt_ptr->start.sector_base;
        conv_to_u8_arr(&data->comms_tp_packet_data[FW_UPDATE_SIZE_OFS], logic_pkt_ptr->start.update_size, FW_UPDATE_SIZE_LEN);
        data->comms_tp_packet_length	= FW_START_PKT_BYTES;
    }
    else if (logic_pkt_ptr->pkt_type == FW_FRACTION) {

        conv_to_u8_arr(&data->comms_tp_packet_data[FW_CUR_FRACTION_OFS], logic_pkt_ptr->fraction.current_fraction, FW_CUR_FRACTION_LEN);
        memcpy((void*)&data->comms_tp_packet_data[FW_FRACTION_OFS], (void*)logic_pkt_ptr->fraction.data, 512); //memcpy!
        data->comms_tp_packet_length	= FW_FRACTION_PKT_BYTES;
    }
    else if (logic_pkt_ptr->pkt_type == FW_STOP) {
        conv_to_u8_arr(&data->comms_tp_packet_data[FW_CRC32_OFS], logic_pkt_ptr->stop.firmware_crc32, FW_CRC32_LEN);
        data->comms_tp_packet_data[FW_RECONFIG_REQ_OFS]    = logic_pkt_ptr->stop.is_reconfig_requested;
        data->comms_tp_packet_length	= FW_STOP_PKT_BYTES;
    }
}

