#ifndef FIRMWARE_TP_H_
#define FIRMWARE_TP_H_

#include "../comms_tp.h"
#include <stdint.h>
#include <stdbool.h>

#define FW_PKT_VALIDATION_CODE  0x50ABA83F

typedef enum {
    FW_START    = 1,
    FW_FRACTION = 2,
    FW_STOP = 3,
    FW_FB_START = 4,
    FW_FB_STOP = 5,
    FW_FB_ERR = 6
} firmware_logic_pkt_type;

typedef enum {
	FW_START_INVALID    = 1,
	FW_ERASE_ONGOING   = 2,
	FW_ERASE_FAILED    = 3,
	FW_ERASE_SUCCESS   = 4
} start_ack_type;

typedef enum {
    LOCAL    = 1,
    REMOTE   = 2
} source_types;

typedef struct {
        firmware_logic_pkt_type pkt_type;
        source_types    source_type;
        union
        {
            struct
            {
                uint8_t  sector_base; //offset for the firmware file in flash
                uint32_t update_size; //bytes of the firmware file
            }start;

            struct
            {
                uint16_t current_fraction;
                uint8_t  data[512]; //pointer!!
            }fraction;

            struct
            {
                uint32_t firmware_crc32;
                bool is_reconfig_requested;
            }stop;
            
            struct
            {
                start_ack_type start_ack;
                uint8_t  current_sector; //epcs64: 64k sector
                uint8_t  total_sector;
            }fb_start;
            
            struct
            {
                bool fw_crc_ok;
                bool reconfig_ack;
                uint16_t num_fracs_retrans; //0 means last feedback, no retrans requested, the host can stop transmission
                uint16_t fracs_retrans[256];
            }fb_stop;
            
            struct
            {
                uint8_t debug_code;
            }fb_error;
        };
} firmware_logic_pkt_struct;

firmware_logic_pkt_type get_type(comms_tp_packet* data);
bool is_firmware_packet(comms_tp_packet* data);
void unpack_raw(firmware_logic_pkt_struct* logic_pkt_ptr, comms_tp_packet* data);
void pack_logic(comms_tp_packet* data, firmware_logic_pkt_struct* logic_pkt_ptr);

#endif /*FIRMWARE_TP_H_*/
