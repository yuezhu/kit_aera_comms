#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include "firmware_tp.h"
#include "epcs.h"

#include "crc32.h"
#include "timer.h"

//#define DEBUG_CORRUPT_FIRMWARE_10_35_99

uint32_t Crc_tx_temp = 0xffffffff;
uint32_t Update_size_left_tx;
uint16_t Current_fraction;
unsigned char Pkt_id_service_temp;

static uint8_t  Epcs_sectors_erased_flag[BLK_NUM];
static comms_timer_startstop Halt_timer;

extern bool Debug_comms_tp_1;
extern void exit_program();

FILE *FileDes = NULL;

bool generate_firmware_packet(comms_tp_packet* data, uint8_t sector_base, char* file_name, bool isReconfig, unsigned char service, component_code destination)
{
    typedef enum {FW_DONOTHING=-1, FW_IDLE=0, FW_STARTED=1, FW_STOPPED=2, FW_FINISH=3, FW_HALTED=4} transfer_host_states;
    static transfer_host_states State = FW_DONOTHING;
    static int Count = 0;
    firmware_logic_pkt_struct logic_pkt;
    bool pkt_to_send = false;
    uint32_t addr_offset;
    uint16_t sector_offset;

    if (State == FW_DONOTHING) {
        if (Count == 10){
            State = FW_IDLE;
            Count = 0;
        }
        else{
            Count ++;
        }
    }
    else if (State == FW_IDLE) {
    	unsigned long fileLen;
    	if (file_name != NULL) {
    	   FileDes = fopen(file_name, "rb");
    	   if (!FileDes)
    	   {
    		   fprintf(stderr, "Unable to open file %s\n", file_name);
    		   exit(0);
    	   }
    	   //Get file length
    	   fseek(FileDes, 0, SEEK_END);
    	   fileLen=ftell(FileDes);
    	   fseek(FileDes, 0, SEEK_SET);

    	   if (Debug_comms_tp_1 == true) {
    		   printf("FWhost - start firmware transfer, sector_base: %x, update_size: %ld B\n", sector_base, fileLen);
    	   }
       }
       else {
    	   fileLen = 0;
    	   if (Debug_comms_tp_1 == true) {
    		   printf("FWhost - no firmware to transfer, could be reconfig, sector_base: %x, update_size: %ld B\n", sector_base, fileLen);
    	   }
       }
       Crc_tx_temp          = 0xffffffff;
       Update_size_left_tx  = fileLen;
       Current_fraction     = 0;

       logic_pkt.pkt_type          = FW_START;
       logic_pkt.start.sector_base = sector_base;
       logic_pkt.start.update_size = Update_size_left_tx;

       memset((void*)Epcs_sectors_erased_flag, 0, BLK_NUM); //on receiving, clear all erased flags

       pkt_to_send = true;
       State = FW_STARTED;
    }
    else if (State == FW_STARTED) {
	   if (Update_size_left_tx > 0) {
		   //for (i=0; i<512; i++) {
		   //	logic_pkt.fraction.data[i] = (i+1)%256;
		   //}
		   fread(logic_pkt.fraction.data, 512, 1, FileDes);

		   if (Update_size_left_tx > 512) {
			  Crc_tx_temp = chksum_crc32_cont(Crc_tx_temp, logic_pkt.fraction.data, 512, false);
			  Update_size_left_tx = Update_size_left_tx - 512;
		   }
		   else {
			  Crc_tx_temp = chksum_crc32_cont(Crc_tx_temp, logic_pkt.fraction.data, Update_size_left_tx, false);
			  Update_size_left_tx = 0;
		   }

		   logic_pkt.pkt_type                  = FW_FRACTION;
		   logic_pkt.fraction.current_fraction = Current_fraction;
		   pkt_to_send 							= true;

	       //throttling send process (2 secs blank time) after packet send
		   addr_offset = 512*Current_fraction;
	       sector_offset = addr_offset>>BLK_LEN_LOG2; //truncate
	       if (Epcs_sectors_erased_flag[sector_base+sector_offset] == false) { //not erased yet
	    	   start_timer_startstop(&Halt_timer,false);
	    	   State = FW_HALTED;
	    	   if (Debug_comms_tp_1 == true) {
	    		   printf("FWhost - throttle packet sending for 2 seconds to after this packet for flash-erase, sector_base: %x, sector_offset: %x\n", sector_base, sector_offset);
	    	   }
	       }
		   Current_fraction ++;
	   }
	   else { //Update_size_left_tx = 0
		   Crc_tx_temp = chksum_crc32_cont(Crc_tx_temp, logic_pkt.fraction.data, Update_size_left_tx, true);
		   State = FW_STOPPED;
	   }
    }
    else if (State == FW_HALTED) {
    	stop_timer_startstop(&Halt_timer,false);
		if (get_time_from_start_to_stop(&Halt_timer) >= 2) {//2 seconds
	        addr_offset		= 512*Current_fraction;
	        sector_offset	= addr_offset>>BLK_LEN_LOG2; //truncate
			Epcs_sectors_erased_flag[sector_base+sector_offset] = true;
			State = FW_STARTED;
    	}
    }
    else if (State == FW_STOPPED) {
       logic_pkt.pkt_type                   = FW_STOP;
       logic_pkt.stop.firmware_crc32        = Crc_tx_temp;
       logic_pkt.stop.is_reconfig_requested = isReconfig;
       pkt_to_send = true;
       State = FW_FINISH;

	   if (Debug_comms_tp_1 == true) {
		   printf("FWhost - terminate firmware transfer, firmware_crc32: 0x%x, isReconfig: %d \n", Crc_tx_temp, isReconfig);
	   }
	   if (FileDes != NULL) {
		   fclose(FileDes);
	   }
    }

    if (pkt_to_send == true) {
       pack_logic(data, &logic_pkt);
       data->comms_tp_packet_id_service		= service;
       data->comms_tp_packet_id_destination = destination;
       data->comms_tp_packet_crc 		  	= crc_cal(data);

#ifdef DEBUG_CORRUPT_FIRMWARE_10_35_99
       if ((logic_pkt.pkt_type == FW_FRACTION) && ((logic_pkt.fraction.current_fraction == 10) || (logic_pkt.fraction.current_fraction == 35) || (logic_pkt.fraction.current_fraction == 99))) {
    	   data->comms_tp_packet_data[0] = 0x55;
    	   data->comms_tp_packet_data[1] = 0xAA;
       }
#endif

	   if (Debug_comms_tp_1 == true) {
		   printf("FWhost - send firmware packet with type:%x, crc %d:%d\n", logic_pkt.pkt_type, Current_fraction, (int)Crc_tx_temp);
	   }
	   return true;

	   /*//test
	   if (logic_pkt.pkt_type == FW_FRACTION) {
		   int j;
		   for (j=0; j<512/16; j++) {
			   printf("%x, %x, %x, %x, %x, %x, %x, %x, %x, %x, %x, %x, %x, %x, %x, %x\n", logic_pkt.fraction.data[j*16+0], \
					   logic_pkt.fraction.data[j*16+1], \
					   logic_pkt.fraction.data[j*16+2], \
					   logic_pkt.fraction.data[j*16+3], \
					   logic_pkt.fraction.data[j*16+4], \
					   logic_pkt.fraction.data[j*16+5], \
					   logic_pkt.fraction.data[j*16+6], \
					   logic_pkt.fraction.data[j*16+7], \
					   logic_pkt.fraction.data[j*16+8], \
					   logic_pkt.fraction.data[j*16+9], \
					   logic_pkt.fraction.data[j*16+10], \
					   logic_pkt.fraction.data[j*16+11], \
					   logic_pkt.fraction.data[j*16+12], \
					   logic_pkt.fraction.data[j*16+13], \
					   logic_pkt.fraction.data[j*16+14], \
					   logic_pkt.fraction.data[j*16+15]);
		   }
		   printf("----------------------\n");
	   }
	   return false;*/
    }
    return false;
}

void interpret_fw(comms_tp_packet* data)
{
	firmware_logic_pkt_struct logic_pkt;
	//simulate local
	unpack_raw(&logic_pkt, data);
	if (Debug_comms_tp_1 == true) {
	    if (logic_pkt.pkt_type == FW_FB_START) {
	    	printf("FWhost -  recv firmware packet feedback start source type: %d\n", logic_pkt.source_type);
	    }
	    else if (logic_pkt.pkt_type == FW_FB_ERR) {
	    	printf("FWhost -  recv firmware packet feedback error source type: %d, error_code: %d\n", logic_pkt.source_type, logic_pkt.fb_error.debug_code);
	    }
	    else if (logic_pkt.pkt_type == FW_FB_STOP) {
			printf("FWhost -  recv firmware packet feedback stop source type: %d, checksum ok: %d,\n" \
					"fraction retran-request number: %d, is reconfiged? %x\n", \
					logic_pkt.source_type, logic_pkt.fb_stop.fw_crc_ok, logic_pkt.fb_stop.num_fracs_retrans, logic_pkt.fb_stop.reconfig_ack);
			int i;
			if (logic_pkt.fb_stop.num_fracs_retrans > 0) {
				printf("list:");
				for (i=0; i<logic_pkt.fb_stop.num_fracs_retrans; i++) {
					printf("%d,", logic_pkt.fb_stop.fracs_retrans[i]);
				}
				printf("\n");
			}
			exit_program();
	    }
	}
}

