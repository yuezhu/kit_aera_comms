
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include "can4linux.h"
#define BLOCKING
/***********************************************************************
*
* set_bitrate - sets the CAN bit rate
*
*
* Changing these registers only possible in Reset mode.
*
* RETURN:
*
*/

int	set_bitrate(
	int fd,			/* device descriptor */
	int baud		/* bit rate */
	)
{
Config_par_t  cfg;
volatile Command_par_t cmd;
int ret;


    cmd.cmd = CMD_STOP;
    ioctl(fd, CAN_IOCTL_COMMAND, &cmd);

    cfg.target = CONF_TIMING; 
    cfg.val1   = baud;
    ret = ioctl(fd, CAN_IOCTL_CONFIG, &cfg);

    cmd.cmd = CMD_START;
    ioctl(fd, CAN_IOCTL_COMMAND, &cmd);

    if (ret < 0) {
	perror("set_bitrate");
	exit(-1);
    } else {
	ret = 0;
    }
    return ret;
}

/***********************************************************************
*
* set_lomode - sets the CAN in Listen Only Mode
*
*
* Changing these registers only possible in Reset mode.
*
* RETURN:
*
*/
/*int set_lomode(int fd)
{
Config_par_t  cfg;
volatile Command_par_t cmd;
int ret;

    cmd.cmd = CMD_STOP;
    ioctl(fd, CAN_IOCTL_COMMAND, &cmd);

    cfg.target = CONF_LISTEN_ONLY_MODE; 
    cfg.val1   = 1;
    ret = ioctl(fd, CAN_IOCTL_CONFIG, &cfg);

    cmd.cmd = CMD_START;
    ioctl(fd, CAN_IOCTL_COMMAND, &cmd);

    if (ret < 0) {
	perror("set_lomode");
	exit(-1);
    } else {
	ret = 0;
    }
    return ret;
}*/

int can_init (char *device, int baud)
{
	int fd;
	#ifdef BLOCKING
		fd = open(device, O_RDWR);
	#else
		fd = open(device, O_RDWR|O_NONBLOCK);
	#endif
	if( fd < 0 )
	{
		fprintf(stderr,"Error opening CAN device %s\n", device);
		perror("open");
		exit(1);
	}
	set_bitrate(fd, baud);
	return (fd);
}

void can_exit(int fd) {
    close(fd);
}

/*
int can_read_mon (int fd)
{
int got, id;
canmsg_t rx[100];
int messages_to_read = 1;
      got=read(fd, &rx, messages_to_read);
      printf("can_read(): got=%d\n", got);
      id = 0;
      if( got > 0) {
        int i;
        int j;
        for(i = 0; i < got; i++) {
			switch ( rx[i].id ) {
					case 0x100:		//Power measurement frame
					break;
				case 0x101:		//Pressure and temperature measurements frame
					break;
				default:		//Later expansions
					break;
			}
		}
	}
	else
	{
		perror("ERROR: can4linux driver read() call gives unknown return value\n");
	}
	return (id);
}*/

int can_reset(int fd) {
int ret;
volatile Command_par_t cmd;


    cmd.cmd = CMD_RESET;
    ret = ioctl(fd, CAN_IOCTL_COMMAND, &cmd);

    return ret;
}

int can_start(int fd) {
int ret;
volatile Command_par_t cmd;


    cmd.cmd = CMD_CLEARBUFFERS;
    ret = ioctl(fd, CAN_IOCTL_COMMAND, &cmd);
    cmd.cmd = CMD_START;
    ret = ioctl(fd, CAN_IOCTL_COMMAND, &cmd);

    return ret;
}

/*
* Show CAN errors as text,
* in case of Bus-Off, try to recover
*/
void displayerror(int fd, canmsg_t *rx)
{
/* static int buffcnt = 0; */

	printf("Flags 0x%02x,", rx->flags);
	if( 0 == (rx->flags & MSG_ERR_MASK)) {
		printf(" CAN Error Free");
	}
	if( rx->flags & MSG_WARNING) {
		printf(" CAN Warning Level,");
	}
	if( rx->flags & MSG_PASSIVE) {
		printf(" CAN Error Passive,");
	}
	if( rx->flags & MSG_BUSOFF) {
		printf(" CAN Bus Off,");
		can_reset(fd);
		/* sleep 100ms */
		usleep (100000);
		can_start(fd);
	}
	printf("\n");
}

int show_status(int can_fd)
{
	volatile CanStatusPar_t estatCAN;

	ioctl(can_fd, CAN_IOCTL_STATUS, &estatCAN);
	printf("%u, %u | %u, %u, %u, %u | %u, %u, %u, %u | %u\n", estatCAN.baud, estatCAN.status,
			estatCAN.error_warning_limit, estatCAN.rx_errors,  estatCAN.tx_errors, estatCAN.error_code,
			estatCAN.rx_buffer_size, estatCAN.rx_buffer_used, estatCAN.tx_buffer_size,estatCAN.tx_buffer_used,
			estatCAN.type);

	return(estatCAN.status);
}

//important! work around, used to almost bypass
//can4linux driver tx buffer and interrupt-machinism,
//that doesn't work reliably with the opensource SJA1000-based CAN-Chip!
//The problem could probably be the CAN-Chip FSM (status-bit), not the driver!
int is_send_secure(int can_fd)
{
#define MSK_PHILIPS_SJA1000_SR_TS (1<<5)//can_bsp.v: output transmit_status;
#define MSK_PHILIPS_SJA1000_SR_TBS (1<<2)//can_registers.v: 	reg transmit_buffer_status;

	volatile CanStatusPar_t estatCAN;

	ioctl(can_fd, CAN_IOCTL_STATUS, &estatCAN);

	//1. internal tx_buffer==0 FORCES that each write() NEVER appends a canmsg to the driver-tx-buffer but trigger a direct can-msg write into CAN-chip-tx-buffer
	//(check "can_write()" in write.c of the driver). This is used to forbidden the driver-internal "CAN_Interrupt()" to handle the direct write, which could corrupt CAN-chip FSM or tx-data!
	//2. gurantee that transmit status is back to 0! this is CAN-chip internel error, that _TS bit and _TBS bit happens to be both 1 --> WRONG! and everytime ceases sending.
	//3. gurantee that transmit buffer status is back to 0. This is normally true when 2. is fulfilled. But combined with 1. ensures direct triggering can-msg write into chip.
	//4. not implemented here, but each time, only one canmsg can be sent!!!!!!
	//all these should ensure the senario:
	//each canmsg write() should go directly into CAN-Chip -->
	//the CAN_TRANSMIT_INT interrupt by CAN-Chip would awake CAN_Interrupt(), but CAN_Interrupt() should not find pending canmsg in driver-tx-buffer -->
	//so the CAN_Interrupt() doesn't do msg-forwarding at all, just reallow direct write (Txfifo->active = 0)
	//the next canmsg write() take cares that _TS bit is set back to 0, for a 100% secure send.
	//....CRAZY work....
	int ret = //(estatCAN.tx_buffer_used == 0) &&
				((estatCAN.status & MSK_PHILIPS_SJA1000_SR_TS) == 0) &&
				((estatCAN.status & MSK_PHILIPS_SJA1000_SR_TBS) != 0);

	return ret;
}

int write_low_level(int can_fd, canmsg_t* tx) //mem provided by caller
{
	volatile Send_par_t par;
	par.Tx = tx; //redirect
	return (ioctl(can_fd, CAN_IOCTL_SEND, &par));
}

