/*
 * comms_tp_controller.h
 *
 *  Created on: Aug 29, 2012
 *      Author: dev
 */

#ifndef COMMS_TP_CONTROLLER_H_
#define COMMS_TP_CONTROLLER_H_

void init_comms_tp_controller();

int is_send_acceptable(unsigned char service_id);
int get_slot_opened();

void can_send(int fd);
void can_recv(int fd);
int poll_comms_ready();

#endif /* COMMS_TP_CONTROLLER_H_ */
