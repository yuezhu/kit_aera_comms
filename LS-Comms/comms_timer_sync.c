/*
 * comms_timer_sync.c
 *
 *  Created on: Dec 23, 2011
 *      Author: dev
 */

#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <time.h>
#include "comms_timer_sync.h"

//#define DEBUG_TIMER_SYNC

#define USEC_LATENCY_OFFSET 10000 //usecs should be multiples of 10ms = 10000us
#define LEN_ARRAY_USEC_INTERVAL 10

static timer_t Tid;
long Usec_interval; //1s initial
static long Array_usec_interval[LEN_ARRAY_USEC_INTERVAL];
static int Index_array_usec_interval;
static long Usec_last;

int Counter_expire_timer_sync;

enum state_timer_sync {UNINITIALIZED, INITIALIZED, STABLIZED, CALIBRATED} State_timer_sync;
//unitialized state: timer is run an arbitrary length (e.g. 1s) unaligned to sync_call
//initialized state: timer is run an arbitrary length (e.g. 1s) aligned to sync_call to calculate the duration
//stablized state: timer is set to the "initial" duration between the first two sync calls, could be non-accurate
//calibrated: "LEN_ARRAY_USEC_INTERVAL" number of sync calls are done, timer is set to the averaged and thus more accurate duration,
//	this duration is fixed to this value (not updated) to the end of the program run for stability,
//	but the timer will be periodically (every LEN_ARRAY_USEC_INTERVAL sync call) aligned to the sync call to eliminate long term offset
//	between 1pps-based sync call and linux system clock
//	if the duration of sync call (frame duration) is changed, which is a very rare case in application, the program should be restarted to adapt

//extern void open_socket_connections();

static long average_array_usec_interval()
{
	//long max=Array_usec_interval[0], min=Array_usec_interval[0];
	long sum=0;
	int i;
	for (i=0; i<LEN_ARRAY_USEC_INTERVAL; i++) {
		/*if (Array_usec_interval[i] > max) {
			max = Array_usec_interval[i];
		}
		else if (Array_usec_interval[i] < min) {
			min = Array_usec_interval[i];
		}*/
		sum += Array_usec_interval[i];
	}
	//return (sum-max-min)/(LEN_ARRAY_USEC_INTERVAL-2);
	return sum/LEN_ARRAY_USEC_INTERVAL;
}

void update_timer_sync(long usec)
{
    struct itimerspec itval;
	//initial timeout value
    itval.it_value.tv_sec = (time_t) USEC_LATENCY_OFFSET / 1000000L;
    itval.it_value.tv_nsec = (long) (USEC_LATENCY_OFFSET % 1000000L) * 1000;
    //periodic timeout value loaded after initial timeout
    itval.it_interval.tv_sec = (time_t) usec / 1000000L;
    itval.it_interval.tv_nsec = (long) (usec % 1000000L) * 1000;
	if (timer_settime(Tid, 0, &itval, NULL) != 0) {
	    perror("time_settime error!");
	}
#ifdef DEBUG_TIMER_SYNC
	printf("updated with usec: %ld\n", usec);
#endif
}

long get_timer_sync_resttime_to_next_exp()
{
	struct itimerspec tv;
	timer_gettime (Tid, &tv); //tv.it_value is the remaining time
	return (tv.it_value.tv_nsec/1000 + tv.it_value.tv_sec*1000000L);
}

void create_timer_sync(int sig_nr)
{
    struct sigevent sigev;

    // Create the POSIX timer to generate signo
    sigev.sigev_notify = SIGEV_SIGNAL;
    sigev.sigev_signo = sig_nr;
    sigev.sigev_value.sival_ptr = &Tid;
    //create a relative timer
    //CLOCK_REALTIME represents the machine's best-guess as to the current wall-clock, time-of-day time. As Ignacio and MarkR say,
    //this means that CLOCK_REALTIME can jump forwards and backwards as the system time-of-day clock is changed, including by NTP.
    //CLOCK_MONOTONIC represents the absolute elapsed wall-clock time since some arbitrary, fixed point in the past.
    //It isn't affected by changes in the system time-of-day clock.
    //If you want to compute the elapsed time between two events observed on the one machine without an intervening reboot, CLOCK_MONOTONIC is the best option.
    if (timer_create(1, &sigev, &Tid) != 0) { //arg0: CLOCK_MONOTONIC = 1,
        perror("timer_create error!");
    }
    reset_timer_sync();
    Counter_expire_timer_sync = 0;
}

void reset_timer_sync()
{
    Usec_interval = DEFAULT_INTERVAL_USEC; //1s initial
    Index_array_usec_interval = 0;
    State_timer_sync = UNINITIALIZED;
    Usec_last = USEC_LATENCY_OFFSET;
    //start 1s timer on init UNALLIGNED to any sync msg for the program
    //to indicate for how long the sync msg is not coming.
    update_timer_sync(Usec_interval);
}

void sync_timer_sync()
{
	long usec;
	if (State_timer_sync == UNINITIALIZED) {
		//could be much longer than the correct sync length (frame)
		//just for initialization of the timer
		//the 1s ALLIGNED timer is "restarted" to determine the relative delay between sync_call and timeout
		update_timer_sync(Usec_interval);
		State_timer_sync = INITIALIZED;
	}
	else if (State_timer_sync == INITIALIZED || State_timer_sync == STABLIZED || State_timer_sync == CALIBRATED) {
		//the Counter_expire_min_sync_timer_sync should be 1
		//Counter_expire_min_sync_timer_sync > 1 in case:
		//A. missed sync call (maybe missed sync can msg) - the timer sync is designed to be robust in missing several sync calls.
		//or B. Interval_Jitter_error, see below
		//Counter_expire_min_sync_timer_sync = 0 in case:
		//C. Sync_call_error, see below
		Counter_expire_timer_sync = 0;
		usec = get_timer_sync_resttime_to_next_exp();
		// sync_call_1 <-Usec_last-> timeout_1 <-(Usec_interval-usec)-> sync_call_2 <-usec-> timeout_2
		// usec_interval_next calculates duration between sync_call_1 and sync_call_2 = frame duration

		//Interval_Jitter_error: sync_call_1 <-Usec_last-> timeout_1 <-> timeout_2 <-(Usec_interval-usec)-> sync_call_2 <-usec-> timeout_3
		//this can happen when B1. the initial Usec_interval is too small,
		//or B2. USEC_LATENCY_OFFSET is too short compared to timer accuracy plus other jitter effects in program runtime
		//in this case Counter_expire_min_sync_timer_sync is at least 2,
		//usec is big and usec_interval_next is very small (a Usec_interval is missed) and thus erroneous.
		//this error should be AVOIDED!
		//the reason why not adding the missed Usec_interval is that: it collides with the case A (the program cannot decide case A or B)
		//we consider case A (missed sync call) is more often than case B (B1/2)
		//C. Sync_call_error: sync_call_1 <-Usec_last-> timeout_1 <-(Usec_interval-usec)-> sync_call_2 <-> sync_call_3 <-usec-> timeout_2
		//C1. sync call too often (duration between sync call too short) - should be AVOIDED!
		//C2. Usec_interval is too big - should only happen at the before timer stablized.
		//in this case: usec_interval_next is very big
		long usec_interval_next = Usec_last + (Usec_interval-usec);
		if (State_timer_sync == INITIALIZED) {
			//interval is initialized to a nearly correct sync length
			//since last update one initial timeout should already be triggered! this timeout is set in it_value, taken as USEC_LATENCY_OFFSET
			Usec_interval = usec_interval_next;
			//the second func call should "stablize" the timer to a nearly correct sync length
			//if the second func call is biased or missed (e.g. sync can_msg is lost), the initial sync length is bad.
			update_timer_sync(Usec_interval);
			Usec_last = USEC_LATENCY_OFFSET;
			State_timer_sync = STABLIZED;
		}
		else if (State_timer_sync == STABLIZED || State_timer_sync == CALIBRATED) {
			bool toPrint = false;
			Array_usec_interval[Index_array_usec_interval] = usec_interval_next;
			Index_array_usec_interval++;
			if (Index_array_usec_interval == LEN_ARRAY_USEC_INTERVAL) {
				Index_array_usec_interval = 0;
				if (State_timer_sync == STABLIZED) {
					//the sync length is averaged and "smoothed", stripped of one max and one min
					long avg = average_array_usec_interval();
					//update Usec_interval "LEN_ARRAY_USEC_INTERVAL" number of func calls.
					Usec_interval = avg;
					State_timer_sync = CALIBRATED;

					//open ethernet here!
					//open_socket_connections();

					toPrint = true;
				}
				//in CALIBTRATED state, Usec_interval will only be changed every "Index_array_usec_interval" number of sync calls
				//with fixed Usec_interval
				update_timer_sync(Usec_interval);
				Usec_last = USEC_LATENCY_OFFSET;
				if (toPrint == true) {
					printf("timer_sync::calibrated, interval is %ld usec\n", Usec_interval);
				}
			}
			else {
				Usec_last = usec;
			}
		}
#ifdef DEBUG_TIMER_SYNC
		printf("usec: %ld, usec_next: %ld, interval: %ld, state: %d\n", usec, usec_interval_next, Usec_interval, State_timer_sync);
#endif
	}
}

void delete_timer_sync()
{
	timer_delete(Tid);
}
