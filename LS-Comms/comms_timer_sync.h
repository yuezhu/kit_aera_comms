/*
 * comms_timer_sync.h
 *
 *  Created on: Dec 23, 2011
 *      Author: dev
 */

#ifndef COMMS_TIMER_SYNC_H_
#define COMMS_TIMER_SYNC_H_

#define DEFAULT_INTERVAL_USEC 1000000L

void create_timer_sync(int sig_nr);
void sync_timer_sync();
void reset_timer_sync();
void delete_timer_sync();

#endif /* COMMS_TIMER_SYNC_H_ */
