/*
 * infobase.h
 *
 *  Created on: Dec 21, 2012
 *      Author: dev
 */

#ifndef INFOBASE_H_
#define INFOBASE_H_


#include <stdio.h>

#include "rthq_infobase.h" /* transparent, for higher-level infos */
#include "comms_tp.h" /* for loopback_mode_t */

CREATE_HISTO1D(5,linkstat)

typedef struct {
	int value[2];
} double_value_t;

typedef struct {
	/* source: CAN messages */
	int su_id_inst;
	minmaxsigma2_t su_rssi_acc;
	minmaxsigma2_t su_rssi_bg_acc;
	minmaxsigma_t temperature_in_cel_acc;
	minmaxsigma_t tx_power_acc;

	double_value_t firmware_version_inst; //invalid cond
	double_value_t software_version_inst; //invalid cond
	loopback_mode_t loopback_mode_inst; //su specific, invalid cond: LOOPBACK_UNKNOWN
	linkstat_histo1d_t link_status_acc;

	int rx_total_sloterrors_inst; //in last 1000 slots: unfortunately, the SU doesn't give Err_count_stage...cannot use accumulator

	int bandwidth_acc;
	/* source: this program- d_layer (datap)*/
	int throughput_acc; //bandwidth
} station_stat_t;

extern station_stat_t Station_stat; //singleton

void reset_station_stat();
void print_station_stat(FILE* out, int seconds);
void print_station_stat_title(FILE* out);

#endif /* INFOBASE_H_ */
