/*
 * statistic_output.h
 *
 *  Created on: Jan 4, 2013
 *      Author: dev
 */

#ifndef STATISTIC_OUTPUT_H_
#define STATISTIC_OUTPUT_H_

void init_statistic_output(char* path, int interval);

void output_statistics();

#endif /* STATISTIC_OUTPUT_H_ */
