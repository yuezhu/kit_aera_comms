/*
 * comms_tp_fsm_buffer.c
 *
 *  Created on: Jan 2, 2012
 *      Author: dev
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include "comms_tp_controller.h"

#include "comms_tp.h"
#include "comms_timer_sync.h"

#include "timer.h"
#include "comms_signal.h"
#include "conversion.h"

#include "infobase.h"

#define SIGNR_CTRLC SIGINT
#define SIGNR_TIMER_ONESHOT SIGUSR1
#define SIGNR_TIMER_ONESHOT2 SIGHUP //ok to avoid terminal drop?
#define SIGNR_TIMER_SYNC SIGUSR2

#define SYNC_LOSS_COUNT 100
#define NUM_SYNC_CALL_COUNT_TESTSTABLE 300
#define NUM_SYNC_CALL_COUNT_TESTINSTABLE 600
#define NUM_SYNC_CALL_COUNT_TESTLOSS 900

bool Debug_comms_tp_3 = false;
extern bool Debug_comms_tp_1;
extern bool Debug_comms_tp_2;

static timer_t Tid;
static timer_t Tid2;

//shared or used between tp_fsm and timer_sync
extern int Counter_expire_timer_sync;
static int Sync_call_count = 0;
extern enum state_timer_sync {UNINITIALIZED, INITIALIZED, STABLIZED, CALIBRATED} State_timer_sync;
static int Last_state_timer_sync = (int)UNINITIALIZED;

extern int Pipefd[2];

comms_tp_packet Tx_data; //tx pending buffer
comms_tp_packet Rx_data; //rx combination buffer
bool Send_pending = false;
bool Recv_pending = false;

//shared between tp_fsm and data_packer
ibsb_struct Remote_ibsb;
bwb_struct Remote_bwb;

//public global between irq-service-routine and main program
int Sent_cnt = 0;
int Recved_cnt = 0;

static comms_tp_tx_state Tx_state = START;
unsigned short Tx_bytes_left;

static comms_tp_rx_state Rx_state = IDLE;
unsigned short Rx_bytes_left;
unsigned short Tx_pkt_id_currect = 0;

//packet send wait transfer status response from SU timeout
//notice: not smaller than 1/HZ = 10msecs or potentially accuracy problem.
int Send_waitresp_timeout_msec = 100;

//packet recv transfer timeout from start to stop
//notice: not smaller than 1/HZ = 10msecs or potentially accuracy problem.
int Recv_waitresp_timeout_msec = 1500; //1.5sec appropriate?


// since the sync can msg can be buffered in can tx buffer of sender or can rx buffer of receiver
// the time interval between the first several sync calls are irregular, thus cannot be used as timer synchronization
// So the first X sync can msgs should be ignored (without being used as sync call)
// the X depends on the sender/receiver tx/rx buffer size
int Num_first_n_sync_msg_ignored = 100;


//extern void close_socket_connections();
extern void exit_program();

static void tx_state_reload(void)
{
	Sent_cnt ++;
	//no change on Remote_ibsb and Remote_bwb
	Tx_state = START; //release the block of tx fsm
}

static void sig_handler1(int signo)
{
	if(signo == SIGNR_TIMER_ONESHOT)
	{
		fprintf(stderr,"%s - warn: CAN send timeout, no answer from SU\n", __FUNCTION__);
		D_layer_acc.D_sendfail++;
		//no need to disarm_timer since it is one shot!
		tx_state_reload();
		//disarm_timer_oneshot();
		//clear send pending of the corresponding packet id in buffer
		Send_pending = false;
	}
	else if(signo == SIGNR_TIMER_ONESHOT2)
	{
		fprintf(stderr,"%s - warn: CAN recv timeout, still no stop\n", __FUNCTION__);
		D_layer_acc.D_recvRdErr++;
		//no need to disarm_timer since it is one shot!
		//disarm_timer_oneshot();
		Rx_state = IDLE; //release the block of rx fsm
	}
	else if(signo == SIGNR_CTRLC)
	{
		exit_program();
	}
}

static void sig_handler2(int signo)
{
	static int Counter;
	if(signo == SIGNR_TIMER_SYNC)
	{
		if (State_timer_sync == CALIBRATED) {
			char c = 'f';
			write(Pipefd[1], &c, sizeof(char)); //any char, 'f' indicate frame
		}
		else if (State_timer_sync == UNINITIALIZED && Sync_call_count == 0) { //ignore phase not started
			Counter = (Counter+1)%60;
#ifdef DEBUG_TIMER_SYNC
			if (Counter%10 == 0) {
				printf("-\n");
			}
			if (Counter == 0) {
				printf("timer_sync::no LS_FRAME_SYNC message since %ld secs\n", DEFAULT_INTERVAL_USEC*60/1000000L);
			}
#endif
		}
		//Counter_expire_min_sync_timer_sync should be 0
		//if >0 : could be missed can sync msg
		Counter_expire_timer_sync++;
		if (Counter_expire_timer_sync > SYNC_LOSS_COUNT) { //missed too much can sync msgs: deemed synchronization loss -> resync
			Counter_expire_timer_sync = 0;
			if (State_timer_sync == CALIBRATED) { //to ensure only one resync and one console-output after synchonization loss, or it will periodically issue resync
				reset_timer_sync(); //resync

				//close ethernet here!
				//close_socket_connections();

				Counter	= 0;
				Sync_call_count = 0; //so that "Num_first_n_sync_msg_ignored" of can sync msgs will be ignored also after resync, the same as new program start
				fprintf(stderr, "timer_sync::synchonization loss: resync\n");
				//printf("%d, %d, %d, %d, %d\n", Tx_state, Rx_state, Tx_bytes_left, Rx_bytes_left, Tx_pkt_id_currect);

				//debug
				#include "can_funcs.h"
				extern int Fd;
				show_status(Fd);
			}
		}

#ifdef DEBUG_TIMER_SYNC
		printf("timer_sync expired, cnt: %d\n", Counter_expire_timer_sync);
#endif
	}
}

void init_comms_tp_controller()
{
	assign_signal(SIGNR_CTRLC, sig_handler1); //ctrl-c signal
	add_signal(SIGNR_TIMER_ONESHOT, sig_handler1); //SIGNR_TIMER_ONESHOT signal for timer
	add_signal(SIGNR_TIMER_ONESHOT2, sig_handler1); //SIGNR_TIMER_ONESHOT2 signal for timer
	assign_signal(SIGNR_TIMER_SYNC, sig_handler2); //SIGNR_TIMER_SYNC signal

	create_timer_oneshot(SIGNR_TIMER_ONESHOT, &Tid);
	create_timer_oneshot(SIGNR_TIMER_ONESHOT2, &Tid2);
	create_timer_sync(SIGNR_TIMER_SYNC);


	bool remote_norm_pkt_full_array[4] = {false, false, false, false};
	bool remote_norm_slot_opened_array[4] = {false, false, false, false}; //default: all slots closed
	unsigned char remote_ext_pkt_space_left = 0;
	unsigned char remote_ext_pkt_slot_opended = 0;
	init_ibsb(&Remote_ibsb,remote_norm_pkt_full_array, remote_ext_pkt_space_left);
	init_bwb(&Remote_bwb,remote_norm_slot_opened_array, remote_ext_pkt_slot_opended);
}

int is_send_acceptable(unsigned char service_id) {
	return (check_send_acceptable(&Remote_ibsb, &Remote_bwb, service_id) == OK_TO_SEND);
}

int get_slot_opened()
{
	int ret = Remote_bwb.slot_extpkt_num;
	int i;
	for (i=0; i<4; i++) {
		ret += Remote_bwb.slot_normpkt_open[i];
	} //pro frame
	return ret;
}

void can_send(int fd)
{
	comms_tp_event tx_event;
	//check if centain packet with packet id is pending
	if (Send_pending == true) {
		//big data block of rx has higher priority!!!, once the loop starts to receive data packets, the send process will be hold.
		if (Rx_state != STARTED) { //Rx_state == IDLE || Rx_state == STOPPED
			//comms_tp_tx_state temp_tx_state = Tx_state;
			//unsigned short temp_tx_bytes_left = Tx_bytes_left;
			//send part of the data packet with N can messages, N>=1 && N<=16
			if (Tx_state == START) {
				Tx_bytes_left = Tx_data.comms_tp_packet_length;
			}
			can_write_packet(fd, &Tx_data, &Tx_state, &tx_event, &Tx_bytes_left);
			if (tx_event.isTransEvent == true && tx_event.eventtype == TRANS_STOPPED) {
				//start oneshot timer which expires when no response comes
				arm_timer_oneshot(Send_waitresp_timeout_msec, &Tid);
				Tx_pkt_id_currect = Tx_data.comms_tp_packet_id_service;
				//clear send pending of the corresponding packet id in buffer
				//Send_pending = false;
				if (Debug_comms_tp_3 == true) {
					printf("packet send stopped, id: %d, dest: %d, length: %d, crc: %lx\n",
							Tx_data.comms_tp_packet_id_service, Tx_data.comms_tp_packet_id_destination, Tx_data.comms_tp_packet_length, Tx_data.comms_tp_packet_crc);
				}
			}
		}
	}
}

void can_recv(int fd)
{
	char* buffer[2];
	char str1[200], str2[200];
	buffer[0] = str1;
	buffer[1] = str2;

	comms_tp_event rx_event;

	comms_tp_rx_error rx_error;
	rx_error.isTransErr = false;
	rx_error.errortype = SUCCESSFUL;

	can_read(fd, &Rx_data, &Rx_state, &rx_event, &rx_error, &Rx_bytes_left);

	if (rx_event.isTransEvent == true)
	{
		if (rx_event.eventtype == SU_TRANS_STAT_RESPONSE)
		{
			Remote_ibsb = rx_event.trans_stat_response.ibsb; //object copy
			Remote_bwb = rx_event.trans_stat_response.bwb; //object copy
			if (Debug_comms_tp_3 == true) {
				print_tsb(&(rx_event.trans_stat_response.tsb), stdout);
				print_ibsb(&Remote_ibsb, stdout);
				print_bwb(&Remote_bwb, stdout);
				printf("last send with service id: %x\n", rx_event.trans_stat_response.trans_service_id);
			}
			if (Tx_state == RESPONSE) {
				comms_tp_packet data;
				decode_packet_id(&data, rx_event.trans_stat_response.trans_service_id);
				if (Tx_pkt_id_currect == data.comms_tp_packet_id_service) {
					tx_state_reload();
					disarm_timer_oneshot(&Tid);
					//clear send pending of the corresponding packet id in buffer
					Send_pending = false;

					/* give some debug, if both error_bit and warning_bit set, only error is shown*/
					if (rx_event.trans_stat_response.tsb.error_bit == true) {
						D_layer_acc.D_sendfail++;
						fprintf(stderr, "%s - last send of sid %d has error: code: %d\n", __FUNCTION__, rx_event.trans_stat_response.trans_service_id, rx_event.trans_stat_response.tsb.errortype);
					}
					else if (rx_event.trans_stat_response.tsb.warning_bit == true) {
						fprintf(stderr, "%s - last send sid %d has warning: code: %d\n", __FUNCTION__, rx_event.trans_stat_response.trans_service_id, rx_event.trans_stat_response.tsb.errortype);
					}

					/* transmit interval for security, disable it for performance */
					//long usec = get_timer_oneshot_resttime_to_next_exp(&Tid);
					//printf("time left to next expiration: %ld usec\n", usec);
					//usleep(usec); //compensate the timer
			    } else {
					fprintf(stderr, "%s - warn: a su_trans_stat_response received of sid %d, but not related to current sid %d?\n ", __FUNCTION__,
							rx_event.trans_stat_response.trans_service_id, Tx_pkt_id_currect);
			    }
			} else {
				fprintf(stderr, "%s - warn: a su_trans_stat_response of sid %d received unexpectly (tx-fsm is not in RESPONSE state), \n " \
						"maybe the response has delayed too long", __FUNCTION__, rx_event.trans_stat_response.trans_service_id);
			}


		}
		else if (rx_event.eventtype == TRANS_STARTED)
		{
			arm_timer_oneshot(Recv_waitresp_timeout_msec, &Tid2);
		}
		else if (rx_event.eventtype == TRANS_STOPPED)
		{
			disarm_timer_oneshot(&Tid2);

			comms_tp_event tx_event;
			tx_event.isTransEvent = true;
			tx_event.eventtype = LS_TRANS_STAT_RESPONSE;
			init_tsb(&(tx_event.trans_stat_response.tsb), rx_error.isTransErr, rx_error.errortype, false, false); //trans finish and no warning (only rx_error)
			bool local_norm_pkt_full_array[4] = {false, false, false, false};
			bool local_norm_slot_opened_array[4] = {true, true, true, true};
			unsigned char local_ext_pkt_space_left = 15;
			unsigned char local_ext_pkt_slot_opended = 15;
			init_ibsb(&(tx_event.trans_stat_response.ibsb),local_norm_pkt_full_array, local_ext_pkt_space_left); //encoded: 0xf0
			init_bwb(&(tx_event.trans_stat_response.bwb),local_norm_slot_opened_array,local_ext_pkt_slot_opended); //encoded: 0xff
			tx_event.trans_stat_response.trans_service_id = encode_packet_id(&Rx_data);

			if (rx_error.isTransErr == true) {
				fprintf(stderr, "%s - current recv of sid %d has error: code: %d\n", __FUNCTION__, Rx_data.comms_tp_packet_id_service, rx_error.errortype);
				D_layer_acc.D_recvRdErr++;
				D_layer_acc.D_recvartifacts++;
				if (rx_error.errortype == CRC_ERROR) {
					D_layer_acc.D_recvCRC1fail++;
					D_layer_acc.D_recvartifacts++;
				}
			} else if (Rx_data.comms_tp_packet_length == 0) {
				D_layer_acc.D_recvNullPack++;
				D_layer_acc.D_recvartifacts++;
			} else { //error packet and zero length packet ("null-packet") will not be provided for processing
				Recv_pending = true;
			}

			can_write_message(fd, &tx_event);

			if (Debug_comms_tp_3 == true) {
				printf("packet recv stopped, id: %d, dest: %d, length: %d, crc: %lx, errortype: %d\n",
							Rx_data.comms_tp_packet_id_service, Rx_data.comms_tp_packet_id_destination, Rx_data.comms_tp_packet_length, Rx_data.comms_tp_packet_crc, rx_error.errortype);
			}

			Rx_state = IDLE; //release the block of rx fsm
			Recved_cnt ++;
		}
		else if (rx_event.eventtype == LS_FRAME_SYNC)
		{
			if (Sync_call_count < Num_first_n_sync_msg_ignored) {
				if (Debug_comms_tp_1 == true) {
					if (Sync_call_count == 0) {
						printf("timer_sync::start ignore can sync message.\n");
					}
					else if (Sync_call_count == Num_first_n_sync_msg_ignored - 1) {
						printf("timer_sync::sync done.\n");
					}
					else {
						printf("."); fflush(stdout);
					}
				}
				Sync_call_count ++;
			}
			else {
				if (Debug_comms_tp_2 == true) {
					printf(".\n"); //
				}
				sync_timer_sync();
				Remote_ibsb = rx_event.ibsb_response.ibsb; //object copy
				Remote_bwb = rx_event.ibsb_response.bwb; //object copy

				//log wireless stat to a log file
				int n = get_slot_opened();
				Station_stat.bandwidth_acc += n*540;
			}
		}
		else if (rx_event.eventtype == SU_STATUS_RESPONSE) {
			unsigned char* data = rx_event.status_response.data;
			if (data[0] == 0) { //SU_STATUS_RESPONSE type 0
				link_status_t link_status = data[1];
				signed char tx_power = data[2];
				signed char su_rssi = data[3];
				signed char su_rssi_bg = data[4];
				signed char temperature = data[5];
				unsigned short rx_slots_err = conv_u8_arr(&data[6], 2);

				char* s;
				switch (link_status) {
					case awaiting_connection: 	s = "awaiting";
												add_histo1d_linkstat(&Station_stat.link_status_acc, 0); //map to bin 0
												break;
					case established:           s = "established";
												add_histo1d_linkstat(&Station_stat.link_status_acc, 1); //map to bin 1
												break;
					case broken:				s = "broken";
												add_histo1d_linkstat(&Station_stat.link_status_acc, 2); //map to bin 2
												break;
					case unused:                s = "unused";
												add_histo1d_linkstat(&Station_stat.link_status_acc, 3); //map to bin 3
												break;
					default:                    s = "undefined";
												add_histo1d_linkstat(&Station_stat.link_status_acc, 4); //map to bin 4. unknown
												break;
				}
				add_minmaxsigma2(&Station_stat.su_rssi_acc, (int)su_rssi);
				add_minmaxsigma2(&Station_stat.su_rssi_bg_acc, (int)su_rssi_bg);
				add_minmaxsigma(&Station_stat.tx_power_acc, (int)tx_power);
				add_minmaxsigma(&Station_stat.temperature_in_cel_acc, (int)temperature);
				Station_stat.rx_total_sloterrors_inst = rx_slots_err;

			} else { //SU_STATUS_RESPONSE type 1
				unsigned short id = conv_u8_arr(&data[1], 2);
				unsigned char fw_ver_main = data[3];
				unsigned char fw_ver_sub = data[4];
				unsigned char sw_ver_main = data[5];
				unsigned char sw_ver_sub = data[6];
				loopback_mode_t loopback_mode = data[7];

				Station_stat.su_id_inst = id;
				Station_stat.firmware_version_inst.value[0] = fw_ver_main;
				Station_stat.firmware_version_inst.value[1] = fw_ver_sub;
				Station_stat.software_version_inst.value[0] = sw_ver_main;
				Station_stat.software_version_inst.value[1] = sw_ver_sub;
				Station_stat.loopback_mode_inst = loopback_mode;
			}
		}
		else if (rx_event.eventtype == SU_CHANGELOOPBACKMODE_RESPONSE) {
			printf("SU_CHANGELOOPBACKMODE_RESPONSE\n");
		}
		else if (rx_event.eventtype == SU_RESET_RESPONSE) {
			printf("SU_RESET_RESPONSE\n");
		}
	}
}

int poll_comms_ready()
{
	int ret;
	if (State_timer_sync == CALIBRATED && Last_state_timer_sync != (int)CALIBRATED) {
		ret = 1; //becomes ready
	}
	else if (State_timer_sync != CALIBRATED && Last_state_timer_sync == (int)CALIBRATED) {
		ret = 2; //lost ready
	}
	else {
		ret = 0;
	}
	Last_state_timer_sync = (int)State_timer_sync;
	return ret;
}
