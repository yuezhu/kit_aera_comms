/** @file infobase.c
 *  @brief
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */
#include <math.h> /* for nan.h */
#include "infobase.h"

CREATE_HISTO1D_C(5,linkstat)

station_stat_t Station_stat; //singleton

void reset_station_stat()
{
	Station_stat.su_id_inst = -1; //invalid

	reset_minmaxsigma2(&Station_stat.su_rssi_acc);
	reset_minmaxsigma2(&Station_stat.su_rssi_bg_acc);
	reset_minmaxsigma(&Station_stat.temperature_in_cel_acc);
	reset_minmaxsigma(&Station_stat.tx_power_acc);

	Station_stat.firmware_version_inst.value[0] = -1;  //invalid
	Station_stat.firmware_version_inst.value[1] = -1;  //invalid
	Station_stat.software_version_inst.value[0] = -1;  //invalid
	Station_stat.software_version_inst.value[1] = -1;  //invalid
	Station_stat.loopback_mode_inst = LOOPBACK_UNKNOWN;

	Station_stat.rx_total_sloterrors_inst = -1;

	Station_stat.throughput_acc = 0;
	Station_stat.bandwidth_acc = 0;

	reset_histo1d_linkstat(&Station_stat.link_status_acc);
}

void print_station_stat(FILE* out, int seconds) {
	fprintf(out, "su, %d, "
			"%d, %d, %d, %d, %d, "
			"%d, %d, "

			"%d, "
			"%d, %d, %d, %5.3f, "
			"%d, %d, %d, %5.3f, "
			"%d, %d, %d, "
			"%d, %d, %d, "
			"%d.%d, %d.%d, "
			"%d, ",
	Station_stat.su_id_inst,
	Station_stat.link_status_acc.bin[0], Station_stat.link_status_acc.bin[1], Station_stat.link_status_acc.bin[2], Station_stat.link_status_acc.bin[3], Station_stat.link_status_acc.bin[4],
	Station_stat.throughput_acc/seconds, Station_stat.bandwidth_acc/seconds,

	Station_stat.rx_total_sloterrors_inst,
	Station_stat.su_rssi_acc.min_reg, Station_stat.su_rssi_acc.max_reg, (int)cal_avg_minmaxsigma2(&Station_stat.su_rssi_acc), cal_stddev_minmaxsigma2(&Station_stat.su_rssi_acc),
	Station_stat.su_rssi_bg_acc.min_reg, Station_stat.su_rssi_bg_acc.max_reg, (int)cal_avg_minmaxsigma2(&Station_stat.su_rssi_bg_acc), cal_stddev_minmaxsigma2(&Station_stat.su_rssi_bg_acc),
	Station_stat.tx_power_acc.min_reg, Station_stat.tx_power_acc.max_reg, (int)cal_avg_minmaxsigma(&Station_stat.tx_power_acc),
	Station_stat.temperature_in_cel_acc.min_reg, Station_stat.temperature_in_cel_acc.max_reg, (int)cal_avg_minmaxsigma(&Station_stat.temperature_in_cel_acc),
	Station_stat.firmware_version_inst.value[0], Station_stat.firmware_version_inst.value[1], Station_stat.software_version_inst.value[0], Station_stat.software_version_inst.value[1],
	Station_stat.loopback_mode_inst);
}

void print_station_stat_title(FILE* out) {
	fprintf(out, "type, %s, "
			"%s, %s, %s, %s, %s, "
			"%s, %s, "

			"%s, "
			"%s, %s, %s, %s, "
			"%s, %s, %s, %s, "
			"%s, %s, %s, "
			"%s, %s, %s, "
			"%s, %s, "
			"%s, ",
	"id",
	"awaiting", "established", "broken", "unused", "unknown",
	"Bytes per sec", "max Bytes per sec",

	/* the following part are invalid if SU is not connected (no uplink packets received) */
	"downlink: rx slot errors in last 1000 slots",
	"su_rssi.min", "su_rssi.max", "su_rssi.avg", "su_rssi.stddev",
	"su_rssi_bg.min", "su_rssi_bg.max", "su_rssi_bg.avg", "su_rssi_bg.stddev",
	"tx_power.min", "tx_power.max", "tx_power.avg",
	"temperature.min", "temperature.max", "temperature.avg",
	"firmware", "software",
	"loopback_mode");
}
