/*
 * comms_can_id.h
 *
 *  Created on: Sep 30, 2011
 *      Author: Yue Zhu
 */
//NOTE: This file is provided by Comms for synchronization with Comms protocol and program on SU

#ifndef COMMS_DEFS_H_
#define COMMS_DEFS_H_

//Data packet transfer related
#define CANID_PKT_STAT_REQ_UP	0xA2 //tx by LS
#define CANID_PKT_STAT_REQ_DN   0xA3 //rx by LS
#define CANID_PKT_STAT_UP		0xA6 //tx by LS
#define CANID_PKT_STAT_DN		0xA7 //rx by LS
#define CANID_PKT_IBSB_REQ_UP	0xAA //tx by LS
#define CANID_PKT_IBSB_REQ_DN	0xAB //rx by LS
#define CANID_PKT_IBSB_UP		0xAE //tx by LS
#define CANID_PKT_IBSB_DN		0xAF //rx by LS
#define CANID_PKT_START_UP		0xB2 //tx by LS
#define CANID_PKT_START_DN		0xB3 //rx by LS
#define CANID_PKT_CONT_UP		0xB6 //tx by LS
#define CANID_PKT_CONT_DN		0xB7 //rx by LS
#define CANID_PKT_STOP_UP		0xBA //tx by LS
#define CANID_PKT_STOP_DN		0xBB //rx by LS

//Miscellaneous
#define CANID_SU_STATUS_REQUEST		0x90 //tx by LS
#define CANID_SU_STATUS_RESPONSE	0x93 //rx by LS
#define CANID_LS_STATUS_REQUEST		0x91 //rx by LS
#define	CANID_LS_STATUS_RESPONSE	0x92 //tx by LS
#define CANID_LS_GPS_TIME_REQUEST	0x95 //rx by LS
#define CANID_LS_GPS_TIME_RESPONSE	0x96 //tx by LS
#define CANID_LS_GPS_POS_REQUEST	0x97 //rx by LS
#define CANID_LS_GPS_POS_RESPONSE	0x98 //tx by LS
#define CANID_SU_SHUTDOWN			0x94 //tx by LS
#define CANID_SU_RESET_REQUEST		0x9A //tx by LS
#define CANID_LS_RESET_REQUEST		0x99 //rx by LS

#define CANID_SU_RESET_RESPONSE		0x9B //rx by LS
#define CANID_LS_RESET_RESPONSE		0x9C //tx by LS
#define CANID_LS_FRAME_SYNC			0x9D //rx by LS
#define CANID_SU_CHANGELOOPBACKMODE_REQUEST  0x9E //tx by LS
#define CANID_SU_CHANGELOOPBACKMODE_RESPONSE 0x9F //rx by LS

#define OFS_TRANS_IN_PROG			0
#define MSK_TRANS_IN_PROG			(1<<OFS_TRANS_IN_PROG)
#define OFS_ERROR_BIT				1
#define MSK_ERROR_BIT				(1<<OFS_ERROR_BIT)
#define OFS_WARNING_BIT				2
#define MSK_WARNING_BIT				(1<<OFS_WARNING_BIT)
#define OFS_ERRORTYPE				4
#define MSK_ERRORTYPE				(0x0f<<OFS_ERRORTYPE)
#endif /* COMMS_DEFS_H_ */
