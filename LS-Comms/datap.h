/** @file datap.h
 *  @brief IF (interface) class for data packet in-out
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#ifndef DATAP_H
#define DATAP_H

/** @brief [IO functions] prepare an out-going test packet (or send it right away, implementation flavor) for "main"
 *
 *	With fixed typical length (540B), fixed service_id = 0, fixed destinatio
 *
 *	@param	mode		"variety" selector, define and implement as you wish e.g. for random/iterating data generation etc...
 */
void prepare_packet_send(int mode);

/** @brief [IO functions] interpret an incoming test packet for "main"
 *	@param	mode		"variety" selector, define and implement as you wish e.g. for random/iterating data generation etc...
 */
void congest_packet_recv(int mode);

/** @brief [handshake functions] is frame switched? detected by packets frame information for main()
 *	@return	int 	1 is signal, 0 no signal
 */
//int is_frame_end_soft();

/** @brief [handshake functions] is send sequence finished? signals that main() should cease sending
 *	@return	int 	1 is signal, 0 no signal
 */
int is_send_sequence_fin();

#endif
