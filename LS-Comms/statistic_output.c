/*
 * statistic_output.c
 *
 *  Created on: Jan 4, 2013
 *      Author: dev
 */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "timer.h"
#include "statistic_output.h"
#include "infobase.h"
#include "switch_log.h"

//wireless log variables
#define LINES_PER_LOG 10080 //about one week of 1min
static switch_logger Su_status_logger;

static int Interval = 60; //60 seconds as init value

static struct performanceTimer Statistic_timer;

char Filename[256];
char Filename_switch[256];

void init_statistic_output(char* path, int interval)
{
	sprintf(Filename, "%s/wireless_log0", path);
	sprintf(Filename_switch, "%s/wireless_log0s", path);

	init_switch_logger(&Su_status_logger, Filename, Filename_switch, NULL, LINES_PER_LOG);

	Interval = interval;

	reset_performanceTimer(&Statistic_timer, false, stdout);
}

void output_statistics() {
	if (Interval > 0) {
		time_t current_time;
		struct tm *local_time;
		char timestamp[18];
		int is_reopen;
		FILE* log_fd;

		if (snapshot_performanceTimer(&Statistic_timer, false, stdout) >= Interval*1000) { //plot something
			current_time = time(NULL);
			local_time = localtime(&current_time);
			strftime(&timestamp[0], 16,"%m.%d. %H:%M:%S", local_time);

			log_fd = getfile_switchlogger(&Su_status_logger, &is_reopen);
			if (log_fd != NULL) {
				if (is_reopen == 1) { //print title
					fprintf(log_fd, "time, ");
					print_station_stat_title(log_fd);
					print_rthq_infobase_title(log_fd);
					fprintf(log_fd, "\n");
				}
				fprintf(log_fd, "%s, ", timestamp);
				print_station_stat(log_fd, Interval);
				print_rthq_infobase(log_fd, Interval);
				fprintf(log_fd, "\n"); //end line

				registerLineWrite_switchlogger(&Su_status_logger, log_fd); //close file, register line
			} else {
				fprintf(stderr, "%s - cannot open file to write\n", __FUNCTION__);
			}

			reset_performanceTimer(&Statistic_timer, false, stdout);
		}
	}
}

