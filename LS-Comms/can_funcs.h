/*
 * can_funcs.h
 *
 *  Created on: Oct 6, 2011
 *      Author: Yue Zhu
 */

#ifndef CAN_FUNCS_H_
#define CAN_FUNCS_H_

#include "can4linux.h"

int can_init (char *device, int baud); //return device descriptor "int fd"
void can_exit(int fd);

int set_bitrate(int fd,			/* device descriptor */
	int baud		/* bit rate */
	);
//int set_lomode(int fd);

int can_start(int fd);
int can_reset(int fd);
void displayerror(int fd, canmsg_t *rx);

int show_status(int can_fd);

int is_send_secure(int can_fd);

int write_low_level(int can_fd, canmsg_t* tx);

#endif /* CAN_FUNCS_H_ */
