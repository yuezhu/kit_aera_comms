/** @file datap.c
 *  @brief IF (interface) class for data packet in-out, function implementation
 *
 *  @author Yue Zhu
 *  @bug No known bugs.
 */

#include <stdlib.h>
#include <unistd.h>
#include <string.h>


#include "datap.h"

#include "comms_tp.h"
#include "comms_tp_controller.h"
#include "./firmware/epcs.h"

#include "rthq_CONFIG.h"
#include "infobase.h"

#include "d_layer.h" /*function implementation for d_layer */

//for "frame-aware" sequence-send state variables
static unsigned char Next_service_id = 0;
static int Send_sequence_fin = 0; //handshake with main()

extern comms_tp_packet Tx_data; //tx pending buffer
extern comms_tp_packet Rx_data; //rx combination buffer
extern bool Send_pending;
extern bool Recv_pending;

extern int Loopback_test;

//extern int Pipefd[2]; //for frame sync

extern bool generate_firmware_packet(comms_tp_packet* data, uint8_t sector_base, char* file_name, bool isReconfig, unsigned char service, component_code destination);
extern void interpret_fw(comms_tp_packet* data);

char *Fwfile_name = NULL;
bool IsFwFileHW	= false;
bool IsFwFileFactory = false;
bool IsReconfig	= false;

bool Debug_comms_tp_2 = false;

void generate_packet(comms_tp_packet* data, unsigned char service, component_code destination, unsigned short length, bool ifRandom)
{
	data->comms_tp_packet_id_service = service;
	data->comms_tp_packet_id_destination = destination;
	data->comms_tp_packet_length = length;
	int i;
	if (ifRandom == true)
	{
		for (i=0; i<length; i++)
		{
			data->comms_tp_packet_data[i%MAX_COMMS_TP_PACKET_LENGTH] = (unsigned char)(rand()%256);
		}
	}
	else
	{
		for (i=0; i<length; i++)
		{
			data->comms_tp_packet_data[i%MAX_COMMS_TP_PACKET_LENGTH] = 255-(i%256);
		}
	}
	data->comms_tp_packet_crc =crc_cal(data);
}

void pack_packet(comms_tp_packet* data, unsigned char service, component_code destination, unsigned short length, unsigned char* p)
{
	data->comms_tp_packet_id_service = service;
	data->comms_tp_packet_id_destination = destination;
	data->comms_tp_packet_length = length;
	memcpy(data->comms_tp_packet_data, p, length);
	data->comms_tp_packet_crc =crc_cal(data);
}

void print_packet(comms_tp_packet* data)
{
	unsigned char id = data->comms_tp_packet_id_service;
	unsigned char code = data->comms_tp_packet_id_destination;
	unsigned short length = data->comms_tp_packet_length;
	int i;
	printf("id: %d\ndest: %d\ndata:\n", id, code);
	for (i=0; i<length; i++)
	{
		printf("ofs %d: %d, ", i, data->comms_tp_packet_data[i%MAX_COMMS_TP_PACKET_LENGTH]);
		if ((i%10) == 9) printf("\n");
	}
	printf("\ncrc: %lx\n", data->comms_tp_packet_crc);
}

void ReadFile(char *name)
{
	FILE *file;
	char *buffer;
	unsigned long fileLen;

	//Open file
	file = fopen(name, "rb");
	if (!file)
	{
		fprintf(stderr, "Unable to open file %s", name);
		return;
	}

	//Get file length
	fseek(file, 0, SEEK_END);
	fileLen=ftell(file);
	fseek(file, 0, SEEK_SET);

	//Allocate memory
	buffer=(char *)malloc(fileLen+1);
	if (!buffer)
	{
		fprintf(stderr, "Memory error!");
                                fclose(file);
		return;
	}

	//Read file contents into buffer
	fread(buffer, fileLen, 1, file);
	fclose(file);

	//Do what ever with buffer

	free(buffer);
}

void prepare_packet_send(int mode)
{
	if (mode == 1) { //test data gen
		component_code dest = (Loopback_test == 1)?LOCAL_GATEWAY:REMOTE_HOST;
		unsigned short ls_data_length = 540; //for test
		static int BW_limit_counter = 0;
		if (Send_pending == false) { //honor remote status (bandwidth, buffer...)
			if (BW_limit_counter == 8) {
				BW_limit_counter = 0;
				Send_sequence_fin = 1;
			}
			//fill
			generate_packet(&Tx_data, 0, dest, ls_data_length, true); //random
			if (Debug_comms_tp_2 == true) {
					printf("tx pkt gen, id: %d, dest: %d, length: %d, crc: %lx\n",
					Tx_data.comms_tp_packet_id_service, Tx_data.comms_tp_packet_id_destination, Tx_data.comms_tp_packet_length, Tx_data.comms_tp_packet_crc);
			}
			Send_pending = true;
			BW_limit_counter++;
		}
	}

	else if (mode == 2) { //firmware upload for SU
		if (d_get_send_rdy()) { //honor remote status (bandwidth, buffer...)
			//fill if packet to send
			bool ok = generate_firmware_packet(&Tx_data, IsFwFileFactory==false?(IsFwFileHW==true?HW2_OFS:SW2_OFS):(IsFwFileHW==true?HW1_OFS:SW1_OFS), Fwfile_name, IsReconfig, Next_service_id, LOCAL_GATEWAY);
			if (ok == true) {
				if (Debug_comms_tp_2 == true) {
					printf("tx pkt gen, id: %d, dest: %d, length: %d, crc: %lx\n",
							Tx_data.comms_tp_packet_id_service, Tx_data.comms_tp_packet_id_destination, Tx_data.comms_tp_packet_length, Tx_data.comms_tp_packet_crc);
				}
				Send_pending = true;
			}
			//important: put it here not direct after Send_pending, as the send_sequence will last too long,
			//if generate_firmware_packet() not ok (e.g. the fw-fsm halt 2 seconds)
			Next_service_id++;
		}
	}
	else { //other mode...
	}
}


void congest_packet_recv(int mode)
{
	if (Recv_pending == true) {
		Recv_pending = false;
		if (Debug_comms_tp_2 == true) {
			printf("rx pkt ok, id: %d, dest: %d, length: %d, crc: %lx\n", Rx_data.comms_tp_packet_id_service, Rx_data.comms_tp_packet_id_destination, Rx_data.comms_tp_packet_length, Rx_data.comms_tp_packet_crc);
		}
		if (mode == 1)  {
			/*
			    if (Send_pending == false) {
				    if (is_send_acceptable(Rx_data.comms_tp_packet_id_service) == 1) {
					    memcpy((void*)(&Tx_data), (void*)(&Rx_data), sizeof(comms_tp_packet)); //inefficient, but ok for test function...
					    if (Debug_comms_tp_2 == true) {
						    printf("rx pkt with id: %d looped back\n", Tx_data.comms_tp_packet_id_service);
					    }
					    Send_pending = true;
				    } else {
					    fprintf(stderr, "%s: \nCould *not* redirect packet as no slot opened for id %d\n", __FUNCTION__, Rx_data.comms_tp_packet_id_service);
				    }
			    } else {
				    fprintf(stderr, "%s: \nCould *not* redirect packet as still send pending, send costs longer than recv interval?\n", __FUNCTION__);
			    }
			*/
		}
		else if (mode == 2) {
			interpret_fw(&Rx_data);
		}
		else { //unknown mode
		}
	}
}

int is_send_sequence_fin()
{
	if (Send_sequence_fin == 1) {
		Send_sequence_fin = 0; //handshake reset
		return 1;
	}
	return 0;
}

#ifndef SIM
void init_d_layer() {}

//combined flow control
int d_get_send_rdy() //send as many as possible and honor status
{
	unsigned char service_id;
	int is_acceptable_id_found = 0;

	//check out-going send buffer readiness (if stall)
	if (Send_pending == true) { //still full (CAN-stream not finished yet)
		//printf("bl");
		return 0; //send sequence still in progress
	}

	/*
	//new code
	//check if new frame is started
	char buf[64];
	char c = 'f';
	int n = read(Pipefd[0], &buf, 64);
	if (n == 1) { //normal: no new frame
		write(Pipefd[1], &c, sizeof(char)); //compensate
	} else if (n >= 2) { //running into a new frame,
		if (Debug_comms_tp_2 == true) {
			printf("running into new frame, %d\n", n);
		}
		Next_service_id = 0; //reset state variable
		write(Pipefd[1], &c, sizeof(char)); //compensate
		return 0;
	} else { // n==0 EOF empty pipe? should not happen
		perror("d_get_send_rdy() call beyond scope\n");
	}
	*/

	// search next acceptable service id
	for (service_id = Next_service_id; service_id < 8; service_id++) {
		if (is_send_acceptable(service_id) == 1) {
			is_acceptable_id_found = 1;
			break;
		}
	}
	//if no acceptable id found --> no more packets can be sent in this frame
	if (is_acceptable_id_found == 0){
		//printf("nf");
		Next_service_id = 0; //reset state variable
		Send_sequence_fin = 1; //mark send sequence finish (CAN-stream should be finished, as Send_pending == false)
		return 0;
	}
	//otherwise save it to Next_service_id for the use of send_next()
	Next_service_id = service_id;
	return 1;
}

//return packets per second
int d_get_bandwidth()
{
	return (get_slot_opened());
}

void d_recv(post_t* post, uint8_t *p, unsigned short* p_id)
{
	if (p != NULL) {
		if (Recv_pending == true) {
			memcpy(p, Rx_data.comms_tp_packet_data, Rx_data.comms_tp_packet_length);
			/* for the symmetry, as the LS don't know about bs_id, this is just supposed to be 0 */
			*p_id = 0;
			if (Debug_comms_tp_2 == true) {
				printf("rx pkt ok, id: %d, dest: %d, length: %d, crc: %lx\n", Rx_data.comms_tp_packet_id_service, Rx_data.comms_tp_packet_id_destination, Rx_data.comms_tp_packet_length, Rx_data.comms_tp_packet_crc);
			}
			Recv_pending = false;

			D_layer_acc.D_recvok++;
			set_post(post, LAYER_D, MSG_NOTE, NOTE_RECV_OK);
			return;
		}
		set_post(post, LAYER_D, MSG_NOTE, NOTE_RECV_NULL);
		return;
	}
	set_post(post, LAYER_D, MSG_ERR, ERR_NULLPTR);
}

void d_send(post_t* post, uint8_t *p, unsigned short payload_length)  //TODO: speed limiting
{
	if (p != NULL) {
		//ensure flow control (actually redundant. If caller honors is_sendable() with flow control check,
		//should not be called)
		if ( (is_send_acceptable(Next_service_id) == 0) ||
			 (Send_pending == true) ) {
			fprintf(stderr, "%s: \nCould *not* send data packet! flow control malfunction\n", __FUNCTION__);
			D_layer_acc.D_sendfail++;
			set_post(post, LAYER_D, MSG_WARN, WARN_SENDBUFFBLOCKED); //lost! should be avoided
			return;
		}
		component_code dest = (Loopback_test == 1)?LOCAL_GATEWAY:REMOTE_HOST;
	    //fill send pending buffer
		pack_packet(&Tx_data, Next_service_id, dest, payload_length, p);

		//mark pending, so that can controller will segment it and send it "slowly"
		Send_pending = true;
		//increase next_id, the next id should be checked!
		Next_service_id ++;

		//debug output
		if (Debug_comms_tp_2 == true) {
			printf("tx pkt pack, id: %d, dest: %d, length: %d, crc1: %lx\n",
					Tx_data.comms_tp_packet_id_service, Tx_data.comms_tp_packet_id_destination, Tx_data.comms_tp_packet_length, Tx_data.comms_tp_packet_crc);
		}

		Station_stat.throughput_acc += payload_length;

		set_post(post, LAYER_D, MSG_NOTE, NOTE_SEND_OK);
		return; //successful
	}
	set_post(post, LAYER_D, MSG_ERR, ERR_NULLPTR);
}
#endif
