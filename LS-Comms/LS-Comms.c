/* system */
//#include "comms_global.h"
#include <unistd.h>
#include <getopt.h>
#include <poll.h>

/* Shared */
#include "crc32.h"
#include "timer.h"
#include "syslogger.h" //using syslogger should also include <syslog.h>
#include "serversock.h"
#include "linux_process_ctrl.h"
#include "comms_signal.h"
#include "amsg_c.h"
#include "sniffer.h" //for ip packet printout

/* rthq */
#include "rthq_CONFIG.h"
#include "port_settings.h"
#include "t_layer.h"
#include "post.h"
#ifdef SIM
	#include "d_layer.h"
#endif

/* this project */
#include "can_funcs.h"
#include "datap.h"
#include "comms_tp_controller.h"
#include "statistic_output.h"
#include "id.h"

//1 Mbps
#define CAN_baud 1000

//shared between tp_fsm and tp
extern unsigned char N_txmsg_per_fcall;  //default to 1 CAN msg per can_write_packet

//defaults to no debug info
extern bool Debug_comms_tp_4;
extern bool Debug_comms_tp_3;
extern bool Debug_comms_tp_2;
bool Debug_comms_tp_1 = false;
bool Debug_comms_tp_0 = false;

int Loopback_test = 0;

extern char *Fwfile_name;
extern bool IsFwFileHW;
extern bool IsFwFileFactory;
extern bool IsReconfig;

int Fd;
int Pipefd[2];
static struct connInfo ConnInfoArray[3] = {{"RT_CONN", PORT_NUMBER_RT, 0, 0, 0, SERVER_DOWN},
									  	   {"HQ_CONN", PORT_NUMBER_HQ, 0, 0, 0, SERVER_DOWN},
									  	   {"IP_CONN", PORT_NUMBER_IP, 0, 0, 0, SERVER_DOWN}};

extern int Send_waitresp_timeout_msec;
extern int Num_first_n_sync_msg_ignored;

struct performanceTimer PTimer;
struct performanceTimer LatencyTimer;
static struct performanceTimer Program_timer;

static char* Version = "3.3";
static char* New_features = "1. Release offered with 03 2. fixed an ip bridge bug, and ip bridge support addr filter 3. minor: interval==0 means no output 4.Sorted RT receive, ON via -S! 5. finished IP bridge, -D 1-2 for debug!";
static char* Main_todos = "STACKSIZE: default 4K, 1. CAN problem is solved. 2. Statistic should be ok!, now test";

static void usage(char *s)
{
	static char *usage_text  = "\
    -m x [m]ode: x=1: random data test mode; x=2: firmware upload by given -f filename (-H,-F,-R); x=3: ethernet over rthq (-l for loopback, no -l for bridge); default is 3\n\
	-p x poll() timeout in millisecond, default is 250ms\n\
	-d [d]eveloper version using /home/nfs as data directory, default: /log \n\
	-n x (1~16) send x tx messages at a time, default 4\n\
	-t x [t]imeout in ms of the transmitter to wait for response, default is 100\n\
	-D x [D]ebugging level 0-4, default -1 (no debug info), 1-basic info, 2-packet level, 3-fsm level, 4-candrv level\n\
	-i x x can sync msgs will be ignored for frame synchronization at the program start, default is 100\n\
	-I [I]nterval in seconds to output to statistics, default is 0, 0 means no output\n\
	-L set [L]oopback test\n\
	-C activate flow [C]ontrol, default off: 2k buffer for RT, 32k buffer for HQ at default, if full, no data-pull out on socket\n\
	------------SU firmware upload related------------\n\
	-f [f]ilename, binary file of firmware for update over CAN 4\n\
	-H the firmware file is a [H]ardware image, if not used --> software image\n\
	-F the firmware file is a [F]actory image, DANGEROUS! if factory image is corrupt, a field reflash is needed\n\
    -R [R]econfig after firmware is transfered, default off\n\
	-S switch ON sorted RT recv, default: off\n\
	-k [k]ill existing processes\n\
	";
	//old arguments:
	//-S sleep 1ms after each write() call, default disabled
	//-s sleep 1ms after each write() error call, default enabled
	//-w x send packet id from 0-(x-1) x<=8 comms packets

	printf("*********************************\n");
	printf("*KIT_comms application, ver %s *\n", Version);
	printf("*********************************\n");
	printf("added features: %s\n", New_features);
	printf("Main TODOs: %s\n", Main_todos);
	printf("usage: %s [options]\n", s);
    printf("%s", usage_text);
}


void close_socket_connections()
{
    teardownConnection(&ConnInfoArray[0]);
    teardownConnection(&ConnInfoArray[1]);
    teardownConnection(&ConnInfoArray[2]);
}

void exit_program()
{
	long diff = snapshot_performanceTimer(&Program_timer, true, stdout); printf("Program exit, runtime: %ld seconds\n", diff);
	close_socket_connections();
	can_exit(Fd);
	exit(EXIT_SUCCESS);
}

void open_socket_connections()
{
	int turns;
	int i;
	close_socket_connections();
	for (i = 0; i<3; i++) {
	    turns = 0;
	    while (initConnection(&ConnInfoArray[i]) < 0) { //try open 0:RT 1:HQ 2:IP
	    	usleep(50000);
	    	if (turns >= 3) {
	    		exit_program();
	    	}
	    	turns++;
	    }
	}
}

void test_send(Qos_t qos, uint16_t length)
{
	post_t ap;
	uint8_t* amsg_c_t;
	staId_t recverId;
	objwrapper_t *rc_amsg_c;

	recverId = get_myId();
	amsg_c_t = create_test_amsg_c(length, (qos==Qos_RT)); //about 300Hz
	if (amsg_c_t != NULL) {
		rc_amsg_c	= objwrapper_create(amsg_c_t, free); //wrap the data field with a reference count "rc_"
		if (rc_amsg_c != NULL) {
			add_send(&ap, rc_amsg_c, qos, recverId); //rc_amsg_c taken over by add_send
			if (ap.msgType == MSG_WARN || ap.msgType == MSG_ERR) {
				snapshot_performanceTimer(&PTimer, true, stderr);
				fprintf(stderr, "<add_send>:");
				print_post(&ap, stderr);
			}
			objwrapper_release(rc_amsg_c); //release only frees the buffer if add_send fails (ref_cnt=1)
		}
		else {
			snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "MEM_ERR!%s - comms_int send data objwrapper malloc fail!\n", __FUNCTION__);
			free(amsg_c_t);
		}
	} else {
		snapshot_performanceTimer(&PTimer, true, stderr); fprintf(stderr, "MEM_ERR!%s - comms_int send data malloc fail!\n", __FUNCTION__);
	}
}

int main(int argc, char **argv)
{
	extern char *optarg;
	extern int optind;

	int developer_version = 0;  // flag = 1: use /home/nfs instead of /log as data directory
	int c;
	int interval = 0; //0 means no output
	int debug_level = -1; //default -1
	int mode = 3;
	bool flowCtrl = false;
	bool sortedRTRecv = false; //default: false
	int to_poll_msec = TO_POLL_MSEC; //milliseconds for main poll() timeout

    chksum_crc32gentab();
    srand ( time(NULL) );

	//parse command line
	/* strip the path from app-name to "pname" */
    char *pch = strtok(argv[0], "./~");
    char *pname = pch;
    while (pch != NULL) {
    	pname = pch;
    	pch = strtok(NULL, "./~"); //continues
    }

    int pid_list[10];
    int pid_list_len = 10;
    while ((c = getopt(argc, argv, "m:p:dn:t:D:i:I:LCf:HFRSkh")) != EOF)
    {
		switch (c)
		{
			case 'm':
				mode = atoi(optarg);
				break;
			case 'p':
				to_poll_msec = atoi(optarg);
				break;
			case 'd': // developer version
				developer_version = 1;
				break;
			case 'n':
				N_txmsg_per_fcall = (unsigned char)atoi(optarg);
				break;
			case 't':
				Send_waitresp_timeout_msec = (unsigned char)atoi(optarg);
				break;
			case 'D':
				debug_level = atoi(optarg);
				break;
			case 'i':
				Num_first_n_sync_msg_ignored = atoi(optarg);
				break;
			case 'I':
				interval = atoi(optarg);
				break;
			case 'L':
				Loopback_test = 1;
				break;
			case 'C':
				flowCtrl = true;
				break;
			case 'f':
				Fwfile_name	= optarg;
				break;
			case 'H':
				IsFwFileHW	= true;
				break;
			case 'F':
				IsFwFileFactory	= true;
				break;
			case 'R':
				IsReconfig	= true;
				break;
			case 'S':
				sortedRTRecv = true; //different as AERA-Comms
				break;
			case 'k':
				if (getProcessID(pname, pid_list, &pid_list_len) >= 0) {
					if (pid_list_len > 0) {
						printf("%d existing processes found\n", pid_list_len-1);
						killProcessID(pid_list, pid_list_len-1);
					}
				}
				exit(EXIT_SUCCESS);
				break;
			case 'h':
			default: usage(pname); exit(0);
		}
    }

    //set debug level
	Debug_comms_tp_4 = debug_level>=4?true:false;
	Debug_comms_tp_3 = debug_level>=3?true:false;
	Debug_comms_tp_2 = debug_level>=2?true:false;
	Debug_comms_tp_1 = debug_level>=1?true:false;
	Debug_comms_tp_0 = debug_level>=0?true:false;

	//set IP debug
	if (Debug_comms_tp_2 == true) setfullinfo();

    reset_performanceTimer(&Program_timer, false, stdout);
    reset_performanceTimer(&PTimer, true, stdout);

	fflush(stdout);

	Fd = can_init ( "/dev/can0", CAN_baud );

    if (pipe(Pipefd) == -1) { //pipefd[0]: read; pipefd[1]: write
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    initConnInfo(&ConnInfoArray[0], "RT_CONN", PORT_NUMBER_RT);
    initConnInfo(&ConnInfoArray[1], "HQ_CONN", PORT_NUMBER_HQ);
    initConnInfo(&ConnInfoArray[2], "IP_CONN", PORT_NUMBER_IP);

	//MuxIO by polling begin
	struct pollfd pfds[5];
	pfds[0].fd = Fd;	// event 0: CAN read
	pfds[0].events = POLLIN;
	pfds[1].fd = Pipefd[0]; // event 1: Frame
	pfds[1].events = POLLIN;
	int poll_count = 2; //default
	//MuxIO by polling end

	/********************************************/
	/* Initialization							*/
	/********************************************/
	char path[80];

    chksum_crc32gentab();
    time_t seed = time(NULL);
    srand (1345138369);
    fprintf(stdout, "random seed: %u,\n"
    		"poll timeout? %d msec,\n"
    		"is development? %d,\n"
    		"n_canmsg_per_write(): %d,\n"
    		"canmsg send stream timeout: %d,\n"
    		"debug_level: %d,\n"
    		"number of start-sync-canmsg to ignore: %d,\n"
    		"stat interval %d,\n"
    		"is loopback test? %d,\n"
    		"is flow control activated? %d\n"
    		"is RT recv sorted? %d\n", (unsigned int)seed, to_poll_msec, developer_version, N_txmsg_per_fcall,
    		Send_waitresp_timeout_msec, debug_level, Num_first_n_sync_msg_ignored, interval, Loopback_test, flowCtrl, sortedRTRecv);

    init_t_layer();
    if (sortedRTRecv) set_sorted_rt_recv();
    init_comms_tp_controller();

	if (developer_version == 0) {
		strcpy(&path[0], "/log");
	} else {
		strcpy(&path[0], "/home/nfs");
	}

	//init_syslogger(path); //no need for syslogger
	init_statistic_output(path, interval);

	int ind;

#ifdef SIM
	    log_fd = fopen("./svr_log", "a");
	    if (log_fd == NULL) {
	    	fprintf(stderr, "%s - cannot append ./svr_log to write\n", __FUNCTION__);
	    	exit(EXIT_FAILURE);
	    }
/*
//PER simulation
		float error_rate = 0.10;
		init_testChannel(error_rate, 0.0, 0.0);
	    fprintf(log_fd, "**** sim channel error rate: %1.6f ****\n", error_rate);
	    fclose(log_fd);
	    printf("**** sim channel error rate: %1.6f ****\n", error_rate);
*/

		float error_rate = 0.001;
		init_testChannel(error_rate, 0.0, 0.0);

/*
//Frame number simulation
		uint32_t Nf = 4;
		uint32_t frame_delay = 1000/Nf;
		set_frame_delay_msec(frame_delay);
		fprintf(log_fd, "**** sim Nf: %u, frame_delay: %u ****\n", Nf, frame_delay);
		fclose(log_fd);
		printf("**** sim Nf: %u, frame_delay: %u ****\n", Nf, frame_delay);
*/
		uint32_t Nf = 3;
		uint32_t frame_delay = 1000/Nf;
		set_frame_delay_msec(frame_delay);

//slot_open simulation
		uint8_t slot_opened = 8;
		set_slot_opened(slot_opened);
		fprintf(log_fd, "**** sim slot: %d ****\n", slot_opened);
		fclose(log_fd);
		printf("**** sim slot: %d ****\n", slot_opened);


		poll_count = 0; //disable poll
		struct performanceTimer BWtimer, T2SendTimer, T3SendTimer;
		reset_performanceTimer(&BWtimer, false, stdout);
		reset_performanceTimer(&LatencyTimer, false, stdout);
		reset_performanceTimer(&T2SendTimer, false, stdout);
		reset_performanceTimer(&T3SendTimer, false, stdout);
		int T2length, Thruput_RT, Thruput_HQ;
		int T2_rate, T3_rate;
		int T2_recv_cnt = 0;
		int T3_recv_cnt = 0;
		int T2_send_cnt_tot = 0;
		int T3_send_cnt_tot = 0;
		int T2_recv_cnt_tot = 0;
		int T3_recv_cnt_tot = 0;

		uint64_t T2_recv_latency_acc = 0;
		uint64_t T3_recv_latency_acc = 0;
		uint64_t T2_recv_latency_pow2_acc = 0;
		uint64_t T3_recv_latency_pow2_acc = 0;

		bool send_ena = true;
		bool recv_ok;
		int min_count = 0;
		uint32_t latency;
#endif

	while (1)
	{
#ifndef SIM
		poll(pfds, poll_count, to_poll_msec); //so that the high level timer is 250ms granularity
		if (pfds[1].revents & POLLIN) //io poll frame
		{
    		if (mode < 3) {
				//prepare data to be sent
				prepare_packet_send(mode);
			}
			else {
				post_t sp;
				poll_send(&sp);
		    	if (sp.msgType == MSG_WARN || sp.msgType == MSG_ERR) {
		    		snapshot_performanceTimer(&PTimer, true, stderr);
		    		fprintf(stderr, "<poll_send>:");
	        		print_post(&sp, stderr);
		    	}
			}

	    	can_send(Fd);
			if (is_send_sequence_fin()) { //in datap.c by d_get_send_rdy()
			    char buf[64];
				read(Pipefd[0], &buf, 64); //flush
			}
		}
		if (pfds[0].revents & POLLIN) //io poll can receive
		{
			can_recv(Fd);

			if (mode < 3) {
				//congest recved data
				congest_packet_recv(mode);
			}
			else { //standard amsg
				post_t rp; //recv post
				objwrapper_t* rc_amsg_c_r = NULL; //reference-counted_amsg _container_receive
				uint8_t tId_r;
				staId_t senderId_r;
				Qos_t	qos_r;
				poll_recv(&rp, &rc_amsg_c_r, &tId_r, &qos_r, &senderId_r);
				if (rp.msgType == MSG_WARN || rp.msgType == MSG_ERR) {
					snapshot_performanceTimer(&PTimer, true, stderr);
					fprintf(stderr, "<poll_recv>:");
					print_post(&rp, stderr);
				}
				if (rp.layer == LAYER_T && rp.msgType == MSG_NOTE && rp.msg == NOTE_RECV_OK) {
					uint8_t* amsg_c_r = objwrapper_get_object(rc_amsg_c_r);
					if (amsg_c_r != NULL) {
						uint8_t ind = (qos_r==Qos_RT)?0:1;
						writeToServerConnection(&ConnInfoArray[ind], rc_amsg_c_r); //write to the corresponding connInfo
					}
					objwrapper_release(rc_amsg_c_r); //release, should destroy
				}
				if (rp.layer == LAYER_T && rp.msgType == MSG_NOTE && rp.msg == NOTE_RECV_IP) {
					//already declared in t_layer.h
					//extern uint8_t Ip_bridge_rxbuf[1024];
					//extern uint16_t Ip_bridge_rxbuf_len;
					if (Debug_comms_tp_1 == true) {
						printf("\n*****got a IP packet from wireless*****\n");
						ProcessPacket((unsigned char*)Ip_bridge_rxbuf, Ip_bridge_rxbuf_len);
					}
					//forward
					sendSimpleSockServer(&ConnInfoArray[2],Ip_bridge_rxbuf, Ip_bridge_rxbuf_len);
				}
			}
		}
		for (ind = 0; ind <2; ind++) {
			if (pfds[ind+2].revents & POLLIN) {//io poll connection accept or receive
				Qos_t qos_send = (ind == 0)?Qos_RT:Qos_HQ;
				bool isCongestable = true;
				unsigned long tLenTotal_RT, tLenTotal_HQ; //for print
				int num_RT, num_HQ; //for print
				if (flowCtrl == true) {
					isCongestable = check_congestion_ctrl(qos_send, &tLenTotal_RT, &tLenTotal_HQ, &num_RT, &num_HQ);
				}
				if (isCongestable == true) {
					if (ConnInfoArray[ind].actionOnEvent != NULL) { // do the corresponding action (read or accept)
						ConnInfoArray[ind].actionOnEvent(&ConnInfoArray[ind], NULL);
					}
					if (ConnInfoArray[ind].arg != NULL) { //if it is read, the arg should point to the amsg_c
						objwrapper_t* rc_amsg_c_toforward = (objwrapper_t*)(ConnInfoArray[ind].arg); //the amsg_c is realloced to be able to fill CRC in add_send()!


						post_t ap;
						staId_t recverId = (Loopback_test == 1)? get_myId() : SBC_ID; //if loopback test?
						add_send(&ap, rc_amsg_c_toforward, qos_send, recverId); //rc_amsg_c taken over by add_send
						if (ap.msgType == MSG_WARN || ap.msgType == MSG_ERR) {
							snapshot_performanceTimer(&PTimer, true, stderr);
							fprintf(stderr, "<add_send>:");
							print_post(&ap, stderr);
						}

						objwrapper_release(rc_amsg_c_toforward); //release only frees the buffer if add_send fails (ref_cnt=1)
						ConnInfoArray[ind].arg = NULL;
					}

					//update the current socket! since it can change to accept or receive refering to different socket value (updated in socketCurrent)
					//NOTE! if the socketCurrent is changed to 0 by handler (SERVER_DOWN) then the polling will not visit here anymore -->needa main loop function
					pfds[ind+2].fd = ConnInfoArray[ind].socketCurrent;
				} else { //not congestable --> do nothing --> should stall at the other end of socket
					snapshot_performanceTimer(&PTimer, true, stdout);
					fprintf(stdout, "stall, %s-buf full: %d transfers of tot %lu bytes present\n", (qos_send==Qos_RT)?"RT":"HQ",
							(qos_send==Qos_RT)?num_RT:num_HQ,
							(qos_send==Qos_RT)?tLenTotal_RT:tLenTotal_HQ);
				}
			}
		}
		if (pfds[4].revents & POLLIN) {
			if (Ip_bridge_txbuf_pending == 0) { //no pending of IP bridge tx buffer
				if (ConnInfoArray[2].actionOnEvent != NULL) { // do the corresponding action (read or accept)
					if (ConnInfoArray[2].state == SERVER_UP_ACCEPTED) { //special!!! use simpleSock read explicitly instead of "actionOnEvent"-read from AERA "socklib"
						#include "tdatagram.h" //for IP_HDRBLEN, but unsauber!, so that Ip_bridge_txbuf can be used straitforward to avoid one memcpy. we sacrifice "code" for performance
						Ip_bridge_txbuf_len = 1024 - IP_HDRBLEN; //give the max length for check
						if (recvSimpleSockServer(&ConnInfoArray[2], &Ip_bridge_txbuf[IP_HDRBLEN], &Ip_bridge_txbuf_len) >= 0) { //ok
							unsigned char ipv4[4];
							get_ipv4_daddr_in_arr(&Ip_bridge_txbuf[IP_HDRBLEN], ipv4);
							Ip_bridge_txbuf_recvId = ipv4[3];
							Ip_bridge_txbuf_pending = 1; //handshake with t_layer
							if (Debug_comms_tp_1 == true) {
								printf("\n*****got a IP packet from local for %d.%d.%d.%d*****\n", ipv4[0], ipv4[1], ipv4[2], ipv4[3]);
								ProcessPacketOut((unsigned char*)(&Ip_bridge_txbuf[IP_HDRBLEN]), Ip_bridge_txbuf_len);
							}
						}
					} else { //maybe accept
						ConnInfoArray[ind].actionOnEvent(&ConnInfoArray[ind], NULL);
					}
				}

				//update the current socket! since it can change to accept or receive refering to different socket value (updated in socketCurrent)
				//NOTE! if the socketCurrent is changed to 0 by handler (SERVER_DOWN) then the polling will not visit here anymore -->needa main loop function
				pfds[4].fd = ConnInfoArray[2].socketCurrent;
			} else {
				snapshot_performanceTimer(&PTimer, true, stdout);
				fprintf(stdout, "stall, ip internal buffer full, application send too quickly?\n");
			}
		}

#else
		if (send_ena) {
/*
			if (check_congestion_ctrl(Qos_RT)) {
				T2length = 267+rand()%800;
				test_send(Qos_RT, T2length); //267~1067 bytes, simulating T2 (200Hz to 800 Hz, 1/3 second periodic amsg send)
				T2_send_cnt_tot++;
				Thruput_RT += T2length;
			}
*/

			//if (snapshot_performanceTimer(&T3SendTimer, false, stdout) >= 10*1000) { //10 seconds 1 T3, 0,1Hz> 0.087Hz
				if (check_congestion_ctrl(Qos_HQ)) {
					test_send(Qos_HQ, 6000); //6 kbytes, simulating T3
					T3_send_cnt_tot++;
					Thruput_HQ += 6000;
				}
				//reset_performanceTimer(&T3SendTimer, false, stdout);
			//}

		}

		post_t sp;
		poll_send(&sp);
		if (sp.msgType == MSG_WARN || sp.msgType == MSG_ERR) {
			snapshot_performanceTimer(&PTimer, true, stderr);
			fprintf(stderr, "<poll_send>:");
			print_post(&sp, stderr);
		}

		post_t rp; //recv post
	    objwrapper_t* rc_amsg_c_r = NULL; //reference-counted_amsg _container_receive
	    uint8_t tId_r;
	    staId_t senderId_r;
	    Qos_t	qos_r;
		poll_recv(&rp, &rc_amsg_c_r, &tId_r, &qos_r, &senderId_r);
    	if (rp.msgType == MSG_WARN || rp.msgType == MSG_ERR) {
    		snapshot_performanceTimer(&PTimer, true, stderr);
    		fprintf(stderr, "<poll_recv>:");
    		print_post(&rp, stderr);
    	}
		if (rp.layer == LAYER_T && rp.msgType == MSG_NOTE && rp.msg == NOTE_RECV_OK) {
			uint8_t* amsg_c_r = objwrapper_get_object(rc_amsg_c_r);
			if (amsg_c_r != NULL) {
				recv_ok = autocheck_testdata(amsg_c_r, (qos_r==Qos_RT), &latency, true);
				if (recv_ok == true) {
					if (qos_r == Qos_RT) {
						T2_recv_cnt++;
						T2_recv_latency_acc += latency;
						T2_recv_latency_pow2_acc += latency*latency;
					} else {
						T3_recv_cnt++;
						T3_recv_latency_acc += latency;
						T3_recv_latency_pow2_acc += latency*latency;
					}
				}
			}
			objwrapper_release(rc_amsg_c_r); //release, should destroy
		}

		poll_checkTimeoutFrameTimer();

		if (snapshot_performanceTimer(&BWtimer, false, stdout) >= 1*60*1000) { //1 minute
			T2_rate = Thruput_RT/60/4; //Hz
			T3_rate = Thruput_HQ/60/6; //mHz

		    log_fd = fopen("./svr_log", "a");
		    if (log_fd == NULL) {
		    	fprintf(stderr, "%s - cannot append ./svr_log to write\n", __FUNCTION__);
		    	exit(EXIT_FAILURE);
		    }
		    snapshot_performanceTimer(&PTimer, true, log_fd); fprintf(log_fd, "%d, %d, %d, %llu, %llu, %d, %llu, %llu\n", T2_rate, T3_rate, T2_recv_cnt,
		    		T2_recv_latency_acc, T2_recv_latency_pow2_acc, T3_recv_cnt, T3_recv_latency_acc, T3_recv_latency_pow2_acc);
		    fclose(log_fd);

		    snapshot_performanceTimer(&PTimer, true, stdout); printf("%d, %d, %d, %llu, %llu, %d, %llu, %llu\n", T2_rate, T3_rate, T2_recv_cnt,
		    		T2_recv_latency_acc, T2_recv_latency_pow2_acc, T3_recv_cnt, T3_recv_latency_acc, T3_recv_latency_pow2_acc);

		    T2_recv_cnt_tot += T2_recv_cnt;
		    T3_recv_cnt_tot += T3_recv_cnt;

		    T2_recv_cnt = 0;
		    T2_recv_latency_acc = 0;
		    T2_recv_latency_pow2_acc = 0;
		    T3_recv_cnt = 0;
		    T3_recv_latency_acc = 0;
		    T3_recv_latency_pow2_acc = 0;

		    Thruput_RT = 0;
			Thruput_HQ = 0;
			reset_performanceTimer(&BWtimer, false, stdout);

			min_count ++;
			if (min_count >= 11) {
				send_ena = false;
			}
			if (min_count == 12) {
			    log_fd = fopen("./svr_log", "a");
			    if (log_fd == NULL) {
			    	fprintf(stderr, "%s - cannot append ./svr_log to write\n", __FUNCTION__);
			    	exit(EXIT_FAILURE);
			    }
			    fprintf(log_fd, "***END***");
			    snapshot_performanceTimer(&PTimer, true, log_fd); fprintf(log_fd, "%d, %d, %d, %d\n", T2_send_cnt_tot, T2_recv_cnt_tot, T3_send_cnt_tot, T3_recv_cnt_tot);
			    fclose(log_fd);

			    printf("***END***");
			    snapshot_performanceTimer(&PTimer, true, stdout); printf("%d, %d, %d, %d\n", T2_send_cnt_tot, T2_recv_cnt_tot, T3_send_cnt_tot, T3_recv_cnt_tot);

/*
//PER simulation
			    if (error_rate > 0.0001) {
			    	error_rate = error_rate/10;
					init_testChannel(error_rate, 0.0, 0.0);
*/
/*
//Frame number simulation
			    if (Nf > 1) {
			    	Nf--;
			    	frame_delay = 1000/Nf;
			    	set_frame_delay_msec(frame_delay);
*/
			    if (slot_opened > 1) {
			    	slot_opened --;
			    	set_slot_opened(slot_opened);

					log_fd = fopen("./svr_log", "a");
					if (log_fd == NULL) {
						fprintf(stderr, "%s - cannot append ./svr_log to write\n", __FUNCTION__);
						exit(EXIT_FAILURE);
					}
/*
//PER simulation
					fprintf(log_fd, "**** sim channel error rate: %1.6f ****\n", error_rate);
					fclose(log_fd);
					printf("**** sim channel error rate: %1.6f ****\n", error_rate);
*/
/*
//Frame number simulation
					fprintf(log_fd, "**** sim Nf: %u, frame_delay: %u ****\n", Nf, frame_delay);
					fclose(log_fd);
					printf("**** sim Nf: %u, frame_delay: %u ****\n", Nf, frame_delay);
*/
					fprintf(log_fd, "**** sim slot: %d ****\n", slot_opened);
					fclose(log_fd);
					printf("**** sim slot: %d ****\n", slot_opened);

					T2_send_cnt_tot = 0;
					T3_send_cnt_tot = 0;
					T2_recv_cnt_tot = 0;
					T3_recv_cnt_tot = 0;
					min_count = 0;
					send_ena = true;
			    }
			    else {
				    exit(EXIT_SUCCESS);
			    }

			}
		}
#endif

	    /* poll timeout timer! */
		poll_checkTimeout();

		/* export the infobase, timing controlled within the function*/
		output_statistics();


		/* delayed forward for RT recv, not within the select-ifs, but happened after timeout*/
		if (get_sorted_rt_recv() == true) {
			objwrapper_t* rc_amsg_c_arr[SORT_DEPTH];
			int num = 0;
			bool ready = get_rt_forward_ready(rc_amsg_c_arr, &num);
			while(ready) { //forward all the "ready" and "sorted" transfers from certain send
				int i;
				for(i=0; i<num; i++) { //forward all the "ready" and "sorted" transfers from certain sender
					uint8_t* amsg_c_r = objwrapper_get_object(rc_amsg_c_arr[i]);
					if (amsg_c_r != NULL) {
						writeToServerConnection(&ConnInfoArray[0], rc_amsg_c_arr[i]); //write to RT
					}
					objwrapper_release(rc_amsg_c_arr[i]); //release, should destroy
				}
				ready = get_rt_forward_ready(rc_amsg_c_arr, &num);
			}
		}

		/* port connection */
		int ret = 0;
		ret = poll_comms_ready();
		if (ret == 1) {
			open_socket_connections(); //if comms becomes ready (periodic sync message)

			pfds[2].fd = ConnInfoArray[0].socketCurrent; // event 2: RT connection read or accept
			pfds[2].events = POLLIN;
			pfds[3].fd = ConnInfoArray[1].socketCurrent; // event 3: HQ connection read or accept
			pfds[3].events = POLLIN;
			pfds[4].fd = ConnInfoArray[2].socketCurrent; // event 4: IP connection read or accept
			pfds[4].events = POLLIN;
			poll_count = 5;

			if (Loopback_test == 1) { //loopback
#include "comms_tp.h"
				printf("...CHANGED the local gate way to loopback_local MODE...!\n");
				comms_tp_event tx_event;
				tx_event.isTransEvent = true;
				tx_event.eventtype = SU_CHANGELOOPBACKMODE_REQUEST;
				tx_event.mode = 2; //(local loopback)
				can_write_message(Fd, &tx_event);
				/***** test end ****/
			}
		} else if (ret == 2) {
			close_socket_connections(); //if comms becomes not available (power loss, no sync message)
			poll_count = 2;
		}
	}
	can_exit(Fd);
	exit(EXIT_SUCCESS);
}
