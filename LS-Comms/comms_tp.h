/*
 * comms_tp.h
 *
 *  Created on: Sep 30, 2011
 *      Author: Yue Zhu
 */

#ifndef COMMS_TP_H_
#define COMMS_TP_H_

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#define MAX_COMMS_TP_PACKET_LENGTH 1024

typedef enum {REMOTE_HOST = 0, LOCAL_HOST = 1, REMOTE_GATEWAY = 2, LOCAL_GATEWAY = 3} component_code;

typedef struct {
        unsigned char	comms_tp_packet_id_service;         //3 bit
        component_code	comms_tp_packet_id_destination;		//2 bit
        unsigned short	comms_tp_packet_length;     //16 bit - 2 byte
        unsigned char	comms_tp_packet_data[MAX_COMMS_TP_PACKET_LENGTH];
        unsigned long	comms_tp_packet_crc;
} comms_tp_packet;

typedef enum {START = 0, CONTINUE = 1, STOP = 2, RESPONSE = 3} comms_tp_tx_state;
typedef	enum {IDLE = 0, STARTED = 1, STOPPED = 2} comms_tp_rx_state;

//error type is 4 bits, maximum value = 15
typedef enum {SUCCESSFUL = 0,  //LS, SU
			LEN_INVALID = 1,   //LS, SU
			LEN_UNMATCHED = 2, //LS, SU
			CRC_ERROR = 3, 	   //LS, SU
			TRANS_FORBID = 4,  //LS, SU
			CONT_INVALID = 5,  //LS, SU
			STOP_INVALID = 6,  //LS, SU
			FRAME_ERROR = 7,   //Only SU as receiver can fire this error
			TIME_OUT = 8,      //LS, SU
			START_INVALID = 9, //LS, SU
			FRAME_WARNING = 10 //Only SU as receiver can fire this error
} comms_tp_errortype;

typedef struct {
		bool isTransErr; //if the ongoing rx transfer is erroneous
		comms_tp_errortype errortype; //type of error
} comms_tp_rx_error;

//error type is 4 bits, maximum value = 15
typedef enum {NO_EVENT,
			TRANS_STARTED,
			TRANS_STOPPED,
			LS_TRANS_STAT_REQUEST,
			SU_TRANS_STAT_RESPONSE,
			LS_IBSB_REQUEST, //Input Buffer Status Byte
			SU_IBSB_RESPONSE,
			SU_STATUS_RESPONSE,
			LS_STATUS_REQUEST,
			LS_GPS_TIME_REQUEST,
			LS_GPS_POS_REQUEST,
			LS_RESET_REQUEST,
			SU_RESET_RESPONSE,

			SU_TRANS_STAT_REQUEST,
			LS_TRANS_STAT_RESPONSE,
			SU_IBSB_REQUEST, //Input Buffer Status Byte
			LS_IBSB_RESPONSE,
			LS_STATUS_RESPONSE,
			SU_STATUS_REQUEST,
			LS_GPS_TIME_RESPONSE,
			LS_GPS_POS_RESPONSE,
			SU_RESET_REQUEST,
			LS_RESET_RESPONSE,

			SU_SHUTDOWN,
			LS_FRAME_SYNC,
            SU_CHANGELOOPBACKMODE_REQUEST,
            SU_CHANGELOOPBACKMODE_RESPONSE
} comms_tp_eventtype;

typedef struct {
	bool trans_in_prog_bit;
	bool error_bit;
	bool warning_bit;
	comms_tp_errortype errortype;
} tsb_struct;

typedef struct {
	bool	buf_normpkt_full[4];
	unsigned char buf_extpkt_left;
} ibsb_struct;

typedef struct {
	bool	slot_normpkt_open[4];
	unsigned char slot_extpkt_num;
} bwb_struct;

typedef struct {
		bool isTransEvent; //if a rx CAN message event triggers an event
		comms_tp_eventtype eventtype; //type of event
		union
		{
			//for certain request type -> requested to respond with one messages / x frame periodically,
			//x=0 means only one response, 1 is every frame
			unsigned char periodicity;

			struct
			{
				tsb_struct tsb; //ofs 0
				ibsb_struct ibsb; //ofs 1
				bwb_struct bwb; //ofs 2
				unsigned short trans_service_id; //ofs 3
			}trans_stat_response; //for LS_TRANS_STAT_RESPONSE

			struct
			{
				ibsb_struct ibsb; //ofs 0
				bwb_struct bwb; //ofs 1
			}ibsb_response; //for SU_IBSB_RESPONSE and LS_FRAME_SYNC

			struct
			{
				unsigned char data[8] ; //TODO: ??? unknown, fine granular structure?
			}status_response; //for SU_STATUS_RESPONSE

			struct
			{
				unsigned long long datetime; //yymmddhhssmm
				unsigned char offset;
				unsigned char validity;
			}gps_time_response;

			struct
			{
				unsigned long latitude;
				unsigned long longitude;
				unsigned short height;
				unsigned char validity;
			}gps_pos_response;

            //coding: loopback_mode in arq_command.h
            unsigned char mode;
			//TODO: more structs for CAN commands can be added here
		};
} comms_tp_event;

void can_write_packet(int fd,
					 comms_tp_packet* data, //in
					 comms_tp_tx_state* state, //inout
					 comms_tp_event* event, //out
					 unsigned short* bytes_left //out
					 );

void can_write_message(int fd,
		 	 	 	comms_tp_event* event); //in

void can_read(int fd,
			  comms_tp_packet* data, //out
			  comms_tp_rx_state* state, //inout
			  comms_tp_event* event, //out
			  comms_tp_rx_error* error, //inout
			  unsigned short* bytes_left //inout
			  );


unsigned char encode_packet_id(comms_tp_packet* data);
void decode_packet_id(comms_tp_packet* data, unsigned char packet_id_encoded);

unsigned long crc_cal(comms_tp_packet* data);
bool crc_check(comms_tp_packet* data);

void init_tsb(tsb_struct* tsb, bool error_bit, comms_tp_errortype errortype, bool trans_in_prog_bit, bool warning_bit);
void init_ibsb(ibsb_struct* ibsb, bool buf_normpkt_full[], unsigned char buf_ext_pkt_left);
void init_bwb(bwb_struct* bwb, bool slot_normpkt_open[], unsigned char slot_extpkt_num);

void print_tsb(tsb_struct* tsb, FILE* out);
void print_ibsb(ibsb_struct* ibsb, FILE* out);
void print_bwb(bwb_struct* bwb, FILE* out);

void print_tsb_2string(tsb_struct* tsb, char* out, size_t size);
void print_bwb_2string(bwb_struct* bwb, char* out, size_t size);
void print_ibsb_2string(ibsb_struct* ibsb, char* out, size_t size);

typedef	enum {OK_TO_SEND = 0, NO_SLOT = 1, NO_BUFFER = 2, SERVICE_ID_INVALID = 3} Send_acceptance;
Send_acceptance check_send_acceptable(ibsb_struct* ibsb, bwb_struct* bwb, unsigned char comms_tp_packet_id);

typedef enum {NO_LOOPBACK = 0, LOOPBACK_ONAIR = 1, LOOPBACK_LOCAL = 2, SOURCE_LOCAL = 3, SOURCE_ONAIR = 4, LOOPBACK_UNKNOWN = -1} loopback_mode_t;
typedef enum {awaiting_connection = 0, established = 1, broken = 2, unused = 3} link_status_t;

#endif /* COMMS_TP_H_ */
