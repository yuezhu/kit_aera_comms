#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
//#include <linux/eventfd.h>
#include "unistd.h"
#include <fcntl.h>
#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <math.h>
#include <errno.h>
#include <semaphore.h>
//#include <linux/time.h>

#include "amsg_c.h"
#include "clientsock.h"
#include "crc32.h"
#include "timer.h"

#include "port_settings.h"

#include "ainc/amsg.h"

static char* Version = "1.4";

static struct connCliInfo ConnCliInfoArray[2];

struct performanceTimer PTimer;
struct performanceTimer LatencyTimer;
static struct performanceTimer Program_timer;
static struct performanceTimer BW_timer;

static unsigned long long T3req_len_acc = 0;

static void usage(char *s)
{
	static char *usage_text  = "\
    -N no fake T3 request send\n\
	-n check real trigger \n\
	";

	printf("*********************************\n");
	printf("*KIT_comms application, ver %s *\n", Version);
	printf("*********************************\n");
	printf("usage: %s [options]\n", s);
    printf("%s", usage_text);
}

void T2_Outbuffer_Debug(char* Buffer);

int main(int argc, char **argv)
{
	extern char *optarg;
	extern int optind;

	int c;

	int rd_timeout = 125; //msec
	bool isFakeT3ReqGen = true;
	bool isRealTrigger = false;

    char *pname;
    pname = *argv;

    // parse command line
    while ((c = getopt(argc, argv, "Nn")) != EOF)
    {
		switch (c)
		{
			case 'N':
				isFakeT3ReqGen = false;
				break;
			case 'n':
				isRealTrigger = true;
				break;
			case 'h':
			default: usage(pname); exit(0);
		}
    }

    reset_performanceTimer(&BW_timer, false, stdout);
    reset_performanceTimer(&Program_timer, false, stdout);
    reset_performanceTimer(&LatencyTimer, false, stdout);
    reset_performanceTimer(&PTimer, true, stdout);

    fflush(stdout);

    /* the second file for writing */
    FILE* log_fd = fopen("./cli_log", "w");
    if (log_fd == NULL) {
    	fprintf(stderr, "%s - cannot append ./cli_log to write\n", __FUNCTION__);
    	exit(EXIT_FAILURE);
    }
    snapshot_performanceTimer(&PTimer, true, log_fd);
    fprintf(log_fd, "**** new-run ****\n");
    fclose(log_fd);

    chksum_crc32gentab();
    time_t seed = time(NULL);
    srand (1345138369);
    fprintf(stdout, "random seed: %u\n", (unsigned int)seed);

	FILE *fp;
	char line[200];                               // dummy name and LS_id
	fp = fopen("./ip_sbc.conf","r"); //same directory
	if (fp == NULL) {
		fprintf(stderr, "ip_sbc.conf not found under current folder!\n");
		exit(EXIT_FAILURE);
	}
	if (fgets(line,200,fp) == NULL) {
		fprintf(stderr, "ip_sbc.conf blank!\n");
		exit(EXIT_FAILURE);
	}
	printf("SBC ip is %s, strlen: %d\n", line, strlen(line));

    initConnCliInfo(&ConnCliInfoArray[0], "RT_CONN", line, PORT_NUMBER_RT);
    initConnCliInfo(&ConnCliInfoArray[1], "HQ_CONN", line, PORT_NUMBER_HQ);

	//int ind;
	int loopCount = 0;
	//int secondCnt = 0;
	void* amsg_c_r;
	uint8_t* amsg_c_t;
	uint8_t connIndex;

	uint32_t latency;
	uint16_t length;

	while (1)
	{
		// loop to recover
		if (loopCount % 100 == 0) {
			recoverConnectionIfLost(&ConnCliInfoArray[0]);
			recoverConnectionIfLost(&ConnCliInfoArray[1]);
		}
		amsg_c_r = NULL;
		readFromClientConnections(ConnCliInfoArray, 2, &amsg_c_r, &connIndex, rd_timeout*1000);
		if (amsg_c_r != NULL) {
			if (isRealTrigger == false) {
				autocheck_testdata(amsg_c_r, (connIndex==0)?true:false, &latency, true); //connIndex 0 is RT, otherwise HQ
			} else {
				T2_Outbuffer_Debug(amsg_c_r);
			}
			free(amsg_c_r);		// only necessary for ReadSockMsg, ReadSockMsgSM_TO uses static memories
		}
		if (snapshot_performanceTimer(&BW_timer, false, stdout) >= 500) { //each 0.5 second do something (2Hz) T2-req = 0~20 (station) x 12B/station = 0B~240B --> 240B/s
			if (isFakeT3ReqGen == true) {
				length = (rand()%20+1) * 12; //must be >= 10
				if (length > 0) {
					amsg_c_t = create_test_amsg_c(length, true);
					if (amsg_c_t != NULL) {
						if (writeToClientConnection(&ConnCliInfoArray[0], amsg_c_t) == 0) { //for RT
							T3req_len_acc += length;
						}
						free(amsg_c_t);
					} else {
						fprintf(stderr, "MEM_ERR!%s - socket connection send data malloc fail!\n", __FUNCTION__);
					}
				}
			}
			reset_performanceTimer(&BW_timer, false, stdout);
		}

		if (snapshot_performanceTimer(&Program_timer, false, stdout) >= 1*30*1000) { //30 seconds, plot something
			log_fd = fopen("./cli_log", "a");
		    if (log_fd == NULL) {
		    	fprintf(stderr, "%s - cannot append ./cli_log to write\n", __FUNCTION__);
		    	exit(EXIT_FAILURE);
		    }
		    snapshot_performanceTimer(&PTimer, true, log_fd); fprintf(log_fd, "alive, T3_req_acc: %llu\n", T3req_len_acc);
		    fclose(log_fd);

		    reset_performanceTimer(&Program_timer, false, stdout);
		}
		loopCount++;
	}
}

/**

 * @brief For testing that T2s are put correctly into Socklib messages

 * @param Buffer the msg buffer to be displayed

 */
/*
void T2_Outbuffer_Debug(char* Buffer) {
	Msg *RawData;
	AMSG *T2_msg;
	T2BODY *T2_MsgBody;
	int TimestampCnt, TimestampIdx;
	RawData = (Msg*) Buffer;
	T2_msg = (AMSG*) RawData->data;
	T2_MsgBody = (T2BODY*) T2_msg->body;
	TimestampCnt = (T2_msg->length - 5) >> 1;
	printf("%s: MSG length: %d \n", __FUNCTION__, RawData->length);
	printf("%s: AMSG length: %d, tag: %d, ID: %d, Sec: %d, TCnt: %d \n", __FUNCTION__,
			T2_msg->length, T2_msg->tag, T2_MsgBody->LS_id, (T2_MsgBody->t0[1] << 16) + T2_MsgBody->t0[0], TimestampCnt);

	for (TimestampIdx = 0; TimestampIdx < TimestampCnt; TimestampIdx++) {
		T2SSEC *nanos;
		nanos = (T2SSEC*) &T2_MsgBody->t2ssec[TimestampIdx];
		printf("%s: idx: %d, T2 nanos: %d\n", __FUNCTION__, TimestampIdx, T2NSEC(nanos));
		printf("%s: NS1 %d, NS2: %d, NS3: %d, ADC %d \n", __FUNCTION__, nanos->NS1, nanos->NS2, nanos->NS3, nanos->ADC);
	}
}
*/
void T2_Outbuffer_Debug(char* Buffer) {
	Msg *RawData;
	AMSG *T2_msg;
	T2BODY *T2_MsgBody;
	int TimestampCnt, TimestampIdx;
	RawData = (Msg*) Buffer;
	T2_msg = (AMSG*) RawData->data;
	T2_MsgBody = (T2BODY*) T2_msg->body;
	uint8_t* uint8_T2_p = (uint8_t*) T2_msg->body;

	TimestampCnt = (T2_msg->length - 5) >> 1;
	printf("%s: MSG length: %d \n", __FUNCTION__, RawData->length);
	printf("%s: AMSG length: %d, tag: %d, ID: %d, Sec: %d, TCnt: %d \n", __FUNCTION__,
			T2_msg->length, T2_msg->tag, T2_MsgBody->LS_id, (T2_MsgBody->t0[1] << 16) + T2_MsgBody->t0[0], TimestampCnt);

	for (TimestampIdx = 0; TimestampIdx < TimestampCnt; TimestampIdx++) {
		T2SSEC *nanos;
		//nanos = (T2SSEC*) &T2_MsgBody->t2ssec[TimestampIdx];
		nanos = (T2SSEC*) (uint8_T2_p+6+sizeof(T2SSEC)*TimestampIdx); //6bytes offset
		printf("%s: idx: %d, T2 nanos: %d\n", __FUNCTION__, TimestampIdx, T2NSEC(nanos));
		printf("%s: NS1 %d, NS2: %d, NS3: %d, ADC %d \n", __FUNCTION__, nanos->NS1, nanos->NS2, nanos->NS3, nanos->ADC);
	}
}

