/*
 * AeraDefs.h
 *
 *  Created on: 26.08.2009
 *      Author: weindl
 *      Description: holds basic definitions used in aera setup
 */

#ifndef AERADEFS_H_
#define AERADEFS_H_

#include <string>

/**
 * @brief namespace intended to be used inside the AERA daq
 */
namespace aera {

#define AEVB_MAJOR_VERSION_NO				0
#define AEVB_MAJOR_VERSION_STRING			"0"
#define AEVB_MINOR_VERSION_NO				2
#define AEVB_MINOR_VERSION_STRING			"2"
#define	AEVB_VERSION_STRING					AEVB_MAJOR_VERSION_STRING"."AEVB_MINOR_VERSION_STRING

// possible event types
#define AERA_T3_EVENT					1


#define AERA_CRS_DOMAIN					".crs.aera.local"

#define EB_SYSLOG_IDENT					"Event_Builder"

// global definitions for postmaster - aevb is client to postmaster, this way the LS messages arrive
#define POSTMASTER_BASE_NAME			"postmaster"		// base name extended with numbers 1-4, no more than 4 postmasters
extern std::string POSTMASTER_SERV_IP;
#define POSTMASTER_SERV_PORT			5013
#define POSTMASTER_COUNT    			1			// initially only one

#define T3_BUFFCOUNT_MAX				4			// how many buffers are processed at max from T3 in a sequence
#define PM_BUFFCOUNT_MAX				6			// how many buffers are processed at max from postmaster in a sequence
#define GUI_BUFFCOUNT_MAX				3			// how many buffers are processed at max from GUI in a sequence
#define PR_BUFFCOUNT_MAX				2			// how many buffers are processed at max from prompt in a sequence

// global definitions for t3_maker - aevb is a server for t3_maker
extern std::string T3_MAKER_SERV_IP;

#define T3_MAKER_SERV_PORT			5032

// global definitions for AERA GUI - aevb is server for gui
extern std::string GUI_SERV_IP;
#define GUI_SERV_PORT 				5034
#define GUI_EVNO_UP_INT				5			// the time interval in seconds to send actual event number to GUI
#define GUI_DB_UP_INT				30			// the time interval in seconds to write updated values for GUI to DB

#define PROMPT_SERV_PORT			5039		// open port for a prompt comand

// global general definitions
#ifdef LAB
#define DAQHOME				"/home/weindl"
#else
#define DAQHOME				"/home/daq"
#endif
//#define AERA_SAVE_PATH		"./data/"
#define AERA_SAVE_PATH		"./data/aevb/"

#define MAX_LOG_SIZE			100000000		// 100000000 = 100 MB
#define MAX_LOG_TIME			7200			// in seconds => 2 hours
#define AERA_LOG_PATH			"./log"  // JLBY 16/02/11

#define AERA_CONF_PATH		"./conf/"
#define AERA_CONF_DIR		"conf"				// common config directory for all AERA daq components, located in HOME
#define AEVB_CONF_FILE		"aevb_init"			// specific config file for aera event builder

#define CLIENT_ACCEPT_TIMEOUT		100			// this is the timeout in microseconds to wait for accepting clients - BE CAREFUL, valid for all and
												// may sum up! (several postmaster clients, one t3_maker, GUI ...)
#define CLIENT_READ_TIMEOUT			100			// this is the timeout in microseconds to wait at checking clients for read - BE CAREFUL, valid for all and
												// may sum up! (several postmaster clients, one t3_maker, GUI ...)

#define MAX_CONN_READ_FAILS			3			// how many read failures in a sequence are allowed until connection is closed

// time in seconds counted from last ... msg, after which a warning is written or an action happens. 0 means, there is no timeout
#define LAST_PM_MSG_TIMEOUT			10
#define LAST_T3_MSG_TIMEOUT			10
#define LAST_GUI_MSG_TIMEOUT		0
#define LAST_PR_MSG_TIMEOUT			0

#define T3_RATE_INTERVAL			600			// interval in seconds to calculate the actual T3 rate
#define EB_RATE_INTERVAL			600			// interval in seconds to calculate the actual Eventbody rate
#define EV_RATE_INTERVAL			600			// interval in seconds to calculate the actual event rate

#define REM_INF_COUNT				5			// how may T3 or LS data should be remembered

// TODO: ONLY ONE OF THE TWO FOLLOWING CAN BE USED AT ONCE!!!! define the time interval for AERA and apply to GPS timestamps
#define EVENT_MERGE_INTERVAL		2			// interval in seconds, within which data are considered to belong to one event
#define EVENT_MERGE_INTERVAL_NS		100000		// interval in nano seconds, within which data are considered to belong to one event

#define EVENT_TIMEOUT_TIME_INT		120			// interval in seconds, then event is regarded as timed out, from creation time
#define LS_PARAM_TIMEOUT_TIME_INT	10			// interval in seconds to wait for hardware settings after the request LS_GET
#define EVENT_LIST_MAX_SIZE			1000			// maximum number of events accepted in list, events coming while list is full are ignored
#define EVENT_SAVE_MIN_COUNT		3			// how many events must be minimum in list at opening new file, before saving (to ensure ordering)

// JLBY 16/02/11
#define USLEEP_MAIN_LOOP			100		// interval in microseconds passed to usleep in main loop

#define	TL_DEF						0x33333333

//#define LS_MAX_COUNT				25			// maximum number of LS, that can take part in the run. In first AERA stage we have 25
#define LS_MAX_COUNT				154			// maximum number of LS, that can take part in the run. In second AERA stage we have 154
// numbers taken from "MessageStructure v002"
#define LS_MAX_CHAN					4			// maximum number of channels of one LS (dutch or german)
#define LS_MAX_CHAN_FR				2			// maximum number of channels of one french LS
#define LS_TRACE_LENGTH				2048		// trace length in bytes for dutch and german electronics
#define LS_TRACE_LENGTH_FR			5120		// trace length in bytes for french electronics
#define LS_SAMPLE_SIZE				1.5			// sample size in byte, 12 bit = 1.5 byte, for dutch and german electronics
#define LS_SAMPLE_SIZE_FR			2.0			// sample size in byte, 16 bit = 2.0 byte, for french electronics
#define LS_MAX_DATA_SIZE			(LS_MAX_CHAN * LS_TRACE_LENGTH * LS_SAMPLE_SIZE)	// = 12288 bytes, max size of only data of one station (dutch or german), without the struct size sizeof(ls_data)
#define LS_MAX_DATA_SIZE_FR			(LS_MAX_CHAN * LS_TRACE_LENGTH_FR * LS_SAMPLE_SIZE_FR)	// = 12288 bytes, max size of only data of one station (dutch or german), without the struct size sizeof(ls_data)
#define LS_MAX_ID					(1<<16)												// = 65536

#define LS_SECOND_MASK				0xFF000000
#define LS_SECOND_OFFS				24
#define LS_SUBSEC_MASK				0x00FFFFFF
#define LS_SUBSEC_OFFS				0

// AW, 26.02.2010: asuming upper byte gives seconds, lower 3 give nanoseconds
#define GET_LS_GPS_SECONDS(lstime)	 ((lstime & LS_SECOND_MASK)>>LS_SECOND_OFFS)
#define GET_LS_GPS_SUBSECS(lstime)	 ((lstime & LS_SUBSEC_MASK)>>LS_SUBSEC_OFFS)

#define LS_T2_MAX_DELAY				10			// assumed maximum delay, a T2 timestamp information reaches the event builder
												// used to determine the correct minute out of the T2 - second information

#define LS_DATA_FREE_SPACE_SIZE		20			// free space in upcoming LS data, count of bytes
#define AERA_EVENT_FREE_SPACE_SIZE	8			// use 2 ints at first as free space forseen in aera event
#define AERA_FILE_FREE_SPACE_SIZE	8			// use 2 ints at first as free space forseen in aera event

//#define AERA_FILE_MAX_SIZE			(1*1024*1024*1024)		// max file size ( 1GB)
//#define AERA_FILE_MAX_SIZE			1000000000			// (1*1000*1000*1000) bytes		// max file size ( 1GB) for measurement
#define AERA_FILE_MAX_SIZE			1000000000			// (1*1000*1000) bytes, used for tests	// max file size ( 1MB) for tests
#define AERA_USE_FILE_SIZE			1
#define AERA_FILE_MAX_EV_COUNT		500						// max events per file (about 800MB)
#define AERA_USE_EVENT_COUNT		2
#define AERA_OPEN_NEW_FILE			AERA_USE_EVENT_COUNT

#define AERA_NORMAL_RUN				1
#define AERA_TEST_RUN				2
#define AERA_CALIB_RUN				3


// aera filename has format adXXXXX.fYYYY
#define AERA_NORMAL_FNAME_HEAD		"ad"
#define AERA_TEST_FNAME_HEAD		"adt"
#define AERA_CALIB_FNAME_HEAD		"adc"
#define AERA_MIN_FNAME_HEAD			AERA_NORMAL_FNAME_HEAD
#define AERA_MAX_FNAME_HEAD			AERA_CALIB_FNAME_HEAD
#define AERA_FNAME_SEP				"."
#define AERA_FNAME_TYPE				"f"

#define AERA_RUN_ID_DIGITS			6		// how many digits are used for run numbers in file name
#define AERA_FILE_ID_DIGITS			4		// how many digits are used for file numbers in file name

#define AERA_MIN_FILENAME_SIZE		(strlen(AERA_MIN_FNAME_HEAD) + AERA_RUN_ID_DIGITS + strlen(AERA_FNAME_SEP) + strlen(AERA_FNAME_TYPE) + AERA_FILE_ID_DIGITS)
#define AERA_MAX_FILENAME_SIZE		(strlen(AERA_MAX_FNAME_HEAD) + AERA_RUN_ID_DIGITS + strlen(AERA_FNAME_SEP) + strlen(AERA_FNAME_TYPE) + AERA_FILE_ID_DIGITS)

// this is only a temporary definition !!!!! info  size may change from event to event => change event handling to temporary keep events in memory and point to them, before saving to file
#define INFO_SIZE			2			// count of shorts with additional info, before data start


// definitions for 2th complements
#define TWO_C_NEG_DE				0x0800			// 0x800	marks the bit, which indicates negative numbers in 2th complement
#define BIT_MASK_DE					0x0FFF			// 0xFFF	gives the valid bit range for inversion to get numbers from 2th complement

#define FIRST_T3_IGNORE_CNT			0
#define FIRST_LS_IGNORE_CNT			0


//	how to decide, if T3maker restarts numbering from the beginning? Running event numbers range from 1 to 65535.
//  On restarting from 1, the difference between the event numbers get large!! (Even, if some event numbers are missed)
//	so it is checked if the actual and the last running event number(s) differ by a certain amount.
//	The second problem is, how to sort arriving events in a list, that is not simply increasing, but may have a (or more?) breaks
//	in numbering? As breaks move with removing events from list, they have to be identified each time on searching for correct place.
#define RUN_NO_BREAK_DIFF		50				// which difference in running event numbers indicate a break? use 50 in lab, where running event numbers range from 1 to 500 in tests, use 10000 in AERA Daq
#define RUN_NO_BREAK_DEPTH		10				// how many last events are searched for a break - use 10 in lab, where running event numbers range from 1 to 500 in tests, use 50 in AERA Daq



char *getTimeString(time_t jdate);
char *getTimeStringNow();
char *getTimeStringNowUS();			// US means UnderScore, sometimes underscore are better for filenames than double points

int getChannelCount(int channel_mask);

#ifdef USE_MSG
#define PMSV_OUTPUT_BUFFER_SIZE         65000		// based on RC buffers, this is used as maximum msg size for MSG, too
#endif


}		// namespace aera

#endif /* AERADEFS_H_ */
