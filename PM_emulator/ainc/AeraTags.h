/*
	atags.h - header with tag definitions of communication used by aera event builder

	AW, 11.11.2009



*/




#ifndef		__ATAGS_H__
#define		__ATAGS_H__


// tags from postmaster to event builder
#define POSTMASTER_TO_EVENT_BUILDER			0x1000
// subtags from postmaster to event builder
//#define LS_DATA							0x1001
// tags from event builder to postmaster
#define EVENT_BUILDER_TO_POSTMASTER			0x1050
// subtags from event builder to postmaster

// tags from GUI to aevb
#define GUI_TO_EVENT_BUILDER			0x1100
// subtags from GUI to event builder
#define GET_T3_ACT_EVENT_RATE			0x1101
#define GET_T3_TOT_EVENT_RATE			0x1102
#define GET_LS_ACT_DATA_RATE			0x1103
#define GET_LS_TOT_DATA_RATE			0x1104
#define GET_DISK_FREE_SPACE				0x1105
#define GET_ACT_ABS_EVENT_NO			0x1106
//#define SET_ACTUA_RUN_NO				0x1107		// AW, 23.02.2010: not needed any more
#define START_RUN						0x1108
#define STOP_RUN						0x1109

// tags from event builder to GUI
#define EVENT_BUILDER_TO_GUI			0x1150
// subtags to GUI
#define T3_ACT_EVENT_RATE				0x1151
#define T3_TOT_EVENT_RATE				0x1152
#define LS_ACT_DATA_RATE				0x1153
#define LS_TOT_DATA_RATE				0x1154
#define DISK_FREE_SPACE					0x1155
#define ACT_ABS_EVENT_NO				0x1156

// tags from t3_maker to event builder
#define T3_MAKER_TO_EVENT_BUILDER		0x1200
// subtags from t3_maker to event builder
//#define T3_EVENT_REQUEST_LIST			0x1201

#define EVENT_BUILDER_TO_T3_MAKER		0x1250
// subtags from event builder to t3_maker

// tags from prompt to event builder
#define PROMPT_TO_EVENTBUILDER			0x1300
#define SET_LOG_LEVEL					0x1301		// expects an int from 0-3 following
#define DUMP_CFG_PARAMS					0x1302
#define GET_CONN_STAT					0x1303
#define GET_DAQ_STAT					0x1304

#define DUMP_EVLIST						0x1310

#define DUMP_EBLIST						0x1320

#define DUMP_T3LIST						0x1330


// tags from Gui to event builder
#define GUI_TO_EVENTBUILDER				0x1350


#define EVENTBUILDER_TO_PROMPT			0x1350


#endif		// _ATAGS_H__
