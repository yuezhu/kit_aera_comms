//-------------------------------------------
// Before 19/11/12
// #define AERA_LOG_PATH                   "/home/ruehle/AERA/trunk/log"

//#define RCRECOVERY_COMMAND                RcSocketRecoveryExtended
#ifndef	USE_RC6
#define RCRECOVERY_COMMAND              0
#define INPUT_FIFO_SIZE                 16
#define OUTPUT_FIFO_SIZE                16
#define INPUT_QUICKLOOK                 10
#define OUTPUT_QUICKLOOK                10

#define PMCL_INPUT_BUFFER_SIZE          65000
#define PMCL_OUTPUT_BUFFER_SIZE         15000

#define PMSV_INPUT_BUFFER_SIZE          65000
#define PMSV_OUTPUT_BUFFER_SIZE         65000

#define T3UI_INPUT_BUFFER_SIZE          20000
#define T3UI_OUTPUT_BUFFER_SIZE         20000

#define EBUI_INPUT_BUFFER_SIZE          20000
#define EBUI_OUTPUT_BUFFER_SIZE         20000

#define T3EB_INPUT_BUFFER_SIZE          65000
#define T3EB_OUTPUT_BUFFER_SIZE         65000
#endif

//-------------------------------------------
// After 19/11/12 ---> RcSocketNew changes in Rc5 and Rc6 !
#ifdef	USE_RC6
#define BUFFER_SIZE                     6200 // Default value.

#define INPUT_FIFO_SIZE                 16
#define OUTPUT_FIFO_SIZE                16

#define TIMEOUT                         1000
#define SELECT_TIMEOUT                  1000000

// PM buffer sizes
#define LSPM_BUFFER_SIZE                BUFFER_SIZE // For T2 and Evt
#define PMLS_BUFFER_SIZE                6           // For GetT3

#define EBPM_BUFFER_SIZE                16          // No comm.
#define PMEB_BUFFER_SIZE                62000       // For Ls event

#define UIPM_BUFFER_SIZE                BUFFER_SIZE // TBD
#define PMUI_BUFFER_SIZE                BUFFER_SIZE // TBD

#define PRPM_BUFFER_SIZE                BUFFER_SIZE // TBD
#define PMPR_BUFFER_SIZE                BUFFER_SIZE // TBD

// T3 buffer sizes
#define T3PM_BUFFER_SIZE                600         // For T3List
#define PMT3_BUFFER_SIZE                600         // For T2

#define T3EB_BUFFER_SIZE                BUFFER_SIZE // For T3List
#define EBT3_BUFFER_SIZE                16          // No comm.

#define UIT3_BUFFER_SIZE                BUFFER_SIZE // TBD
#define T3UI_BUFFER_SIZE                BUFFER_SIZE // TBD

// EB buffer sizes
#define UIEB_BUFFER_SIZE                BUFFER_SIZE // TBD
#define EBUI_BUFFER_SIZE                BUFFER_SIZE // TBD

#define PREB_BUFFER_SIZE                BUFFER_SIZE // TBD
#define EBPR_BUFFER_SIZE                BUFFER_SIZE // TBD
#endif
