#ifndef _AMSG_H_
#define _AMSG_H_

#ifndef RcType_
//#include <RcType.h>
#endif

#include <stdint.h>		// AW, 12.01.2011: already included in RcType.h, but needed it here to build???

#define LS_BOOT     0
#define LS_UPLOAD   1
#define LS_RESET    2
#define LS_INITIALIZE 3
#define LS_SET      4
#define LS_GET      5
#define LS_MONITOR  6
#define LS_START    7
#define LS_STOP     8
#define LS_T2       9
#define LS_GETEVENT 10
#define LS_NO_EVENT 11
#define LS_EVENT    12
#define LS_GETMEM   13
#define LS_SETMEM   14
#define LS_SETBIT   15
#define LS_CLRBIT   16
#define LS_GETFILE  17
#define LS_SENDFILE 18
#define LS_EXEC     19
#define LS_CONNECT  20
#define LS_LISTEN   21
#define LS_SCRIPT   22
#define LS_CALIBRATE   23
#define LS_PARAM       24
#define LS_GET_EXT_SD_EVENT	    25
#define LS_GET_EXT_GUI_EVENT	26
#define LS_GET_EXT_FD_EVENT	    27

#define T3_EVENT_REQUEST_LIST           201
#define T3_EXT_SD_EVENT_REQUEST_LIST    202
#define T3_EXT_GUI_EVENT_REQUEST_LIST   203
#define T3_EXT_FD_EVENT_REQUEST_LIST    204

#define GUI_UPDATE_DB 401
#define GUI_INITIALIZE 402
#define GUI_START_RUN 403
#define GUI_STOP_RUN 404
#define GUI_TRIGGER 405
#define GUI_DELETE_RUN 406
#define GUI_ALLOW_PROMPT 407
#define GUI_BC_SETATTENUATION  450
#define GUI_BC_SWITCHIO  451
#define GUI_BC_SETMODE  452

// ########        from AERA tcp/ip transport definitions  START     ###############
#define ALIVE				9998
#define ALIVE_ACK			9999

#define MAX_AERA_BUF_LEN	65532	// this is the maximum buffer size, users can use (other bytes for protocol!

#define MIN_ALIVE_SEND_INTERVAL				5		// in seconds, minimum time between sending successive ALIVE messages
#define MAX_ALIVE_ACK_ANSWER_INTERVAL_LS	5		// in seconds
#define MAX_ALIVE_ACK_ANSWER_INTERVAL		3		// in seconds
// ########         from AERA tcp/ip transport definitions  END      ###############


#define LS_HWNL 1
#define LS_HWFR 2
#define LS_HWDU 3
#define LS_AEVB 7

#define TRIGGER_T3_EXT_SD       0x0100
#define TRIGGER_T3_EXT_GUI      0x0200
#define TRIGGER_T3_EXT_FD       0x0400


typedef struct{
  uint16_t length;
  uint16_t tag;
  uint16_t body[];
}AMSG;

/* Halfword offset of AMSG fields assuming struct is packed */
/* Necessary to avoid alignment problems on ARM */
/* These need to be updated if struct is changed */
#define AMSG_OFFSET_LENGTH  0
#define AMSG_OFFSET_TAG     1
#define AMSG_OFFSET_BODY    2

#define LSPOS(a)       (a&0xff)
#define LSHWTYPE(a)    ((a>>8)&0x7)
#define LSHWVERSION(a) ((a>>11)&0x1f)

#define T2FILL(a,b,c) {a->NS1=(b&0xff0000)>>16;a->NS2=(b&0xff00)>>8;a->NS3=(b&0xff);a->ADC=c&0xff;}
//#define T2NSEC(a) ((((unsigned int)(a->NS3))<<6)+(((unsigned int)(a->NS2))<<14)+(((unsigned int)(a->NS1))<<22))
#define T2NSEC(a) ((((unsigned int)((a)->NS3))<<6)+(((unsigned int)((a)->NS2))<<14)+(((unsigned int)((a)->NS1))<<22))
//#define T2ADC(a)  (a->ADC)
#define T2ADC(a)  ((a)->ADC)

typedef struct{
  unsigned char NS1;
  unsigned char NS2;
  unsigned char NS3;
  unsigned char ADC;
}T2SSEC;

#define T0(a) ((a[1]<<16)+a[0])

typedef struct{
  uint16_t LS_id;
  uint16_t t0[2];
  T2SSEC t2ssec[];
}T2BODY;

#define T3STATIONSIZE 3
#define T3STATFILL(a,b,c,d) {a->LS_id=b;a->sec=c&0xff;a->NS1=(d&0xff0000)>>16;a->NS2=(d&0xff00)>>8;a->NS3=(d&0xff);}

typedef struct{
  uint16_t LS_id;
  unsigned char sec;
  unsigned char NS1;
  unsigned char NS2;
  unsigned char NS3;
}T3STATION;

typedef struct{
  uint16_t event_nr;
  T3STATION  t3station[];
}T3BODY;


// AW, 07.02.2011 EVENTBODY version in use since ??.02.2011
#define USE_EVENTBODY_VERSION
#define EVENTBODY_VERSION		2

// since using the EVENTBODY_VERSION, the minimal header length has changed from 11 to 12
#ifdef USE_EVENTBODY_VERSION
#define MIN_EVHEADER_LENGTH 12
#else
#define MIN_EVHEADER_LENGTH 11
#endif


typedef struct{
  uint16_t length;
  uint16_t event_nr;
  uint16_t LS_id;
  uint16_t header_length;
  unsigned int GPSseconds;          // CT, aevb did not compile with other defn
  unsigned int GPSnanoseconds;      //
  //uint32_t GPSseconds;          // AW, changed definition for 64 bit
  //uint32_t GPSnanoseconds;      // AW, changed definition for 64 bit
  uint16_t trigger_flag;
  uint16_t trigger_pos;
  uint16_t sampling_freq;
  uint16_t channel_mask;
  uint16_t ADC_resolution;
  uint16_t tracelength;
#ifdef USE_EVENTBODY_VERSION
  uint16_t version;				// use a version information, in use since ??.02.2011
#endif
  uint16_t info_ADCbuffer[];
}EVENTBODY;

/* Halfword offset of EVENTBODY fields assuming struct is packed */
/* Necessary to avoid alignment problems on ARM */
/* These need to be updated if struct is changed */
#define EB_OFFSET_LENGTH       0
#define EB_OFFSET_EVENT_NR     1
#define EB_OFFSET_LS_ID        2
#define EB_OFFSET_HDR_LENGTH   3
#define EB_OFFSET_GPSSEC       4
#define EB_OFFSET_GPSNSEC      6
#define EB_OFFSET_TRIG_FLAG    8
#define EB_OFFSET_TRIG_POS     9
#define EB_OFFSET_SAMP_FREQ   10
#define EB_OFFSET_CHAN_MASK   11
#define EB_OFFSET_ADC_RES     12
#define EB_OFFSET_TRACELENGTH 13
#ifdef USE_EVENTBODY_VERSION
#define EB_OFFSET_VERSION     14
#define EB_OFFSET_ADCBUFFER   15
#else
#define EB_OFFSET_ADCBUFFER   14
#endif

typedef struct{
  uint16_t LS_id;
  uint16_t event_nr;
  unsigned char sec;
  unsigned char NS1;
  unsigned char NS2;
  unsigned char NS3;
}ls_geteventbody;

typedef struct{
  uint16_t event_nr;
  uint16_t LS_id;
} LS_NO_EVENTBODY;

#endif
