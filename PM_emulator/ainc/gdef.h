/* gdef.h
	AW, 29.04.2009


	Global definitions, used in the AERA DAQ


*/

#define TAGTYPE_LENGTH int
#define TAGTYPE_TAGNO int
#define TAGTYPE_SUBTAGNO int
#define MSGTYPE_LENGTH int

#define SOCKET_SIZE (32*(1<<10))

#define MAXBUFF_UNIX 32768

#ifndef MIN
#define MIN(a,b)   ((a) < (b) ? (a) : (b))
#endif
#ifndef MAX
#define MAX(a,b)   ((a) > (b) ? (a) : (b))
#endif
