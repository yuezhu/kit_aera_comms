/**
 * @file Event.h
 *
 * @brief generic base class for an event. It has only one member, that is a TimeStamp
 *
 * @author andreas.weindl@kit.edu
 *
 * @class Event Event.h ainc/Event.h
 *
 *      this is the base class for any type of Event. For example for AeraEvent, ...
 */

#ifndef EVENT_H_
#define EVENT_H_

#include <time.h>

#define UINT unsigned int

/**
 * @struct TimeStamp Event.h ainc/Event.h
 * @brief a TimeStamp represents the time of the Event in seconds and subseconds
 */
struct TimeStamp {
	time_t julian_date;
	UINT   subseconds;
};

/**
 * @brief base/generic class for events
 *
 * @class Event Event.h ainc/Event.h
 * This is the generic base class for an event with the single property TimeStamp
 */
class Event {
public:
	Event();
	Event(TimeStamp timeStamp);
	virtual ~Event();		// virtual ~Event() = 0;

//	virtual TimeStamp getTimeStamp() = 0;
	TimeStamp* getTimeStamp() {
		return &ts;
	}
	void setTimeStamp(TimeStamp tist);

private:
	TimeStamp ts;

};


#endif /* EVENT_H_ */
