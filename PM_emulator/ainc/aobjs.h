/*
 * aobjs.h
 *
 *  Created on: 25.08.2009
 *      Author: weindl
 */

#ifndef AOBJS_H_
#define AOBJS_H_

#include "defs.h"

//extern int tcCount;



struct TimeStamp {
	time_t julian_date;
	UINT   subseconds;
};

#endif /* AOBJS_H_ */
