/**
 * @file llog.h
 *
 * @author andreas.weindl@kit.edu
 *
 * @brief header file of a logging system
 *
 *      Offers logging functions, just accepting strings, so any "cout" can be converted easily into a log oput
 *      Functions differ in log level, the lower the number, the lower the amount of messages
 *
 */

#ifndef LLOG_H_
#define LLOG_H_

#include <stdlib.h>
#include <stdio.h>

#include <iostream>
using namespace std;


#define LLOG_LEVEL_0			0
#define LLOG_LEVEL_1			1
#define LLOG_LEVEL_2			2
#define LLOG_LEVEL_3			3

#define LLOG_LEVEL_NO			LLOG_LEVEL_0
#define LLOG_LEVEL_WARN			LLOG_LEVEL_1
#define LLOG_LEVEL_SOME			LLOG_LEVEL_2
#define LLOG_LEVEL_VERBOSE		LLOG_LEVEL_3

#define LLOG_LEVEL_MIN			LLOG_LEVEL_0
#define LLOG_LEVEL_MAX			LLOG_LEVEL_3

#define LLOG_LEVEL				LLOG_LEVEL_1

#define LLOGBUFSIZE				1024
#define MIN_LLOGBUFSIZE			256
#define MAX_LLOGBUFSIZE			(1024*1024)

extern int loglevel;

extern ostream llout0;
extern ostream llout1;
extern ostream llout2;
extern ostream llout3;

void llog(int lev, string logmsg) ;
void initLLogLevel (int level);
void initLLogPrintTime (int printTime);
void initLLogLogBufSize (int bufsize);
void initLLogFile (FILE* fd);
void initLLogBuf();

void initLog (int level);


void llout(char *fmt, ...);


/*
class Llog {
private:
	int loglevel;

public:
	Llog();
	~Llog();
	void llog(int lev, string logmsg) ;
	void initLog (int level);

}
Llog llog;
*/


#endif /* LLOG_H_ */
